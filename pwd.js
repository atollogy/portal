const debug = require('debug')('portal:pwd');

let extracted = function (pass) {
  const bcrypt = require('bcrypt');
  const saltRounds = 10; //the higher the better - the longer it takes to generate & compare
  debug(pass, bcrypt.hashSync(pass, saltRounds));
};

extracted('trimacatollogy1234!');
