// Load the SDK for JavaScript
const AWS = require('aws-sdk');
const config = require('./lib/dynamic-config');
const debug = require('debug')('portal:s3');

// AWS Configuration
var s3;

async function configure() {
  AWS.config.update({
    accessKeyId: config.imagesConfig.accessKeyID,
    secretAccessKey: config.imagesConfig.secretAccessKey,
    region: config.imagesConfig.region
  });

  s3 = new AWS.S3();
}

AWS.config.logger = console;

configure();

module.exports.signedLink = async function (resourceLocation, expires) {
  // One may want to consider using "signedLinkForExistentObj" function instead
  if (!s3) return null;
  expires = expires || 900;
  var parts = resourceLocation.substring('s3://'.length).split('/');
  var bucket = parts.shift();
  var key = parts.join('/');
  if (bucket.startsWith('atl-stg-') && config.imagesConfigStage) {
    const s3Stg = new AWS.S3({
      accessKeyId: config.imagesConfigStage.accessKeyID,
      secretAccessKey: config.imagesConfigStage.secretAccessKey,
      region: config.imagesConfig.region
    });
    return s3Stg.getSignedUrl('getObject', {Bucket: bucket, Key: key, Expires: expires});
  }
  return s3.getSignedUrl('getObject', {Bucket: bucket, Key: key, Expires: expires});
};

async function signedLinkForExistentBucketKey(bucket, key, expires) {
  if (!s3) return null;
  expires = expires || 900;

  var parameters = {
    Bucket: bucket,
    Key: key
  };

  return new Promise((resolve, reject) => {
    s3.headObject(parameters, function (err, data) {
      if (err) {
        if (err.statusCode === 404) {
          // not found
          resolve(null);
        } else {
          debug(err, err.stack);
          // an error other than 404 occurred
          reject(err);
        }
      } else {
        // successful response
        resolve(s3.getSignedUrl('getObject', {...parameters, Expires: expires}));
      }
    });
  });
}

module.exports.signedLinkForExistentBucketKey = signedLinkForExistentBucketKey;

module.exports.signedLinkForExistentObj = async function (resourceLocation) {
  if (!s3) return null;

  var parts = resourceLocation.substring('s3://'.length).split('/');
  var bucket = parts.shift();
  var key = parts.join('/');

  return signedLinkForExistentBucketKey(bucket, key);
};

async function directReturnBucketKey(bucket, key) {
  if (!s3) return null;

  var parameters = {
    Bucket: bucket,
    Key: key
  };

  return new Promise((resolve, reject) => {
    s3.getObject(parameters, function (err, data) {
      if (err) {
        if (err.statusCode === 404) {
          // not found
          resolve(null);
        } else {
          debug(err, err.stack);
          // an error other than 404 occurred
          reject(err);
        }
      } else {
        // successful response
        resolve(data.Body);
      }
    });
  });
}

module.exports.directReturnBucketKey = directReturnBucketKey;

module.exports.directReturnBucketObject = async function (resourceLocation) {
  if (!s3) return null;

  var parts = resourceLocation.substring('s3://'.length).split('/');
  var bucket = parts.shift();
  var key = parts.join('/');

  return directReturnBucketKey(bucket, key);
};