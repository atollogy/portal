const debug = require('debug')('portal:report-config');
const router = require('express-promise-router')();
const moment = require('moment');
const consul = require('../consul.js');
const config = require('../db/reportConfig.js');
module.exports = router;
const user = require('../lib/user-management/');

let knownCGRs;
let cgrUpdateTime;

const isAuthenticatedUser = (request, response, next) => {
  if (!user.authenticateCgr(request, response)) {
    response.status(401).send({ message: 'Not allowed'});
    return;
  }
  next();
};
const hasAuthenticatedFacility = (request, response, next) => {
  if (!user.authenticateFacility(request, response)) {
    response.status(401).send({ message: 'Not allowed'});
    return;
  }
  next();
};

router.get('/newConfig', isAuthenticatedUser, async (req, res) => {
  if (!knownCGRs || moment().diff(cgrUpdateTime) > 5 * 60 * 1000) {
    knownCGRs = await consul.getKnownCGRs();
    cgrUpdateTime = moment();
  }
  let respJson = {
    facility: '',
    cgr: '',
    gwName: '',
    subjectName: '',
    stepName: '',
    measureName: '',
    camId: '',
    extractType: '',
    extractKey: '',
    extractImgName: '',
    logoName: '',
    imgInterval: '',
    states: '',
    cgrs: knownCGRs,
    reportType: 'new'
  };
  res.render('reportConfig', respJson);
});

router.post('/updateConfig', async (req, res) => {
  let cgrs = req.body.cgrs;
  let params = req.body;
  try {
    await config.setConfig(cgrs, params);
    res.status(200).send('Configurations saved');
  } catch (e) {
    debug(`POST::updateConfig swallowing an error: ${e}.`);
    res.status(500).send('There was some error saving the configs.');
  }
});

router.get('/renderConfig/:cust', isAuthenticatedUser, async (req, res) => {
  const queryCust = req.params.cust;
  let result = await config.getConfig(queryCust);
  // res.send(result);
  res.render('viewReportConfig', {res: result, cust: queryCust});
});

router.get('/getConfig/:cust', isAuthenticatedUser, async (req, res) => {
  const  customer = req.params.cust || req.query.customer;
  let queryParams = {};
  queryParams.date = (req.query.date && moment(req.query.date, 'YYYY-MM-DD').isValid()) ? req.query.date : null;
  queryParams.shift = req.query.shift || 'day';
  queryParams.facility = req.query.facility || null;
  queryParams.previous = req.query.prev === 'true';
  queryParams.from = req.query.time ? moment(req.query.time).startOf('day') : moment(req.query.date).startOf('day');
  queryParams.to = req.query.time ? moment(req.query.time) : moment(req.query.date);
  res.send(await config.getConfig(customer, queryParams));
});

router.get('/getYardConfig', isAuthenticatedUser, async (req, res) => {
  let {assetConfigs, settings} = await config.getCustomerConfigs({customer: req.query.customer, facility: req.query.facility});
  let cfgObj = {};
  cfgObj[req.query.customer] = {};
  cfgObj[req.query.customer][req.query.facility] = {
    timeSpan: {}
  };
  assetConfigs.forEach(x=> {
    cfgObj[req.query.customer][req.query.facility]['timeSpan'][x.state] = {
      color: x.color,
      min: x.min_time,
      max: x.max_time
    };
  });
  if(assetConfigs.length) {
    cfgObj[req.query.customer][req.query.facility]['timeFilter'] = cfgObj[req.query.customer][req.query.facility]['timeSpan']['long']['max'];
  }
  if(settings.length) {
    cfgObj[req.query.customer][req.query.facility] = {...cfgObj[req.query.customer][req.query.facility], ...settings[0].settings};
  }
  res.send(cfgObj);
});

router.post('/deleteConfig', isAuthenticatedUser, async (req, res) => {
  const cust = req.body.cust;
  let params = req.body;
  await config.deleteConfig(cust, params);
  res.status(200).send('Config deleted.');
});

router.get('/editConfig', isAuthenticatedUser, async (req, res) => {
  let editObj = req.query.editObj;
  const obj = JSON.parse(decodeURIComponent(editObj));
  let objArr = [];
  objArr.push(obj);
  if (!knownCGRs || moment().diff(cgrUpdateTime) > 5 * 60 * 1000) {
    knownCGRs = await consul.getKnownCGRs();
    cgrUpdateTime = moment();
  }
  let resObj = objArr.map(x => {
    return {
      facility: x.facility,
      cgr: x.cgr,
      gwName: x.gateway_name,
      subjectName: x.subject_name,
      stepName: x.step_name,
      measureName: x.measure_name,
      camId: x.camera_id,
      extractType: x.extract_type,
      extractKey: x.extract_key,
      extractImgName: x.extract_image_name,
      imgInterval: x.cs_config.img_interval,
      logoName: x.cs_config.image_name,
      states: x.cs_config.states,
      cgrs: knownCGRs,
      reportType: 'edit'
    };
  });
  res.render('reportConfig', resObj.pop());
});

function newNavigationManager() {
  const navigationConfig = {};

  return {
    getNavigationConfig: async (segment) => {
      if (!Object.prototype.hasOwnProperty.call(navigationConfig, '_ready')) {
        const theNavigationConfig = await user.fetchNav();
        Object.assign(navigationConfig, theNavigationConfig);
        Object.defineProperty(navigationConfig, '_ready', { value: true, enumerable: false, writable: false });
        Object.freeze(navigationConfig);
      }

      return navigationConfig[segment];
    }
  };
}

const navigationManager = newNavigationManager();

// export our router to be mounted by the parent application
module.exports = router;

router.get('/navCfg', isAuthenticatedUser, hasAuthenticatedFacility, async (req, res) => {
  const cgrNavs = await navigationManager.getNavigationConfig('cgrNavs');
  //Find config for customer/facility
  if (cgrNavs[req.query.customer]) {
    //If facility specific config does not exist just use common cfg across all facilities else use the one for facility in question
    const pages = cgrNavs[req.query.customer][req.query.facility] || cgrNavs[req.query.customer];
    Object.keys(pages).forEach((key) => (pages[key] == null) && delete pages[key]);  //delete pages with null config
    delete pages.landing; //remove landing page
    let navPages = JSON.parse(JSON.stringify(pages));
    if(navPages['tableau'] && (!req.user.tableau || (req.user.tableau.showNav && !req.user.tableau.group.some(y=> y===`${req.query.customer}:${getFacilityWithNocapsAndSpace(req.query.facility)}`)))
      // && (req.user.tableau.showNav && !req.user.tableau.group.some(y=> y!==`${req.query.customer}:${getFacilityWithNocapsAndSpace(req.query.facility)}`))
      && req.user.cgrs[0] !== 'atl') {
      delete navPages.tableau;
    }
    const nav = Object.keys(navPages).map(k => ({ //return structure for d3 to digest
      label: navPages[k].label,
      path: navPages[k].path
    }));
    res.send(nav);
  } else {
    res.send(`nav config for ${req.query.customer} does not exist yet`);
  }
});

router.get('/landingPage', isAuthenticatedUser, hasAuthenticatedFacility, async (req, res) => {
  if (req.query.customer) {
    const cgrs = [req.query.customer];
    const landingPage = {};
    landingPage.url = await user.calcLandingPage(cgrs);
    res.send(landingPage);
  } else {
    res.send(`Invalid customer Name: ${req.query.customer}`);
  }
});

router.get('/featureCfg', isAuthenticatedUser, hasAuthenticatedFacility, async (req, res) => {
  const isAtl = req.session.cgrs.some(cgr => cgr == 'atl');
  const cgrNavs = await navigationManager.getNavigationConfig('cgrNavs');
  //Find config for customer/facility
  if (cgrNavs[req.query.customer]) {
    //If facility specific config does not exist just use common cfg across all facilities else use the one for facility in question
    const pages = cgrNavs[req.query.customer][req.query.facility] || cgrNavs[req.query.customer];
    const page = pages && pages[req.query.page]; //if page has associated config
    const pageFeatures = page && page.features; //if features are specified
    const features = isAtl ? (pageFeatures && pageFeatures.atl || {}) : (pageFeatures && pageFeatures.customer || {}); //session user based selection of features
    res.send(features);
  } else {
    res.send(`nav config for ${req.query.customer} does not exist yet`);
  }

});

router.get('/tableauCfg', isAuthenticatedUser, async (req, res) => {
  const cgrNavs = await navigationManager.getNavigationConfig('cgrNavs');
  //Find config for customer/facility
  if (cgrNavs[req.query.customer]) {
    const tableauConfig = cgrNavs[req.query.customer]['tableau'];
    const tableauUrl = tableauConfig && tableauConfig['tableau_url']; //if page has associated config
    res.send({url: tableauUrl});
  } else {
    res.send(`nav config for ${req.query.customer} does not exist yet`);
  }

});

router.get('/allNavs', isAuthenticatedUser, hasAuthenticatedFacility, async (req, res) => {
  const navs = await navigationManager.getNavigationConfig('cgrNavs');
  res.send(navs);
});

function getFacilityWithNocapsAndSpace(facility) {
  return facility.toLowerCase().replace(/\s/g, '');
}
