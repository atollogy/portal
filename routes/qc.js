const router = require('express-promise-router')();
const {getShortHash: version, getCurrentYear} = require('../lib/health');
const debug = require('debug')('portal:routes:qc');

module.exports = router;
const user = require('../lib/user-management/');

router.get('/', async (req, res) => {
  debug(req.user.cgrs);
  if (req.user.cgrs.some(x => x !== 'gatwickairport' && x !== 'tia' && x !== 'atl')) {
    res.status(404).send('not found');
    return;
  }
  res.render('../views/qc', {user: req.user, cgr: req.user.cgrs[0], version: await version(), year: getCurrentYear()});
});

router.get('/images', async (req, res) => {
  if (!user.authenticateCgr(req, res)) return;
  let params = req.query;
  debug(params);
  res.render('../views/qc/images', {user: req.user, cgr: req.user.cgrs[0], version: await version(), year: getCurrentYear()});
});
