
module.exports = (function (){
  const hasOwnProperty = (target, property) => Object.prototype.hasOwnProperty.call(target, property);

  const checkPermissions = async (request, response, next) => {
    if (!request.user.cgrs.some((customer) => customer === 'atl')) {
      response.status(401).send({ message: 'Not Authorized' });
      return;
    }

    try {
      await next(request, response);
    } catch (error) {
      response.status(500).send({ error: `Unexpected error ${error.message}` });
    }
  };

  const checkQueryParameter = (request, parameter, defaultValue = undefined) => {
    if (hasOwnProperty(request.query, parameter)) {
      return request.query[parameter];
    }

    return 'function' === typeof defaultValue ? defaultValue.call({}) : defaultValue;
  };

  const checkPathParameter = (request, parameter) => {
    if (!hasOwnProperty(request.params, parameter)) {
      throw new Error(`${parameter} is required!`);
    }

    return request.params[parameter];
  };

  return Object.freeze({
    checkQueryParameter,
    checkPathParameter,
    checkPermissions,
    hasOwnProperty
  });
}());