const router = require('express-promise-router')();
const user = require('../lib/user-management/');
const { getApplicationContext } = require('../lib/view-context');

module.exports = router;

router.get('/', async (req, res) => {
  if (!user.authenticateCgr(req, res)) return;
  res.render('dashboard/index', await getApplicationContext(req));
});

router.get('/:cust', async function (req, res) {
  if (!user.authenticateCgr(req, res)) return;
  res.render(`../views/dashboard/gwStatus.ejs`, await getApplicationContext(req, { customer: req.params.cust }));
});
