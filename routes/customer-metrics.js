const router = require('express-promise-router')();
const metrics = require('../db/metrics');
const { checkQueryParameter, checkPathParameter, checkPermissions } = require('./utils');

const DEFAULT_OFFSET = 0;
const DEFAULT_LIMIT = 100;

module.exports = router;

router.get('/customer/:customer/filters',  async (req, resp) => {
  checkPermissions(req, resp, async (request, response) => {
    const offset = checkQueryParameter(request, 'offset', DEFAULT_OFFSET);
    const limit = checkQueryParameter(request, 'limit', DEFAULT_LIMIT);

    const customer = checkPathParameter(request, 'customer');

    const _links = [];
    const filters = [];
    const [ cameras, steps ] = await Promise.all([
      metrics.fetchGateways({ customer, offset, limit }),
      metrics.fetchStepNames({ customer, offset, limit })
    ]);

    if (steps && steps.length) {
      steps.forEach(({ step_name }) => filters.push({ type: 'step', value: step_name }));
    }
    if (cameras && cameras.length) {
      cameras.forEach(({ gateway_id }) => filters.push({ type: 'camera', value: gateway_id }));
    }

    const count = filters.length;
    response.status(200).send({ filters: Object.freeze(filters), count, offset, _links });
  });
});

router.get('/customer/:customer', async (req, resp) => {
  checkPermissions(req, resp, async (request, response) => {
    const offset = checkQueryParameter(request, 'offset', DEFAULT_OFFSET);
    const limit = checkQueryParameter(request, 'limit', DEFAULT_LIMIT);

    const customer = checkPathParameter(request, 'customer');

    const _links = [];
    const records = await metrics.fetchMetrics({ customer, offset, limit });

    const steps = records.reduce((mapOfSteps, { step_name }) => {
      if (step_name && 'string' === typeof step_name) {
        mapOfSteps[step_name] = 1 + (mapOfSteps[step_name] || 0);
      }
      return mapOfSteps;
    }, {});
    const cameras = records.reduce((mapOfCameras, { gateway_id }) => {
      if (gateway_id && 'string' === typeof gateway_id) {
        mapOfCameras[gateway_id] = 1 + (mapOfCameras[gateway_id] || 0);
      }
      return mapOfCameras;
    }, {});

    const measures = records.reduce((mapOfMeasures, { collection_time, total_count }) => {
      const measure = mapOfMeasures[collection_time] || ({ name: collection_time, value: 0 });
      measure.value += Number(total_count);
      mapOfMeasures[collection_time] = measure;
      return mapOfMeasures;
    }, {});

    const count = records.length;
    response.status(200).send({
      measures: Object.values(measures),
      cameras: Object.keys(cameras),
      steps: Object.keys(steps),
      count, offset, _links
    });
  });
});

router.get('/customer/:customer/cameras', async (req, resp) => {
  checkPermissions(req, resp, async (request, response) => {
    const offset = checkQueryParameter(request, 'offset', DEFAULT_OFFSET);
    const limit = checkQueryParameter(request, 'limit', DEFAULT_LIMIT);

    const customer = checkPathParameter(request, 'customer');

    const _links = [];
    const records = await metrics.fetchGateways({ customer, offset, limit });

    const cameras = records.reduce((mapOfCameras, { gateway_id }) => {
      if (gateway_id && 'string' === typeof gateway_id) {
        mapOfCameras[gateway_id] = 1 + (mapOfCameras[gateway_id] || 0);
      }
      return mapOfCameras;
    }, {});

    const count = records.length;
    response.status(200).send({ cameras: Object.keys(cameras), count, offset, _links });
  });
});

router.get('/customer/:customer/camera/:gateway', async (req, resp) => {
  checkPermissions(req, resp, async (request, response) => {
    const offset = checkQueryParameter(request, 'offset', DEFAULT_OFFSET);
    const limit = checkQueryParameter(request, 'limit', DEFAULT_LIMIT);

    const customer = checkPathParameter(request, 'customer');
    const gateway = checkPathParameter(gateway, 'gateway');

    const _links = [];
    const records = await metrics.fetchMetrics({ customer, gateway, offset, limit });

    const cameras = { [ gateway ]: 0 };
    const steps = records.reduce((mapOfSteps, { step_name }) => {
      if (step_name && 'string' === typeof step_name) {
        mapOfSteps[step_name] = 1 + (mapOfSteps[step_name] || 0);
      }
      return mapOfSteps;
    }, {});
    const measures = records.map(({ collection_time, total_count }) => ({ name: collection_time, value: total_count }));

    const count = records.length;
    response.status(200).send({ measures, cameras: Object.keys(cameras), steps: Object.keys(steps), count, offset, _links });
  });
});

router.get('/customer/:customer/steps', async (req, resp) => {
  checkPermissions(req, resp, async (request, response) => {
    const offset = checkQueryParameter(request, 'offset', DEFAULT_OFFSET);
    const limit = checkQueryParameter(request, 'limit', DEFAULT_LIMIT);

    const customer = checkPathParameter(request, 'customer');

    const _links = [];
    const records = await metrics.fetchStepNames({ customer, offset, limit });

    const steps = { };
    const cameras = records.reduce((mapOfCameras, { gateway_id }) => {
      if (gateway_id && 'string' === typeof gateway_id) {
        mapOfCameras[gateway_id] = 1 + (mapOfCameras[gateway_id] || 0);
      }
      return mapOfCameras;
    }, {});
    const measures = records.map(({ collection_time, total_count }) => ({ name: collection_time, value: total_count }));

    const count = records.length;
    response.status(200).send({ measures, cameras: Object.keys(cameras), steps: Object.keys(steps), count, offset, _links });
  });
});

router.get('/customer/:customer/step/:step_name', async (req, resp) => {
  checkPermissions(req, resp, async (request, response) => {
    const offset = checkQueryParameter(request, 'offset', DEFAULT_OFFSET);
    const limit = checkQueryParameter(request, 'limit', DEFAULT_LIMIT);

    const customer = checkPathParameter(request, 'customer');
    const step = checkPathParameter(request, 'step_name');

    const _links = [];
    const records = await metrics.fetchMetrics({ customer, step, offset, limit });

    const steps = { [ step ]: 0 };
    const cameras = records.reduce((mapOfCameras, { gateway_id }) => {
      if (gateway_id && 'string' === typeof gateway_id) {
        mapOfCameras[gateway_id] = 1 + (mapOfCameras[gateway_id] || 0);
      }
      return mapOfCameras;
    }, {});
    const measures = records.map(({ collection_time, total_count }) => ({ name: collection_time, value: total_count }));

    const count = records.length;
    response.status(200).send({ measures, cameras: Object.keys(cameras), steps: Object.keys(steps), count, offset, _links });
  });
});
