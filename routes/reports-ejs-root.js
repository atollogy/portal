const shiftInfo = require('../db/shifts');
const subjectReports = require('../db/subjects');
const { getApplicationContext } = require('../lib/view-context');
const userManagement = require('../lib/user-management');

const router = require('express-promise-router')();

['cycleList', 'subjectsList', 'cycle', 'cycleStatus', 'deliveryLog',
  'subjectSummary', 'subjectDetail', 'safetySubjectDetail', 'safetySubjectSummary',
  'YardSubjectDetail', 'YardSubjectSummary', 'tableau',
  'workOrder', 'workerZones', 'beaconTracking',
  'cvg-terminalA-gates', 'tia-gates', 'gatwick-stands',
  'vdb-map', 'gateStatus', 'error', 'beacon', 'gateTurn', 'aia-gates', 'filters'].forEach((reportView) => {
  router.get('/' + reportView, async function (req, res) {
    res.render(`../views/reports/${reportView}.ejs`, await getApplicationContext(req));
  });
});

router.get('/metrics', async function (req, res) {
  res.render(`../views/metrics/index.ejs`, await getApplicationContext(req));
});

function isAt0l(user) {
  return user.cgrs.filter(e => e === 'atl').length > 0;
}

['homepage'].forEach((reportView) => {
  router.get('/' + reportView, async function (req, res) {
    req.user.homePage = await userManagement.calcLandingPage(req.user.cgrs);
    if (isAt0l(req.user)) {
      res.render(`../views/reports/${reportView}.ejs`, await getApplicationContext(req));
    } else {
      res.status(401).send('unauthorized');
    }
  });
});

['kiosk/subjectSummary'].forEach((screeenView) => {
  router.get('/' + screeenView, async function (req, res) {
    res.render(`../views/screens/${screeenView}.ejs`, await getApplicationContext(req, {
      subject: 'Mill 5',
      secondarySubjects: ['Mill 13', 'Mill 12', 'Mill 6']
    }));
  });
});

router.get('/summaryGrid', async function (req, res) {
  let customer = req.query.customer;
  if (!req.user.cgrs.some(cgr => cgr == 'atl' || cgr == customer)) {
    res.status(404).render('../views/reports/error.ejs', await getApplicationContext(req));
    return;
  }
  let params = {};
  params.date = req.query.date || null;
  params.facility = req.query.facility;
  if(!params. facility) {
    let facility = await shiftInfo.listFacilities(customer);
    params.facility = facility[0].facility;
  }
  if (customer === 'lumenetix') {
    params.measures = ['bulb'];
    params.states = ['solid blue', 'solid red', 'solid amber'];
  } else {
    params.measures = ['composite'];
    params.states = ['running'];
  }
  params.shift = null;
  params.previous = false;
  let result = await subjectReports.fetchMetrics(customer, params, params.facility);
  let subjectArr = [];
  let resultObj = {};
  let shifts = [];
  result.forEach(x => {
    if(x.shift_name) {
      let subjects = {};
      subjects['shift_utilization'] = {};
      subjects['shift_total_duration'] = {};
      shifts.push(x.shift_name);
      if (!subjectArr.some(d => d.subject == x.subject_name)) {
        subjects.subject = x.subject_name;
        subjects['shift_utilization'][x.shift_name] = Number(x.utilization).toFixed(1);
        subjects['shift_total_duration'][x.shift_name] = Number(x.total_duration);
        subjectArr.push(subjects);
      } else {
        let i = subjectArr.findIndex(d => d.subject == x.subject_name);
        subjectArr[i]['shift_utilization'][x.shift_name] = Number(x.utilization).toFixed(1);
        subjectArr[i]['shift_total_duration'][x.shift_name] = Number(x.total_duration);
      }
    }
  });

  let uniqueShifts = shifts.filter(function (item, pos) {
    return item!=null && shifts.indexOf(item) == pos;
  });

  resultObj.date = result.length ? result[0].date : null;
  resultObj.station = subjectArr;
  let next_route = '../views/reports/summaryGrid.ejs';
  res.render(next_route, await getApplicationContext(req, {
    cust: customer,
    result: resultObj,
    shifts: uniqueShifts
  }));
});

module.exports = router;
