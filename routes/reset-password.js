const debug = require('debug')('portal:reset-password');
const router = require('express-promise-router')();
const config = require('../lib/dynamic-config');
const AWS = require('aws-sdk');
const user = require('../lib/user-management/');
const nodemailer = require('nodemailer');
const bcrypt = require('bcrypt');
var randomstring = require('randomstring');
const moment = require('moment');
const _ = require('lodash');
const snsServices = require('../lib/sns-services');

const appBaseUrl = config.appBaseUrl;
const saltRounds = 10;
const SMTP_URL = {
  SES: new AWS.SES({
    accessKeyId: config.accessKeyID,
    secretAccessKey: config.secretAccessKey,
    region: config.region,
    apiVersion: '2010-12-01'
  })
};

const templates = Object.freeze({
  html: ({ firstName, host, appBaseUrl: theBaseURL, resetToken }) => {
    return (
      `<p>Hello ${firstName},<br/><br/>We have received a request to reset your Atollogy account. 
      <br/><br/><a href="${host}${theBaseURL}/reset/${resetToken}">Click the link to reset your password.</a><br/><br/>Thank You.</p>`
    );
  },
  text: ({ firstName, host, appBaseUrl: theBaseURL, resetToken }) => {
    return (
      `Hello ${firstName}
      We have received a request to reset your Atollogy account. Click the below link to reset your password.
      ${host}${theBaseURL}/reset/${resetToken}`
    );
  }
});

router.post('/', async (req, res) => {
  let params = {};
  params.email = req.body.email;
  let results = await user.fetchByEmail(params);

  const host = _.get(req.headers.referer.match(/(.*)\/portal/), '[1]', 'https://atollogy.com');
  // Changing back from 'https://portal.atollogy.com' to 'https://atollogy.com' to fix the Password Link Bug
  // TODO: Revert this change and make it backward compatible

  try {
    if (results.length) {
      const userInfo = results[0];
      params.resetToken = randomstring.generate(40);
      params.tokenCreated = moment.utc().format('YYYY-MM-DD HH:mm:ss.SSS');
      await user.updateByEmail(params);
      const emailData = {
        from: 'admin@atollogy.com',
        to: params.email,
        subject: 'Atollogy Password Reset Link',
        text: templates.text({ firstName: userInfo.first_name, host, appBaseUrl, resetToken: params.resetToken }),
        html: templates.html({ firstName: userInfo.first_name, host, appBaseUrl, resetToken: params.resetToken })
      };
      sendEmail(emailData);
    }
  } catch (e) {
    res.render('linkExpired', {error: e});
  }
  res.send({status: 200});
});

function sendEmail(emailData, smtpUrl = SMTP_URL) {
  const transporter = nodemailer.createTransport(smtpUrl);
  return transporter.sendMail(emailData)
    .then(info => debug('Message sent: ' + info.response))
    .catch(err => debug('Error sending an email: ' + err));
}

router.get('/:token', async (req, res) => {
  let results = await user.fetchByResetToken(req.params);
  if (results.length) {
    let currentDate = moment().utc();
    let tokenExpireDate = moment(results[0].reset_token_created);
    if (currentDate.diff(tokenExpireDate, 'minutes') <= 1440) {
      res.render('resetPassword', {email: results[0].username});
    } else {
      res.render('linkExpired', {error: 'Reset Password Link has expired. Please try again resetting your password.'});
    }
  } else {
    res.render('linkExpired', {error: 'Incorrect Reset Password Link. Please use the link sent in your email address'});
  }
});

router.post('/finish-reset', async (req, res) => {
  let params = {};
  params.resetToken = req.body.token;
  params.pwd = req.body.pwd;
  params.encryptPwd = bcrypt.hashSync(params.pwd, saltRounds);
  try {
    let rows = await user.updatePasswordByToken(params);
    const dataSns = {
      requestor: 'Atollogy',
      username: rows[0].username,
      password: params.pwd
    };
    await snsServices.publishSNS({
      arn: config.updatePasswordSNSArn,
      subject: config.updatePasswordSNSTopic
    }, {
      ...dataSns
    });
    res.send({
      flashMessage: 'success'
    });
  } catch (e) {
    res.send({flashMessage: 'error ' + e});
  }
});

module.exports = router;
