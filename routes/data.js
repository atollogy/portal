const router = require('express-promise-router')();
const moment = require('moment');
const consul = require('../consul.js');
const stats = require('../db/stats.js');
const beacons = require('../db/beacons.js');
const imageSteps = require('../db/imageStep.js');
const cycles = require('../db/cycle.js');
const subjects = require('../db/subjects');
const YardSubjects = require('../db/YardSubjects');
const safetySubjects = require('../db/safetySubjects.js');
const reports = require('../db/report-listing.js');
const shifts = require('../db/shifts');
const user = require('../lib/user-management/');
const dashboard = require('../db/new-dashboard.js');
const debug = require('debug')('portal:data');
const soiConfig = require('../db/soiConfig.js');

const isAuthenticatedUser = (request, response, next) => {
  if (!user.authenticateCgr(request, response)) {
    response.status(401).send({ message: 'Not allowed'});
    return;
  }
  next();
};

const isAtollogist = (request, response, next) => {
  if (!request.user.cgrs.some(cgr => cgr === 'atl')) {
    response.status(403).send('not found');
    return;
  }
  next();
};

// export our router to be mounted by the parent application
module.exports = router;

var knownCGRs;
let knowNodeName, lastCGR, nodeNameUpdateTime;
var cgrUpdateTime;

const fetchCycleTimeReport = async (request) => {
  const oQueryParams = Object.assign({}, request.query);
  const oPathParams = Object.assign({}, request.params);

  Object.keys(oQueryParams).forEach((parameter) => {
    const value = oQueryParams[parameter];
    if (decodeURI(value) !== decodeURIComponent(value)) {
      oQueryParams[parameter] = decodeURIComponent(value);
    }
  });
  Object.keys(oPathParams).forEach((parameter) => {
    const value = oPathParams[parameter];
    if (decodeURI(value) !== decodeURIComponent(value)) {
      oPathParams[parameter] = decodeURIComponent(value);
    }
  });

  let cycleParams = {};
  if (oQueryParams.date && moment(oQueryParams.date, 'YYYY-MM-DD').isValid()) {
    cycleParams.date = oQueryParams.date;
  }
  if (oQueryParams.shift) {
    cycleParams.shift = oQueryParams.shift;
  }
  if (oQueryParams.version) {
    cycleParams.version = oQueryParams.version;
  }
  if (oQueryParams.facility) {
    cycleParams.facility = oQueryParams.facility;
  }

  if (!(cycleParams.date && cycleParams.shift)) {
    return [{ statistics: { total: Math.pow(120 * 60 * 1000, 2), count: 1 } }];
  }

  cycleParams.to = null;
  cycleParams.from = null;
  cycleParams.qualifier = 'license_plate';
  cycleParams.tagNames = !request.user.cgrs.some(cgr => cgr === 'atl') ? ['customer_tag'] : ['customer_tag', 'qc_tag'];

  cycleParams.filters = [];

  if (isWhiteListCgr(request)) {
    cycleParams.filters.push('whitelist');
  }

  const listOfFacilityReports = await cycles.cycleEventsWithAttributes(request.params.cust, Object.assign({}, cycleParams));

  const cycleReportResponse = listOfFacilityReports.map((listOfSubjectEvents) => {
    const congruentCycles = listOfSubjectEvents.iterations.filter(eventObject => {
      return eventObject.states.some(stateObject => stateObject.is_start || stateObject.is_end );
    });

    const metrics = congruentCycles.map(eventObject => {
      return {duration: moment(eventObject.states[eventObject.states.length - 1].startTime).diff(moment(eventObject.states[0].startTime))};
    });

    const uniqueSubjects = listOfSubjectEvents.iterations.reduce((agg, { subject }) => Object.assign(agg, { [subject]: true }), {});

    return Object.assign({ }, listOfSubjectEvents, {
      statistics: {
        count: metrics.length,
        total: metrics.reduce((agg, {duration}) => agg + (duration * duration), 0),
        subjects: Object.keys(uniqueSubjects).length,
        events: listOfSubjectEvents.iterations.length
      }
    });
  });

  return cycleReportResponse;
};

const fetchCurrentCycleReport = async (request) => {
  let cycleLogParams = {};
  const oQueryParams = Object.assign({}, request.query);
  const oPathParams = Object.assign({}, request.params);

  Object.keys(oQueryParams).forEach((parameter) => {
    const value = oQueryParams[parameter];
    if (decodeURI(value) !== decodeURIComponent(value)) {
      oQueryParams[parameter] = decodeURIComponent(value);
    }
  });

  if (oQueryParams.facility) {
    cycleLogParams.facility = oQueryParams.facility;
  }
  if (['atl', 'eimco', 'eurofins'].includes(oPathParams.cust)) {
    cycleLogParams.qualifier = 'worker_badge';
    cycleLogParams.filters = [];
    cycleLogParams.from = moment(oQueryParams.time).format('YYYY-MM-DD HH:mm:ss').subtract(8, 'hours');
    cycleLogParams.to = moment(oQueryParams.time).format('YYYY-MM-DD HH:mm:ss');
    cycleLogParams.date = null;
    cycleLogParams.shift = null;
  } else {
    cycleLogParams.qualifier = 'license_plate';
    cycleLogParams.filters = [];
    if (isWhiteListCgr(request)) cycleLogParams.filters.push('whitelist');
    cycleLogParams.to = oQueryParams.time ? moment(oQueryParams.time).format('YYYY-MM-DD HH:mm:ss') : null;
    cycleLogParams.time = oQueryParams.time ? moment(oQueryParams.time).format('YYYY-MM-DD HH:mm:ss') : null;
    cycleLogParams.date = oQueryParams.time ? await shifts.findShiftsInfo(oPathParams.cust, cycleLogParams) : oQueryParams.date;
    cycleLogParams.shift = oQueryParams.shift;
    cycleLogParams.from = null;
  }

  cycleLogParams.tagNames = !request.user.cgrs.some(cgr => cgr === 'atl') ? ['customer_tag'] : ['customer_tag', 'qc_tag'];
  cycleLogParams.dataQueryName = isCgrGroundOps(oPathParams.cust) ? 'subjectCycleLog' : 'cycleLogQuery';
  cycleLogParams.zonesQueryName = isCgrGroundOps(oPathParams.cust) ? 'subjectZonesQuery' : 'cycleZonesQuery';
  cycleLogParams.previous = false;
  cycleLogParams.byClass = oQueryParams.byClass ? JSON.parse(oQueryParams.byClass) : false;

  const currentViewReport = await cycles.cycleLog(oPathParams.cust, cycleLogParams);

  return currentViewReport;
};

router.get('/statsNoConsul', isAtollogist, async (request, response) => {
  const theSelectedCustomer = request.query.cust;
  var cutoff = moment().endOf('minute').subtract(2, 'hours').toDate();

  if (!knownCGRs || moment().diff(cgrUpdateTime) > 5 * 60 * 1000) {
    knownCGRs = await consul.getKnownCGRs();
    cgrUpdateTime = moment();
  }

  const listOfCustomers = knownCGRs.filter(theCustomer => !theSelectedCustomer || theSelectedCustomer === theCustomer);
  const results = await Promise.allSettled(listOfCustomers.map(async (customer) => {
    const control = {};
    const timeout = new Promise((resolve, reject) => {
      control.timeout = setTimeout(() => {
        clearTimeout(control.timeout);
        const cause = new Error('StatsTimeout');
        reject(Object.freeze({
          system: customer,
          gateways: [],
          status: 'error: ' + cause
        }));
      }, 2* 60 * 1000);
    });

    const fetch = new Promise((resolve) => {
      let results;
      try {
        results = stats.statsForCustomerNoConsul(customer, cutoff);
      } catch (cause) {
        debug(`GET::statsNoConsul swallowing an error: ${cause}.`);
        results = Object.freeze({
          system: customer,
          gateways: [],
          status: 'error: ' + cause
        });
      } finally {
        resolve(results);
      }
    });

    return await Promise.race([timeout, fetch]).then((result) => {
      clearTimeout(control.timeout);
      return result;
    });
  })).then((values) => {
    return values.filter(result => result.status === 'fulfilled').map(result => result.value);
  });

  response.send(results);
});

router.get('/stats', isAtollogist, async (request, response) => {
  const theSelectedCustomer = request.query.cust;
  var cutoff = moment().endOf('minute').subtract(4, 'hours').toDate();

  if (!knownCGRs || moment().diff(cgrUpdateTime) > 5 * 60 * 1000) {
    knownCGRs = await consul.getKnownCGRs();
    cgrUpdateTime = moment();
  }

  const listOfCustomers = knownCGRs.filter(theCustomer => !theSelectedCustomer || theSelectedCustomer === theCustomer);
  const results = await Promise.allSettled(listOfCustomers.map(async (customer) => {
    const control = {};
    const timeout = new Promise((_, reject) => {
      control.timeout = setTimeout(() => {
        clearTimeout(control.timeout);
        const cause = new Error('StatsTimeout');
        reject(Object.freeze({
          system: customer,
          gateways: [],
          status: 'error: ' + cause
        }));
      }, 2 * 60 * 1000);
    });

    const fetch = new Promise((resolve) => {
      let results;
      try {
        results = stats.statsForCustomer(customer, cutoff);
      } catch (cause) {
        debug(`GET::statsNoConsul swallowing an error: ${cause}.`);
        results = Object.freeze({
          system: customer,
          gateways: [],
          status: 'error: ' + cause
        });
      } finally {
        resolve(results);
      }
    });

    return await Promise.race([timeout, fetch]).then((result) => {
      clearTimeout(control.timeout);
      return result;
    });
  })).then((values) => {
    return values.filter(result => result.status === 'fulfilled').map(result => result.value);
  });

  response.send(results);
});
router.get('/cgrs', isAtollogist, async (request, response) => {
  //cgrs are only available to ATL users
  if (!knownCGRs || moment().diff(cgrUpdateTime) > 5 * 60 * 1000) {
    knownCGRs = await consul.getKnownCGRs();
    cgrUpdateTime = moment();
  }

  response.send(knownCGRs);
});

router.get('/beaconList/:cust', isAuthenticatedUser, async (request, response) => {
  const customer = request.params.cust;
  var results = await beacons.activeBeaconList(customer);
  response.send(results);
});

router.get('/beacon', isAuthenticatedUser, async (request, response) => {
  const customer = request.query.customer;
  let queryParams = {};
  queryParams.date = null;
  queryParams.shift = null;
  queryParams.facility = request.query.facility;
  queryParams.namespace = request.query.namespace;
  queryParams.beaconId = request.query.id;
  queryParams.from = moment(request.query.time).subtract(12, 'hours').format('YYYY-MM-DD HH:mm:ss');
  queryParams.to = moment(request.query.time).format('YYYY-MM-DD HH:mm:ss');
  var results = await beacons.beaconObservations(customer, queryParams);
  response.send(results);
});

router.get('/winner/:cust', isAuthenticatedUser, async (request, response) => {
  const customer = request.params.cust;
  let from = moment(request.query.time).subtract(12, 'hours').format('YYYY-MM-DD HH:mm:ss');
  let to = moment(request.query.time).format('YYYY-MM-DD HH:mm:ss');
  let queryParams = {};
  queryParams.from = from;
  queryParams.to = to;
  queryParams.facility = request.params.facility;
  queryParams.date = null;
  queryParams.shift = null;
  queryParams.namespace = request.query.namespace;
  queryParams.beaconId = request.query.id;
  var results = await beacons.winners(customer, queryParams);
  response.send(results);
});

router.get('/lastImageSteps/:cust', isAuthenticatedUser, async (request, response) => {
  const customer = request.params.cust;
  var results;
  if (request.query.time) {
    var time = moment.unix(Number(request.query.time)).toDate();
    results = await imageSteps.imageStepsAtTime(customer, time);
  } else {
    results = await imageSteps.lastImageSteps(customer);
  }

  response.send(results);
});

router.get('/lastImageStepsNoConsul/:cust', isAuthenticatedUser, async (request, response) => {
  const customer = request.params.cust;

  var results;
  if (request.query.time) {
    var time = moment.unix(Number(request.query.time)).toDate();
    results = await imageSteps.imageStepsAtTimeNoConsul(customer, time);
  } else {
    results = await imageSteps.lastImageStepsNoConsul(customer);
  }

  response.send(results);
});

const buildImageStepHistoryAction = (imagaStepHistoryDelegate) => {
  return async (request, response) => {
    if (['cust', 'gatewayId', 'cameraId'].some(param => request.params[param] === undefined)) {
      response.status(404).send('not found');
      return;
    }

    const customer = request.params.cust;
    if (!user.authenticateFacility(request, response)) return;

    var from = request.query.from ? moment.unix(Number(request.query.from)).toDate() : moment().subtract('1', 'hour').toDate();
    var to = request.query.to ? moment.unix(Number(request.query.to)).toDate() : moment().toDate();

    var results = await imagaStepHistoryDelegate(customer, request.params.gatewayId, request.params.cameraId, from, to);
    response.send(results);
  };
};

router.get('/imageStepHistory/:cust/:gatewayId/:cameraId', isAuthenticatedUser, buildImageStepHistoryAction(imageSteps.imageStepHistory));

router.get('/imageStepHistoryNoConsul/:cust/:gatewayId/:cameraId', isAuthenticatedUser, buildImageStepHistoryAction(imageSteps.imageStepHistoryNoConsul));

let isWhiteListCgr = (request) => (
  request.user.cgrs.some(cgr => (cgr === 'atl'))) ? (request.query.facility !== 'Victorville') : true;

router.get('/cycle/:cust', isAuthenticatedUser, async (request, response) => {
  if (!user.authenticateFacility(request, response)) return;

  const cycleReportResponse = await fetchCycleTimeReport(request);

  response.cacheControl({ maxAge: 5 * 60, private: true });
  response.send(cycleReportResponse);
});

router.get('/cycleLog/:cust', isAuthenticatedUser, async (request, response) => {
  if (!user.authenticateFacility(request, response)) return;

  const [ currentViewReport, cycleReportResponse ] = await Promise.all([
    fetchCurrentCycleReport(request),
    fetchCycleTimeReport(request)
  ]);

  response.cacheControl({ maxAge: 5 * 60, private: true });
  response.send(Object.assign({}, currentViewReport, { cycleTimeReport: cycleReportResponse }));
});

router.get('/cycle_known_identifiers/:cust', isAuthenticatedUser, async (request, response) => {
  if (!user.authenticateFacility(request, response)) return;

  let visitLogParams = {};
  if (request.query.facility) {
    visitLogParams.facility = request.query.facility;
  }
  visitLogParams.qualifier = 'license_plate';
  visitLogParams.to = request.query.time ? moment(request.query.time).format('YYYY-MM-DD HH:mm:ss') : null;
  visitLogParams.time = request.query.time ? moment(request.query.time).format('YYYY-MM-DD HH:mm:ss') : null;
  visitLogParams.date = request.query.time ? await shifts.findShiftsInfo(request.params.cust, visitLogParams) : request.query.date;
  visitLogParams.from = null;
  visitLogParams.shift = request.query.shift;
  visitLogParams.filters = [];
  visitLogParams.skipFilter = true;
  if (isWhiteListCgr(request)) visitLogParams.filters.push('whitelist');
  visitLogParams.tagNames = !request.user.cgrs.some(cgr => cgr === 'atl') ? ['customer_tag'] : ['customer_tag', 'qc_tag'];
  var results = await cycles.knownIdentifiers(request.params.cust, visitLogParams);
  response.send(results);
});


router.get('/cycleList/:cust', isAuthenticatedUser, async (request, response) => {
  if (!user.authenticateFacility(request, response)) return;

  var results = await cycles.activeCycleList(request.params.cust);
  response.send(results);
});

router.get('/subjects/:cust', isAuthenticatedUser, async (request, response) => {
  let oSubjectParameters = {};
  const oQueryParams = Object.assign({}, request.query);
  oSubjectParameters.date = (oQueryParams.date && moment(oQueryParams.date, 'YYYY-MM-DD').isValid()) ? oQueryParams.date : null;
  oSubjectParameters.shift = oQueryParams.shift || 'day';
  oSubjectParameters.facility = oQueryParams.facility;
  oSubjectParameters.previous = oQueryParams.prev === 'true';
  oSubjectParameters.from = oQueryParams.time ? moment(oQueryParams.time).startOf('day') : null;
  oSubjectParameters.to = oQueryParams.time ? moment(oQueryParams.time).endOf('day') : null;
  var results = await subjects.fetchSubjects(request.params.cust, oSubjectParameters);
  if (results) {
    delete results.rawEvents;
    response.cacheControl({maxAge: results.timeSlotSizeMillis / 1000, private: true});
    response.send(results);
  } else {
    response.send({});
  }

});

router.get('/yardsubjects/:cust', async (request, response) => {
  if (!user.authenticateCgr(request, response)) return;

  let queryParams = {};
  queryParams.date = (request.query.date && moment(request.query.date, 'YYYY-MM-DD').isValid()) ? request.query.date : null;
  queryParams.shift = request.query.shift || 'day';
  queryParams.facility = request.query.facility;
  queryParams.previous = request.query.prev === 'true';
  queryParams.from = request.query.time ? moment(request.query.time).startOf('day') : null;
  queryParams.to = request.query.time ? moment(request.query.time) : null;
  var results = await YardSubjects.fetchYardSubjects(request.params.cust, queryParams);
  if (results) {
    delete results.rawEvents;
    response.cacheControl({maxAge: results.timeSlotSizeMillis / 1000, private: true});
    response.send(results);
  } else {
    response.send({});
  }

});

router.get('/safetySubjectDetail/:cust', async (request, response) => {
  if (!user.authenticateCgr(request, response)) return;

  let queryParams = {};
  queryParams.date = (request.query.date && moment(request.query.date, 'YYYY-MM-DD').isValid()) ? request.query.date : null;
  queryParams.shift = request.query.shift || 'day';
  queryParams.facility = request.query.facility;
  queryParams.previous = request.query.prev === 'true';
  queryParams.from = request.query.time ? moment(request.query.time).startOf('day') : null;
  queryParams.to = request.query.time ? moment(request.query.time) : null;
  queryParams.violation = true;
  let results = await safetySubjects.fetchSafetySubjects(request.params.cust, queryParams);
  if (results) {
    delete results.events;
    delete results.config;
    response.cacheControl({maxAge: results.timeSlotSizeMillis / 1000, private: true});
    response.send(results);
  } else {
    response.send({});
  }

});

router.get('/reportsList/:cust', async function (request, response) {
  const customer = request.params.cust;
  let facility = request.query.facility;
  let result = await reports.reportList(customer, facility);
  if (result) {
    //maxAge = 4 hours
    response.cacheControl({maxAge: 5 * 60, private: true});
    response.send(result);
  } else {
    response.status(404).send('not found');
  }
});

router.get('/getShifts/:cust', isAuthenticatedUser, async (request, response) => {
  let date = undefined;
  if (request.query.date && moment(request.query.date, 'YYYY-MM-DD').isValid()) {
    date = request.query.date;

    const results = await reports.getShifts(request.params.cust, date, request.query.facility);
    if (results) {
      //maxAge = 4 hours
      response.cacheControl({ maxAge: 5 * 60, private: true });
      response.send(results.map((shiftObject) => shiftObject.name));
    } else {
      response.status(404);
      response.send('not found');
    }
  }
});

router.get('/currentShiftInfo/:cust', isAuthenticatedUser, async (request, response) => {
  const customer = request.params.cust;
  const facility = request.query.facility;
  const date = request.query.date ? request.query.date : null;

  let result = await shifts.currentShiftsInfo(customer, { facility, date });

  if (result) {
    result = result.map((row) => ({
      'date': moment(row.date).format('YYYY-MM-DD'),
      'shift': row.shift_name,
      'startTime': row.shift_start,
      'endTime': row.shift_end,
      'inProgress': row.in_progress,
      'completed': row.completed,
      'timezone': row.shift_timezone_name
    }));

    response.cacheControl({ maxAge: 5 * 60, private: true });
    response.send(result);
  } else {
    response.status(404);
    response.send({ message: `no shifts found for customer ${customer}`});
  }
});

router.get('/getFacilityList', isAuthenticatedUser, async (request, response) => {
  const parameters = Object.assign({ customer: 'invalid' }, request.query);
  let result = await shifts.listFacilities(parameters.customer);

  response.cacheControl({ maxAge: 5 * 60, private: true });
  response.send(result.map((d) => d.facility));
});

router.get('/getPermittedFacilities', async (request, response) => {
  const permittedFacilities = JSON.stringify(request.user.permittedFacilities);
  response.cacheControl({ maxAge: 5 * 60, private: true });
  response.send(permittedFacilities);
});

router.get('/getSubjectList', isAuthenticatedUser, async (request, response) => {
  const listOfSubjects = await shifts.listSubjects(request.query.customer, request.query.facility);
  let subjectList = listOfSubjects.map(subject => subject.subject_name);
  response.send(subjectList);
});

router.get('/getLatestWOBinding/:cust', isAuthenticatedUser, async (request, response) => {
  let result = await cycles.latestBindingStatus(request.params.cust);
  response.send(result);
});

router.get('/gwImgs/', isAuthenticatedUser, async (request, response) => {
  const queryCust = request.query.customer;
  let result = await dashboard.gwImgs(queryCust);
  if (lastCGR !== queryCust || moment().diff(nodeNameUpdateTime) > 5 * 60 * 1000) {
    lastCGR = queryCust;
    knowNodeName = await consul.getNodeInfo(queryCust);
    nodeNameUpdateTime = moment();
  }
  result = result.map(row => {
    return {
      gatewayId: row.gateway_id,
      nodeName: row.nodename || (knowNodeName[row.gateway_id] && knowNodeName[row.gateway_id].nodeName),
      name: row.name,
      cameraId: row.camera_id,
      stepName: row.step_name,
      collectionTime: row.collection_time,
      collectionInterval: row.collection_interval,
      facility: row.facility,
      img_keys: row.img_keys,
      timezone: row.timezone
    };
  });
  response.send(result);
});

router.get('/deviceStats/', isAuthenticatedUser, async (request, response) => {
  var customer = request.query.customer;
  var cutoff = moment().endOf('minute').subtract(request.query.timeSpan, 'minute').toDate();
  if (!knownCGRs || moment().diff(cgrUpdateTime) > 5 * 60 * 1000) {
    knownCGRs = await consul.getKnownCGRs();
    cgrUpdateTime = moment();
  }
  let results = await stats.deviceStatsForCustomer(customer, cutoff);
  response.send(results.missing);
});

router.get('/gateStatus/:cust', isAuthenticatedUser, async (request, response) => {
  let queryParams = {};
  queryParams.date = (request.query.date && moment(request.query.date, 'YYYY-MM-DD').isValid()) ? request.query.date : null;
  queryParams.shift = request.query.shift || 'day';
  queryParams.facility = request.query.facility;
  queryParams.previous = request.query.prev === 'true';
  queryParams.from = null;
  queryParams.to = null;
  queryParams.smoothening = request.query.smoothening ? JSON.parse(request.query.smoothening) : true;
  var results = await subjects.gateStatus(request.params.cust, queryParams);

  if (results) {
    response.cacheControl({maxAge: results.timeSlotSizeMillis / 1000, private: true});
    response.send(results);
  } else {
    response.send({});
  }

});

router.get('/beaconZoneTracking', isAuthenticatedUser, async (request, response) => {
  if (!user.authenticateFacility(request, response)) {
    response.status(401).send({ message: 'Not allowed' });
    return;
  }

  let beaconParams = {};
  if (request.query.facility) {
    beaconParams.facility = request.query.facility;
  }

  //If specific time provided (time-travel) then use from and to
  beaconParams.from = request.query.time ? moment(request.query.time).startOf('day').format('YYYY-MM-DD HH:mm:ss') : null;
  beaconParams.to = request.query.time ? moment(request.query.time).format('YYYY-MM-DD HH:mm:ss') : null;

  //Else use date and shift (live-mode)
  beaconParams.date = request.query.date || null;
  beaconParams.shift = request.query.shift || null;

  const [currentViewReport, cycleReportResponse] = await Promise.all([
    beacons.beaconTracking(request.query.customer, beaconParams),
    fetchCycleTimeReport(request)
  ]);

  response.send(Object.assign({}, currentViewReport, { cycleTimeReport: cycleReportResponse }));
});

router.get('/beaconHistory', isAuthenticatedUser, async (request, response) => {
  if (!user.authenticateFacility(request, response)) {
    response.status(401).send({ message: 'Not allowed' });
    return;
  }

  let beaconHistory = {};
  if (request.query.facility) {
    beaconHistory.facility = request.query.facility;
  }

  //If specific time provided (time-travel) then use from and to
  beaconHistory.from = request.query.time ? moment(request.query.time).startOf('day').format('YYYY-MM-DD HH:mm:ss') : null;
  beaconHistory.to = request.query.time ? moment(request.query.time).format('YYYY-MM-DD HH:mm:ss') : null;

  //Else use date and shift (live-mode)
  beaconHistory.date = request.query.date || null;
  beaconHistory.shift = request.query.shift || null;

  //Fetch beacon history for selected beacons
  beaconHistory.beaconName = request.query.beaconName;
  var results = await beacons.beaconHistoryService(request.query.customer, beaconHistory);
  response.send(results);
});

router.get('/turn', isAuthenticatedUser, async (request, response) => {
  let queryParams = {};
  queryParams.date = null;
  queryParams.shift = null;
  queryParams.facility = request.query.facility;
  queryParams.previous = false;
  queryParams.from = request.query.start;
  queryParams.to = request.query.end;
  let results = await subjects.fetchTurn(request.query.customer, queryParams);

  if (results) {
    response.cacheControl({maxAge: results.timeSlotSizeMillis / 1000, private: true});
    response.send(results);
  } else {
    response.send({});
  }

});

router.get('/shiftInfo', isAuthenticatedUser, async (request, response) => {
  if (!user.authenticateFacility(request, response)) {
    response.status(401).send({ message: 'Not allowed' });
    return;
  }

  const shiftParams = {
    time: moment().format('YYYY-MM-DD HH:mm'),
    facility: 'Mills'
  };

  const result = await shifts.findShiftsInfo(request.query.customer, shiftParams);

  return result[0].date;
});

router.get('/getSOIConfigs', isAuthenticatedUser, async (request, response) => {
  // if (!user.authenticateFacility(request, response)) return;
  let configs = await soiConfig.listSOIConfigs(request.query.customer);
  response.send(configs);
});

function isCgrGroundOps(customer) {
  let groundOpsCgr = ['tia', 'cvgairport', 'gatwickairport'];
  return !!groundOpsCgr.some(x => x === customer);
}
