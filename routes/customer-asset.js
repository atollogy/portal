const router = require('express-promise-router')();
const tags = require('../db/tag.js');
const user = require('../lib/user-management/');
const moment = require('moment');

module.exports = router;

const onCreateAsset = async (requestObject, responseObject) => {
  if (!user.authenticateFacility(requestObject, responseObject)) return;

  let objParams = {};
  objParams.lp = requestObject.body.subject;
  objParams.tagName = !requestObject.user.cgrs.some(cgr => cgr == 'atl') ? 'customer_tag' : 'qc_tag';
  let newTags = requestObject.body.newTags;
  let oldTags = requestObject.body.oldTags;

  objParams.tagsAdded = newTags.filter((tag) => !oldTags.includes(tag));
  objParams.tagsRemoved = oldTags.filter(tag => !newTags.includes(tag));

  try {
    const customer = requestObject.body.customer;
    await tags.insertTag(customer, objParams);
    responseObject.send({ status: 'success' });
  } catch (errorObject) {
    responseObject.send(errorObject);
  }
};

const onAddAssetInfo = async (requestObject, responseObject) => {
  if (!user.authenticateFacility(requestObject, responseObject)) return;

  let objParams = {};
  objParams.licensePlate = requestObject.body.licensePlate;
  objParams.trailerId = requestObject.body.trailerId;
  objParams.carrierName = requestObject.body.carrierName;

  try {
    const customer = requestObject.body.customer;
    await tags.insertAsset(customer, objParams);
    responseObject.send({ status: 'success' });
  } catch (errorObject) {
    responseObject.send(errorObject);
  }
};

router.post('/addAsset', async (req, res) => {
  if (!user.authenticateCgr(req, res)) return;
  onCreateAsset(req, res);
});

router.post('/addAssetInfo', async (req, res) => {
  if (!user.authenticateCgr(req, res)) return;
  onAddAssetInfo(req, res);
});

//add asset from react-portal (without authenticateCgr)
router.post('/addAssetTag', async (req, res) => await onCreateAsset(req, res));

router.get('/getCustomerTags', async (req, res) => {
  const parameters = {
    tagNames: ['customer_tag']
  };

  if (req.user.cgrs.some(cgr => cgr == 'atl')) {
    parameters.tagNames.push('qc_tag');
  }
  const customer = req.params.cust || req.query.customer;
  // if (!user.authenticateCgr(req, res)) return;
  let result = await tags.listAllTags(customer, parameters);
  result = result.map((record) => record.attribute_value);
  res.send(result);
});

router.get('/getAssetId', async (req, res) => {
  const parameters = {};

  const customer = req.params.cust || req.query.customer;
  parameters.carrierName = req.query.carrierName;
  parameters.trailerId = req.query.trailerId;
  // if (!user.authenticateCgr(req, res)) return;
  let result = await tags.getAssetId(customer, parameters);
  result = result.map((record) => record.asset_id
  );
  res.send(result);
});


router.get('/getAllLPNAssets', async (req, res) => {
  const parameters = {};

  const customer = req.params.cust || req.query.customer;
  // if (!user.authenticateCgr(req, res)) return;
  parameters.customer = customer;
  let result = await tags.getAllLPNAssets(customer, parameters);
  res.send(result);
});

router.get('/allAssets',async (req,res) => {
  let objParams = {};
  objParams.tagNames = !req.user.cgrs.some(cgr => cgr == 'atl') ? ['customer_tag'] : ['customer_tag', 'qc_tag'];
  const customer = req.params.cust || req.query.customer;
  let result = await tags.listAllTagAssets(customer, objParams);
  res.send(result);
});

router.post('/filterAssets',async (req,res) => {
  let objParams = {};
  objParams.tagNames = !req.user.cgrs.some(cgr => cgr == 'atl') ? ['customer_tag'] : ['customer_tag', 'qc_tag'];
  objParams.tags = req.body.tags ? req.body.tags : null;
  objParams.assetId = req.body.assetId ? req.body.assetId : '';
  objParams.camIds = req.body.camIds && req.body.camIds.length > 0 ? req.body.camIds : null;
  objParams.pageSize = req.query.pageSize ? req.query.pageSize : undefined;
  objParams.offset = req.query.offset ? req.query.offset : undefined;
  objParams.from = (req.body.from == undefined) ? null : moment.unix(Number(req.body.from)).utc().toDate();
  objParams.to = (req.body.to == undefined) ? null : moment.unix(Number(req.body.to)).utc().toDate();
  objParams.sortDate = req.body.sortDate ? req.body.sortDate : 'desc';
  objParams.includeNotag = req.body.includeNotag ? req.body.includeNotag : false;
  const customer = req.params.cust || req.query.customer;
  const result = await tags.filterTagAssets(customer, objParams);
  res.send(result);
});
router.post('/filterAssetCount',async (req,res) => {
  let objParams = {};
  objParams.tagNames = !req.user.cgrs.some(cgr => cgr == 'atl') ? ['customer_tag'] : ['customer_tag', 'qc_tag'];
  objParams.tags = req.body.tags && req.body.tags.length > 0 ? req.body.tags : '{}';
  objParams.assetId = req.body.assetId ? req.body.assetId : '';
  objParams.camIds = req.body.camIds && req.body.camIds.length > 0 ? req.body.camIds : null;
  const customer = req.params.cust || req.query.customer;
  let result = await tags.filterTagAssetCount(customer, objParams);
  res.send(result);
});

const onUpdateAssetTag = async (requestObject, responseObject) => {
  if (!user.authenticateFacility(requestObject, responseObject)) return;

  let objParams = {};
  objParams.assetIds = requestObject.body.assetIds;
  objParams.tagName = !requestObject.user.cgrs.some(cgr => cgr == 'atl') ? 'customer_tag' : 'qc_tag';
  objParams.tags = requestObject.body.tags;

  try {
    const customer = requestObject.body.customer;
    await tags.updateTags(customer, objParams);
    responseObject.send({ status: 'success' });
  } catch (errorObject) {
    responseObject.send(errorObject);
  }
};

router.post('/updateAssetTag', async (req, res) => await onUpdateAssetTag(req, res));