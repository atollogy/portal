const annot = require('../db/imageStepAnnotation.js');
const router = require('express-promise-router')();
const moment = require('moment');
const consul = require('../consul.js');
module.exports = router;
const user = require('../lib/user-management/');
const debug = require('debug')('portal:imageStepAnnotation');

router.post('/setAnnotation', async (req, res) => {
  const customer = req.body.customer;
  req.body.user_id = req.user.id;
  req.body.collection_time = moment.unix(Number(req.body.collection_time)).utc().toDate();
  //if (!user.authenticateCgr(req, res)) return;
  await annot.setAnnotation(customer, req.body);
  res.send({
    'status' : 200
  });

});

router.get('/getAnnotation/:cust', async (req, res) => {
  let customer = req.params.cust;
  req.query.collection_time = moment.unix(Number(req.query.collection_time)).utc().toDate();
  //if (!user.authenticateCgr(req, res)) return;
  let result = await annot.getAnnotation(customer, req.query);

  let resJSON = result.map((theResponseObject) => {
    let json = {
      'isAccurate': theResponseObject.is_accurate,
      'comment':theResponseObject.comment,
      'createdAtTime': theResponseObject.created_at_date,
      'userName': theResponseObject.user_name
    };
    return json;
  });

  res.send(resJSON);
});

var knownCGRs;
var cgrUpdateTime;

router.get('/annotationStats', async (req, res) => {
  if (!user.authenticateCgr(req, res)) return;

  if(!knownCGRs || moment().diff(cgrUpdateTime) > 10*60*1000){
    knownCGRs = await consul.getKnownCGRs();
    cgrUpdateTime = moment();
  }

  var result = await Promise.all(knownCGRs.map(async (customer) => {
    try {
      let statsRow = await annot.getAnnotationStats(customer);
      let statCustObj = {};
      statCustObj.customer = customer;
      let custStats = statsRow.map(row => {
        let obj = {};
        obj.stepName = row.step_name;
        obj.gatewayName = row.gateway_name;
        obj.accuracy = Number(row.accurate_percentage).toFixed(2);
        obj.reportDate = row.last_report;
        obj.accurateCount = row.accurate;
        obj.totalAnnotation = row.total;
        return obj;
      });
      statCustObj.stats = custStats;

      return statCustObj;
    } catch (e){
      debug(`GET::annotationStats swallowing an error: ${e}.`);
      return {
        system: customer,
        gateways: [],
        status: 'error: ' + e
      };
    }
  }));
  res.send(result);
});

router.post('/restateWeight', async (req, res) => {
  const customer = req.body.customer;
  req.body.collection_time = moment.unix(Number(req.body.collection_time)).utc().toDate();
  //if (!user.authenticateCgr(req, res)) return;
  let result = await annot.restateScaleWeight(customer, req.body);
  if(result == 0){
    res.send({flashMessage:`Scale Weight update successfully applied. See report change in 1 min.`});
  } else {
    res.send({flashMessage:`Failed to apply update: ${result}`});
  }
});
