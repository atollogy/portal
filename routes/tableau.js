const router = require('express-promise-router')();
const debug = require('debug')('portal:routes:qc');
const user = require('../lib/user-management/');
const tableau = require('../lib/tableau-management/');
const {getShortHash: version, getCurrentYear} = require('../lib/health');
module.exports = router;

router.get('/url', async (req, res) => {
  debug(req.user.cgrs);
  if (!user.authenticateCgr(req, res)) return;
  try {
    const navigation = await user.fetchNav();
    const cgrNavs = navigation['cgrNavs'];
    //Find config for customer/facility
    let customer = req.query.customer;
    let facility = req.query.facility;
    let username = req.user.username;
    let signInRes = JSON.parse(await tableau.getTableauTokenForRestAPI());
    if (!signInRes || !signInRes['credentials'] || !signInRes['credentials']['token'] || !signInRes['credentials']['site']['id']) {
      res.send({errorCode: 'PEC.0005'});
    }
    const token = signInRes['credentials']['token'];
    const siteId = signInRes['credentials']['site']['id'];
    const userAuth = await authenticateUser(username, token, siteId);
    if (userAuth.errorCode !== -1) {
      res.send(userAuth);
      return;
    }
    const userAuthorization = await authorizeUser(username, customer, facility, siteId, token);
    if (userAuthorization['errorCode'] !== -1) {
      res.send(userAuthorization);
      return;
    }
    if(!req.user.tableau || !req.user.tableau.accessUrl.length) {
      console.log('errorCode: PEC.0003');
      res.send({errorCode: 'PEC.0003'});
      return;
    }
    if (cgrNavs[customer]) {
      //If facility specific config does not exist just use common cfg across all facilities else use the one for facility in question
      let ticket = await tableau.getTrustedAuth(username);
      let reportUrl = req.user.tableau.accessUrl.filter(x=>x.dashboardName===req.query.dashboardName);
      if(reportUrl.length) {
        let displayUrl = `/portal/tableau/trusted/${ticket}${reportUrl[0].url}`;
        res.send({errorCode: -1, url: displayUrl});
      } else {
        res.send({errorCode: 'PEC.0003', url: ''});
      }
    }
  } catch (e) {
    console.log(e);
    res.send({url: ''});
  }
});

async function authorizeUser(username, customer, facility, siteId, token) {
  let userGroups = await getRequiredGroups(customer, facility, siteId, token);
  if (userGroups['errorCode'] !== -1) {
    return JSON.stringify(userGroups);
  }
  let result = JSON.parse(await tableau.getUsersInAGroup(token, siteId, userGroups['group']['id']));
  let users = result['users'];
  let authorizedUser = users.user.filter(x => x.name === username);
  if (authorizedUser.length > 0) {
    return {errorCode: -1, user: authorizedUser[0]};
  } else {
    return {errorCode: 'PEC.0002', user: null};
  }
}

async function getRequiredGroups(customer, facility, siteId, token) {
  let result = JSON.parse(await tableau.getAllUserGroups(token, siteId));
  let groups = result['groups'] || [];
  let facilityStr = getFacilityWithNocapsAndSpace(facility);
  let group = groups.group.filter(x => x.name === `${customer}:${facilityStr}`);
  if (group.length > 0) {
    return {errorCode: -1, group: group[0]};
  } else {
    return {errorCode: 'PEC.0004', group: null};
  }
}

function getFacilityWithNocapsAndSpace(facility) {
  return facility.toLowerCase().replace(/\s/g, '');
}

async function authenticateUser(username, token, siteId) {
  let result = JSON.parse(await tableau.getAllTableauUsers(token, siteId));
  let users = result['users'];
  let authorizedUser = users.user.filter(x => x.name === username);
  if (authorizedUser.length > 0) {
    return ({errorCode: -1, user: authorizedUser[0]});
  } else {
    return ({errorCode: 'PEC.0001', user: null});
  }
}

router.get('/groups', async (req, res) => {
  if (req.user.cgrs[0] !== 'atl') {
    res.status(404).send('not found');
  }
  let signInRes = JSON.parse(await tableau.getTableauTokenForRestAPI());
  const token = signInRes['credentials']['token'];
  const siteId = signInRes['credentials']['site']['id'];
  let result = JSON.parse(await tableau.getAllUserGroups(token, siteId));
  let respObj = {
    user: req.user,
    version: await version(),
    year: getCurrentYear(),
    naturalCompare: require('string-natural-compare')
  };

  if(result.error) {
    respObj = {...respObj, res: []};
  } else {
    respObj = {...respObj, res: result.groups.group};
  }
  res.send(respObj);
});

router.get('/getAllDashboards', async (req, res) => {
  if (!user.authenticateCgr(req, res)) return;
  if(req.user.tableau) {
    let dashboards = req.user.tableau.accessUrl;
    let respObj = dashboards.map(x=>x.dashboardName);
    res.send({errorCode: -1, dashboards: respObj});
  } else {
    res.send({errorCode: 'PEC.0003', dashboards: []});
  }
});
