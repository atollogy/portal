const router = require('express-promise-router')();
const filter = require('../db/filter');
module.exports = router;

const isAtollogist = (request, response, next) => {
  if (!request.user.cgrs.some(cgr => cgr === 'atl')) {
    response.status(403).send('not found');
    return;
  }
  next();
};

router.get('/active', isAtollogist, async (request, response) => {
  const pathParameters = Object.assign({}, request.params);
  const queryParameters = Object.assign({}, request.query);

  const contextParameters = Object.assign({}, queryParameters, pathParameters);
  let filters = await filter.fetchAllFilters(contextParameters);
  response.send(filters);
});

router.post('/add', isAtollogist, async (request, response) => {
  let params = request.body;
  let result = await filter.updateFilters(params);
  response.send(result);
});
