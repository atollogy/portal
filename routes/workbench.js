const wb = require('../db/workbench.js');
const moment = require('moment');
const uuidv1 = require('uuid/v1');
const consul = require('../consul.js');
const { mappingError } = require('../lib/utils/error');

const express = require('express');
const user = require('../lib/user-management/');
const debug = require('debug')('portal:routes:workbench');

const workbench = express();

workbench.on('mount', () => {
  debug(`Workbench Mounted as ${workbench.mountpath}`);

  const whitelist = [
    'at0l.io',
    'atollogy.com'
  ].reduce((listOfUrls, domain) => {
    const hostnames = ['dev', 'stg', 'prd', ''].map(
      (environment) => `https://admin.${environment}.${domain}`
    );

    return listOfUrls.concat(hostnames);
  },[]);
  workbench.locals.whitelist = whitelist;

  workbench.use((request, response, next) => {
    const originUrl = request.header('Origin');
    if (whitelist.some((candidate) => originUrl.indexOf(candidate) >= 0)) {
      response.setHeader('Access-Control-Allow-Origin', originUrl);
    }
    next();
  });

  workbench.get('/getStepDetail/:cust/', async (req, res) => {
    const customer = req.params.cust;
    if (!user.authenticateCgr(req, res)) return;

    let result = await wb.stepInfo(customer);
    let resJSON = result.map((theResponseObject) => {
      let json = {
        'gatewayId': theResponseObject.gateway_id,
        'gatewayName':theResponseObject.gateway_name,
        'camId': theResponseObject.camera_id,
        'stepName': theResponseObject.step_name,
        'fnVersion': theResponseObject.step_function_version,
        'cfgVersion':theResponseObject.step_config_version
      };
      return json;

    });
    res.send(resJSON);
  });

  workbench.get('/getImageKeys/:cust', async (req, res) => {
    let customer = req.params.cust;
    // if (!user.authenticateCgr(req, res)) return;

    let parameters = {};
    parameters.step = (req.query.step == undefined) ? null : req.query.step.split(',');
    parameters.gw = (req.query.gatewayId == undefined) ? null : req.query.gatewayId.split(',');
    parameters.camId = (req.query.camId == undefined) ? null : req.query.camId;
    parameters.fnVersion = (req.query.fnVersion == undefined) ? null : req.query.fnVersion;
    parameters.cfgVersion = (req.query.cfgVersion == undefined) ? null : req.query.cfgVersion;
    parameters.resultType = (req.query.resultType == undefined) ? null : req.query.resultType;
    parameters.stepFunction = (req.query.stepFunction == undefined) ? null : req.query.stepFunction.split(',');
    parameters.imageStepId = (req.query.imageStepId == undefined) ? null : req.query.imageStepId;
    parameters.session_id = uuidv1();
    parameters.from = (req.query.from == undefined) ? null : moment.unix(Number(req.query.from)).utc().toDate();
    parameters.to = (req.query.to == undefined) ? null : moment.unix(Number(req.query.to)).utc().toDate();
    parameters.pageSize = (req.query.pageSize == undefined) ? null : req.query.pageSize;
    parameters.offset = (req.query.offset == undefined) ? null : req.query.offset;
    try {
      var results = await wb.stepKeys(customer, parameters);
      let resJSON = {
        'result':results,
        'sessionId':parameters.session_id
      };
      res.send(resJSON);
    } catch (e) {
      const errorData = await mappingError(e, 'workbench-getImageKeys');
      res.send(errorData);
    }
  });

  workbench.post('/imageStepAnnotation', async (req, res) => {
    try {
      const customer = req.body.customer;
      if (!user.authenticateCgr(req, res)) return;

      let parameters = req.body;
      parameters.userId = req.user.id;
      await wb.setAnnotation(customer, parameters);
      res.send({
        'status' : 200
      });
    } catch (e) {
      const errorData = await mappingError(e, 'workbench-imageStepAnnotation');
      res.send(errorData);
    }
  });

  workbench.get('/imageStepAnnotation/:cust', async (req, res) => {
    try {
      let customer = req.params.cust;
      if (!user.authenticateCgr(req, res)) return;

      let parameters = req.query;
      parameters.collectionTime = moment.unix(Number(req.query.collectionTime)).utc().toDate();
      let result = await wb.getAnnotations(customer, parameters);
      let resJSON = result.map((theResponseObject) => {
        let json = {
          'qc_result': theResponseObject.qc_result,
          'comment':theResponseObject.comment,
          'createdAtTime': theResponseObject.created_at_date,
          'userName': theResponseObject.user_name
        };
        return json;

      });
      res.send(resJSON);
    } catch (e) {
      const errorData = await mappingError(e, 'workbench-imageStepAnnotation-detail');
      res.send(errorData);
    }
  });

  workbench.get('/getImageAnnonationsCount/:cust', async (req, res) => {
    try {
      let customer = req.params.cust;
      if (!user.authenticateCgr(req, res)) return;
      let parameters = {};
      parameters.step = (req.query.step == undefined) ? null : req.query.step.split(',');
      parameters.gw = (req.query.gatewayId == undefined) ? null : req.query.gatewayId.split(',');
      parameters.camId = (req.query.camId == undefined) ? null : req.query.camId;
      parameters.fnVersion = (req.query.fnVersion == undefined) ? null : req.query.fnVersion;
      parameters.cfgVersion = (req.query.cfgVersion == undefined) ? null : req.query.cfgVersion;
      parameters.resultType = (req.query.resultType == undefined) ? null : req.query.resultType;
      parameters.stepFunction = (req.query.stepFunction == undefined) ? null : req.query.stepFunction.split(',');
      parameters.imageStepId = (req.query.imageStepId == undefined) ? null : req.query.imageStepId;
      parameters.session_id = uuidv1();
      parameters.from = (req.query.from == undefined) ? null : moment.unix(Number(req.query.from)).utc().toDate();
      parameters.to = (req.query.to == undefined) ? null : moment.unix(Number(req.query.to)).utc().toDate();
      parameters.pageSize = (req.query.pageSize == undefined) ? null : req.query.pageSize;
      parameters.offset = (req.query.offset == undefined) ? null : req.query.offset;
      const results = await wb.getImageAnnonationsCount(customer, parameters);
      res.send({
        'status' : 200,
        'results': +results
      });
    } catch (e) {
      const errorData = await mappingError(e, 'workbench-getImageAnnonationsCount');
      res.send(errorData);
    }
  });

  workbench.get('/getImageAnnonations/:cust', async (req, res) => {
    try {
      let customer = req.params.cust;
      if (!user.authenticateCgr(req, res)) return;
      let parameters = {};
      parameters.step = (req.query.step == undefined) ? null : req.query.step.split(',');
      parameters.gw = (req.query.gatewayId == undefined) ? null : req.query.gatewayId.split(',');
      parameters.camId = (req.query.camId == undefined) ? null : req.query.camId;
      parameters.fnVersion = (req.query.fnVersion == undefined) ? null : req.query.fnVersion;
      parameters.cfgVersion = (req.query.cfgVersion == undefined) ? null : req.query.cfgVersion;
      parameters.resultType = (req.query.resultType == undefined) ? null : req.query.resultType;
      parameters.stepFunction = (req.query.stepFunction == undefined) ? null : req.query.stepFunction.split(',');
      parameters.imageStepId = (req.query.imageStepId == undefined) ? null : req.query.imageStepId;
      parameters.session_id = uuidv1();
      parameters.from = (req.query.from == undefined) ? null : moment.unix(Number(req.query.from)).utc().toDate();
      parameters.to = (req.query.to == undefined) ? null : moment.unix(Number(req.query.to)).utc().toDate();
      parameters.pageSize = (req.query.pageSize == undefined) ? null : req.query.pageSize;
      parameters.offset = (req.query.offset == undefined) ? null : req.query.offset;
      const results = await wb.getImageAnnonations(customer, parameters);
      res.send({
        'status' : 200,
        'results': results
      });
    } catch (e) {
      const errorData = await mappingError(e, 'workbench-getImageAnnonations');
      res.send(errorData);
    }
  });

  workbench.post('/getImageStepOutput/:cust', async (req, res) => {
    try {
      let customer = req.params.cust;
      if (!user.authenticateCgr(req, res)) return;

      let parameters = req.query;
      parameters.collectionTime = moment.unix(Number(req.query.collectionTime)).utc().toDate();
      parameters.imageStepIdList = req.body.image_step_id_list;
      parameters.gw = (req.query.gatewayId == undefined) ? null : req.query.gatewayId.split(',');

      let result = await wb.getAnnotations(customer, parameters);
      const stepOutput = await wb.geStepOutPut(customer, parameters);
      const dataRawImage = await wb.getDataRawImage(customer, parameters);
      let resJSON = result.map((theResponseObject) => {
        let json = {
          'qc_result': theResponseObject.qc_result,
          'comment':theResponseObject.comment,
          'createdAtTime': theResponseObject.created_at_date,
          'userName': theResponseObject.user_name
        };
        return json;

      });

      let image_step_output_map = {};

      stepOutput.forEach(e => {
        image_step_output_map[e.image_step_id] = e.step_output;
      });

      res.send({
        image_step_output_map,
        qcResults: resJSON,
        dataRawImage: dataRawImage
      });
    } catch (e) {
      const errorData = await mappingError(e, 'workbench-getImageStepOutput');
      res.send(errorData);
    }
  });



  let knownCGRs = undefined;
  let cgrUpdateTime = undefined;

  workbench.get('/imageStepAnnotationStats', async (req, res) => {
    // /let customer = req.query.customer;
    if (!user.authenticateCgr(req, res)) return;

    if(!knownCGRs || moment().diff(cgrUpdateTime) > 10*60*1000){
      knownCGRs = await consul.getKnownCGRs();
      cgrUpdateTime = moment();
    }

    var result = await Promise.all(knownCGRs.map(async (customer) => {
      try {
        let statsRow = await wb.getAnnotationStats(customer);
        let statCustObj = {};
        statCustObj.customer = customer;
        let custStats = statsRow.map(row => {
          let obj = {};
          obj.stepName = row.step_name;
          obj.gatewayName = row.gateway_name;
          obj.accuracy = Number(row.accurate_percentage).toFixed(2);
          obj.reportDate = row.last_report;
          obj.accurateCount = row.accurate;
          obj.totalAnnotation = row.total;
          return obj;
        });
        statCustObj.stats = custStats;

        return statCustObj;
      } catch (e){
        debug(`GET::imageStepAnnotationStats swallowing an error: ${e}.`);
        return {
          system: customer,
          gateways: [],
          status: 'error: ' + e
        };
      }
    }));
    res.send(result);
  });

  workbench.get('/getGatewayId/:cust', async (req, res) => {
    try {
      let customer = req.params.cust;
      if (!user.authenticateCgr(req, res)) return;
      const results = await wb.getGetewayId(customer);
      const response = results.map(item => ({
        gateway_id: item.gateway_id,
        cam_name: item.s3_url.substring(5, item.s3_url.length).split('/')[2]
      }));
      res.send(response);
    } catch(e) {
      const errorData = await mappingError(e, 'workbench-getGatewayId');
      res.send(errorData);
    }
  });

  workbench.get('/getImageKeysObjectDetection/:cust', async (req, res) => {
    let customer = req.params.cust;
    let parameters = {};
    parameters.step = (req.query.step == undefined) ? null : req.query.step.split(',');
    parameters.gw = (req.query.gatewayId == undefined) ? null : req.query.gatewayId.split(',');
    parameters.camId = (req.query.camId == undefined) ? null : req.query.camId;
    parameters.resultType = (req.query.resultType == undefined) ? null : req.query.resultType.split(',');
    parameters.stepFunction = (req.query.stepFunction == undefined) ? null : req.query.stepFunction.split(',');
    parameters.imageStepId = (req.query.imageStepId == undefined) ? null : req.query.imageStepId;
    parameters.session_id = uuidv1();
    parameters.from = (req.query.from == undefined) ? null : moment.unix(Number(req.query.from)).utc().toDate();
    parameters.to = (req.query.to == undefined) ? null : moment.unix(Number(req.query.to)).utc().toDate();
    parameters.pageSize = (req.query.pageSize == undefined) ? null : req.query.pageSize;
    parameters.offset = (req.query.offset == undefined) ? null : req.query.offset;
    try {
      var results = await wb.stepKeysObjectDetection(customer, parameters);
      let resJSON = {
        'result':results,
        'sessionId':parameters.session_id
      };
      res.send(resJSON);
    } catch (e) {
      const errorData = await mappingError(e, 'workbench-getImageKeysObjectDetection');
      res.send(errorData);
    }
  });

  workbench.get('/getCameraStatus/:cust', async (req, res) => {
    let customer = req.params.cust;
    let parameters = {};
    parameters.gw = (req.query.gatewayId == undefined) ? null : req.query.gatewayId;
    try {
      var result = await wb.getCameraStatus(customer, parameters);
      res.send(result);
    } catch (e) {
      const errorData = await mappingError(e, 'workbench-getImageKeysObjectDetection');
      res.send(errorData);
    }
  });

  workbench.get('/getCameraImages/:cust', async (req, res) => {
    let customer = req.params.cust;
    let parameters = {};
    parameters.gw = (req.query.gatewayId == undefined) ? null : req.query.gatewayId;
    parameters.from = (req.query.from == undefined) ? null : moment.unix(Number(req.query.from)).utc().toDate();
    parameters.to = (req.query.to == undefined) ? null : moment.unix(Number(req.query.to)).utc().toDate();
    parameters.pageSize = (req.query.pageSize == undefined) ? null : req.query.pageSize;
    parameters.offset = (req.query.offset == undefined) ? null : req.query.offset;
    try {
      var result = await wb.getCameraImages(customer, parameters);
      const response = result.map(image => ({
        ...image, customer
      }));
      res.send(response);
    } catch (e) {
      const errorData = await mappingError(e, 'workbench-getCameraImages');
      res.send(errorData);
    }
  });

  workbench.get('/getDateList/:cust', async (req, res) => {
    let customer = req.params.cust;
    try {
      let parameters = {};
      parameters.gw = (req.query.gatewayId == undefined) ? null : req.query.gatewayId;
      var result = await wb.getDateList(customer, parameters);
      res.send(result);
    } catch (e) {
      const errorData = await mappingError(e, 'workbench-getDateList');
      res.send(errorData);
    }
  });

});

module.exports = workbench;
