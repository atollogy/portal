const imageStep = require('../db/imageStep.js');
const router = require('express-promise-router')();
module.exports = router;

router.post('/blacklistContext', async (req, res) => {
  const customer = req.body.customer;
  let parameters = {
    collection_time: Number(req.body.collection_time),
    step_name: req.body.step_name,
    camera_id: req.body.camera_id,
    gateway_id: req.body.gateway_id
  };
  //if (!user.authenticateCgr(req, res)) return;
  const result = await imageStep.getStepOutput(customer, parameters);
  if(result && result.length > 0 && result[0].step_output) {
    parameters.step_output = Object.assign({}, result[0].step_output);
    parameters.step_output.blacklistedContext = true;
    parameters.step_output = JSON.stringify(parameters.step_output);
  }
  const rowCount = await imageStep.blacklistContext(customer, parameters);
  if (rowCount > 0) {
    res.send({
      'status' : 200
    });
  } else {
    res.send({
      'status' : 400
    });
  }
});
