const router = require('express-promise-router')();
const moment = require('moment');
const imageStep = require('../db/imageStep.js');
const s3 = require('../s3.js');
const user = require('../lib/user-management/');
const debug = require('debug')('portal:image');

// export our router to be mounted by the parent application
module.exports = router;
const reDecimalPort = /(\.\d{1,})/;
const SUPPORTED_WIDTH_LIST = [200, 400];

const LOGO_BUCKET = 'cgr-logos';

router.get('/:cust/:gw/:cam/:time/:step/:index', async (req, res) => {
  //check the parameters
  if (['cust', 'gw', 'cam', 'time', 'step', 'index'].some(param => req.params[param] === undefined)) {
    res.status(404).send('not found');
    return;
  }
  const customer = req.params.cust;
  let use_bypass = null;
  const bypass = req.headers.bypass || req.query.bypass;

  if (bypass === '85eklvbsdflgfvldkgnfgrf3wifewkvb') {
    use_bypass = true;
  } else if (!user.authenticateCgr(req, res)) {
    return;
  }

  let width = null;
  let width_list_idx = -1;
  if (req.query.width) {
    width = parseInt(req.query.width);
    if (isNaN(width)) {
      res.status(400).send('width parameter is not a number');
      return;
    }
    width_list_idx = SUPPORTED_WIDTH_LIST.indexOf(width);
    if (width_list_idx < 0) {
      res.status(400).send(
        'width parameter does not refer to one of supported values: ' + SUPPORTED_WIDTH_LIST);
      return;
    }
  }

  let params = Object.assign({}, req.params);

  params.time = moment.unix(Number(req.params.time));
  if (reDecimalPort.test(req.params.time)) {
    const theDecimalValue = reDecimalPort.exec(req.params.time).find((_, index) => index == 1);
    const theUnixTimestamp = moment.unix(Number(req.params.time));
    const theFixePart = theUnixTimestamp.format('YYYY-MM-DD HH:mm:ss');
    const theOffsetPart = theUnixTimestamp.format('Z');
    params.time = `${theFixePart}${theDecimalValue} ${theOffsetPart}`;
  } else {
    params.time = moment.unix(Number(req.params.time)).toDate();
  }

  let locationInfo = await imageStep.imageLocation(customer, params);

  if (!locationInfo) {
    res.status(404).send('not found');
    return;
  }
  let link = null;
  let expires = req.query.expires;

  if (width) {
    let parts = locationInfo.substring('s3://'.length).split('/');
    let bucket = parts.shift();
    let key = parts.join('/');
    const {
      Bucket: resizedBucket,
      Prefix: resizedKeyPrefix
    } = getResizedS3LocInfo(bucket);

    for (let index = width_list_idx; index < SUPPORTED_WIDTH_LIST.length; index++) {
      let curWidth = SUPPORTED_WIDTH_LIST[index];
      let newResizedKey = resizedKeyPrefix + 'img_' + curWidth + '/' + key;
      link = await s3.signedLinkForExistentBucketKey(resizedBucket, newResizedKey, expires);
      if (link !== null) {
        break;
      } else {
        debug('Cannot find resized image: bucket=' + resizedBucket + ' key=' + newResizedKey);
      }
    }
    if (link == null) {
      // fall back to original image
      link = await s3.signedLinkForExistentBucketKey(bucket, key, expires);
      debug('Fall back to original image: bucket=' + bucket + ' key=' + key);
    }

  } else {
    link = await s3.signedLink(locationInfo, expires);
    // TODO to switch over to signedLinkForExistentObj, as coded below (?)
    // link = await s3.signedLinkForExistentObj(locationInfo, expires);
  }

  if (use_bypass) {
    //send actual image data
    let data = await s3.directReturnBucketObject(locationInfo);
    if (!data) {
      res.status(404).send('not found');
    } else {
      req.query.expires = 31536000;
      res.set('Cache-control', 'public, max-age=31536000');
      res.writeHead(200,{'Content-type':'image/jpg'});
      res.end(data, 'binary');
    }
  } else if (link != null) {
    //send a redirect to a signed link
    const period = 60 * 60;
    res.set('Cache-control', `public, max-age=${period}`);
    res.redirect(link);
  } else {
    res.status(404).send('not found');
  }
});

router.get('/logo/:cust', async (req, res) => {

  //check that the user has access to the customer
  if (!user.authenticateCgr(req, res)) return;

  let params = Object.assign({}, req.params);

  const imageName = `${params.cust}.png`;
  let link = null;
  let expires = 60 * 60;

  let bucketLocation = `S3://${LOGO_BUCKET}/${imageName}`;
  link = await s3.signedLink(bucketLocation, expires);
  if (link !== null) {
    const period = 60 * 60;
    res.set('Cache-control', `public, max-age=${period}`);
    res.redirect(link);
  } else {
    debug('Cannot find image: at Bucket Location=' + bucketLocation);
    res.status(404).send('not found');
  }
});

const DATA_SUFFIX = '-data';
const RAWDATA_SUFFIX = '-rawdata';
const RESIZED_SUFFIX = '-resized';
function getResizedS3LocInfo(bucket) {
  let theBucket;

  if (bucket.endsWith(RAWDATA_SUFFIX)) {
    theBucket = bucket.substring(0, bucket.length - RAWDATA_SUFFIX.length) + RESIZED_SUFFIX;
    return Object.freeze({Bucket: theBucket, Prefix: 'resized/rawdata/' });
  }

  if (bucket.endsWith(DATA_SUFFIX)) {
    theBucket = bucket.substring(0, bucket.length - DATA_SUFFIX.length) + RESIZED_SUFFIX;
    return Object.freeze({ Bucket: theBucket, Prefix: 'resized/data/' });
  }

  throw RangeError('Unsupported bucket name pattern: ' + bucket);
}
