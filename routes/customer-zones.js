const { checkPermissions, checkPathParameter} = require('./utils');
const router = require('express-promise-router')();

module.exports = router;

router.get('/zones/:customer',  async (req, resp) => {
  checkPermissions(req, resp, async (request, response) => {
    const customer = checkPathParameter(request, 'customer');
    response.status(200).send({
      customer
    });
  });
});
