const {getShortHash: version, getCurrentYear} = require('../lib/health');
const router = require('express-promise-router')();
const user = require('../lib/user-management/');
const bcrypt = require('bcrypt');
const saltRounds = 10;
const tableau = require('../lib/tableau-management/');

router.get('/new', async (req, res) => {
  if (req.user.cgrs[0]!=='atl') {
    res.status(404).send('not found');
  }
  let newUser = {
    username: '',
    first_name: '',
    last_name: '',
    phone_number: '',
    cgrs: '',
    facilities:[],
    user_access: {
      tableau: {
        showNav: false,
        group: [],
        accessUrl: []
      }
    }
  };
  res.render('editUser', {
    updateUser: newUser,
    userType: 'new',
    user: req.user,
    version: await version(),
    year: getCurrentYear(),
    naturalCompare: require('string-natural-compare')
  });
});

router.get('/:id', async (req, res) => {
  if (req.user.cgrs[0]!=='atl') {
    res.status(404).send('not found');
  }
  let userRes = await user.fetchById([req.params.id]);
  if (userRes.rows[0].cgrs.split(',').some(x => x === req.user.cgrs[0]) || req.user.cgrs[0] === 'atl') {
    if (!userRes.rows[0].user_access || !userRes.rows[0].user_access.tableau) {
      userRes.rows[0].user_access = {
        ...userRes.rows[0].user_access,
        tableau: {
          showNav: false,
          group: [],
          accessUrl: []
        }
      };
    }
    res.render('editUser', {
      updateUser: userRes.rows[0],
      userType: 'edit',
      user: req.user,
      version: await version(),
      year: getCurrentYear(),
      naturalCompare: require('string-natural-compare')
    });
  } else {
    res.render('linkExpired', {error: 'User does not have access to this user.'});
  }

});

router.get('/', async (req, res) => {
  if (req.user.cgrs[0]!=='atl') {
    res.status(404).send('not found');
  }
  let userList = await user.fetchUserList();

  userList = userList.map(key => {
    key.full_name = `${key.first_name} ${key.last_name}`;
    return key;
  });

  res.render('usersList', {
    res: userList,
    userType: 'edit',
    user: req.user,
    version: await version(),
    year: getCurrentYear(),
    naturalCompare: require('string-natural-compare')
  });
});

router.post('/new', async (req, res) => {
  if (req.user.cgrs[0]!=='atl') {
    res.status(404).send('not found');
  }
  let obj = req.body;
  obj.pwd = bcrypt.hashSync('silverchicken', saltRounds);
  let resObj = {
    tableau:{
      errorCode: -1
    }
  };
  if(obj.user_access.new.tableau.showNav || obj.user_access.new.tableau.group.length) {
    let result = await createTableauUser(obj.user_access, req.body.email);
    if(result.errorCode !== -1) {
      resObj.tableau.errorCode = result.errorCode;
    }
  }
  obj.id = req.params.id;
  let tabUserAccess = obj.user_access.new;
  delete obj.user_access;
  obj.user_access = tabUserAccess;
  console.log(obj.user_access);
  let count = await user.addNewUser(obj);
  let message = '';
  if (count == 1) {
    if(resObj.tableau.errorCode !== -1) {
      message = 'New user created in Portal. Tableau User Creation failied due to errorCode '+resObj.tableau.errorCode;
    } else {
      message = 'New user created successfully.';
    }
  } else if (count == -1) {
    message = 'Email already exists.';
  } else {
    message = 'User not created because of server error.';
  }
  resObj = {...resObj, count: count, message: message};
  res.send(resObj);
});

router.post('/:id', async (req, res) => {
  if (req.user.cgrs[0] !== 'atl') {
    res.status(404).send('not found');
  }
  let obj = req.body;
  let resObj = {};
  if(obj.user_access.new.tableau.showNav || !tableau.compareArrayElements(obj.user_access.old.tableau.group, obj.user_access.new.tableau.group)) {
    let result = await createTableauUser(obj.user_access, req.body.email);
    if(result.errorCode !== -1) {
      resObj.tableau = {
        errorCode: result.errorCode
      };
    }
  }
  obj.id = req.params.id;
  let tabUserAccess = obj.user_access.new;
  delete obj.user_access;
  obj.user_access = tabUserAccess;
  // let olderDetail = obj.olderDetail;
  let count = await user.updateById(obj);
  let message = '';
  if (count == 1) {
    message = 'User information updated.';
  } else if (count == -1) {
    message = 'User information cannot be updated. Try again.';
  } else {
    message = 'Could not find user.';
  }
  resObj = {...resObj, count: count, message: message};
  res.send(resObj);
});

router.put('/delete/:id', async (req, res) => {
  if (req.user.cgrs[0] != 'atl') {
    res.status(404).send('not found');
  }
  let params = {};
  params.id = req.params.id;
  params.isActive = req.body.is_active;
  let userRes = await user.inActivateUser(params);
  if (userRes.rowCount == 1) {
    res.send({updatedRow: userRes.rowCount, message: 'User status saved successfully'});
  } else {
    res.send({updatedRow: userRes.rowCount, message: 'Something went wrong. Please try again.'});
  }
});

router.get('/changeCgr/:cgr', async (req, res) => {
  const cgrs = req.user.cgrs;
  const selectedCgr = req.params.cgr;

  //Change the order of cgr's in list of cgrs => move selectedCgr to the front
  cgrs.splice(cgrs.indexOf(selectedCgr), 1);
  cgrs.unshift(selectedCgr);

  //Store modified cgrs array back in session
  req.session.cgrs = cgrs;
  req.session.homepage = user.calcLandingPage(req.session.cgrs);

  //Authenticate and redirect
  if (!user.authenticateCgr(req, res)) return;
  res.send({landingPage: req.session.homepage});
});

async function isTableauUser(token, siteId, username) {
  let result = JSON.parse(await tableau.getAllTableauUsers(token, siteId));
  let users = result['users'];
  let authorizedUser = users.user.filter(x => x.name === username);
  return authorizedUser;
}

async function getTableauGroupIfExists(token, siteId, groupNames) {
  let result = JSON.parse(await tableau.getAllUserGroups(token, siteId));
  let groups = result['groups'] || {group: []};
  let groupArr = groups.group.filter(x => groupNames.some(y=> x.name===y));
  if (groupArr.length > 0) {
    return {errorCode: -1, group: groupArr};
  } else {
    return {errorCode: 'PEC.0004', group: null};
  }
}

async function createTableauUser(userAccess, email) {
  const theTokenResponse = await tableau.getTableauTokenForRestAPI();
  let signInRes = JSON.parse(theTokenResponse);
  if(signInRes.error) {
    return {errorCode: 'PEC.0005', message: 'Tableau Server Login failed'};
  }
  const token = signInRes['credentials']['token'];
  const siteId = signInRes['credentials']['site']['id'];
  let userRes = await isTableauUser(token, siteId, email);
  if(userRes.length === 0) {
    userRes = JSON.parse(await tableau.createUserForSite(token, email, siteId));
    if (userRes.error) {
      return {errorCode: 'PEC.0001', message: 'Unable to create User in Tableau'};
    }
    userRes = userRes.user;
  } else {
    userRes = userRes[0];
  }
  //
  if(userAccess.old.tableau.group.length && !tableau.compareArrayElements(userAccess.old.tableau.group, userAccess.new.tableau.group)) {
    let oldGroupsResults = await getTableauGroupIfExists(token, siteId, userAccess.old.tableau.group);

    if(oldGroupsResults.errorCode!== -1) {
      return {errorCode: 'PEC.0003', message: 'Failed to update the group of the user'};
    }

    const removeUserRes =  await Promise.all(
      oldGroupsResults.group.map( /* prepare the query context to the config */
        (group) => tableau.removeUserFromGroup(token, siteId, group.id, userRes.id)
      )
    );
    let removeUserArr = removeUserRes.map(theJSONString => theJSONString !== '' ? JSON.parse(theJSONString) : theJSONString);
    // let removeUserRes = await tableau.removeUserFromGroup(token, siteId, oldGroupResults.group.id, userRes.id);
    if(removeUserArr.filter(x=>x.error).length) {
      return {errorCode: 'PEC.0003', message: 'Failed to update the group of the user'};
    }
  }
  if(userAccess.new.tableau.showNav){
    // let newGroupResults = await getTableauGroupIfExists(token, siteId, userAccess.new.tableau.group);
    let newGroupResults = await getTableauGroupIfExists(token, siteId, userAccess.new.tableau.group);
    if(newGroupResults.errorCode !== -1) {
      return {errorCode: 'PEC.0003', message: 'Failed to update the group of the user'};
    }
    const addUserRes =  await Promise.all(
      newGroupResults.group.map( /* prepare the query context to the config */
        (group) => tableau.addUserToGroup(token, siteId, group.id, userRes.id)
      )
    );
    let addedUser = addUserRes.map(x=>JSON.parse(x));
    // let addedUser = JSON.parse(await tableau.addUserToGroup(token, siteId, newGroupResults.group.id, userRes.id));
    if(addedUser.filter(x=>x.error).length) {
      return {errorCode: 'PEC.0001', message: 'Unable to add User in a given group'};
    }
  }
  return {errorCode: -1, message: ''};
}

module.exports = router;
