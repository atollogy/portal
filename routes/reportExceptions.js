const debug = require('debug')('portal:reportException');
const router = require('express-promise-router')();
const moment = require('moment-timezone');
const { fetchSubjects } = require('../db/subjects');
const user = require('../lib/user-management/');
const config = require('../lib/dynamic-config');
const nodemailer = require('nodemailer');
const AWS = require('aws-sdk');
const imageStep = require('../db/imageStep.js');
const s3 = require('../s3.js');


// export our router to be mounted by the parent application
module.exports = router;

//Duplicated Code to be re-factored out when time permits
const SMTP_URL = {
  SES: new AWS.SES({
    accessKeyId: config.accessKeyID,
    secretAccessKey: config.secretAccessKey,
    region: config.region,
    apiVersion: '2010-12-01'
  })
};

function sendEmail(emailData, smtpUrl = SMTP_URL) {
  const transporter = nodemailer.createTransport(smtpUrl);
  return transporter.sendMail(emailData)
    .then(info => debug('Message sent: ' + info.response))
    .catch(err => debug('Error sending an email: ' + err));
}

//fetch pre-signed s3 link from aws as a string
async function getS3Link(customer, obj) {
  const params = {
    gw: obj.gateway_id,
    cam: obj.camera_id,
    time: moment(obj.collection_time).toDate(),
    step: obj.step_name,
    index: obj.name
  };
  const locationInfo = await imageStep.imageLocation(customer, params);
  return (await s3.signedLink(locationInfo, 604800));
}

//Run the data-service to load a cemex-brooksville report and find a exception if one occurred within given time-frame
async function fetchExceptions(req, res) {
  const queryParams = {
    date: null,
    shift: null,
    previous: false,
    facility: req.query.facility,
    from: req.query.from,
    to: req.query.to
  };
  const results = await fetchSubjects(req.params.cust, queryParams);
  if (!results) {
    res.send('No Data returned');
    return;
  }
  const subjects = results.subjects;
  const timezone = results.timezone;
  let result = {};
  await Promise.all(subjects.map(async subjectObject => {
    const composite = subjectObject.measures && subjectObject.measures.composite;
    const exceptions = composite && composite.filter(eventObject => eventObject.state === 'exception');
    const latestException = exceptions && exceptions.sort((x, y) => moment(x.start_time) - moment(y.start_time))[0]; //pick first exception in given time-frame
    if (latestException) {
      const image = await getS3Link(req.params.cust, latestException.images[0]);
      result[subjectObject.name] = {
        time: moment.utc(latestException.startTime).tz(timezone).format('hh:mm a z'),
        image: image
      };
    }
  }));

  if (Object.keys(result).length) {
    res.send(result);
    return result;
  } else res.send('No exception occurred in given time interval');
}

router.get('/brooksvilleViolation/:cust', async (req, res) => {
  if (!user.authenticateCgr(req, res)) return;
  if (!req.session.cgrs.some(cgr => cgr === req.params.cust) || req.query.facility !== 'Brooksville') {
    res.send('This data service can only be used for Cemex Brooksville');
    return;
  }
  const exceptions = await fetchExceptions(req, res);
  if (exceptions && Object.keys(exceptions).length) {
    Object.keys(exceptions).forEach(s => {
      //Send an email per each subject where exception was observed
      const data = {
        from: 'admin@atollogy.com',
        to: req.query.email,
        subject: `ALERT: Type - Safety - ${req.query.facility} ${s} - ${exceptions[s].time}`,
        html: `<br/>ALERT: Type - Safety - ${req.query.facility} ${s} - ${exceptions[s].time}<br/>
          <br/>POTENTIAL SAFETY INCIDENT. PLEASE REPORT IMMEDIATELY!<br/>
          <br/><a href="${exceptions[s].image}">Image Link</a><br/>`
      };
      sendEmail(data);
    });
  }
});