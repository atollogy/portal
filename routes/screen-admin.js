const router = require('express-promise-router')();
const screens = require('../lib/screen-manager');
const tokenMgr = require('../lib/token-manager');
const user = require('../lib/user-management/');
const debug = require('debug')('portal:screen-admin');

const { getApplicationContext } = require('../lib/view-context');

router.post('/activate', async function (req, res) {
  let params = {};
  params.customer = req.params.cust || req.user.cgrs[0];
  if (!user.authenticateCgr(req, res)) return;
  if (!user.authenticateFacility(req, res)) return;

  params.token = req.body.activationToken;
  params.screenName = req.body.screenName;
  params.existingScreens = await screens.allScreenNames(params.customer);
  if (await tokenMgr.claimToken(params)) {
    res.redirect('./configureScreen');
  } else {
    const contextObject = await getApplicationContext(req, {
      customer : params.customer,
      existingScreens : params.existingScreens,
      token : params.token,
      screen_name : params.screenName,
      flashMessage: `Could not claim token ${params.token}`
    });
    res.render('../views/screens/activate-screen.ejs', contextObject);
  }
});

router.get('/activate', async function (req, res) {
  let customer = req.params.cust || req.user.cgrs[0];
  debug(customer);
  if (!user.authenticateCgr(req, res)) return;
  if (!user.authenticateFacility(req, res)) return;

  let screenResults = await screens.allScreenNames(customer);
  let existingScreens = screenResults.map(d=>{return d.screen_name;});

  const contextObject = await getApplicationContext(req, {
    customer,
    existingScreens,
    token:'',
    screen_name:'',
    type:'',
    flashMessage: ''
  });

  res.render('../views/screens/activate-screen.ejs', contextObject);
});

router.get('/configureScreen', async function (req, res) {
  let customer = req.params.cust || req.user.cgrs[0];
  if (!user.authenticateCgr(req, res)) return;
  if (!user.authenticateFacility(req, res)) return;

  let subjects = await screens.subjectNames(customer);
  subjects = subjects.reduce((obj, s)=>{
    const key = s.facility; //group-by facility
    const grp = obj[key] || [];
    grp.push(s.subject_name);
    obj[key] = grp;
    return obj;
  }, {});

  let config = await screens.screensGetConfig(customer);
  const contextObject = await getApplicationContext(req, { customer, subjects, config });
  res.render('../views/screens/configure-screen', contextObject);
});

const getLandingPageView = (landingPage) => {
  if (landingPage == 'Summary Report') {
    return 'subjectSummary.ejs';
  } else if(landingPage == 'Detail Report') {
    return 'subjectDetail.ejs';
  } else if(landingPage == 'Yard Summary Report') {
    return 'YardSubjectSummary.ejs';
  } else if(landingPage == 'Yard Detail Report') {
    return 'YardSubjectDetail.ejs';
  } else if(landingPage == 'Cycle Report') {
    return 'cycle.ejs';
  } else if(landingPage == 'Visit Log') {
    return 'cycleStatus.ejs';
  } else if(landingPage == 'Current View') {
    return 'beaconTracking.ejs';
  }
};

router.post('/configureScreen', async function (req, res) {
  let params = {};
  params.customer = req.params.cust || req.user.cgrs[0];
  if (!user.authenticateCgr(req, res)) return;
  if (!user.authenticateFacility(req, res)) return;

  try{
    params.config = req.body.config;
    params.config.landing_page = getLandingPageView(req.body.landingPage);
    params.screenName = req.body.screenName;
    await screens.screensSetConfig(params);
    res.send({flashMessage: req.body.screenName +` device is now configured`});
  }catch(e){
    res.send({flashMessage:`There was some problem configuring the device.`});
  }

});

module.exports = router;

