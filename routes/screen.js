const _ = require('lodash');
const debug = require('debug')('portal:screen');
const router = require('express-promise-router')();

const tokenMgr = require('../lib/token-manager');
const { userIsAtollogist } = require('../lib/utils/cgr-util');
const { getApplicationContext } = require('../lib/view-context');

router.get('/awake/:screen_type', async function (request, response) {

  // Send unregistered screens through the on-boarding
  let params = { screen_type: request.params.screen_type };

  const sessionToken = _.get(request, 'session.posterConfig.token');
  const queryParamToken = _.get(request, 'query.token');
  let tokenIsFromQuery = false;

  if (userIsAtollogist(request.user)) {
    params.token = queryParamToken;
    tokenIsFromQuery = true;
  } else {
    params.token = sessionToken;
  }

  if (!params.token) {
    response.redirect('../register_me?screen_type=' + params.screen_type);
    return;
  }

  // If a token exists in session; then fetch the posterConfig associated with it
  _.set(request, 'session.posterConfig', await tokenMgr.fetchConfig(params));

  let updateParams = {
    token: params.token,
    cgr: request.session.posterConfig.cgr,
    screenName: request.session.posterConfig.screen_name,
    screenType: request.session.posterConfig.screen_type
  };
  await tokenMgr.updateLastAwakeTime(updateParams);
  // If a landing page was already configured in the fetched posterConfig then render it
  let landingPage = _.get(request, 'session.posterConfig.config.landing_page');
  if (!landingPage) {
    response.redirect('../register_me?screen_type=' + params.screen_type);
    return;
  }

  // Give pahu board sessions some user details for the reports and logos to work with
  if (!tokenIsFromQuery) {
    debug('setting long-lived pahu cookie');
    _.set(request, 'session.cookie.expires', );
    let cgr = _.get(request, 'session.posterConfig.cgr');
    _.set(request, 'user.cgrs', [cgr]);
    _.set(request, 'user.employeeType', cgr);
  }

  const contextObject = await getApplicationContext(request, {
    screenName: request.session.posterConfig.screen_name,
    landingPage: request.session.posterConfig.config.landing_page
  });

  // render the pahu poster
  response.render('../views/screens/' + params.screen_type + '/' + landingPage, contextObject);
});

router.get('/register_me', async function (request, response) {
  let params = {
    screen_type: request.query.screen_type || undefined,
    token: _.get(request, 'session.posterConfig.token')
  };

  if (undefined === params.token) {
    params.token = await tokenMgr.generateNextToken(params);
    _.set(request, 'session.posterConfig.token', params.token);
    _.set(request, 'session.posterConfig.screen_type', params.screen_type);
  }

  const contextObject = await getApplicationContext(request, {
    token: params.token,
    screen_type: params.screen_type
  });

  response.render('../views/screens/register_me.ejs', contextObject);
});

router.get('/latestConfig', async function (request, response) {
  let params = { token: _.get(request, 'session.posterConfig.token') };
  let newConfig = await tokenMgr.fetchConfig(params);
  _.set(request, 'session.posterConfig', newConfig);

  response.cacheControl({ maxAge: 5 * 60, private: true });
  response.send(newConfig || false);
});

module.exports = router;

