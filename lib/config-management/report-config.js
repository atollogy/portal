module.exports = {
  createNewReportConfigForVdbMill: async ({subjectName, overheadGatewayName, andonGatewayName, stickerStepName, operatorStepName}) => `
INSERT INTO public.customer_report_config (subject_name, measure_name, gateway_name, camera_id, step_name, extract_type, extract_key, extract_image_name) 
VALUES ('${subjectName}', 'bulb', '${andonGatewayName}', 'video0', 'andon', 'color_state', null, null);
INSERT INTO public.customer_report_config (subject_name, measure_name, gateway_name, camera_id, step_name, extract_type, extract_key, extract_image_name) 
VALUES ('${subjectName}', 'door', '${overheadGatewayName}', 'video0', '${stickerStepName}', 'state', 'position', null);
INSERT INTO public.customer_report_config (subject_name, measure_name, gateway_name, camera_id, step_name, extract_type, extract_key, extract_image_name) 
VALUES ('${subjectName}', 'operator', '${overheadGatewayName}', 'video0', '${operatorStepName}', 'bbox', 'person', 'anonymous.jpg');
INSERT INTO public.customer_report_config (subject_name, measure_name, gateway_name, camera_id, step_name, extract_type, extract_key, extract_image_name) 
VALUES ('${subjectName}', 'composite', '${overheadGatewayName}', 'video0', '${operatorStepName}', 'haas_mill_composite', null, 'anonymous.jpg');
`
};
