
const querystring = require('querystring');
const { requiredAppParam: getRequiredParameter } = require('../app-params');

const PAGE_SIZE = 1000;
module.exports = (function (request, fs, os) {
  const hostname = process.env.TABLEAU_SERVER || os.hostname();

  const oSecutiryObjects = {};
  const withSecurityOptions = (payloadObject) => {
    const certificate = getRequiredParameter('ca_cert');

    if (!Object.prototype.hasOwnProperty.call(oSecutiryObjects, 'rejectUnauthorized')) {
      Object.assign(oSecutiryObjects, {
        rejectUnauthorized: false,
        agentOptions: {
          ca: fs.readFileSync(certificate, 'utf8')
        }
      });
    }
    return Object.assign({}, oSecutiryObjects, payloadObject);
  };

  const getTableauEndpoint = () => `https://${hostname}:8443/portal/tableau`;

  return Object.freeze({
    getTableauTokenForRestAPI: () => {
      const signInUrl = `${getTableauEndpoint()}/api/3.4/auth/signin`;
      let postData = {
        credentials: {
          name: process.env.TABLEAU_ADMIN,
          password: process.env.TABLEAU_PWD,
          site: {
            'contentUrl': ''
          }
        }
      };
      const postOptions = withSecurityOptions({
        url: signInUrl,
        method: 'POST',
        body: JSON.stringify(postData),
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json'
        }
      });
      return new Promise(function (resolve, reject) {
        // Do async job
        request.post(postOptions, function (err, resp, body) {
          if (err) {
            reject(err);
          } else {
            resolve(body);
          }
        });
      });
    },
    createUserForSite : (token, username, siteId) => {
      const createUserUrl = `${getTableauEndpoint()}/api/3.4/sites/${siteId}/users`;
      let postData = JSON.stringify({
        user: {
          name: username,
          siteRole: 'Viewer'
        }
      });
      const options = withSecurityOptions({
        url: createUserUrl,
        body: postData,
        headers: {
          'X-Tableau-Auth': token,
          Accept: 'application/json',
          'Content-Type': 'application/json'
        }
      });
      return new Promise(function (resolve, reject) {
        // Do async job
        request.post(options, function (err, resp, body) {
          if (err) {
            reject(err);
          } else {
            resolve(body);
          }
        });
      });
    },
    getAllTableauUsers : (token, siteId) => {
      const userUrl = `${getTableauEndpoint()}/api/3.4/sites/${siteId}/users?pageSize=${PAGE_SIZE}`;
      const options = withSecurityOptions({
        url: userUrl,
        headers: {
          'X-Tableau-Auth': token,
          Accept: 'application/json'
        }
      });

      return new Promise(function (resolve, reject) {
        // Do async job
        request.get(options, function (err, resp, body) {
          if (err) {
            reject(err);
          } else {
            resolve(body);
          }
        });
      });
    },
    getAllUserGroups: (token, siteId) => {
      const groupUrl = `${getTableauEndpoint()}/api/3.4/sites/${siteId}/groups?pageSize=${PAGE_SIZE}`;
      const options = withSecurityOptions({
        url: groupUrl,
        headers: {
          'X-Tableau-Auth': token,
          Accept: 'application/json'
        }
      });
      return new Promise(function (resolve, reject) {
        // Do async job
        request.get(options, function (err, resp, body) {
          if (err) {
            reject(err);
          } else {
            resolve(body);
          }
        });
      });
    },
    getUsersInAGroup: (token, siteId, groupId) => {
      const userGroupUrl = `${getTableauEndpoint()}/api/3.4/sites/${siteId}/groups/${groupId}/users?pageSize=${PAGE_SIZE}`;

      const options = withSecurityOptions({
        url: userGroupUrl,
        headers: {
          'X-Tableau-Auth': token,
          Accept: 'application/json'
        }
      });

      return new Promise(function (resolve, reject) {
        // Do async job
        request.get(options, function (err, resp, body) {
          if (err) {
            reject(err);
          } else {
            resolve(body);
          }
        });
      });
    },
    removeUserFromGroup : (token, siteId, groupId, userId) => {
      let removeUserUrl = `${getTableauEndpoint()}/api/3.4/sites/${siteId}/groups/${groupId}/users/${userId}`;
      const options = withSecurityOptions({
        url: removeUserUrl,
        headers: {
          'X-Tableau-Auth': token,
          Accept: 'application/json'
        }
      });
      return new Promise(function (resolve, reject) {
        // Do async job
        request.delete(options, function (err, resp, body) {
          if (err) {
            reject(err);
          } else {
            resolve(body);
          }
        });
      });
    },
    addUserToGroup: (token, siteId, groupId, userId) => {
      let addUserToGroup = `${getTableauEndpoint()}/api/3.4/sites/${siteId}/groups/${groupId}/users`;
      let postBody = JSON.stringify({
        user: {
          id: userId
        }
      });
      const options = withSecurityOptions({
        url: addUserToGroup,
        headers: {
          'X-Tableau-Auth': token,
          Accept: 'application/json',
          'Content-Type': 'application/json'
        },
        body: postBody
      });
      return new Promise(function (resolve, reject) {
        // Do async job
        request.post(options, function (err, resp, body) {
          if (err) {
            reject(err);
          } else {
            resolve(body);
          }
        });
      });
    },
    getTrustedAuth: (username) => {
      // Setting URL and headers for request
      const postUrl = `${getTableauEndpoint()}/trusted`;
      const postData = querystring.stringify({ username });
      const postOptions = withSecurityOptions({
        url: postUrl,
        method: 'POST',
        body: postData,
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Length': postData.length
        }
      });
      // Return new promise
      return new Promise(function (resolve, reject) {
        // Do async job
        request.post(postOptions, function (err, resp, body) {
          if (err) {
            reject(err);
          } else {
            resolve(body);
          }
        });
      });
    },
    //comparing elements of two arrays. return true if all the elements are same
    compareArrayElements: (a, b) => {

      if(a.length !== b.length) return false;

      const mapper = {};

      const hasOwnProperty = Object.prototype.hasOwnProperty.bind(mapper);

      a.forEach(val => {
        mapper[val] = 1;
      });

      b.forEach(val => {
        if(hasOwnProperty(val)) {
          delete mapper[val];
        } else {
          mapper[val] = 1;
        }
      });

      return Object.keys(mapper).length === 0;
    }
  });

}(/** */
  require('request'),
  require('fs'),
  require('os')
));