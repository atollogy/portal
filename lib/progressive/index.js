'use strict';

(function makeProgressive (global, registrar) {
  registrar.call(global, '', ['window'], function (window) {

    const MAX_RETRIES = 3;
    const { document, MutationObserver, requestAnimationFrame, Image } = window;

    /**
     * Check which items are in the viewport and trigger the image loading
     */
    let processedCount;
    const checkViewport = function (elements) {
      if (elements && elements.length) {
        requestAnimationFrame(function checkViewportTick() {
          const windowHeight = window.innerHeight;

          let index = 0;
          while (index < elements.length) {
            const element = elements[index];
            const boundings = element.getBoundingClientRect();

            const { top, height } = boundings;
            if (0 < top + height && windowHeight > top) {
              loadFullImage(element);
              element.classList.remove('replace');
            } else {
              index += 1;
            }
          }

          processedCount = elements.length;
        });
      }
    };

    let refreshTimer;
    const onViewportEvent = (elements) => {
      refreshTimer = refreshTimer || setTimeout(() => {
        refreshTimer = null;
        checkViewport(elements);
      }, 200);
    };

    const onImageLoaded = function ({ image, element, href }) {
      requestAnimationFrame(function imageLoadedTick() {
        if (href === element.href) {
          element.style.cursor = 'default';
          element.addEventListener('click', function (event) {
            event.preventDefault();
          }, false);
        }

        /**
         * leverages animation using keyframes on scale and opacity
         */
        const pImage = element.querySelector && element.querySelector('img.preview');
        element.insertBefore(pImage, pImage && pImage.nextSibling).addEventListener('animationend', function () {
          if (pImage) {
            element.removeChild(pImage);
          }

          image.classList.remove('reveal');
        });
      });
    };

    const loadFullImage = function(element, retry) {
      const href = element && element.getAttribute('data-href') || element.href;
      if (!href) return;

      const image = new Image();

      const dataset = element.dataset || undefined;
      if (dataset) {
        if (dataset.srcset) image.srcset = dataset.srcset;
        if (dataset.sizes) image.sizes = dataset.sizes;
      }

      image.onload = () => onImageLoaded({ image, element, href });

      retry = 1 + (retry || 0);
      if (retry < MAX_RETRIES) {
        const delay = retry * 1000;
        image.onerror = function () {
          setTimeout(() => loadFullImage(element, retry), delay);
        };
      }

      image.className = 'reveal';
      image.src = href;
    };

    /**
     * Find all 'progressive replace' elements on load.
     */
    window.addEventListener('load', function () {
      const elements = document.getElementsByClassName('progressive replace');

      window.addEventListener('scroll', () => onViewportEvent(elements), false);
      window.addEventListener('resize', () => onViewportEvent(elements), false);

      if (MutationObserver) {
        const observer = new MutationObserver(function () {
          if (elements.length !== processedCount) {
            checkViewport(elements);
          }
        });

        observer.observe(document.body, {
          subtree: true,
          childList: true,
          attributes: true,
          characterData: true
        });
      }

      setTimeout(() => checkViewport(elements), 200);
    }, false);
  });
})(this, function makeRegistrar(name, dependencies, factory) {
  const global = this;

  if ('function' === typeof require && 'object' === typeof module && hasOwnProperty.call(module, 'exports')) {
    dependencies = dependencies.map((dependency) => require(dependency));
    module.exports = factory.call({}, dependencies);
  } else {
    dependencies = dependencies.map((dependency) => global[dependency]);
    const instance = factory.call({}, dependencies);
    const previous = global[name] || undefined;
    global[name] = instance;

    instance.noConflict = () => {
      global[name] = previous;
      return instance;
    };
  }
});