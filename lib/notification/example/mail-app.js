const {createTransport} = require('nodemailer');
const AWS = require('aws-sdk');
const debug = require('debug')('portal:mail-app');
const _ = require('lodash');
const ejs = require('ejs');

const config = require('../../dynamic-config');
const moment = require('moment');
require('moment-timezone');

const createSesTransport = () => (
  createTransport({
    SES: new AWS.SES({
      accessKeyId: config.accessKeyID,
      secretAccessKey: config.secretAccessKey,
      region: config.region,
      apiVersion: '2010-12-01'
    })
  })
);

const getPortalUrl = () => _.get({
  stg: 'https://www.stg.at0l.io',
  prd: 'https://www.atollogy.com'
}, config.env, 'http://localhost:3000');

const writeMessage = async (userRec, log) => ({
  from: 'frank@atollogy.com',
  to: 'stephen@atollogy.com', //userRec.emailAddress,
  subject: 'Medley long-lived visitors list',
  text: `The following trucks have been on site for over 90 minutes.
    <<insert log>>
    [Link to live report: ${getPortalUrl()}]`,
  html: await ejs.renderFile('./lib/notification/example/templates/simple.ejs', {userRec, log, portalUrl:getPortalUrl()})
});

const enableMail = true;

const pctToString = function (float) {
  return float.toLocaleString('en', {style: 'percent'});
};

const timezone = 'US/Eastern';

async function genAndSendMail(mailingList) {

  function removeExitRecords(d) {
    return (!d.states[0].isEnd && d.states[0].img_keys != null);
  }

  function inProgressRecords(d, minsToLookBack = 90) {
    if (!d || !d.length) return [];
    const withoutExits = d[0].subjects.filter(removeExitRecords);
    const then = moment.tz(moment().add(-1 * minsToLookBack, 'minutes'), timezone).toDate();
    const recents = withoutExits.filter(function (dataObject) {
      const lastSeenPlus = moment.tz(dataObject.states[0].lastSeen, timezone).toDate();
      const isLingering = lastSeenPlus < then;
      return isLingering;
    });
    return recents;
  }

  // get list of trucks on yard
  const report = require('../../../db/cycle').knownIdentifiers;
  const data = await report('titanamerica', {
    facility: 'Medley',
    qualifier: 'licence_plate',
    to: null,
    time: null,
    date: '2019-01-31',
    from: null,
    shift: 'day',
    filters: ['whitelist'],
    tagNames: [
      'customer_tag',
      'qc_tag'
    ]
  });
  const log = inProgressRecords(data);
  const lps = log.map(r => r.subjectId);
  debug(lps);

  const transport = createSesTransport();
  mailingList.forEach(async (email, pos, allEmails) => {
    const message = await writeMessage(email, lps);
    if (enableMail) {
      transport.sendMail(message, (err) => {
        if (err) console.error(err);
      });
    } else {
      debug(`email would have been sent to ${email.emailAddress}\n`, message);
    }
    debug(`Email progress: ${pctToString(pos / allEmails.length)} : ${email.emailAddress}`);
  });
  debug(`Email progress: ${pctToString(1.0)} Done`);
}

genAndSendMail([{emailAddress: 'stephen@bluegood.com'}]);
