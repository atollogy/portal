'use strict';
module.exports = (function () {

  const MIN_VALUE = 0;
  const MAX_VALUE = 0x7FFFFFFF;

  const Observation = function () {};

  /**
   * @member {String} type the type of object identified
   */
  Observation.prototype['type'] = undefined;
  /**
   * @member width the width of the bounding box where the object is contained
   */
  Observation.prototype['width'] = undefined;
  /**
   * @member height the height of the bounding box where the object is contained
   */
  Observation.prototype['height'] = undefined;
  /**
   * @member x the x offset in pixels of the top-right position of the bounding box
   */
  Observation.prototype['x'] = undefined;
  /**
   * @member x the y offset in pixels of the top-right position of the bounding box
   */
  Observation.prototype['y'] = undefined;
  /**
   * @member confidence the confidence score of the object identification
   */
  Observation.prototype['confidence'] = undefined;

  /**
   * @returns {Object/Objservation}
   */
  Observation.fromBoundingBox = (theSourceObject, theInstanceObject) => {
    if (theSourceObject) {
      theInstanceObject = theInstanceObject || new Observation();
      const hasOwnProperty = Object.prototype.hasOwnProperty.bind(theSourceObject);

      if (hasOwnProperty('type')) {
        Object.assign(theInstanceObject, { type: String( theSourceObject['type'] )});
      }

      if (hasOwnProperty('width')) {
        Object.assign(theInstanceObject, { width: Number( theSourceObject['width'] )});
      }

      if (hasOwnProperty('height')) {
        Object.assign(theInstanceObject, { height: Number( theSourceObject['height'] )});
      }

      if (hasOwnProperty('x')) {
        Object.assign(theInstanceObject, { x: Number( theSourceObject['x'] )});
      }

      if (hasOwnProperty('y')) {
        Object.assign(theInstanceObject, { y: Number( theSourceObject['y'] )});
      }

      if (hasOwnProperty('confidence')) {
        Object.assign(theInstanceObject, { confidence: Number( theSourceObject['confidence'] )});
      }

      theInstanceObject = Object.freeze(theInstanceObject);
    }

    return theInstanceObject;
  };

  /**
   * Interface
   * @constructor
   */
  const Filter = function () {
  };

  Filter.prototype.match = function () {
    throw 'NotImplementedError';
  };

  /**
   *
   * @param {Filter} left
   * @param {Filter} right
   */
  const AndFilter = function(left, right) {
    Filter.apply(this);
    this.left = left;
    this.right = right;
  };
  AndFilter.prototype = Object.create(Filter.prototype);

  AndFilter.prototype.match = function (eventObject) {
    const { left, right } = this;
    return left.match(eventObject) && right.match(eventObject);
  };

  const ObjectClassMapping = {
    car: 'vehicle:car',
    truck: 'vehicle:truck',
    train: 'vehicle:train',
    tractor: 'vehicle:tractor',
    trailer: 'vehicle:trailer',
    operator: 'human:operator',
    person: 'human:person',
    human: 'human:generic',
    vehicle: 'vehicle:generic',
    isHuman: (type) => type && type.length > 4 && type.startsWith('human'),
    isVehicle: (type) => type && type.length > 6 && type.startsWith('vehicle')
  };

  Object.defineProperty(ObjectClassMapping, 'toStandardType', {
    value: (type) => {
      return ObjectClassMapping[type] || type;
    },
    enumerable: true,
    write: false
  });

  const BoundingBoxes = Object.freeze({
    filter: (listOfBoundingBoxes, propertyName) => {
      const expected = ObjectClassMapping.toStandardType(propertyName);
      const predicate = ObjectClassMapping.isHuman(expected) ? ObjectClassMapping.isHuman : ObjectClassMapping.isVehicle;
      return listOfBoundingBoxes.filter(element => !!element).filter(
        ({ type }) => predicate(ObjectClassMapping[type])
      ).map(({ x, y, width, height, confidence, getSourceObject = () => {} }) => {
        return { 'x1': x, 'y1': y, 'x2': width + x, 'y2': height + y, width, height, confidence, getSourceObject };
      });
    }
  });

  Filter.prototype.and = function (other) {
    return new AndFilter(this, other);
  };

  /**
   *
   * @param {Filter} left
   * @param {Filter} right
   */
  const OrFilter = function(left, right) {
    Filter.apply(this);
    this.left = left;
    this.right = right;
  };
  OrFilter.prototype = Object.create(Filter.prototype);

  OrFilter.prototype.match = function (eventObject) {
    const { left, right } = this;
    return left.match(eventObject) || right.match(eventObject);
  };

  Filter.prototype.or = function (other) {
    return new OrFilter(this, other);
  };

  /* */
  const Holder = {};

  const IndetityFilter = function () {
    Filter.apply(this);
  };

  IndetityFilter.prototype = Object.create(Filter.prototype);

  IndetityFilter.prototype.match = (/*ignored*/) => true;
  IndetityFilter.prototype.and = (other) => other;
  IndetityFilter.prototype.or = (/* ignored */) => Holder.identity;

  Holder.identity = new IndetityFilter();

  /**
   *
   * @param lower the minimum area
   * @param upper the maximum area
   * @constructor
   */
  const AreaFilter = function (measure, lower, upper) {
    Filter.apply(this);
    this.measure = measure || 'ignored';
    this.lower = lower || MIN_VALUE;
    this.upper = upper || MAX_VALUE;
  };
  AreaFilter.prototype = Object.create(Filter.prototype);

  AreaFilter.prototype.match = function (eventObject) {
    let hasMatchingObject = false;
    const { measure: measureName, lower, upper } = this;

    const configObject= eventObject._ConfigObject;
    const thePropertyName = configObject[measureName].extractKey;
    const mapOfIdentifiedObjects = eventObject._Observations;

    if (mapOfIdentifiedObjects && mapOfIdentifiedObjects.length) {

      let theBoundingBoxObjects = BoundingBoxes.filter(mapOfIdentifiedObjects, thePropertyName);

      const countOfMatchingObject = theBoundingBoxObjects.reduce((counter, theBoundingBoxObject) => {
        /* increment the counter if the area within the bounds */
        if ('width' in theBoundingBoxObject && 'number' === typeof theBoundingBoxObject.width && 'height' in theBoundingBoxObject && 'number' === typeof theBoundingBoxObject.height ) {
          const area = theBoundingBoxObject.width * theBoundingBoxObject.height;
          const isWithinArea = (area >= lower && area <= upper);
          counter += isWithinArea ? 1 : 0;
        }
        return counter;
      }, 0);

      hasMatchingObject = countOfMatchingObject > 0;
    }

    return hasMatchingObject;
  };

  /**
   * @class NotFilter
   */
  const NotFilter = function (delgateFilter) {
    Filter.apply(this);
    this.delgateFilter = delgateFilter;
  };

  NotFilter.prototype = Object.create(Filter.prototype);

  NotFilter.prototype.match = function (eventObject) {
    const { delgateFilter } = this;
    return !delgateFilter.match(eventObject);
  };

  /**
   *
   * @param lower the minimum area
   * @param upper the maximum area
   * @constructor
   */
  const WidthFilter = function (measure, lower, upper) {
    Filter.apply(this);
    this.measure = measure || 'ignored';
    this.lower = lower || MIN_VALUE;
    this.upper = upper || MAX_VALUE;
  };
  WidthFilter.prototype = Object.create(Filter.prototype);

  WidthFilter.prototype.match = function (eventObject) {
    let hasMatchingObject = false;
    const { measure: measureName, lower, upper } = this;

    const configObject= eventObject['_ConfigObject'];
    const thePropertyName = configObject[measureName]['extractKey'];
    const mapOfIdentifiedObjects = eventObject['_Observations'];

    if (mapOfIdentifiedObjects && mapOfIdentifiedObjects.length) {

      let theBoundingBoxObjects = BoundingBoxes.filter(mapOfIdentifiedObjects, thePropertyName);

      const countOfMatchingObject = theBoundingBoxObjects.reduce((counter, theBoundingBoxObject) => {
        /* increment the counter if the area within the bounds */
        if ('width' in theBoundingBoxObject && 'number' === typeof theBoundingBoxObject.width) {
          const isWithinWidth = (theBoundingBoxObject.width >= lower && theBoundingBoxObject.width <= upper);
          counter += isWithinWidth ? 1 : 0;
        }
        return counter;
      }, 0);

      hasMatchingObject = countOfMatchingObject > 0;
    }

    return hasMatchingObject;
  };

  /**
   *
   * @param lower the minimum area
   * @param upper the maximum area
   * @constructor
   */
  const ConfidenceFilter = function (measure, lower, upper) {
    Filter.apply(this);
    this.measure = measure || 'ignored';
    this.lower = lower || MIN_VALUE;
    this.upper = upper || MAX_VALUE;
  };
  ConfidenceFilter.prototype = Object.create(Filter.prototype);

  ConfidenceFilter.prototype.match = function (eventObject) {
    let hasMatchingObject = false;
    const { measure: measureName, lower, upper } = this;

    const configObject= eventObject._ConfigObject;
    const thePropertyName = configObject[measureName].extractKey;
    const mapOfIdentifiedObjects = eventObject._Observations;

    if (mapOfIdentifiedObjects && mapOfIdentifiedObjects.length) {

      let theBoundingBoxObjects = BoundingBoxes.filter(mapOfIdentifiedObjects, thePropertyName);

      const countOfMatchingObject = theBoundingBoxObjects.reduce((counter, theBoundingBoxObject) => {
        /* increment the counter if the area within the bounds */
        if ('confidence' in theBoundingBoxObject && 'number' === theBoundingBoxObject.confidence) {
          const isWithinConfidence = (theBoundingBoxObject.confidence >= lower && theBoundingBoxObject.confidence <= upper);
          counter += isWithinConfidence ? 1 : 0;
        }
        return counter;
      }, 0);

      hasMatchingObject = countOfMatchingObject > 0;
    }

    return hasMatchingObject;
  };

  /**
   * Used to exclude events where a person is within the vehicle bounding box and
   * the distance of the bottom lines is less than then threshold percentage.
   * The percentage is relative to the height of the vehicle bounding box.
   * @param lower the minimum area
   * @param upper the maximum area
   * @constructor
   */
  const InVehicleFilter = function (measure, lower, upper) {
    Filter.apply(this);
    this.measure = measure || 'ignored';
    this.lower = lower || MIN_VALUE;
    this.upper = upper || MAX_VALUE;
  };
  InVehicleFilter.prototype = Object.create(Filter.prototype);

  InVehicleFilter.prototype.match = function (eventObject) {
    const { lower } = this;
    const mapOfIdentifiedObjects = eventObject._Observations;
    const listOfVehicleObjects = BoundingBoxes.filter(mapOfIdentifiedObjects, 'vehicle');
    const listOfHumanObjects = BoundingBoxes.filter(mapOfIdentifiedObjects, 'human');

    const countOfMatchingObject = listOfVehicleObjects.reduce((counter, vehicle) => {
      /* increment the counter if the area within the bounds */
      listOfHumanObjects.forEach(human => {
        if(human && vehicle) {
          const total = Math.abs(vehicle.y2 - vehicle.y1);
          const allowed = Math.ceil(lower * total / 100);
          const actual = Math.abs(vehicle.y2 - human.y2);

          const isHumanWithinVehicle = (human.x1 >= vehicle.x1 && human.y1 >= vehicle.y1 && human.x2 <= vehicle.x2 && human.y2 <= vehicle.y2);
          const isOverAllowedDistance = actual >= allowed;
          counter += isHumanWithinVehicle && isOverAllowedDistance ? 1 : 0;
        }
      });

      return counter;
    }, 0);

    return countOfMatchingObject > 0;
  };

  /**
   *
   * @param lower the minimum area
   * @param upper the maximum area
   * @constructor
   */
  const DistanceFilter = function (measure, lower, upper) {
    Filter.apply(this);
    this.measure = measure || 'ignored';
    this.lower = lower || MIN_VALUE;
    this.upper = upper || MAX_VALUE;
  };

  DistanceFilter.prototype = Object.create(Filter.prototype);

  DistanceFilter.prototype.match = function (eventObject) {
    const { lower, upper } = this;
    const mapOfIdentifiedObjects = eventObject._Observations;
    const listOfVehicleObjects = BoundingBoxes.filter(mapOfIdentifiedObjects, 'vehicle');
    const listOfHumanObjects = BoundingBoxes.filter(mapOfIdentifiedObjects, 'human');

    if(listOfVehicleObjects.length === 0 || listOfHumanObjects.length === 0) return true;
    const countOfMatchingObject = listOfVehicleObjects.reduce((counter, vehicle) => {
      /* increment the counter if the area within the bounds */
      listOfHumanObjects.forEach(human => {
        if (human && vehicle) {
          const dx = vehicle.x2 - human.x1;
          const dy = vehicle.y2 - human.y2;
          const distance = Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
          const isWithinDistance = distance >= lower && distance <= upper;
          const nonOverlap = (human.x2 < vehicle.x1 || human.x1 > vehicle.x2) || (human.y1 > vehicle.y2 || human.y2 < vehicle.y1);
          counter += nonOverlap && isWithinDistance;
        }
      });

      return counter;
    }, 0);

    return countOfMatchingObject > 0;
  };

  /**
   *
   * @param lower the minimum area
   * @param upper the maximum area
   * @constructor
   */
  const ContainClassFilter = function (measure, lower, upper) {
    Filter.apply(this);
    this.measure = measure || 'ignored';
    this.lower = lower || MIN_VALUE;
    this.upper = upper || MAX_VALUE;
  };
  ContainClassFilter.prototype = Object.create(Filter.prototype);

  ContainClassFilter.prototype.match = function (eventObject) {
    const mapOfIdentifiedObjects = eventObject._Observations;
    const listOfVehicleObjects = BoundingBoxes.filter(mapOfIdentifiedObjects, 'vehicle');
    const listOfHumanObjects = BoundingBoxes.filter(mapOfIdentifiedObjects, 'human');

    const countOfMatchingObject = listOfVehicleObjects.reduce((counter, vehicle) => {
      /* increment the counter if the area within the bounds */
      listOfHumanObjects.forEach(human => {
        if(human && vehicle) {
          counter += (human.x1 >= vehicle.x1 && human.y1 >= vehicle.y1)
          /* */ && (human.x2 <= vehicle.x2 && human.y2 <= vehicle.y2);
        }
      });

      return counter;
    }, 0);

    return countOfMatchingObject > 0;
  };

  /**
   *
   * @param lower the minimum area
   * @param upper the maximum area
   * @constructor
   */
  const HeightFilter = function (measure, lower, upper) {
    Filter.apply(this);
    this.measure = measure || 'ignored';
    this.lower = lower || MIN_VALUE;
    this.upper = upper || MAX_VALUE;
  };
  HeightFilter.prototype = Object.create(Filter.prototype);

  HeightFilter.prototype.match = function (eventObject) {
    let hasMatchingObject = false;
    const { measure: measureName, lower, upper } = this;

    const configObject= eventObject['_ConfigObject'];
    const thePropertyName = configObject[measureName]['extractKey'];
    const mapOfIdentifiedObjects = eventObject['_Observations'];

    if (mapOfIdentifiedObjects && mapOfIdentifiedObjects.length) {
      let theBoundingBoxObjects = BoundingBoxes.filter(mapOfIdentifiedObjects, thePropertyName);

      const countOfMatchingObject = theBoundingBoxObjects.reduce((counter, theBoundingBoxObject) => {
        /* increment the counter if the area within the bounds */
        if ('height' in theBoundingBoxObject && 'number' === typeof theBoundingBoxObject.height) {
          const isWithinHeight = (theBoundingBoxObject.height >= lower && theBoundingBoxObject.height <= upper);
          counter += isWithinHeight ? 1 : 0;
        }
        return counter;
      }, 0);

      hasMatchingObject = countOfMatchingObject > 0;
    }

    return hasMatchingObject;
  };

  /* IMAGE FILTERS */
  const SupportedFilters = Object.freeze({
    'AREA': (measure, lower, upper) => new AreaFilter(measure, lower, upper),
    'WIDTH': (measure, lower, upper) => new WidthFilter(measure, lower, upper),
    'HEIGHT': (measure, lower, upper) => new HeightFilter(measure, lower, upper),
    'CONFIDENCE_SCORE': (measure, lower, upper) => new ConfidenceFilter(measure, lower, upper),
    'CONTAIN_CLASS': (measure, lower, upper) => new ContainClassFilter(measure, lower, upper),
    'IN_VEHICLE': (measure, lower, upper) => new InVehicleFilter(measure, lower, upper),
    'DISTANCE': (measure, lower, upper) => new DistanceFilter(measure, lower, upper),
    'INVALID': (theFilterType) => {
      throw `UnsupportedFilterType(${theFilterType})`;
    }
  });

  /** CONTENT FILTERES **/
  /**
   *
   * @param {String} gatewayId
   */
  const GatewayFilter = function (gatewayId) {
    Filter.apply(this);
    Object.assign(this, { gatewayId });
  };
  GatewayFilter.prototype = Object.create(Filter.prototype);

  GatewayFilter.prototype.match = function (eventObject) {
    const { gatewayId } = this;
    return eventObject && eventObject.gateways && eventObject.gateways.length && eventObject.gateways.includes(gatewayId);
  };

  /**
   *
   * @param {String} measureName
   */
  const MeasureNameFilter = function (measureName) {
    Filter.apply(this);
    this.measureName = measureName;
  };
  MeasureNameFilter.prototype = Object.create(Filter.prototype);

  MeasureNameFilter.prototype.match = function (eventObject) {
    const { measureName } = this;
    return eventObject && eventObject.measure_name && measureName === eventObject.measure_name;
  };

  /**
   *
   * @param {String} subjectName
   */
  const SubjectNameFilter = function (subjectName) {
    Filter.apply(this);
    this.subjectName = subjectName;
  };
  SubjectNameFilter.prototype = Object.create(Filter.prototype);

  SubjectNameFilter.prototype.match = function (eventObject) {
    const { subjectName } = this;
    return eventObject && eventObject.subject_name && subjectName === eventObject.subject_name;
  };

  /**
   * @function FilterTypeNotSupported exception case for unsupported filter types
   */
  const FilterTypeNotSupported = (theFilterType) => {
    throw `UnsupportedFilterType ${theFilterType}`;
  };

  const Filters = {
    identity: () => Holder.identity,
    negate: (filterObject) => new NotFilter(filterObject),
    withGatewayId: (gatewayId) => new GatewayFilter(gatewayId),
    withMeasureName: (measureName) => new MeasureNameFilter(measureName),
    withSubjectName: (subjectName) => new SubjectNameFilter(subjectName),
    ofType: (theFilterType) => {
      let theObjectFactory;

      const hasOwnProperty = Object.prototype.hasOwnProperty.bind(SupportedFilters);
      if (hasOwnProperty(theFilterType)) {
        theObjectFactory = SupportedFilters[theFilterType];
      } else {
        FilterTypeNotSupported(theFilterType);
      }

      return Object.freeze({
        withThresholds: ({measure, lower, upper}) => theObjectFactory(measure, lower, upper)
      });
    }
  };

  return {
    Filter: Object.freeze(Filter),
    Filters: Object.freeze(Filters),
    Observation: Object.freeze(Observation)
  };
}());
