const {apiKey, getFleetUrl} = require('../discovery');
const request = require('request');

module.exports = {
  login: (req, res) => {
    const {email} = req.body;
    request({
      method: 'POST',
      url: `${getFleetUrl('user')}/sign-in-with-api-key`,
      body: JSON.stringify({
        email, apiKey
      })
    }, (error, response, body ) => {
      if (body) {
        body = JSON.parse(body);
        res.send(body);
      } else {
        res.send(error);
      }
    });

  }
};