const AWS = require('aws-sdk');
const moment = require('moment');
const { accessKeyID, secretAccessKey, region} = require('../dynamic-config');
var cloudwatchlogs;

async function configure() {
  AWS.config.update({
    accessKeyId: accessKeyID,
    secretAccessKey: secretAccessKey,
    region: region
  });
  cloudwatchlogs = new AWS.CloudWatchLogs();
}

configure();

const createCloudWatch = (apiName, dataLog) => {
  cloudwatchlogs.createLogGroup({
    logGroupName: apiName
  }, (err, data) => {
    var parameters = {
      logGroupName: apiName,
      logStreamName: `${moment(new Date()).format('YYYY/MM/DD')}`
    };
    cloudwatchlogs.createLogStream(parameters, () => {
      var sequenceTokenRequest = {
        logGroupName: apiName,
        descending: true,
        limit: 1
      };
      cloudwatchlogs.describeLogStreams(sequenceTokenRequest, (oLogError, oDecribeResponse) => {
        if (oLogError) return;
        const sequenceTokenParams = oDecribeResponse.logStreams[0].uploadSequenceToken;
        const logData = {
          logEvents: [
            {
              message: `${JSON.stringify(dataLog)}`,
              timestamp: new Date().getTime()
            }
          ],
          logGroupName: apiName,
          logStreamName: parameters.logStreamName,
          sequenceToken: sequenceTokenParams
        };
        cloudwatchlogs.putLogEvents(logData, (oUpdateError) => {
          console.log('err; ', oUpdateError);
        });
      });
    });
  });
};

module.exports = {
  createCloudWatch
};

