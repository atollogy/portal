const pg = require('pg');
const { types } = pg;

const reDecimalPort = /(\.\d{1,})/;
const isSubSecondEnabled = () => !Object.prototype.hasOwnProperty.call(process.env, 'DISABLE_MS');

/**
 * pg.types.setTypeParser(1082, pgToString); // date
 * pg.types.setTypeParser(1083, pgToString); // time
 * pg.types.setTypeParser(1114, pgToString); // timestamp
 * pg.types.setTypeParser(1184, pgToString); // timestamptz
 * pg.types.  (1266, pgToString); // timetz
 */

/**
 * '2020-10-04 05:00:00+00'
 */
const theDateTimeZoneParser = types.getTypeParser(types.builtins.TIMESTAMPTZ);
types.setTypeParser(types.builtins.TIMESTAMPTZ, (value) => {
  const theDateValue = theDateTimeZoneParser(value);
  if (isSubSecondEnabled() && reDecimalPort.test(value)) {
    const theIntegerValue = (theDateValue.getTime() / 1000) >>> 0;
    const theDecimalValue = reDecimalPort.exec(value).find((_, index) => index === 1);
    const theTimeFloat = `${theIntegerValue}${theDecimalValue}`;
    return parseFloat(theTimeFloat * 1000);
  }
  return theDateValue;
});

/**
 * '2020-10-04 00:00:00'
 */
const theDateTimeParser = types.getTypeParser(types.builtins.TIMESTAMP);
types.setTypeParser(types.builtins.TIMESTAMP, (value) => {
  const theDateValue = theDateTimeParser(value);
  if (isSubSecondEnabled() && reDecimalPort.test(value)) {
    const theIntegerValue = (theDateValue.getTime() / 1000)  >>> 0;
    const theDecimalValue = reDecimalPort.exec(value).find((_, index) => index === 1);
    const theTimeFloat = `${theIntegerValue}${theDecimalValue}`;
    return parseFloat(theTimeFloat * 1000);
  }
  return theDateValue;
});

module.exports = pg;