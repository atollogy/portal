'use strict';

module.exports = (function (path, debug, { getShortHash, getCurrentYear }) {
  const listOfExtensions = [];
  const getOrCreateExtensionList = () => {
    if (listOfExtensions.length) {
      return listOfExtensions;
    }

    [ 'db/customerLPFilters.js', 'lib/cycles.js' ].forEach((dependency) => {
      try {
        const { ExtensionID } = require(path.join(__dirname, `../../${dependency}`));
        const theExtensionSource = Object.freeze({ source: ExtensionID });
        listOfExtensions.push(theExtensionSource);
      } catch(ignored) {
        debug('unable to load extension %s', dependency);
      }
    });

    return listOfExtensions;
  };

  const naturalCompare = require('string-natural-compare');
  const getVersionInfo = async () => {
    return Object.assign(
      { /* Target */ },
      { /* Standard */
        year: getCurrentYear(),
        version: await getShortHash(),
        extensions: getOrCreateExtensionList(),
        naturalCompare: naturalCompare
      }
    );
  };

  const oContextDefaults = Object.freeze({
    user: {},
    session: {},
    customer: '',
    facility: '',
    _started: Date.now()
  });

  const newApplicationContext = async (request, context) => {
    const { user } = Object.assign({}, oContextDefaults, request);
    const oRequestObject = Object.assign({}, oContextDefaults, request);
    const organizations = oRequestObject.session.cgrs || oRequestObject.user.cgrs;
    const { customer, facility } = Object.assign({}, oContextDefaults, request.query);

    const versionInfo = await getVersionInfo();

    return Object.assign(
      { /* Target */ },
      { /* Standard */
        user: user,
        customer: customer,
        facility: facility,
        cgrs: organizations
      },
      versionInfo,
      context
    );
  };

  return Object.freeze({
    getApplicationExtensions: getOrCreateExtensionList,
    getApplicationContext: newApplicationContext
  });
}(
  require('path'),
  require('debug')('portal:views'),
  require('../health')
));