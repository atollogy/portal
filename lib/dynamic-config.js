const os = require('os');
const bcm = require('./bcm');
const _ = require('lodash');
const cluster = require('cluster');

const DATABASE_TIMEOUT = 10 * 60 * 1000;
const DEFAULT_CONNECTIONS = 2;
const DEFAULT_MAX_CONNECTIONS = 4;

module.exports = {
  hostname: os.hostname(),
  consulHost: (bcm.get('env') === 'prd') ? '' : '',
  accessKeyID: bcm.get('aws_creds.akid'),
  secretAccessKey: bcm.get('aws_creds.sak'),
  region: bcm.get('aws_region'),
  imagesConfig: {
    accessKeyID: bcm.get('image_creds.akid'),
    secretAccessKey: bcm.get('image_creds.sak'),
    region: bcm.get('aws_region')
  },
  imagesConfigStage: {
    accessKeyID: bcm.get('image_creds_stage.akid'),
    secretAccessKey: bcm.get('image_creds_stage.sak')
  },
  env: bcm.get('env'),
  bcmPath: bcm.get('bcmPath'),
  logoutRedirectUrl: '/',
  appBaseUrl: '/portal',
  sessionSecret: '',
  sessionCookieMaxAge: 3 * 24 * 60 * 60 * 1000,
  authDbmsConnParameters: Object.assign(_.merge(
    {
      max: cluster.isMaster ? DEFAULT_CONNECTIONS : DEFAULT_MAX_CONNECTIONS,
      idleTimeoutMillis: DATABASE_TIMEOUT,
      connectionTimeoutMillis: DATABASE_TIMEOUT
    },
    bcm.get('endpoints.userdb')), {
    max: cluster.isMaster ? DEFAULT_CONNECTIONS : DEFAULT_MAX_CONNECTIONS,
    query_timeout: DATABASE_TIMEOUT,
    statement_timeout: DATABASE_TIMEOUT
  }),
  updatePasswordSNSArn: bcm.get('updatePasswordSNSArn'),
  updatePasswordSNSTopic: bcm.get('updatePasswordSNSTopic'),
  awsFleetApi: bcm.get('aws_creds_fleet_api')
};
