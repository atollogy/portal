const sql = require('yesql').pg;
const debug = require('debug')('portal:cache');

const ReadParameter = require('../app-params').optionalAppParam;
const useReportCache = ReadParameter('use-report-cache') || true;
const oDatabaseConnection = require('../../db');

const DEFAULT_TIME_TO_LIVE = 5 * 60;
const MAX_CACHE_SIZE = 16 * 1024 * 1024;
class Cache {
  constructor(ttl) {
    if (!ttl) throw new Error('You must provide a ttl to construct this cache');
    this.ttl = ttl;
    debug(`New cache created with TTL of ${ttl} seconds.`);
  }

  async get(customer, key) {
    let result = [];
    try {
      const parameters = { key: JSON.stringify(key) };
      const statement = sql(`SELECT cache_value FROM public.report_cache WHERE cache_key = :key AND expiration_time > now();`)(parameters);

      const row = (await oDatabaseConnection.query(customer, statement.text, statement.values)).rows[0];
      debug(`cache ${row ? 'hit' : 'miss'} {ttl: ${this.ttl}}`);

      result = row && row.cache_value.value;
    } catch (e) {
      debug(`cache is swallowing an error: ${e}.`);
    }

    return result;
  }

  async put(customer, key, value) {
    try {
      const statement = sql(`
        INSERT INTO public.report_cache (expiration_time, cache_key, cache_value) VALUES(now() + :ttl * INTERVAL '1 second', :key, :value)
        ON CONFLICT (cache_key) DO UPDATE SET cache_value = :value::jsonb, insert_time = now(), expiration_time = now() + :ttl * INTERVAL '1 second';
        `)({
        ttl: this.ttl.toString(),
        key: JSON.stringify(key),
        value: { value }
      });

      await oDatabaseConnection.query(customer, statement.text, statement.values, /** writeable */ true);
    } catch (cause) {
      debug(`cache is swallowing an error: ${cause}.`);
    }
  }
}

class CacheDecorator {
  constructor(cache) {
    this.cache = cache;
    this.enabled = readCacheEnabledParameter(useReportCache);
    debug(`use-report-cache is ${this.enabled}`);
  }

  async get(customer, key) {
    return this.enabled ? this.cache.get(customer, key) : undefined;
  }

  async put(customer, key, value) {
    if (this.enabled) {
      try {
        const content = value ? '' : JSON.stringify(value);

        if (content.length > MAX_CACHE_SIZE) {
          debug(`Cache size exceeded (max: ${MAX_CACHE_SIZE}).`);
          return;
        }

        this.cache.put(customer, key, value);
      } catch(cause) {
        debug(`cache is swallowing an error: ${cause}.`);
      }
    }
    return value;
  }
}

const readCacheEnabledParameter = (value) => {
  if ('boolean' === typeof value) {
    return value;
  }

  if ('string' === typeof value) {
    return 'true' === value.toLowerCase();
  }

  return false;
};

class CacheClient extends CacheDecorator {
  constructor(ttl = DEFAULT_TIME_TO_LIVE) {
    super(new Cache(ttl));
  }
}

module.exports = Object.freeze({ Cache: CacheClient });
