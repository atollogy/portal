const c = require('./index');

//Run tests in sequence
test('test create cache and insert key-value. check it', async () => {
  //Construct cache
  const cache = new c.Cache(5 * 60); //5 minutes

  //Put simple value into key
  const testKeyA1 = {
    cgr: 'alpha',
    date: new Date('2019-JAN-01'),
    shift: 'Day'
  };
  const testValueA1 = [{baz: 5, cgr: 'alpha'}];
  await cache.put(testKeyA1, testValueA1);
  const fetchValue = await cache.get(testKeyA1)
  expect(fetchValue).toEqual(testValueA1);
});

test('test replace value in cache with another value', async () => {
  //Construct cache
  const cache = new c.Cache(5 * 60); //5 minutes

  //Put simple value into key
  const testKeyA1 = {
    cgr: 'alpha',
    date: new Date('2019-JAN-01'),
    shift: 'Day'
  };
  const testValueA1 = [{baz: 5, cgr: 'alpha'}];
  await cache.put(testKeyA1, testValueA1);

  //Replace value
  const testValueA2 = [{baz: 7, cgr: 'alpha'}];
  await cache.put(testKeyA1, testValueA2);

  //Verify replace worked
  const value = await cache.get(testKeyA1);
  expect(value).not.toEqual(testValueA1);
  expect(value).toEqual(testValueA2);
});

test('test try to fetch value for non-existing key. get undefined', async () => {
  //Construct cache
  const cache = new c.Cache(5 * 60); //5 minutes

  const testKeyC1 = {
    cgr: 'charlie',
    date: new Date('2019-JAN-01'),
    shift: 'Day'
  };
  //Verify get returned undefined for non-existing key
  expect(await cache.get(testKeyC1)).toEqual(undefined);
});

test('test try to get value from cache after time expired and get undefined', async () => {
  //Construct cache
  const cache = new c.Cache(-5 * 60); //5 minutes
  jest.useFakeTimers();

  //First insert valid key/value into cache
  const testKeyA1 = {
    cgr: 'alpha',
    date: new Date('2019-JAN-01'),
    shift: 'Day'
  };
  const testValueA1 = [{baz: 5, cgr: 'alpha'}];
  await cache.put(testKeyA1, testValueA1);

  //jest.advanceTimersByTime(5 * 60 * 1000 + 2); //Fast forward to 5 minutes + 2 milliseconds
  expect(await cache.get(testKeyA1)).toEqual(undefined); //Verify key/value got deleted from cache and undefined was returned
});
