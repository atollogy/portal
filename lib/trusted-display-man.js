const _ = require('lodash');
const tokenMgr = require('../lib/token-manager');
const debug = require('debug')('portal:trusted-middleware');
const userIsAtollogist = require('../lib/utils/cgr-util').userIsAtollogist;

module.exports = {
  middleware: (userAuthFn) => {
    return async function (req, res, next) {

      const sessionToken = _.get(req, 'session.posterConfig.token');
      const queryParamToken = _.get(req, 'query.token');
      let tokenIsFromQuery = false;
      let token;

      if (userIsAtollogist(req.user)) {
        token = queryParamToken;
        tokenIsFromQuery = true;
      } else {
        token = sessionToken;
      }

      if (!token) {
        // if the session has no token, auth them
        userAuthFn(req, res, next);
      } else {
        if (undefined === _.get(req, 'session.posterConfig')) {
          // if we don't have the full config info on hand, then go get it
          _.set(req, 'session.posterConfig', await tokenMgr.fetchConfig(token));
        }
        const isActive = _.get(req, 'session.posterConfig.is_active');
        if (!isActive) {
          // pass them along for interactive auth
          userAuthFn(req, res, next);
        } else {
          // active displays get CGR info and get their resources
          if (!tokenIsFromQuery) {
            debug('setting long-lived pahu cookie');
            let cgr = _.get(req, 'session.posterConfig.cgr');
            _.set(req, 'user.cgrs', [cgr]);
            _.set(req, 'user.employeeType', cgr);
          }
          next();
        }
      }
    };
  }
};
