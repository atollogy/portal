const config = require('../dynamic-config');
const {ensureLoggedIn} = require('connect-ensure-login');
const trustedDisplayMiddleware = require('../trusted-display-man').middleware;

function createProtector(app) {
  const { appBaseUrl } = config;

  return function (pathPart, handler) {
    app.use(
      `${appBaseUrl}/${pathPart}`,
      trustedDisplayMiddleware(ensureLoggedIn(`${appBaseUrl}/login`)),
      handler);
  };
}

module.exports = createProtector;
