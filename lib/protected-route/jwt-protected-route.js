const config = require('../dynamic-config');
const jwt = require('jsonwebtoken');

const withAuth = function(req, res, next) {
  const token =
    req.body.token ||
    req.query.jwt ||
    req.headers['authorization'] ||
    req.cookies.token;
  if (!token) {
    res.status(401).send('Unauthorized: No token provided');
  } else {
    jwt.verify(token, config.sessionSecret, function(err, decoded) {
      if (err) {
        res.status(401).send('Unauthorized: Invalid token');
      } else {
        req.user = decoded;
        next();
      }
    });
  }
};
module.exports = withAuth;
