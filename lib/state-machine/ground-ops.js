var StateMachine = require('javascript-state-machine');

let FSM = StateMachine.factory({
  init: 'idle',
  transitions: [
    { name: 'planeArrived',     from: 'idle',  to: 'arrived' },
    { name: 'shiftStart',     from: 'idle',  to: 'serviced' },
    { name: 'startServicing',   from: ['arrived', 'running', 'serviced', 'idle'], to: 'running'  },
    { name: 'doneServicing', from: ['running', 'leaving'], to: 'serviced'    },
    { name: 'planeLeaving', from: ['serviced','leaving'],    to: 'leaving' },
    { name: 'planeLeft', from: ['leaving','arrived'],    to: 'idle' },
    { name: 'goto', from: '*', to: function(s) { return s;} }
  ],
  methods: {
    onInvalidTransition: function(transition, from, to) {
      console.log('transition not allowed from that state');
    }
  }
});

module.exports.fsm = FSM;
