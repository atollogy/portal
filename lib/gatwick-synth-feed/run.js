const gatwick = require('.');
const moment = require('moment');

// const now = moment(new Date()).add(8, 'hours').toDate();
const now = moment(new Date()).add(7, 'hours').toDate();

const gate = 27;

let when = now;

for(let i = 10; i <= 10; i++) {
  try {
    const src = `stand${gate}/02/20/segment_${i}.mp4`;
    const cp = gatwick.createCopyJobPlan(gate, src, when);
    console.log(cp);
    // gatwick.executeCopyPlan(cp);
  } catch (e) {
    console.error(e);
  }
  when = moment(when).add(16 * 60 + 42, 'seconds').toDate();
}
