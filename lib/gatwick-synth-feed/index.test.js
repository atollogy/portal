const gatwickSynth = require('./index');

test('String parses to date', () => {
  expect(gatwickSynth.parseTimestampStr('01Oct2018_093517')).toEqual(new Date(2018, 9, 1, 9, 35, 17, 0));
});

test('16m42s offset is accurate', () => {
  expect(gatwickSynth.offsetTimestamp(new Date(2018, 0, 1, 0, 0, 0, 0))).toEqual(new Date(2018, 0, 1, 0, 16, 42, 0));
});

test('output date formats are accurate', () => {
  expect(gatwickSynth.formatOutputDateStr(new Date(2018, 0, 1, 0, 0, 0, 0))).toEqual('01Jan2018_000000');
});

test('can reach example prd S3 dest file', async () => {
  expect(await gatwickSynth.anyDestFileExists('MyExport')).toEqual(true);
});

test('can reach example dev S3 src file', async () => {
  expect(await gatwickSynth.isSrcFileExists('stand14/02/15/segment_10.mp4')).toEqual(true);
});

test('test overall ability to create a correct copy plan', () => {
  const now = new Date(2018, 0, 1, 0, 0, 0, 0);
  const gate = 14;
  const src = `stand${gate}/02/15/segment_10.mp4`;
  const nowStr = '01Jan2018_000000';
  const offsetNowStr = '01Jan2018_001642';
  expect(gatwickSynth.createCopyJobPlan(gate, src, now)).toEqual(
    {
      src,
      dest: `gatwick/incoming/MyExport_LFR-00932 Stand ${gate} Centreline_01Oct2018_093517_10Oct2018_124021_${nowStr}_${offsetNowStr}.mp4`
    }
  );
});
