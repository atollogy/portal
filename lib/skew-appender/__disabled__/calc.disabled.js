const skew = require('../calc');

test('if utils mocked automatically', () => {

  const input = [{'y': 246, 'x': 465, 'tot': 711}, {'y': 252, 'x': 526, 'tot': 778}, {
    'y': 285,
    'x': 524,
    'tot': 809
  }, {'y': 280, 'x': 463, 'tot': 743}];
  const output = {
    'length_top': 61.29437168288782,
    'length_bottom': 61.204574992397426,
    'angle_top': -5.61758059012683,
    'angle_bottom': -4.685899839502702,
    'angle_left': 86.63353933657021,
    'angle_right': 86.53177074108285,
    'width_left': 34.058772731852805,
    'width_right': 33.06055050963308,
    'rect_area': 2087.6110748891906,
    'area': 2054.5
  };
  expect(skew(input)).toEqual(output);

});
