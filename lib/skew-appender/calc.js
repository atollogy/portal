// const math = require('Math');

//Shoelace formula implementation for area of a polygon - vertices need to be supplied in clockwise order
function polygonArea(corners) {
  const n = corners.length;
  let area = 0.0;
  for (let i = 0; i < n; i++) {
    const j = (i + 1) % n;
    area += corners[i][0] * corners[j][1];
    area -= corners[j][0] * corners[i][1];
  }
  area = Math.abs(area) / 2.0;
  return area;
}

function radians_to_degrees(radians) {
  return radians * (180 / Math.PI);
}

/**
 *
 * @param coordinates four xy corners of an LPR read
 */
module.exports = function (coordinates) {

  const x1 = coordinates[0]['x'];
  const x2 = coordinates[1]['x'];
  const x3 = coordinates[2]['x'];
  const x4 = coordinates[3]['x'];
  const y1 = coordinates[0]['y'];
  const y2 = coordinates[1]['y'];
  const y3 = coordinates[2]['y'];
  const y4 = coordinates[3]['y'];

  const metrics = {};

  //Length
  const dxTop = x2 - x1;
  const dyTop = y2 - y1;
  const dxBottom = x3 - x4;
  const dyBottom = y3 - y4;

  metrics['length_top'] = Math.hypot(dxTop, dyTop);
  metrics['length_bottom'] = Math.hypot(dxBottom, dyBottom);

  metrics['angle_top'] = undefined;
  if (dxTop !== 0) metrics['angle_top'] = -1 * radians_to_degrees(Math.atan(dyTop / dxTop));

  metrics['angle_bottom'] = undefined;
  if (dxBottom !== 0) metrics['angle_bottom'] = -1 * radians_to_degrees(Math.atan(dyBottom / dxBottom));

  // Width
  const dxLeft = x1 - x4;
  const dyLeft = y1 - y4;
  const dxRight = x2 - x3;
  const dyRight = y2 - y3;

  metrics['angle_left'] = undefined;
  if (dxLeft !== 0) metrics['angle_left'] = -1 * radians_to_degrees(Math.atan(dyLeft / dxLeft));

  metrics['angle_right'] = undefined;
  if (dxRight !== 0) metrics['angle_right'] = -1 * radians_to_degrees(Math.atan(dyRight / dxRight));

  metrics['width_left'] = Math.hypot(dxLeft, dyLeft);
  metrics['width_right'] = Math.hypot(dxRight, dyRight);

  // Angle & Area
  metrics['rect_area'] = metrics['length_top'] * metrics['width_left'];
  metrics['area'] = polygonArea([[x1, y1], [x2, y2], [x3, y3], [x4, y4]]); //vertices in clockwise order

  return metrics;
};
