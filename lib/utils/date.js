const moment = require('moment');

function generateDateSeries(aStartDate, aStopDate) {
  const dateArray = [];
  const stopDate = moment(aStopDate);
  let currentDate = moment(aStartDate);
  while (currentDate.isSameOrBefore(stopDate)) {
    dateArray.push(moment(currentDate).format('YYYY-MM-DD'));
    currentDate = moment(currentDate).add(1, 'days');
  }
  return dateArray;
}

function _safeToDate(aDate, inputFormat) {
  let m = moment(aDate, inputFormat);
  if (typeof aDate === 'number') {
    m = moment(aDate);
  }
  return m;
}

function offsetDate(aDate, duration, unit, outFormat = 'YYYY-MM-DD', inputFormat = 'DD MMM YYYY') {
  const m = _safeToDate(aDate, inputFormat);
  const n = m.add(unit, duration);
  return moment(n).format(outFormat);
}

function formatDateToStr(aDate, outFormat = 'YYYY-MM-DD', inputFormat = 'DD MMM YYYY') {
  const m = _safeToDate(aDate, inputFormat);
  return moment(m).format(outFormat);
}

module.exports = {
  generateDateSeries, offsetDate, formatDateToStr
};
