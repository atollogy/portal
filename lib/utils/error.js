const { createCloudWatch } = require('../../lib/cloudwatch-services');
const randomstring = require('randomstring');

const errorCode = {
  syntaxError: 'S1',
  logicError: 'L1'
};

const mappingError = async (error, apiName) => {
  let message = 'An error occurred while processing your request, we are actively addressing the issue.';
  let code;
  if (
    error instanceof Error ||
    error instanceof EvalError ||
    error instanceof ReferenceError ||
    error instanceof TypeError ||
    error instanceof SyntaxError ||
    error instanceof RangeError
  ) {
    code = errorCode.syntaxError;
  } else {
    code = errorCode.logicError;
  }
  const errorResponse = {
    errorCode: code,
    code: 500,
    message,
    detail: error.toString(),
    errorId: randomstring.generate(10)
  };
  if (apiName) {
    await createCloudWatch(apiName, errorResponse);
  }
  return errorResponse;
};


module.exports = {
  mappingError
};
