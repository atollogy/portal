function countDistinctValuesForProp(ary, property) {
  function theReducer(a, v) {
    a[v] = 1;
    return a;
  }

  const map = ary.map(e => e[property]);
  const reduce = map.reduce(theReducer, {});

  return Object.keys(reduce).length;
}

module.exports = {
  countDistinctValuesForProp
};

