'use strict';

module.exports = (() => {
  const Settings = {
    enabled: false
  };

  const excludeRules = Object.freeze({
    'incorrect-value': (license) => {
      return 'string' !== typeof license;
    },
    'incorrect-length': (license) => {
      /** XXX: Accept 8 Digits license plates (TRIMAC) */
      return license.length <= 4 && license > 8;
    },
    'excluded-prefixes': (license) => {
      return /^(CA|GA|DOT).*$/i.test(license);
    },
    'is-single-character': (license) => {
      return license && /^(.)\1{1,}$/.test(license);
    },
    'no-digits-present': (license) => {
      return /^[^0-9]+$/.test(license);
    }
  });

  const evaluate = (license) => {
    return Object.values(excludeRules).some((rule) => rule.call(Settings, license));
  };

  return Object.freeze({
    configure: (options) => {
      Object.assign(Settings, options);
    },
    filterByBlacklistRules: (license, LPFilter=false) => {
      return Settings.enabled || LPFilter ? !evaluate(license) : license;
    }
  });
})();