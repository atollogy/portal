const cu = require('../cgr-util');

test('User is at0l', () => {
  expect(cu.userIsAtollogist({cgrs:['atl']})).toBeTruthy();
});

test('User is not at0l', () => {
  expect(cu.userIsAtollogist({cgrs:['butterfly']})).toBeFalsy();
});
