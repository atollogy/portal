const du = require('../date');

test('format is sane', () => {
  const result = du.formatDateToStr('15 Nov 1994');
  const expected = '1994-11-15';
  expect(expected).toBe(result);
});

test('offset is sane', () => {
  const result = du.offsetDate('15 Nov 1994', 'year', 1);
  const expected = '1995-11-15';
  expect(expected).toBe(result);
});
