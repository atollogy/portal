
function userIsAtollogist(user) {
  return user
    && 'cgrs' in user
    && Array.isArray(user.cgrs)
    && user.cgrs.includes('atl');
}

module.exports = {
  userIsAtollogist
};
