'use strict';

module.exports = {
  hashCode: (text) => {
    let hash = Date.now();

    if (text && 'string' === typeof text && text.length) {
      hash = 0;
      for(let index = 0; index < text.length; index += 1) {
        hash = Math.imul(31, hash) + text.charCodeAt(index) | 0;
      }
    }

    return hash;
  }
};
