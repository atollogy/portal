-- CREATE TABLE stephen.dw.d_timeslot AS
SELECT
  'lumenetix'                                        AS cgr,
  datum                                         AS DATEZ,
  shift_name,
  dayz,
  dayz + start_offset                           AS shift_start,
  dayz + end_offset                             AS shift_end,
  datum - (dayz + start_offset)                 AS shift_time_spent,
  (dayz + end_offset) - datum                   AS shift_time_remaining,
  ((dayz + end_offset) - (dayz + start_offset)) AS shift_length,
  --   start_offset,
  --   end_offset,
  shift_timezone_name
--   EXTRACT(YEAR FROM datum)                                              AS YEAR,
--   EXTRACT(MONTH FROM datum)                                             AS MONTH,
--   -- Localized month name
--   to_char(datum, 'TMMonth')                                             AS MonthName,
--   EXTRACT(DAY FROM datum)                                               AS DAY,
--   EXTRACT(DOY FROM datum)                                               AS DayOfYear,
--   -- Localized weekday
--   to_char(datum, 'TMDay')                                               AS WeekdayName,
--   -- ISO calendar week
--   EXTRACT(WEEK FROM
--           datum)                                                        AS CalendarWeek,
--   to_char(datum, 'mm/dd/yyyy')                                          AS FormattedDate,
--   'Q' || to_char(datum, 'Q')                                            AS Quartal,
--   to_char(datum, 'yyyy/"Q"Q')                                           AS YearQuartal,
--   to_char(datum, 'yyyy/mm')                                             AS YearMonth,
--   to_char(datum,
--           'iyyy/IW')                                                    AS YearCalendarWeek,
--   CASE WHEN EXTRACT(ISODOW FROM datum) IN (6, 7)
--     THEN 'Weekend'
--   ELSE 'Weekday' END                                                    AS Weekend,
--   --   CASE WHEN to_char(datum, 'MMDD') IN ('0101', '0704', '1225', '1226')
--   --     THEN 'Holiday'
--   --   ELSE 'No holiday' END                                                 AS AmericanHoliday,
--   EXTRACT(ISODOW FROM datum)                                            AS dow,
--   EXTRACT(DAY FROM datum) + (1 - EXTRACT(ISODOW FROM datum)) :: INTEGER AS CWStart,
--   EXTRACT(DAY FROM datum) + (7 - EXTRACT(ISODOW FROM datum)) :: INTEGER AS CWEnd,
--   -- Start and end of the month of this date
--   EXTRACT(DAY FROM datum) + (1 - EXTRACT(DAY FROM datum)) :: INTEGER    AS MonthStart
-- --   (EXTRACT(DAY FROM datum) + (1 - EXTRACT(DAY FROM datum)) + '1 month' :: INTERVAL) -
-- --   '1 day'                                                               AS MonthEnd
FROM (SELECT
        generate_series                                                   AS datum,
        date_trunc('day', generate_series) :: TIMESTAMP WITHOUT TIME ZONE AS dayz
      FROM generate_series('2018-01-01 00:00' :: TIMESTAMP, '2018-12-31 23:59', '1 minutes')) DQ
  LEFT JOIN customer_shift
    ON (day_of_week IS NULL OR day_of_week = EXTRACT(ISODOW FROM datum)) AND
       datum BETWEEN (dayz + start_offset) AND (dayz + end_offset)
WHERE shift_name IS NOT NULL
ORDER BY 1;

--
-- SELECT *
-- FROM generate_series('2018-01-01 00:00' :: TIMESTAMP WITH TIME ZONE,
--                      '2018-01-04 12:00', '1 minute');
