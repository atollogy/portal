WITH stephen AS (
    --- Generate fact data for a non-yard customer
     SELECT --sh.shift_name,
            calculate_slot(collection_time, i.collection_interval) AS d_time_slot_time,
            'tlf'                                                  AS d_cgr,
            r.subject_name                                         AS d_subject_name_major,
            r.measure_name                                         AS d_subject_name_minor,
            --fn_measure_name,
            r.extract_type                                         AS d_report_bar,
            step_function                                          AS d_fn_step_function,
            i.step_name                                            AS d_fn_step_name,
            coalesce(step_output -> 'subjects' ->> 'person',
                     (step_output ->> 'state') || ' ' || (step_output ->> 'color'),
                     step_output ->> 'position',
                     step_output ->> 'average_color',
                     step_output ->> 'Work_order',
                     step_output ->> 'card_state',
                     step_output ->> 'weight',
                     step_output ->> 'license_plate')              AS d_state,
--             step_output,
            --     step_output -> 'subjects' ->> 'person'                        AS person_count,
            --     (step_output ->> 'state') || ' ' || (step_output ->> 'color') AS andon_color_state,
            --     step_output ->> 'position'                                    AS door_position,
            --     step_output ->> 'average_color'                               AS average_color,
            --     step_output ->> 'Work_order'                                  AS work_order,
            --     step_output ->> 'card_state'                                  AS ocr_card_state,

            collection_time                                        AS f_collection_time,
            step_output ->> 'brightness'                           AS f_brightness,
            images                                                 AS f_images,
            CASE WHEN (step_output ->> 'warning') IS NULL
                      THEN 0
                 ELSE 1 END                                        AS f_warning,
            b.subject_name                                         AS d_sensor_gateway_display_name,
            gateway_id                                             AS d_sensor_gateway_id,
            i.camera_id                                            AS d_sensor_camera_id
     FROM image_step i
            LEFT JOIN customer_binding b ON i.gateway_id = b.beacon_id AND b.subject = 'gateway' AND
                                            i.collection_time BETWEEN b.start_time AND COALESCE(b.end_time, now())
            LEFT JOIN customer_report_config r
              ON b.subject_name = r.gateway_name AND i.camera_id = r.camera_id AND i.step_name = r.step_name
         --             LEFT JOIN customer_shift sh ON i.collection_time BETWEEN
         --              ((date_trunc('day', i.collection_time)) :: TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE
         --               sh.shift_timezone_name + sh.start_offset)
         --              AND
         --              ((date_trunc('day', i.collection_time)) :: TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE
         --               sh.shift_timezone_name + sh.end_offset)
     WHERE step_function NOT IN('baseMetadata',
                                'thumbnail',
                                'snapshot',
                                'pixel_average',
                                'digit_number_recognition',
                                'motion',
                                'lpra',
                                'ocr',
                                'scale_reader') AND
           collection_time BETWEEN '2018-08-02 17:42:00.000001' AND '2019-09-01 00:00:00'
         --            shift_name IS NOT NULL
     ORDER BY 1 DESC
    --   and step_function = 'sticker_detection'
    --   and cast(step_output ->> 'brightness' as float) < 100
    --   and cast(step_output -> 'subjects' ->> 'person' as INTEGER) > 2
    -- ORDER BY
    --   r.subject_name,
    --   calculate_slot(collection_time, i.collection_interval),
    --   r.measure_name
     )
SELECT *
    --     DISTINCT d_fn_step_function,
    --                 d_fn_step_name
FROM stephen
WHERE d_state IS NOT NULL;

--LIMIT 500;


SELECT DISTINCT step_function
FROM image_step
WHERE step_function NOT IN('baseMetadata', 'thumbnail', 'snapshot', 'digit_number_recognition', 'motion');
