const debug = require('debug')('portal:datamart:new-instance');
debug.log = console.log.bind(console);

function die(e) {
  console.error(e);
  process.exit(-1);
}

function makeNewClient(connInfo) {
  const {Client} = require('../../../lib/database');
  const client = new Client(connInfo);
  client.connect().catch(die);
  return client;
}

function runQryOnClient(client, query, qryParamValues) {
  return client
    .query(query, qryParamValues)
    .then(debug)
    .catch(die);
}

module.exports = {makeNewClient, runQryOnClient};
