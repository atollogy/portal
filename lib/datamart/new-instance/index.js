#!/usr/bin/env node

require('dotenv').config();

const {makeNewClient, runQryOnClient} = require('./db');
const {getDataMartConfig} = require('../incremental-dataloader/config-handling');
const {connInfo} = getDataMartConfig();

function createBootStrapConn(otherDbConn) {
  const bootstrapConnInfo = {...otherDbConn};
  bootstrapConnInfo.database = 'postgres';
  return bootstrapConnInfo;
}

async function createNewEmptyDatamart(q) {
  const bootConnInfo = createBootStrapConn(connInfo);
  const client = makeNewClient(bootConnInfo);
  await runQryOnClient(client, q.text, q.values);
  await client.end();
}

async function foo() {
  const q = `DROP DATABASE IF EXISTS $1`;
  const f = {text: q, values: [`${connInfo.database}z`]};
  await createNewEmptyDatamart(f);
}

foo().then(process.exit);
