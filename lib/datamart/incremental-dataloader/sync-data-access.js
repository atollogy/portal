'use strict';

/**
 *
 * @class {SyncDataAccess}
 */
module.exports = (function () {
  const {Pool} = require('../../../lib/database');
  const sql = require('yesql').pg;
  const syncConfig = require('./sync-config.js');
  const {SyncObject} = require('./sync-object.js');
  const debug = require('debug')('portal:datamart:access');

  const EMPTY_ARRAY = [];

  const prepareSyncObjectInsert = sql(`
      INSERT INTO datamart_sync(customer, last_image_step_id, num_records)
      VALUES(:customer, :lastImageStepId, :numRecords);
  `);

  const SELECT_LATEST_SYNC = sql(`
    SELECT *
     FROM datamart_sync
    WHERE customer = :customer
      AND enabled = :enabled
    ORDER BY sync_event_time DESC
    LIMIT 1;
  `);

  const SELECT_SYNC = sql(`
    SELECT *
     FROM datamart_sync
    WHERE customer = :customer
      AND enabled = :enabled
    ORDER BY sync_event_time DESC
    LIMIT 100;
  `);

  const DELETE_CUSTOMER_SYNC = sql(`
    UPDATE datamart.public.datamart_sync
       SET enabled = :enabled
     WHERE customer = :customer
       AND sync_event_time > =:sync_event_time::date
       AND enabled = true
  `);

  const SyncDataAccess = function () {
    const theConnectionPool = new Pool(syncConfig.ConnectionInfo);
    Object.defineProperty(this, '_ConnectionPool', {
      value: theConnectionPool,
      enumerable: false,
      writable: false
    });
  };

  SyncDataAccess.prototype.deleteSyncRecords = async (theCustomer, theSinceEventTime) => {
    if (!(theCustomer && 'string' === typeof theCustomer)) {
      throw new Error('Customer paramater must be a valid string');
    }
    if (!(theSinceEventTime && 'string' === typeof theSinceEventTime)) {
      throw new Error('SinceEventTime paramater must be a valid string');
    }

    try {
      const {_ConnectionPool: theConnectionPool} = this;
      /* enforces the latest time for the syncTime */
      const deleteSyncOptions = Object.assign({}, { customer: theCustomer, enabled: false, sync_event_time: theSinceEventTime });
      /* insert the syncObjectClone to the database */
      const statement = DELETE_CUSTOMER_SYNC(deleteSyncOptions);
      await theConnectionPool.query(statement.text, statement.values);
    } catch (cause) {
      debug(`Unexpected failure while deleting sync points, reason ${cause}`);
    }
  };

  SyncDataAccess.prototype.createSyncRecord = async function (theSyncObject) {
    try {
      const {_ConnectionPool: theConnectionPool} = this;
      /* enforces the latest time for the syncTime */
      const syncObjectClone = Object.assign({}, theSyncObject, { syncTimestamp: Date.now(), enabled: true });
      /* insert the syncObjectClone to the database */
      const q = prepareSyncObjectInsert(syncObjectClone);
      await theConnectionPool.query(q.text, q.values);
    } catch (cause) {
      debug(`Unexpected failure while saving next sync point, reason ${cause}`);
    }
  };

  SyncDataAccess.prototype.fetchSync = async function (customer) {
    let records;
    try {
      const {_ConnectionPool: theConnectionPool} = this;
      const q = SELECT_SYNC({ customer: customer, enabled: true });
      records = (await theConnectionPool.query(q.text, q.values)).rows;
    } catch (cause) {
      debug(`Unexpected failure while fetch sync records, reason ${cause}`);
    }

    return (records && records.length
      ? records.map((record) => SyncObject.fromDBObject(record))
      : EMPTY_ARRAY);
  };


  SyncDataAccess.prototype.fetchLatestSync = async function (customer) {
    let records;
    try {
      const {_ConnectionPool: theConnectionPool} = this;
      const q = SELECT_LATEST_SYNC({ customer: customer, enabled: true });
      records = (await theConnectionPool.query(q.text, q.values)).rows;
    } catch (cause) {
      debug(`Unexpected failure while fetch starting point for sync, reason ${cause}`);
    }

    return (records && records.length
      ? records.map((record) => SyncObject.fromDBObject(record))
      : EMPTY_ARRAY);

  };

  const SharedSyncDataAccess = new SyncDataAccess();

  return Object.freeze({
    SyncDataAccess: SharedSyncDataAccess
  });
}());