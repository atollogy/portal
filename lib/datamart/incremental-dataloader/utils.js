const yesql = require('yesql').pg;
const customerDbs = require('../../../db/');

async function fetchFacilities(cgr) {
  const sql = `
-- Fetch distinct list of all relevant facility & shift name combinations
SELECT DISTINCT coalesce(b.facility, a.facility) AS facility
FROM (SELECT DISTINCT facility FROM customer_binding) a
       LEFT OUTER JOIN customer_shift b ON (a.facility = b.facility OR b.facility IS NULL);`;
  const qry = yesql(sql)({});
  const {rows} = await customerDbs.query(cgr, qry.text, qry.values);
  return rows;
}

module.exports = {fetchFacilities};