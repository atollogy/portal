'use strict';

module.exports = (function () {
  const SyncObject = function () {
  };

  /**
   *
   * @member {String} customer the customer for which datamart is preparing the data
   */
  SyncObject.prototype['customer'] = undefined;

  /**
   *
   * @member {String} facility the facility for which datamart is preparing the data
   */
  SyncObject.prototype['facility'] = undefined;

  /**
   *
   * @member {Number} id the unique id of this record
   */
  SyncObject.prototype['id'] = undefined;

  /**
   *
   * @member {Number} lastImageStepId the id of the last image step
   */
  SyncObject.prototype['lastImageStepId'] = undefined;

  /**
   *
   * @member {Number} syncTimestamp Milliseconds from epoch
   */
  SyncObject.prototype['syncTimestamp'] = undefined;

  /**
   *
   * @member {Number} syncCreated Milliseconds from epoch
   */
  SyncObject.prototype['syncCreated'] = undefined;

  /**
   * @member {Number} numRecords count of records inserted.
   */
  SyncObject.prototype['numRecords'] = undefined;

  SyncObject.fromDatamartEvent = (theSourceEvent, theInstanceObject) => {
    if (theSourceEvent) {
      theInstanceObject = theInstanceObject || new SyncObject();

      theInstanceObject['customer'] = String(theSourceEvent['customer']);

      const theLatestEvent = Object.assign({}, theSourceEvent.latest);
      const theLatestImageStepId = theLatestEvent['image_step_id'];
      theInstanceObject['lastImageStepId'] = theLatestImageStepId;

      theInstanceObject['numRecords'] = Number(theSourceEvent['recordsLength']);
    }

    return theInstanceObject;
  };

  /**
   * Converts a returned record from the database into the SyncObject instance.
   * @param theDatabaseRecord the object returned by the query
   * @param theInstanceObject null or undefined or the object where we want the fields to be set-up.
   * @returns {SyncObject}
   */
  SyncObject.fromDBObject = (theDatabaseRecord, theInstanceObject) => {
    if (theDatabaseRecord) {
      theInstanceObject = theInstanceObject || new SyncObject();

      theInstanceObject['id'] = Number(theDatabaseRecord['synchronization_id']);

      theInstanceObject['customer'] = String(theDatabaseRecord['customer']);

      theInstanceObject['facility'] = String(theDatabaseRecord['facility']);

      theInstanceObject['lastImageStepId'] = Number(theDatabaseRecord['last_image_step_id']);

      if (Object.prototype.hasOwnProperty.call(theDatabaseRecord, 'sync_event_time')) {
        theInstanceObject['syncTimestamp'] = Number(theDatabaseRecord['sync_event_time']);
        /**
         * v0 compatibility: v1 introduced syncCreated
         */
        theInstanceObject['syncCreated'] = new Date(theInstanceObject['syncTimestamp']);
      }

      if (Object.prototype.hasOwnProperty.call(theDatabaseRecord, 'sync_event_created')) {
        const theSyncCreatedTimestamp = Number(theDatabaseRecord['sync_event_created']);
        theInstanceObject['syncCreated'] = new Date(theSyncCreatedTimestamp);
      }

      if (Object.prototype.hasOwnProperty.call(theDatabaseRecord, 'sync_record_count')) {
        theInstanceObject['numRecords'] = Number(theDatabaseRecord['sync_record_count']);
      }
    }

    return theInstanceObject;
  };

  return Object.freeze({
    SyncObject: SyncObject
  });
}());
