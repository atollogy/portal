const cyclesJs = require('../../../db/cycle.js');

async function beaconCycles(customer, date, shift, facility, qualifier) {
  const cyclesLogData = await cyclesJs.cycleLog(customer, {
    date,
    shift,
    facility,
    from: null,
    to: null,
    qualifier,
    tagNames: ['qc_tag', 'customer_tag'],
    filters: [],
    dataQueryName: 'cycleLogQuery',
    zonesQueryName: 'cycleZonesQuery'
  });
  return cyclesLogData.data;
}

module.exports = beaconCycles;