const yesql = require('yesql').pg;
const debug = require('debug')('portal:datamart:inserter');
const pgp = require('pg-promise')({ noWarnings: true, capSQL: true });

const { optionalAppParam: parameters } = require('../../app-params');

module.exports = function (config) {

  const { ConnectionPool } = require('../../data-access');

  const db = pgp(config.connInfo);
  const sql = `
    SELECT min(measure_start_time), max(measure_end_time), count(*), cgr
      FROM ${config.targetTables.coalescedEvents}
      GROUP BY 4`;
  const highWaterQuery = yesql(sql)({});

  function isBlankArray(records) {
    return !records || records.length === 0;
  }

  const primaryKeysCache = {};
  async function getPrimaryKeyColumns(theTableName) {
    let primaryKeyColumns = [];
    if (!Object.prototype.hasOwnProperty.call(primaryKeysCache, theTableName)) {
      const datamartConnection = ConnectionPool.findOrCreate(config.connInfo);

      const { rows } = await datamartConnection.query(`
        SELECT DISTINCT
            a.attname as column_name
        FROM
            pg_class t,
            pg_class i,
            pg_index ix,
            pg_attribute a
        WHERE
            t.oid = ix.indrelid
            AND i.oid = ix.indexrelid
            AND a.attrelid = t.oid
            AND a.attnum = ANY(ix.indkey)
            AND t.relkind = 'r'
            AND t.relname = $1`, [theTableName]);

      primaryKeyColumns = rows && rows.length ? rows.map(({ column_name }) => column_name.toLowerCase()) : null;
      primaryKeysCache[theTableName] = primaryKeyColumns;
    }

    primaryKeyColumns = primaryKeysCache[theTableName];
    debug(`Primary key columns ${JSON.stringify(primaryKeyColumns, null, 2)}`);
    return primaryKeyColumns;
  }

  function onConflict(theTableName, columnSet, listOfPrimaryKeyColumns) {
    if (!(listOfPrimaryKeyColumns && listOfPrimaryKeyColumns.length)) {
      return 'ON CONFLICT DO NOTHING';
    }
    let primaryKeyColumns;
    let updateableColumns;
    const table = `${theTableName}`.toLowerCase();
    switch(table) {
      case 'f_events':
      case 'f_intervals':
        primaryKeyColumns = listOfPrimaryKeyColumns.join(', ');
        primaryKeyColumns = primaryKeyColumns.split(',').map((element) => element.trim());

        updateableColumns = columnSet.columns.map(({ name }) => name.toLowerCase());
        updateableColumns = updateableColumns.filter((name) => !primaryKeyColumns.some((element) => element ===  name));
        updateableColumns = updateableColumns.map((name) => `${name}=EXCLUDED.${name}`);

        primaryKeyColumns = primaryKeyColumns.join(',');
        updateableColumns = updateableColumns.join(',');
        return `ON CONFLICT (${primaryKeyColumns}) DO UPDATE SET ${updateableColumns}`;
      case 'f_cycles_detailed':
        return 'ON CONFLICT DO NOTHING';
      case 'f_cycles_summarized':
        return 'ON CONFLICT DO NOTHING';
      default:
        return 'ON CONFLICT DO NOTHING';
    }
  }

  function segmentRecords(records, theTableName, listOfPrimaryKeyColumns) {
    const segments = [];
    if (!(listOfPrimaryKeyColumns && listOfPrimaryKeyColumns.length)) {
      return 'ON CONFLICT DO NOTHING';
    }

    let primaryKeyColumns;
    const visitedRecords = {};
    const table = `${theTableName}`.toLowerCase();

    switch(table) {
      case 'f_events':
      case 'f_intervals':
        primaryKeyColumns = listOfPrimaryKeyColumns.join(', ');
        primaryKeyColumns = primaryKeyColumns.split(',').map((element) => element.trim());
        for (let index =  0; index < records.length; index += 1) {
          const record = records[index];
          const primaryKey = primaryKeyColumns.map((column) => String(record[column]) || '').join('\\:\\');
          const segmentIndex = visitedRecords[primaryKey] || (visitedRecords[primaryKey] = 0);
          const segment = segments[segmentIndex] || (segments[segmentIndex] = []);
          segment.push(record);
          visitedRecords[primaryKey] = segmentIndex + 1;
        }
        break;
      default:
        segments.push(records);
        break;
    }

    const chunkSize = 2000;
    const chunks = segments.reduce((R, segment) => {
      for (let offset =  0; offset < segment.length; offset += chunkSize) {
        R.push(segment.slice(offset, offset + chunkSize));
      }
      return R;
    }, []);

    return chunks;
  }

  const ITERATIONS = 12;
  async function thinkTime(min = 200, max = 400) {
    let delta = max - min;
    let totalTime = min * min * ITERATIONS;
    for (let index = 0; index < ITERATIONS; index +=1) {
      const value = 0.5 * (delta + Math.random() * delta);
      totalTime += value * value ;
    }
    totalTime = Math.ceil(Math.sqrt(totalTime / ITERATIONS));
    await new Promise((resolve) => setTimeout(resolve, totalTime));
  }

  async function bulkInsertRecordsIntoTable(records, theTableName) {
    if (isBlankArray(records)) {
      // EARLY RETURN
      return;
    }

    debug(`Processing ${records.length} records for ${String(theTableName).toUpperCase()}`);
    const columnNames = Object.keys(records[0]);

    const columnSet = new pgp.helpers.ColumnSet(columnNames, { table: theTableName  });

    const primaryKeyColumns = await getPrimaryKeyColumns(theTableName);
    const onConflictStatement = onConflict(theTableName, columnSet, primaryKeyColumns);
    const segments = segmentRecords(records, theTableName, primaryKeyColumns);

    const oDateObject = new Date();
    for (let index =  0; index < segments.length; index += 1) {
      const segment = segments[index];
      const query = pgp.helpers.insert(segment, columnSet) + ' ' + onConflictStatement;
      const response = await db.none(query);
      await thinkTime();
      oDateObject.setTime(Date.now());
      debug(`${oDateObject.toISOString()} Processing segment ${index} (records: ${segment.length}, response: ${response})`);
    }
    return true;
  }

  async function bulkInsertSummaryCycleRecords(records) {
    return await bulkInsertRecordsIntoTable(records, config.targetTables.summarizedCycles);
  }

  async function bulkInsertDetailedCycleRecords(records) {
    return await bulkInsertRecordsIntoTable(records, config.targetTables.detailedCycles);
  }

  async function bulkInsertBeaconCycleRecords(records) {
    return await bulkInsertRecordsIntoTable(records, config.targetTables.beaconCycles);
  }

  async function bulkInsertRawEventRecords(table, records) {
    return await bulkInsertRecordsIntoTable(records, table);
  }

  const getStartingDate = () => parameters('start_date');

  async function fetchHighWaterMarks(customer) {
    const theStartingDate = getStartingDate();
    const datamart = ConnectionPool.findOrCreate(config.connInfo);

    let {rows} = await datamart.query(highWaterQuery.text, highWaterQuery.values);

    if (rows.length === 0 || theStartingDate) {
      rows = [{
        cgr: customer,
        min: `${theStartingDate}`,
        max: `${theStartingDate}`
      }];
    }

    return rows;
  }

  async function bulkInsertCoalescedSubjectRecords(records) {
    return await bulkInsertRecordsIntoTable(records, config.targetTables.coalescedEvents);
  }

  return {
    bulkInsertCoalescedSubjectRecords,
    bulkInsertSummaryCycleRecords,
    bulkInsertDetailedCycleRecords,
    bulkInsertRawEventRecords,
    fetchHighWaterMarks,
    bulkInsertBeaconCycleRecords
  };
};
