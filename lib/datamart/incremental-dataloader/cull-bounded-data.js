require('dotenv').config();
const yesql = require('yesql').pg;
const {Pool} = require('../../database');
const dmConf = require('./config-handling');
const config = dmConf.getDataMartConfig();
const db = new Pool(config.connInfo);
const { optionalAppParam: parameters } = require('../../app-params');
const firstDayToDelete = parameters('start_date');  //indicates slot to start the deletion from
const lastDayToDelete = parameters('end_date');     //indicates slot to start the deletion from

const theDatamartConfig = Object.assign({}, config.connInfo, { database: 'datamart' });
const theDatamartDB = new Pool(theDatamartConfig);

const queryParameters = Object.freeze({ firstDayToDelete, lastDayToDelete });

const deleteSyncRecords = yesql(`
      DELETE FROM public.datamart_sync WHERE customer = :customer`)({ customer: config.cgrs[0] });

const deleteEvents = yesql(`
      DELETE FROM ${config.targetTables.rawEvents}
      WHERE date_trunc('day', f_collection_time) BETWEEN :firstDayToDelete AND :lastDayToDelete`)(queryParameters);

const deleteIntervals = yesql(`
      DELETE FROM ${config.targetTables.coalescedEvents}
      WHERE date_trunc('day', measure_start_time) BETWEEN :firstDayToDelete AND :lastDayToDelete`)(queryParameters);

// Perform deletion of events and intervals
(async () => await theDatamartDB.query(deleteSyncRecords.text, deleteSyncRecords.values))().then((theResponseObject) => {
  console.log('Deletion of sync records complete.', theResponseObject.rowCount, 'rows deleted');
}).catch((theErrorObject) => {
  console.log('deleting sync records failed ' + theErrorObject);
});

(async () => await db.query(deleteEvents.text, deleteEvents.values))().then((theResponseObject) => {
  console.log('Deletion of events complete.', theResponseObject.rowCount, 'rows deleted');
}).catch((theErrorObject) => {
  console.log('culling events failed ' + theErrorObject);
});

(async () => await db.query(deleteIntervals.text, deleteIntervals.values))().then((theResponseObject) => {
  console.log('Deletion of intervals complete.', theResponseObject.rowCount, 'rows deleted');
}).catch((theErrorObject) => {
  console.log('culling intervals failed ' + theErrorObject);
});