WITH f_event_feed AS (
SELECT image_step_id::text,
       calculate_slot(i.collection_time, COALESCE(i.collection_interval, interval '60 seconds'))                AS d_time_slot_time,
       cast(:cgr AS TEXT)                                                                                       AS d_cgr,
       COALESCE(r.subject_name, b.subject_name, 'unconfigured')                                                 AS d_subject_name_major,
       COALESCE(r.measure_name, b.subject_name || '/' || i.camera_id || '/' || i.step_name, 'unconfigured')     AS d_subject_name_minor,
       COALESCE(r.extract_type, b.subject_name || '/' || i.camera_id || '/' || i.step_function, 'unconfigured') AS d_report_bar,
       i.step_function                                                                                          AS d_fn_step_function,
       i.step_name                                                                                              AS d_fn_step_name,
       COALESCE(((i.step_output -> 'subjects') ->> 'person'),
                (i.step_output -> 'brake' ->> 'activity'),
                (((i.step_output ->> 'state') || ' ') || (i.step_output ->> 'color')),
                (i.step_output ->> 'position'),
                (i.step_output ->> 'average_color'),
                (i.step_output ->> 'Work_order'),
                (i.step_output ->> 'card_state'),
                (i.step_output ->> 'weight'),
                (i.step_output ->> 'license_plate'))                                                            AS d_state,
       i.collection_time                                                                                        AS f_collection_time,
       (i.step_output ->> 'brightness')                                                                         AS f_brightness,
       NULL                                                                                                     AS f_images,
       CASE WHEN ((i.step_output ->> 'warning') IS NULL) THEN 0 ELSE 1 END                                      AS f_warning,
       b.subject_name                                                                                           AS d_sensor_gateway_display_name,
       i.gateway_id                                                                                             AS d_sensor_gateway_id,
       i.camera_id                                                                                              AS d_sensor_camera_id,
       images
FROM image_step i
    LEFT OUTER JOIN customer_binding b ON (
            i.gateway_id = b.beacon_id AND
            b.subject = 'gateway' AND
            i.collection_time >= b.start_time AND
            (b.end_time IS NULL OR i.collection_time <= b.end_time)
    )
    LEFT OUTER JOIN customer_report_config r ON (
        b.subject_name = r.gateway_name AND
        i.camera_id = r.camera_id AND
        i.step_name = r.step_name AND
        i.collection_time >= r.start_time AND
        (r.end_time IS NULL OR i.collection_time <= r.end_time)
    )
WHERE COALESCE(((i.step_output -> 'subjects') ->> 'person'),
      (i.step_output -> 'brake' ->> 'activity'),
      (((i.step_output ->> 'state') || ' ') || (i.step_output ->> 'color')),
      (i.step_output ->> 'position'),
      (i.step_output ->> 'average_color'),
      (i.step_output ->> 'Work_order'),
      (i.step_output ->> 'card_state'),
      (i.step_output ->> 'weight'),
      (i.step_output ->> 'license_plate')) IS NOT NULL
  AND i.collection_time::timestamp >= :min::timestamp
  AND i.collection_time::timestamp <= :max::timestamp
  AND (:startOffset::bigint IS NULL OR i.image_step_id::bigint >= :startOffset::bigint)
  AND (:endingOffset::bigint IS NULL OR i.image_step_id::bigint <= :endingOffset::bigint)
ORDER BY image_step_id::bigint
LIMIT :limit
)
SELECT * FROM f_event_feed;