
WITH shifts AS (
    SELECT series                                                               AS shift_date,
           shifts.shift_name                                                    AS shift_name,
           series AT TIME ZONE shifts.shift_timezone_name + shifts.start_offset AS shift_start,
           series AT TIME ZONE shifts.shift_timezone_name + shifts.end_offset   AS shift_end,
           shifts.shift_timezone_name                                           AS shift_timezone,
           shifts.facility
    FROM generate_series(:min::date, NOW(), INTERVAL '1 day') series
             JOIN customer_shift shifts ON shifts.day_of_week IS NULL OR
                                           shifts.day_of_week = date_part('dow', series)
),
facilities AS (
    SELECT DISTINCT facility FROM (
        SELECT DISTINCT facility FROM customer_report_config config
        UNION
        SELECT DISTINCT facility FROM customer_binding bindings
        UNION
        SELECT DISTINCT facility FROM customer_shift shifts
    ) all_facilities
),
customer_calendar AS (
    SELECT DISTINCT config.step_name,
                    config.gateway_name,
                    config.subject_name,
                    config.measure_name,
                    facilities.facility,
                    shifts.shift_name,
                    shifts.shift_start,
                    shifts.shift_end,
                    bindings.subject_name = config.gateway_name AND
                    shifts.shift_start >= config.start_time AND
                    shifts.shift_end <= COALESCE(config.end_time, NOW()) AND
                    shifts.shift_start >= bindings.start_time AND
                    shifts.shift_end <= COALESCE(bindings.end_time, NOW()) AS shift_enabled
    FROM facilities facilities
             LEFT OUTER JOIN customer_report_config config
                 ON facilities.facility = config.facility
             LEFT JOIN customer_binding bindings
                 ON facilities.facility = bindings.facility
             LEFT JOIN shifts shifts
                 ON bindings.facility = shifts.facility
    ORDER BY shift_start, facility, subject_name, measure_name
)
SELECT *
  FROM customer_calendar calendar
 WHERE shift_enabled = true
   AND calendar.shift_end <= :max::timestamp
   AND calendar.shift_start >= :min::timestamp