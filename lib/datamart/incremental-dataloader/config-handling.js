const _ = require('lodash');
const debug = require('debug')('portal:config-handling');

function getDataMartConfig(dataMartInstance = getDataMartConfigParameter()) {
  const defaultConfig = require('./datamart-default-config');
  const dmc = require('./datamart-configs');
  //console.log(dmc, dataMartInstance);
  const config = dmc[dataMartInstance];
  //console.log(`config: ${config}`);
  if (!config) throw new Error('datamart-instance is not recognized');
  const cgr = {
    cgrs: [dataMartInstance]
  };
  const finalConfig = _.merge(
    cgr,
    defaultConfig,
    config,
    {
      needsCoalescedEtl: ('NONE' !== config.targetTables.coalescedEvents),
      needsBeaconTrackingEtl: ('NONE' !== config.targetTables.beaconCycles)
    }
  );
  debug(`Final Config: ${JSON.stringify(finalConfig, null, 2)}`);
  return finalConfig;
}

function getDataMartConfigParameter() {
  const env = require('../../app-params');
  return env.requiredAppParam('datamart-instance');
}

module.exports = {
  getDataMartConfig
};
