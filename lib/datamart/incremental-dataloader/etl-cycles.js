require('dotenv').config();
const cluster = require('cluster');
const memored = require('memored');
const dmConf = require('./config-handling');
const xforms = require('./transforms');
const {generateDateSeries} = require('../../utils/date');
const getCycles = require('./cycles-calc');
const config = dmConf.getDataMartConfig();
const dmFacade = require(`./${config.datamartType}-inserter`)(config);
const debug = require('debug')('portal:etl-cycles');
const du = require('../../utils/date');
const parameters = require('../../app-params').optionalAppParam;
const startDate = parameters('start_date') || du.offsetDate(Date.now(), 'day', -2);
const stopDate = parameters('stop_date') || du.offsetDate(Date.now(), 'day', 1);
const { shiftDetails, shiftNames } = require('../../../db/cycle');
const moment = require('moment-timezone');

const TERMINATION_TIMEOUT = 5 * 1000;
function stateEndTimeDescComp(a, b) {
  return a.endTime < b.endTime;
}

async function fetchCycles2(customer, selectedDate, site, shift, plans) {
  const rawData = await getCycles(customer, selectedDate, shift, site);
  for (const plan of plans) {
    const transformFn = plan.fetchFromDbFn(customer, site);
    const transformedData = transformFn(rawData);
    await plan.pushToDatamartFn(transformedData);
    debug('"%s / %s / %s / %s / %s" had %d records sent to output DB.', customer, shift, selectedDate, site, plan.name, transformedData.length);
  }
}

async function runCycles(plans) {
  const out = [];
  for (const cgr of config.cgrsForCycles) {
    const { cgr: customer } = cgr;
    const timeSpan = generateDateSeries(startDate, stopDate);

    for (const site of cgr.facilities) {
      //Initialize last shifts end time
      let previousShiftEnd = null;
      let timezone = null;
      const lisfOfShiftNames = await shiftNames(customer, site);
      debug('Processing shifts', lisfOfShiftNames);

      for (const d of timeSpan) {
        previousShiftEnd = moment(previousShiftEnd).tz(timezone);

        //If next date lies before end  time of last day's shift then skip insert
        const listOfShiftQueries = lisfOfShiftNames.map(
          (shiftName) => shiftDetails(customer, { date: d, shift: shiftName, facility: site })
        );
        const listOfShiftInfo = await Promise.allSettled(listOfShiftQueries).then((results)  => {
          return results.filter(({ status }) => status === 'fulfilled').map(({ value }) => value);
        });
        const shiftInfo = listOfShiftInfo.reduce((elements, element) => elements.concat(element), []);
        shiftInfo.sort();

        if (shiftInfo.length) {
          for (let index = 0; index < shiftInfo.length; index += 1) {
            const shiftInfoObject = shiftInfo[index];
            const { shift_name: shiftName } = shiftInfoObject;

            let timeBetween = moment(shiftInfoObject.end_time).diff(previousShiftEnd, 'seconds');

            if (!previousShiftEnd || Math.abs(timeBetween) >= 360) {
              timezone = shiftInfoObject.shift_timezone_name;
              previousShiftEnd = shiftInfoObject.end_time;
              await fetchCycles2(customer, d, site, shiftName, plans);
              out.push({ cgr, date: d, site });
            } else {
              debug('Skippping shift', previousShiftEnd, JSON.stringify(shiftInfoObject, null, 2));
            }
          }
        }
      }
    }
  }
  return out;
}

async function rebuildCycles() {
  const deleteLog = [];
  process.env.DISABLE_MS = 1;
  process.env.ENABLE_MISSING_DATA = 1;
  const detailedTripInserts = await runCycles([{
    fetchFromDbFn: xforms.createTransformerForAllStatesFn,
    pushToDatamartFn: dmFacade.bulkInsertDetailedCycleRecords,
    name: 'details'
  }, {
    fetchFromDbFn: xforms.createTransformerForOnlyVisitsFn,
    pushToDatamartFn: dmFacade.bulkInsertSummaryCycleRecords,
    name: 'summary'
  }]);
  return {deleteLog, detailedTripInserts};
}

function end(final) {
  debug('GOOD End of line. Final result is %s.', JSON.stringify(final, null, 2));
  process.exit();
}

function die(error) {
  debug('BAD End of line. Final error is %O.', error);
  console.error(error);
  process.exit(-1);
}

async function run() {
  const STOP_SIGNALS = [
    'SIGHUP', 'SIGINT', 'SIGQUIT', 'SIGILL', 'SIGTRAP', 'SIGABRT',
    'SIGBUS', 'SIGFPE', 'SIGUSR1', 'SIGSEGV', 'SIGUSR2', 'SIGTERM'
  ];

  if (cluster.isMaster) {
    memored.setup({ purgeInterval: 10000 });

    const theControlObject = {};
    new Promise((resolve) => Object.assign(theControlObject, { resolve })).then(() => {
      console.log('Exiting process');
      process.exit();
    });

    const worker = cluster.fork();

    worker.on('exit', function(workerObject, code, signal) {
      if (signal) {
        console.log(`worker ${workerObject} was killed by signal: ${signal}`);
      } else if (code) {
        console.log(`worker ${workerObject} exited with error code: ${code}`);
      } else {
        console.log(`worker ${workerObject} exited success!`);
      }
      setTimeout(() => theControlObject.resolve(), TERMINATION_TIMEOUT);
    });

    STOP_SIGNALS.forEach((signal) => {
      process.on(signal, () => {
        console.log(`Got ${signal}, stopping workers...`);
        cluster.disconnect(function() {
          console.log('All workers stopped, exiting.');
          setTimeout(() => theControlObject.resolve(), TERMINATION_TIMEOUT);
        });
      });
    });
    console.log('DONE');
  } else {
    await rebuildCycles().then(end).catch(die);
  }
}

run();

module.exports = {
  stateEndTimeDescComp,
  getDates: generateDateSeries
};
