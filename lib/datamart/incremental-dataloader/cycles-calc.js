const moment = require('moment');
const { computeCycleMetrics } = require('../../../lib/cycles.js');
const cyclesJs = require('../../../db/cycle.js');
const { getCustomerFilterForSite } = require('../../../db/customerLPFilters.js');
const debug = require('debug')('portal:cycles-calc');

module.exports = cycleReport;

function convertSingleStateToCycle(data) {
  data[0].iterations.forEach(iter => {
    for (let i = 0; i < iter.states.length; i++) {
      let nextState = iter.states[i + 1];
      let prevState = iter.states[i];
      if (iter.states.length === 1) {
        prevState.is_end = false;
      }
      if (nextState && prevState) {
        let timeBetween = moment(nextState.startTime).diff(moment(prevState.startTime), 'minutes');
        if (nextState.is_start && nextState.is_end && prevState.state !== 'Exit' && timeBetween < 360) {
          nextState.state = 'Exit';
          nextState.is_start = false;
          prevState.is_end = false;
          if (!data[0].legends.includes('Exit')) {
            data[0].legends.push('Exit');
          }
        }
      } else if (prevState && prevState.is_start && prevState.is_end && prevState.state !== 'Exit') {
        prevState.is_end = false;
      }
    }
  });
  return data;
}

function countTrips(finalCycles) {
  return finalCycles.reduce((summ, facilityObj) => {
    return summ + facilityObj.iterations.length;
  }, 0);
}

async function cycleReport(customer, date, shift, site) {
  if (!(date && moment(date, 'YYYY-MM-DD').isValid())) {
    throw new Error(`Bad date format: ${date}`);
  }

  const parameters = {
    date,
    shift,
    'facility': site,
    'from': null,
    'to': null,
    'qualifier': 'license_plate',
    'tagNames': ['customer_tag', 'qc_tag'],
    'filters': isWhiteListCgr(customer, site) ? ['whitelist'] : []
  };

  const data = await cyclesJs.cycleEventsWithAttributes(customer, parameters);

  let filteredData = data.map((cyclesResponse) => {
    const { facility }=  cyclesResponse;
    const targetSite = facility.toLocaleUpperCase();
    const { iterations } = Object.assign({ iterations: [] }, cyclesResponse);
    debug(`Processing events for site`, targetSite, `Initial iterations size: ${iterations && iterations.length ? iterations.length : 0}`);
    const filtered = iterations.filter((record) => getCustomerFilterForSite(customer, targetSite, record));
    debug(`Processing events for site`, targetSite, `Filtered iterations size: ${iterations && iterations.length ? iterations.length : 0}`);
    return Object.assign({}, cyclesResponse, { iterations: filtered });
  });

  if (filteredData && filteredData.length) {
    filteredData = convertSingleStateToCycle(filteredData);

    //Calculate cycle stats for filtered filteredData
    filteredData[0] = computeCycleMetrics(filteredData[0]);

    filteredData[0].iterations.forEach((x, idx) => {
      x.states[0].isReportStart = true;
      x.states[x.states.length - 1].isReportEnd = true;
      if (!filteredData[0].iterations[idx].d_images) {
        filteredData[0].iterations[idx].d_images = {};
      }
      x.states.forEach((y) => {
        for (const key in y.img_keys) {
          const imgs = y.img_keys[key];
          if (imgs.length) {
            filteredData[0].iterations[idx].d_images[key] = [];
          }
          imgs.forEach(curr => {
            if (curr.name && curr.collection_time) {
              let time = '';
              if (typeof(curr.collection_time) === 'number') {
                time = curr.collection_time;
              } else {
                time = moment(curr.collection_time).unix();
                if (curr.collection_time.split('.').length > 1) {
                  time += '.' + curr.collection_time.split('.')[1].split('+')[0];
                }
              }
              const portalImgLink = `https://atollogy.com/portal/image/${customer}/${curr.gateway_id}/${curr.camera_id}/${time}/${curr.step_name}/${curr.name}`;
              filteredData[0].iterations[idx].d_images[key].push(portalImgLink);
            }
          });
        }
      });
    });

    debug('final cycle count is %d for "%s/%s/%s/%s".', countTrips(filteredData), customer, date, site, shift);
  }

  return filteredData;
}

let isWhiteListCgr = (customer, facility) => facility !== 'Victorville';

