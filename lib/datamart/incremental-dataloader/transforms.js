const _ = require('lodash');

function createTransformerForAllStatesFn(cgr, site) {
  const returnVal =  results => _.flattenDeep(
    (results || []).map(v => v.iterations.map(i => i.states.map(s => ({...i, ...s, cgr, site}))))
  ).map(r => ({
    subject: r.subject,
    visit_count: r.cycle,
    state: r.state,
    weight: r.weight,
    start_time: r.startTime,
    end_time: r.endTime,
    cgr: r.cgr,
    site: r.site,
    is_state_of_cycle: r.is_start,
    is_end_of_cycle: r.is_end,
    d_images: r.d_images
  }));
  return returnVal;
}

function createTransformerForOnlyVisitsFn(cgr, site) {
  return results => _.flattenDeep(
    (results || []).map(v => v.iterations.map(i => (
      {
        ...i, cgr, site,
        startTime: i.states.find(j => j.is_start) ? i.states.find(j => j.is_start).startTime : i.states[0].startTime, //use first startTime if no is_start visit found
        endTime: i.states.find(j => j.is_end) ? i.states.find(j => j.is_end).endTime : i.states[i.states.length - 1].endTime //use last endTIme if no is_end visit found
      }))
    )
  ).map(r => ({
    subject: r.subject,
    start_time: r.startTime,
    end_time: r.endTime,
    cgr: r.cgr,
    visit_count: r.cycle,
    site: r.site,
    d_images: r.d_images
  }));
}

function createTransformerForBeacons(cgr, site) {
  return results => results.map(r => ({
    cgr: cgr,
    facility: site,
    start_time: r.start_time,
    end_time: r.end_time,
    subject: r.subject_id,
    zone: r.state,
    cia_attributes: r.attributes,
    binding_time: r.binding_time,
    binding_image: r.img_keys
  }));
}

module.exports = {
  createTransformerForOnlyVisitsFn,
  createTransformerForAllStatesFn,
  createTransformerForBeacons
};
