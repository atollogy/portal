require('dotenv').config();
const moment = require('moment');
const fs = require('fs');
const path = require('path');
/**  */

const yesql = require('yesql').pg;
const _ = require('lodash');
const cluster = require('cluster');
const memored = require('memored');
const debug = require('debug')('portal:datamart:subjects');
const { AnalyticEvents } = require('./analytics.js');
const { ConnectionPool } = require('../../data-access');
const { optionalAppParam: ReadConfigParameter } = require('../../app-params');

const { generateDateSeries } = require('../../utils/date');
const { fetchSubjects } = require('../../../db/subjects');
const { fetchYardSubjects } = require('../../../db/YardSubjects');
const { fetchSafetySubjects } = require('../../../db/safetySubjects');
const reports = require('../../../db/report-listing.js');
const userManagement = require('../../user-management');
const customerDbs = require('../../../db/');
const dmConf = require('./config-handling');

const config = dmConf.getDataMartConfig();
const { fetchFacilities } = require('./utils');

const dmFacade = require(`./${config.datamartType}-inserter`)(config);

const ITERATIONS = 12;
async function thinkTime(min = 200, max = 400) {
  let delta = max - min;
  let totalTime = min * min * ITERATIONS;
  for (let index = 0; index < ITERATIONS; index +=1) {
    const value = 0.5 * (delta + Math.random() * delta);
    totalTime += value * value ;
  }
  totalTime = Math.ceil(Math.sqrt(totalTime / ITERATIONS));
  return totalTime;
}

const MissingDataEvent = Object.freeze({
  d_subject_name_major: 'missing-data',
  d_subject_name_minor: 'missing-data',
  d_report_bar: 'missing-data',
  d_fn_step_function: 'missing-data',
  d_fn_step_name: 'missing-data',
  d_state: 'missing-data',
  f_brightness: null,
  f_images: null,
  f_warning: 0,
  d_sensor_gateway_display_name: null,
  d_sensor_gateway_id: '0bad0bad0bad',
  d_sensor_camera_id: 'video0',
});

const DEFAULT_QUEUE_SIZE = 1;
const TERMINATION_TIMEOUT = 5 * 1000;

const BatchRecordSize = 20000;

const deleteSummaries = yesql(`
  DELETE FROM ${config.targetTables.summarizedCycles}
  WHERE cgr = :cgr AND date_trunc('day', start_time) BETWEEN :firstDayToDelete AND :lastDayToDelete`);

const deleteDetailed = yesql(`
  DELETE FROM ${config.targetTables.detailedCycles}
  WHERE cgr = :cgr AND date_trunc('day', start_time) BETWEEN :firstDayToDelete AND :lastDayToDelete`);

const deleteBeacons = yesql(`
  DELETE FROM ${config.targetTables.beaconCycles}
  WHERE cgr = :cgr AND date_trunc('day', start_time) BETWEEN :firstDayToDelete AND :lastDayToDelete`);

module.exports.removeCycleRecords = async function removeCycleRecords(listOfDates, cgr) {
  const firstDayToDelete = listOfDates[0];
  const lastDayToDelete = listOfDates[listOfDates.length - 1];
  const parameters = {cgr, firstDayToDelete, lastDayToDelete};
  const datamart = ConnectionPool.findOrCreate(config.connInfo);
  const deleteSummaryStatement = deleteSummaries(parameters);
  const deleteDetailsStatement = deleteDetailed(parameters);

  const { rowCount: summaryCount } = await datamart.query(
    (deleteSummaryStatement.sql || deleteSummaryStatement.text), deleteSummaryStatement.values);
  const { rowCount: detailedCount } = await datamart.query(
    (deleteDetailsStatement.sql || deleteDetailsStatement.text), deleteDetailsStatement.values);

    return {cgr, firstDayToDelete, lastDayToDelete, summaryCount, detailedCount};
};

module.exports.removeBeaconRecords = async function removeBeaconRecords(listOfDates, cgr) {
  try {
    const firstDayToDelete = listOfDates[0];
    const lastDayToDelete = listOfDates[listOfDates.length - 1];
    const parameters = {cgr, firstDayToDelete, lastDayToDelete};
    const datamart = ConnectionPool.findOrCreate(config.connInfo);
    const deleteBeaconsStatement = deleteBeacons(parameters);
    const {rowCount: count} = await datamart.query((deleteBeaconsStatement.sql || deleteBeaconsStatement.text), deleteBeaconsStatement.values);
    return {cgr, firstDayToDelete, lastDayToDelete, count};
  } catch (error) {
    debug(`removeBeaconRecords error: ${error}`);
  }
};

function convertToIntervalRow(cgr, measureObj, measureName, subjectObj, reportObj) {
  return {
    cgr,
    facility: subjectObj.facility,
    shift_name: reportObj.shiftName,
    subject_name: subjectObj.name,
    measure_name: measureName,
    measure_state: measureObj.state,
    measure_start_time: measureObj.startTime,
    measure_end_time: measureObj.endTime,
    d_images: measureObj.d_images
  };
}

function transformSubjectDataForDatamart(cgr, theReportObject) {
  const { subjects: theListOfSubjects } = Object.assign({ subjects: [] }, theReportObject);
  const listOfIntervals = _.flattenDeep(theListOfSubjects.map(theSubjectObject => {
    return Object.keys(theSubjectObject.measures).map(
      theMeasureName => theSubjectObject.measures[theMeasureName].map(mObj => convertToIntervalRow(cgr, mObj, theMeasureName, theSubjectObject, theReportObject)));
  }), 2);

  if (listOfIntervals && listOfIntervals.length) {
    const theListOfIntervals = _.sortBy(listOfIntervals, ['measure_name', 'subject_name', 'measure_start_time','measure_end_time']);

    const { shift_name: shift, facility, cgr: customer } = theListOfIntervals[0];
    const { measure_start_time } = _.minBy(listOfIntervals, 'measure_start_time')
    const startOfInterval = moment(measure_start_time).format('YYYY-MM-DD HH:mm:ss');

    const { measure_end_time } = _.maxBy(listOfIntervals, 'measure_end_time');
    const endOfInterval = moment(measure_end_time).format('YYYY-MM-DD HH:mm:ss')

    debug(`Interval Dataset for atollogy:interval::${customer}/${facility}/${shift} from: "${startOfInterval}" to: "${endOfInterval}"`);
  }
  return listOfIntervals;
}

async function getCoalescedSubjectRecordsByPlan(thePlanDataObject, writeFn, onProcessEventBatch) {
  try {
    const theCoalescedSubjectsBatch = await getCoalescedSubjectRecords(
      thePlanDataObject.cgr,
      thePlanDataObject.shiftName,
      thePlanDataObject.shiftDate,
      thePlanDataObject.startOfShift,
      thePlanDataObject.endOfShift,
      thePlanDataObject.facility
    );

    thePlanDataObject.rowCount = -1;
    await writeFn(transformSubjectDataForDatamart(thePlanDataObject.cgr, theCoalescedSubjectsBatch));
    if (theCoalescedSubjectsBatch && theCoalescedSubjectsBatch.subjects && theCoalescedSubjectsBatch.subjects.length) {
      const countOfElements = theCoalescedSubjectsBatch.subjects.reduce((currentCount, subjectElement) => {
        return currentCount + Object.values(subjectElement.measures).reduce(
          (measureCounter, measureGroup) => measureCounter + measureGroup.length, 0
        );
      }, 0);
      thePlanDataObject.rowCount = countOfElements;
      debug(`---- Processed ${countOfElements} elements for ${thePlanDataObject.shiftName}`);
    } else {
      /*  */
      const sql = readQueryStatementOnce('f_empty_shifts.sql');
      const parameters = Object.assign({
        date: thePlanDataObject.shiftDate,
        facility: thePlanDataObject.facility
      });
      const statement = yesql(`
        SELECT DISTINCT subject_name, measure_name
          FROM customer_report_config
         WHERE start_time >=  :date
           AND (facility is NULL OR facility = :facility)
      `)({
        date: thePlanDataObject.shiftDate,
        facility: thePlanDataObject.facility
      });
      const { rows } = await customerDbs.query(thePlanDataObject.cgr, statement.text, statement.values);

      const listOfIntervals = rows.map((theMeasureObject) => Object.assign({
        cgr: thePlanDataObject.cgr,
        facility: thePlanDataObject.facility,
        shift_name: thePlanDataObject.shiftName,
        subject_name: theMeasureObject.subject_name,
        measure_name: theMeasureObject.measure_name,
        measure_state: '[BAD] Missing Data [CONFIG]',
        measure_start_time: thePlanDataObject.startOfShift,
        measure_end_time: thePlanDataObject.endOfShift,
      }));

      writeFn(listOfIntervals);
      debug(`---- [BAD] Processed 0x00000000 elements for ${thePlanDataObject.shiftName}, ${JSON.stringify(listOfIntervals, null, 2)}`);
    }

    const theOffsetObject = await reports.fetchImageInterval(
      thePlanDataObject.cgr, thePlanDataObject.startOfShift, thePlanDataObject.endOfShift);
    debug(`----> Processing events offset ${JSON.stringify(theOffsetObject)}`);
    await onProcessEventBatch(thePlanDataObject, theOffsetObject);
  } catch (cause) {
    if (cause.message !== 'Cannot read property \'subjects\' of null') {
      console.error(`Eating error for coalesced top off of: ${JSON.stringify(thePlanDataObject)}`, cause);
    }
  }
}

const TIME_OFFSET = 8 * 60 * 60 * 1000;
const newDateProvider = () => {
  const theCurrentTime = Date.now();
  const theCurrentTImeoutWithoutMillis = 1000 * Math.round(theCurrentTime / 1000);
  const theCurrentDate = new Date(theCurrentTImeoutWithoutMillis - TIME_OFFSET);
  return () => theCurrentDate.toISOString().substring(0, 10);
};

const newTargetDateProvider = () => {
  const getCurrentDate = newDateProvider();
  const theProcessConfigDate = ReadConfigParameter('end_date') || undefined;
  return () => theProcessConfigDate || getCurrentDate();
};

const getTargetDate = newTargetDateProvider();

async function handleAllNeededCoalescedSubjectRecords(configObject, writeFn, onProcessEventBatch) {
  const theTargetDate = getTargetDate();
  const { cgr: customer, max: theStartDate } = Object.assign({}, configObject);
  const theFinishDate = moment(theTargetDate).format('YYYY-MM-DD');
  const theBeginDate = moment(theStartDate).format('YYYY-MM-DD');
  const neededDates = generateDateSeries(theBeginDate, theFinishDate);
  debug(`==================================== END_DATE ${theTargetDate} ====================================`);

  const listOfPlanPromises = [];
  const facilities = await fetchFacilities(customer);
  const listOfFacilities = facilities.map(({ facility }) => facility);
  for (const dateOfInterest of neededDates) {
    for (const facility of listOfFacilities) {
      const theDateFacilityPromise = new Promise(async (resolve) => {
        try {
          const shiftsForDay = await reports.getShifts(customer, dateOfInterest, facility);
          const plansForDay = shiftsForDay.map(shift => Object.assign({
            cgr: customer,
            shiftName: shift.name,
            shiftDate: dateOfInterest,
            startOfShift: shift.start,
            endOfShift: shift.end,
            facility
          }));
          resolve(plansForDay);
        } catch(cause) {
          if (cause.message !== 'Cannot read property \'subjects\' of null') {
            console.error(`Eating error for coalesced top off of: ${customer}::${dateOfInterest}::${facility}`, cause);
          }
          resolve([])
        }
      });

      listOfPlanPromises.push(theDateFacilityPromise);
    }
  }

  const thePlansPerDay = await Promise.allSettled(listOfPlanPromises).then((values) => {
    return values.filter(result => result.status === 'fulfilled').map(result => result.value);
  });

  const plans = thePlansPerDay.reduce((listOfPlans, elements) => listOfPlans.concat(elements), []);

  const promisesQueues = [];
  const globalStartTime = Date.now();
  const targetGroupSize = Number(process.env.QUEUE_SIZE || DEFAULT_QUEUE_SIZE);
  for(let index = 0; index < targetGroupSize; index += 1) {
    const theMessageContent = `====> Starting execution of TASK:GLOBAL ${index} at ${globalStartTime}`;
    promisesQueues.push(Promise.resolve(theMessageContent));
  }

  plans.forEach((thePlanObject, index) => {
    const queueIndex = index % targetGroupSize;
    const promiseQueue = promisesQueues[queueIndex];

    promisesQueues[queueIndex] = promiseQueue.then((theMessage) => new Promise(async (resolve, reject) => {
      debug(theMessage);
      const localStartTime = Date.now();
      try {
        await getCoalescedSubjectRecordsByPlan(thePlanObject, writeFn, onProcessEventBatch);
        resolve(`====> Execution completed for TASK ${index}`);
      } catch(cause) {
        reject(cause);
      } finally {
        const localCurrentTime = Date.now();
        debug(`Execution complete in ${localCurrentTime - localStartTime}Ms (Running: ${localCurrentTime - globalStartTime})`);
      }
    }));
  });

  debug('Waiting for tasks to complete...');
  await Promise.allSettled(promisesQueues);

  return plans;
}

const SubjectReportTypes = Object.freeze({
  'Standard': {
    matcher: (pages) => (pages.some(page => /^Subject\s(Detail|Summary)$/.test(page))),
    fetch: fetchSubjects,
    overrides: { violation: false }
  },
  'WashBay': {
    matcher: (pages) => (pages.some(page => /^Wash\sBay\s(Detail|Summary)$/.test(page))),
    fetch: fetchYardSubjects,
    overrides: { violation: false }
  },
  'SafeRack': {
    matcher: (pages) => (pages.some(page => /^Yard\sSubject\s(Detail|Summary)$/.test(page))),
    fetch: fetchYardSubjects,
    overrides: { violation: false }
  },
  'Safety': {
    matcher: (pages) => (pages.some(page => /^Violation\s(Detail|Summary)$/.test(page))),
    fetch: fetchSafetySubjects,
    overrides: { violation: true }
  }
});

let reportTypesCache = {};
async function getCustomerReportTypes(customer, facility) {
  const theCacheKey = `atollogy:reports:${customer}::${facility}`;

  if (Object.prototype.hasOwnProperty.call(reportTypesCache, theCacheKey)) {
    /** Nothing to do, just use the cached version */
  } else {
    const navigation = await userManagement.fetchNav();
    const cgrNavs = navigation['cgrNavs'];
    const theNavigation = Object.assign({ }, cgrNavs[customer], cgrNavs[customer][facility]);
    const pages = Object.keys(theNavigation).map((component) => {
      const theNavigationComponent = Object.assign({ subject: 'Subject Detail' }, theNavigation[component]);
      return theNavigationComponent['label'];
    }).filter(Boolean);
    const reportTypes = Object.keys(SubjectReportTypes).reduce((elements, reportType) => {
      const { matcher, fetch, overrides } = SubjectReportTypes[reportType];
      if (matcher(pages)) {
        const reportProcesssor = Object.freeze({
          fetch, overrides, type: reportType.toLowerCase()
        });
        return elements.concat(reportProcesssor);
      }
      return elements;
    }, []);

    reportTypesCache[theCacheKey] = reportTypes;
  }

  return reportTypesCache[theCacheKey];
}

async function getCoalescedSubjectRecords(cgr, theShiftName, theShiftDate, startOfShift, endOfShift, facility) {
  const reportTypes = await getCustomerReportTypes(cgr, facility);
  const theSubjectParameters = {
    'shift': theShiftName,
    'date': theShiftDate,
    'previous': false,
    'from': null,
    'to': null,
    'byClass': false,
    'facility': facility
  };

  const theSegmentParameters = Object.freeze({
    'from': startOfShift.toISOString(),
    'to': endOfShift.toISOString()
  });

  // Object.assign(theSubjectParameters, theSegmentParameters);

  const groupOfSubjects = [];
  debug(`---- Processing with ${JSON.stringify(theSegmentParameters, null, 2)}`);
  for (let index = 0; index < reportTypes.length; index += 1) {
    const theSelectedDataSourceConfig = reportTypes[index];
    const { type, fetch: theDataSourceProvider, overrides } = theSelectedDataSourceConfig;

    const fSubjectDataSource = async (customer, parameters) => {
      let listOfSubjects = {};
      const startTime = Date.now();
  
      try {
        listOfSubjects = await theDataSourceProvider(customer, Object.assign({}, parameters, overrides));
      } finally {
        const responseTime = Date.now() - startTime;
        debug(`>>[${type}]>> Executed ${theDataSourceProvider.name} in ${responseTime}Ms.`);
      }
  
      return listOfSubjects
    };  

    const theListOfSubjects =  await fSubjectDataSource(cgr, theSubjectParameters);
    groupOfSubjects.push(theListOfSubjects);
  }

  const listOfCoalescedSubject = groupOfSubjects.reduce((responseObject, subjectResponse) => {
    if (responseObject) {
      const { subjects: oldSubjects } = responseObject;
      const { subjects: newSubjects } = subjectResponse;
      responseObject.subject = [].concat(oldSubjects).concat(newSubjects)
    } else {
      responseObject = subjectResponse;
    }
    return responseObject;
  }, undefined);

  return Object.assign({ subject: [] }, listOfCoalescedSubject)
}

const readQueryStatementOnce = (function () {
  const cacheOfFileContent = {}

  return function (theFileName) {
    if  (Object.prototype.hasOwnProperty.call(cacheOfFileContent, theFileName)) {
      return cacheOfFileContent[theFileName];
    }
    const theQueryString = fs.readFileSync(path.join(__dirname, 'sql', theFileName), 'utf8')
    Object.assign(cacheOfFileContent, { [theFileName]: theQueryString });

    return theQueryString
  }
}());

async function getEmptyShiftRecords(configObject, offsetObject) {
  const { cgr: customer } = configObject;
  const sql = readQueryStatementOnce('f_empty_shifts.sql');
  const parameters = Object.assign({}, { ...configObject }, { ...offsetObject });
  const theQueryContext = yesql(sql)(parameters);

  const {rows} = await customerDbs.query(customer, (theQueryContext.sql || theQueryContext.text), theQueryContext.values);

  const records = rows.reduce((listOfEvents, {
    step_name,
    gateway_name,
    subject_name,
    measure_name,
    shift_start,
    shift_end
  }) => {
    const boundaries = [shift_start, shift_end];

    const events = boundaries.map((collection_time, index) => {
      const eventObject = { ...MissingDataEvent };

      const suffix = index === 0 ? 'start' : 'end';
      Object.assign(eventObject, {
        d_cgr: customer,
        d_fn_step_name: step_name,
        d_report_bar: `missing-data`,
        d_time_slot_time: collection_time,
        f_collection_time: collection_time,
        d_subject_name_minor: measure_name,
        d_subject_name_major: subject_name,
        d_fn_step_function: `missing-data-${suffix}`,
        d_sensor_gateway_display_name: gateway_name,
      });

      return Object.freeze(eventObject);
    });

    return listOfEvents.concat(events);
  }, /** */ []);
  debug(`========================= // [EMPTY SHIFT] ${JSON.stringify({ records }, null, 2)} // =========================`);

  return records;
}

async function getNewRawEventRecords(configObject, offsetObject) {
  const { cgr: customer } = configObject;
  const sql = readQueryStatementOnce('f_event_feed.sql');
  const parameters = Object.assign({}, { ...configObject }, { ...offsetObject }, { limit: BatchRecordSize });
  const theQueryContext = yesql(sql)(parameters);

  const {rows} = await customerDbs.query(customer, (theQueryContext.sql || theQueryContext.text), theQueryContext.values);
  for (let i=0; i < rows.length; ++i ) {
    if (rows[i].images && rows[i].images.length) {
      const row = rows[i];
      rows[i].d_images = [];
      row.images.forEach(image => {
        const splits = image.split('/');
        const lastChunk = splits[splits.length - 1].split('_');
        const imgName = lastChunk[lastChunk.length - 1];
        const time = lastChunk[2];
        const imgStr = `https://atollogy.com/portal/image/${row.d_cgr}/${row.d_sensor_gateway_id}/${row.d_sensor_camera_id}/${time}/${row.d_fn_step_name}/${imgName}`;
        rows[i].d_images.push(imgStr);
      });
      delete rows[i].images;
    }
  }
  return rows;
}

function end(countOfRecords) {
  debug(`GOOD End of line. This cycle wrote ${countOfRecords} records.`);
  process.exit();
}

function die(cause) {
  debug('BAD End of line. Final error is %O.', cause.message || '');
  console.error(cause);
  process.exit(-1);
}

function MakeQuerablePromise(thePromiseObject) {
  // Don't modify any promise that has been already modified.
  if (Object.prototype.hasOwnProperty.call(thePromiseObject, 'isResolved')) {
    return thePromiseObject;
  }

  // Set initial state
  let isPending = true;
  let isRejected = false;
  let isFulfilled = false;

  // Observe the promise, saving the fulfillment in a closure scope.
  const theQuerablePromise = thePromiseObject.then(
    function onPromiseResolved(theResponseObject) {
      isFulfilled = true;
      isPending = false;
      return theResponseObject;
    },
    function onPromiseRejected(theErrorObject) {
      isRejected = true;
      isPending = false;
      throw theErrorObject;
    }
  );

  theQuerablePromise.isFulfilled = function() {
    return isFulfilled;
  };

  theQuerablePromise.isPending = function() {
    return isPending;
  };

  theQuerablePromise.isRejected = function() {
    return isRejected;
  };

  return theQuerablePromise;
}

async function processSubjectEventsForPlan(cgr, customer, planObject, offsetObject) {
  const { startOfShift, endOfShift } = planObject;

  let countOfRecords = 0;
  let recordsToAdd = null;
  let insertionPromises = [];
  let hasMoreRecords = false;
  let isFirstQueryForShift = true;

  const configObject = Object.freeze({
    cgr: cgr,
    max: `${moment(endOfShift).format('YYYY-MM-DD HH:mm:ss')}Z`,
    min: `${moment(startOfShift).format('YYYY-MM-DD HH:mm:ss')}Z`
  });
  const configOptions = {};
  const theOffsetObject = { ...offsetObject };

  do {
    // Keep adding f_events records until we get at most BatchRecordSize
    const theConfigObject =  Object.assign({}, configObject, configOptions);
    debug(`========================= // [FETCH] ${JSON.stringify(theConfigObject)} // =========================`);
    recordsToAdd = await getNewRawEventRecords(theConfigObject, theOffsetObject);

    if (isFirstQueryForShift  && recordsToAdd.length  ===  0) {
      recordsToAdd = await getEmptyShiftRecords(theConfigObject, theOffsetObject);
    }
    isFirstQueryForShift = false;

    /**
     * Store the current date that it's processing to continute
     * checking until we reach the end of the interval
     * */
    hasMoreRecords = recordsToAdd && recordsToAdd.length >= BatchRecordSize;

    // query now returns events sorted by image_step_id. re-sorting by time
    debug(`========================= // [SORT] ${JSON.stringify(theConfigObject)} // =========================`);
    recordsToAdd = _.sortBy(recordsToAdd, 'f_collection_time');

    /* notifies we made some progress */
    if (recordsToAdd && recordsToAdd.length) {
      const latestImageStep = {};
      Object.assign(latestImageStep, _.maxBy(recordsToAdd, 'f_collection_time'));
      const theStartOffset = Math.max(configOptions.min, latestImageStep.f_collection_time);

      Object.assign(latestImageStep, _.maxBy(recordsToAdd, 'image_step_id'));
      const theLastImageStepID = latestImageStep.image_step_id;
      debug(`========================= // Last Image Step ID: ${theLastImageStepID} // =========================`);

      if (theLastImageStepID > theOffsetObject.startOffset) {
        Object.assign(theOffsetObject, { startOffset: theLastImageStepID });
      } else {
        Object.assign(configOptions, { min: moment(theStartOffset).format('YYYY-MM-DD HH:mm:ss') });
      }

      AnalyticEvents.emit('progress', {
        customer: customer,
        recordsLength: recordsToAdd.length,
        latest: latestImageStep,
      });

      // remove extra image_step_id column
      recordsToAdd = _.map(recordsToAdd, object => _.omit(object, 'image_step_id'));
      insertionPromises.push(MakeQuerablePromise(
        dmFacade.bulkInsertRawEventRecords(config.targetTables.rawEvents, recordsToAdd)
      ));

      debug('Submitted %d records to "%s" (Batch Record Size: %s).', recordsToAdd.length, cgr, BatchRecordSize);
      countOfRecords += recordsToAdd.length;

      const countOfPromises = insertionPromises.length;
      insertionPromises = insertionPromises.filter((promise) => promise.isPending());
      const countOfPendingPromises = insertionPromises.length;
      debug('Number of pending promises %d (of %d total promises)', countOfPendingPromises, countOfPromises);
    }

    await new Promise((resolve) => setTimeout(resolve.bind(planObject, theOffsetObject), thinkTime()));

  } while (hasMoreRecords);

  insertionPromises = insertionPromises.filter((promise) => promise.isPending());
  if (insertionPromises.length) {
    debug(`Waiting for ${insertionPromises.length} to complete...`);
    await Promise.allSettled(insertionPromises);
  }
  debug(`Done writing ${JSON.stringify(planObject, null, 2)} with offset ${JSON.stringify(offsetObject, null, 2)}`);

  return Object.freeze({
    countOfRecords: countOfRecords,
    configObject: Object.assign({}, planObject, configObject)
  });
}

async function copyNewRecordsBetweenDatabases() {
  let countOfRecords = 0;
  try {
    AnalyticEvents.emit('start');

    const organization = Array.isArray(config.cgrs) ? config.cgrs[0] : config.cgrs;
    const listOfApplicableCGRs = Array.isArray(config.cgrsForCycles) && config.cgrsForCycles.length
      ? config.cgrsForCycles
      : (Array.isArray(config.cgrs) ? config.cgrs.map((customer) => ({ cgr: customer })) : [ { cgr: config.cgrs } ]);

    for (const configOptions of listOfApplicableCGRs) {
      const cgr = configOptions['cgr'];
      const customer = organization === cgr ? organization : `${organization}:${cgr}`

      const newHighs = await dmFacade.fetchHighWaterMarks(cgr);
      if (newHighs && newHighs.length) {
        const configObject = Object.assign({}, newHighs[0], { cgr });
        const doBulkSubjectRecords = dmFacade.bulkInsertCoalescedSubjectRecords;

        const onSubjectRecords = async (planObject, offsetObject) => {
          const {
            configObject: theConfigObject,
            countOfRecords: partialCountOfRecords
          } = await processSubjectEventsForPlan(cgr, customer, planObject, offsetObject);

          countOfRecords += partialCountOfRecords;
        };

        await handleAllNeededCoalescedSubjectRecords(configObject, doBulkSubjectRecords, onSubjectRecords);
      }
    }
  } finally {
    AnalyticEvents.emit('complete');
  }

  return countOfRecords;
}

async function runIncremental() {
  process.env.DISABLE_MS = 1;
  process.env.ENABLE_MISSING_DATA = 1;
  AnalyticEvents.bindCustomerEvents();
  debug('datamart-top-off-raw-events', new Date());
  return  await copyNewRecordsBetweenDatabases();
}

async function run () {
  const STOP_SIGNALS = [
    "SIGHUP", "SIGINT", "SIGQUIT", "SIGILL", "SIGTRAP", "SIGABRT",
    "SIGBUS", "SIGFPE", "SIGUSR1", "SIGSEGV", "SIGUSR2", "SIGTERM"
  ];

  if (cluster.isMaster) {
    memored.setup({ purgeInterval: 10000 });

    const theControlObject = {};
    new Promise((resolve) => Object.assign(theControlObject, { resolve })).then(() => {
      debug('Exiting process');
      process.exit();
    });

    const worker = cluster.fork();

    worker.on('exit', function(workerObject, code, signal) {
      if (signal) {
        debug(`worker ${workerObject} was killed by signal: ${signal}`);
      } else if (code) {
        debug(`worker ${workerObject} exited with error code: ${code}`);
      } else {
        debug(`worker ${workerObject} exited success!`);
      }
      setTimeout(() => theControlObject.resolve(), TERMINATION_TIMEOUT);
    });

    STOP_SIGNALS.forEach((signal) => {
			process.on(signal, () => {
				debug(`Got ${signal}, stopping workers...`);
        cluster.disconnect(function() {
					debug("All workers stopped, exiting.");
					setTimeout(() => theControlObject.resolve(), TERMINATION_TIMEOUT);
				});
			});
    });
  } else {
    await runIncremental().then(end).catch((cause) => die(cause));
  }
};

run();