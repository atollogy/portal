require('dotenv').config();
const dmConf = require('./config-handling');
const xforms = require('./transforms');
const {generateDateSeries} = require('../../utils/date');
const {fetchFacilities} = require('./utils');
const getBeaconCycles = require('./beacon-cycles');
const cycleDb = require('../incremental-dataloader');
const config = dmConf.getDataMartConfig();
const dmFacade = require(`./${config.datamartType}-inserter`)(config);
const debug = require('debug')('portal:etl-beacons');
const du = require('../../utils/date');
const parameters = require('../../app-params').optionalAppParam;
const startDate = parameters('start_date') || du.offsetDate(Date.now(), 'day', -2);
const stopDate = parameters('stop_date') || du.formatDateToStr(Date.now());
const {shiftDetails} = require('../../../db/cycle');
const moment = require('moment');

function stateEndTimeDescComp(a, b) {
  return a.endTime < b.endTime;
}

async function fetchBeaconCycles(cgr, d, site, qualifier, plans) {
  const shiftFacility = await fetchFacilities(cgr.cgr);
  const shifts = shiftFacility.filter(s => s.facility === site);
  for (const {shift_name} of shifts) {
    const rawData = await getBeaconCycles(cgr.cgr, d, shift_name, site, qualifier);
    for (const plan of plans) {
      const transformFn = plan.fetchFromDbFn(cgr.cgr, site);
      const transformedData = transformFn(rawData);
      await plan.pushToDatamartFn(transformedData);
      debug('"%s / %s / %s / %s" had %d records sent to output DB.', cgr.cgr, d, site, plan.name, transformedData.length);
    }
  }
}

async function runBeacons(plans) {
  const out = [];
  for (const cgr of config.cgrsForBeacons) {
    const timeSpan = generateDateSeries(startDate, stopDate);

    //Initialize last shifts end time
    let previousShiftEnd = null;
    for (const site of cgr.facilities) {
      for (const d of timeSpan) {
        //If next date lies before end time of last day's shift then skip insert
        const shiftInfo = await shiftDetails(cgr.cgr, {date: d, shift: 'day', facility: site});
        if (!shiftInfo.length || !previousShiftEnd || moment(d).isAfter(moment(previousShiftEnd))) {
          previousShiftEnd = shiftInfo[0] && shiftInfo[0].end_time;
          await fetchBeaconCycles(cgr, d, site, cgr.qualifier, plans);
          out.push({cgr, date: d, site});
        }
      }
    }
  }
  return out;
}

async function rebuildBeacons() {
  const deleteLog = [];
  for (const cgr of config.cgrsForBeacons) {
    deleteLog.push(await cycleDb.removeBeaconRecords([startDate, stopDate], cgr.cgr));
  }
  const beaconInserts = await runBeacons([{
    fetchFromDbFn: xforms.createTransformerForBeacons,
    pushToDatamartFn: dmFacade.bulkInsertBeaconCycleRecords,
    name: 'beacons'
  }]);
  return {deleteLog, beaconInserts};
}

function end(final) {
  debug('GOOD End of line. Final result is %s.', JSON.stringify(final, null, 2));
  process.exit();
}

function die(error) {
  debug('BAD End of line. Final error is %O.', error);
  console.error(error);
  process.exit(-1);
}

rebuildBeacons()
  .then(end)
  .catch(die);

module.exports = {
  stateEndTimeDescComp,
  getDates: generateDateSeries
};
