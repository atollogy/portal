const fs = require('fs');
const path = require('path');

let cfg_path = path.join(__dirname, 'customer_cfgs');

const dmcExports = {
  datamart: {
    connInfo: {
      user: 'atollogy',
      ro_host: 'analytics.ccbxi26o2tim.us-west-2.rds.amazonaws.com',
      host: 'analytics.ccbxi26o2tim.us-west-2.rds.amazonaws.com',
      password: 'BobCrapKnowsNothing470',
      port: 5432,
      database: 'datamart'
    },
    targetTables: {
      rawEvents: 'f_events',
      summarizedCycles: 'f_cycles_summarized',
      detailedCycles: 'f_cycles_detailed',
      coalescedEvents: 'f_intervals',
      beaconCycles: 'f_beacon_intervals'
    },
    datamartType: 'pg',
    modes: {
      projection: {
        projections: {
          mom: ['subjects', 'beacons'],
          yom: ['subjects', 'cycles']
        },
        queryMap: {
          mom: [],
          yom: []
        }
      },
      regenerate: {
        projections: {
          mom: ['subjects', 'beacons'],
          yom: ['subjects', 'cycles']
        },
        queryMap: {
          mom: [
            ['delete_sync_markers', ['datamart_sync'], 'system'],
            ['delete_events', ['f_events'], 'datamart'],
            ['delete_intervals', ['f_intervals'], 'datamart']
          ],
          yom: [
            ['delete_sync_markers', ['datamart_sync'], 'system'],
            ['delete_cycle_steps', ['f_cycles_detailed'], 'datamart'],
            ['delete_cycles', ['f_cycles_summarized'], 'datamart']
          ]
        }
      },
      system_query: {
        projections: {
          mom: [],
          yom: []
        },
        queryMap: {
          clear_markers: ['delete_sync_markers', ['datamart_sync'], 'system']
        }
      },
      top_off: {
        projections: {
          mom: ['subjects', 'beacons'],
          yom: ['subjects', 'cycles']
        },
        queryMap: {
          mom: [],
          yom: []
        }
      }
    },
    projections: {
      beacons: {
        module: 'lib/datamart/incremental-dataloader/etl-beacons.js'
      },
      cycles: {
        module: 'lib/datamart/incremental-dataloader/etl-cycles.js'
      },
      subjects: {
        module: 'lib/datamart/incremental-dataloader/index.js'
      }
    },
    tables: {
      datamart_sync: {
        ttype: 'system',
        knownby: ['datamart_sync', 'watermarks', 'sync'],
        sql: {
          delete_sync_markers: (p) => `DELETE FROM datamart_sync WHERE customer = '${p.cgr}' AND sync_event_time BETWEEN '${p.startTime}' AND '${p.endTime}';`
        }
      },
      f_beacon_intervals: {
        ttype: 'datamart',
        knownby: ['f_beacon_intervals', 'beaconCycles'],
        sql: {
          delete_intervals: (p) => `DELETE FROM f_beacon_intervals WHERE f_collection_time BETWEEN '${p.startTime}' AND '${p.endTime}';`
        }
      },
      f_cycles_detailed: {
        ttype: 'datamart',
        knownby: ['f_cycles_detailed', 'detailedCycles'],
        sql: {
          delete_cycle_steps: (p) => `DELETE FROM f_cycles_detailed WHERE start_time BETWEEN '${p.startTime}' AND '${p.endTime}';`
        }
      },
      f_cycles_summarized: {
        ttype: 'datamart',
        knownby: ['f_cycles_summarized', 'summarizedCycles'],
        sql: {
          delete_cycles: (p) => `DELETE FROM f_cycles_summarized WHERE start_time BETWEEN '${p.startTime}' AND '${p.endTime}';`
        }
      },
      f_events: {
        ttype: 'datamart',
        knownby: ['f_events','rawEvents'],
        sql: {
          delete_events: (p) => `DELETE FROM f_events WHERE f_collection_time  BETWEEN '${p.startTime}' AND '${p.endTime}';`
        }
      },
      f_intervals: {
        ttype: 'datamart',
        knownby: ['f_intervals','coalescedEvents'],
        sql: {
          delete_intervals: (p) => `DELETE FROM f_intervals WHERE measure_start_time  BETWEEN '${p.startTime}' AND '${p.endTime}';`
        }
      }
    }
  }
};

fs.readdirSync(cfg_path).forEach(function(item) {
  if (item.includes('json') && ! item.includes('archive')) {
    dmcExports[item.split('.')[0]] = JSON.parse(fs.readFileSync(`${cfg_path}/${item}`));
  }
});
//console.log(JSON.stringify(dmcExports, null, 2));

module.exports = dmcExports;
