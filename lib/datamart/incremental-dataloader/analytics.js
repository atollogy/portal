'use strict';
const debug = require('debug')('portal:datamart:analytics');

const newAnalyticsModule = function () {
  const util = require('util');
  const EventEmitter = require('events');
  const {SyncObject} = require('./sync-object.js');
  const {SyncDataAccess} = require('./sync-data-access.js');

  /**
   * Create a new instance of the analytics event
   * @constructor
   */
  const AnalyticsObject = function () {
    EventEmitter.call(this);
    Object.defineProperties(this, {
      connections: { value: {}, enumerable: false, writable: true }
    });
  };

  util.inherits(AnalyticsObject, EventEmitter);

  /**
   * Execute each of the listeners in order with the supplied arguments.
   * Returns true if the event had listeners, false otherwise.
   * This method is actually a facade to the 'events' module and it depends on it.
   * @param payload
   */
  AnalyticsObject.prototype.notifyStarted = function (payload) {
    this.emit('start', preparePayload(payload));
  };

  AnalyticsObject.prototype.notifyCompleted = function (payload) {
    this.emit('completed', preparePayload(payload));
  };

  AnalyticsObject.prototype.notifyProgress = function (payload) {
    this.emit('progress', preparePayload(payload));
  };

  AnalyticsObject.prototype.bindCustomerEvents = function () {
    this.on('start', () => debug(`==================================== STARTED ====================================`));

    this.on('progress', async (theEventInfo) => {
      const theSyncObject = SyncObject.fromDatamartEvent(theEventInfo);
      const hasOwnProperty = Object.prototype.hasOwnProperty.bind(theSyncObject);
      if (hasOwnProperty('lastImageStepId') && 'undefined' !== typeof theSyncObject.lastImageStepId) {
        debug('==================================== [START] Saved progress to datamart sync ====================================');
        await SyncDataAccess.createSyncRecord(theSyncObject);
        debug('==================================== [COMPLETE] Saved progress to datamart sync ====================================');
      }
    });

    this.on('complete', () => debug(`==================================== COMPLETED ====================================`));
  };

  const preparePayload = (payload) => {
    return Object.assign({}, payload, { timestamp: Date.now() });
  };

  /**
   * Shared instance used across all consumers of this module.
   * @private
   * */
  const SingletonInstance = new AnalyticsObject();
  return SingletonInstance;
};

module.exports.AnalyticEvents = newAnalyticsModule();
