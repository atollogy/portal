const _ = require('lodash');

// Micro-ET EventFact Routine
class EventFact {
  constructor(event) {
    this.event = event;
  }

  static assessForErrors() {
    return [];
  }

  prop(name) {
    return this.event[name];
  }
}

class Strategy {
  applyTo(fact) {
  }
}

class OutputStreamer {
  post(factEvent) {
    console.log(factEvent);
  }
}

let stopProccessingOnErrors = function (errors) {
  if (errors.length) throw new Error(`There were problems while processing. They are:\n ${errors.join('\n\t* ')}`);
};

let fetchActiveDimensionAndInferenceStrategies = function () {
  const strategies = _.flatten([require('./strategiesForDimensionInference')]);
  return strategies;
};

class EventFactProcessor {
  async processOneRow(imageStepRow) {
    // * Create new empty EventFact. Populate image fields, value, confidence, collection_time
    const out = new EventFact(imageStepRow);
    // * Infer it’s dimensions, create new dimension records, as needed
    const strategies = fetchActiveDimensionAndInferenceStrategies();
    //     * Iterate over a set of Strategy objects, each deciding to contribute a particular dimension or to
    //       populate some attributes on EventFact
    strategies.forEach((strategy) => {
      //     * Interested Strategies will update the EventFact with dimension FKs
      //     * When creating a new dimension record, flag this somehow so that deeper/related information can be
      //       populated by system users (e.g. like when we find a new d_operator or a new d_subject)
      strategy.applyTo(out);
    });
    // * A sanity checker assesses the EventFact for suitability & throws an exception, otherwise.
    const errors = EventFact.assessForErrors();
    stopProccessingOnErrors(errors);
    //
    return out;
  }
}

// Micro-Load EventFact Routine
// * Read from Kinesis EventFacts
// * Write to Postgres/etc
//
// Micro-ETL f_interval Routine
// * Follow Kinesis stream of EventFact creations for a given CGR or maybe set of subjects.
// * Cache the last n minutes/hours of EventFact data.
// * Pour over the EventFacts data and … hmmm
//     * So, perhaps restate all f_intervals when any EventFact comes along for that subject hour?
//     * Perhaps, calc the exact f_intervals impacted and restate those (I like this) as needed

////// Main, but temp

// * Sane EventFacts are pushed to a Kinesis stream.
// const outputStream = new OutputStreamer();
// outputStream.post(out);

const rows = require('./test/someRealRows');
const temp = new EventFactProcessor();
// * Grab the next image_step(s) and process them
rows().then(records => Promise.all(records.map(async r => temp.processOneRow(r)))
  .then(console.log)
  .catch(console.err));
