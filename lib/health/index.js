const _ = require('lodash');
const git = require('simple-git/promise')();
const conf = require('../dynamic-config');

let hash = undefined;

async function localGitVersionInfo() {
  try {
    return {
      // status: await git.status(),
      log: await git.log(['-1'])
    };
  } catch (e) {
    return {versionError: e};
  }
}

function localConfigInfo() {
  try {
    return {
      env: conf.env, hostname: conf.hostname, bcmPath: conf.bcmPath
    };
  } catch (e) {
    return {configError: e};
  }
}

async function dbmsHealth() {
  try {
    return {
      dbmsHealth: await require('../user-management').healthCheck()
    };
  } catch (e) {
    return {dbmsError: e};
  }
}

async function getGitHash() {
  if (!hash) hash = await git.log(['-1']);
  return hash;
}

function parseHash(gitLog) {
  return gitLog.latest.hash.substring(0, 8);
}

async function getShortHash() {
  return parseHash(await getGitHash());
}

function processInfo() {
  return {release: process.release, uptime: process.uptime()};
}

function getCurrentYear() {
  return new Date().getFullYear();
}

module.exports = {
  healthCheck: async () => {
    return _.merge(
      await localGitVersionInfo(),
      await dbmsHealth(),
      processInfo(),
      localConfigInfo());
  }, getShortHash, getCurrentYear
};
