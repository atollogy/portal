'use strict';

(function (register) {
  register('@atollogy/cycle-stats', ['moment'], function (moment) {
    const ExtensionID = 'f0afacf7-fae4-4148-9e44-44318a1e3191';

    const computeCycleMetrics = function cycleStats(listOfScycles) {
      let cycle = Object.assign({}, listOfScycles);
      let dataFilterForDuration = cycle.iterations.filter((iteration) => iteration.states.some(({ is_start, is_end }) => is_start || is_end));

      const theLowerBound = 0;
      const theUpperBound = moment.now().valueOf();
      let iterationStats = dataFilterForDuration.map(theCycleObject => {
        const theCycleStates = theCycleObject.states;
        const theStartTime = theCycleStates.reduce((current, { startTime }) => Math.min(current, moment(startTime).valueOf()), theUpperBound);
        const theFinishingTime = theCycleStates.reduce((current, { endTime }) => Math.max(current, moment(endTime).valueOf()), theLowerBound);
        return { duration: theFinishingTime - theStartTime };
      });

      const distinctCycles = listOfScycles.iterations.reduce((mapOfCycles, { subject }) => Object.assign(mapOfCycles, { [subject]: true }, {}));

      listOfScycles.stats = {
        avgDuration: iterationStats.length ? iterationStats.reduce((grandTotal, { duration }) => grandTotal + duration, 0) / iterationStats.length : 0,
        distinctCycles: Object.keys(distinctCycles).length,
        cycleCount: cycle.iterations.length
      };

      return listOfScycles;
    };

    return Object.freeze({
      computeCycleMetrics,
      ExtensionID
    });
  });
}(
  function (name, dependencies, factory) {
    const global = this;

    const objects = [];
    let newModuleInstance;

    if ('function' === typeof require && 'undefined' !== typeof module && hasOwnProperty.call(module, 'exports')) {
      for (let index = 0; index < dependencies.length; index += 1) {
        const requiredModule = dependencies[index];
        const moduleInstance = require( requiredModule );
        objects.push(moduleInstance);
      }

      newModuleInstance = factory.apply({}, objects);
      module.exports = newModuleInstance;
    } else {
      for (let index = 0; index < dependencies.length; index += 1) {
        const requiredModule = dependencies[index];
        const moduleInstance = global[requiredModule];
        objects.push(moduleInstance);
      }

      newModuleInstance = factory.apply({}, objects);
      global[name] = newModuleInstance;
    }

  }.bind(this)
));