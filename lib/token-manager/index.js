const randomString = require('randomstring');

const { sql, query } = require('../data-access');

function generateANewRandomToken() {
  return randomString.generate({
    length: 6,
    charset: 'alphanumeric',
    capitalization: 'uppercase'
  });
}

async function storeTokenInDb(token, parameters) {
  parameters.token = token;
  let q = sql(`
  INSERT INTO customer_screen_config (token, cgr, screen_name, screen_type, is_active, activation_date, last_config_date, config) 
  VALUES (:token, null, null, :screen_type, FALSE, null, null, null);`)(parameters);
  return (await query(q.text, q.values)).rowCount === 1;

}

module.exports = {
  generateNextToken: async (parameters) => {
    let token = undefined;


    do {
      token = await generateANewRandomToken();
    } while (token.match(discardCharRegex)!==null || !await storeTokenInDb(token,parameters));
    return token;
  },
  claimToken: async (parameters) => {
    let q = sql(`
    UPDATE customer_screen_config
    SET (cgr, screen_name, is_active, activation_date) = (:customer, :screenName, TRUE, now())
    WHERE token = :token`)(parameters);
    const insert = await query(q.text, q.values);
    return insert.rowCount === 1;
  },
  fetchConfig: async (parameters)  => {
    let q = sql(`
    SELECT token, cgr, screen_name, screen_type, config, is_active
    FROM customer_screen_config
    WHERE token = :token`)(parameters);
    let rows = await query(q.text,q.values);
    return rows.rows[0];
  },
  updateLastAwakeTime: async (parameters) => {
    let q = sql(`
    UPDATE customer_screen_config SET last_awake_date = now()
    WHERE token = :token AND cgr = :cgr AND screen_name = :screenName AND screen_type = :screenType AND is_active = TRUE
    `)(parameters);
    let rows = await query(q.text,q.values);
    return rows;
  }
};
