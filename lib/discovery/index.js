'use strict';

module.exports = (function () {
  const allowedServices = ['user', 'group'];

  const environment = process.env.BCM_ENV_NAME || 'dev';

  /**
   * TODO: update this from database or environment variables
   */
  const envToHostMapping = Object.freeze({
    'dev': { host: 'api-portal.dev.at0l.io', version: '' },
    'stg': { host: 'api-portal.stg.at0l.io', version: '1.0.0' },
    'prd': { host: 'api.atollogy.com', version: '1.0.0' }
  });

  const Endpoints = Object.freeze({
    Fleet: Object.freeze(Object.assign({ protocol: 'https' }, envToHostMapping[environment]))
  });

  return Object.freeze({
    allowedServices,
    Endpoints,
    apiKey:'8c2d2130-b726-4fd1-bd0b-f8c9dbfe4948',
    getFleetUrl: function getFleetUrl(service) {
      const allowed = allowedServices.some(
        (allowedService) => allowedService === service
      );
      const { protocol, host, version } = Endpoints.Fleet;

      if (allowed) {
        const separator = version ? '_' : '';
        return `${protocol}://${host}/${service}${separator}${version}`;
      }

      throw new Error(`Service ${service} is not allowed or supported`);
    }
  });
})();