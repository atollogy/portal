const moment = require('moment');

module.exports.generatePortalImageLinks = (smoothenedIntervals, customer) => {
  smoothenedIntervals.forEach((interval, idx) => {
    smoothenedIntervals[idx].d_images = [];
    interval.images.forEach(image => {
      let time = moment(image.collection_time).unix();
      if (image.collection_time.split('.').length > 1) {
        time += '.' + image.collection_time.split('.')[1].split('+')[0];
      }
      const portalImgLink = `https://atollogy.com/portal/image/${customer}/${image.gateway_id}/${image.camera_id}/${time}/${image.step_name}/${image.name}`;

      smoothenedIntervals[idx].d_images.push(portalImgLink);
    });
  });
  return smoothenedIntervals;
};