const _ = require('lodash');
const bcrypt = require('bcrypt');
const debug = require('debug')('portal:user-management');

const NAV_QUERY = `SELECT navigation from portal_navigation`;

const { sql, pool, query, config } = require('../data-access');

const passportSerializeUser = (user, done) => {
  done(null, user.id);
};

const fetchNav = async function () {
  try {
    const theQueryPromise = new Promise((onQueryResolve, onQueryReject) => {
      pool.query(NAV_QUERY, [], (cause, result) => {
        if (cause) {
          onQueryReject({ error: cause, source: 'client' });
        } else {
          onQueryResolve(result);
        }
      });
    });

    const result =  await theQueryPromise;
    const navCfg = result.rows[0]['navigation'];
    return navCfg;
  } catch(cause) {
    debug('Error when fetching Navigation: ', cause);
  }
};


const calcLandingPage = async function (cgrs) {
  const navCfg = await fetchNav();
  const appBaseUrl = 'portal';
  const firstCgr = _.get(cgrs, '0');
  const nav4Cgr = navCfg['cgrNavs'][firstCgr];
  //try to fetch landingPage if failed try to fetch it from one level below in tree
  let landingPage = nav4Cgr['landing'] || nav4Cgr[Object.keys(nav4Cgr)[0]].landing;
  return `/${appBaseUrl}${landingPage}?customer=${firstCgr}`;
};

let buildUserObjFromUserRow = function (first) {
  const cgrs = parseDbCgrsField(first.cgrs);
  return {
    id: first.id,
    username: first.username,
    type: first.type,
    cgrs: cgrs,
    env: config.env,
    employeeType: _.get(cgrs, '0', 'sbay'),
    displayName: `${first.first_name} ${first.last_name}`,
    title: first.job_title || 'Manager',
    mail: first.username,
    ou: first.company || 'Favorite Atollogy Customer',
    telephoneNumber: first.phone_number || '555-555-1212',
    street: first.street_address || '123 Anywhere',
    l: first.city || 'Anytown',
    st: first.state || 'ZZ',
    postalCode: first.zipcode || '12345',
    permittedFacilities: first.permitted_facilities,
    tableau: (first.user_access && first.user_access.tableau) || null
  };
};

const passportDeserializeUser = (id, cb) => {
  pool.query('SELECT *, username emailAddress, id dn FROM users WHERE id = $1', [parseInt(id, 10)], (err, results) => {
    if (err) {
      debug('Error when selecting user on session deserialize', err);
      return cb(err);
    }
    const first = results.rows[0];
    cb(null, buildUserObjFromUserRow(first));
  });
};

function parseDbCgrsField(cgrsString) {
  return cgrsString.split(',').map(e => e.trim());
}

function passportLocalStrategy(username, password, cb) {
  pool.query(`SELECT *, username emailAddress, id dn
              FROM users
              WHERE lower(username) = lower($1)
                AND is_active = TRUE`, [username], (err, result) => {
    if (err) {
      debug('Error when selecting user on login', err);
      return cb(err);
    }

    if (result.rows.length > 0) {
      const first = result.rows[0];
      bcrypt.compare(password, first.password, (theErroObject, theResponseObject) => {
        if (theResponseObject) {
          cb(null, buildUserObjFromUserRow(first));
        } else {
          cb(null, false);
        }
      });
    } else {
      cb(null, false);
    }
  });
}

function passportJWTStrategy(jwtPayload, cb){
  console.log('payload received', jwtPayload);
  pool.query(`SELECT *, username emailAddress, id dn FROM users WHERE id=$1 AND is_active=TRUE`, [jwtPayload.id], (err, result) => {
    if (err) {
      debug('Error when selecting user on login', err);
      return cb(err);
    }
    if (result.rows.length > 0) {
      return result;
    } else {
      cb(null, false);
    }
  });
}

async function fetchUserList() {
  let {rows} = await query(`SELECT *
                            FROM users
                            ORDER BY id`, []);
  return rows;
}

async function fetchById(userIds) {
  return query(`
    SELECT id                                                     as user_id,
           first_name || ' ' || substring(last_name, 1, 1) || '.' as user_name,
           first_name,
           last_name,
           username,
           phone_number,
           cgrs,
           company,
           permitted_facilities facilities,
           user_access,
           alerts
    FROM users
    WHERE id = ANY ($1);`, [userIds]);
}

async function fetchByEmail(parameters) {
  let q = sql(`SELECT *
               FROM users
               WHERE lower(username) = lower(:email);`)(parameters);
  let {rows} = await query(q.text, q.values);
  return rows;
}

async function updateByEmail(parameters) {
  let q = sql(`UPDATE users
               SET reset_token         = :resetToken,
                   reset_token_created = :tokenCreated
               WHERE lower(username) = lower(:email);`)(parameters);
  await query(q.text, q.values);
}

async function updateById(parameters) {
  let q = sql(`UPDATE users
               SET first_name           = :firstName,
                   last_name            = :lastName,
                   cgrs                 = :cgrs,
                   phone_number         = :phoneNumber,
                   company              = :company,
                   permitted_facilities = :facilities,
                   user_access = :user_access,
                   alerts = :alerts
               WHERE id = :id
                 AND username = :email;`)(parameters);
  try {
    let row = await query(q.text, q.values);
    return row.rowCount;
  } catch (e) {
    return -1; //internal error
  }
}

async function updatePasswordByToken(parameters) {
  let q = sql(`UPDATE users
               SET password = :encryptPwd
               WHERE reset_token = :resetToken  
               RETURNING *`)(parameters);
  let { rows } = await query(q.text, q.values);
  return rows;
}

async function fetchByResetToken(parameters) {
  let q = sql(`SELECT *
               FROM users
               WHERE reset_token = :token;`)(parameters);
  let {rows} = await query(q.text, q.values);
  return rows;
}

async function addNewUser(parameters) {
  let ids = await query('SELECT id FROM users ORDER BY id desc', []);
  parameters.id = Number(ids.rows[0].id) + 1;

  let q = sql(`INSERT INTO users (id, username, password, cgrs, company, first_name, last_name, phone_number, job_title,
                                  street_address, city, state, zip, permitted_facilities, is_active, user_access)
               VALUES (:id, :email, :pwd, :cgrs, :company, :firstName, :lastName, :phoneNumber, 'Atollogist',
                       '900 Lafayette St', 'Santa Clara', 'CA', '95051', :facilities, TRUE, :user_access)`)(parameters);
  try {
    let rows = await query(q.text, q.values);
    return rows.rowCount;
  } catch (e) {
    if (e.constraint === 'users_username_key') {
      return -1; //username already exists
    } else {
      return 0; //server errors
    }
  }
}

async function inActivateUser(parameters) {
  let q = sql(`UPDATE users
               SET is_active = :isActive
               WHERE id = :id;`)(parameters);
  let rows = await query(q.text, q.values);
  return rows;
}

function authenticateCgr(req, res) {
  const paramCgr = req.params.cust || req.query.customer || req.body.customer;
  req.session.cgrs = req.session.cgrs || req.user.cgrs;
  return req.session.cgrs.some(cgr => cgr == 'atl' || !paramCgr || cgr == paramCgr) || false;
}

function authenticateFacility(req, res) {
  const permitFacilities = req.user.permittedFacilities;
  const paramFacility = req.query.facility || req.body.facility;

  if (permitFacilities == null || !paramFacility || permitFacilities.length == 0) {
    return true;
  } else if (permitFacilities.some((facility) => facility === paramFacility)) {
    return true;
  } else {
    res.status(401);
    res.render('linkExpired', {
      error: 'user does not have access to this facility'
    });
    return false;
  }
}

module.exports = {
  healthCheck: () => query('select count(*) as hello from users', []).then((r) => {
    return r.rows[0].hello;
  }),
  passportLocalStrategy,
  passportSerializeUser,
  passportDeserializeUser,
  fetchUserList,
  fetchById,
  addNewUser,
  fetchByEmail,
  updateByEmail,
  updateById,
  inActivateUser,
  fetchByResetToken,
  updatePasswordByToken,
  authenticateCgr,
  authenticateFacility,
  passportJWTStrategy,
  calcLandingPage,
  query,
  fetchNav
};
