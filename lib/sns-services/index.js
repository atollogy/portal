const AWS = require('aws-sdk');
const { accessKeyID, secretAccessKey, awsFleetApi} = require('../dynamic-config');
var sns;

const defaults =  {
  region: 'us-west-2'
};
const fleetConfig = Object.assign({}, defaults, awsFleetApi);

async function configure() {
  AWS.config.update({
    accessKeyId: accessKeyID,
    secretAccessKey: secretAccessKey,
    region: fleetConfig.region
  });
  sns = new AWS.SNS();
}

configure();

const publishSNS = (config, data, callback) => {
  if (config) {
    const publishToTopicSNS = {
      TargetArn: config.arn,
      Subject: config.subject,
      Message: JSON.stringify(data),
      MessageAttributes: {
        orderData: { DataType: 'String', StringValue: JSON.stringify(data) }
      }
    };
    console.log(publishToTopicSNS);

    const oSnSPromise = new Promise((resolve, reject) => {
      sns.publish(publishToTopicSNS, function(error, oPublishResponse) {
        if (error) {
          console.log('error response: ', error, error.stack);
          reject(error);
        } else {
          console.log('data response: ', oPublishResponse);
          resolve(oPublishResponse);
        }
      });
    });

    return callback
      ? oSnSPromise.then(response => callback(null, response)).catch(error => callback(error))
      : oSnSPromise;
  }

  return Promise.reject({ error: 'No configuration found ' });
};

module.exports = {
  publishSNS
};
