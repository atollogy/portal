const { db, sql, query } = require('../data-access');

module.exports.allScreenNames = async function (customer) {
  const statement = `
      SELECT screen_name
      FROM customer_screen_config
      WHERE cgr = $1;
  `;

  const {rows} = await query(statement, [customer]);
  return rows;

};

module.exports.subjectNames = async function (customer) {
  const statement = `
      SELECT DISTINCT ON (subject_name) subject_name, facility
      FROM customer_report_config
      WHERE end_time IS NULL
  `;
  try {
    const {rows} = await db.query(customer, statement);
    return rows;
  } catch (error) {
    throw new Error(`Problem getting subjectNames from ${customer} customer_report_config. Error is: ${error}.`);
  }
};

module.exports.screensGetConfig = async function (customer) {
  const statement = `
      SELECT token, screen_name, config, screen_type
      FROM customer_screen_config
      WHERE is_active = TRUE
        AND cgr = $1
      ORDER BY activation_date desc;
  `;

  const {rows} = await query(statement, [customer]);
  return rows;

};

module.exports.screensSetConfig = async function (parameters) {
  const q = sql(`UPDATE customer_screen_config
                    SET (last_config_date, config) = (now(), :config)
                  WHERE cgr = :customer
                    AND screen_name = :screenName;`)(parameters);

  const {rows} = await query(q.text, q.values);
  return rows;

};

module.exports.deactivateScreen = async function (token) {
  const statement = `
      UPDATE customer_screen_config
      SET (screen_name, is_active, activation_date, last_config_date, config) = (null, FALSE, null, now(), null)
      WHERE token = $1;
  `;

  const {rows} = await query(statement, [token]);
  return rows;

};

module.exports.screensGetConfigForDevice = async function (customer, screenName) {
  const statement = `
      SELECT *
      FROM customer_screen_config
      WHERE is_active = TRUE
        AND cgr = $1
        AND screen_name = $2
  `;

  const {rows} = await query(statement, [customer, screenName]);
  return rows;
};


