const sql = require('yesql').pg;
const debug = require('debug')('portal:db-query');

const db = require('../db/');
const config = require('./dynamic-config');
const { Pool } = require('../lib/database');
const { hashCode } = require('../lib/utils/hash');
const sanitizeParameters = require('../lib/sanitize');

const newErrorTemplate = (hashcode, cause, statement, summary) =>  `<!![EXCEPTION[
${hashcode} DB threw an error while running a statement. Both follow:
Error:
    ${cause}
SQL:
    ${statement}
Parameters:
    ${JSON.stringify(summary, null, 2)}
]]>`;

module.exports = (function () {
  const util = require('util');
  const EventEmitter = require('events');

  const metrics = { connected: 0, acquired: 0, removed: 0, errors: 0 };

  const SimpleQueryStrategy = async (pool, statement, parameters) => {
    const hashcode = hashCode(statement);

    try {
      const theQueryPromise = new Promise((onQueryResolve, onQueryReject) => {
        pool.query(statement, parameters, (cause, result) => {
          if (cause) {
            onQueryReject({ error: cause, source: 'client' });
          } else {
            onQueryResolve(result);
          }
        });
      });

      return await theQueryPromise;
    } catch(cause) {
      const summary = sanitizeParameters(parameters);
      throw new Error(newErrorTemplate(hashcode, cause, statement, summary));
    }
  };

  const ManagedQueryStrategy = async (pConnectionPool, pStatement, oParameters) => {
    const control = {};
    const hashcode = hashCode(pStatement);

    try {
      const theQueryPromise = new Promise((resolve, reject) => {
        pConnectionPool.connect((error, client, release) => {
          Object.assign(control, { release });

          if (error) {
            reject({ error, source: 'pool' });
          } else {
            client.query(pStatement, oParameters, (cause, result) => {
              release();

              Object.assign(control, { release: false });
              if (cause) {
                reject({ error: cause, source: 'client' });
              } else {
                resolve(result);
              }
            });
          }
        });
      }).catch((cause) => {
        const summary = sanitizeParameters(oParameters);
        throw new Error(`${hashcode} DB threw an error while running a statement. Both follow:\nError:\n${cause}\n\nSQL:\n${pStatement}\n\nParameters:\n${JSON.stringify(summary, null, 2)}`);
      });

      return await theQueryPromise;
    } finally {
      try {
        if (control && control.release && 'function' === typeof control) {
          control.release(true);
        }
      } catch(cause) {
        debug('[WARNING] error while trying to release client', cause.message ? cause.message : cause);
      }
    }
  };

  const fBindPoolEvents = (oConnectionPool) => {
    const logger = require('debug')('portal:metrics:connections');

    oConnectionPool.on('remove', (/* client */) => {
      metrics.removed += 1;
      logger(`Connection removed (${JSON.stringify(metrics)})`);
    });
    oConnectionPool.on('acquire', (/* client */) => {
      metrics.acquired += 1;
      logger(`Connection acquired (${JSON.stringify(metrics)})`);
    });
    oConnectionPool.on('error', (cause /* client */) => {
      metrics.errors += 1;
      logger(`Error in connection ${Object.prototype.hasOwnProperty.call(cause, 'message') ? cause.message : cause}`);
    });
    oConnectionPool.on('connect', (client) => {
      metrics.connected += 1;
      client.query(`SET work_mem = '16MB'`);
      client.query(`SET temp_buffers = '16MB'`);
      logger(`Opening new connection (${JSON.stringify(metrics)})`);
    });

    return oConnectionPool;
  };

  const fInitConnectionPool = (oConfigObject) => {
    const oPoolConfig = {
      user: oConfigObject.authDbmsConnParameters.user || '',
      password: oConfigObject.authDbmsConnParameters.password || '',
      database: oConfigObject.env,
      host: oConfigObject.authDbmsConnParameters.host,
      port: oConfigObject.authDbmsConnParameters.port || '5432',
      max: oConfigObject.authDbmsConnParameters.max,
      idleTimeoutMillis: oConfigObject.authDbmsConnParameters.idleTimeoutMillis,
      connectionTimeoutMillis: oConfigObject.authDbmsConnParameters.connectionTimeoutMillis
    };

    const newConnectionPool = new Pool(oPoolConfig);

    return fBindPoolEvents(newConnectionPool);
  };

  const pool = fInitConnectionPool(config);

  const ConnectionPool = function (pQueryStrategy) {
    EventEmitter.call(this);
    const theQueryStrategy = pQueryStrategy || ManagedQueryStrategy;
    Object.assign(this, { QueryStrategy: theQueryStrategy });
    Object.defineProperties(this, { state: { value: {}, enumerable: false, writable: true } });
  };

  util.inherits(ConnectionPool, EventEmitter);

  ConnectionPool.prototype.findOrCreate = function (options) {
    const { state } = this;
    const { user, database, host } = options;
    const key = Symbol.for(`${user}-${database}-${host}`);
    if (Object.prototype.hasOwnProperty.call(state, key)) {
      return state[key];
    } else {
      const datamart = new Pool(options);
      Object.assign(state, { [key]: datamart });
    }

    return state[key];
  };

  ConnectionPool.prototype.query = async function query(text, parameters) {
    const { QueryStrategy: _query } = this;
    return await _query(pool, text, parameters);
  };

  ConnectionPool.ManagedQueryStrategy = ManagedQueryStrategy;
  ConnectionPool.SimpleQueryStrategy = SimpleQueryStrategy;
  ConnectionPool.Singleton = new ConnectionPool(ManagedQueryStrategy);

  return Object.freeze({
    config, db, sql, pool,
    query: ConnectionPool.Singleton.query.bind(ConnectionPool.Singleton),
    ConnectionPool: { findOrCreate: ConnectionPool.Singleton.findOrCreate.bind(ConnectionPool.Singleton) }
  });
}());
