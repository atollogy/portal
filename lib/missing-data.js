const memored = require('memored');
const moment = require('moment');

const theDebugModule = require('debug')('portal:metrics:missing-data');

const isMissingDataOverwrite = () => Object.prototype.hasOwnProperty.call(process.env, 'ENABLE_MISSING_DATA');

module.exports = (function newMissingDataHandler(debug) {
  const DEFAULT_CAPTURE_INTERVAL = 60 * 1000;
  const MISSING_DATA_STATE = 'Missing Data';

  const newSubjectEvent = (startTime, endTime, state) => ({
    startTime,
    endTime,
    images: [],
    missingMeasures: [],
    state
  });

  const HOUR_IN_MILLIS = 60 * 60 * 1000;

  const newMissingDataControl = (customer, theReportConfig) => {
    return ({ facility, subject, measure }) => {
      let isEnabled = false;
      const theSettingKey = `${customer}:${facility}:${subject}:${measure}`;

      if (Object.prototype.hasOwnProperty.call(theReportConfig, theSettingKey)) {
        const { [theSettingKey]: value } = theReportConfig;
        isEnabled = value === true || ('string' === typeof value && ['1', 'true'].includes(value));
      }

      isEnabled = isEnabled || isMissingDataOverwrite();

      return isEnabled;
    };
  };

  const newMissingDataProcessor = (isMissingDataEnabled, theReportConfig, theShiftConfig) => {
    return async (theCustomerName, theSubjects) => {
      const { subjects: theListOfSubjects } = theSubjects;
      const theUpperShiftLimit = HOUR_IN_MILLIS * Math.floor(Date.now() / (HOUR_IN_MILLIS));
      const { shiftStartTime: theStartOfShift, shiftEndTime: theEndOfShift, timeSlotSizeMillis: theTimeSlotSizeMillis } = theSubjects;
      const theCaptureInterval = 'number' === typeof theTimeSlotSizeMillis ? theTimeSlotSizeMillis : DEFAULT_CAPTURE_INTERVAL;
      const endOfShift = theEndOfShift > theUpperShiftLimit ? theUpperShiftLimit : theEndOfShift;
      const startOfShift = theStartOfShift > theUpperShiftLimit ? theUpperShiftLimit : theStartOfShift;

      const listOfSubjects = [];
      const theCurrentDate = new Date();
      for (let theSubjectIndex = 0; theSubjectIndex < theListOfSubjects.length; theSubjectIndex += 1) {
        const theSubjectInfo = theListOfSubjects[theSubjectIndex];
        const { name: theSubjectName, facility: theFacilityName, measures: mapOfMeasures } = theSubjectInfo;

        const theMapOfMeasures = {};
        const listfoMeasureGroups = Object.keys(mapOfMeasures);
        for (let theMeasureGroupIndex = 0; theMeasureGroupIndex < listfoMeasureGroups.length; theMeasureGroupIndex += 1) {
          const theMeasureName = listfoMeasureGroups[theMeasureGroupIndex];
          const listOfMeasures = mapOfMeasures[theMeasureName] || [];
          const theTimeContext = { startTime: startOfShift, endTime: endOfShift };
          const theMissingDataEvents = [];

          if (isMissingDataEnabled({ customer: theCustomerName, subject: theSubjectName, facility: theFacilityName, measure: theMeasureName })) {
            debug(`MISSING DATA [CACHE]: ${theCustomerName}/${theFacilityName}/${theSubjectName}/${theMeasureName}:${startOfShift}:${endOfShift}`);
            const theCacheKey = `${theCustomerName}:${theFacilityName}:${theSubjectName}:${theMeasureName}:${startOfShift}:${endOfShift}`;

            try {
              const theCacheReadPromise = new Promise((onCacheReadResolved, onCacheReadRejected) => {
                memored.read(theCacheKey, (theCacheError, theCacheValue) => {
                  if (theCacheError) {
                    debug('Unable to retrive cached value for key', theCacheKey);
                    return onCacheReadRejected(theCacheError);
                  }

                  if (theCacheValue && Object.prototype.hasOwnProperty.call(theCacheValue, 'elements')) {
                    return onCacheReadResolved(theCacheValue);
                  }

                  onCacheReadRejected(new Error('Cache entry has no elements'));
                });
              });

              const theCacheReadResponse = await theCacheReadPromise;
              const { elements: listOfMissingEvents } = theCacheReadResponse;
              listOfMissingEvents.forEach((theMissingDataEvent) => theMissingDataEvents.push(theMissingDataEvent));
            } catch(theReadCacheError) {
              const theMeasureSummary = { startOfShift, endOfShift };
              const theShiftKey = `${theCustomerName}:${theFacilityName}:${theSubjectName}:${theMeasureName}`;
              if (listOfMeasures.length === 0 &&
                theShiftConfig[theShiftKey].shiftStartTime < Date.now()) {
                theMissingDataEvents.push(
                  newSubjectEvent(
                    moment(new Date(theShiftConfig[theShiftKey].shiftStartTime)),
                    moment(new Date(theShiftConfig[theShiftKey].shiftEndTime)),
                    MISSING_DATA_STATE
                  )
                );
              } else {
                for (let theMeasureIndex = 0; theMeasureIndex < listOfMeasures.length; theMeasureIndex += 1) {
                  const theMeasureInfo = listOfMeasures[theMeasureIndex];
                  const { startOfShift, endOfShift } = theMeasureSummary;
                  const theTimeSinceLastEvent = theCaptureInterval * Math.floor((Number(theMeasureInfo.startTime) - Number(theTimeContext.startTime)) / theCaptureInterval);

                  if (theTimeSinceLastEvent >= theCaptureInterval) {
                    const theMissingDataStartTime = Number(theTimeContext.startTime);
                    const theMissingDataEndTime = Number(theMeasureInfo.startTime);
                    theMissingDataEvents.push(
                      newSubjectEvent(
                        moment(new Date(theMissingDataStartTime)),
                        moment(new Date(theMissingDataEndTime)),
                        MISSING_DATA_STATE
                      )
                    );
                    debug(`MISSING DATA [CREATE]: ${theMeasureName}/${theMissingDataStartTime}:${theMissingDataEndTime}?duration=${theTimeSinceLastEvent}`);
                  }
                  Object.assign(theTimeContext, { startTime: moment(theMeasureInfo.endTime) });

                  Object.assign(theMeasureSummary, {
                    startOfShift: Number(startOfShift) > Number(theMeasureInfo.startTime) ? theMeasureInfo.startTime : startOfShift,
                    endOfShift: Number(endOfShift) > Number(theMeasureInfo.endTime) ? endOfShift : theMeasureInfo.endTime
                  });
                }
              }

              const theTimeToShiftEnd = theCaptureInterval * Math.floor(
                (Number(theTimeContext.endTime) - Number(theTimeContext.startTime)) / theCaptureInterval);
              if (theTimeToShiftEnd >= theCaptureInterval) {
                const theMissingDataStartTime = Number(theTimeContext.startTime);
                const theMissingDataEndTime = Number(theTimeContext.endTime);
                theMissingDataEvents.push(
                  newSubjectEvent(
                    moment(new Date(theMissingDataStartTime)),
                    moment(new Date(theMissingDataEndTime)),
                    MISSING_DATA_STATE
                  )
                );
                debug(`MISSING DATA [COMPLEMENT]: ${theMeasureName}/${theMissingDataStartTime}:${theMissingDataEndTime}?duration=${theTimeToShiftEnd}`);
              }

              debug(`MISSING DATA [SUMMARY]: ${theMeasureName}:${theMissingDataEvents.length}, ${JSON.stringify(theMeasureSummary)}`);

              try {
                const theWriteCachePromise = new Promise((onCacheWriteResolved, onCacheWriteRejected) => {
                  const theCacheValue = Object.freeze({ startOfShift, endOfShift, elements: theMissingDataEvents });
                  const theCacheExpiration = 60 * 60 * 1000;
                  memored.store(theCacheKey, theCacheValue, theCacheExpiration, (theCacheError, theExpirationTime) => {
                    if (theCacheError) {
                      debug('Unable to persist cache entry for', theCacheKey);
                      return onCacheWriteRejected(theCacheError);
                    }

                    debug('Cache key', theCacheKey, 'stored with expiration time', theExpirationTime);
                    onCacheWriteResolved({ expires: theExpirationTime });
                  });
                });

                await theWriteCachePromise;
              } catch(theWriteCacheError) {
                /** ignored */
              }
            }
          }

          const theMeasureElements = [].concat(listOfMeasures).concat(theMissingDataEvents);

          theMeasureElements.sort((x, y) => Math.sign(x.startTime - y.startTime));

          Object.assign(theMapOfMeasures, { [theMeasureName]: theMeasureElements });
        }

        const theSubjectObject = { name: theSubjectName, facility: theFacilityName, measures: theMapOfMeasures };
        listOfSubjects.push(theSubjectObject);
      }

      if (theListOfSubjects.length === 0) {
        const theMapOfSubjects = {};
        Object.keys(theShiftConfig).forEach((theShiftKey) => {
          const theShiftElement = theShiftConfig[theShiftKey];
          const {
            subject: theSubjectName,
            measure: theMeasureName,
            facility: theFacilityName,
            shiftStartTime, shiftEndTime
          } = theShiftElement;

          if (theCurrentDate.getTime() > new Date(shiftEndTime)) {
            debug(`MISSING DATA[A]: ${theMeasureName}: ${JSON.stringify(theShiftElement)}`);

            const theSubjectObject = {};
            const theSubjectKey = `${theSubjectName}:${theFacilityName}`;
            if (Object.prototype.hasOwnProperty.call(theMapOfSubjects, theSubjectKey)) {
              Object.assign(theSubjectObject, theMapOfSubjects[theSubjectKey]);
            } else {
              Object.assign(theSubjectObject, {
                name: theSubjectName,
                facility: theFacilityName,
                measures: { /** */ }
              });
              listOfSubjects.push(theSubjectObject);
              Object.assign(theMapOfSubjects, { [theSubjectKey]: theSubjectObject });
            }

            const theMeasureElements = [
              newSubjectEvent(new Date(shiftStartTime), new Date(shiftEndTime), MISSING_DATA_STATE)];
            const { measures: theMapOfMeasures } = theSubjectObject;
            Object.assign(theMapOfMeasures, { [theMeasureName]: theMeasureElements });
          }
        });
      }
      theSubjects.subjects = listOfSubjects;

      return theSubjects;
    };
  };

  const newReportConfig = (customer, reportConfig) => {
    const theReportConfig = {};
    try {
      const listOfMeasureConfig = JSON.parse(JSON.stringify(reportConfig));

      listOfMeasureConfig.forEach((theAtomicConfig) => {
        const { subject_name, measure_name, facility, missing_data_enabled = true } = theAtomicConfig;
        const theSettingKey = `${customer}:${facility}:${subject_name}:${measure_name}`;
        Object.assign(theReportConfig, { [theSettingKey]: missing_data_enabled });
      });
    } catch(cause) {
      debug(`Unable to perform missing data analysis, reason:`, cause);
    }

    return Object.freeze(theReportConfig);
  };

  const newShiftConfig = (customer, reportConfig) => {
    const theShiftConfig = {};
    try {
      const listOfMeasureConfig = JSON.parse(JSON.stringify(reportConfig));
      listOfMeasureConfig.forEach((theAtomicConfig) => {
        const { subject_name, measure_name, facility } = theAtomicConfig;
        const theShiftElement = { measure: measure_name , facility, subject:subject_name };

        const theShiftKey = `${customer}:${facility}:${subject_name}:${measure_name}`;
        if (Object.prototype.hasOwnProperty.call(theShiftConfig, theShiftKey)) {
          Object.assign(theShiftElement, theShiftConfig[theShiftKey]);
        } else {
          Object.assign(theShiftElement, {
            shiftStartTime: Number.MAX_VALUE,
            shiftEndTime: Number.MIN_VALUE
          });
        }
        const { shiftStartTime, shiftEndTime } = theShiftElement;
        const { start_time: theShiftStartTime, end_time: theShiftEndTime } = theAtomicConfig;

        theShiftElement.shiftStartTime = Math.min(shiftStartTime, Date.parse(theShiftStartTime));
        theShiftElement.shiftEndTime = Math.max(shiftEndTime, Date.parse(theShiftEndTime));

        Object.assign(theShiftConfig, { [theShiftKey]: theShiftElement });
      });
    } catch (cause) {
      debug(`Unable to perform missing data analysis, reason:`, cause);
    }

    return Object.freeze(theShiftConfig);
  };

  const newMissingDataHandler = (customer, reportConfig) => {
    const theReportConfig = newReportConfig(customer, reportConfig);
    const theShiftConfig = newShiftConfig(customer, reportConfig);
    const isMissingDataEnabled = newMissingDataControl(customer, theReportConfig);
    const handleMissingEvents = newMissingDataProcessor(isMissingDataEnabled, theReportConfig, theShiftConfig);
    const theMissingDataHandler = (theSubjects) => handleMissingEvents(customer, theSubjects);
    return theMissingDataHandler;
  };


  return Object.freeze({ newMissingDataHandler, newReportConfig });
}(theDebugModule));