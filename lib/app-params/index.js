require('dotenv').config();
const debug = require('debug')('portal:app-params');
const _ = require('lodash');
const minimist = require('minimist');
const changeCase = require('change-case');

const cache = {};

const getParameters = function () {
  if (!Object.prototype.hasOwnProperty.call(cache, 'params')) {
    const params = process.argv.slice(2);
    const processed = minimist(params, {});
    Object.assign(cache, { params: processed });
  }

  return cache.params;
};

const getParameterValue = (parameterName, defaultValue) => {
  const parameters = getParameters();
  return _.get(parameters, parameterName, defaultValue);
};

const getEnvironment = function () {
  const value = process.env.BCM_ENV_NAME || process.env.NODE_ENV || getParameterValue('env');
  debug(`env is ${value}`);
  if (!value) throw new Error('Either the environment variable "NODE_ENV" or the --env argument must be set.');
  return value;
};

function get(name) {
  const requiredArgVal = getParameterValue(name);
  if (requiredArgVal) return requiredArgVal;
  const varName = changeCase.constantCase(name);
  if (process.env[varName]) return process.env[varName];
  return undefined;
}

module.exports = {
  env: getEnvironment(),
  argv: getParameters(),
  optionalAppParam: get,
  requiredAppParam: (name) => {
    const out = get(name);
    if (out) {
      return out;
    } else {
      throw new Error(`--${name} arg is required`);
    }
  },
  isTrueAppParam: (name) => {
    const argVal = getParameterValue(name);
    const newVar = argVal && argVal.toLocaleString().toLowerCase() === 'true';
    return true === newVar;
  }
};
