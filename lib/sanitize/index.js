'use strict';

const MAX_DEPTH = 8;

module.exports = function sanitizeParameters(parameters, depth) {
  const summary = {};

  depth = depth && 'number' === typeof depth && Number(depth) || 0;

  if (parameters && Array.isArray(parameters)) {
    const values = [] ;
    for(let index = 0; index < 10; index+= 1) {
      values[index] = sanitizeParameters(parameters[index], depth + 1);
    }

    Object.assign(summary, { _elements: values });
  } else if (parameters && 'object' === typeof parameters) {
    Object.keys(parameters).forEach((property) => {
      let value = parameters[property];

      if (value && Array.isArray(value)) {
        value = value.slice(0, Math.min(10, value.length));
      } else if (value && 'object' === typeof value && depth < MAX_DEPTH) {
        value = sanitizeParameters(value, depth + 1);
      }

      Object.assign(summary, { [property]: value });
    });
  } else {
    return parameters;
  }

  return summary;
};