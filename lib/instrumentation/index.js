const _ = require('lodash');
const debug = require('debug')('portal:instrumentation');
const getShortHash = require('../health').getShortHash;

const knownCustomerParams = ['params.customer', 'query.customer', 'params.cgr', 'query.cgr', 'params.cust', 'query.cust', 'session.posterConfig.cgr'];
const knownFacilityParams = ['params.facility', 'query.facility', 'session.posterConfig.config.facility'];
const knownShiftParams = ['params.shift', 'query.shift'];

function huntFor(ary) {
  return (req) => {
    let out = undefined, i = 0;
    while (!out && i < ary.length) {
      out = _.get(req, ary[i++]);
    }
    return out;
  };
}

let huntForPahuToken = function (res, req) {
  return _.get(res, 'locals.token',
    _.get(req, 'session.posterConfig.token',
      _.get(req, 'params.token',
        _.get('req', 'query.token'))));
};

module.exports = {
  express_middleware: async (req, res, next) => {
    const context = {
      username: _.get(req, 'user.username'),
      cgr: huntFor(knownCustomerParams)(req),
      facility: huntFor(knownFacilityParams)(req),
      shift: huntFor(knownShiftParams)(req),
      gitHash: await getShortHash(),
      pahuToken: huntForPahuToken(res, req),
      pahuPosterConfigLandingPage: _.get(req, 'session.posterConfig.config.landing_page')
    };
    debug(context);
    next();
  }
};
