'use strict';

const express = require('express');
const debug = require('debug')('portal:jwtApp.js');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const sassMiddleware = require('node-sass-middleware');
const path = require('path');
const LocalStrategy = require('passport-local').Strategy;
const passport = require('passport');
const jwtApp = express();
const jwt = require('jsonwebtoken');
const passportJWT = require('passport-jwt');
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;
const moment = require('moment');
const cacheResponseDirective = require('express-cache-response-directive');
const config = require('./lib/dynamic-config');
const _ = require('lodash');
const appBaseUrl = config.appBaseUrl;
const withAuth = require('./lib/protected-route/jwt-protected-route');
const userManagement = require('./lib/user-management');
const cors = require('cors');
debug('hostname: ' + config.hostname);

passport.use(
  new LocalStrategy({
    usernameField: 'email'
  }, userManagement.passportLocalStrategy));

passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.sessionSecret
}, userManagement.passportJWTStrategy));

passport.serializeUser(userManagement.passportSerializeUser);

jwtApp.set('views', path.join(__dirname, 'views'));
jwtApp.set('view engine', 'pug');
jwtApp.set('view engine', 'ejs');
jwtApp.locals.moment = moment;
jwtApp.use(cacheResponseDirective());
jwtApp.use(cookieParser());
jwtApp.use(bodyParser.json());
jwtApp.use(bodyParser.urlencoded({extended: false}));
jwtApp.use(passport.initialize());
jwtApp.use(cors());
jwtApp.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: process.env.NODE_ENV === 'development',
  prefix: appBaseUrl
}));
jwtApp.use(`${appBaseUrl}/`, express.static(path.join(__dirname, 'public')));

jwtApp.use(`${appBaseUrl}/data`, withAuth, require('./routes/data'));
jwtApp.use(`${appBaseUrl}/workbench`, withAuth, require('./routes/workbench'));
jwtApp.use(`${appBaseUrl}/image`, withAuth, require('./routes/image'));
jwtApp.use(`${appBaseUrl}/customer-asset`, withAuth, require('./routes/customer-asset'));

jwtApp.use(`${appBaseUrl}/config`, withAuth, require('./routes/report-config'));

jwtApp.get(`${appBaseUrl}/login`,
  async (req, res) => {
    return res.render('login.pug', {
      message: (_.get(req, 'session.messages[0]', undefined))
    });
  }
);

jwtApp.post(`${appBaseUrl}/login`, function (req, res, next) {
  passport.authenticate('local', {
    // successReturnToOrRedirect: `${appBaseUrl}/`,
    // failureRedirect: `${appBaseUrl}/login`,
    failureMessage: true,
    session: false
  }, (err, user, info) => {
    if (err || !user) {
      return res.status(404).json({
        message: 'The credentials provided weren\'t recognized.',
        user: user
      });
    }
    req.login(user, {session: false}, (err) => {
      if (err) {
        res.send(err);
      }
      let payLoad = {
        id: user.id,
        sub: user.displayName,
        env: user.env,
        cgrs: user.cgrs,
        username: user.username
      };
      // generate a signed json web token with the contents of user object and return it in the response
      const token = jwt.sign(payLoad, config.sessionSecret, {
        expiresIn: config.sessionCookieMaxAge // expires in 24 hours
      });
      console.log('User--->' + user.displayName);
      return res.json({user, token});
    });
  })(req, res);
});

jwtApp.use(function (error, req, res, next) {
  let err = error;
  if ('string' === typeof error) {
    err = {message: error};
  }
  console.error(err.message); // Log error message in our server's console
  if (!err.statusCode) err.statusCode = 500; // If err has no specified error code, set error code to 'Internal Server Error (500)'
  res.status(err.statusCode).send(err.message); // All HTTP requests must have a response, so let's send back an error with its status code and message
});

module.exports = jwtApp;
