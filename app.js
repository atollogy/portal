'use strict';
const debug = require('debug')('portal:app.js');

const favicon = require('serve-favicon');
const flash = require('connect-flash');

const { query } = require('./lib/data-access');

const nrInstrumentation = require('./lib/instrumentation').express_middleware;
const cookieParser = require('cookie-parser');
const sassMiddleware = require('node-sass-middleware');
const session = require('express-session');
const _ = require('lodash');
const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const compression = require('compression');
const helmet = require('helmet');
const path = require('path');
const LocalStrategy = require('passport-local').Strategy;
const moment = require('moment');
const cacheResponseDirective = require('express-cache-response-directive');
const config = require('./lib/dynamic-config');
const userManagement = require('./lib/user-management');
const fleetApi = require('./lib/fleet-api');
const cors = require('cors');
const app = express();
const appBaseUrl = config.appBaseUrl;

const useProtectedRoute = require('./lib/protected-route')(app);

debug('hostname: ' + config.hostname);

passport.use(
  new LocalStrategy({
    usernameField: 'email'
  }, userManagement.passportLocalStrategy));
passport.serializeUser(userManagement.passportSerializeUser);
passport.deserializeUser(userManagement.passportDeserializeUser);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('view engine', 'ejs');
app.locals.moment = moment;
app.use(flash());
app.use(helmet());
app.use(compression());
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(cacheResponseDirective());

app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors({ credentials: true }));
const pg = require('./lib/database');
const pgSession = require('connect-pg-simple')(session);

const dbConfig = {
  user: config.authDbmsConnParameters.user || '',
  password: config.authDbmsConnParameters.password || '',
  database: config.env,
  host: config.authDbmsConnParameters.host,
  port: config.authDbmsConnParameters.port || '5432',
  max: config.authDbmsConnParameters.max,
  idleTimeoutMillis: config.authDbmsConnParameters.idleTimeoutMillis
};

app.use(session({
  store: new pgSession({
    pool: new pg.Pool(dbConfig)
  }),
  secret: config.sessionSecret,
  resave: false,
  saveUninitialized: false,
  cookie: {maxAge: config.sessionCookieMaxAge}
}));

app.use(passport.initialize());
app.use(passport.session());

app.use(sassMiddleware({
  src: path.join(__dirname, 'public'),
  dest: path.join(__dirname, 'public'),
  indentedSyntax: true, // true = .sass and false = .scss
  sourceMap: process.env.NODE_ENV === 'development',
  prefix: appBaseUrl
}));

app.use(nrInstrumentation);

// mount all of the various EJS reports
useProtectedRoute(`reports`, require('./routes/reports-ejs-root'));
//mount the protected dashboard pages (must happen before mounting the rest of the public folder)
useProtectedRoute('dashboard', express.static(path.join(__dirname, 'public', 'dashboard')));

useProtectedRoute('camera-status', require('./routes/camera-status'));

useProtectedRoute('image', require('./routes/image'));
useProtectedRoute('data', require('./routes/data'));
useProtectedRoute('analytics', require('./routes/tableau'));
useProtectedRoute('reportExceptions', require('./routes/reportExceptions'));
useProtectedRoute(`qc`, require('./routes/qc'));
useProtectedRoute('workbench', require('./routes/workbench'));
useProtectedRoute('imageStepAnnotation', require('./routes/imageStepAnnotation'));
useProtectedRoute('imagestep', require('./routes/imageStep'));

useProtectedRoute('reports_listing', async (request, response) => {
  request.user.homePage = await userManagement.calcLandingPage(request.user.cgrs);
  return response.redirect(request.user.homePage);
});
useProtectedRoute('metrics', require('./routes/customer-metrics'));
useProtectedRoute('zones', require('./routes/customer-zones'));
useProtectedRoute('filter', require('./routes/customer-filters'));
useProtectedRoute('screen-admin', require('./routes/screen-admin'));
useProtectedRoute('customer-asset', require('./routes/customer-asset'));
useProtectedRoute('config', require('./routes/report-config'));

const portal = express();

portal.on('mount', () => {
  debug(`Portal Mounted as ${portal.mountpath}`);

  const whitelist = [
    'at0l.io',
    'atollogy.com'
  ].reduce((listOfUrls, domain) => {
    const hostnames = ['dev', 'stg', 'prd', ''].map(
      (environment) => `https://admin.${environment}.${domain}`
    );

    return listOfUrls.concat(hostnames);
  },[]);

  portal.locals.whitelist = whitelist;

  const corsOptionsDelegate = function (request, callback) {
    const originUrl = request.header('Origin');
    const corsOptions = { origin: whitelist.indexOf(originUrl) >= 0, credentials: true };
    callback(null, corsOptions);
  };

  portal.options('/login', cors(corsOptionsDelegate));

  portal.post(`/login`, cors(corsOptionsDelegate), passport.authenticate('local', {
    successReturnToOrRedirect: `${portal.mountpath}/`,
    failureRedirect: `${portal.mountpath}/login`,
    failureFlash: 'Invalid username or password, please check for typos and try again.',
    failureMessage: true
  }));

  portal.get(`/login`, (request, response) => {
    return response.render('login.pug', {
      messages: request.flash('error')
    });
  });

  portal.post(`/sign-in-fleet-api`, fleetApi.login);

  portal.get(`/logout`, (request, response) => {
    request.session.destroy((err) => {
      response.redirect(config.logoutRedirectUrl);
    });
    debug(`destroyed session`);
  });

  debug('Binding of login actions completed ');
});

portal.use(`/reset`, require('./routes/reset-password'));

portal.use(`/users`, require('./routes/users'));

portal.get(`/`, async (req, res) => {
  req.user.homePage = await userManagement.calcLandingPage(req.user.cgrs);
  return res.redirect(_.get(req, 'user.homePage', `/login`));
});

portal.get(`/portal/*`, (request, response) => {
  const hostname = request.get('Host');
  const { protocol, originalUrl } = request;
  const expectedUrl = `${protocol}://${hostname}${originalUrl}`;
  const targetUrl = expectedUrl.replace(/(\/portal\/)+(.*)/, '/$2');

  response.redirect(301, targetUrl);
});

portal.use(`/spa_v2/`, express.static(path.join(__dirname, 'spa_client_v2', 'build')));

portal.use(`/screen`, require('./routes/screen')); // mount the Big Screen & Kiosk UIs without protection

/** https://media.atollogy.com/lehighhanson/38af294f13d2/video0/1621939442.4914/lpr/annotated.jpg */
portal.use(`/cfmedia`, require('./routes/image')); // mount images for cloudfront

portal.use(`/`, express.static(path.join(__dirname, 'public')));

/**
 * TODO: Serve the file appropriately and remove this hack
 */
portal.use(`/discovery/index.js`, function(ignored, response){
  const DiscoveryObject = require('./lib/discovery');

  const theFileContent = `
  const allowedServices = ${JSON.stringify(DiscoveryObject.allowedServices)}; \n
  const Endpoints = ${JSON.stringify(DiscoveryObject.Endpoints)}; \n
  ${String(DiscoveryObject.getFleetUrl)}
  `;

  response.setHeader('Content-Type', 'application/javascript');
  response.setHeader('Content-Length', theFileContent.length);
  response.send(theFileContent);
});

const SCRIPT_CACHE_EXPIRATION = 7 * 24 * 60 * 60;
const CACHE_DIRECTIVES = [
  'private',
  'no-transform',
  'must-revalidate',
  `max-age=${SCRIPT_CACHE_EXPIRATION}`
];

[ 'db/customerLPFilters.js', 'lib/cycles.js' ].forEach((dependency) => {
  const [ directory, file ] = dependency.split('/');
  const { ExtensionID } = require(path.join(__dirname, directory, file));

  const targetEndpoint = `/extensions/${ExtensionID}`;
  portal.get(targetEndpoint, function (_, response) {
    response.setHeader('Content-Type', 'application/javascript; charset=UTF-8');
    response.setHeader('Cache-Control', CACHE_DIRECTIVES.join(','));
    response.sendFile(path.join(__dirname, directory, file));
  });
});

portal.use(`/health`, async (_, response) => {
  const healthcheck = {
    connection: '',
    uptime: process.uptime(),
    message: 'OK',
    timestamp: Date.now()
  };

  try {
    await query(`SELECT * FROM pg_stat_activity WHERE state = 'active'`);
    healthcheck.connection = 'CONNECTED';
  } catch (cause) {
    healthcheck.connection = 'BROKEN';
  }

  try {
    response.status(200);
  } catch (cause) {
    healthcheck.message = cause;
    response.status(503);
  }

  response.setHeader('Content-Type', 'application/json');
  response.send(healthcheck);
});

const { healthCheck: health } = require('./lib/health');
const { userIsAtollogist } = require('./lib/utils/cgr-util');
const { ensureLoggedIn } = require('connect-ensure-login');
portal.use(`/richhealth`, ensureLoggedIn(`/login`), async (request, response) => {
  const status = { now: Date() };

  try {
    if (userIsAtollogist(request.user)) {
      Object.assign(status, await health());
    }

    response.status(200);
  } catch (cause) {
    Object.assign(status, { cause });
    response.status(500);
  } finally {
    response.setHeader('Content-Type', 'application/json');
    response.send(status);
  }
});

app.use(`${appBaseUrl}`, portal);

module.exports = app;
