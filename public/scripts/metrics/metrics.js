'use strict';

const MINUTE_IN_MILLIS = 60 * 1000;
const FIVE_MINUTES_IN_MILLIS = 4 * 60 * MINUTE_IN_MILLIS;

(function (global, makeMetricsView) {
  const { Chart } = global;

  global['MetricsView'] = makeMetricsView(Chart);
}(this, function (Chart) {
  const OPTIONS = Object.freeze({
    responsive: true,
    title: {
      display: false
    },
    tooltips: {
      mode: 'index',
      intersect: true
    },
    plugins: {
      datalabels: { display: false }
    },
    scales: {
      xAxes: [{
        ticks: {
          autoSkip: false,
          maxRotation: 100
        }
      }],
      yAxes: [{
        ticks: {
          suggestedMin: 0,
          beginAtZero: true
        }
      }]
    }
  });

  const hasOwnProperty = (target, property) => Object.prototype.hasOwnProperty.call(target, property);
  const MetricsView = function (selector) {
    Object.assign(this, {
      selector: selector
    });
  };

  MetricsView.prototype.init = function () {};

  MetricsView.prototype.render = function (metrics) {
    const { measures } = Object.assign({ measures: [] }, metrics);

    const mapOfLabels = {};
    const mapOfValues = {};
    measures.forEach(({ name, value }) => {
      const label = FIVE_MINUTES_IN_MILLIS * Math.floor(Date.parse(name) / FIVE_MINUTES_IN_MILLIS);
      mapOfLabels[label] = 1 + (mapOfLabels[label] || 0);
      mapOfValues[label] = Number(value) + (mapOfValues[label] || 0);
    });

    const labels = Object.keys(mapOfLabels).map((label) => (new Date(Number(label))).toISOString());
    const elements = Object.keys(mapOfValues).map((label) => Math.floor(mapOfValues[label] / mapOfLabels[label]));

    const element = document.getElementById('metrics-canvas');
    const context = element.getContext('2d');

    /**
     * { "category": "readings", "timestamp": "20161218T00:00:00Z", "label": "", "value": 1 }
     */
    const Measurement = function () {};

    /**
     * @property category
     */
    Measurement.prototype['category'] = undefined;
    Measurement.prototype['timestamp'] = undefined;
    Measurement.prototype['label'] = undefined;
    Measurement.prototype['value'] = undefined;

    Measurement.constructFromObject = (theSourceObject, theInstanceObject) => {
      if (theSourceObject) {
        theInstanceObject = theInstanceObject || new Measurement();

        if (hasOwnProperty(theSourceObject, 'category')) {
          theInstanceObject['category'] = String(theSourceObject['category']);
        }

        if (hasOwnProperty(theSourceObject, 'timestamp')) {
          theInstanceObject['timestamp'] = Number(theSourceObject['timestamp']);
        }

        if (hasOwnProperty(theSourceObject, 'label')) {
          theInstanceObject['label'] = String(theSourceObject['label']);
        }

        if (hasOwnProperty(theSourceObject, 'value')) {
          theInstanceObject['value'] = Number(theSourceObject['value']);
        }
      }
      return theInstanceObject;
    };

    /**
     * { "title": "", elements: [ Measurement ]}
     */
    const Metrics = function () {};

    /**
     * @property title
     */
    Metrics.prototype['title'] = undefined;
    Metrics.prototype['elements'] = undefined;

    Metrics.constructFromObject = (theSourceObject, theInstanceObject) => {
      if (theSourceObject) {
        theInstanceObject = theInstanceObject || new Metrics();

        if (hasOwnProperty(theSourceObject, 'title')) {
          theInstanceObject['title'] = String(theSourceObject['title']);
        }

        if (hasOwnProperty(theSourceObject, 'elements')) {
          let theElementArray = theSourceObject['elements'];
          theElementArray = Array.isArray(theElementArray) ? theElementArray : [theElementArray];

          theInstanceObject['elements'] = [].concat(theElementArray).map(
            (sourceObject) => Measurement.constructFromObject(sourceObject)
          );
        }

        theInstanceObject = Object.freeze(theInstanceObject);
      }

      return theInstanceObject;
    };

    const MetricsChart = new Chart(context, {
      type: 'bar',
      data: {
        labels: labels,
        datasets: [{
          type: 'line',
          label: 'Readings', //'Forecast',
          backgroundColor: 'rgb(54, 162, 235)',
          borderColor: 'rgb(54, 162, 235)',
          borderWidth: 4,
          data: elements,
          fill: false
        }]
      },
      options: OPTIONS
    });

    return Object.freeze({
      chart: MetricsChart,
      labels: labels,
      metrics: {
        label: 'Metrics per minute',
        elements: elements
      }
    });
  };

  return Object.freeze(MetricsView);
}));
