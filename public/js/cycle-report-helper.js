/* eslint-disable no-undef,no-unused-vars */
if (!Object.prototype.hasOwnProperty.call(this, 'window')) {
  // eslint-disable-next-line no-redeclare
  var moment = require('moment');
  const jQuery = require('jquery');
  const { JSDOM } = require('jsdom');
  const { window } = new JSDOM();

  /* eslint-disable-next-line no-global-assign */
  screen = Object.freeze({ width: 1280, height: 768 });

  $ = jQuery(window);
  getVisibleWidth = () => Math.round(screen.width * 0.58);
  getQueryParams = require('./report-helper').getQueryParams;
  computeCycleMetrics = require('../../lib/cycles.js').computeCycleMetrics;
  getCustomerFilterForSite = require('../../db/customerLPFilters').getCustomerFilterForSite;

  module.exports = { sanityTest, constructMissingStates, sortByStartTime, cycleStats, fixIsEntryIsExitFlags, screen, window };
} else {
  getVisibleWidth = () => Math.round(window.innerWidth * 0.58);
}

let graphWidth = getVisibleWidth();
let graphMargin = 20;
let graphHeight = 40;
let barHeight = 30;

const viewPortHeight = barHeight + graphMargin;
const viewPortWidth = graphWidth + (2 * graphMargin);

let color;
let header = null;
let sticky = null;
let dataRefreshInterval;
let reportView = 'old';
let configObj = null;
let reportCfg = {};
let showAll;
let showInferred;

const MINIMAL_CYCLE_DURATION_IN_MINUTES = 5;

let intervalsBetweenStates = {
  scaleEntry2ScaleExit: {states: {left: 'Scale House Entry 1', right: 'Scale House Exit 1'}, distance: 10},
  entry2Exit: {states: {left: 'Entry A', right: 'Exit A'}, distance: 60},
  entry2ScaleEntry: {states: {left: 'Entry A', right: 'Scale House Entry 1'}, distance: 15},
  scaleExit2Exit: {states: {left: 'Scale House Exit 1', right: 'Exit A'}, distance: 10}
};

try {
  if (getQueryParams()['customer'] !== 'titanamerica') {
    intervalsBetweenStates = {
      scaleEntry2ScaleExit: {states: {left: 'Scale House Entry 1', right: 'Scale House Exit 1'}, distance: 10},
      entry2Exit: {states: {left: 'Entry', right: 'Exit'}, distance: 60},
      entry2ScaleEntry: {states: {left: 'Entry', right: 'Scale House Entry 1'}, distance: 15},
      scaleExit2Exit: {states: {left: 'Scale House Exit 1', right: 'Exit'}, distance: 10}
    };
  }
} catch (e) {
  console.log('ignoring error on init of intervalsBetweenStates. error is:', e);
}

function binarySearchImgKeys(imgKeys, target) {
  let low = 0;
  let high = imgKeys.length - 1;
  let foundElement = false;
  let closest = {timestamp: Infinity};
  let closestIndex = -1;
  while (low <= high) {
    let mid = Math.floor((low + high + 1) / 2);
    if (Math.abs(target - imgKeys[mid].timestamp) < Math.abs(target - closest.timestamp)) {
      closest = imgKeys[mid];
      closestIndex = mid;
    }
    if (imgKeys[mid].timestamp === target) {
      foundElement = true;
      break;

    } else if (imgKeys[mid].timestamp < target) {
      low = mid + 1;
    } else {
      high = mid - 1;
    }
  }
  return foundElement ? closestIndex : (low - 1);
}

function render(data, parameters) {

  let stats = getStats(data);

  let x = d3.scaleTime().domain(
    [moment.tz(stats.shiftStart, timezone).valueOf(), moment.tz(stats.shiftEnd, timezone).valueOf()]
  ).range([graphMargin, graphWidth + graphMargin]);

  let prevDt = null;
  const override_colors = {
    'Entry': '#33cc33',
    'Entry A': '#3399ff',
    'Entry A (left)': '#3399ff',
    'Entry A (right)': '#cc6699',
    'Entry B': '#0033cc',
    'Exit A': '#33cc33',
    'Exit B': '#336600',
    'Unloading': '#0033cc',
    'Untie': '#ff9900',
    'Scale A': '#33ccff',
    'Scale B': '#3399ff',
    'Turnaround': '#ff0000',
    'Exit': '#336600'
  };

  const arrayOfColors = d3.scaleOrdinal(d3.schemePaired).domain(data[0].legends);
  const colorObj = {};
  data[0].legends.forEach((legendObject, index) => {
    const name = legendObject.toLowerCase();
    if (reportCfg[legendObject] && reportCfg[legendObject]['legendColor']) {
      colorObj[name] = reportCfg[legendObject]['legendColor'];
    } else {
      colorObj[name] = override_colors[name] || arrayOfColors(index);
    }
  });

  //cycle
  d3.select('div#cycles').append('div').classed('legend', true);
  let cycleDivs = d3.select('div#cycles').selectAll('div.cycle').data(data);
  let newCycleDivs = cycleDivs.enter().append('div').classed('cycle', true);
  newCycleDivs.append('div').classed('cycleTitle', true);
  newCycleDivs.append('div').classed('stats', true);
  let timeAxes = newCycleDivs.append('div').classed('timeAxis', true);
  timeAxes.append('div').classed('rowLabel', true);

  timeAxes.append('div').classed('chartRow', true).attr('id', 'chartRow')
    .append('svg')
    .attr('width', viewPortWidth)
    .attr('height', viewPortHeight)
    .attr('viewBox', `0 0 ${viewPortWidth} ${viewPortHeight}`);

  timeAxes.append('div').classed('rowNotes', true);
  let mergedCycleDivs = newCycleDivs.merge(cycleDivs);
  mergedCycleDivs.select('div.stats').text(function (d) {
    let durationString = '';
    if (d.iterations.length) {
      let durationObj = moment.duration(d.stats.avgDuration);
      const components = {
        days: durationObj.days(),
        hours: durationObj.hours(),
        minutes: durationObj.minutes()
      };

      const cycleDuration = Object.keys(components).reduce((parts, unit) => {
        const value = components[unit];
        return parts.concat(value ? `${value} ${unit}` : '');
      }, []).join(' ');

      // Not display Average Cycle time in case of Inferred view and showAll view
      if (!showAll && !showInferred) {
        durationString = ', Average Cycle Time: ' + cycleDuration;
      }
    }
    let statsString = 'Cycle Info: ' + d.stats.cycleCount + ' Cycles, ' + d.stats.distinctCycles + ' Distinct Identifiers' + durationString
    + ` - Unique Vehicle Events: ${d.stats.uniqueEntries} Entries, ${d.stats.uniqueExits} Exits, ${d.stats.uniqueIntermediates} Intermediates`;

    return statsString;
  });
  cycleDivs.exit().remove();

  header = document.getElementById('chartRow');
  sticky = header.offsetTop;

  const shiftDurationInDays = moment.tz(stats.shiftEnd, timezone).diff(moment.tz(stats.shiftStart, timezone), 'days');
  let customTicks = (shiftDurationInDays * 2) > 6 ? (shiftDurationInDays * 2) : 12;
  let showAxisDate = shiftDurationInDays > 2;
  //axis
  let timeTicks = x.ticks(customTicks);
  // let timeTicks = x.ticks(12).filter(function(t){return t.getMinutes() == 0;});
  let tickSVG = mergedCycleDivs.select('div.timeAxis svg');
  mergedCycleDivs.select('div.timeAxis div.rowNotes').html('');
  mergedCycleDivs.select('div.timeAxis div.rowNotes').append('div').classed('showAvgDuration', true).text('Average Duration');
  let timeTickLines = tickSVG.selectAll('line').data(timeTicks);
  timeTickLines.enter().append('line').attr('stroke', '#f2f2f2').attr('stroke-width', 1)
    .merge(timeTickLines)
    .attr('x1', x)
    .attr('y1', 33)
    .attr('x2', x)
    .attr('y2', graphHeight);

  let tickLabels = tickSVG.selectAll('text').data(timeTicks);
  tickLabels.enter().append('text')
    .attr('class', 'tickLabelX')
    .attr('y', 0)
    .attr('dy', 10)
    .attr('fill', '#bbb')
    .attr('text-anchor', 'middle')
    .merge(tickLabels)
    .attr('x', x)
    .text(function (d, index) {
      let dt = '';
      if (index > 0 && (moment.tz(d, timezone).format('MM/DD') !== moment.tz(prevDt, timezone).format('MM/DD'))) {
        dt = moment.tz(d, timezone).format('MM/DD');
      }
      prevDt = d;
      return dt + ' ' + moment.tz(d, timezone).format('HH:mm');
    });

  if (showAxisDate) {
    d3.selectAll('.tickLabelX').call(wrap, 10);
  }
  //iteration rows
  let iterationDivs = mergedCycleDivs.selectAll('div.iteration').data(function (d) {
    return d.iterations.filter((element) => {
      return element.subject.toLowerCase().includes(searchText.toLowerCase());
    });

  });
  let newIterationDivs = iterationDivs.enter().append('div').classed('iteration', true);
  let labels = newIterationDivs.append('div').classed('rowLabel', true);
  labels.append('span').classed('identifier', true);
  labels.append('span').classed('cycleCount', true).attr('title', 'Trip Count');

  newIterationDivs.append('div').classed('chartRow', true)
    .append('svg').classed('cycleTimeline', true)
    .attr('height', viewPortHeight)
    .attr('width', viewPortWidth)
    .attr('viewBox', `0 0 ${viewPortWidth} ${viewPortHeight}`)
    .append('g').classed('ticks', true)
    .append('rect')
    .attr('fill', '#eee')
    .attr('x', graphMargin)
    .attr('y', 1)
    .attr('width', graphWidth)
    .attr('height', barHeight);

  newIterationDivs.append('div').classed('rowNotes', true);
  let mergedIterationDivs = newIterationDivs.merge(iterationDivs);
  mergedIterationDivs.select('div.rowLabel span.identifier').text((theCycleObject) => {
    let lpSubject = theCycleObject.subject;
    if (theCycleObject.attributes && theCycleObject.attributes['Truck Type'] && theCycleObject.attributes['Unit Number']) {
      lpSubject += ` (${theCycleObject.attributes['Truck Type'].attr_value} - ${theCycleObject.attributes['Unit Number'].attr_value})`;
    }
    return lpSubject;
  });
  mergedIterationDivs.select('div.rowLabel span.cycleCount').text((theCycleObject) => theCycleObject.cycleNumber > 1 ? theCycleObject.cycleNumber : '');

  if (showAxisDate) {
    let alternateFlag = false;
    // let prevFal = alternateFlag;
    let ticksElements = x.ticks(customTicks);
    let tickObj = ticksElements.map((ticker, index, arr) => {
      if (index % 2 == 0) {
        alternateFlag = !alternateFlag;
      }
      if (index < ticksElements.length - 1) {
        return {start: ticker, end: arr[index + 1], colorChangeFlag: alternateFlag};
      } else {
        return {start: ticker, end: moment.tz(ticker, timezone).add(12, 'hours'), colorChangeFlag: alternateFlag};
      }
    });
    let cycleBars = mergedIterationDivs.select('svg.cycleTimeline').selectAll('rect.time').data(tickObj);
    let newCycleBar = cycleBars.enter().append('rect').classed('time', true).attr('height', barHeight);
    newCycleBar.merge(cycleBars)
      .attr('fill', (d) => d.colorChangeFlag ? '#eee' : '#f2f2f2')
      .attr('x', (y) => x(moment(y.start).valueOf()))
      .attr('y', 1)
      .attr('width', (y) => x(moment(y.end).valueOf()) - x(moment(y.start).valueOf()))
      .attr('height', barHeight);
    cycleBars.exit().remove();
  }
  mergedIterationDivs.select('div.rowNotes').text(function (d) {
    if (reportView == 'new') {
      //average cycle time for vehicle view
      return d.cycle != 0 ? formatDurationAsTime(Math.round(d.totalDuration / d.cycle)) : '';
    } else {
      let totalDuration = calcTotalDuration(d.states);
      return formatDurationAsTime(totalDuration);
    }
  }).attr('title', 'Trip Duration');

  iterationDivs.exit().remove();
  //states for each row
  let stateMarks = mergedIterationDivs.select('svg').selectAll('path').data(function (d) {

    let states = d.states.map(stateObject => {
      stateObject.lastImgKeyForAsset = d.lastImgKeyForAsset;
      Object.defineProperty(stateObject, 'states', { value : d.states, enumerable: false, configurable: true });
      return stateObject;
    });

    return states.filter((state) => state.endTime != null);
  });
  let newStateMarks = stateMarks.enter().append('path')
    .attr('stroke', 'none');
  newStateMarks.append('title');
  let mergedStateMarks = newStateMarks.merge(stateMarks);
  mergedStateMarks
    .attr('d', d3.symbol().size(function (d) {
      if (d.isReportStart || d.isReportEnd) {
        return 100;
      } else {
        return 50;
      }
    }).type(function (d) {
      if (d.isReportStart) {
        return d3.symbolTriangleLeft;
      } else if (d.isReportEnd) {
        return d3.symbolTriangleRight;
      } else {
        return d3.symbolCircle;
      }
    }))
    .attr('fill', function (d) {
      let colorStr = (!d.startTime || !d.endTime) ? 'firebrick' : colorObj[d.state.toLowerCase()];
      return colorStr;
    })
    .attr('opacity', function (d) {
      return d.inferred ? 0.25 : 1.0;
    })
    .attr('stroke', function (d) {
      return d.inferred ? 'black' : 'none';
    })
    .attr('stroke-width', function (d) {
      return d.inferred ? '3' : '0';
    })
    .on('mouseenter', function (d) {
      d3.select(this).raise();
    })
    .on('click', function (d) {
      if (d.inferred) {
        console.log('is inferred event -- no modal available');
        return;
      }
      let listOfImages = [];
      let scaleWeightUnit = configObj[parameters.customer][parameters.facility].hasOwnProperty.call(configObj[parameters.customer][parameters.facility],'scaleWeightUnit') ?
        configObj[parameters.customer][parameters.facility]['scaleWeightUnit'] : 'lbs';

      /**
       * AT-4656: Disable show all images for CTR report, cause miss aligment on the information and
       * confuses the user, this feature needs more details on how to present the data properly.
       */
      const isCurrentElement = (element) => element === d;
      d.states.filter(isCurrentElement).forEach(function (obj) {
        let lpr_img_obj = obj.img_keys['lpr'];

        let asset_img_obj = obj.lastImgKeyForAsset.length != 0 ? [obj.lastImgKeyForAsset[obj.lastImgKeyForAsset.length - 1]] : obj.img_keys['asset'];
        if (reportCfg[obj.state]) {
          let imageKeysArr = Object.keys(obj.img_keys);
          let reportCfgImgArr = Object.keys(reportCfg[obj.state]);

          if (lpr_img_obj[0]) {
            const attributes = d.attributes;
            listOfImages.push({
              linkName: `LP: ${obj.subject}`,
              imgKey: lpr_img_obj[0],
              header: `Time: ${moment.tz(lpr_img_obj[0].collection_time, timezone).format('YYYY-MM-DD HH:mm z')}`,
              customerTag: {
                tagArr: obj.customerTags,
                subject: obj.subject
              }
            });
            if (attributes && attributes['Truck Type'] && attributes['Unit Number']) {
              listOfImages[0].info = `Company: ${attributes['Truck Type'].attr_value} ${attributes['Unit Number'].attr_value}`;
            }
          } else if (asset_img_obj[0]) {  //If LPR image is missing try to fetch asset image instead
            listOfImages.push({
              linkName: `LP: ${obj.subject}`,
              imgKey: asset_img_obj[0],
              header: `Time: ${moment.tz(asset_img_obj[0].collection_time, timezone).format('YYYY-MM-DD HH:mm z')})`,
              customerTag: {
                tagArr: obj.customerTags,
                subject: obj.subject
              }
            });
          }

          let scale_img_obj = obj.img_keys['weight'];
          if (scale_img_obj[0]) {
            let scale_header = {};
            if (obj.warning) {
              scale_header.Warning = 'Potential Error. (' + obj.warning + ')';
            }
            scale_header.collection_time = moment.tz(scale_img_obj[0].collection_time, timezone).format('YYYY-MM-DD HH:mm z');
            listOfImages.push({
              linkName: 'Scale: ' + ('weight' in obj ? `${obj.weight} ${scaleWeightUnit}` : 'N/A'),
              imgKey: scale_img_obj[0],
              header: `Time: ${scale_header.collection_time}`
            });
          }

          let ocr_img_obj = obj.img_keys['ocr'];
          if (ocr_img_obj && ocr_img_obj[0]) {
            let ocr_header = {};
            ocr_header.collection_time = moment.tz(ocr_img_obj[0].collection_time, timezone).format('YYYY-MM-DD HH:mm z');
            listOfImages.push({
              linkName: `${reportCfg[obj.state].imgConfig.ocr_label || 'Container No.'} ` + ('ocr_value' in obj ? `${obj.ocr_value}` : 'N/A'),
              imgKey: ocr_img_obj[0],
              header: `Time: ${ocr_header.collection_time}`
            });
          }

          let truckBedImg = imageKeysArr.find((imageKeyObject) => reportCfgImgArr.some(reportConfigImageObject => imageKeyObject == reportConfigImageObject));
          if (truckBedImg) {
            const bedImgName = reportCfg[obj.state][truckBedImg]['name'];
            const bedImgLabel = reportCfg[obj.state][truckBedImg]['cs_label'];
            // const bedImgPosition = Number(reportCfg[obj.state][truckBedImg]['position']);

            if (d.img_keys[bedImgName]) {

              const lookbackDurationInSeconds = reportCfg[obj.state][bedImgName]['look_before_duration_in_seconds'] || 0;
              const lookaheadDurationInSeconds = reportCfg[obj.state][bedImgName]['look_after_duration_in_seconds'] || 60;

              if (lpr_img_obj[0]) {
                lpr_img_obj[0].timestamp = new Date(lpr_img_obj[0].collection_time);

                for(let index = 0; index < d.img_keys[bedImgName].length; index++) {
                  d.img_keys[bedImgName][index].timestamp = new Date(d.img_keys[bedImgName][index].collection_time);
                }

                if (lookaheadDurationInSeconds > 0) {

                  const lookaheadTimestampTarget = new Date(
                    lpr_img_obj[0].timestamp.getTime() + lookaheadDurationInSeconds * 1000);

                  const closestLookaheadTruckbedIndex = binarySearchImgKeys(d.img_keys[bedImgName], lookaheadTimestampTarget);

                  const lastNelementsToRemove = d.img_keys[bedImgName].length - (closestLookaheadTruckbedIndex  + 1);

                  // Removing Truckbed images captured after Look Ahead period
                  d.img_keys[bedImgName].splice(-1  * lastNelementsToRemove, lastNelementsToRemove);
                }
                if (lookbackDurationInSeconds > 0) {

                  const TargetLookbackTimestamp = new Date(lpr_img_obj[0].timestamp - lookbackDurationInSeconds * 1000);

                  const closestLookbackTruckbedIndex = binarySearchImgKeys(d.img_keys[bedImgName], TargetLookbackTimestamp);

                  // Removing Truckbed images captured before Look Back period
                  d.img_keys[bedImgName].splice(0, closestLookbackTruckbedIndex);
                }
              }

              for(let index = 0; index < d.img_keys[bedImgName].length; index++) {
                listOfImages.push({
                  linkName: `${moment.tz(d.img_keys[bedImgName][index].collection_time).format('HH:mm:ss')}`,
                  imgKey: d.img_keys[bedImgName][index],
                  header: `Time: ${moment.tz(d.img_keys[bedImgName][index].collection_time, timezone).format('HH:mm z')}`
                });
              }
            }
          }
        }
      });
      const correctionText = 'weight' in d ? `${d.weight} ${scaleWeightUnit}` : 'N/A';
      d3.select('#correctionTxt').attr('placeholder', correctionText);

      listOfImages.forEach((imageModalElement) => {
        const { imgKey: imageObject, linkName: measure } = imageModalElement;
        imageObject.measure = measure;
        if (!imageModalElement.linkName) {
          imageModalElement.linkName = `${moment.tz(imageObject.collection_time, timezone).format('HH:mm:ss')}`;
        }
      });

      renderImgModal(listOfImages);
    })
    .attr('transform', function (d) {
      let start = d.startTime ? moment.tz(d.startTime, timezone).toDate() : moment.tz(d.endTime, timezone).subtract(15, 'minutes').toDate();
      if (d.isReportStart) {
        return 'translate(' + Number(x(start) - 5) + ',' + (20) + '), rotate(360)';
      } else if (d.isReportEnd) {
        return 'translate(' + Number(x(start) + 5) + ',' + (20) + '), rotate(-360)';
      } else {
        return 'translate(' + Number(x(start) + 0) + ',' + (20) + ')';
      }
    });
  mergedStateMarks.select('title').text(function (d) {
    return d.state + ': ' + moment.tz(d.startTime, timezone).format('HH:mm z');
  });
  stateMarks.exit().remove();

  //the timeline bar
  let ticks = mergedIterationDivs.select('svg.cycleTimeline g.ticks').selectAll('line.tick').data(timeTicks);
  ticks.enter().append('line')
    .classed('tick', true)
    .attr('stroke', '#e6e6e6')
    .attr('stroke-width', 1)
    .attr('y1', 0)
    .attr('y2', viewPortWidth)
    .merge(ticks)
    .attr('x1', x)
    .attr('x2', x)
    .on('mouseover', function (d) {
      div.transition()
        .duration(200)
        .style('opacity', 1);
      div.html(moment.tz(d, timezone).format('HH:mm z'))
        .style('left', (d3.event.pageX) + 'px')
        .style('top', (d3.event.pageY - 28) + 'px');
    })
    .on('mouseout', function (d) {
      div.transition()
        .duration(400)
        .style('opacity', 0);
    });
  //legend
  let legendEntry = d3.select('div.legend').selectAll('div.legendEntry').data(data[0].legends);
  let newLegendEntries = legendEntry.enter().append('div').classed('legendEntry', true);
  newLegendEntries.append('svg').classed('legendSwatch', true)
    .append('path')
    .attr('d', d3.symbol().size(40).type(d3.symbolSquare))
    .attr('stroke', 'none')
    .attr('transform', 'translate(5,5)')
    .attr('fill', function (d) {
      return colorObj[d.toLowerCase()];
    });
  newLegendEntries.append('div').classed('legendLabel', true).text((element) => element);

  //weights and OCR labels
  let attrLabels = mergedIterationDivs.select('svg').selectAll('text.attrLabel').data(
    d => d.states.filter((state) =>
      ('weight' in state && state.weight && state.weight != null) ||
      ('ocr_value' in state && state.ocr_value && state.ocr_value != null)
    ));

  attrLabels.enter().append('text')
    .classed('attrLabel', true)
    .attr('y', (graphHeight + 2) / 2.0)
    .attr('dx', 3)
    .attr('fill', '#333')
    .attr('text-anchor', 'left')
    .merge(attrLabels)
    .attr('x', d => x(moment.tz(d.startTime, timezone).toDate()))
    .attr('dy', (d, i, n) => {
      let textOffset = 4;
      let labelAreaHeight = graphHeight;
      return textOffset + ((i % 2) ? (labelAreaHeight / 2 - textOffset) : (-1 * (labelAreaHeight / 2 - textOffset)));
    }) //alternating. evenly distributed would be: 4 + (22 * (i+1)/(n.length+1) - 11)
    .text(d => {
      if(d.weight) {
        return d.weight.toLocaleString();
      } else {
        return d.ocr_value.toLocaleString();
      }
    })
    .classed('gray', function (d) {
      if (d.warning) {
        return true;
      }
    })
    .classed('red', function (d) {
      let maxWeightThreshold = Object.prototype.hasOwnProperty.call(
        configObj[parameters.customer][parameters.facility], 'maxWeightThreshold') ? configObj[parameters.customer][parameters.facility]['maxWeightThreshold'] : 80000;
      if (d.weight > maxWeightThreshold && d.warning == null) {
        return true;
      }
    });
  attrLabels.exit().remove();
}

function constructAndRenderImgModal(d, reverse = false, deliveryHeader=null) {
  let theCycleObject = JSON.parse(JSON.stringify(d));
  let listOfImages = [];
  const theSelctedImageIndex = reverse ? theCycleObject.states.length - 1 : 0;
  const subject = theCycleObject.subject || theCycleObject.subjectId;
  const image = d.states[theCycleObject.states.length - 1].img_keys['lpr'][0];
  theCycleObject.image = image;

  if (theCycleObject.images && Object.keys(theCycleObject.images).length > 0) {
    theCycleObject.images.forEach(function (imageObject) {
      listOfImages.push({
        linkName: moment.tz(imageObject.collection_time, timezone).format('HH:mm'),
        imgKey: imageObject,
        url: newImageComponent(parameters.customer, imageObject).toImageURL(),
        header: `License Plate: ${subject}`,
        customerTag: {
          tagArr: theCycleObject.states[theSelctedImageIndex].customerTags,
          subject: subject
        }
      });
    });
  } else {
    listOfImages.push({
      imgKey: theCycleObject.states[theSelctedImageIndex].img_keys['lpr'][0],
      header: `License Plate: ${subject}`,
      linkName: `${moment.tz(theCycleObject.image.collection_time, timezone).format('HH:mm:ss')}`,
      customerTag: {
        tagArr: theCycleObject.states[theSelctedImageIndex].customerTags,
        subject: subject
      }
    });
    listOfImages[0].imgKey.measure = subject;
  }
  // imgObj.imgKey.gateway_id =
  renderImgModal(listOfImages, deliveryHeader);
}

async function reload(parameters) {
  //Fetch config
  await fetchCfg(parameters);
  configObj = await fetchYardCfg(parameters);

  //Display date on page
  if (!latestConfig) {
    constructDatePicker(parameters);
  }
  const paramsString = Object.keys(parameters).map(function (key) {
    return encodeURIComponent(key) + '=' + encodeURIComponent(parameters[key]);
  }).join('&');
  if (liveMode) {
    d3.select('#reportDateTitle div.date').text(`Today, ${moment.tz(new Date(), timezone).format('hh:mm a z')}`);
  } else {
    d3.select('#reportDateTitle div.date').text('');
  }
  const url = '/portal/data/cycle/' + parameters.customer + '?' + paramsString;
  let data = await d3.json(url, {credentials: 'include'});
  return data;
}

function convertSingleStateToCycle(data) {
  // Only Keep groups of two consecutive states as a cycle
  // and putting the rest of the states as a separate iteration
  for (let i = 0; i < data[0].iterations.length; ++i) {
    if (data[0].iterations[i].states.length > 2) {
      let restStates = Object.assign({}, data[0].iterations[i], {});
      restStates.states = data[0].iterations[i].states.splice(2);
      data[0].iterations.push(restStates);
    }
  }

  // Making the even number states as the End cycle
  data[0].iterations.forEach(iter => {
    for (let i = 0; i < iter.states.length; i++) {
      let nextState = iter.states[i + 1];
      let prevState = iter.states[i];
      let singleStateEndCycleStateName = 'Exit';
      let maxCycleDuration = 360;
      if (parameters.customer in configObj && parameters.facility in configObj[parameters.customer]) {
        if ('singleStateEndCycleStateName' in configObj[parameters.customer][parameters.facility]) {
          singleStateEndCycleStateName = configObj[parameters.customer][parameters.facility]['singleStateEndCycleStateName'];
        }
        if ('maxCycleDuration' in configObj[parameters.customer][parameters.facility]) {
          maxCycleDuration = configObj[parameters.customer][parameters.facility]['maxCycleDuration'];
        }
      }
      if (iter.states.length === 1) {
        prevState.is_end = false;
      }
      if (nextState && prevState) {
        let timeBetween = moment(nextState.startTime).diff(moment(prevState.startTime), 'minutes');
        if (nextState.is_start && nextState.is_end && prevState.state !== singleStateEndCycleStateName) {
          if (timeBetween < maxCycleDuration) {
            nextState.state = singleStateEndCycleStateName;
            nextState.is_start = false;
            prevState.is_end = false;
            if (!data[0].legends.includes(singleStateEndCycleStateName)) {
              data[0].legends.push(singleStateEndCycleStateName);
            }
          } else {
            prevState.is_end = false;
            nextState.is_end = false;
          }
        }
      } else if (prevState && prevState.is_start && prevState.is_end && prevState.state !== singleStateEndCycleStateName) {
        prevState.is_end = false;
      }
    }
  });
  return data;
}

function keepOnlyCycles(data) {
  const { customer, facility } = parameters;
  const theConfigObject = configObj[customer][facility];
  return filterIncompleteCycles(data, theConfigObject);
}

function filterIncompleteCycles(data, { isEntryRequired, isExitRequired }) {
  let iterations = data.iterations.filter(
    /** Keep only events with states  */
    ({ states }) => states && states.length
  ).filter(
    /** Keep only events with at least one state in one of the boundaries */
    ({ states }) => states.some((state) => state.is_start || state.is_end)
  );

  if (isEntryRequired) {
    iterations = iterations.filter(
      ({ states }) => states.some((state) => state.is_start)
    );
  }
  if (isExitRequired) {
    iterations = iterations.filter(
      ({ states }) => states.some((state) => state.is_end)
    );
  }

  iterations = iterations.filter(({ states }) => {
    const timeInfo = {
      endTime: Number.NEGATIVE_INFINITY,
      startTime: Number.POSITIVE_INFINITY
    };
    const uniqueStates =  states.reduce((setOfStates, { state, startTime, endTime }) => {
      endTime = Date.parse(endTime);
      startTime = Date.parse(startTime);

      if (!Object.prototype.hasOwnProperty.call(timeInfo, 'endTime')) {
        Object.assign(timeInfo, { endTime });
      }
      if (!Object.prototype.hasOwnProperty.call(timeInfo, 'startTime')) {
        Object.assign(timeInfo, { startTime });
      }

      setOfStates.add(state);
      Object.assign(timeInfo, {
        endTime: Math.max(timeInfo.endTime, endTime),
        startTime: Math.min(timeInfo.startTime, startTime)
      });

      return setOfStates;
    }, new Set());

    const totalDuration = moment.duration(timeInfo.endTime - timeInfo.startTime);
    const requiredCount = totalDuration.asMinutes() > MINIMAL_CYCLE_DURATION_IN_MINUTES ? 1 : 2;

    return uniqueStates.size > requiredCount;
  });

  //calculate cycle count after filtering cycles
  const mapOfSubjects = iterations.reduce((mapOfSubjects, eventObject) => {
    const { subject } = eventObject;

    if (!Object.prototype.hasOwnProperty.call(mapOfSubjects, subject)) {
      mapOfSubjects[subject] = 0;
    }

    mapOfSubjects[subject] += 1;
    eventObject.cycle = mapOfSubjects[subject];

    return mapOfSubjects;
  }, {});

  return Object.assign({}, data, { iterations, _metrics: mapOfSubjects });
}

function inferCycles(data) {
  data.iterations = data.iterations.map((iterationObject) => constructMissingStates(iterationObject, reportCfg));
  return data;
}

function constructMissingStates(iteration, oReportConfig) {
  let states = iteration.states.slice();

  const filterConfigByState = (selectedState) => states.filter(({ state }) => {
    return oReportConfig[state]
        && oReportConfig[state].inferredState
        && oReportConfig[state].inferredState.type === selectedState;
  });

  let scale_entry = filterConfigByState('Scale_entry');
  let scale_exit = filterConfigByState('Scale_exit');
  let entry = filterConfigByState('Entry');
  let exit = filterConfigByState('Exit');
  if (exit.length) {
    exit = [exit[0]];
  }
  //Create Scales Cycle if either scale state is found
  if (scale_entry.length && !scale_exit.length) {
    scale_exit = scale_entry.map(s_entry => createForwardState(s_entry, oReportConfig[s_entry['state']]['inferredState']['scaleEntry2ScaleExit']));
    states = states.concat(scale_exit);
  } else if (scale_exit.length && !scale_entry.length) {
    const foo = scale_exit.map(s_exit => createBackwardState(s_exit, oReportConfig[s_exit['state']]['inferredState']['scaleEntry2ScaleExit']));
    scale_entry = foo;
    states = states.concat(foo);
  }

  //Create Entry/Exit based if Scale Cycle is present
  if (scale_entry.length && !entry.length) {
    const foo = scale_entry.map(s_entry => createBackwardState(s_entry, oReportConfig[s_entry['state']]['inferredState']['entry2ScaleEntry']));
    entry = foo;
    states = states.concat(foo);
  }
  if (scale_exit.length && !exit.length) {
    const foo = scale_exit.map(s_exit => createForwardState(s_exit, oReportConfig[s_exit['state']]['inferredState']['scaleExit2Exit']));
    exit = foo;
    states = states.concat(foo);
  }

  //Create Entry/Exit cycle if either of them are present irrespective of scale
  if (entry.length && !exit.length) {
    const newExits = entry.map(entr => createForwardState(entr, oReportConfig[entr['state']]['inferredState']['entry2Exit']));
    states = states.concat(newExits);
  } else if (exit.length && !entry.length) {
    const newEntries = exit.map(ex => createBackwardState(ex, oReportConfig[ex['state']]['inferredState']['entry2Exit']));
    states = states.concat(newEntries);
  }

  //Push created states into original states array
  states = sortByStartTime(states);
  states = fixIsEntryIsExitFlags(states);
  iteration.states = states;
  return iteration;
}

function fixIsEntryIsExitFlags(orig) {
  //set the is_start and is_end flag for the inferred states
  const out = orig.map(s => {
    if (s.inferred && s.state.startsWith('Entry')) {
      // if s has entry state name then add is_start true, is_end false
      s.is_start = true;
      s.is_end = false;
    } else if (s.inferred && s.state.startsWith('Exit')) {
      // if s has exit state name then add is_start false, is_end true
      s.is_start = false;
      s.is_end = true;
    } else if (s.inferred) {
      // is_start false, is_end false
      s.is_start = false;
      s.is_end = false;
    }
    return s;
  });
  return out;
}

function sortByStartTime(orig) {
  const copy = orig.slice();
  copy.sort((a, b) => {
    const aa = moment.utc(a.startTime);
    const bb = moment.utc(b.startTime);
    const out = aa.isBefore(bb);
    return out ? -1 : 1;
  });
  return copy;
}

let offsetTime = function (time, interval) {
  return moment.utc(time).add(interval, 'minutes').format('YYYY-MM-DD[T]HH:mm:ss.SSS[Z]');
};

function createForwardState(state, interval) {
  let inferredState = Object.assign({}, state, {
    startTime: offsetTime(state.startTime, interval.distance),
    endTime: offsetTime(state.endTime, interval.distance),
    state: interval.states.right,
    inferred: true
  });
  delete inferredState.weight;
  return inferredState;
}

function createBackwardState(state, interval) {
  let inferredState = Object.assign({}, state, {
    startTime: offsetTime(state.startTime, -1 * interval.distance),
    endTime: offsetTime(state.endTime, -1 * interval.distance),
    state: interval.states.left,
    inferred: true
  });
  delete inferredState.weight;
  return inferredState;
}

function cycleStats(data) {
  let cycle = Object.assign({}, data);
  const listOfIterations = cycle.iterations.filter((iteration) => iteration.states.some(({ is_start, is_end }) => is_start || is_end));

  const theLowerBound = 0;
  const theUpperBound = moment.now().valueOf();
  const iterationStats = listOfIterations.map(theCycleObject => {
    const theCycleStates = theCycleObject.states;
    const theStartTime = theCycleStates.reduce((current, { startTime }) => Math.min(current, moment(startTime).valueOf()), theUpperBound);
    const theFinishingTime = theCycleStates.reduce((current, { endTime }) => Math.max(current, moment(endTime).valueOf()), theLowerBound);
    return { duration: theFinishingTime - theStartTime };
  });

  const distinctCycles = data.iterations.reduce((mapOfCycleSubjects, { subject }) => Object.assign(mapOfCycleSubjects, { [subject]: true }), {});
  const countOfElements = iterationStats.reduce((currentTotal, { duration }) => currentTotal + (duration ? 1 : 0), 0);
  const totalDuration = iterationStats.reduce((currentTotal, { duration }) => currentTotal + duration, 0);

  let LPSet = new Set();
  let uniqueEntries = 0;
  let uniqueExits = 0;
  let uniqueIntermediates = 0;

  data.iterations.forEach(lp => {
    if(!LPSet.has(lp.subject)) {
      lp.states.forEach(state => {
        if (state.is_start) {
          ++uniqueEntries;
        } else if (state.is_end) {
          ++uniqueExits;
        } else {
          ++uniqueIntermediates;
        }
      });
    }
    LPSet.add(lp.subject);
  });

  data.stats = {
    avgDuration: countOfElements > 0 ? (totalDuration / countOfElements) : 0,
    distinctCycles: Object.keys(distinctCycles).length,
    cycleCount: cycle.iterations.length,
    uniqueEntries: uniqueEntries,
    uniqueExits: uniqueExits,
    uniqueIntermediates: uniqueIntermediates
  };
  return data;
}

function freezeTimeline() {
  if (window.pageYOffset > sticky) {
    header.classList.add('freeze');
  } else {
    header.classList.remove('freeze');
  }
}

function getStats(data) {
  let statesHash = {};
  let minCycleIds = {};
  if (data) {
    data.forEach(function (cycle) {
      cycle.iterations.forEach(function (iteration) {
        minCycleIds[iteration.subject] = Math.min(minCycleIds[iteration.subject] || 0, iteration.cycle);
        iteration.states.forEach(function (state) {
          statesHash[state] = true;
        });
      });
    });

    //normalize the cycle count
    data.forEach(function (cycle) {
      cycle.iterations.forEach(function (iteration) {
        iteration.cycleNumber = iteration.cycle - minCycleIds[iteration.subject];
      });
    });
  }

  return {
    states: Object.values(statesHash),
    shiftStart: data[0] && data[0].shiftStart,
    shiftEnd: data[0] && data[0].shiftEnd
  };
}

function sanityTest() {
  return 'sane';
}

async function handleIntervalFired(parameters) {
  latestConfig = await getLatestConfig();
  if (latestConfig) {
    if (latestConfig.config.landing_page != landingPage) {
      location.reload();
    }
    parameters.facility = latestConfig.config.facility;
    parameters.customer = latestConfig.cgr;
    urlParams = Object.assign(urlParams, parameters);
  } else {
    await reloadCustomerTags(parameters);
  }
  let data = await reload(parameters);
  let onlyKeep = function (i, qualifier, attrValues) {
    return i.attributes && i.attributes[qualifier] && attrValues.includes(i.attributes[qualifier]['attr_value']);
  };

  data = data.map(d => {
    let out = d;

    // if Trimac & is a Trailer report, then filter out anything that isn't a TRAILER
    if (d.facility.toLocaleUpperCase() === 'TRIMAC TRAILERS') {
      out = Object.assign(d, {iterations: d.iterations.filter(f => onlyKeep(f, 'Type', 'TRAILER'))});
    }

    // if Trimac & is a Tractor (or Truck) report, then filter out anything that isn't a TRACTOR
    if (d.facility.toLocaleUpperCase() === 'TRIMAC TRUCKS') {
      out = Object.assign(d, {iterations: d.iterations.filter(f => onlyKeep(f, 'Type', 'TRACTOR'))});
    }

    if (d.facility.toLocaleUpperCase() === 'NTS TRUCK') {
      out = Object.assign(d, {iterations: d.iterations.filter(f => onlyKeep(f, 'vehicle_class', ['front', 'detached']))});
    }

    if (['NTS TRAILER', 'WASHBAY 5', 'WASHBAY 3', 'WASHBAY 4'].includes(d.facility.toLocaleUpperCase())) {
      out = Object.assign(d, {iterations: d.iterations.filter(f => onlyKeep(f, 'customer_vehicle_type', ['trailer']) || onlyKeep(f, 'vehicle_class', ['back']))});
    }

    return out;
  });

  if (filterOnTags.length) {
    data[0].iterations = data[0].iterations.filter(function (d) {
      return d.customerTags.some(function (tag) {
        return filterOnTags.includes(tag);
      });
    });
  }
  if (searchText != '') {
    data[0].iterations = globalData[0].iterations.filter(function (d) {
      return d.subject.toLowerCase().includes(searchText.toLowerCase());
    });
  }

  const iterations = data[0].iterations;
  const states = iterations.length && iterations[0].states;
  if (states.length && states[0].is_start && states[0].is_end) {
    data = convertSingleStateToCycle(data);
  }

  data[0] = showAll ? data[0] : showInferred ? inferCycles(data[0]) : keepOnlyCycles(data[0]);

  //Calculate cycle stats for filtered data
  data[0] = cycleStats(data[0]);

  data[0].iterations.forEach(x => {
    x.states[0].isReportStart = true;
    x.states[x.states.length - 1].isReportEnd = true;
  });

  if (latestConfig) {
    if (latestConfig && liveMode) {
      data[0].iterations = data[0].iterations.reverse();
    }
  } else {
    if (sortOrder) {
      data[0].iterations = data[0].iterations.reverse();
    }
  }

  if (reportView == 'new') {
    data = mergeStatesOfTruck(data);
  }
  if (!data || data[0].iterations.length == 0) {
    showNoDataIndicator();
    return;
  } else {
    hideNoDataIndicator();
  }
  render(data, parameters);
}

function mergeStatesOfTruck(data) {
  let uniqueLP = [], uniqueLPObj = [];
  let reportData = data.slice(0);
  let d = reportData[0].iterations;
  for (let i in d) {
    if (uniqueLP.some(x => x === d[i].subject)) {

      for (let j in uniqueLPObj) {
        if (uniqueLPObj[j].subject === d[i].subject) {
          let duration = calcTotalDuration(d[i].states);
          let avg = (duration + uniqueLPObj[j].totalDuration);
          uniqueLPObj[j].states = d[i].states.concat(uniqueLPObj[j].states);
          uniqueLPObj[j].cycle = d[i].cycle;
          uniqueLPObj[j].totalDuration = avg;
        }
      }
    } else {
      uniqueLP.push(d[i].subject);
      let duration = calcTotalDuration(d[i].states);
      d[i].totalDuration = duration;
      uniqueLPObj.push(d[i]);
    }
  }
  data[0].iterations = uniqueLPObj;
  return data;
}

async function updateReport(obj) {
  // let btnObj = d3.select(obj);
  d3.selectAll('#cycleNav button.report-type').each(function (d) {
    d3.select(this).classed('active-link', false);
  });

  d3.select(obj).classed('active-link', true);
  let statsData = JSON.parse(JSON.stringify(globalData));
  statsData[0] = showAll ? statsData[0] : showInferred ? inferCycles(statsData[0]) : keepOnlyCycles(statsData[0]);
  //Calculate cycle stats for filtered data
  statsData[0] = cycleStats(statsData[0]);

  statsData[0].iterations.forEach(x => {
    x.states[0].isReportStart = true;
    x.states[x.states.length - 1].isReportEnd = true;
  });
  if ($(obj).text() === 'Vehicles') {
    reportView = 'new';
    statsData = mergeStatesOfTruck(statsData);
  } else if ($(obj).text() === 'Trips') {
    reportView = 'old';
  }

  if (sortOrder) {
    statsData[0].iterations = statsData[0].iterations.reverse();
  }
  render(statsData, parameters);
}

function updateCycles(obj) {
  d3.selectAll('#cycleType button.cycle-type').each(function (d) {
    d3.select(this).classed('active-link', false);
  });

  d3.select(obj).classed('active-link', true);
  let cycleData = JSON.parse(JSON.stringify(globalData));
  if ($(obj).text() === 'Complete') {
    showInferred = false;
  } else if ($(obj).text() === 'Inferred') {
    showInferred = true;
  }

  // statsData[0] = convertSingleStateToCycle(statsData[0]);
  cycleData[0] = showAll ? cycleData[0] : showInferred ? inferCycles(cycleData[0]) : keepOnlyCycles(cycleData[0]);
  //Calculate cycle stats for filtered data
  cycleData[0] = cycleStats(cycleData[0]);

  cycleData[0].iterations.forEach(x => {
    x.states[0].isReportStart = true;
    x.states[x.states.length - 1].isReportEnd = true;
  });

  if (reportView == 'new') {
    cycleData = mergeStatesOfTruck(cycleData);
  }

  if (sortOrder) {
    cycleData[0].iterations = cycleData[0].iterations.reverse();
  }
  render(cycleData, parameters);
}

async function cycleReport(parameters) {
  showAll = parameters.showAll ? JSON.parse(parameters.showAll) : false;

  setTimeout(() => cycleReport(parameters), 60 * 1000);

  let theLatestConfig = await getLatestConfig();
  if (!theLatestConfig) {
    await reloadCustomerTags(parameters);
    renderCustomerTagsDropDown();
  } else {
    d3.select('div.cycleTitle').text('Facility: ' + parameters.facility);
  }
  let rawDataObj = await reload(parameters);
  $('.loading').remove();

  const customer = Object.assign({}, { customer: '' }, parameters);

  if (rawDataObj.length) {
    const site = parameters.facility.toLocaleUpperCase();
    rawDataObj = rawDataObj.filter((record) => getCustomerFilterForSite(customer, site, record));

    const iterations = rawDataObj[0].iterations;
    const states = iterations.length && iterations[0].states;
    if (states.length && states[0].is_start && states[0].is_end) {
      rawDataObj = convertSingleStateToCycle(rawDataObj);
    }

    globalData = JSON.parse(JSON.stringify(rawDataObj));

    rawDataObj[0] = showAll ? rawDataObj[0] : showInferred ? inferCycles(rawDataObj[0]) : keepOnlyCycles(rawDataObj[0]); //FIXME

    //Calculate cycle stats for filtered data
    rawDataObj[0] = cycleStats(rawDataObj[0]);

    rawDataObj[0].iterations.forEach(x => {
      x.states[0].isReportStart = true;
      x.states[x.states.length - 1].isReportEnd = true;
    });

    if (reportView == 'new') {
      rawDataObj = mergeStatesOfTruck(rawDataObj);
    }
    let tempData = JSON.parse(JSON.stringify(rawDataObj));
    if (sortOrder) {
      tempData[0].iterations = tempData[0].iterations.reverse();
    }
    if (tempData[0].iterations.length) {
      render(tempData, parameters);
    } else {
      showNoDataIndicator();
    }

  } else {
    showNoDataIndicator();
  }
}



async function fetchCfg({ customer, facility, date = null }) {
  urlParams = Object.assign(urlParams, { facility, date });
  const paramsString = Object.keys(urlParams).filter(
    (key) => ['number', 'string', 'boolean'].includes(typeof urlParams[key])
  ).map(
    (key) => encodeURIComponent(key) + '=' + encodeURIComponent(urlParams[key])
  ).join('&');

  let cfg = await d3.json('/portal/config/getConfig/' + customer + '?' + paramsString, { credentials: 'include' });
  cfg.forEach(row => {
    let state = row.measure_name;
    if (!Object.keys(reportCfg).some(s => s == state)) {
      reportCfg[state] = {};
    }

    //Put values
    reportCfg[state][row.gateway_name] = row.cs_config ? row.cs_config.imageConfig : null;
    reportCfg[state]['legendColor'] = row.cs_config ? row.cs_config.legendColor : null;
    reportCfg[state]['imgConfig'] = row.cs_config ? row.cs_config.imageConfig : null;
    reportCfg[state]['inferredState'] = row.cs_config && row.cs_config.inferredState ? row.cs_config.inferredState : null;
  });
}


function searchData(data) {
  return data.filter(x => {
    return x.subjectId.toLowerCase().includes(searchText.toLowerCase());
  });
}


function onlyKeepRecentRecords(hoursToLookBack) {
  return function (d) {
    return moment.tz(d.states[0].lastSeen, timezone).add(hoursToLookBack, 'hours').toDate() > (parameters.time ? moment.tz(parameters.time, timezone) : moment.tz(moment(), timezone));
  };
}

function removeExitRecords(d) {
  return (!d.states[0].isEnd && d.states[0].img_keys != null);
}

function inProgressRecords(d, hoursToLookBack) {
  return d.subjects.filter(removeExitRecords)
    .filter(onlyKeepRecentRecords(hoursToLookBack));
}

function hideNoDataIndicator() {
  d3.select('#noDataIndicator').style('display', 'none');
}

function showNoDataIndicator() {
  d3.select('#noDataIndicator').style('display', 'block');
}

function showNoShiftConfiguredMsg() {
  d3.select('#message').style('display', 'block');
}

async function filterByState(state) {
  filterState.push(state);
  let filterData = JSON.parse(JSON.stringify(globalData));
  if (reportType == 1) {
    if (filterState.length) {
      filterData[0].iterations = filterData[0].iterations.filter(function (d) {
        d.states = d.states.filter(function (y) {
          return filterState.some(x => x === y.state);
        });
        if (d.states.length) {
          return d;
        }
      });
    } else {
      filterData = JSON.parse(JSON.stringify(globalData));
    }
  }
  let filterStats = [];

  //Calculate cycle stats for filtered data
  filterStats[0] = cycleStats(filterStats[0]);

  render(filterStats, parameters);
}

function removeItem(arr) {
  var what, a = arguments, L = a.length, ax;
  while (L > 1 && arr.length) {
    what = a[--L];
    while ((ax = arr.indexOf(what)) !== -1) {
      arr.splice(ax, 1);
    }
  }
  return arr;
}

function expandFilter() {
  let selectPicker = d3.select('.dropdown');
  if (selectPicker.classed('shrink-filter')) {
    selectPicker.classed('shrink-filter', false).classed('expand-filter', true);
  } else {
    selectPicker.classed('shrink-filter', true).classed('expand-filter', false);
  }
}

function calcTotalDuration(states) {

  let limits = states.reduce(function (theDuractionBounds, { startTime, endTime }) {
    const theStartTime = moment(startTime).valueOf();
    const theFinishingTime = moment(endTime).valueOf();

    theDuractionBounds.min = Math.min(theDuractionBounds.min, moment(theStartTime).valueOf());
    theDuractionBounds.max=  Math.max(theDuractionBounds.max, moment(theFinishingTime).valueOf());

    return theDuractionBounds;
  }, { min: Infinity, max: -Infinity });
  let avgDuration = Math.round((limits.max - limits.min));
  return avgDuration;
}

async function sortCycleTimeReport() {
  sortOrder = !sortOrder;
  let sortedData = JSON.parse(JSON.stringify(globalData));

  if (sortOrder) {
    $('.sort-desc').show();
    $('.sort-asc').hide();
    sortedData[0].iterations = sortedData[0].iterations.reverse();
  } else {
    $('.sort-desc').hide();
    $('.sort-asc').show();
    sortedData[0].iterations = sortedData[0].iterations.reverse().reverse();
  }

  //Calculate cycle stats for filtered data
  sortedData[0] = showAll ? sortedData[0] : showInferred ? inferCycles(sortedData[0]) : keepOnlyCycles(sortedData[0]);

  sortedData[0] = cycleStats(sortedData[0]);

  sortedData[0].iterations.forEach(x => {
    x.states[0].isReportStart = true;
    x.states[x.states.length - 1].isReportEnd = true;
  });

  if (reportView == 'new') {
    sortedData = mergeStatesOfTruck(JSON.parse(JSON.stringify(sortedData)));
  }
  render(sortedData, parameters);
}
