/* eslint-disable no-undef,no-unused-vars */

let allLPNAssets = null;

async function reloadVisitLog(parameters) {
  if (parameters.time) {
    d3.select('div.title').text(`Visit Log for ${moment.tz(parameters.time, timezone).format('MMMM Do, YYYY HH:mm z')}`);
  }

  await fetchCfg(parameters);
  configObj = await fetchYardCfg(parameters);
  const paramsString = Object.keys(parameters).map((key) => encodeURIComponent(key) + '=' + encodeURIComponent(parameters[key])).join('&');
  let data = await d3.json('/portal/data/cycle_known_identifiers/' + parameters.customer + '?' + paramsString, {credentials: 'include'});
  globalData = JSON.parse(JSON.stringify(data));
  return data;
}


async function getAllLPNAssets(parameters) {
  let allLPNAssets = await d3.json('/portal/customer-asset/getAllLPNAssets?customer=' + parameters.customer, {credentials: 'include'});
  let assetMap = {};
  let carrierSet = new Set();
  let unitNumberSet = new Set();
  allLPNAssets.forEach(asset => {
    carrierSet.add(asset.carrier_name);
    unitNumberSet.add(asset.unit_number);
    if (!assetMap[asset['carrier_name']]) {
      assetMap[asset['carrier_name']] = {};
    }
    assetMap[asset['carrier_name']][asset['unit_number']] = asset['license_plate'];
  });

  return {
    'carrierList': [...carrierSet],
    'unitNumberList' : [...unitNumberSet],
    'assetMap' : assetMap
  };
}

const substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });

    cb(matches);
  };
};

async function renderVisitLog(data, parameters) {
  now = parameters.time ? moment(parameters.time) : moment();
  const hoursToLookBack = getHoursToLookBackByCgr(configObj, parameters);
  const elements = data && data.length ? data[0]: { subjects: [] };

  let extractData = elements.subjects;
  if (filterOnTags.length) {
    extractData = extractData.filter(function (d) {
      return d.customerTags.some(function (tag) {
        return filterOnTags.includes(tag);
      });
    });
  }

  const theSelectedSortOrder = !configObj[parameters.customer][parameters.facility]['sortOrder'];
  //sort the array based on the last entered
  extractData.sort(function (left, right) {
    const dateA = new Date(left.states[0].lastSeen);
    const dateB = new Date(right.states[0].lastSeen);
    return (theSelectedSortOrder ? 1: -1) * Math.sign(dateB - dateA); //sort by lastSeen descending
  });

  extractData.forEach(x => {
    x.states.forEach(y => {
      if (!y.isEnd) {
        x.visited = x.visited + 1;
      }
    });
  });

  let records = createGroupedArray(extractData, 4);

  d3.select('#truckCount').html('');
  d3.select('div.facility').text(`Facility: ${parameters.facility}`);
  d3.select('#truckCount').text(`Active Truck Count: ${extractData.length}`);
  d3.select('#timeTravel').text('Time Travel enabled');
  if (!records.length) {
    showNoDataIndicator();
  } else {
    hideNoDataIndicator();
  }
  d3.selectAll('.cycle-cards').remove();
  var cycleDivsRow = d3.select('#container').selectAll('div.cycle-cards').data(records);
  let newGridRows = cycleDivsRow.enter().append('div').classed('cycle-cards', true).classed('mdl-grid', true);
  let mergedGridDivs = newGridRows.merge(cycleDivsRow);
  cycleDivsRow.exit().remove();
  let gridCol = mergedGridDivs.selectAll('div.card-col').data(function (d) {
    return d;
  });
  let newGridColDivs = gridCol.enter().append('div')
    .classed('card-col', true)
    .classed('mdl-cell', true)
    .classed('mdl-cell--3-col', true)
    .attr('id', function (_) {
      let id= newImageComponent(parameters.customer, _.states[0].img_keys['lpr'][0]).toImageURL().split('/')[6];
      return `card-${id}`;
    });

  let cardBodyContent = newGridColDivs.append('div').classed('demo-card-square', true).classed('mdl-card', true)
    .classed('mdl-shadow--2dp', true).classed('center-align', true).classed('mdl-card__supporting-text', true);
  cardBodyContent.append('button')
    .classed('close', true)
    .classed('float-right', true)
    .style('margin-left', '90%')
    .attr('type', 'button')
    .html('×')
    .on('click', function (d) {
      let imgKeys = d.states[0].img_keys['lpr'][0];
      if (isNaN(imgKeys.collection_time)) {
        let keySplit = imgKeys.collection_time.split('.');
        let collection_time = `${moment(imgKeys.collection_time).unix()}`;
        if (keySplit.length > 1) {
          if(keySplit[1].split('+').length > 1){
            collection_time += '.' + keySplit[1].split('+')[0];
          }
        }
        imgKeys.collection_time = parseFloat(collection_time);
      }
      blacklistContext(imgKeys, parameters);
      d3.select(`#card-${ imgKeys.collection_time}`).remove();
      filteredData.push({
        collection_time: imgKeys.collection_time,
        gateway_id: imgKeys.gateway_id
      });
    });
  cardBodyContent.append('div').classed('truck', true);
  cardBodyContent.append('div').classed('start-time', true);
  cardBodyContent.append('div').classed('duration', true);
  cardBodyContent.append('img')
    .classed('display-img', true)
    .attr('id', function (_) {
      let id= newImageComponent(parameters.customer, _.states[0].img_keys['lpr'][0]).toImageURL().split('/')[6];
      return `display-img-${id}`;
    })
    .property('controls', true)
    .attr('src', function (d) {
      if (d.lprLink) {
        return d.lprLink;
      }
      return '/portal/images/video-play.png';
    })
    .on('mouseout', function (d) {
      d3.select(this).style('cursor', 'default');
    })
    .on('mouseover', function (d) {
      d3.select(this).style('cursor', 'pointer');
    })
    .on('click', function (d) {
      constructAndRenderImgModal(d);
    });
  cardBodyContent.append('div').classed('tags', true);
  let mergedCols = newGridColDivs.merge(gridCol);
  mergedCols.select('div.truck').text(function (d) {
    return 'Truck: ' + d.subjectId + ' (' + d.visited + ')';
  }).on('click', function (d) {
    constructAndRenderImgModal(d);
  });
  mergedCols.select('button.tag-btn').attr('id', function (d, i) {
    return 'demo-menu-lower-right-' + i;
  });
  mergedCols.select('div.start-time').text(function (d) {
    return 'Start: ' + moment.tz(d.states[0].lastSeen, timezone).format('YYYY-MM-DD HH:mm:ss z');
  });
  mergedCols.select('div.duration').text(function (d) {
    if (isNaN(d.states[0].lastSeen)) {
      return 'Time since event: ' + calculateVisitLogDuration(moment.tz(d.states[0].lastSeen, timezone), moment.tz(parameters.time, timezone));
    } else {
      return 'Time since event: ' + calculateVisitLogDuration(moment.unix(d.states[0].lastSeen, timezone), moment.tz(parameters.time, timezone));
    }
  });

  mergedCols.select('div.mdl-card').classed('custom-color', true).style('--customColor', (d) => {
    let theCurrentDatetime = moment.tz(parameters.time, timezone);
    let diff = theCurrentDatetime.diff(d.states[0].lastSeen, 'seconds');
    let temp = cardColoring(diff);
    return temp;
  });
  gridCol.exit().remove();
  mergedGridDivs.merge(mergedCols);
}

async function visitLog(parameters) {

  allLPNAssets = await getAllLPNAssets(parameters);

  $('#carrier-row .typeahead').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  },
  {
    name: 'states',
    source: substringMatcher(allLPNAssets.carrierList)
  });
  $('#trailer-row .typeahead').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  },
  {
    name: 'states',
    source: substringMatcher(allLPNAssets.unitNumberList)
  });
  periodicLoadVisitLog(parameters);
}

async function periodicLoadVisitLog(parameters) {
  setTimeout(() => periodicLoadVisitLog(parameters), 60 * 1000);
  await reloadCustomerTags(parameters);
  let data = await reloadVisitLog(parameters);
  if (data.length > 0) {
    data[0].subjects = data[0].subjects.filter(d => {
      let state = d.states[0].img_keys['lpr'][0];
      return !filteredData.some(ft => {
        let keySplit = state.collection_time.split('.');
        let collection_time = `${moment(state.collection_time).unix()}`;
        if (keySplit.length > 1) {
          if(keySplit[1].split('+').length > 1){
            collection_time += '.' + keySplit[1].split('+')[0];
          }
        }
        return collection_time === `${ft.collection_time}` &&
        state.gateway_id === ft.gateway_id;
      });
    });
  }
  if (!customerTags.length) {
    customerTags = ['entry', 'exit'];
  }
  renderCustomerTagsDropDown();
  $('.loading').remove();
  if (data.length) {
    now = (parameters.time ? parameters.time : moment());
    d3.select('div.title').text(`Visit Log for ${moment.tz(now, timezone).format('MMMM Do, YYYY HH:mm z')}`);
    renderVisitLog(data, parameters);
  } else {
    showNoDataIndicator();
  }

}

async function handleIntervalFiredVisitLog(parameters) {
  now = (parameters.time ? moment.tz(moment(parameters.time), timezone) : moment.tz(moment(), timezone));
  d3.select('div.title').text(`Visit Log for ${moment.tz(now, timezone).format('MMMM Do, YYYY HH:mm z')}`);
  let latestConfig = await getLatestConfig();
  if (latestConfig) {
    if (latestConfig.config.landing_page != landingPage) {
      location.reload();
    } else {
      await reloadCustomerTags(parameters);
    }
    parameters.facility = latestConfig.config.facility;
    parameters.customer = latestConfig.cgr;
    urlParams = Object.assign(urlParams, parameters);
  }
  let data = await reloadVisitLog(parameters);
  if (data.length) {
    renderVisitLog(data, parameters);
  } else {
    showNoDataIndicator();
  }
}

function searchVisitLog(searchText) {
  let localSearchData = JSON.parse(JSON.stringify(globalData));
  localSearchData[0].subjects = globalData[0].subjects.filter(function (d) {
    return d.subjectId.toLowerCase().includes(searchText.toLowerCase());
  });
  renderVisitLog(localSearchData, parameters);
}

async function setDateTime(val) {
  parameters.time = val;
  if (parameters.time) {
    delete parameters.date;
  }
  let paramString = Object.keys(parameters).map(p => p + '=' + parameters[p]).join('&');
  var newurl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?' + paramString;
  window.history.pushState({path: newurl}, '', newurl);
  let data = await reloadVisitLog(parameters);
  renderVisitLog(data, parameters);
}

/* eslint-enable */