'use strict';

(function (global, newColorModule) {
  const Atollogy = global.Atollogy || (global.Atollogy = {});

  Object.assign(Atollogy, newColorModule());
}(this, () => {
  const FLASHING_STATES = [ 'animated' ];
  const ZERO_STATES = [ 'off', 'zero', 'lights off', 'no issue', '0' ];

  const hasFlashingState = ({ state }) => {
    return 'string' === typeof state && FLASHING_STATES.some(theExpectedState => state.toLowerCase().indexOf(theExpectedState) >= 0);
  };

  const StateMappings = Object.freeze({ '{}': 'success' });

  const newColorProvider = (reportConfig) => {
    const theStateObject = { updated: Date.now() };

    const CANDIDATE_COLORS = [
      'FF0000', 'FFD400', 'FF7F00', 'FFFF00',
      'BFFF00', '6AFF00', '00EAFF', '0095FF',
      '0040FF', 'AA00FF', 'FF00AA', 'EDB9B9',
      'E7E9B9', 'B9EDE0', 'B9D7ED', 'DCB9ED',
      '8F2323', '8F6A23', '4F8F23', '23628F',
      '6B238F'
    ];
    const DEFINED_COLORS = Object.freeze({
      'NA': Object.freeze({
        match: (state) => 'string' === typeof state && state.toLowerCase() === 'na',
        color: 'rgba(0, 0, 0, 1)'
      }),
      'Missing Data': Object.freeze({
        match: (state) => 'string' === typeof state && state.toLowerCase() === 'missing data',
        color: 'rgba(116, 116, 116, 1)'
      }),
      'Missing Data-Some': Object.freeze({
        match: (state) => 'string' === typeof state && state.toLowerCase() === 'missing data-some',
        color: 'rgba(166, 161, 161, 1)'
      }),
      'Zero Or Off': Object.freeze({
        match: (state) => 'string' === typeof state && ZERO_STATES.includes(state.toLowerCase()),
        color: 'rgba(202, 199, 199, 1)'
      })
    });

    return ({ subject, measure, state: theStateValue }) => {
      const theCurrentTime = Date.now();
      const theColorStateObject = { /* */ };
      const theStatePerSubject = theStateObject[subject] || (theStateObject[subject] = {});
      const theStatePerMeasure = theStatePerSubject[measure] || (theStatePerSubject[measure] = { counter: 0, previous: null });
      const index = theStatePerMeasure.counter % CANDIDATE_COLORS.length;

      const theMappedState = Object.prototype.hasOwnProperty.call(StateMappings, theStateValue) ? StateMappings[theStateValue] : theStateValue;

      try {
        const theReportConfig = Object.assign({}, reportConfig);
        const theSubjectConfig = theReportConfig[subject] || (theReportConfig[subject] = {});
        const theMeasureConfig = theSubjectConfig[measure] || (theSubjectConfig[measure] = {});

        if (Object.prototype.hasOwnProperty.call(theMeasureConfig, 'uniform_state_legend_color')) {
          const theCandidateColor = theMeasureConfig['uniform_state_legend_color'];
          Object.assign(theColorStateObject, { [theMappedState]: theCandidateColor });
        } else {
          const theStateColor = theMeasureConfig['stateColors'] || (theMeasureConfig['stateColors'] = {});

          const hasDefinedColor = Object.keys(DEFINED_COLORS).map((theStateName) => {
            const matcher = DEFINED_COLORS[theStateName];
            return matcher.match(theMappedState) ? matcher.color : undefined;
          }).filter(Boolean);

          if (hasDefinedColor.length === 0 && Object.prototype.hasOwnProperty.call(theStateColor, theMappedState)) {
            const theCandidateColor = theStateColor[theMappedState];
            Object.assign(theColorStateObject, { [theMappedState]: theCandidateColor });
          } else {
            const theIndexedColor = hasDefinedColor.length ? hasDefinedColor[0] :  `#${CANDIDATE_COLORS[index]}`;
            const thePreviousValue = theStatePerMeasure.previous || theMappedState;
            theStatePerMeasure.counter += thePreviousValue === theMappedState ? 0 : 1;
            theStatePerMeasure.previous = theMappedState;

            Object.assign(theStateColor, { [theMappedState]: theIndexedColor });
            Object.assign(theColorStateObject, { [theMappedState]: theIndexedColor });
          }
        }
      } finally {
        theStateObject.updated = theCurrentTime;
      }

      return theColorStateObject[theMappedState];
    };
  };

  return Object.freeze({ newColorProvider, hasFlashingState });
}));