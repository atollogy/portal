'use strict';

(function makeModalHelper(global, registrar) {
  const { document } = global;
  const findElementBySelector = (cssSelector) => document.querySelector(cssSelector);

  registrar.call(global, `@atollogy/modal-header`, [], function () {
    const makeModalHeaderMarkup = ({ uuid, title, info, icon }) => {
      let header = `<!-- Model Header -->
        <div id="${uuid}" class="modalRow">
        <span class="modal-title">${title}</span>
        <span class="videoCamIcon">
          <i class="material-icons">${icon}</i>
      </span>`;
      if (info !== undefined) {
        header +=
          `<br/><span class="modal-info">${info}</span>`;
      }
      header += '</div>';
      return header;
    };

    const Model = Object.freeze({
      defaults: Object.freeze({ title: 'Atollogy', info: '', icon: 'filter_none' })
    });

    const ModalHeader = function ({ selector, model }) {
      const uuid = 'xxxxyxxxx-xxxx-xxxx-yxxx-xxxxxxxx'.replace(/[xy]/g, (character) => {
        const random = Math.random() * 16 | 0;
        const part = character === 'x' ? random : ((random & 0x03) | 0x08);
        return part.toString(16).toLowerCase();
      });

      Object.assign(this, {
        uuid, selector,
        element: findElementBySelector(selector),
        model: Object.assign({}, Model.defaults, model)
      });
    };

    ModalHeader.prototype.updateMedia = function (eventInfo) {
      const { model } = this;
      const { element } = eventInfo;
      const title = eventInfo.title || model.title;
      const info = eventInfo.info || model.info;
      const icon = element.querySelector('.display-image') ? 'photo_camera' : (
        element.querySelector('.display-video') ? 'videocam' : ''
      );

      ModalHeader.prototype.update.call(this, { title, info, icon });
    };

    ModalHeader.prototype.update = function (model) {
      model = model || this.model || Model.defaults;

      const { selector } = this;
      const { title, info, icon } = model;

      const element = findElementBySelector(selector);

      try {
        element.classList.add('hidden');

        const titleElement = element.querySelector(`[class="modal-title"]`);
        titleElement.innerHTML = title;

        if (info !== undefined) {
          const infoElement = element.querySelector(`[class="modal-info"]`);
          infoElement.innerHTML = info;
        }

        const iconElement = element.querySelector(`[class="material-icons"]`);
        iconElement.innerHTML = icon;
      } finally {
        element.classList.remove('hidden');
      }
    };

    ModalHeader.prototype.render = function (model) {
      model = model || this.model || Model.defaults;

      const { uuid, selector } = this;
      const element = findElementBySelector(selector);

      const { title, info, icon } = model;
      element.innerHTML = makeModalHeaderMarkup({ uuid, title, info, icon });
    };

    return ModalHeader;
  });

  registrar.call(global, `ModalHeader`, ['@atollogy/modal-header'], (ModalHeader) => ModalHeader);

}(this, function makeRegistrarHelper(name, dependencies, factory) {
  const global = this;
  const { module } = this;

  if ('function' === typeof require && 'object' === typeof module && hasOwnProperty.call(module, 'exports')) {
    dependencies = dependencies.map((dependency) => require(dependency));
    module.exports = factory.apply(global, dependencies);
    return;
  }

  dependencies = Array.isArray(dependencies) ? dependencies : [dependencies];
  dependencies = dependencies.map((dependency) => global[dependency]);

  const previous = global[name] || undefined;
  const newModuleInstance = factory.apply(global, dependencies);

  newModuleInstance.noConflict = () => {
    global[name] = previous;
    return newModuleInstance;
  };

  global[name] = newModuleInstance;
}));