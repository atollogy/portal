const testData = [
  [
    //input for Test 0
    [
      {
        "startTime": "2019-01-10T23:16:00.000Z",
        "endTime": "2019-01-10T23:22:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:16:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:17:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:18:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:19:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:20:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:21:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "running",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:22:00.000Z",
        "endTime": "2019-01-10T23:23:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:22:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "ignored",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:23:00.000Z",
        "endTime": "2019-01-10T23:25:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:23:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:24:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "running",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:25:00.000Z",
        "endTime": "2019-01-10T23:40:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:25:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:26:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:27:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:28:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:29:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:30:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:31:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:32:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:33:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:34:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:35:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:36:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:37:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:38:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:39:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "ignored",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:40:00.000Z",
        "endTime": "2019-01-10T23:41:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:40:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "setup/load",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:41:00.000Z",
        "endTime": "2019-01-11T00:19:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:41:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:42:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:43:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:44:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:45:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:46:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:47:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:48:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:49:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:50:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:51:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:52:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:53:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:54:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:55:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:56:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:57:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:58:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-10T23:59:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:00:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:01:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:02:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:03:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:04:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:05:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:06:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:07:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:08:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:09:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:10:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:11:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:12:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:13:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:14:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:15:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:16:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:17:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:18:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "ignored",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:19:00.000Z",
        "endTime": "2019-01-11T00:20:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:19:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        'state': 'Missing Data-Some',
        "missingMeasures": [
          "bulb"
        ]
      },
      {
        "startTime": "2019-01-11T00:20:00.000Z",
        "endTime": "2019-01-11T00:21:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:20:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "ignored",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:21:00.000Z",
        "endTime": "2019-01-11T00:22:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:21:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "idle",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:22:00.000Z",
        "endTime": "2019-01-11T00:31:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:22:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:23:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:24:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:25:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:26:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:27:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:28:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:29:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_5",
            "collection_time": "2019-01-11T00:30:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "ignored",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:31:00.000Z",
        "endTime": "2019-01-11T00:45:00.000Z",
        "images": [],
        'state': 'Missing Data-Some',
        "missingMeasures": [
          "operator"
        ]
      }
    ],
    //expected output for Test 0
    {
      "startTime": "2019-01-11T00:22:00.000Z",
      "endTime": "2019-01-11T00:31:00.000Z",
      "images": [
        {
          "gateway_id": "000094b855f8",
          "camera_id": "video0",
          "step_name": "operator_at_mill_5",
          "collection_time": "2019-01-11T00:22:00+00:00",
          "name": "anonymous.jpg"
        },
        {
          "gateway_id": "000094b855f8",
          "camera_id": "video0",
          "step_name": "operator_at_mill_5",
          "collection_time": "2019-01-11T00:23:00+00:00",
          "name": "anonymous.jpg"
        },
        {
          "gateway_id": "000094b855f8",
          "camera_id": "video0",
          "step_name": "operator_at_mill_5",
          "collection_time": "2019-01-11T00:24:00+00:00",
          "name": "anonymous.jpg"
        },
        {
          "gateway_id": "000094b855f8",
          "camera_id": "video0",
          "step_name": "operator_at_mill_5",
          "collection_time": "2019-01-11T00:25:00+00:00",
          "name": "anonymous.jpg"
        },
        {
          "gateway_id": "000094b855f8",
          "camera_id": "video0",
          "step_name": "operator_at_mill_5",
          "collection_time": "2019-01-11T00:26:00+00:00",
          "name": "anonymous.jpg"
        },
        {
          "gateway_id": "000094b855f8",
          "camera_id": "video0",
          "step_name": "operator_at_mill_5",
          "collection_time": "2019-01-11T00:27:00+00:00",
          "name": "anonymous.jpg"
        },
        {
          "gateway_id": "000094b855f8",
          "camera_id": "video0",
          "step_name": "operator_at_mill_5",
          "collection_time": "2019-01-11T00:28:00+00:00",
          "name": "anonymous.jpg"
        },
        {
          "gateway_id": "000094b855f8",
          "camera_id": "video0",
          "step_name": "operator_at_mill_5",
          "collection_time": "2019-01-11T00:29:00+00:00",
          "name": "anonymous.jpg"
        },
        {
          "gateway_id": "000094b855f8",
          "camera_id": "video0",
          "step_name": "operator_at_mill_5",
          "collection_time": "2019-01-11T00:30:00+00:00",
          "name": "anonymous.jpg"
        }
      ],
      "state": "ignored",
      "missingMeasures": []
    }
  ],
  [
    //input for Test 1
    [
      {
        "startTime": "2019-01-10T23:16:00.000Z",
        "endTime": "2019-01-10T23:17:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:16:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "idle",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:17:00.000Z",
        "endTime": "2019-01-10T23:18:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:17:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "setup/load",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:18:00.000Z",
        "endTime": "2019-01-10T23:19:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:18:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "running",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:19:00.000Z",
        "endTime": "2019-01-10T23:21:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:19:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:20:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "idle",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:21:00.000Z",
        "endTime": "2019-01-10T23:22:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:21:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "setup/load",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:22:00.000Z",
        "endTime": "2019-01-10T23:23:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:22:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "idle",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:23:00.000Z",
        "endTime": "2019-01-10T23:24:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:23:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "setup/load",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:24:00.000Z",
        "endTime": "2019-01-10T23:54:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:24:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:25:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:26:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:27:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:28:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:29:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:30:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:31:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:32:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:33:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:34:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:35:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:36:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:37:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:38:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:39:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:40:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:41:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:42:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:43:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:44:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:45:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:46:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:47:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:48:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:49:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:50:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:51:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:52:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:53:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "idle",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:54:00.000Z",
        "endTime": "2019-01-10T23:55:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:54:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "setup/load",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:55:00.000Z",
        "endTime": "2019-01-10T23:57:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:55:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:56:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "idle",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-10T23:57:00.000Z",
        "endTime": "2019-01-11T00:01:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:57:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:58:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-10T23:59:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:00:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "setup/load",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:01:00.000Z",
        "endTime": "2019-01-11T00:04:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:01:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:02:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:03:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "running",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:04:00.000Z",
        "endTime": "2019-01-11T00:05:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:04:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "setup/load",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:05:00.000Z",
        "endTime": "2019-01-11T00:06:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:05:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "idle",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:06:00.000Z",
        "endTime": "2019-01-11T00:07:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:06:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "running",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:07:00.000Z",
        "endTime": "2019-01-11T00:10:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:07:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:08:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:09:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "setup/load",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:10:00.000Z",
        "endTime": "2019-01-11T00:13:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:10:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:11:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:12:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "running",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:13:00.000Z",
        "endTime": "2019-01-11T00:23:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:13:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:14:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:15:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:16:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:17:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:18:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:19:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:20:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:21:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:22:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "setup/load",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:23:00.000Z",
        "endTime": "2019-01-11T00:28:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:23:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:24:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:25:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:26:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:27:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "running",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:28:00.000Z",
        "endTime": "2019-01-11T00:30:00.000Z",
        "images": [
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:28:00+00:00",
            "name": "anonymous.jpg"
          },
          {
            "gateway_id": "000094b855f8",
            "camera_id": "video0",
            "step_name": "operator_at_mill_12",
            "collection_time": "2019-01-11T00:29:00+00:00",
            "name": "anonymous.jpg"
          }
        ],
        "state": "setup/load",
        "missingMeasures": []
      },
      {
        "startTime": "2019-01-11T00:30:00.000Z",
        "endTime": "2019-01-11T00:31:00.000Z",
        "images": [],
        'state': 'Missing Data-Some',
        "missingMeasures": [
          "bulb",
          "operator"
        ]
      },
      {
        "startTime": "2019-01-11T00:31:00.000Z",
        "endTime": "2019-01-11T00:45:00.000Z",
        "images": [],
        'state': 'Missing Data-Some',
        "missingMeasures": [
          "bulb",
          "operator"
        ]
      }
    ],
    //expected output for Test 1
    {
      "startTime": "2019-01-11T00:28:00.000Z",
      "endTime": "2019-01-11T00:30:00.000Z",
      "images": [
        {
          "gateway_id": "000094b855f8",
          "camera_id": "video0",
          "step_name": "operator_at_mill_12",
          "collection_time": "2019-01-11T00:28:00+00:00",
          "name": "anonymous.jpg"
        },
        {
          "gateway_id": "000094b855f8",
          "camera_id": "video0",
          "step_name": "operator_at_mill_12",
          "collection_time": "2019-01-11T00:29:00+00:00",
          "name": "anonymous.jpg"
        }
      ],
      "state": "setup/load",
      "missingMeasures": []
    }
  ]
];

module.exports = testData;
