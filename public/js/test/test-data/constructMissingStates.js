const moment = require('moment');

const testData = [
  // Test Zero
  [
    "Provide 3 exits and watch entries getting inferred",
    [
      {
        subject: 'GA5892',
        state: 'Exit A',
        startTime: '2019-01-09T05:30:00.000Z',
        endTime: '2019-01-09T05:30:00.000Z',
        warning: null,
        itemId: '1383',
        customerTags: [],
        is_start: false,
        is_end: true
      },
      {
        subject: 'GA5892',
        state: 'Exit A',
        startTime: '2019-01-09T08:30:00.000Z',
        endTime: '2019-01-09T08:30:00.000Z',
        warning: null,
        itemId: '1383',
        customerTags: [],
        is_start: false,
        is_end: true
      },
      {
        subject: 'GA5892',
        state: 'Exit B',
        startTime: '2019-01-09T14:30:00.000Z',
        endTime: '2019-01-09T14:30:00.000Z',
        warning: null,
        itemId: '1383',
        customerTags: [],
        is_start: false,
        is_end: true
      }
    ],
    [{
      'Entry A': {
          imgConfig: {
            "" : ""
          },
          inferredState: {
            type: 'Entry',
            entry2Exit: {
              states: {
                left: 'Entry A',
                right: 'Exit A'
              }
            },
            distance: 60
          }
        },
      'Entry B': {
        imgConfig: {
          "" : ""
        },
        inferredState: {
          type: 'Entry',
          entry2Exit: {
            states: {
              left: 'Entry B',
              right: 'Exit B'
            }
          },
          distance: 60
        }
      },
      'Exit A': {
        imgConfig: {
          "" : ""
        },
        inferredState: {
          type: 'Exit',
          entry2Exit: {
            states: {
              left: 'Entry A',
              right: 'Exit A'
            },
            distance: 60
          }
        }
      },
      'Exit B': {
        imgConfig: {
          "" : ""
        },
        inferredState: {
          type: 'Exit',
          entry2Exit: {
            states: {
              left: 'Entry B',
              right: 'Exit B'
            },
            distance: 60
          }
        }
      }
    }
    ],
    [
      {
        subject: 'GA5892',
        state: 'Entry A',
        startTime: '2019-01-09T04:30:00.000Z',
        endTime: '2019-01-09T04:30:00.000Z',
        warning: null,
        itemId: '1383',
        customerTags: [],
        is_start: true,
        is_end: false,
        inferred: true
      },
      {
        subject: 'GA5892',
        state: 'Exit A',
        startTime: '2019-01-09T05:30:00.000Z',
        endTime: '2019-01-09T05:30:00.000Z',
        warning: null,
        itemId: '1383',
        customerTags: [],
        is_start: false,
        is_end: true
      },
      {
        subject: 'GA5892',
        state: 'Exit A',
        startTime: '2019-01-09T08:30:00.000Z',
        endTime: '2019-01-09T08:30:00.000Z',
        warning: null,
        itemId: '1383',
        customerTags: [],
        is_start: false,
        is_end: true
      },
      {
        subject: 'GA5892',
        state: 'Exit B',
        startTime: '2019-01-09T14:30:00.000Z',
        endTime: '2019-01-09T14:30:00.000Z',
        warning: null,
        itemId: '1383',
        customerTags: [],
        is_start: false,
        is_end: true
      }
    ]
  ],

  // Test One
  [
    "Provide just 1 scale house Entry And watch other states getting inferred",
    [
      {
        subject: 'GA5892',
        state: 'Scale House Entry 1',
        startTime: '2019-01-09T10:00:00.000Z',
        endTime: '2019-01-09T10:00:00.000Z',
        warning: null,
        itemId: '1383',
        customerTags: [],
        is_start: false,
        is_end: false
      }
    ],
    [
      {
        'Scale House Entry 1': {
          imgConfig: {
            "": ""
          },
          inferredState: {
            type: 'Scale_entry',
            entry2ScaleEntry: {
              states: {
                left: 'Entry A',
                right: 'Scale House Entry 1'
              },
              distance: 15
            },
            scaleEntry2ScaleExit: {
              states: {
                left: 'Scale House Entry 1',
                right: 'Scale House Exit 1'
              },
              distance: 10
            }
          }
        },
        'Scale House Exit 1': {
          imgConfig: {
            "": ""
          },
          inferredState : {
            type: "Scale_exit",
            scaleEntry2ScaleExit: {
              states: {
                left: "Scale House Entry 1",
                right: "Scale House Exit 1"
              }, distance: 10
            },
            scaleExit2Exit: {
              states: {
                left: "Scale House Exit 1",
                right: "Exit A"
              },
              distance: 10
            }
          }
        }
      }
    ],
    [
      {
        "subject": "GA5892",
        "state": "Entry A",
        "startTime": "2019-01-09T09:45:00.000Z",
        "endTime": "2019-01-09T09:45:00.000Z",
        "warning": null,
        "itemId": "1383",
        "customerTags": [],
        "is_start": true,
        "is_end": false,
        "inferred": true
      },
      {
        "subject": "GA5892",
        "state": "Scale House Entry 1",
        "startTime": "2019-01-09T10:00:00.000Z",
        "endTime": "2019-01-09T10:00:00.000Z",
        "warning": null,
        "itemId": "1383",
        "customerTags": [],
        "is_start": false,
        "is_end": false
      },
      {
        "subject": "GA5892",
        "state": "Scale House Exit 1",
        "startTime": "2019-01-09T10:10:00.000Z",
        "endTime": "2019-01-09T10:10:00.000Z",
        "warning": null,
        "itemId": "1383",
        "customerTags": [],
        "is_start": false,
        "is_end": false,
        "inferred": true
      },
      {
        "subject": "GA5892",
        "state": "Exit A",
        "startTime": "2019-01-09T10:20:00.000Z",
        "endTime": "2019-01-09T10:20:00.000Z",
        "warning": null,
        "itemId": "1383",
        "customerTags": [],
        "is_start": false,
        "is_end": true,
        "inferred": true
      }
    ]
  ],

  // Test Two
  [
    "Provide 1 Entry And 1scale house Entry And watch exit states getting inferred",
    [
      {
        subject: 'GA5892',
        state: 'Entry A',
        startTime: '2019-01-09T10:00:00.000Z',
        endTime: '2019-01-09T10:00:00.000Z',
        warning: null,
        itemId: '1383',
        customerTags: [],
        is_start: true,
        is_end: false
      },
      {
        subject: 'GA5892',
        state: 'Scale House Exit 1',
        startTime: '2019-01-09T11:00:00.000Z',
        endTime: '2019-01-09T11:00:00.000Z',
        warning: null,
        itemId: '1383',
        customerTags: [],
        is_start: false,
        is_end: false
      }
    ],
    [
      {
        'Entry A': {
          imgConfig: {
            "": ""
          },
          inferredState: {
            type: 'Entry',
            entry2Exit: {
              states: {
                left: 'Entry A',
                right: 'Exit A'
              },
              distance: 60
            },
            entry2ScaleEntry: {
              states: {
                left: 'Entry A',
                right: 'Scale House Entry 1'
              },
              distance: 15
            },
          }
        },
        'Scale House Exit 1': {
          imgConfig: {
            "": ""
          },
          inferredState: {
            type: "Scale_exit",
            scaleEntry2ScaleExit: {
              states: {
                left: "Scale House Entry 1",
                right: "Scale House Exit 1"
              }, distance: 10
            },
            scaleExit2Exit: {
              states: {
                left: "Scale House Exit 1",
                right: "Exit A"
              },
              distance: 10
            }
          }
        }
      }
    ],
    [
      {
        "subject": "GA5892",
        "state": "Entry A",
        "startTime": "2019-01-09T10:00:00.000Z",
        "endTime": "2019-01-09T10:00:00.000Z",
        "warning": null,
        "itemId": "1383",
        "customerTags": [],
        "is_start": true,
        "is_end": false
      },
      {
        "subject": "GA5892",
        "state": "Scale House Entry 1",
        "startTime": "2019-01-09T10:50:00.000Z",
        "endTime": "2019-01-09T10:50:00.000Z",
        "warning": null,
        "itemId": "1383",
        "customerTags": [],
        "is_start": false,
        "is_end": false,
        "inferred": true
      },
      {
        "subject": "GA5892",
        "state": "Scale House Exit 1",
        "startTime": "2019-01-09T11:00:00.000Z",
        "endTime": "2019-01-09T11:00:00.000Z",
        "warning": null,
        "itemId": "1383",
        "customerTags": [],
        "is_start": false,
        "is_end": false
      },
      {
        "subject": "GA5892",
        "state": "Exit A",
        "startTime": "2019-01-09T11:10:00.000Z",
        "endTime": "2019-01-09T11:10:00.000Z",
        "warning": null,
        "itemId": "1383",
        "customerTags": [],
        "is_start": false,
        "is_end": true,
        "inferred": true
      }
    ]
  ]
];

module.exports = testData;
