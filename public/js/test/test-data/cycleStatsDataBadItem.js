module.exports = {
  'subject': '172317F',
  'cycle': '0',
  'customerTags': [],
  'states': [
    {
      'subject': '172317F',
      'state': 'scale house entry 2',
      'startTime': '2019-01-09T13:21:51.000Z',
      'endTime': '2019-01-09T13:21:51.000Z',
      'img_keys': {
        'lpr': [
          {
            'name': 'annotated.jpg',
            'camera_id': 'video0',
            'step_name': 'scale_2_entry',
            'gateway_id': '38af294f1301',
            'collection_time': '2019-01-09T13:21:51+00:00'
          }
        ],
        'weight': []
      },
      'warning': null,
      'itemId': '6266',
      'customerTags': [],
      'is_start': false,
      'is_end': false
    },
    {
      'subject': '172317F',
      'state': 'scale house exit 1',
      'startTime': '2019-01-09T13:31:51.000Z',
      'endTime': '2019-01-09T13:31:51.000Z',
      'img_keys': {
        'lpr': [
          {
            'name': 'annotated.jpg',
            'camera_id': 'video0',
            'step_name': 'scale_2_entry',
            'gateway_id': '38af294f1301',
            'collection_time': '2019-01-09T13:21:51+00:00'
          }
        ],
        'weight': []
      },
      'warning': null,
      'itemId': '6266',
      'customerTags': [],
      'is_start': false,
      'is_end': false,
      'inferred': true
    },
    {
      'subject': '172317F',
      'state': 'entry a',
      'startTime': '2019-01-09T13:06:51.000Z',
      'endTime': '2019-01-09T13:06:51.000Z',
      'img_keys': {
        'lpr': [
          {
            'name': 'annotated.jpg',
            'camera_id': 'video0',
            'step_name': 'scale_2_entry',
            'gateway_id': '38af294f1301',
            'collection_time': '2019-01-09T13:21:51+00:00'
          }
        ],
        'weight': []
      },
      'warning': null,
      'itemId': '6266',
      'customerTags': [],
      'is_start': true,
      'is_end': false,
      'inferred': true
    },
    {
      'subject': '172317F',
      'state': 'exit a',
      'startTime': '2019-01-09T13:41:51.000Z',
      'endTime': '2019-01-09T13:41:51.000Z',
      'img_keys': {
        'lpr': [
          {
            'name': 'annotated.jpg',
            'camera_id': 'video0',
            'step_name': 'scale_2_entry',
            'gateway_id': '38af294f1301',
            'collection_time': '2019-01-09T13:21:51+00:00'
          }
        ],
        'weight': []
      },
      'warning': null,
      'itemId': '6266',
      'customerTags': [],
      'is_start': false,
      'is_end': true,
      'inferred': true
    }
  ],
  'attributes': {
    'licence_plate': '172317F'
  }
};
