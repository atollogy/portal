const crh = require('../cycle-report-helper');
const moment = require('moment');
const constructMissingStates = require('./test-data/constructMissingStates');

//Test for constructMissingStates function
describe('Provide mock data with missing pieces of a Titan cycle. Verify that the missing pieces are inferred', () => {
  test.each(constructMissingStates)(
    'Test Case: %s',
    (name, data, reportCfg, expected) => {
      let {states: result} = crh.constructMissingStates({states: data}, reportCfg[0]);
      expect(result).toEqual(expected);
    }
  );
});

test('sanity', () => {
  expect(crh.sanityTest()).toBe('sane');
});

test('sorting states by startTime', () => {
  const orig = [{startTime: '2000-01-01'}, {startTime: '1900-01-01'}];
  const expected = [{startTime: '1900-01-01'}, {startTime: '2000-01-01'}];
  const result = crh.sortByStartTime(orig);
  expect(result).toEqual(expected);
});

test('cycleStats calculate aggregate statistics', () => {
  const expected = {
    "avgDuration": 2100000,
    "cycleCount": 1,
    "distinctCycles": 1,
    "uniqueEntries": 1,
    "uniqueExits": 1,
    "uniqueIntermediates": 2
  };
  const input = require('./test-data/cycleStatsDataBadItem');
  const {stats: result} = crh.cycleStats({iterations: [input]});
  expect(result).toEqual(expected);
});

test('fixIsEntryIsExitFlags happy path', () => {
  const expected = [{state: 'Entry A', is_start: true, inferred: true, is_end: false}, {
    state: 'Exit A'
  }];
  const input = [{state: 'Entry A', inferred: true}, {state: 'Exit A'}];
  const result = crh.fixIsEntryIsExitFlags(input);
  expect(result).toEqual(expected);
});
