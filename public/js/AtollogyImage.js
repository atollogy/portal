'use strict';

(function (global, makeAtollogyImage){
  const { $ } = global;

  const findElementBySelector = (selector) => document.querySelector(`${selector}`);

  const createImageElement = () => new Image();

  const createCanvasElememt = (sourceObject) => {
    const canvasId = `${sourceObject.imgKey.gateway_id}#${sourceObject.imgKey.collection_time}`;
    const canvas = document.createElement('canvas');
    canvas.setAttribute('id', canvasId);
    return canvas;
  };

  global.AtollogyImage = makeAtollogyImage($, {
    findElementBySelector,
    createImageElement,
    createCanvasElememt
  });
}(this, function makeAtollogyImage($, {
  findElementBySelector,
  createImageElement,
  createCanvasElememt
}) {
  const SHOW_DISTANCE_ENABLED = false;

  /**
   * Creates a Image to be displayed and takes care of drawing bounding boxes and social distance lines
   * base on the provided flags {showBoundingBox} and ${showSocialDistance}.
   *
   * @param {Object} imageObject list of image objects with the information to construct the image link.
   * @param {String} selector the CSS selector for the container where the resulting image will be rendered.
   * @param {Boolean} showBoundingBox flag to indicate if we should render the bounding boxes.
   * @param {Boolean} showSocialDistance flag to indicate if we should render the social distances lines.
   */
  // TODO: selector should be '.modal-image-display
  const AtollogyImage = function(imageObject, selector, showBoundingBox, showSocialDistance) {
    const canvasElement = createCanvasElememt(imageObject);

    showBoundingBox = !!showBoundingBox;
    showSocialDistance = !!showSocialDistance;

    Object.assign(this, {
      imageObject,
      selector,
      showBoundingBox,
      showSocialDistance,
      canvas: canvasElement
    });
  };

  AtollogyImage.prototype.render = function () {
    const { imageObject, canvas, showBoundingBox, showSocialDistance, selector } = this;

    $(selector).append(canvas);

    let imageObj = createImageElement();
    imageObj.onload = function () {
      const { width: imageWidth, height: imageHeight, bounding_box: theBoundingBoxes } = imageObj;

      const element = findElementBySelector(selector);
      const modalWidth = Math.max(768, element.clientWidth);
      const modalHeight = Math.max(468, 3 * element.clientHeight);

      const widthFactor = modalWidth / imageWidth;
      const heighFactor = modalHeight / imageHeight;
      canvas.width = modalWidth;
      canvas.height = modalHeight;

      let context = canvas.getContext('2d');
      context.drawImage(imageObj, 0, 0, modalWidth, modalHeight);
      const fontSize = 10;
      const padding = fontSize * 0.25;
      const titleHeight = fontSize * 1.125;
      const lineWidth = 2;

      context.beginPath();
      context.font = `${fontSize}px Arial`;

      if (showBoundingBox) {
        /** TODO (eprha:tanmay) Pass the bounding box as part of the image key and use to render them. */
        const arrayOfBoundingBox = Array.isArray(theBoundingBoxes) ? theBoundingBoxes: [theBoundingBoxes];

        arrayOfBoundingBox.filter(Boolean).forEach( ({x, y, width, height, type, confidence }) => {
          const bboxText = `${type} ${confidence.toFixed(2) || ''}`;
          // Title
          context.fillStyle = '#90ee90';
          context.fillRect((x * widthFactor) - (lineWidth)/2 , (y * heighFactor) - titleHeight, width*widthFactor + lineWidth, titleHeight, 0.5);
          context.lineWidth = lineWidth;
          context.fillStyle = '#000000';
          context.fillText(bboxText, (x * widthFactor) + padding, (y * heighFactor) - padding);

          // Bounding Box
          if(['person', 'operator'].includes(type)) {
            context.rect(x * widthFactor, y * heighFactor, width * widthFactor, height * heighFactor);
            context.fillStyle = '#5f91e3';
            context.fillRect(x*widthFactor, y*heighFactor, width*widthFactor, height*heighFactor);
          } else {
            context.rect(x*widthFactor, y*heighFactor, width*widthFactor, height*heighFactor);
          }

          context.lineWidth = lineWidth;
          context.strokeStyle = '#90ee90';
          context.stroke();
        });
      }

      if (showSocialDistance) {

        const DistanceColorMapping = Object.freeze({
          '4': 'rgba(255, 0, 0, 0.8)',
          '3': 'rgba(248, 160, 0, 0.8)',
          '2': 'rgba(255, 217, 101, 0.8)',
          '1': 'rgba(0, 175, 80, 0)',
          '0': 'rgba(166, 166, 166, 0)'
        });

        const distances = this.data && this.data.imgKey && this.data.imgKey.distances ? this.data.imgKey.distances : undefined;
        const centroids = this.data && this.data.imgKey && this.data.imgKey.centroids ? this.data.imgKey.centroids : undefined;
        const trapezoid = this.data && this.data.imgKey && this.data.imgKey.trapezoid ? this.data.imgKey.trapezoid : undefined;

        /*eslint-disable no-inner-declarations */
        function writeText(point, text, fillStyle) {
          if (SHOW_DISTANCE_ENABLED) {
            context.fillStyle = fillStyle;
            context.fillText(text, point[0] * widthFactor, point[1] * heighFactor);
          }
        }
        /*eslint-disable no-inner-declarations */
        function drawLine(point1, point2, strokeStyle, linewidthFactor) {
          const [ startX, startY ] = point1;
          const [ targetX, targetY ] = point2;

          context.beginPath();
          context.moveTo(startX * widthFactor, startY * heighFactor);
          context.lineTo(targetX * widthFactor, targetY * heighFactor);

          /** TODO (eprha:tanmay) Check look and feel to make sure it matches the expectation */
          context.lineWidth = linewidthFactor * lineWidth;

          /** TODO (eprha:tanmay) Update the color base on severity (leverage DistanceColorMapping)*/
          context.strokeStyle = strokeStyle;

          context.stroke();
        }

        if (distances) {
          for (let [severity, distancePairs] of Object.entries(distances)) {
            if (DistanceColorMapping[severity]) {
              for (let idx = 0; idx < distancePairs.length; idx += 1) {
                drawLine(distancePairs[idx][0][0], distancePairs[idx][0][1], DistanceColorMapping[severity], 2 /*linewidthFactor */);
                let midPoint = [(distancePairs[idx][0][0][0] + distancePairs[idx][0][1][0]) / 2 + 15,
                  (distancePairs[idx][0][0][1] + distancePairs[idx][0][1][1]) / 2 + 15];
                writeText(midPoint,`${distancePairs[idx][1]} ft`, DistanceColorMapping[severity]);
              }
            }
          }
        } else if (centroids) {
          for (let [severity, centroidsPairs] of Object.entries(centroids)) {
            if (DistanceColorMapping[severity]) {
              for (let idx = 0; idx < centroidsPairs.length; idx += 1) {
                drawLine(centroidsPairs[idx][0], centroidsPairs[idx][1], DistanceColorMapping[severity], 2 /*linewidthFactor */);
              }
            }
          }
        }

        /* eslint-disable  no-inner-declarations */
        if (trapezoid) {
          if (typeof trapezoid.bottom_left === 'string') {
            trapezoid.bottom_left = trapezoid.bottom_left.substring(1,trapezoid.bottom_left.length-1);
            trapezoid.bottom_right = trapezoid.bottom_right.substring(1,trapezoid.bottom_right.length-1);
            trapezoid.top_left = trapezoid.top_left.substring(1,trapezoid.top_left.length-1);
            trapezoid.top_right = trapezoid.top_right.substring(1,trapezoid.top_right.length-1);

            for (let i in trapezoid) {
              let temp = trapezoid[i].split(',');
              trapezoid[i] = [parseInt(temp[0]), parseInt(temp[1])];
            }
          }
          let linewidthFactor = 4;
          let strokeStyle = 'rgba(255, 217, 101, 0.8)';

          drawLine(trapezoid.bottom_left, trapezoid.bottom_right, strokeStyle, linewidthFactor);
          drawLine(trapezoid.bottom_right, trapezoid.top_right, strokeStyle, linewidthFactor);
          drawLine(trapezoid.top_left, trapezoid.top_right, strokeStyle, linewidthFactor);
          drawLine(trapezoid.top_left, trapezoid.bottom_left, strokeStyle, linewidthFactor);
        }
      }
    };
    imageObj.data = imageObject;
    imageObj.src = imageObject.url;
  };

  return  Object.freeze(AtollogyImage);
}));
