'use strict';

(function (global, makeMessages) {
  global.Messages = makeMessages();
}(this, () => {
  const PARAMETER_EXPRESSION = /(:[a-zA-Z]{3..}:)/;
  const APPLICABLE_PROPERTIES = ['customer', 'facility'];

  const settings = {
    customer: '',
    facility: ''
  };

  const updateSettings = ({ parameters }) => {
    const options = {};

    try {
      const content = JSON.stringify(parameters, APPLICABLE_PROPERTIES, 2);
      Object.assign(options, JSON.parse(content));
    } catch(ignored) {
      /** Intended to be ignored */
    }

    Object.assign(settings, options);
  };

  const getMessageTemplate = ({ key, parameters }) => {
    let messageTemplate = key;
    let fullQualifiedKey = `${key}-${settings.customer}`;
    parameters = Object.assign({}, parameters);

    if (Object.prototype.hasOwnProperty.call(messageBundle, fullQualifiedKey)) {
      messageTemplate = messageBundle[fullQualifiedKey];

      messageTemplate = messageTemplate.replace(PARAMETER_EXPRESSION,
        (parameter) => parameters[parameter] || `MISSING PARAMETER ${parameter}!!`);
    }

    return Object.freeze({
      key,
      text: messageTemplate,
      toString: () => messageTemplate
    });
  };

  const messageBundle = Object.freeze({
    'composite-rheemwater': 'System State: Operational Overview'
  });

  return Object.freeze({
    config: (parameters) => {
      /** TODO: check for changes to determine if the update is needed */
      updateSettings({ parameters });
    },
    transpose: ({ key }) => {
      return getMessageTemplate({ key, settings });
    }
  });
}));