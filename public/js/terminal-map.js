/* eslint-disable no-undef,no-unused-vars */
async function renderNoDataAvailable(parameters) {
  d3.select('#subjectCount').text(`${Object.keys(reportCfg).length}`);
  let subjects = await d3.json('/portal/data/getSubjectList?customer=' + parameters.customer + '&facility='+parameters.facility, {credentials: 'include'});
  let currentStatus = d3.select('#currentStats').selectAll('div.statusBadge').data(subjects);
  let newCurrentStat = currentStatus.enter().append('div').classed('statusBadge',true);
  let statusBadge = newCurrentStat.append('div').classed('stats',true);
  // statusBadge.append('div').classed('gateName',true);
  statusBadge.append('div').classed('no-data',true);
  statusBadge.append('div').classed('updateTime',true);
  let mergedCurrentStat = newCurrentStat.merge(currentStatus);
  mergedCurrentStat.select('div.stats').attr('class',d=>{
    let gateName = d.toLowerCase().replace(' ','-');
    return gateName;
  }).classed('stats',true);
  mergedCurrentStat.select('div.no-data').text(() => `No Data Available`);
  mergedCurrentStat.select('div.updateTime').text(() => 'at ' + moment.tz(moment(),timezone).format('HH:mm'));
  currentStatus.exit().remove();
}

function renderStats(stats) {
  d3.select('#subjectCount').text(`${stats.length}`);
  let currentStatus = d3.select('#currentStats').selectAll('div.statusBadge').data(stats);
  let newCurrentStat = currentStatus.enter().append('div').classed('statusBadge',true);
  let statusBadge = newCurrentStat.append('div').classed('stats',true);
  statusBadge.append('div').classed('gateName',true);
  statusBadge.append('div').classed('duration',true);
  statusBadge.append('div').classed('updateTime',true);
  let mergedCurrentStat = newCurrentStat.merge(currentStatus);
  mergedCurrentStat.select('div.stats').attr('class',d=>{
    let gateName = Object.keys(d)[0].toLowerCase().split(' ').join('-');
    return gateName;
  }).classed('stats',true);
  mergedCurrentStat.select('div.stats').classed('clickable',true).on('click',d=>{
    let gateName = Object.keys(d)[0];
    window.location.href = `/portal/reports/subjectDetail?customer=${parameters.customer}&facility=${parameters.facility}&subject=${gateName}`;
  }).style('background-color',d => {
    let [ summaryObject ] = Object.values(d);
    return summaryObject.color;
  }).style('color',d=>{
    let [ summaryObject ] = Object.values(d);
    return parameters.customer==='tia' && summaryObject.color==='#a6a6a6' || summaryObject.color=='#eee'? 'black' : 'white';
  });
  mergedCurrentStat.select('div.gateName').text(d => {
    let [ gate ] = Object.keys(d);
    return !gate.includes('Press Brake') ? `${gate}` : '';
  });
  mergedCurrentStat.select('div.duration').text(d => {
    let [ summaryObject ] = Object.values(d);
    return summaryObject.state;
  });
  mergedCurrentStat.select('div.updateTime').text(d => {
    let [ summaryObject ] = Object.values(d);
    return summaryObject.duration;
  });
  currentStatus.exit().remove();
}

function durationStat(lastState) {
  var duration = moment(lastState.endTime).diff(moment(lastState.startTime));
  return formatDurationAsTime(duration);
}

function createDataToRender(data){
  let statArr = [], timeslotSizeMillis = data.timeslotSizeMillis, highWaterMarkTime = data.highWaterMarkTime;
  let subjects = Object.keys(reportCfg).filter(x=>x!=='Stand 101 PTZ');
  subjects.forEach(d=>{
    let gateStat = {}, lastState={};
    let subjectData = data.subjects.filter(x=>x.name === d);
    gateStat[d] = {};
    if(subjectData.length){
      let composite = subjectData[0].measures[definitions.graphMeasure];
      if(composite.length){
        var lastCompositeState = lastAvailableState(composite);
        gateStat[d].state = reportCfg[d][definitions.graphMeasure]['stateLabels'][lastCompositeState.state];
        gateStat[d].duration = durationStat(lastCompositeState);
        gateStat[d].color = reportCfg[d][definitions.graphMeasure]['stateColors'][lastCompositeState.state];
        gateStat[d].label = reportCfg[d][definitions.graphMeasure]['stateLabels'][lastCompositeState.state];
        if (!composite) return moment(highWaterMarkTime).format('h:mm a');
        lastState = composite[composite.length - 1];
      } else{
        gateStat[d].state = 'No Data Available';
        gateStat[d].duration = '';
        gateStat[d].color = '#c0c0c0';
        gateStat[d].label = '';
        lastState.endTime = moment(highWaterMarkTime).format('h:mm a');
      }
    }else{
      gateStat[d].state = 'No Data Available';
      gateStat[d].duration = '';
      gateStat[d].color = '#c0c0c0';
      gateStat[d].label = '';
      lastState.endTime = moment(highWaterMarkTime).format('h:mm a');
    }
    gateStat[d].recordedTime = moment.tz(lastState.endTime, timezone).subtract(timeslotSizeMillis, 'milliseconds');
    statArr.push(gateStat);
  });
  return statArr;
}

async function handleIntervalFiredForMap(parameters) {
  d3.select('#reportDate').attr('value', parameters.date);
  d3.select('#reportDateTitle div.date').text(`${upperFirst(parameters.shift)} Shift for ${moment(parameters.date).format('MMMM Do, YYYY')}`);
  d3.select('#reportDateTitle div.tz').text(` ${timezone}`);

  //Deleting date, shift and time parameters in-case they leak into parameters from other reports
  //This is beacuse Terminal Map can only ever be in Live Mode
  delete parameters.date;
  delete parameters.shift;
  delete parameters.time;

  let data = await reload(parameters);
  let arrayOfStats = [];
  if (!Object.keys(data).length) {
    renderNoDataAvailable(parameters);
  } else {
    arrayOfStats = await createDataToRender(data);
    if(arrayOfStats.length){
      renderStats(arrayOfStats);
    }
  }
}

async function loadTerminalStats(viewStateParameters, isReload) {
  const parameters = Object.assign({}, await getQueryParams(viewStateParameters));
  await constructFacilityPicker(parameters);
  if (!isReload) {
    renderBreadCrumb(parameters, 'terminal-map');
  }
  await checkLiveModeOn(parameters);
  d3.select('#reportDate').attr('value', parameters.date);
  d3.select('#reportDateTitle div.date').text(`${upperFirst(parameters.shift)} Shift for ${moment(parameters.date).format('MMMM Do, YYYY')}`);
  d3.select('#reportDateTitle div.tz').text(` ${timezone}`);
  await fetchCfg(parameters);

  setTimeout(() => loadTerminalStats(viewStateParameters, true), 60 * 1000);

  let statArr=[];

  //Deleting date, shift and time parameters in-case they leak into parameters from other reports
  //This is beacuse Terminal Map can only ever be in Live Mode
  delete parameters.date;
  delete parameters.shift;
  delete parameters.time;

  let data = await reload(parameters);
  $('.loading').hide();
  renderMap();
  if(!Object.keys(data).length){
    renderNoDataAvailable(parameters);
  }else{
    statArr = await createDataToRender(data);
    if(statArr.length){
      renderStats(statArr);
    }
  }
}
