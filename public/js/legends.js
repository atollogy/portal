'use strict';

(function (global, newLegendModule) {
  const Atollogy = global.Atollogy || (global.Atollogy = {});

  Object.assign(Atollogy, newLegendModule(Atollogy));
}(this, ({ newColorProvider, newLabelProvider, hasFlashingState }) => {
  const Legends = Object.freeze({
    render: (oReportConfig, data, subjectName, mergedTimelines, { States, mergeUtilization }) => {
      const theHighWaterMark = Date.now() > Date.parse(data.shiftEndTime) ? data.shiftEndTime : data.highWaterMarkTime;
      const onLegendDataElement = function (theSubjectEvent) {
        let stats = States.stats(
          theSubjectEvent.values, oReportConfig, theHighWaterMark,
          theSubjectEvent.subject, theSubjectEvent.name
        ).map(v => {
          v.measure = theSubjectEvent.name;
          return v;
        });

        // Modifying stats in Legends to apply uniform_state_legend_name from the config
        // And only keeping one state, summing duration of all the individual states
        if (oReportConfig[theSubjectEvent.subject][theSubjectEvent.name]['uniform_state_legend_name']) {
          stats.forEach(function (stat) {
            stat.state = oReportConfig[theSubjectEvent.subject][theSubjectEvent.name]['uniform_state_legend_name'];
          });
          if (stats.length) {
            const totalDuration = stats.reduce((sum, { duration }) => sum + duration, 0);
            stats[0].duration = totalDuration;
          }
          stats.splice(1);
        }

        return stats;
      };
      // Hiding Utilization duration and percentages for the state legends if specified in the config
      const onLegendAnnotation = function(theSubjectEvent) {
        if (!oReportConfig[theSubjectEvent.subject][theSubjectEvent.measure]['hide_utilization_from_legends']) {
          return mergeUtilization(theSubjectEvent, data);
        }
      };

      //the legend
      var legendEntries = mergedTimelines.select('div.legend').selectAll('div.legendEntry').data(onLegendDataElement);
      var newLegendEntries = legendEntries.enter()
        .append('div')
        .classed('legendEntry', true)
        .classed('hidden', (d) => d.duration === 0 || d.instances === 0);

      const theColorProvider = newColorProvider(oReportConfig);
      const theLabelProvider = newLabelProvider(oReportConfig);
      newLegendEntries.append('svg').classed('legendSwatch', true)
        .append('circle')
        .classed('flashing', hasFlashingState)
        .attr('stroke-width', '0.75')
        .attr('stroke', 'black')
        .attr('cx', 5)
        .attr('cy', 5)
        .attr('r', 5)
        .attr('fill', theColorProvider);

      newLegendEntries.append('div').classed('legendLabel', true).text(theLabelProvider);
      newLegendEntries.append('div').classed('legendAnnotation', true);

      newLegendEntries.merge(legendEntries).select('div.legendAnnotation').text(onLegendAnnotation);

      legendEntries.exit().remove();
    }
  });
  return Object.freeze({ Legends });
}));