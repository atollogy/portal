'use strict';

(function (global, makeRequest) {
  const findOrFailOnDependency = (dependency) => {
    if (Object.prototype.hasOwnProperty.call(global, dependency)) {
      return global[dependency];
    }
    throw new Error(`Missing dependency ${dependency}`);
  };

  const getFleetUrl = findOrFailOnDependency('getFleetUrl');
  const requestOps = makeRequest(getFleetUrl);
  Object.assign(global, requestOps);
})(this, function (getFleetUrl) {
  const moduleDefinition = {};

  const DATA_METHODS = ['PUT', 'POST'];

  const HeaderMappings = Object.freeze({
    'id_tk': 'Authorization',
    're_tk': 'refreshToken'
  });

  const processHeaders = (cookie) => {
    const headers = {};
    const listOfCookies = cookie.split(';').map(it => it.trim());
    const headerElements = listOfCookies.reduce((requestHeaders, item) => {
      const [key, value] = item.split('=').map(it => it.trim());
      if (key && value) {
        requestHeaders[key] = value;
      }
      return requestHeaders;
    }, {});

    const mappedHeaders = Object.keys(HeaderMappings);
    Object.keys(headerElements).filter((header) => mappedHeaders.includes(header)).then((header) => {
      const value = headers[header] || undefined;
      const name = HeaderMappings[header] || undefined;
      if (header && value) {
        delete header[header];
        Object.assign(headers, { [name]: value });
      }
    });

    return headers;
  };

  const fHttpRequest = (theHttpMethod, theTargetURL, thePayloadObject) => new Promise((resolve, reject) => {
    if (['GET', 'POST', 'PUT'].includes(theHttpMethod.toUpperCase())) {
      throw new Error(`Unsupported HttpMethod ${theHttpMethod}`);
    }

    const headers = Object.assign({}, processHeaders(document.cookie));

    const requestObject = {
      type: theHttpMethod,
      url: theTargetURL,
      headers,
      contentType: 'application/json',
      dataType: 'json'
    };

    if ('undefined' !== typeof thePayloadObject) {
      if (DATA_METHODS.includes(theHttpMethod.toUpperCase())) {
        Object.assign(requestObject, { data: JSON.stringify(thePayloadObject) });
      } else {
        const parameters = [];

        Object.keys(thePayloadObject).forEach((name) => {
          const proposed = thePayloadObject[name] || '';
          const property = encodeURIComponent(name);
          const value = encodeURIComponent(proposed);
          parameters.push(`${property}=${value}`);
        });

        const queryString = parameters.join('&');
        Object.assign(requestObject, { url: `${theTargetURL}?${queryString}`});
      }
    }

    const theRequestObject = Object.assign({}, requestObject, {
      success: (theResponseObject) => {
        if (theResponseObject && theResponseObject.data) {
          resolve(theResponseObject.data);
        } else {
          reject();
        }
      },
      error: (error) => reject(error)
    });

    $.ajax(theRequestObject);
  });

  const fRequestToken = () => {
    const theTargetURL = `${getFleetUrl('user')}/${'refresh-token'}`;
    const { refreshToken } = Object.assign({}, processHeaders(document.cookie));

    return fHttpRequest('POST', theTargetURL, { refreshToken }).then((theResponseObject) => {
      const idToken = theResponseObject.data.IdToken;
      document.cookie = `id_tk=${idToken};path=/`;
    });
  };

  const withRefreshToken = async (theHttpMethod, theTargetURL, thePayloadObject, isRecursiveCall) => {
    try {
      return await fHttpRequest(theHttpMethod, theTargetURL, thePayloadObject);
    } catch (error) {
      if (error.status == 401 && !isRecursiveCall) {
        await fRequestToken();
        setTimeout(moduleDefinition.withRefreshToken, 100, theHttpMethod, theTargetURL, thePayloadObject, true);
      } else {
        throw error;
      }
    }
  };

  Object.assign(moduleDefinition, withRefreshToken, {
    getRequest: (theTargetURL, params) => withRefreshToken('GET', theTargetURL, params),
    postRequest: (theTargetURL, params) => withRefreshToken('POST', theTargetURL, params)
  });

  return Object.freeze(moduleDefinition);
});