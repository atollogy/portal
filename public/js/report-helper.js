/* eslint-disable no-unused-vars,no-undef */

if (!Object.prototype.hasOwnProperty.call(this, 'window')) {
  // eslint-disable-next-line no-redeclare
  var moment = require('moment');
  const jQuery = require('jquery');
  const { JSDOM } = require('jsdom');
  const { window } = new JSDOM();

  $ = jQuery(window);
  searchLocation = () => ({});
  computeCycleMetrics = require('../../lib/cycles.js').computeCycleMetrics;
  getCustomerFilterForSite = require('../../db/customerLPFilters').getCustomerFilterForSite;

  module.exports = { window, $, jQuery, getQueryParams, searchLocation };
} else {
  searchLocation = () => window.location.search;
}

let liveMode = false;
let nextShift;
let refreshBaseTime;
let urlParams = {};
let shiftRefreshInterval;
let timezone = null;
let filteredData = [];

let facilitiesAcrossAllCgrs = {};

const toQueryString = (valueObject) => {
  return Object.keys(valueObject).map(function (key) {
    return encodeURIComponent(key) + '=' + encodeURIComponent(valueObject[key]);
  }).join('&');
};

//Collect features for given customer-facility report
async function fetchFeatures(parameters) {
  const { customer, facility, page } = parameters;
  return (await d3.json(`/portal/config/featureCfg?customer=${customer}&facility=${facility}&page=${page}`, {credentials: 'include'}));
}

async function fetchYardCfg({ customer, facility }) {
  let cfg = await d3.json('/portal/config/getYardConfig?customer=' + customer + '&facility=' + facility, {credentials: 'include'});
  return cfg;
}

async function setLogoLandingPage(customer) {
  let landingPage = await d3.json(`/portal/config/landingPage?customer=${customer}`, {credentials: 'include'});
  let landingPageLink = d3.select('#landing_link').attr('href', landingPage.url);
}

async function fetchAllFacilitiesPermitted(parameters) {
  //Fetch list of facilities across all the cgrs this user belongs to
  let allFacilities = {};
  const { customer, facility } = parameters;
  let permittedFacilities = await d3.json('/portal/data/getPermittedFacilities/', {credentials: 'include'});
  const customers = cgrs.some(current => current === 'atl') ? [customer] : cgrs;
  for (const currentCustomer in customers) {
    const url = '/portal/data/getFacilityList?customer=' + customers[currentCustomer];
    const facilitiesForThisCgr = await d3.json(url, {credentials: 'include'});
    facilitiesForThisCgr.forEach(currentFacility => allFacilities[currentFacility] = customers[currentCustomer]);
  }

  //Fetch list of facilities permitted to this user and filtering out from the list of all the facilities across all the cgrs
  let displayFacilities = [];
  if (permittedFacilities != null && permittedFacilities.length != 0) {
    displayFacilities = Object.entries(allFacilities).filter(arr=> permittedFacilities.indexOf(arr[0]) !==-1);
  } else {
    displayFacilities = Object.entries(allFacilities);
  }
  /* TODO: emit event about facility selection */
  if (!facility || !displayFacilities.some((currentFacilites) => currentFacilites[0] == facility)) {
    parameters.facility = displayFacilities[0][0];
  } else {
    displayFacilities.sort((x, y) => x[0] == facility ? -1 : y == facility ? 1 : 0);
  }
  displayFacilities.forEach(x=>facilitiesAcrossAllCgrs[x[0]] = x[1]);
  return displayFacilities;
}

function getQueryParams(parameters = {}) {
  let search = searchLocation();

  search = (search && search.length) ? search : '';
  if (search.startsWith('?')) {
    search = search.substring(1);
  }

  /* Process hash */
  const index = search.indexOf('#');
  if (index >= 0) {
    search = search.substring(0, index);
  }

  const queryString = decodeURI(search);

  const newParameters = Object.assign({}, parameters);
  queryString.split('&').forEach(function (queryParameter) {
    let parts = queryParameter.split('=');
    if (parts.length == 2) {
      const [ name, value ] = parts;
      newParameters[name] = value;
    }
  });

  return newParameters;
}

function constructShiftPicker(parameters) {
  const { date, customer, shift, facility } = parameters;
  d3.json('/portal/data/getShifts/' + customer + '?date=' + date + '&facility=' + facility, {credentials: 'include'}).then(function (shifts) {
    let shiftNavDiv = $('#shiftNav');
    const shiftButtons = shifts.map((theCurrentShift) => {
      const theShiftId = theCurrentShift.replace(/\s+/g, '_');
      return `<button class="btn inactive-link" id="${theShiftId}" onclick="updateUrl({ shift:'${theCurrentShift}' })">${theCurrentShift}</button>`;
    });
    shiftNavDiv.append(`<div class="btn-group shift-nav" role="group" aria-label="Third group">${shiftButtons.join('')}</div>`);
    let shiftId = '#' + shift.replace(/\s+/g, '_');
    if ($(shiftId).hasClass('inactive-link')) {
      $(shiftId).removeClass('inactive-link');
      $(shiftId).addClass('active-link');
    }
  });
}

async function constructFacilityPicker(parameters) {
  try {
    let facilities = await fetchAllFacilitiesPermitted(parameters);
    let displayFacilities = facilities.map(x=>x[0]);
    renderFacilityPicker(displayFacilities);
  } catch (e) {
    console.log(e);
    window.location.href = '/portal/reports/error';
  }

}

function renderFacilityPicker(facilities, selectedIndex) {
  const theSelectedIndex = selectedIndex && /^[0-9]+$/.test(selectedIndex) ? Number(selectedIndex) : 0;
  const facilityPicker = $('#facilityPicker');

  facilityPicker.empty();
  const options = facilities.map((facility, index) => {
    return `<option ${theSelectedIndex === index ? 'selected': ''} value="${facility}">${facility}</option>`;
  });
  const htmlTemplate = options.join('');
  facilityPicker.append(htmlTemplate);

  const theFacilityPicker = $('#facilityPicker');
  if (theFacilityPicker && theFacilityPicker.length) {
    $('#facilityPicker').selectpicker('refresh');
  }
}

function constructDatePicker(parameters) {
  const { customer, facility, date: selectedDate } = parameters;

  const datePickerOptions = {
    format: 'yyyy-mm-dd',
    todayHighlight: true,
    autoclose: true,
    defaultViewDate: selectedDate,
    beforeShowDay: $.datepicker.noWeekends
  };

  try {
    d3.json(`/portal/data/reportsList/${customer}?facility=${facility}`, {credentials: 'include'}).then((listOfDates) => {
      const arrayOfDates = Object.keys(listOfDates).map((dateEntry) => listOfDates[dateEntry].date);

      Object.assign(datePickerOptions, {
        beforeShowDay: (theCandidateDate) => {
          let theDateAsString = moment(theCandidateDate).format('YYYY-MM-DD');
          return arrayOfDates.indexOf(theDateAsString) != -1;
        }
      });

      $('#reportDate').datepicker(datePickerOptions);
    });
  } finally {
    $('#reportDate').datepicker(datePickerOptions);
  }
}


function renderSubjectPicker(subjectList, subject) {
  let subjectNavDiv = $('#subjectNav');
  subjectNavDiv.empty();
  let subjectArr = subjectList.sort(naturalCompare);
  subjectArr.forEach(function (x) {
    subjectNavDiv.append(`<button class="list-group-item list-group-item-action inactive-link" onclick="updateUrl({ subject: '${x}' })" id="${x.replace(/ /g, '')}">${x}</button>`);
  });
  if (subject !== undefined) {
    let subjectId = '#' + subject.replace(/ /g, '');
    if ($(subjectId).hasClass('inactive-link')) {
      $(subjectId).removeClass('inactive-link');
      $(subjectId).addClass('active-link');
    }
  }
}

async function updateUrl(newParams) {
  let [ baseUrl ] = window.location.href.split('?');
  const sanitizedParameters = await getQueryParams();

  // delete shift from newParams as it may not be valid when we navigate to different date
  if ('date' in newParams) {
    delete sanitizedParameters.shift;
  }

  // delete subject and shift as it may not be valid when we navigate to different facility
  if ('facility' in newParams) {
    if (sanitizedParameters.subject) {
      delete sanitizedParameters.subject;
      delete sanitizedParameters.shift;
    }
    const navs = await d3.json(`/portal/config/navCfg?customer=${newParams.customer}&facility=${newParams.facility}`, {credentials: 'include'});
    let currentReportType = $('.nav-link.active').text();
    let sameType = false;
    for (let i in navs) {
      if (navs[i].label === currentReportType) {
        sameType = true;
        break;
      }
    }
    if (!sameType) {
      if (navs && navs.length) {
        let newBaseUrl = baseUrl.split('/');
        newBaseUrl.splice(4,6);
        baseUrl = newBaseUrl.join('/') + navs[0].path;
      }
    }
  }

  let updatedParams = Object.assign({}, sanitizedParameters, newParams);
  let newQueryString = toQueryString(updatedParams);
  window.location.href = `${baseUrl}?${newQueryString}`;
}

function formatDurationAsPercentage(duration, total) {
  return total != 0 ? Math.round(1000 * duration / total) / 10 + '%' : '0%';
}

function shiftIsActive() {
  return moment(lastResult.highWaterMarkTime).add(lastResult.timeSlotSizeMillis, 'millisecond').isBefore(moment(lastResult.shiftEndTime));
}

function isHistoricalDay() {
  return moment(lastResult.shiftEndTime).startOf('day').isBefore(moment().startOf('day'));
}

function formatDurationChangeAsPercentage(change, total) {
  if (change) {
    return ' (' + ((change >= 0) ? '+' : '') + (Math.round(1000 * change / total) / 10) + '%)';
  } else {
    return '';
  }
}

function formatDuration(duration) {
  const theDuration = moment.duration(duration).humanize();
  return theDuration === 'a few seconds' ? 'just now' : theDuration;
}

function renderNoShift(shift, date) {
  d3.select('#message').text(`There is either no data for the shift for the date ${date}, or the shift has not started yet`)
    .style('display', 'btn-block')
    .attr('align','center');
}

function calculateVisitLogDuration(earlierTime, laterTime, displayTrail=true) {
  let diff = laterTime.diff(earlierTime);
  let h = (diff / 3600000) >>> 0;
  let m = ((diff % 3600000) / 60000) >>> 0;

  let hDisplay = h > 0 ? h + (h == 1 ? ' hour ' : ' hours ') : '';
  let mDisplay = m > 0 ? m + (m == 1 ? ' minute ' : ' minutes ') : '';
  let trail = (hDisplay == '' && mDisplay == '') ? 'just now' : 'ago';
  return hDisplay + mDisplay + (displayTrail ? trail : '');
}

function upperFirst(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function formatDurationAsTime(duration) {
  const elements = [];
  let timeLeftInSeconds = (duration / 1000) >>> 0;

  const days = (timeLeftInSeconds / (24 * 60 * 60)) >>> 0;
  elements.push(`${days} days`);
  timeLeftInSeconds = timeLeftInSeconds - (days * 24 * 60 * 60);

  const hours = (timeLeftInSeconds / (60 * 60)) >>> 0;
  elements.push(`${hours} hrs`);
  timeLeftInSeconds = timeLeftInSeconds - (hours * 60 * 60);

  const minutes = (timeLeftInSeconds / 60) >>> 0;
  timeLeftInSeconds = timeLeftInSeconds - (minutes * 60);

  const seconds = timeLeftInSeconds >>> 0;
  const fraction = Number((seconds / 60).toFixed(1));

  const theMinutes = minutes + fraction;
  elements.push(`${theMinutes.toFixed(1)} mins`);

  return elements.filter(element => !element.startsWith('0')).join(' ');
}

function isConfiguredForImage(data) {
  let compositeData = data.measures[definitions.graphMeasure];
  for (let d in compositeData) {
    if (compositeData[d].images.length > 0) {
      return true;
    }
  }
  return false;
}

function numberWithCommas(num) {
  return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}

function goToPreviousDate(parameters) {
  updateUrl({ date: moment(parameters.date).subtract(1, 'days').format('YYYY-MM-DD') });
}

function goToNextDate(parameters) {
  updateUrl({ date: moment(parameters.date).add(1, 'days').format('YYYY-MM-DD') });
}

function hideNoDataIndicator() {
  d3.select('#noDataIndicator').style('display', 'none');
}

function showNoDataIndicator() {
  d3.select('#noDataIndicator').style('display', 'block');
}

function handleliveModeUpdate(parameters) {
  window.location.href = window.location.href.split('?')[0] + '?customer=' + parameters.customer;
}

async function getLatestConfig() {
  return await d3.json('/portal/screen/latestConfig', {credentials: 'include'});
}

function safe(obj) {
  // from https://www.beyondjava.net/elvis-operator-aka-safe-navigation-javascript-typescript
  return new Proxy(obj, {
    get: function (target, name) {
      const result = target[name];
      if (result) {
        return (result instanceof Object) ? safe(result) : result;
      }
      console.error('Missing property is sought.', name, target);
      return safe({});
    }
  });
}

async function checkLiveModeOn(parameters) {
  const { customer, shift, date, time } = parameters;
  let result = await d3.json('/portal/data/currentShiftInfo/' + customer + '?' + toQueryString(parameters), {credentials: 'include'});
  let inProgressShifts = result.filter(shiftInfo => shiftInfo.inProgress);
  let targetShift = {};
  if (date) {
    // Calculate and assign in-progress / first completed shift for the date mentioned in the parameters
    let shiftsForParamsDate = result.filter((currentShift) => currentShift.date === date);
    let shiftInProgress = shiftsForParamsDate.filter(shiftInfo => shiftInfo.inProgress);
    targetShift = shiftInProgress .length ? shiftInProgress[0] : shiftsForParamsDate[0];
  } else {
    // If don't receive date in parameters then calculate and assign current in-progress / latest completed shift
    if (inProgressShifts.length) {
      targetShift = inProgressShifts[0];
    } else {
      let completedShifts = result.filter(shiftInfo => shiftInfo.completed);
      targetShift = completedShifts[completedShifts.length - 1];
    }
  }
  if (!date && !time && !shift) {
    //live mode on
    liveMode = true;
    nextShift = result.filter(shiftInfo => !shiftInfo.completed && !shiftInfo.inProgress)[0];
    refreshBaseTime = (targetShift.endTime > nextShift.startTime) ? targetShift.endTime : nextShift.startTime;
    // Set Timeout (+ 5 mins) for loading next shift
    shiftRefreshInterval = setTimeout(handleliveModeUpdate, moment(refreshBaseTime).diff(moment()) + (5 * 60 * 1000), parameters);
  }
  try {
    // parameters.time = parameters.time ? parameters.time : moment.tz(moment(), targetShift.timezone).format('YYYY-MM-DD HH:mm:ss');
    parameters.shift = shift ? shift : targetShift.shift;
    parameters.date = date ? date : targetShift.date;
  } catch (e) {
    // console.error('error fetching parameters', e);
    return null;
  }
  timezone = targetShift.timezone;
  urlParams = Object.assign({}, parameters);
  //Dashboard Mode indicator
  d3.select('.liveModeIndicator').classed('liveModeOff', !liveMode);
  if (!liveMode) {
    d3.select('.liveModeIndicator').text('Activate Live Mode');
    d3.select('.liveModeIndicator')
      .on('click', function () {
        delete urlParams.date;
        delete urlParams.shift;
        delete urlParams.time;
        //login to redirect to live mode url
        window.location.href = window.location.href.split('?')[0] + '?' + toQueryString(urlParams);
      });
  } else {
    d3.select('.liveModeIndicator').text('Live Mode On');
  }
}

function renderImg(displayImage, parameters) {
  let imgElements = d3.select('#imgModal div.modal-image-display').selectAll('img.display-image').data((displayImage.name.endsWith('.mp4')) ? [] : [displayImage]);
  imgElements.enter().append('img').classed('display-image', true)
    .merge(imgElements)
    .classed('lazy', true)
    .attr('data-src', (imageObject) => newImageComponent(parameters.customer, imageObject).toImageURL());
  imgElements.exit().remove();

  let videoElements = d3.select('#imgModal div.modal-image-display').selectAll('video.display-video').data((displayImage.name.endsWith('.mp4')) ? [displayImage] : [], function (d) {
    return `${d.gateway_id}/${d.camera_id}/${moment(d.collection_time).unix()}/${d.step_name}`;
  });
  let newVideoElements = videoElements.enter().append('video').classed('display-video', true)
    .attr('loop', 'loop')
    .attr('muted', 'muted')
    .attr('playsinline', 'playsinline');

  newVideoElements.append('source').attr('type', 'video/mp4');

  videoElements.merge(newVideoElements)
    .select('source')
    .classed('lazy', true)
    .attr('data-src', (imageObject) => newImageComponent(parameters.customer, imageObject).toImageURL());
  videoElements.exit().remove();

  let zoomIn = d3.select('div#zoomIn').selectAll('a.zoom-in-link').data([displayImage]);
  let newZoomIn = zoomIn.enter().append('a').classed('zoom-in-link', true);
  newZoomIn.append('i').classed('material-icons', true).text('zoom_in');
  let mergedZoomComp = newZoomIn.merge(zoomIn);
  mergedZoomComp.attr('href', (imageObject) => newImageComponent(parameters.customer, imageObject).toImageURL()).attr('target', 'new');
  zoomIn.exit().remove();

}

const makHeaderMarkup = ({ title, info, icon }) => {
  let header = `<div class="modalRow">
  <span class="modal-title">${title}</span>
  <span class="videoCamIcon">
    <i class="material-icons">${icon}</i>
  </span>`;

  if (info !== undefined) {
    header +=
      `<br/><span class="modal-info">${info}</span>`;
  }
  header += '</div>';
  return header;
};

const renderHeader = (title, info, icon) => $('#attributes').html( makHeaderMarkup({ title, info, icon }) );

async function saveQA(keys, state, parameters) {
  let comment = document.getElementById('commentTxt');
  if (state === null && comment.value === '') {
    comment.classList.add('empty');
  } else {
    comment.classList.remove('empty');
    const reqJSON = Object.assign({
      customer: parameters.customer,
      is_accurate: state,
      comment: comment.value
    }, keys);

    await d3.json('/portal/imageStepAnnotation/setAnnotation', {
      method: 'post',
      body: JSON.stringify(reqJSON),
      headers: {'Content-type': 'application/json; charset=UTF-8'}
    });
  }
  renderFooter(keys, parameters);
}

async function saveSyntheticLP(keys, parameters, lprKey=false, comment=null) {
  let imgKeys = JSON.parse(JSON.stringify(keys));
  if(!lprKey) {
    configObj[parameters.customer][parameters.facility].context.forEach(con => {
      if(con.context_gateway === keys.gateway_id) {
        imgKeys.gateway_id = con.lpr_gateway;
        imgKeys.step_name = con.lpr_step_name;
        return;
      }
    });
  }
  const qcResult = Object.assign({
    'capture': {
      'captured': true
    },
    'readingStatus': {
      'read_incorrectly': '',
      'synthetic' : true,
      'offset': 10
    },
    'primaryReason': {
      'late_image': true
    },
    'vehicleClass': {
      'tractor': true
    }
  });
  if (parameters.customer === 'trimac') {
    qcResult.vehicleClass.trailer = true;
    qcResult.vehicleClass.tractor = false;
  }
  qcResult.readingStatus.read_incorrectly = parameters.licensePlate;
  const reqJSON = Object.assign({
    customer: parameters.customer,
    is_accurate: !lprKey,
    comment: comment || 'Manually inserted LPN',
    qc_result: qcResult
  }, imgKeys);

  let result = await d3.json('/portal/imageStepAnnotation/setAnnotation', {
    method: 'post',
    body: JSON.stringify(reqJSON),
    headers: {'Content-type': 'application/json; charset=UTF-8'}
  });
  if (result.status === 200) {
    d3.select('#annotation-confirmation').selectAll('*').remove();
    d3.select('#annotation-confirmation')
      .append('p')
      .style('color', 'green')
      .attr('id', 'annotation-inserted')
      .text(`License Plate inserted successfully.`);
  } else {
    d3.select('#annotation-confirmation').selectAll('*').remove();
    d3.select('#annotation-confirmation')
      .append('p')
      .style('color', 'red')
      .attr('id', 'annotation-inserted')
      .text(`Sorry, Unexpected Error occured.`);
  }
  renderFooter(keys, parameters);

  $('#imgModal').on('hidden.bs.modal', function (d) {
    document.getElementById('lpnEntry').value = '';
    d3.select('#lpn-entry-row').style('display','none');
    d3.select(`#card-${keys.collection_time}`).remove();
    filteredData.push({
      collection_time: keys.collection_time,
      gateway_id: keys.gateway_id
    });
  });
  return result;
}

async function saveAssetInfo(keys, parameters) {

  let obj = {
    customer: parameters.customer,
    facility: parameters.facility,
    carrierName: parameters.carrierName,
    trailerId: parameters.trailerId,
    licensePlate: parameters.licensePlate
  };

  let result = await d3.json('/portal/customer-asset/addAssetInfo', {
    method: 'post',
    body: JSON.stringify(obj),
    headers: {'Content-type': 'application/json; charset=UTF-8'},
    credentials: 'include'
  });
  return result;
}

async function blacklistContext(imgKeys, parameters) {
  let obj = {
    customer: parameters.customer,
    step_name: imgKeys.step_name,
    collection_time: imgKeys.collection_time,
    gateway_id: imgKeys.gateway_id,
    camera_id: 'video0'
  };
  let result = await d3.json('/portal/imageStep/blacklistContext', {
    method: 'post',
    body: JSON.stringify(obj),
    headers: {'Content-type': 'application/json; charset=UTF-8'},
    credentials: 'include'
  });
  if (result.status === 200) {
    d3.select('#annotation-confirmation').selectAll('*').remove();
    d3.select('#annotation-confirmation')
      .append('p')
      .style('color', 'sienna')
      .attr('id', 'annotation-inserted')
      .text(`Context video blacklisted successfully.`);
    $('#imgModal').on('hidden.bs.modal', function (d) {
      document.getElementById('lpnEntry').value = '';
      d3.select('#lpn-entry-row').style('display','none');
      d3.select(`#card-${imgKeys.collection_time}`).remove();
      filteredData.push({
        collection_time: imgKeys.collection_time,
        gateway_id: imgKeys.gateway_id
      });
    });
  }
}

async function lookupLP(carrierName, trailerId) {
  let paramString = '/portal/customer-asset/getAssetId';
  paramString += `?customer=${parameters.customer}&carrierName=${carrierName}&trailerId=${trailerId}`;

  let licensePlate = await d3.json(paramString, {credentials: 'include'});
  if (licensePlate && licensePlate.length > 0) {

    d3.select('#annotation-confirmation').selectAll('*').remove();
    d3.select('#licenseplate-entry').selectAll('*').remove();
    d3.select('#licenseplate-entry').append('p')
      .style('color', 'green')
      .attr('id', 'found-lp')
      .text(`Found License Plate: ${licensePlate[0]}`);
  } else {
    d3.select('#annotation-confirmation').selectAll('*').remove();
    d3.select('#licenseplate-entry').selectAll('*').remove();
    d3.select('#licenseplate-entry').append('p')
      .style('color', 'red')
      .text(`No matching License Plate found.`);
    carrierName = carrierName.replaceAll(' ','');
    trailerId = trailerId.replaceAll(' ','');
    d3.select('#licenseplate-entry').append('p')
      .attr('id', 'found-lp')
      .style('color', 'green')
      .text(`Constructed Pseudo License Plate: VID:${carrierName}:${trailerId}`);
  }
}

function upateTrailerSuggestions() {
  let carrierName = document.getElementById('carrierName').value;
  if (carrierName) {
    d3.select('#licenseplate-entry').selectAll('*').remove();
    let trailerSuggestions = carrierName in allLPNAssets['assetMap'] ? Object.keys(allLPNAssets['assetMap'][carrierName])
      : [];
    $('#trailer-row .typeahead').typeahead('destroy').typeahead({
      hint: true,
      highlight: true,
      minLength: 1
    },
    {
      name: 'states',
      source: substringMatcher(trailerSuggestions)
    });
  }
}

function clearAnnotationState() {
  document.getElementById('carrierName').value = '';
  document.getElementById('trailerId').value = '';
  d3.select('#licenseplate-entry').selectAll('*').remove();
  d3.select('#annotation-confirmation').selectAll('*').remove();
}

async function applyCorrection(keys, value, parameters) {
  let reqJSON = Object.assign({customer: parameters.customer, weight: value}, keys);
  let result = await d3.json('/portal/imageStepAnnotation/restateWeight', {
    method: 'post',
    body: JSON.stringify(reqJSON),
    headers: {'Content-type': 'application/json; charset=UTF-8'}
  });
  d3.select('#modal-message').text(result.flashMessage);
}

async function setupFooter(imgKeys, tags) {

  if (imgKeys.step_name === 'weight') {
    d3.select('#correction-row').classed('correction-row', false);
  } else {
    d3.select('#correction-row').classed('correction-row', true);
    d3.select('div.tag-trigger').data([tags])
      .on('click', function (d) {
        d3.select('div#tag-trigger-0').classed('success', false);
        let tagsDivs = $('div.tag-sel-item');
        let tagsArr = [];
        for (let i = 0; i < tagsDivs.length; i++) {
          tagsArr.push(tagsDivs[i].innerText);
        }
        let tagObj = {};
        tagObj.newTags = tagsArr;
        tagObj.oldTags = d.tagArr;
        tagObj.subject = d.subject;
        tagObj.customer = parameters.customer;
        tagObj.facility = parameters.facility;
        storeCustomerTags(tagObj);
        d.tagArr = tagObj.newTags;
      });
  }
  d3.select('div#tag-trigger-0').classed('success', false);
  d3.select('div#tag-trigger-0').classed('error', false);
  if (tags && Object.keys(tags).length) {
    if (tags.tagArr.length) {
      if ($('#ms1').length) {
        $('#ms1').tagSuggest().removeSelection();
        $('#tag-input-0').attr('style',null);
      }
      // $('#ms1 #tag-sel-ctn-0').find('div.tag-sel-item ').remove();
      let arr = tags.tagArr;
      let tagArr = [];
      for (let i in arr) {
        let obj = {};
        obj.id = Number(i) + 1;
        obj.name = arr[i];
        tagArr.push(obj);
      }
      if ($('#ms1').length) {
        $('#ms1').tagSuggest().addToSelection(tagArr, true);
        $('#tag-input-0').attr('style',null);
      }
    } else {
      if ($('#ms1').length) {
        $('#ms1').tagSuggest().removeSelection();
        $('#ms1').tagSuggest().addToSelection([], true);
        $('#tag-input-0').attr('style',null);
      }
    }
  }

  d3.select('#trailerId').on('focusout', function () {
    let carrierName = document.getElementById('carrierName').value.toUpperCase();
    let trailerId = document.getElementById('trailerId').value.toUpperCase();
    if (!carrierName || !trailerId) {
      d3.select('#annotation-confirmation').selectAll('*').remove();
      d3.select('#licenseplate-entry').selectAll('*').remove();
      if (!carrierName) {
        document.getElementById('carrierName').value = '';
        document.getElementById('trailerId').value = '';
        d3.select('#licenseplate-entry').append('p')
          .style('color', 'red')
          .text('Carrier Name needs to be entered first');
      } else {
        d3.select('#licenseplate-entry').append('p')
          .style('color', 'red')
          .text('Enter Trailer ID.');
      }
    } else {
      lookupLP(carrierName, trailerId);
    }
  });

  d3.select('#ignore-record').on('click', function () {
    blacklistContext(imgKeys, parameters);
  });

  d3.select('#save-licenseplate').on('click', function () {

    if (document.getElementById('found-lp')) {
      let licensePlate = document.getElementById('found-lp').innerHTML;
      let carrierName = document.getElementById('carrierName').value.toUpperCase();
      let trailerId = document.getElementById('trailerId').value.toUpperCase();
      if (licensePlate.split('Plate: ').length > 1) {
        licensePlate = licensePlate.split('Plate: ')[1];
      }
      parameters.licensePlate = licensePlate.toUpperCase();
      imgKeys.measure = licensePlate;
      let result = saveSyntheticLP(imgKeys, parameters);
      parameters.carrierName = carrierName;
      parameters.trailerId = trailerId;
      let _ = saveAssetInfo(imgKeys, parameters);
      d3.select('#annotation-confirmation').selectAll('*').remove();
    } else {
      d3.select('#annotation-confirmation').selectAll('*').remove();
      d3.select('#annotation-confirmation')
        .append('p')
        .style('color', 'red')
        .attr('id', 'annotation-inserted')
        .text(`Please Enter Carrier Name and Trailer ID to look up License Plate to store`);
    }
  });

  d3.select('#qc-accurate').on('click', function () {
    saveQA(imgKeys, true, parameters);
  });
  d3.select('#qc-inaccurate').on('click', function () {
    if (reportType === 1 || reportType === 2) {
      d3.select('#lpn-entry-row').style('display','flex');
    }
    saveQA(imgKeys, false, parameters);
  });
  d3.selectAll('#qc-save').on('click', function () {
    if(document.getElementById('lpnEntry')) {
      let licensePlate = document.getElementById('lpnEntry').value.toUpperCase();
      parameters.licensePlate = licensePlate.toUpperCase();
      let comment = `Manually Corrected from ${imgKeys.measure} to ${licensePlate}`;
      imgKeys.measure = licensePlate;
      let result = saveSyntheticLP(imgKeys, parameters, lprKey=true, comment);
    }
    saveQA(imgKeys, null, parameters);

  });
  clearAnnotationState();
  renderFooter(imgKeys, parameters);
}

async function renderFooter(keys, parameters) {
  //Fetch existing annotations: comments and accuracy

  let paramsString = toQueryString(keys);
  let annotData = await d3.json('/portal/imageStepAnnotation/getAnnotation/' + parameters.customer + '?' + paramsString, {credentials: 'include'});

  //Render existing annotations inside modal
  let commentTxt = document.getElementById('commentTxt');
  let correctTionTxt = document.getElementById('correctionTxt');
  if (commentTxt) {
    commentTxt.value = '';
  }
  if (correctTionTxt) {
    correctTionTxt.value = '';
  }
  d3.select('#commentList').html('');
  let commentDiv = d3.select('#commentList').selectAll('div.comment').data(annotData);
  let newComments = commentDiv.enter().append('div').classed('comment', true);
  newComments.append('div').classed('state', true);
  newComments.append('div').classed('date', true);
  newComments.append('div').classed('content', true);
  newComments.append('div').classed('user', true);
  newComments.append('div').classed('actions', true);
  let mergedDivs = newComments.merge(commentDiv);
  mergedDivs.select('div.state').classed('accurate', function (d) {
    return d.isAccurate;
  }).classed('inaccurate', function (d) {
    return d.isAccurate === false;
  }).text(function (d) {
    return d.isAccurate ? 'Accurate' : (d.isAccurate === false ? 'Inaccurate' : '');
  });
  mergedDivs.select('div.content').html('').text(function (d) {
    return d.comment;
  });
  mergedDivs.select('div.date').text(function (d) {
    return moment(d.createdAtTime).format('YYYY-MM-DD h:mm a');
  });
  mergedDivs.select('div.user').text(function (d) {
    return '— ' + d.userName;
  });
  if (!username.includes('atollogy')) {
    d3.select('#ms1').remove();
  }
  commentDiv.exit().remove();
}
function sortByCollectionTime( a, b ) {
  if ( a.imgKey.collection_time < b.imgKey.collection_time ){
    return -1;
  }
  if ( a.imgKey.collection_time > b.imgKey.collection_time ){
    return 1;
  }
  return 0;
}

function renderImgModal(listOfImages, d=null, deliveryHeader=null) {
  clearAnnotationState();

  if (!configObj[parameters.customer] ||
    !configObj[parameters.customer][parameters.facility] ||
    !configObj[parameters.customer][parameters.facility].LPNInfoEnrichment ||
    reportType !== 0
  ) {
    d3.select('#lp-asset-block').style('display', 'none');
  }
  if (d && d.state === 'Missing Data-Some') {
    d.missingMeasures.forEach((measure) => {
      let listOfEmptyRecords = [];
      let uniqueTime = new Set();
      listOfImages.forEach((image) => {
        record = Object.assign({}, image);
        if (!uniqueTime.has(record.linkName)) {
          uniqueTime.add(record.linkNaame);
          record.url = '/portal/images/no-image.png';
          record.imgKey = {};
          record.imgKey.measure = measure;
          listOfEmptyRecords.push(record);
          uniqueTime.add(record.linkName);
        }
      });
      listOfImages = [...listOfImages, ...listOfEmptyRecords];
    });
  }
  if (reportType !== 1) {
    listOfImages = listOfImages.sort(sortByCollectionTime);
  }
  $('#imgModal').modal({backdrop: 'static'});
  const theObservationModal = new ObservationModal(parameters.customer, listOfImages, deliveryHeader);
  theObservationModal.render();

  let listOfImagesCopy = JSON.parse(JSON.stringify(listOfImages));

  if (isNaN(listOfImages[0].imgKey.collection_time)) {
    let keySplit = listOfImages[0].imgKey.collection_time.split('.');
    let collection_time = `${moment(listOfImages[0].imgKey.collection_time).unix()}`;
    if (keySplit.length > 1) {
      if(keySplit[1].split('+').length > 1){
        collection_time += '.' + keySplit[1].split('+')[0];
      }
    }
    listOfImagesCopy[0].imgKey.collection_time = parseFloat(collection_time);
  }

  setupFooter(listOfImagesCopy[0].imgKey, listOfImagesCopy[0].customerTag);
}

async function storeCustomerTags(obj) {
  let result = await d3.json('/portal/customer-asset/addAsset', {
    method: 'post',
    body: JSON.stringify(obj),
    headers: {'Content-type': 'application/json; charset=UTF-8'},
    credentials: 'include'
  });
  if (result.status == 'success') {
    d3.select('div#tag-trigger-0').classed('success', true);
  } else {
    d3.select('div#tag-trigger-0').classed('success', false);
    d3.select('div#tag-trigger-0').classed('error', true);
  }
}

async function renderBreadCrumb(parameters, currentPage) {
  const { customer, facility, time, date } = parameters;
  const navs = await d3.json(`/portal/config/navCfg?customer=${customer}&facility=${facility}`, {credentials: 'include'});
  let navBar = $('.custom-nav');
  let links = '';
  const cust = cgrs.some(current => current === 'atl') ? 'atl' : customer;
  await setLogoLandingPage(cust);
  for (let index = 0; index < navs.length; index++) {
    if (index !== navs.length - 1) {
      links = links + `<a class="nav-link custom-nav-link" href="#">${navs[index].label}</a><span style="padding-top: 8px;font-size: 1.2rem;">|</span>`;
    } else {
      links = links + `<a class="nav-link custom-nav-link" href="#">${navs[index].label}</a>`;
    }
  }
  navBar.append(links);

  let navLinks = d3.select('nav.custom-nav').selectAll('a.custom-nav-link').nodes();
  if (time && !date) {
    parameters.date = moment(time).format('YYYY-MM-DD');
  }
  let newURLParameters = JSON.parse(JSON.stringify(parameters));

  //We do this because checkLiveMode modifies parameters object and adds calculated date/shift/time parameters to it.
  //This means that when we nvaigate to the link from the bread crumb we will no longer be in liveMode on that page
  //since checkLiveMode() (called again from that page) would return false
  if (liveMode) {
    delete newURLParameters.date;
    delete newURLParameters.shift;
    delete newURLParameters.time;
  }

  for (let index in navLinks) {
    if (navs[index].path === (`/reports/${currentPage}`)) {
      d3.select(navLinks[index]).classed('active', true);
      if (navs[index].label !== 'Cycle Time Report V2') {
        if (parameters.version) {
          d3.select(navLinks[index]).classed('active', false);
        }
      }
    }
    if (navs[index].label === 'Cycle Time Report V2') {
      if (!parameters.version) {
        d3.select(navLinks[index]).classed('active', false);
      }
      newURLParameters.version = 2;
    } else {
      delete newURLParameters.version;
    }

    //Generating target url from config and applying it to link anchor
    const url = '/portal' + navs[index].path + '?' + toQueryString(newURLParameters);
    d3.select(navLinks[index]).attr('href', url);
  }

}

function createGroupedArray(arr, chunkSize) {
  var groups = [], i;
  for (i = 0; i < arr.length; i += chunkSize) {
    groups.push(arr.slice(i, i + chunkSize));
  }
  return groups;
}

async function searchLPR(search) {
  searchText = search;
  let searchData = JSON.parse(JSON.stringify(globalData));
  searchData[0].iterations = globalData[0].iterations.filter(function (d) {
    return d.subject.toLowerCase().includes(searchText.toLowerCase());
  });
  let statsData = [];

  searchData[0] = showAll ? searchData[0] : showInferred ? inferCycles(searchData[0]) : keepOnlyCycles(searchData[0]);

  statsData[0] = await cycleStats(searchData[0]);

  statsData[0].iterations.forEach(x => {
    x.states[0].isReportStart = true;
    x.states[x.states.length - 1].isReportEnd = true;
  });
  if (reportView == 'new') {
    statsData = mergeStatesOfTruck(statsData);
  }
  if (sortOrder) {
    statsData[0].iterations = statsData[0].iterations.reverse();
  }
  render(statsData, parameters);
}

async function reloadCustomerTags(parameters) {
  customerTags = await d3.json('/portal/customer-asset/getCustomerTags?customer=' + parameters.customer, {credentials: 'include'});
  var jsonData = [];
  for (var index = 0; index < customerTags.length; index++) {
    jsonData.push({
      id: index,
      name: customerTags[index]
    });
  }
  $('#ms1').tagSuggest({
    data: jsonData,
    sortOrder: 'name',
    maxDropHeight: 200,
    name: 'ms1'
  });
}

const capitalize = (text) => {
  const rSentenceExpr = /\s*(?:\.|$)\s*/;
  return text.split(rSentenceExpr).filter(sentence => !!sentence).map(
    (sentence) => sentence.substring(0, 1).toUpperCase() + sentence.substring(1).toLowerCase() + ' '
  ).join('').trim();
};

function renderCustomerTagsDropDown() {
  let selectPicker = $('.selectpicker').html('');
  const options = customerTags.map((customerTag) => `<option value="${customerTag}">${capitalize(customerTag)}</option>`).join('');
  selectPicker.append(options);
  $('select').selectpicker('refresh');
  d3.selectAll('li a.dropdown-item').on('click', function () {
    filterTags(d3.select(this));
  });
}

const DefaultConfig = Object.freeze({
  'default': {
    'default': {
      timeFilter: 8 * 60 * 60,
      timeSpan: {
        'short': { min: Number.MIN_VALUE, max: 30 * 60, color: '#f59211' },
        'medium': { min: 30 * 60, max: 45 * 60, color: '#1e8f18' },
        'long': { min: 45 * 60, max: Number.MAX_VALUE, color: '#b8122b' }
      }
    }
  }
});

const getHoursToLookBackByCgr = function (pConfigObject, parameters) {
  const theConfigObject = Object.assign({}, DefaultConfig, pConfigObject);
  const { customer, facility } = parameters;

  const oCustomerConfig = theConfigObject[customer] || theConfigObject['default'];
  const oSiteConfig = oCustomerConfig[facility] || oCustomerConfig['default'];
  const cardTimeout = oSiteConfig['timeFilter'] || 3600;

  return Math.max(1, (cardTimeout / 3600)).toFixed(4);
};

function cardColoring(delta) {
  const theConfigObject = Object.assign({}, DefaultConfig, configObj);
  const { customer, facility } = parameters;

  const oDefaultCustomerConfig = theConfigObject['default'];
  const oCustomerConfig = Object.assign({}, oDefaultCustomerConfig, theConfigObject[customer] || undefined);

  const oDefaultSiteConfig = oDefaultCustomerConfig['default'];
  const oSiteConfig = Object.assign({}, oDefaultSiteConfig, oCustomerConfig[facility] || undefined);
  const oColorConfig = oSiteConfig['timeSpan'];

  let durationClass = '';
  Object.keys(oColorConfig).forEach((timeClass) => {
    const oTimeColorConfig = oColorConfig[timeClass];
    const { min: lowerBound, max: upperBound, color } = oTimeColorConfig;
    if (delta > lowerBound && delta <= upperBound) {
      durationClass = color;
    }
  });
  return durationClass;
}

//function to wrap the time axis labels
function wrap(arrayOfText, width) {
  arrayOfText.each(function () {
    let text = d3.select(this),
      words = text.text().split(/\s+/).reverse(),
      word,
      line = [],
      lineNumber = 0,
      lineHeight = 1.1, // ems
      x = text.attr('x'),
      y = text.attr('y'),
      dy = 0,
      tspan = text.text(null)
        .append('tspan')
        .attr('x', x)
        .attr('y', y)
        .attr('dy', dy + 'em');
    while (words.length) {
      word = words.pop();
      line.push(word);
      tspan.text(line.join(' '));
      // if (tspan.node().getComputedTextLength() > width) {
      line.pop();
      tspan.text(line.join(' '));
      line = [word];
      tspan = text.append('tspan')
        .attr('x', x)
        .attr('y', y)
        .attr('dy', ++lineNumber * lineHeight + dy + 'em')
        .text(word);
      // }
    }
  });
}

function renderNoShiftConfigured() {
  d3.select('#message').text(`There is no shift configured for this report`);
}

if (typeof window !== 'undefined') {
  $('#facilityPicker').selectpicker();
}

$(function () {
  $('select#facilityPicker').on('changed.bs.select',
    async function (e, clickedIndex, newValue, oldValue) {

      const newParams = Object.assign({}, {
        customer: facilitiesAcrossAllCgrs[this.value],
        facility: this.value
      });

      updateUrl(newParams);
    });
});

async function filterTags(tags) {
  if (tags.classed('selected')) {
    let index = filterOnTags.indexOf(tags.text());
    filterOnTags.splice(index, 1);
  } else {
    let check = filterOnTags.findIndex(x => x === tags.text());
    if (check === -1) {
      filterOnTags.push(tags.text());
    }
  }
  let filterData = JSON.parse(JSON.stringify(globalData));

  if (reportType == 1) {
    if (filterOnTags.length) {
      filterData[0].iterations = filterData[0].iterations.filter(function (d) {
        return d.customerTags.some(function (tag) {
          return filterOnTags.includes(tag);
        });
      });
    }
    filterData[0] = showAll ? filterData[0] : showInferred ? inferCycles(filterData[0]) : keepOnlyCycles(filterData[0]); //FIXME

    //Calculate cycle stats for filtered data
    filterData[0] = cycleStats(filterData[0]);

    filterData[0].iterations.forEach(x => {
      x.states[0].isReportStart = true;
      x.states[x.states.length - 1].isReportEnd = true;
    });

    if (reportView == 'new') {
      filterData = mergeStatesOfTruck(filterData);
    }

    if (sortOrder) {
      filterData[0].iterations = filterData[0].iterations.reverse();
    }
    render(filterData, parameters);
  } else if(reportType == 0) {
    if (filterOnTags.length) {
      d3.select('div.custom-badge').attr('data-badge', filterOnTags.length);
      filterData[0].subjects = filterData[0].subjects.filter(d => {
        return d.customerTags.some(function (tag) {
          return filterOnTags.includes(tag);
        });
      });
    } else {
      d3.select('div.custom-badge').attr('data-badge', null);
    }
    renderVisitLog(filterData, parameters);
  } else if(reportType == 'Outdoor') {
    //filter by zone
    filterData = zoneFilter.length ? filterZone(filterData) : filterData;
    //filter by tags
    filterData = filterOnTags.length ? filterByTags(filterData) : filterData;
    render(filterData);
  }
}
