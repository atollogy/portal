'use strict';

(function (global, newAtollogyContext) {
  const Atollogy = Object.assign({},newAtollogyContext() );

  const reSubSecondPrecision = /(\.\d{1,})/;
  const reDateStringFormat = /\d{2,4}-\d{1,2}-\d{1,2}T\d{1,2}:\d{1,2}:\d{1,2}(.\d{1,6})/;

  const getTimePrecision = (theCollectionTime) => {
    if (reDateStringFormat.test(theCollectionTime)) {
      return reDateStringFormat.exec(theCollectionTime).find((_, index) => index === 1);
    }

    if (reSubSecondPrecision.test(theCollectionTime)) {
      return reSubSecondPrecision.exec(theCollectionTime / 1000).find((_, index) => index === 1);
    }

    const numberOfDigits = Math.max((Math.log10(theCollectionTime) >>> 0) - 9, 0);
    if (numberOfDigits > 0) {
      const thePrecisionValue = theCollectionTime % Math.pow(10, numberOfDigits);
      return thePrecisionValue > 0 ? `${String(thePrecisionValue / 1000).substring(1)}` : 0;
    }

    return 0;
  };

  const getImageTimestamp = (theCollectionTime) => {
    const theTimeUnix = moment(theCollectionTime).unix();
    const theTimePrecision = getTimePrecision(theCollectionTime);
    return `${theTimeUnix}${Number(theTimePrecision) > 0 ? theTimePrecision : ''}`;
  };

  const newImageComponent = (customer, imageObject) => {
    const theImageInfo = Object.keys(imageObject).reduce((theInstanceObject, attribute) => {
      const value = imageObject[attribute];
      const property = attribute.split('_').reduce((content, part) => {
        if (content) {
          return `${content}${part.substring(0, 1).toUpperCase()}${part.substring(1)}`;
        }
        return part;
      }, undefined);
      return Object.assign(theInstanceObject, { [property]: value });
    }, { customer });

    if (Object.prototype.hasOwnProperty.call(theImageInfo, 'imageName')) {
      /** Nothing to do here */
    } else if (Object.prototype.hasOwnProperty.call(theImageInfo, 'name')) {
      const { name: imageName } = theImageInfo;
      Object.assign(theImageInfo, { imageName });
    }

    const theStateObject = Object.assign({ imageName: 1 }, theImageInfo, {
      timestamp: getImageTimestamp(theImageInfo.collectionTime)
    });

    const theImageComponent = {};

    Object.defineProperty(theImageComponent, 'toResourceURL', {
      value: () => {
        const { gatewayId, cameraId, timestamp, stepName } = theStateObject;
        return `${gatewayId}/${cameraId}/${timestamp}/${stepName}`;
      },
      writable: false,
      enumerable: true
    });

    Object.defineProperty(theImageComponent, 'toImageURL', {
      value: () => {
        const { customer, imageName } = theStateObject;
        return `/portal/image/${customer}/${theImageComponent.toResourceURL()}/${imageName}`;
      },
      writable: false,
      enumerable: true
    });

    Object.defineProperty(theImageComponent, 'withName', {
      value: (imageName) => {
        Object.assign(theStateObject, { imageName });
        return theImageComponent;
      },
      writable: false,
      enumerable: true
    });

    Object.defineProperty(theImageComponent, 'withCollectionTime', {
      value: (collectionTime) => {
        Object.assign(theStateObject, { collectionTime });
        return theImageComponent;
      },
      writable: false,
      enumerable: true
    });

    Object.defineProperty(theImageComponent, 'toSmallImageURL', {
      value: () => `${theImageComponent.toImageURL()}?width=400`,
      writable: false,
      enumerable: true
    });

    Object.defineProperty(theImageComponent, 'toTinyImageURL', {
      value: () => `${theImageComponent.toImageURL()}?width=200`,
      writable: false,
      enumerable: true
    });

    Object.defineProperty(theImageComponent, 'toSourceSet', {
      value: () => [
        theImageComponent.toTinyImageURL(),
        theImageComponent.toSmallImageURL(),
        theImageComponent.toImageURL()
      ].map(
        (theImageURL, index) => `${theImageURL} ${1 + index}x`
      ).join(' '),
      writable: false,
      enumerable: true
    });


    return Object.freeze(theImageComponent);
  };

  Object.assign(global, { Atollogy, reSubSecondPrecision, reDateStringFormat, newImageComponent });
}(this, () => {
  const StateMappings = Object.freeze({ '{}': 'success' });
  const getCustomerFilterForSite = (record) => record;

  return Object.freeze({ StateMappings, getCustomerFilterForSite });
}));
