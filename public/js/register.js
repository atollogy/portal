'use strict';

(function (global, $, makeRegisterModule) {
  const findOrFailOnDependency = (dependency) => {
    if (Object.prototype.hasOwnProperty.call(global, dependency)) {
      return global[dependency];
    }
    throw new Error(`Missing dependency ${dependency}`);
  };

  const postRequest = findOrFailOnDependency('postRequest');
  const registerOps = makeRegisterModule(postRequest);
  document.body.style.paddingBottom = 0;
  document.body.style.minHeight = '100vh';

  Object.assign(global, registerOps);
})(this, $, function (postRequest) {
  function verifyEmail() {
    let email = $('#email').val();
    $.post( '/portal/reset',{email:email}, function( data ) {
      if(data && data.status == 200){
        if($('#message').hasClass('error-verification')){
          $('#message').removeClass('error-verification');
        }
        $('#message').addClass('success-verification').text('Sent an email to address '+email+' for verification.');
        $('#verifyBtn').attr('disabled','disabled');
      }else{
        if($('#message').hasClass('success-verification')){
          $('#message').removeClass('success-verification');
        }
        $('#message').addClass('error-verification').text(email+' is not registered. ');
      }
    });
  }

  async function login() {
    let email = $('#email-login').val();
    try {
      const data = await postRequest(`/portal/sign-in-fleet-api`, {email});
      if (data && data.IdToken && data.RefreshToken && data.AccessToken) {
        const idToken = data.IdToken;
        const refreshToken = data.RefreshToken;
        const accessToken = data.AccessToken;
        document.cookie = `id_tk=${idToken};path=/`;
        document.cookie = `re_tk=${refreshToken};path=/`;
        document.cookie = `ac_tk=${accessToken};path=/`;
      }
    } catch (ignored) {
      // We ignore the login error from the Fleet API for now
      /*
        The existed issue: when the Atollogist has an account on the portal database and doesn’t have an account in the fleet database,
        they can login to the portal but the home page will throw the error.
        Because they don’t have an account in the fleet database, so they don’t have the fleet api token to get the facilities
      */
    }
    $('#login').submit();
    return false;
  }

  return Object.freeze({ verifyEmail, login });
});
