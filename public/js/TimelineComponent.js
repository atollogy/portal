'use strict';

(function (global, makeTimelineComponent) {
  const { EventEmitter } = global;
  const findElementBySelector = (cssSelector) => document.querySelector(cssSelector);
  const findElementsBySelector = (cssSelector) => document.querySelectorAll(cssSelector);

  global.TimelineComponent = makeTimelineComponent(EventEmitter, {
    findElementBySelector,
    findElementsBySelector
  });
}(this, function makeTimelineComponent (EventEmitter, {
  findElementBySelector,
  findElementsBySelector
}) {

  const augmentEventInfo = (eventInfo) => {
    const { target: element } = eventInfo;
    const measureName = element.getAttribute('data-timeline-measure');
    const timelineIndex = element.getAttribute('data-timeline-index');
    return Object.assign({}, eventInfo, {
      measure: measureName,
      to: Number(timelineIndex)
    });
  };

  const makeTimelimeElement = (item, index) => {
    if (item.timestamp) {
      return `
          <li class="atl-timeline-element ${index === 0 ? 'active ' : '' }" data-timeline-measure="${item.measure}" data-timeline-timestamp="${item.timestamp}" data-timeline-index="${index}">
            <div class="atl-timeline-container">
              <a class="atl-timeline-timestamp" data-timeline-measure="${item.measure}" data-timeline-timestamp="${item.timestamp}" data-timeline-index="${index}" href="#">${item.caption}</a>
            </div>
          </li>
        `;
    } else {
      return `<li class="atl-timeline-element ${index === 0 ? 'active ' : '' }" data-timeline-measure="${item.measure}" data-timeline-timestamp="${item.timestamp}" data-timeline-index="${index}">
            <div class="atl-timeline-container">
              <a class="atl-timeline-timestamp" data-timeline-measure="${item.measure}" data-timeline-timestamp="${item.timestamp}" data-timeline-index="${index}" href="#" style="text-decoration: line-through;">${item.caption}</a>
            </div>
          </li>
          `;
    }
  };

  const makeTimelineMarkup = (elementsMarkup, countOfElements) => (`
      <ol class="atl-timeline" data-count="${countOfElements}">${elementsMarkup}</ol>
    `);

  const Model = Object.freeze({
    defaults: {
      elements: []
    }
  });

  const State = Object.freeze({
    defaults: {
      selected: ''
    }
  });

  const TimelineComponent = function ({ selector, model, state }) {
    EventEmitter.call(this);

    Object.assign(this, {
      selector,
      element: findElementBySelector(selector),
      model: Object.assign({}, Model.defaults, model),
      state: Object.assign({}, State.defaults, state)
    });
  };
  TimelineComponent.prototype = Object.create(EventEmitter.prototype);

  TimelineComponent.prototype.render = function (model, state) {
    model = model || this.model || Model.defaults;
    state = state || this.state || State.defaults;
    const { element } = this;

    Object.assign(this, { model, state });

    const countOfElements = model.elements.length || 0;
    const elementsMarkup = model.elements.map((item, index) => makeTimelimeElement(item, index)).join('');
    element.innerHTML = makeTimelineMarkup(elementsMarkup, countOfElements);

    const { emit, markSelected } = this;
    const fireEvent = Function.prototype.bind.call(emit, this);
    const fireMarkSelected = Function.prototype.bind.call(markSelected, this);

    const onTimelineItemClicked = (eventInfo) => {
      eventInfo = augmentEventInfo(eventInfo);
      fireMarkSelected(eventInfo);
      fireEvent('timeline:element:activated', eventInfo);
    };

    const children = findElementsBySelector('.atl-timeline-timestamp');
    if (children.length) {
      let firstVisibleElement = children[0];

      const targetMeasure = firstVisibleElement.getAttribute('data-timeline-measure');
      const { setMeasureSelected } = this;
      setMeasureSelected.call(this, { measure: targetMeasure });
      children.forEach((timelineElement) => {
        timelineElement.addEventListener('click', (eventInfo) => {
          eventInfo.preventDefault();
          return onTimelineItemClicked(eventInfo);
        });
      });
    }
  };

  TimelineComponent.prototype.update = function (model, state) {
    Object.assign(this, {
      model: Object.assign({}, model),
      state: Object.assign({}, state)
    });
  };

  TimelineComponent.prototype.setMeasureSelected = function (eventInfo) {
    const { measure: targetMeasure } = eventInfo;

    let firstVisibleElement;
    const children = findElementsBySelector('.atl-timeline-element:not([hidden])');
    /** hide every entry that doesn't not match the target measure */
    children.forEach((timelineElement) => {
      const measure = timelineElement.getAttribute('data-timeline-measure');
      /** cleanup DOM state */
      timelineElement.classList.remove('hidden');
      timelineElement.classList.remove('active');

      /** Setup expected DOM state */
      timelineElement.classList.add('hidden');
      if (measure === targetMeasure) {
        timelineElement.classList.remove('hidden');
        firstVisibleElement = firstVisibleElement || timelineElement;
      }
    });

    if (firstVisibleElement) {
      /** Mark first visible item as active */
      firstVisibleElement.classList.add('active');
      /** Tell everyone insterested that we just marke an element active */
      this.emit('timeline:element:activated', augmentEventInfo({ target: firstVisibleElement }));
    }
  };

  TimelineComponent.prototype.markSelected = function (eventInfo) {
    const { to: timelineIndex, measure: targetMeasure } = eventInfo;

    const children = findElementsBySelector('.atl-timeline-element');
    children.forEach((timelineElement) => {
      const index = timelineElement.getAttribute('data-timeline-index');
      const measure = timelineElement.getAttribute('data-timeline-measure');

      /** cleanup DOM state */
      timelineElement.classList.remove('hidden');
      timelineElement.classList.remove('active');

      /** Setup expected DOM state */
      timelineElement.classList.add('hidden');
      if(timelineIndex === Number(index)) {
        timelineElement.classList.add('active');
        timelineElement.classList.remove('hidden');
      } else if (measure === targetMeasure) {
        timelineElement.classList.remove('hidden');
      }
    });
  };

  return Object.freeze(TimelineComponent);
}));
