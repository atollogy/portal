/* eslint-disable no-unused-vars,no-undef */
let searchText = '';
let now = moment();
let reportCfg = {};
let configObj = null;

const MAX_COLUMN_WIDTH = 408;
const MAX_COLUMN_HEIGHT = 636;

const attributesToRemove = [
  'license_plate',
  'vehicle_class',
  'workorder_id',
  'customer_tag',
  'beacon_id',
  'badge_id',
  'qc_tag',
  'bin_id'
];

const MARKER = `xxxxyyyy-xxxx-xxxx-xxxxxxxx-yyyyyyyy`.replace(/[xy]/g, (character) => {
  const factor = character === 'x' ? 16 : 25;
  const ramdon = ((Math.random() * factor) | 0);
  return ramdon.toString(factor).toLowerCase();
});

const getOnlineOfflineMessage = ( stat, gwIds ) => {
  let message = '';

  if (stat.length === 0) {
    message = 'All devices are online';
  } else if (stat.length > 0 && stat.length < gwIds.length) {
    let str = stat.map(x => x.name).join(',');
    message = `Device(s) ${str} are offline`;
  } else if (stat.length === gwIds.length) {
    let str = stat.map(x => x.name).join(',');
    message = `${str} is offline`;
  }

  return message;
};

const newColorGenerator = () => {
  let index = -1;

  const colors = [
    'rgba(189, 120, 0, 0.9)'
    /*
     * XXX:
     * (intended) don't transition the colors
     *
    'rgba(189, 127, 20, 0.9)',
    'rgba(189, 141, 58, 0.9)',
    'rgba(189, 148, 77, 0.9)',
    'rgba(189, 155, 96, 0.9)',
    'rgba(189, 148, 77, 0.9)',
    'rgba(189, 141, 58, 0.9)',
    'rgba(189, 127, 20, 0.9)'
    // */
  ];

  return Object.freeze({
    next: () => {
      index = (index + 1) % colors.length;

      return Object.freeze({
        color: colors[index],
        done: false
      });
    }
  });
};

const augmentDataWithTransitions = (data, zoneConfig) => {
  const theDataObject = Object.assign({}, { allZones: [], zonedSubjects: {}, cycleTimeMetrics: { total: Math.pow(30 * 60 * 1000, 2), count: 1 } }, data);

  const totalTimeOfCycles = theDataObject.cycleTimeMetrics.total;
  const countOfCycles = (1e-4 + theDataObject.cycleTimeMetrics.count);
  const theAverageCycleTime = Number.parseFloat((1.5 * Math.sqrt(totalTimeOfCycles / countOfCycles)).toFixed(2));
  console.log(`1.5x Average cycle time is ${theAverageCycleTime}`);

  if (!Object.prototype.hasOwnProperty.call(theDataObject, MARKER)) {
    const { zoneColumns } = Object.assign([], { zoneColumns: {} } , zoneConfig);
    let { zones: listOfZones } = Object.assign([], { zones: {} } , zoneColumns);

    listOfZones = Object.entries(listOfZones).reduce((theZoneConfig, theZoneConfigPair) => {
      const [ name, settings ] = theZoneConfigPair;
      return Object.assign({}, theZoneConfig, { [name]: settings });
    }, {});

    const transitions = theDataObject.allZones.map((element) => {
      const theZoneGroupConfig = {
        transition: true,
        display: `Transition`,
        name: `T.- ${element}`,
        timeout: 5 * 60 * 1000,
        capacity: Number.MAX_VALUE,
        enabled: Object.prototype.hasOwnProperty.call(listOfZones, element)
      };

      if (Object.prototype.hasOwnProperty.call(listOfZones, element)) {
        const theCurrentConfig = Object.assign({}, theZoneGroupConfig, listOfZones[element]);
        const theTransitionTimeout = theCurrentConfig.timeout * 60 * 1000;
        const theTransitionCapacity = theCurrentConfig.capacity <= 0 ? 0x0000000 : 0x7FFFFFFF;
        return Object.assign({}, theZoneGroupConfig, theCurrentConfig, {
          timeout: theTransitionTimeout,
          capacity: theTransitionCapacity,
          enabled: theTransitionCapacity > 0
        });
      }

      return Object.freeze(theZoneGroupConfig);
    });

    const configs = theDataObject.allZones.map((element) => {
      const theZoneGroupConfig = {
        enabled: true,
        transition: false,
        name: `${element}`,
        display: `${element}`,
        timeout: 5 * 60 * 1000,
        capacity: 0x7FFFFFFF
      };

      if (Object.prototype.hasOwnProperty.call(listOfZones, element)) {
        const theCurrentConfig = Object.assign({}, theZoneGroupConfig, listOfZones[element]);
        const theTransitionTimeout = theCurrentConfig.timeout * 60 * 1000;
        const theTransitionCapacity = theCurrentConfig.capacity > 0 ? theCurrentConfig.capacity : 0x7FFFFFFF;
        return Object.assign({}, theZoneGroupConfig, theCurrentConfig, {
          timeout: theTransitionTimeout,
          capacity: theTransitionCapacity,
          scrollable: theTransitionCapacity === 0x7FFFFFFF
        });
      }

      return Object.freeze(theZoneGroupConfig);
    });

    const zonesWithTransitions = [].concat(configs).concat(transitions);
    const zoneConfigMapping = zonesWithTransitions.map((zoneProperties) => {
      const { display } = zoneProperties;
      return Object.assign({}, { [display]: zoneProperties });
    }).reduce(
      (currentObject, arrayOfZones) => Object.assign({}, currentObject, arrayOfZones), {}
    );

    theDataObject.allZones.map(
      (areaOfInterest) => ({
        areaOfInterest: theDataObject.zonedSubjects[areaOfInterest],
        arrayOfZones: zoneConfigMapping[areaOfInterest]
      })
    ).forEach(({ areaOfInterest, arrayOfZones  }) => {
      const { capacity, timeout } = Object.assign({ capacity: 0, timeout: 5 * 60 * 1000 }, arrayOfZones);
      Object.assign(areaOfInterest, { capacity, timeout });
    });

    const oCounterState = { value: 0 };
    const theThresholdTimestamp = 0 + (parameters.time ? moment.tz(parameters.time, timezone) : moment().tz(timezone));
    const zoneSubjectsWithTransitions = Object.keys(theDataObject.zonedSubjects).reduce((zonedSubjects, areaOfInterest) => {
      const { subjects: elements, sortOrder, capacity, timeout } = Object.assign({ subjects: [], sortOrder: false }, theDataObject.zonedSubjects[areaOfInterest]);

      const { active, transition } = elements.reduce((distribution, element) => {
        const [ { startTime } ] = element.states;
        const theEventTimestamp = 0 + moment(startTime);
        const amountOfTimeFromEvent = Math.abs(theEventTimestamp - theThresholdTimestamp);

        const listOfUnits = distribution.active.reduce((listOfStates , theSubjectElement) => {
          const { lastStates } = Object.assign({ lastStates: [] }, theSubjectElement);
          return listOfStates.concat(lastStates);
        }, []);

        const mustTransition = element.lastStates.reduce((isPresent, stateOfInterest) => {
          return isPresent && listOfUnits.includes(stateOfInterest);
        }, true);

        if (mustTransition === false && amountOfTimeFromEvent < timeout) {
          distribution.active.push(element);
          if (distribution.active.length > capacity) {
            element = distribution.active.shift();
            distribution.transition.push(element);
          }
        } else if (capacity !== 0x7FFFFFFF && amountOfTimeFromEvent < theAverageCycleTime) {
          distribution.transition.push(element);
        }

        return distribution;
      }, { active: [], transition: [] });

      /** Do not count zones that will not be displayed */
      const defaultZones = theDataObject.defaultZones || [];
      if (defaultZones.includes(areaOfInterest)) {
        oCounterState.value += active.length + transition.length;
      }
      zonedSubjects[`${areaOfInterest}`] = { subjects: active, sortOrder, capacity, timeout };
      zonedSubjects[`T.- ${areaOfInterest}`] = { subjects: transition, sortOrder, capacity: Math.max(0, elements.length - capacity), timeout };

      return zonedSubjects;
    }, {});

    Object.assign(theDataObject, {[MARKER]: MARKER});

    return Object.assign({}, theDataObject, {
      subjectCount: oCounterState.value,
      allZones: zonesWithTransitions,
      zonedSubjects: zoneSubjectsWithTransitions
    });
  }

  return theDataObject;
};

const newSortOrder = (isDescending) => {
  return isDescending
    ? function fDescendingOrder(left, right) {
      return Math.sign(moment(right.start_time) - moment(left.start_time));
    }
    : function fAscendingOrder(left, right) {
      return Math.sign(moment(left.start_time) - moment(right.start_time));
    };
};

const newSortSubject = (isDescending) => {
  return isDescending
    ? function fDescendingOrder(left, right) {
      if (left.states[0].startTime < right.states[0].startTime) return 1;
      if (left.states[0].startTime > right.states[0].startTime) return -1;
      return 0;
    }
    : function fAscendingOrder(left, right) {
      if (left.states[0].startTime < right.states[0].startTime) return -1;
      if (left.states[0].startTime > right.states[0].startTime) return 1;
      return 0;
    };
};

const makeColumnIndexer = (initial) => {
  const theInternalState = { counter: initial };

  return Object.freeze({
    read: (increment) => {
      const value = theInternalState.counter;
      theInternalState.counter += increment ? 1 : 0;
      return value;
    }
  });
};

function render(originalData) {
  let { gatewayZoneMap, missingDevice } = originalData;

  const zoneConfig = configObj[parameters.customer][parameters.facility] || {};
  const data = augmentDataWithTransitions(originalData, zoneConfig);
  const numberOfColumns = data.allZones.length >> 0x001;

  const theColumnIndexer = makeColumnIndexer(1);
  const theTransitionColumnIndexer = makeColumnIndexer(1);
  const computedColumnHeight = Math.max(135, 20 * Math.floor(window.innerHeight / 40));

  const computedMaxWidth = Math.floor(window.innerWidth / 4);
  const sugestedColumnWidth = 4 * Math.floor((window.innerWidth / numberOfColumns) / 4);
  const computedColumnWidth = Math.max(240, Math.min(computedMaxWidth, sugestedColumnWidth));

  d3.select('div#workOrder').html('');
  d3.select('div#workOrder')
    .style('display', 'grid')
    .style('grid-gap', '0')
    .style('grid-row-gap', '0')
    .style('grid-column-gap', '0')
    .style('grid-template-rows', `40vh auto`)
    .style('grid-template-columns', `repeat(${numberOfColumns}, ${computedColumnWidth}px)`);

  let wo = d3.select('div#workOrder').selectAll('div.zone-col').data(data.allZones);
  let newWo = wo.enter().append('div')
    .classed('zone-col', true)
    .classed('mdl-cell', true)
    .classed('segment-view', true)
    .classed('custom-mdl-cell--col', true)
    .style('max-height', `49%`)
    // .style('overflow-y', 'scroll')
    .style('max-width', `${computedColumnWidth}px`)
    .style('visibility', ({ enabled }) => enabled ? 'visible' : 'hidden')
    .style('grid-row', ({ transition, scrollable }) => transition ? '2' : `${ scrollable ? '1 / span 2' : '1' }` )
    .style('max-height', ({ transition, scrollable }) => (transition || scrollable )? 'auto' : `${computedColumnHeight}px`)
    .style('grid-column', ({ transition }) => transition ? theTransitionColumnIndexer.read(true): theColumnIndexer.read(true) );

  let cardSquare = newWo.append('div')
    .classed('demo-card-square', true)
    .classed('mdl-card', true)
    .classed('zone-card', true)
    .classed('position-relative', true);

  let cardTitle = cardSquare.append('div')
    .classed('mdl-card__title', true)
    .classed('mdl-card--expand', true);

  cardSquare.append('div')
    .classed('mdl-card__supporting-text', true)
    .classed('scrollable-column', true)
    .classed('zone-grids', true);

  let deviceStatIcon = cardTitle.append('div').classed('device-stat', true);
  deviceStatIcon.append('i').classed('material-icons', true).text('brightness_1');
  cardTitle.append('h2').classed('mdl-card__title-text', true);
  cardTitle.append('h5').classed('card-count', true);
  cardTitle.append('div').classed('sort-icon', true);
  let mergeWO = newWo.merge(wo);

  const colorGenerator = newColorGenerator();
  mergeWO.select('div.zone-col div.mdl-card__title')
    .classed('zone', true)
    .classed('position-sticky', true)
    .style('background-color', ({ name }) => {
      if(configObj[parameters.customer][parameters.facility]['zoneColumns']
        && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones']
        && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones'][name]
        && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones'][name]['color']) {
        return configObj[parameters.customer][parameters.facility]['zoneColumns']['zones'][name]['color'];
      }

      /** TODO: (eprha.carvajal) Theme specs for portal to have a default color */
      return colorGenerator.next().color;
    });

  mergeWO.select('div.zone-col h2.mdl-card__title-text').text(function ({ name, display }) {
    let label = (configObj[parameters.customer][parameters.facility]['zoneColumns']
      && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones']
      && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones'][name]
      && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones'][name]['displayName']) || display;
    return upperFirst(label);
  });

  mergeWO.select('div.device-stat').classed('all-devices-good', ({ name }) => {
    let groupName = (configObj[parameters.customer][parameters.facility]['groups'] && configObj[parameters.customer][parameters.facility]['groups'].filter(x=> x.groupName === name)) || [];
    let gwIds = groupName.length ? groupName[0].zones.map(x=> gatewayZoneMap[x]) : gatewayZoneMap[name];
    let stat = 'undefined' === typeof gwIds ? [] : missingDevice.filter(y => gwIds.some(x => x === y.gateway_id && (name === y.zone_name || name === y.name)));
    return stat.length === 0;
  }).classed('few-devices-offline', ({ name }) => {
    let groupName = (configObj[parameters.customer][parameters.facility]['groups'] && configObj[parameters.customer][parameters.facility]['groups'].filter(x=> x.groupName === name)) || [];
    let gwIds = (groupName.length ? groupName[0].zones.map(x=> gatewayZoneMap[x]) : gatewayZoneMap[name]) || [];
    let stat = missingDevice.filter(y => gwIds.some(x => x === y.gateway_id && (name === y.zone_name || name === y.name)));
    return stat.length > 0 && stat.length < gwIds.length;
  }).classed('all-devices-offline', ({ name }) => {
    let groupName = (configObj[parameters.customer][parameters.facility]['groups'] && configObj[parameters.customer][parameters.facility]['groups'].filter(x=> x.groupName === name)) || [];
    let gwIds = (groupName.length ? groupName[0].zones.map(x=> gatewayZoneMap[x]) : gatewayZoneMap[name]) || [];
    let stat = missingDevice.filter(y => gwIds.some(x => x === y.gateway_id && (name === y.zone_name || name === y.name)));
    return stat.length > 0 && stat.length === gwIds.length;
  }).on('mouseover', function ({ name }) {
    tooltip.transition()
      .duration(200)
      .style('opacity', .9);

    let groupName = (configObj[parameters.customer][parameters.facility]['groups'] && configObj[parameters.customer][parameters.facility]['groups'].filter(x=> x.groupName === name)) || [];
    let gwIds = (groupName.length ? groupName[0].zones.map(x=> gatewayZoneMap[x]) : gatewayZoneMap[name]) || [];
    let stat = missingDevice.filter(y => gwIds.some(x => x === y.gateway_id && (name === y.zone_name || name === y.name)));
    let message = getOnlineOfflineMessage(stat, gwIds);

    tooltip.html(message)
      .style('left', (d3.event.pageX - 50) + 'px')
      .style('top', (d3.event.pageY - 28) + 'px');
  }).on('mouseout', function () {
    tooltip.transition()
      .duration(500)
      .style('opacity', 0);
  });

  mergeWO.select('div.zone-col div.sort-icon').html('')
    .append('i').attr('class', function ({ name }) {
      if (data.zonedSubjects[name]) {
        if (data.zonedSubjects[name].sortOrder) {
          return 'fa-sort-alpha-up';
        } else {
          return 'fa-sort-alpha-down';
        }
      }
    }).on('click', function ({ name }) {
      sortZone(data, name);
    }).classed('fas', true);

  d3.select('#woCount').text(`Card Count ${data.subjectCount}`);
  d3.select('#timeTravel').text('Time Travel enabled');

  mergeWO.select('div.zone-col h5.card-count').text(function ({ name }) {
    if (data.zonedSubjects[name]) {
      return ' (' + data.zonedSubjects[name].subjects.length + ')';
    }
  });

  let zoneCardBody = mergeWO.select('div.zone-grids').selectAll('div.mdl-grid').data(function ({ name }) {
    if (data.zonedSubjects[name]) {
      if (data.zonedSubjects[name].subjects !== undefined) {
        if (data.zonedSubjects[name].sortOrder) {
          //descending order based on startTime
          data.zonedSubjects[name].subjects = data.zonedSubjects[name].subjects.sort(function (left, right) {
            if (left.states[0].startTime < right.states[0].startTime) return 1;
            if (left.states[0].startTime > right.states[0].startTime) return -1;
            return 0;
          });
        } else {
          //ascending order based on startTime
          data.zonedSubjects[name].subjects = data.zonedSubjects[name].subjects.sort(function (left, right) {
            if (left.states[0].startTime < right.states[0].startTime) return -1;
            if (left.states[0].startTime > right.states[0].startTime) return 1;
            return 0;
          });
        }

        return data.zonedSubjects[name].subjects.filter(x => {
          return x.subject.toLowerCase().includes(searchText.toLowerCase());
        });
      }
    } else {
      return [];
    }
  });

  let newZoneCard = zoneCardBody.enter().append('div').classed('mdl-grid', true).classed('mdl-grid--no-spacing', true);
  let zoneCardCol = newZoneCard.append('div').classed('mdl-cell', true).classed('mdl-cell--12-col', true);

  let zoneInnerCard = zoneCardCol.append('div')
    .classed('demo-card-square', true).classed('mdl-card', true)
    .classed('mdl-shadow--2dp', true).classed('mcd-card', true);

  let zoneCardState = zoneInnerCard.append('div')
    .classed('mdl-card__supporting-text', true)
    .classed('card-body', true);

  zoneCardState.append('ul').classed('attributes', true);

  //refactor here
  let cardAttributes = zoneCardState.select('ul.attributes').selectAll('li.attribute-item').data(d => {
    //Filter out unwanted properties (all known across all cgrs) from attributes before rendering
    const filteredAttributes = Object.assign({}, d.attributes);
    attributesToRemove.forEach((name) => delete filteredAttributes[name] );

    //Create a array of filtered attributes
    const attributes = Object.keys(filteredAttributes).map(k => k + ': ' + filteredAttributes[k].attr_value);

    //Add Subject and Seen AT at the beginning of this array
    // attributes.unshift(`Last Seen At: ${moment.tz(d.states[0].endTime, timezone).format('hh:mm a')}`);
    attributes.unshift(`${moment.tz(d.states[0].startTime, timezone).format('hh:mm a')}`);
    attributes.unshift(`${d.subject}`);

    return attributes;
  });

  let newCardAttributes = cardAttributes.enter().append('li').classed('attribute-item', true);
  let mergedCardAttributes = newCardAttributes.merge(cardAttributes);
  mergedCardAttributes.text(d => d);
  cardAttributes.remove();

  //hero image
  let svgContainer = zoneCardState.append('div').classed('svg', true);
  let svgImg = svgContainer.append('svg').classed('img-svg', true);
  svgImg.append('image').classed('display-img', true)
    .on('mouseout', function (d) {
      d3.select(this).style('cursor', 'default');
    })
    .on('mouseover', function (d) {
      d3.select(this).style('cursor', 'pointer');
    })
    .on('click', function (d) {
      constructAndRenderImgModal(d);
    });
  zoneCardState.append('div').classed('no-display-img', true);
  let mergedCardState = newZoneCard.merge(zoneCardBody);
  mergedCardState.select('div.truck a.lpr')
    .attr('href', 'javascript:void(0)')
    .text(function (x) {
      return 'Subject: ' + x.subject;
    })
    .on('click', function (d) {
      constructAndRenderImgModal(d);
    });
  mergedCardState.select('.img-svg').attr('viewBox', d => {
    if (!parameters.byClass || !d.bbox) {
      return '0 0 3840 2160';
    } else {
      let bbox = '';
      d.bbox.pop();
      d.bbox.forEach(function (element) {
        bbox = bbox + ' ' + element;
      });
      return bbox;
    }
  });

  mergedCardState.select('.svg').classed('svg-container', d => {
    return d.lastImgKey.length || d.states[0].imgKeys['lpr'].length || d.states[0].imgKeys['asset'].length || d.states[0].imgKeys['other'].length;
  });

  mergedCardState.select('.display-img')
    .attr('xlink:href', function (d) {
      var lpr_img_obj = d.lastImgKey[0] || d.states[0].imgKeys['lpr'][0] || d.states[0].imgKeys['asset'][0] || d.states[0].imgKeys['other'][0];
      if (lpr_img_obj) {
        let imageObject = lpr_img_obj;
        return newImageComponent(parameters.customer, imageObject).toImageURL();
      }
    })
    .attr('preserveAspectRatio', 'xMidYMid meet')
    .attr('height', () => {
      // let mdlCardHeight = $('.mcd-card').height();
      // // return mdlCardHeight*6;
      return 2160;
    })
    .attr('width', () => {
      // let mdlCardWidth = $('.mcd-card').width();
      // return mdlCardWidth*6;
      return 3840;
    });

  mergedCardState.select('div.no-display-img').classed('show-no-display-img', d => {
    return !(d.states[0].imgKeys['lpr'][0] || d.states[0].imgKeys['asset'][0] || d.states[0].imgKeys['other'][0]);
  }).text(d => {
    return !(d.states[0].imgKeys['lpr'][0] || d.states[0].imgKeys['asset'][0] || d.states[0].imgKeys['other'][0]) ? 'No Image Available' : '';
  });

  mergedCardState.select('div.truck .visit-count').text(function (x) {
    return x.visited ? `  (${x.visited})` : '';
  });

  //History cards
  mergedCardState.select('button.history-icon').on('click', function (d) {
    var mousex = d3.event.pageX; //Get X coordinates
    var mousey = d3.event.pageY;
    beaconHistory.transition()
      .duration(200)
      .style('opacity', 1);

    if (screen.width - mousex < 600) {
      beaconHistory.style('left', mousex - 200 + 'px');
    } else {
      beaconHistory.style('left', (d3.event.pageX) + 'px');
    }
    if (screen.height - mousey < 375) {
      beaconHistory.style('top', mousey - 10 + 'px');
    } else {
      beaconHistory.style('top', (d3.event.pageY - 35) + 'px');
    }
    beaconHistory.select('div.history').html('');
    let historyheader = beaconHistory.select('div.history').append('div').classed('mdl-grid', true);
    historyheader.append('div').classed('mdl-cell', true).classed('mdl-cell--5-col', true).text('Zone');
    historyheader.append('div').classed('mdl-cell', true).classed('mdl-cell--5-col', true).text('Seen at');
    let historyDiv = beaconHistory.select('div.history').selectAll('div.history-window div.mdl-grid').data(d.states);
    let newHistoryDiv = historyDiv.enter().append('div').classed('mdl-grid', true);
    newHistoryDiv.append('div').classed('mdl-cell', true).classed('mdl-cell--5-col', true).classed('gwName', true);
    newHistoryDiv.append('div').classed('mdl-cell', true).classed('mdl-cell--5-col', true).classed('seen', true);
    let mergedHistorDiv = newHistoryDiv.merge(historyDiv);
    mergedHistorDiv.select('div.gwName').text(function (y, i) {
      if (i != 0) {
        return upperFirst(y.state);
      }
    }).on('click', y => {
      let listOfImages = [];
      let lpr_img_obj = y.imgKeys['lpr'][0];
      let weight_img_obj = y.imgKeys['weight'][0];
      let other_img_obj = y.imgKeys['other'] ? y.imgKeys['other'][0] : null;
      let bedImg = null;
      let bedImgName = null;
      let bedImgLabel = null;
      let bedImgPosition = null;
      let scaleWeightUnit = configObj[parameters.customer][parameters.facility].hasOwnProperty.call(configObj[parameters.customer][parameters.facility],'scaleWeightUnit') ?
        configObj[parameters.customer][parameters.facility]['scaleWeightUnit'] : 'lbs';
      if (reportCfg[y.state]) {
        bedImgName = reportCfg[y.state].imgConfig.name;
        bedImgLabel = reportCfg[y.state].imgConfig.cs_label;
        bedImgPosition = Number(reportCfg[y.state].imgConfig.position);
        bedImg = bedImgPosition <= (y.imgKeys[bedImgName].length - 1) ? y.imgKeys[bedImgName][bedImgPosition] : y.imgKeys[bedImgName][0];
      }

      if (lpr_img_obj) {
        listOfImages.push({
          linkName: `License Plate`,
          imgKey: lpr_img_obj,
          header: `Previous State: ${upperFirst(y.state)} at ${moment.tz(y.startTime, timezone).format('hh:mm a')}`,
          customerTag: {
            itemId: y.itemId ? y.itemId : null,
            tagArr: y.customerTags ? y.customerTags : [],
            subject: y.subject
          }
        });
      }
      if (other_img_obj) {
        listOfImages.push({
          linkName: `${d.subject}`,
          imgKey: other_img_obj,
          header: `Gate: ${d.subject}`,
          customerTag: {
            itemId: d.itemId ? d.itemId : null,
            tagArr: d.customerTags ? d.customerTags : [],
            subject: d.subject
          }
        });
      }
      if (weight_img_obj) {
        let scale_header = {};
        if (d.warning) {
          scale_header.Warning = 'Potential Error. (' + d.warning + ')';
        }
        scale_header.collection_time = moment.tz(weight_img_obj.collection_time, timezone).format('HH:mm a');
        listOfImages.push({
          linkName: `Scale: ${y.weight} ${scaleWeightUnit}`,
          imgKey: weight_img_obj,
          header: `Time: ${scale_header.collection_time}`,
          customerTag: {
            itemId: d.itemId ? d.itemId : null,
            tagArr: d.customerTags ? d.customerTags : [],
            subject: d.subject
          }
        });
      }
      if (bedImg) {
        let bed_header = {};
        bed_header.collection_time = moment.tz(bedImg.collection_time, timezone).format('HH:mm a');
        listOfImages.push({
          linkName: bedImgLabel || 'Truck Bed Image',
          imgKey: bedImg,
          header: `Time: ${bed_header.collection_time}`,
          customerTag: {
            itemId: d.itemId ? d.itemId : null,
            tagArr: d.customerTags ? d.customerTags : [],
            subject: d.subject
          }
        });
      }
      renderImgModal(listOfImages);
    }).classed('clickable-div', y => {
      let imgs = Object.values(y.imgKeys);
      let hasImgArr = imgs.some(x => {
        return x.length;
      });
      return hasImgArr;
    });
    mergedHistorDiv.select('div.seen').text(function (y, i) {
      if (i != 0) {
        return moment.tz(y.startTime, timezone).format('hh:mm a');
      }
    });
    historyDiv.exit().remove();
  });

  mergedCardState.select('div.mdl-card').classed('custom-color', true).style('--customColor', (d) => {
    const currentDatetime = moment.tz(parameters.time, timezone);
    let diff = currentDatetime.diff(d.states[0].startTime, 'seconds');
    let temp = cardColoring(diff);
    return temp;
  });
  mergeWO.merge(mergedCardState);
  zoneCardBody.exit().remove();
  wo.exit().remove();
}


function constructAndRenderImgModal(d) {
  let currentState = d.states[0];
  let lpr_img_obj = currentState.imgKeys['lpr'];
  let asset_img_obj = d.lastImgKey.length ? [d.lastImgKey[d.lastImgKey.length - 1]] : currentState.imgKeys['asset'] ? currentState.imgKeys['asset'] : null;
  let other_img_obj = currentState.imgKeys['other'] ? currentState.imgKeys['other'] : null;
  let scale_img_obj = currentState.imgKeys['weight'];
  let bedImg = null;
  let bedImgName = null;
  let bedImgLabel = null;
  let bedImgPosition = null;
  let scaleWeightUnit = configObj[parameters.customer][parameters.facility].hasOwnProperty.call(configObj[parameters.customer][parameters.facility],'scaleWeightUnit') ?
    configObj[parameters.customer][parameters.facility]['scaleWeightUnit'] : 'lbs';
  if (reportCfg[currentState.state]) {
    bedImgName = reportCfg[currentState.state].imgConfig.name;
    bedImgLabel = reportCfg[currentState.state].imgConfig.cs_label;
    bedImgPosition = Number(reportCfg[currentState.state].imgConfig.position);

    if (currentState.imgKeys[bedImgName]) {
      bedImg = bedImgPosition <= (currentState.imgKeys[bedImgName].length - 1) ? currentState.imgKeys[bedImgName][bedImgPosition] : currentState.imgKeys[bedImgName][0];
    }
  }

  let listOfImages = [];
  if (lpr_img_obj[0]) {
    listOfImages.push({
      linkName: `LP: ${d.subject}`,
      imgKey: lpr_img_obj[0],
      header: `LP: ${d.subject}`,
      customerTag: {
        itemId: d.itemId ? d.itemId : null,
        tagArr: d.customerTags ? d.customerTags : [],
        subject: d.subject
      }
    }); //Only if LPR keys dont exist then attmept to add asset img keys
  } else if (asset_img_obj && asset_img_obj[0]) {
    listOfImages.push({
      linkName: `${d.subject}`,
      imgKey: asset_img_obj[0],
      header: `LP: ${d.subject}`,
      customerTag: {
        itemId: d.itemId ? d.itemId : null,
        tagArr: d.customerTags ? d.customerTags : [],
        subject: d.subject
      }
    });
  }

  if (scale_img_obj[0]) {
    let scale_header = {};
    if (d.warning) {
      scale_header.Warning = 'Potential Error. (' + d.warning + ')';
    }
    scale_header.collection_time = moment.tz(scale_img_obj.collection_time, timezone).format('HH:mm z');
    listOfImages.push({
      linkName: `Scale: ${currentState.weight} ${scaleWeightUnit}`,
      imgKey: scale_img_obj[0],
      header: `${scale_header.collection_time}`,
      customerTag: {
        itemId: d.itemId ? d.itemId : null,
        tagArr: d.customerTags ? d.customerTags : [],
        subject: d.subject
      }
    });
  }

  if (other_img_obj && other_img_obj[0]) {
    listOfImages.push({
      linkName: `${d.subject}`,
      imgKey: other_img_obj[0],
      header: `Gate: ${d.subject}`,
      customerTag: {
        itemId: d.itemId ? d.itemId : null,
        tagArr: d.customerTags ? d.customerTags : [],
        subject: d.subject
      }
    });
  }

  if (bedImg) {
    let header = {};
    header.collection_time = moment.tz(bedImg.collection_time, timezone).format('HH:mm z');
    listOfImages.push({
      linkName: bedImgLabel || 'Truck Bed Image',
      imgKey: bedImg,
      header: `Time: ${header.collection_time}`,
      customerTag: {
        itemId: d.itemId ? d.itemId : null,
        tagArr: d.customerTags ? d.customerTags : [],
        subject: d.subject
      }
    });
  }
  renderImgModal(listOfImages);
}

//get priority/order and default zones from the config obj
function getPriorityAndDefaultZones(obj) {
  let priority = [], defaultZones = [];
  if(obj['zoneColumns'] && obj['zoneColumns']['zones']) {
    const listOfZones = obj['zoneColumns']['zones'];
    Object.entries(listOfZones).filter(
      ([name, zone]) => zone.enabled
    ).forEach(x => {
      if (x.length && x[1].priority) {
        priority[x[1].priority - 1] = x[0];
      }
      if (x.length && x[1].selected) {
        defaultZones.push(x[0]);
      }
    });
  }
  return {priority, defaultZones};
}

async function reload(parameters) {
  //Fetch config
  await fetchCfg(parameters);

  parameters.reportType = reportType;
  configObj = await fetchYardCfg(parameters);
  let {priority, defaultZones} = getPriorityAndDefaultZones(configObj[parameters.customer][parameters.facility]);
  let deviceStat = [];
  let spanToLookBackForStat = 5;
  deviceStat = await d3.json(`/portal/data/deviceStats?customer=${parameters.customer}&timeSpan=${spanToLookBackForStat}`, {credentials: 'include'});

  const paramsString = Object.keys(parameters).map(function (key) {
    return encodeURIComponent(key) + '=' + encodeURIComponent(parameters[key]);
  }).join('&');
  const url = '/portal/data/cycleLog/' + parameters.customer + '?' + paramsString;
  let { data, allZones, gatewayZoneMap, cycleTimeReport } = await d3.json(url, {credentials: 'include'});

  cycleTimeReport = Array.isArray(cycleTimeReport) ? cycleTimeReport : [];
  const cycleTimeMetrics = cycleTimeReport.reduce((metrics, facilityReport) => {
    const { statistics, iterations: listOfIterations } = facilityReport;
    const iterations = listOfIterations.map(({ subject, states }) => {
      const latestStateObject = states.reduce((_ignored, stateObject) => {
        const { startTime, endTime, state } = stateObject;
        return Object.freeze({ startTime, endTime, state });
      }, {});
      return Object.freeze(Object.assign({}, { subject }, latestStateObject));
    });
    metrics.iterations = metrics.iterations.concat(iterations);
    metrics.total += statistics.total;
    metrics.count += statistics.count;
    return metrics;
  }, { iterations: [], total: Math.pow(30 * 60 * 1000, 2), count: 1});

  /** Remove exited */
  const iterations = cycleTimeMetrics.iterations.map(
    ({ subject, state }) => Object.assign({}, { subject }, state)
  ).filter(
    ({state}) => state && state.toLowerCase().indexOf('exit') === -1
  );
  Object.assign(cycleTimeMetrics, { iterations });

  $('#loading g').children().removeClass();
  $('.loading').remove();
  if (!data.length && !allZones.length) {
    showNoDataIndicator();
    return;
  }
  now = (parameters.time ? moment.tz(moment(parameters.time), timezone) : moment.tz(moment(), timezone));
  let displayTime = parameters.time ? moment(parameters.time).format('hh:mm a, MM/DD/YYYY') : moment.tz(parameters.time, timezone).format('hh:mm a, MM/DD/YYYY');
  d3.select('div.title').text('Current View at ' + displayTime);
  d3.select('div.tz').text(timezone);

  //Sort by start time descending and only keep events happened in last X hours
  const theSelectedSortOrder = !configObj[parameters.customer][parameters.facility]['sortOrder'];
  const fSortingMode = newSortOrder(theSelectedSortOrder);
  data.sort(fSortingMode);

  //Grouping columns together based on the config
  if(configObj[parameters.customer][parameters.facility]['groups'] && configObj[parameters.customer][parameters.facility]['groups'].length) {
    let arrayOfZoneGroups = configObj[parameters.customer][parameters.facility]['groups'];
    arrayOfZoneGroups.forEach(theZoneGroup => {
      theZoneGroup.zones.forEach(theZoneElement => {
        removeElementFromArray(allZones, theZoneElement);
      });
      allZones.unshift(theZoneGroup.groupName);
    });
  }

  let subjects = {};
  let lastStates = {};
  const requiredImageAttributes = [
    { name: 'lpr', default: [] },
    { name: 'weight', default: [] },
    { name: 'asset', default: [] },
    { name: 'other', default: [] }
  ];

  data.forEach(d => {
    if (!d.img_keys) {
      d.img_keys = {};
    }

    requiredImageAttributes.forEach((attribute) => {
      const { name, default: defaultValue } = attribute;
      if (!Object.prototype.hasOwnProperty.call(d.img_keys, name) || !d.img_keys[name]) {
        d.img_keys[name] = defaultValue;
      }
    });

    if (subjects[d.subject_id] && moment(subjects[d.subject_id].lastStartTime).valueOf() >= moment(d.start_time).valueOf()) {
      lastStates[d.subject_id] = [d.state];
      subjects[d.subject_id].states.push({
        startTime: moment(d.start_time).valueOf(),
        endTime: moment(d.end_time).valueOf(),
        state: d.state,
        weight: d.weight,
        isStart: d.is_start,
        isEnd: d.is_end,
        imgKeys: d.img_keys
      });
      subjects[d.subject_id].lastImgKey = subjects[d.subject_id].lastImgKey.concat(d.img_keys.lpr);
    } else {
      if (!lastStates[d.subject_id]) {
        lastStates[d.subject_id] = [];
      }
      lastStates[d.subject_id].push(d.state);
      subjects[d.subject_id] = {
        subject: d.subject_id,
        cycle: d.cycle,
        visited: d.visited,
        attributes: d.attributes,
        customerTags: d.tags || [],
        lastStartTime: d.start_time,
        bbox: d.bbox ? JSON.parse(d.bbox)[0] : null,
        lastStates: lastStates[d.subject_id],
        lastImgKey: d.img_keys.lpr
      };
      subjects[d.subject_id].states = [];
      subjects[d.subject_id].states.push({
        startTime: moment(d.start_time).valueOf(),
        endTime: moment(d.end_time).valueOf(),
        state: d.state,
        weight: d.weight,
        isStart: d.is_start,
        isEnd: d.is_end,
        imgKeys: d.img_keys
      });
    }
  });

  let zoned = {};
  allZones.forEach(zone => {
    zoned[zone] = {};
    zoned[zone]['subjects'] = [];
    zoned[zone]['sortOrder'] = theSelectedSortOrder;
  });

  let zoneGroups = (configObj[parameters.customer][parameters.facility]['groups']) || [];

  const visited = {};
  const countOfViews = {};
  Object.values(subjects).forEach(theSubjectObject => {
    const { lastStates: arrayOfLatestState } = theSubjectObject;

    arrayOfLatestState.forEach(lastState => {
      let filteredGroup = zoneGroups.filter(x=>x.zones.some((zone) => zone === lastState));
      if(filteredGroup.length) {
        const groupName = filteredGroup[0].groupName;
        const { subject } = theSubjectObject;

        countOfViews[subject] = 1 + (countOfViews[subject] || 0);
        if (visited[subject]) {
          visited[subject].call({});
        }

        { /* prepare the remove object */
          const position = zoned[groupName]['subjects'].length;
          visited[subject] = () => {
            zoned[groupName]['subjects'].splice(position, 1);
          };
        }

        zoned[groupName]['sortOrder'] = theSelectedSortOrder;
        zoned[groupName]['subjects'].push(theSubjectObject);
      } else {
        zoned[lastState]['sortOrder'] = theSelectedSortOrder;
        zoned[lastState]['subjects'].push(theSubjectObject);
      }
    });
  });

  //setting the priority in allZones for rendering
  if(priority.length) {
    allZones = priority.filter((zone) => !!zone);
  }
  let filterObj = filterByTime(zoned);

  const currentViewModelObject = {
    zonedSubjects: filterObj.zoned,
    subjectCount: filterObj.subjectCount,
    allZones: allZones,
    missingDevice: deviceStat,
    gatewayZoneMap: gatewayZoneMap,
    defaultZones: defaultZones,
    cycleTimeMetrics: cycleTimeMetrics
  };

  globalData = JSON.parse(JSON.stringify(currentViewModelObject ));

  return currentViewModelObject;
}

function removeElementFromArray (arr, el) {
  let index = arr.indexOf(el);
  if (index > -1) {
    arr.splice(index, 1);
  }
}

async function reloadBeaconZone(parameters) {
  //Fetch config
  await fetchCfg(parameters);
  parameters.reportType = reportType;
  configObj = await fetchYardCfg(parameters);
  let {priority, defaultZones} = getPriorityAndDefaultZones(configObj[parameters.customer][parameters.facility]);
  let deviceStat = [];
  let spanToLookBackForStat = 5;
  deviceStat = await d3.json(`/portal/data/deviceStats?customer=${parameters.customer}&timeSpan=${spanToLookBackForStat}`, {credentials: 'include'});
  const paramsString = Object.keys(parameters).map(function (key) {
    return encodeURIComponent(key) + '=' + encodeURIComponent(parameters[key]);
  }).join('&');
  const url = '/portal/data/beaconZoneTracking/?' + paramsString;
  let { data, allZones, gatewayZoneMap, cycleTimeReport } = await d3.json(url, {credentials: 'include'});

  cycleTimeReport = Array.isArray(cycleTimeReport) ? cycleTimeReport : [];
  const cycleTimeMetrics = cycleTimeReport.reduce((metrics, facilityReport) => {
    const { statistics, iterations: listOfIterations } = facilityReport;
    const iterations = listOfIterations.map(({ subject, states }) => {
      const latestStateObject = states.reduce((_ignored, stateObject) => {
        const { startTime, endTime, state } = stateObject;
        return Object.freeze({ startTime, endTime, state });
      }, {});
      return Object.freeze(Object.assign({}, { subject }, latestStateObject));
    });
    metrics.iterations = metrics.iterations.concat(iterations);
    metrics.total += statistics.total;
    metrics.count += statistics.count;
    return metrics;
  }, { iterations: [], total: Math.pow(30 * 60 * 1000, 2), count: 1});

  /** Remove exited */
  const iterations = cycleTimeMetrics.iterations.map(
    ({ subject, state }) => Object.assign({}, { subject }, state)
  ).filter(
    ({state}) => state && state.toLowerCase().indexOf('exit') === -1
  );
  Object.assign(cycleTimeMetrics, { iterations });

  $('#loading g').children().removeClass();
  $('.loading').remove();
  if (!data.length && !allZones.length) {
    showNoDataIndicator();
    return;
  }
  let displayTime = parameters.time ? moment(parameters.time).format('hh:mm a, MM/DD/YYYY') : moment.tz(parameters.time, timezone).format('hh:mm a, MM/DD/YYYY');
  d3.select('div.title').text('Beacon Report at ' + displayTime);
  d3.select('div.tz').text(timezone);
  //Sort by start time descending and only keep events happened in last X hours
  const theSelectedSortOrder = !configObj[parameters.customer][parameters.facility]['sortOrder'];
  const fSortingMode = newSortOrder(theSelectedSortOrder);
  data.sort(fSortingMode);

  //Grouping columns together based on the config
  if(configObj[parameters.customer][parameters.facility]['groups'] && configObj[parameters.customer][parameters.facility]['groups'].length) {
    let zoneGroups = configObj[parameters.customer][parameters.facility]['groups'];
    zoneGroups.forEach(x=> {
      x.zones.forEach(y=>{
        removeElementFromArray(allZones, y);
      });
      allZones.unshift(x.groupName);
    });
  }
  let subjects = {};
  let lastStates = {};
  data.forEach(d => {
    if (subjects[d.beacon_name]) {
      lastStates[d.beacon_name] = [];
      subjects[d.beacon_name].states.push({
        startTime: moment(d.slot).valueOf(),
        state: d.gateway_name
      });
    } else {
      if (!lastStates[d.beacon_name]) {
        lastStates[d.beacon_name] = [];
      }
      lastStates[d.beacon_name].push(d.gateway_name);
      subjects[d.beacon_name] = {
        subject: d.beacon_name,
        lastStartTime: d.slot,
        lastStates: lastStates[d.beacon_name]
      };
      subjects[d.beacon_name].states = [];
      subjects[d.beacon_name].states.push({
        startTime: moment(d.slot).valueOf(),
        state: d.gateway_name
      });
    }
  });

  let zoned = {};
  allZones.forEach(z => {
    zoned[z] = {};
    zoned[z]['subjects'] = [];
    zoned[z]['sortOrder'] = theSelectedSortOrder;
  });
  Object.values(subjects).forEach(subject => {
    subject.lastStates.forEach((state) => {
      zoned[state]['sortOrder'] = theSelectedSortOrder;
      zoned[state]['subjects'].push(state);
    });
  });
  if(priority.length) {
    allZones = priority.filter((zone) => !!zone);
  }
  let filterObj = filterByTime(zoned);
  filterObj = filterByCycleTime(filterObj, cycleTimeMetrics);

  const currentViewModelObject = {
    zonedSubjects: filterObj.zoned,
    subjectCount: filterObj.subjectCount,
    allZones: allZones,
    missingDevice: deviceStat,
    gatewayZoneMap: gatewayZoneMap,
    defaultZones: defaultZones,
    cycleTimeMetrics: cycleTimeMetrics
  };

  globalData = JSON.parse(JSON.stringify(currentViewModelObject));

  return currentViewModelObject;
}

function filterByTime(zoned) {
  let filterObj = {};
  let subjectCount = 0;
  let zoneObject = Object.keys(zoned);
  const hoursToLookBack = getHoursToLookBackByCgr(configObj, parameters);
  const minimumAcceptedTime = (parameters.time ? moment.tz(parameters.time, timezone) : moment().tz(timezone));
  zoneObject.forEach((areaOfInterest) => {
    let filteredSubjects = zoned[areaOfInterest].subjects.filter((subjectObject) => {
      return moment(subjectObject.states[0].startTime).add(hoursToLookBack, 'hours').toDate() >= minimumAcceptedTime;
    });
    zoned[areaOfInterest].subjects = [];
    zoned[areaOfInterest].subjects = filteredSubjects;
    subjectCount = subjectCount + zoned[areaOfInterest].subjects.length;
  });
  filterObj.zoned = zoned;
  filterObj.subjectCount = subjectCount;
  return filterObj;
}

function filterByCycleTime(zoned, metrics) {
  let filterObj = {};
  let subjectCount = 0;
  let zoneObjects = Object.keys(zoned);
  const acceptedSubjects = metrics.iterations.reduce((mapOfSubjects, { subject }) => {
    acceptedSubjects[subject] = 1;
    return mapOfSubjects;
  });
  zoneObjects.forEach((areaOfInterest) => {
    const filteredSubjects = zoned[d].subjects.filter(
      ({ subject }) => Object.prototype.hasOwnProperty.call(acceptedSubjects, subject)
    );
    zoned[areaOfInterest].subjects = [];
    zoned[areaOfInterest].subjects = filteredSubjects;
    subjectCount = subjectCount + zoned[areaOfInterest].subjects.length;
  });
  filterObj.zoned = zoned;
  filterObj.subjectCount = subjectCount;
  return filterObj;
}

function renderZonePicker(data, options) {
  const zonPickerElement = $('#zone-picker');
  const { showZonePicker, selectedZones } = Object.assign({ showZonePicker: false, selectedZones: [] }, options);

  zonPickerElement.html('');

  if (showZonePicker) {
    let zoneButtons = '';
    let zones = data.allZones;

    for (let areaOfInterest in zones) {
      let label = (configObj[parameters.customer][parameters.facility]['zoneColumns']
        && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones']
        && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones'][zones[areaOfInterest]]
        && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones'][zones[areaOfInterest]]['displayName'])|| zones[areaOfInterest];
      zoneButtons = zoneButtons + `<button class="zone-filter btn inactive-link ${zoneFilter.includes(label) ? 'active-link' : ''}" onclick="updateZonePicker(this)">${upperFirst(label)}</button>`;
    }
    zonPickerElement.append(zoneButtons);
  } else {
    zonPickerElement.css('display', 'none');
    zonPickerElement.css('visibility', 'hidden');
  }

  applyZoneFilter(selectedZones);
}

function hideNoDataIndicator() {
  d3.select('#message').text('');
}

function showNoDataIndicator() {
  d3.select('#message').text('No Data To Display');
}

function applyZoneFilter(add, remove) {
  zoneFilter = [...new Set([...zoneFilter, ...add])];
  zoneFilter = zoneFilter.filter(z => z !== remove);
  let filterData = JSON.parse(JSON.stringify(globalData));
  filterData = zoneFilter.length ? filterZone(filterData) : filterData; //filter by zone
  filterData = filterOnTags.length ? filterByTags(filterData) : filterData; //filter by tags
  render(filterData);
}

function updateZonePicker(zoneObj) {
  const zoneName = $(zoneObj).text();
  const configZoneObj = configObj[parameters.customer][parameters.facility]['zoneColumns']
    && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones'];
  let zoneInternalName = configZoneObj ? Object.entries(configZoneObj).filter(x=> x[1].displayName && x[1].displayName === zoneName) : [];
  if (d3.select(zoneObj).classed('active-link')) {
    d3.select(zoneObj).classed('active-link', false);
    applyZoneFilter([], zoneInternalName.length ? zoneInternalName[0][0] : zoneName);
  } else {
    d3.select(zoneObj).classed('active-link', true);
    applyZoneFilter([zoneInternalName.length ? zoneInternalName[0][0] : zoneName], '');
  }
}

function filterZone(filterData) {

  filterData.allZones = filterData.allZones.filter(d => {
    return zoneFilter.some(x => x.toLowerCase() == d.toLowerCase());
  });
  filterData.subjectCount = 0;
  filterData.allZones.forEach(zoneName => {
    let groups = configObj[parameters.customer][parameters.facility]['groups'];
    let filteredGroup = groups.filter((groupObject) => groupObject.groupName === zoneName);
    const groupOfInterest = filteredGroup && filteredGroup.length ? filteredGroup[0].groupName : zoneName;
    const zonedSubjects = Object.assign({ subjects: [] }, filterData.zonedSubjects[groupOfInterest]);
    filterData.subjectCount = filterData.subjectCount + zonedSubjects.subjects.length;
  });
  return filterData;
}

async function handleIntervalFired(parameters) {
  now = (parameters.time ? moment(parameters.time) : moment());
  // parameters.time = now.format('YYYY-MM-DD HH:mm:ss');
  let displayTime = parameters.time
    ? moment(parameters.time).format('hh:mm a, MM/DD/YYYY')
    : moment.tz(parameters.time, timezone).format('hh:mm a, MM/DD/YYYY');
  d3.select('div.title').text('Current View at ' + displayTime);

  d3.select('div.tz').text(timezone);
  d3.select('div.facility').text('Facility: ' + parameters.facility);
  let data = await reload(parameters);

  //filter by zone
  let filterData = zoneFilter.length ? filterZone(data) : data;
  //filter by tags
  filterData = filterOnTags.length ? filterByTags(filterData) : filterData;
  render(filterData);
}

async function handleBindingTableIntervalFired(parameters) {
  await reloadBindingTable(parameters);
}

function searchWO(text) {
  searchText = text;
  let filterData = JSON.parse(JSON.stringify(globalData));
  //filter by zone
  filterData = zoneFilter.length ? filterZone(filterData) : filterData;
  //filter by tags
  filterData = filterOnTags.length ? filterByTags(filterData) : filterData;
  render(filterData);

}

function searchBeacons(text) {
  searchText = text;
  if (zoneFilter.length) {
    let data = JSON.parse(JSON.stringify(globalData));
    let filteredData = filterZone(data);
    renderBeaconZone(filteredData);
  } else {
    renderBeaconZone(globalData);
  }
}

function sortZone(data, zone) {
  data.zonedSubjects[zone].sortOrder = !data.zonedSubjects[zone].sortOrder;
  render(data);
}

function loadHistory() {
  beaconHistory.append('a').attr('href', 'javascript:void(0)').attr('id', 'close-wo-history').append('i').classed('material-icons', true).text('close');
  beaconHistory.select('#close-wo-history').on('click', function () {
    beaconHistory.select('div.history .history-header').remove();
    beaconHistory.select('div.history .history-body').remove();
    beaconHistory.style('opacity', 0);
  });
  beaconHistory.append('div').classed('history', true);
}

async function changeTheReportView() {
  zoneFilter = [];
  parameters.byClass = d3.select('#viewSwitch').property('checked');
  let data = await reload(parameters);
  renderZonePicker(data);
  render(data);
}

function renderSwitch(parameters) {

  if (parameters.customer === 'gatwickairport' || parameters.customer === 'tia') {
    let switchDiv = d3.select('div.switch');
    let switchLabel = switchDiv.append('label').classed('mdl-switch', true).classed('mdl-js-switch', true).classed('mdl-js-ripple-effect', true).attr('for', 'viewSwitch');
    switchLabel.append('span').classed('asset-span', true).text('By Asset');
    switchLabel.append('input').attr('type', 'checkbox').attr('checked', parameters.byClass).attr('id', 'viewSwitch').classed('mdl-switch__input', true).on('click', () => changeTheReportView());
    switchLabel.append('span').classed('mdl-switch__label', true);
    componentHandler.upgradeDom();
  }
}

async function reloadBindingTable(parameters) {
  let status = await d3.json('/portal/data/getLatestWOBinding/' + parameters.customer, {credentials: 'include'});
  renderBindingTable(status);
}

function renderBindingTable(data) {

  let bindingTable = d3.select('#bindingTable').selectAll('tr.bindingStatusRow').data(data);
  let newTableData = bindingTable.enter().append('tr').classed('bindingStatusRow', true);
  newTableData.append('td').classed('woId', true);
  newTableData.append('td').classed('woStatus', true);
  let mergedData = newTableData.merge(bindingTable);
  mergedData.select('td.woId').text(d => d.wo_id);
  mergedData.select('td.woStatus').text(d => {
    //let duration = moment.duration(moment().diff(moment(d.collection_time), 'seconds')).humanize();
    return d.status + ' ' + ' (' + moment(d.latest_seen_time).format('YYYY-MM-DD hh:mm a z') + ') ';
  }).classed('success', d => d.status == 'SUCCESS').classed('fail', d => d.status == 'FAIL');
  bindingTable.exit().remove();
}

async function fetchYardCfg({ customer, facility }) {
  let cfg = await d3.json('/portal/config/getYardConfig?customer=' + customer + '&facility=' + facility, {credentials: 'include'});
  return cfg;
}

async function fetchCfg({ customer, facility }) {
  let cfg = await d3.json('/portal/config/getConfig/' + customer + '?' + 'facility=' + facility, {credentials: 'include'});
  cfg.forEach(row => {
    let state = row.extract_type;
    if (!Object.keys(reportCfg).some(s => s == state)) {
      reportCfg[state] = {};
    }

    //Put values
    reportCfg[state]['imgConfig'] = row.cs_config ? row.cs_config.imageConfig : null;
  });
}

async function setDateTime(val) {
  parameters.time = val;
  let paramString = Object.keys(parameters).map(p => p + '=' + parameters[p]).join('&');
  var newurl = window.location.protocol + '//' + window.location.host + window.location.pathname + '?' + paramString;
  window.history.pushState({path: newurl}, '', newurl);
  let data = await reload(parameters);
  renderZonePicker(data);
  render(data);

  loadHistory();
}

function showNoShiftConfiguredMsg() {
  d3.select('#message').text('There is no shift configured for this report');
}

function renderBeaconZone(originalData) {
  let colWidthPercent = 100 / originalData.allZones.length;
  colWidthPercent = colWidthPercent > 50 ? 50 : colWidthPercent;
  let gatewayZoneMap = originalData.gatewayZoneMap;
  let missingDevice = originalData.missingDevice;

  const data = augmentDataWithTransitions(originalData);

  d3.select('div#workOrder').html('');

  const wo = d3.select('div#workOrder').selectAll('div.zone-col').data(data.allZones);

  const newWo = wo.enter().append('div')
    .classed('zone-col', true)
    .classed('mdl-cell', true)
    .classed('custom-mdl-cell--col', true)
    .classed('scrollable-zone', true)
    .style('--percent-width', colWidthPercent + '%');

  const cardSquare = newWo.append('div')
    .classed('demo-card-square', true)
    .classed('mdl-card', true)
    .classed('mdl-shadow--2dp', true)
    .classed('zone-card', true);

  const cardTitle = cardSquare.append('div')
    .classed('mdl-card__title', true)
    .classed('mdl-card--expand', true);

  cardSquare.append('div')
    .classed('mdl-card__supporting-text', true)
    .classed('scrollable-column'. true)
    .classed('zone-grids', true);

  let deviceStatIcon = cardTitle.append('div').classed('device-stat', true);
  deviceStatIcon.append('i').classed('material-icons', true).text('brightness_1');
  cardTitle.append('h2').classed('mdl-card__title-text', true);
  cardTitle.append('div').classed('sort-icon', true);
  cardTitle.append('h5').classed('card-count', true);

  const mergeWO = newWo.merge(wo);
  mergeWO.select('div.zone-col div.mdl-card__title')
    .attr('class', function (d, index) {
      return 'zone' + index % 4;
    }).style('background-color',d=> {
      if(configObj[parameters.customer][parameters.facility]['zoneColumns']
        && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones']
        && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones'][d]
        && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones'][d]['color']) {
        return configObj[parameters.customer][parameters.facility]['zoneColumns']['zones'][d]['color'];
      }
    });
  mergeWO.select('div.zone-col h2.mdl-card__title-text').text(function (d) {
    let label = (configObj[parameters.customer][parameters.facility]['zoneColumns']
      && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones']
      && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones'][d]
      && configObj[parameters.customer][parameters.facility]['zoneColumns']['zones'][d]['displayName'])|| d;
    return upperFirst(label);
  });
  mergeWO.select('div.device-stat')
    .classed('all-devices-good', d => {
      let groupName = (configObj[parameters.customer][parameters.facility]['groups'] && configObj[parameters.customer][parameters.facility]['groups'].filter(x=> x.groupName === d)) || [];
      let gwIds = groupName.length ? groupName[0].zones.map(x=> gatewayZoneMap[x]) : gatewayZoneMap[d];
      let stat = missingDevice.filter(y => gwIds.some(x => x === y.gateway_id && d === y.zone_name));
      return stat.length === 0;
    }).classed('few-devices-offline', d => {
      let groupName = (configObj[parameters.customer][parameters.facility]['groups'] && configObj[parameters.customer][parameters.facility]['groups'].filter(x=> x.groupName === d)) || [];
      let gwIds = groupName.length ? groupName[0].zones.map(x=> gatewayZoneMap[x]) : gatewayZoneMap[d];
      let stat = missingDevice.filter(y => gwIds.some(x => x === y.gateway_id && d === y.zone_name));
      return stat.length > 0 && stat.length < gwIds.length;
    }).classed('all-devices-offline', d => {
      let groupName = (configObj[parameters.customer][parameters.facility]['groups'] && configObj[parameters.customer][parameters.facility]['groups'].filter(x=> x.groupName === d)) || [];
      let gwIds = groupName.length ? groupName[0].zones.map(x=> gatewayZoneMap[x]) : gatewayZoneMap[d];
      let stat = missingDevice.filter(y => gwIds.some(x => x === y.gateway_id && d === y.zone_name));
      return stat.length === gwIds.length;
    }).on('mouseover', function (d) {
      tooltip.transition()
        .duration(200)
        .style('opacity', .9);
      let groupName = (configObj[parameters.customer][parameters.facility]['groups'] && configObj[parameters.customer][parameters.facility]['groups'].filter(x=> x.groupName === d)) || [];
      let gwIds = groupName.length ? groupName[0].zones.map(x=> gatewayZoneMap[x]) : gatewayZoneMap[d];
      let stat = missingDevice.filter(y => {
        return gwIds.some(x => x === y.gateway_id && d === y.zone_name);
      });
      let message = getOnlineOfflineMessage(stat, gwIds);

      tooltip.html(message)
        .style('left', (d3.event.pageX - 50) + 'px')
        .style('top', (d3.event.pageY - 28) + 'px');
    }).on('mouseout', function (d) {
      tooltip.transition()
        .duration(500)
        .style('opacity', 0);
    });
  mergeWO.select('div.zone-col div.sort-icon').html('')
    .append('i').attr('class', function (d) {
      if (data.zonedSubjects[d]) {
        if (data.zonedSubjects[d].sortOrder) {
          return 'fa-sort-alpha-up';
        }
        return 'fa-sort-alpha-down';
      }
    }).on('click', function (d) {
      sortZoneForBeacon(data, d);
    }).classed('fas', true);

  d3.select('#woCount').text(`Beacons: ${data.subjectCount}`);
  d3.select('#timeTravel').text('Time Travel enabled');
  mergeWO.select('div.zone-col h5.card-count').text(function (d) {
    if (data.zonedSubjects[d]) {
      return ' (' + data.zonedSubjects[d].subjects.length + ')';
    }
  });
  let zoneCardBody = mergeWO.select('div.zone-grids').selectAll('div.mdl-grid').data(function (d) {
    if (data.zonedSubjects[d]) {
      if (data.zonedSubjects[d].subjects !== undefined) {
        const theSortSubjectOrder = newSortSubject(data.zonedSubjects[d].sortOrder);
        data.zonedSubjects[d].subjects = data.zonedSubjects[d].subjects.sort(theSortSubjectOrder);

        return data.zonedSubjects[d].subjects.filter(x => {
          return x.subject.toLowerCase().includes(searchText.toLowerCase());
        });
      }
    } else {
      return [];
    }
  });
  let newZoneCard = zoneCardBody.enter().append('div').classed('mdl-grid', true).classed('mdl-grid--no-spacing', true);
  let zoneCardCol = newZoneCard.append('div').classed('mdl-cell', true).classed('mdl-cell--12-col', true);
  let zoneInnerCard = zoneCardCol.append('div').classed('demo-card-square', true).classed('mdl-card', true)
    .classed('mdl-shadow--2dp', true).classed('mcd-card', true);
  let zoneCardState = zoneInnerCard.append('div').classed('mdl-card__supporting-text', true).classed('card-body', true);
  let zoneCardTextDiv = zoneCardState.append('div').classed('beaconName', true);
  zoneCardState.append('button').classed('history-icon', true).classed('mdl-button', true).classed('mdl-js-button', true).classed('mdl-button--icon', true)
    .append('i').classed('material-icons', true).text('history');
  zoneCardTextDiv.append('a').classed('beacon', true);
  zoneCardState.append('div').classed('startTime', true);
  let mergedCardState = newZoneCard.merge(zoneCardBody);
  mergedCardState.select('div.beaconName a.beacon')
    .attr('href', 'javascript:void(0)')
    .text(function (x) {
      return 'Subject: ' + x.subject;
    });
  mergedCardState.select('button.history-icon').on('click', async function (d) {
    var mousex = d3.event.pageX; //Get X coordinates
    var mousey = d3.event.pageY;
    beaconHistory.transition()
      .duration(200)
      .style('opacity', 1);

    if (screen.width - mousex < 600) {
      beaconHistory.style('left', mousex - 200 + 'px');
    } else {
      beaconHistory.style('left', (d3.event.pageX) + 'px');
    }
    if (screen.height - mousey < 375) {
      beaconHistory.style('top', mousey - 10 + 'px');
    } else {
      beaconHistory.style('top', (d3.event.pageY - 35) + 'px');
    }
    beaconHistory.select('div.history').html('');
    let historyHeader = beaconHistory.select('div.history').append('div').classed('history-header', true);
    let headerRow = historyHeader.append('div').classed('mdl-grid', true);
    headerRow.append('div').classed('mdl-cell', true).classed('mdl-cell--5-col', true).text('Zone');
    headerRow.append('div').classed('mdl-cell', true).classed('mdl-cell--5-col', true).text('Seen at');
    historyHeader.append('hr');

    $('div.history').append(`<div class="box"><div class="loader-20"></div></div>`);
    let historyParams = Object.assign({}, state, parameters, {beaconName: d.subject});
    await getHistoryData(historyParams);

  });
  mergedCardState.select('div.startTime').text(function (x) {
    if (!x.states[0].isEnd && !x.states[0].isStart) {
      return 'Seen at : ' + moment.tz(x.states[0].startTime, timezone).format('YYYY-MM-DD hh:mm a');
    }
  });
  mergedCardState.select('div.mdl-card').classed('custom-color', true).style('--customColor', (d) => {
    let currentDatetime = moment.tz(parameters.time, timezone);
    let diff = currentDatetime.diff(d.states[0].startTime, 'seconds');
    let temp = cardColoring(diff);
    return temp;
  });
  mergeWO.merge(mergedCardState);
  zoneCardBody.exit().remove();
  wo.exit().remove();
}

async function handleIntervalFiredForBeaconZone(parameters) {
  let displayTime = parameters.time
    ? moment(parameters.time).format('hh:mm a, MM/DD/YYYY')
    : moment.tz(parameters.time, timezone).format('hh:mm a, MM/DD/YYYY');

  d3.select('div.title').text('Beacon Report at ' + displayTime);
  d3.select('div.tz').text(timezone);
  d3.select('div.facility').text('Facility: ' + parameters.facility);

  let data = await reloadBeaconZone(parameters);
  if (zoneFilter.length) {
    data = filterZone(data);
  }
  renderBeaconZone(data);
}

function updateZonePickerForBeacons(zoneObj) {
  if (d3.select(zoneObj).classed('active-link')) {
    d3.select(zoneObj).classed('active-link', false);
  } else {
    d3.select(zoneObj).classed('active-link', true);
  }

  let zoneName = $(zoneObj).text();
  if (zoneFilter.length) {
    let index = zoneFilter.findIndex(x => {
      return x.toLowerCase() == zoneName.toLowerCase();
    });
    if (index == -1) {
      zoneFilter.push(zoneName);
    } else {
      zoneFilter.splice(index, 1);
    }
  } else {
    zoneFilter.push(zoneName);
  }
  if (zoneFilter.length) {
    let data = JSON.parse(JSON.stringify(globalData));
    let filterData = filterZone(data);
    renderBeaconZone(filterData);
  } else {
    renderBeaconZone(JSON.parse(JSON.stringify(globalData)));
  }
}

function sortZoneForBeacon(data, zone) {
  data.zonedSubjects[zone].sortOrder = !data.zonedSubjects[zone].sortOrder;
  renderBeaconZone(data);
}

function renderHistory(history) {
  d3.select('div.history div.box').remove();
  //removing the 1st element since its the current
  history.shift();
  let historyBody = beaconHistory.select('div.history').append('div').classed('history-body', true);
  if (!history.length) {
    historyBody.text('No history to show');
    return;
  }
  let historyDiv = historyBody.selectAll('div.historyBody div.history-data').data(history);
  let newHistoryDiv = historyDiv.enter().append('div').classed('history-data', true).classed('mdl-grid', true);
  newHistoryDiv.append('div').classed('mdl-cell', true).classed('mdl-cell--5-col', true).classed('gwName', true);
  newHistoryDiv.append('div').classed('mdl-cell', true).classed('mdl-cell--5-col', true).classed('seen', true);
  let mergedHistorDiv = newHistoryDiv.merge(historyDiv);
  mergedHistorDiv.select('div.gwName').text(x => x.gateway_name);
  mergedHistorDiv.select('div.seen').text(x => moment.tz(x.start_time, timezone).format('hh:mm a'));
}


async function getHistoryData(parameters) {
  let paramString = Object.keys(parameters).map(p => p + '=' + parameters[p]).join('&');
  let history = await d3.json('/portal/data/beaconHistory?' + paramString);
  renderHistory(history);
  // return history;
}

function filterByTags(filterData) {
  let zonedSubjects = filterData.zonedSubjects;
  for (let zones in zonedSubjects) {
    zonedSubjects[zones].subjects = zonedSubjects[zones].subjects.filter(d => {
      return d.customerTags.some(function (tag) {
        return filterOnTags.includes(tag);
      });
    });
  }
  filterData.zonedSubjects = zonedSubjects;
  return filterData;
}
