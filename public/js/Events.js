'use strict';

(function makeEventEmitter (global) {
  const EventEmitter = function() {
    Object.defineProperty(this, 'topics', { value: {}, enumerable: false, writable: true });
  };

  EventEmitter.prototype.on = function (topic, listener) {
    if ('string' !== typeof topic) {
      throw new TypeError('topic parameter must be of type string');
    }

    if ('function' !== typeof listener) {
      throw new TypeError('listener parameter must be of type function');
    }

    let subscribers;
    const { topics } = this;
    if (Object.prototype.hasOwnProperty.call(topics, topic)) {
      subscribers = topics[topic];
    } else {
      Object.defineProperty(topics, topic, { value: {}, enumerable: true, writable: true });
      subscribers = (topics[topic] = {});
    }

    const uuid = 'xxxxxxxx-yxxx-xxxx-xxxx-xxxxxxxxxxxx'.replace(/[xy]/, (character) => {
      const random = Math.random() * 16 | 0;
      const part = character === 'x' ? random : ((random & 0x03) | 0x08);
      return part.toString(16).toLowerCase();
    });

    Object.defineProperty(subscribers, 'uuid', { value: listener, enumerable: true, writable: true });

    return Object.freeze({
      remove: () => {
        delete subscribers[uuid];
      }
    });
  };

  EventEmitter.prototype.off = function (topic, listener) {
    if ('string' !== typeof topic) {
      throw new TypeError('topic parameter must be of type string');
    }

    if ('function' !== typeof listener) {
      throw new TypeError('listener parameter must be of type function');
    }

    let subscribers;
    const { topics } = this;
    if (Object.prototype.hasOwnProperty.call(topics, topic)) {
      subscribers = topics[topic];

      Object.keys(subscribers).forEach((uuid) => {
        if (listener === subscribers[uuid]) {
          delete subscribers[uuid];
        }
      });
    }

    return null !== subscribers;
  };

  EventEmitter.prototype.emit = function (topic) {
    if ('string' !== typeof topic) {
      throw new TypeError('topic parameter must be of type string');
    }

    const parameters = [];
    for (let index = 1; index < arguments.length; index++) {
      parameters.push(arguments[index]);
    }

    let subscribers;
    const { topics } = this;
    if (Object.prototype.hasOwnProperty.call(topics, topic)) {
      subscribers = topics[topic];

      Object.keys(subscribers).forEach((uuid) => {
        const target = subscribers[uuid];
        Function.prototype.apply.call(target, this, parameters);
      });
    }

    return null !== subscribers;
  };

  global.EventEmitter = Object.freeze(EventEmitter);
}(this));
