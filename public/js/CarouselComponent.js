'use strict';

/*globals parameters, renderFooter, setupFooter */

(function(global, makeCarouselComponent) {
  const LAZY_THRESHOLD = 3;
  const { EventEmitter, AtollogyImage } = global;
  const findElementBySelector = (cssSelector) => document.querySelector(cssSelector);

  const loadMediaIfNeeed = (element) => {
    const target = element.querySelector('[data-src]');

    if (target) {
      /** makes sure the element is hidden */
      target.classList.remove('hidden');
      target.classList.add('hidden');
      try {
        target.setAttribute('src', target.getAttribute('data-src'));
        target.removeAttribute('data-src');
      } finally {
        target.classList.remove('hidden');
      }
    }
  };

  const loadInitialMedias = (selector) => {
    const element = findElementBySelector(`[id="${selector}"]`);
    const children = element.querySelectorAll('[class="atl-media-element"]');
    const length = children.length;
    for (let index = 0; index < Math.min(LAZY_THRESHOLD, length); index += 1) {
      if (index === 0) {
        $('.carousel-control-zoom').attr('href', children[0].dataset.mediaSource);
      }
      const target = children[index];
      loadMediaIfNeeed(target);
    }

    for (let index = length - 1; index >= Math.max(0, length - LAZY_THRESHOLD); index -= 1) {
      const target = children[index];
      loadMediaIfNeeed(target);
    }
  };

  const slideCarouselTo = (selector, index) => {
    $(`#${selector}`).carousel(index);
  };

  const createCarousel = (selector, onTransitionStarted, onTransitionComplete) => {
    const theCarouselElement = $(`#${selector}`);

    theCarouselElement.carousel();

    if (onTransitionStarted && 'function' === typeof onTransitionStarted) {
      theCarouselElement.on('slide.bs.carousel', function ({ direction, relatedTarget: element, from, to }) {
        loadMediaIfNeeed(element);
        const eventInfo = Object.freeze({ direction, element, from, to });
        Function.prototype.apply.call(onTransitionStarted, {}, [eventInfo]);
      });
    }

    if (onTransitionComplete && 'function' === typeof onTransitionComplete) {
      theCarouselElement.on('slid.bs.carousel', function ({ direction, relatedTarget: element, from, to}) {
        loadMediaIfNeeed(element);
        const eventInfo = Object.freeze({ direction, element, from, to });
        Function.prototype.apply.call(onTransitionComplete, {}, [eventInfo]);
      });
    }

    loadInitialMedias(selector);
    theCarouselElement.carousel(0);
  };

  const destroyCarousel = (selector) => {
    $(`#${selector}`).carousel('dispose');
  };

  const openMediaInTarget = (theTargetUrl, theTargetWindow) => {
    window.open(theTargetUrl, theTargetWindow);
  };

  global.CarouselComponent = makeCarouselComponent(EventEmitter, AtollogyImage, {
    findElementBySelector,
    createCarousel,
    slideCarouselTo,
    destroyCarousel,
    openMediaInTarget
  });
}(this, function makeCarouselComponent(EventEmitter, AtollogyImage, {
  findElementBySelector,
  createCarousel,
  slideCarouselTo,
  destroyCarousel,
  openMediaInTarget
}) {
  /**
   * The minimum number of indicators elements required to show the indicators
   */
  const MIN_INDICATORS = 2;
  const DEFAULT_WINDOW = '_blank';

  const augmentEventInfo = (eventInfo) => {
    const { element } = Object.assign({ element: {} }, eventInfo);
    const measureInfo = element.getAttribute('data-measure-name');
    const viewportMedia = element.getAttribute('data-timeline-media');
    const timelineInfo = element.getAttribute('data-timeline-timestamp');
    const imageSplit = viewportMedia.split('/');
    let keys = {};
    keys.name = imageSplit[imageSplit.length - 1];
    keys.camera_id = imageSplit[5];
    keys.gateway_id = imageSplit[4];
    keys.collection_time = imageSplit[6];
    keys.step_name = imageSplit[7];
    keys.measure = measureInfo;
    setupFooter(keys, null);
    renderFooter(keys, parameters);
    return Object.assign({}, eventInfo, {
      image: viewportMedia,
      measure: measureInfo,
      timeline: timelineInfo
    });
  };

  const Model = Object.freeze({
    defaults: Object.freeze({ elements: [] })
  });

  const State = Object.freeze({
    defaults: Object.freeze({ selected: '' })
  });

  function CarouselComponent({ selector, model, state }) {
    EventEmitter.call(this);

    const uuid = 'xxxxyxxxx-xxxx-xxxx-yxxx-xxxxxxxx'.replace(/[xy]/g, (character) => {
      const random = Math.random() * 16 | 0;
      const part = character === 'x' ? random : ((random & 0x03) | 0x08);
      return part.toString(16).toLowerCase();
    });

    Object.assign(this, {
      uuid, selector,
      element: findElementBySelector(selector),
      model: Object.assign({}, Model.defaults, model),
      state: Object.assign({}, State.defaults, state)
    });
  }
  CarouselComponent.prototype = Object.create(EventEmitter.prototype);

  const pickIndicators = (elements) => {
    const choices = [];
    const spacing = Math.max(1, Math.floor(elements.length / 20));

    for (let index = 0; index < elements.length; index += spacing) {
      choices.push({ index, caption: index + 1, visible: true });

      for (let idx = 1; idx < spacing; idx += 1) {
        const position = idx + index;
        choices.push({
          index: position,
          caption: position + 1,
          visible: false
        });
      }
    }

    if (elements.length && !choices.includes(elements.length - 1)) {
      Object.assign(choices[elements.length - 1], { visible: true });
    }

    return choices;
  };

  CarouselComponent.prototype.render = function(state, model) {
    model = model || this.model || Model.defaults;
    state = state || this.state || State.defaults;
    const { selector, uuid } = this;

    const countOfElements = model.elements.length || 0;

    Object.assign(this, { state, model });

    const indicators = pickIndicators(model.elements).map(
      (item, index) => makeIndicatorElement({ selector: uuid, item, index })
    ).reduce((html, line) => html.concat(line), ``);

    const content = model.elements.map(
      (item, index) => makeIndicatorsMarkup(item, index)
    ).reduce((html, line) => html.concat(line), ``);

    destroyCarousel(uuid);
    const element = findElementBySelector(selector);
    element.innerHTML = makeCarouselMarkup(uuid, indicators, content, countOfElements, countOfElements < MIN_INDICATORS);

    model.elements.forEach((item, index) => {
      const theImageContainerSelector = `.display-image[data-image-index="${index}"]`;
      (new AtollogyImage(
        item,
        theImageContainerSelector,
        /** dont show bounding box */ false,
        /** show the social distance lines */ true)
      ).render();
    });

    const { emit } = this;
    const fireEvent = Function.prototype.bind.call(emit, this);

    const onTransitionStarted  = (eventInfo) => {
      fireEvent('carousel:transition:started', augmentEventInfo(eventInfo));
    };
    const onTransitionComplete = (eventInfo) => {
      $('.carousel-control-zoom').attr('href', eventInfo.element.dataset.timelineMedia);
      fireEvent('carousel:transition:completed', augmentEventInfo(eventInfo));
    };

    const onZoomRequested = (eventInfo) => {
      eventInfo = augmentEventInfo(eventInfo);
      fireEvent('carousel:zoom:requested', eventInfo);
      openMediaInTarget(eventInfo.image, DEFAULT_WINDOW);
    };

    createCarousel(uuid, onTransitionStarted, onTransitionComplete);

    const zoomIn = findElementBySelector('.carousel-control-zoom');
    zoomIn.addEventListener('click', (eventInfo) => {
      eventInfo.preventDefault();
      const target = findElementBySelector('.carousel-item.active');
      onZoomRequested({ element: target });
    });
  };

  CarouselComponent.prototype.markSelected = function(eventInfo) {
    const { uuid } = this;
    const { to: carouselIndex } = eventInfo;
    slideCarouselTo(uuid, carouselIndex);
  };

  const makeIndicatorElement = ({ selector, item, index }) => {
    const active = index === 0 ? 'active ' : '';
    const visible = item.visible ? '' : 'hidden';
    return (`
      <li class="atl-carousel-item ${active} ${visible}" data-target="#${selector}" data-slide-to="${item.index}">${item.caption}</li>
    `);
  };

  const makeMediaElementMarkup = ({ url, linkName, index }) => {
    const identity = `xxxy-xxxx-yxxx`.replace(/[xy]/g, (character) => {
      const random = Math.random() * 16 | 0;
      const part = character === 'x' ? random : ((random & 0x03) | 0x08);
      return part.toString(16).toLowerCase();
    });

    return (url ? url.endsWith('.mp4')
      ? (`<video id="video-${identity}" class="display-video" data-src="${url}" controls autoplay loop muted playsinline/>`)
      : (`<div id="image-${identity}" class="display-image" data-image-index="${index}" data-src="${url}" alt="${linkName}">&nbsp;</div>`)
      : (`<div id="image-${identity}" class="display-image" data-image-index="${index}" data-src="/portal/images/no-image.png" alt="${linkName}" style="text-decoration: line-through;">&nbsp;</div>`));
  };

  const makeIndicatorsMarkup = ({ url, linkName, imgKey }, index) => (`
    <div class="carousel-item ${index === 0 ? 'active ' : '' }" data-measure-name="${imgKey.measure}" data-timeline-timestamp="${imgKey.collection_time}" data-timeline-media="${url}">
      <div class="atl-media-element" data-media-source="${url}">${makeMediaElementMarkup({ url, linkName, index })}</div>
    </div>`
  );

  const makeCarouselMarkup = (selector, indicators, content, countOfElements, noIndicators) => (`
    <div id="${selector}" class="carousel" data-ride="carousel" data-carousel-count="${countOfElements}" data-interval="360000">
      <ol class="carousel-indicators ${noIndicators ? 'hidden' : ''}">${indicators}</ol>

      <div class="carousel-inner">
        ${content}
        <a class="carousel-control-prev ${noIndicators ? 'hidden' : ''}" href="#${selector}" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next ${noIndicators ? 'hidden' : ''}" href="#${selector}" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
        <a class="carousel-control-zoom" href="#zoom-${selector}" role="button">
          <span class="material-icons" aria-hidden="true">zoom_in</span>
          <span class="sr-only">Zoom in</span>
        </a>
      </div>
    </div>
  `);

  CarouselComponent.prototype.update = function(state, model) {
    Object.assign(this, {
      model: Object.assign({}, model),
      state: Object.assign({}, state)
    });
  };

  return Object.freeze(CarouselComponent);
}));
