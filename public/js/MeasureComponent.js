'use strict';

(function(global, makeMeasureComponent) {
  const { EventEmitter } = global;
  const findElementBySelector = (cssSelector) => document.querySelector(cssSelector);
  const findElementsBySelector = (cssSelector) => document.querySelectorAll(cssSelector);

  global.MeasureComponent = makeMeasureComponent(EventEmitter, {
    findElementBySelector,
    findElementsBySelector
  });
}(this, function makeMeasureComponent (EventEmitter, {
  findElementBySelector,
  findElementsBySelector
}) {
  const augmentEventInfo = (eventInfo) => {
    const { target: element } = eventInfo;
    const measureInfo = element.getAttribute('data-measure-name');

    return Object.assign({}, eventInfo, { measure: measureInfo });
  };

  const Model = Object.freeze({ defaults: { elements: [] } });

  const State = Object.freeze({ defaults: { selected: '' } });

  const makeMeasureElement = (item, index) => (`
    <li class="atl-measures-element ${index === 0 ? 'active ' : '' }" data-measure-name="${item.value}" data-measure-index="${index}">
      <div class="atl-measures-element-container">
        <a class="atl-measure-name" data-measure-name="${item.value}" data-measure-index="${index}" href="#">${item.caption}</a>
      </div>
    </li>
  `);

  const makeMeasureMarkup = (elementsMarkup, countOfMeasures) => {
    const visible = countOfMeasures > 1 ? '' : 'hidden';
    return (`<ol class="atl-measures ${visible}" data-measures-count="${countOfMeasures}">${elementsMarkup}</ol>`);
  };

  const MeasureComponent = function({ selector, state, model }) {
    EventEmitter.call(this);

    Object.assign(this, {
      selector,
      element: findElementBySelector(selector),
      model: Object.assign({}, Model.defaults, model),
      state: Object.assign({}, State.defaults, state)
    });
  };
  MeasureComponent.prototype = Object.create(EventEmitter.prototype);

  MeasureComponent.prototype.render = function (model, state) {
    model = model || this.model || Model.defaults;
    state = state || this.state || State.defaults;
    const { element } = this;

    Object.assign(this, { model, state });

    const elementsMarkup = model.elements.map((item, index) => makeMeasureElement(item, index)).join('');

    const countOfMeasures = model.elements.length || 0;
    element.innerHTML = makeMeasureMarkup(elementsMarkup, countOfMeasures);

    const { emit, markSelected } = this;
    const fireEvent = Function.prototype.bind.call(emit, this);
    const fireMarkSelected = Function.prototype.bind.call(markSelected, this);

    const onMeasureElementClicked = (eventInfo) => {
      eventInfo = augmentEventInfo(eventInfo);
      fireMarkSelected(eventInfo);
      fireEvent('measure:element:activated', eventInfo);
    };

    const children = findElementsBySelector('.atl-measure-name');
    children.forEach((timelineElement) => timelineElement.addEventListener('click', (eventInfo) => {
      eventInfo.preventDefault();
      return onMeasureElementClicked(eventInfo);
    }));
  };

  MeasureComponent.prototype.markSelected = function (options) {
    const children = findElementsBySelector('.atl-measures-element');

    if ('object' === typeof options && Object.prototype.hasOwnProperty.call(options, 'measure')) {
      const { measure: targetMeasure } = options;

      children.forEach((measureElement) => {
        const elementName = measureElement.getAttribute('data-measure-name');
        measureElement.className = 'atl-measures-element';
        if( elementName === targetMeasure ) measureElement.classList.add('active');
      });
    } else if ('object' === typeof options && Object.prototype.hasOwnProperty.call(options, 'index')) {
      const { index: targetIndex } = options;

      children.forEach((measureElement) => {
        const elementIndex = measureElement.getAttribute('data-measure-index');
        measureElement.className = 'atl-measure-element';
        if( targetIndex === elementIndex ) measureElement.classList.add('active');
      });
    }
  };

  MeasureComponent.prototype.update = function (model, state) {
    Object.assign(this, {
      model: Object.assign({}, model),
      state: Object.assign({}, state)
    });
  };

  return Object.freeze(MeasureComponent);
}));
