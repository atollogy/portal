'use strict';

(function (global, makeEventSequence) {
  const EventSequence = makeEventSequence(global);
  global.EventSequence = Object.freeze(EventSequence);
}(this, function (global) {
  const EventSequence = function () {};

  const SafeRack = new EventSequence();
  const YardOperation = new EventSequence();
  const GroundOperation = new EventSequence();
  const ManufactureOperation = new EventSequence();

  Object.assign(EventSequence, { YardOperation, GroundOperation, ManufactureOperation, SafeRack });

  return EventSequence;
}));
