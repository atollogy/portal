'use strict';

(function (global, window, document) {
  const self = this;
  const PRELOAD_THRESHOLD = 3;
  const { getComputedStyle } = window;

  const state = {
    active: false,
    enabled: false,
    elements: [/* */]
  };

  const registerListeners = function registerListeners() {
    document.addEventListener('scroll', onLazyLoading);
    window.addEventListener('resize', onLazyLoading);
    window.addEventListener('orientationchange', onLazyLoading);
  };

  const removeListeners = function removeListeners() {
    document.removeEventListener('scroll', onLazyLoading);
    window.removeEventListener('resize', onLazyLoading);
    window.removeEventListener('orientationchange', onLazyLoading);
  };

  const onComponentUpdate = (function onComponentUpdate() {
    let { elements } = state;

    try {
      elements.forEach((element, index) => {
        const container = element.tagName.toLowerCase() === 'source' ? element.parentElement : element;

        const isPreloaded = index < PRELOAD_THRESHOLD;

        const styles = getComputedStyle(container);
        const bounds = container.getBoundingClientRect();
        const isVisible = bounds.top <= window.innerHeight && bounds.bottom >= 0 && styles.display !== 'none';

        if (isPreloaded || isVisible) {
          ['src', 'srcset'].forEach((attribute) => {
            const theDataAttribute = `data-${attribute}`;
            const mediaElementSource = element.getAttribute(theDataAttribute);
            if (mediaElementSource) {
              element.removeAttribute(theDataAttribute);
              element[attribute] = mediaElementSource;
            }
          });

          element.classList.remove('lazy');
          elements = elements.filter((media) => media !== element);
        }
      });

      if (elements.length === 0) {
        removeListeners();
        state.enabled = false;
      }

    } finally {
      state.elements = elements;
      state.active = false;
    }
  }).bind(self);

  const onLazyLoading = (function onLazyLoading() {
    if (state.active === false) {
      state.active = true;
      setTimeout(onComponentUpdate, 100);
    }
  }).bind(self);

  const doComponentCycle = (function doComponentCycle(){
    const proposed = ['src', 'srcset'].reduce((aggregate, attribute) => {
      const selector = `.lazy[data-${attribute}]`;
      const elements = [].slice.call(document.querySelectorAll(selector));
      return [].concat(aggregate).concat(elements);
    }, []);

    if (state.timeout) clearTimeout(state.timeout);

    if (proposed.length) {
      state.elements = [].concat(proposed);
      state.timeout = setTimeout(() => {
        onComponentUpdate();
        onContentLoaded();
      }, 100);
    } else {
      state.timeout = setTimeout(doComponentCycle, 150);
    }
  }).bind(self);

  const onComponentLoaded = registerListeners.bind(self);

  const onContentLoaded = (function() {
    if (state.enabled === false) {
      state.enabled = true;
      state.elements = ['src', 'srcset'].reduce((aggregate, attribute) => {
        const selector = `.lazy[data-${attribute}]`;
        const elements = [].slice.call(document.querySelectorAll(selector));
        return [].concat(aggregate).concat(elements);
      }, []);
      onComponentLoaded();
    }
  }).bind(self);

  document.addEventListener('DOMContentLoaded', doComponentCycle);

  const theLazyLoadingModule = Object.freeze({
    init: () => doComponentCycle(),
    active: () => state.active,
    enabled: () => state.enabled,
    elements: () => {
      const context = {
        index: 0,
        elements: [].concat(state.elements)
      };

      return Object.freeze({
        [Symbol.iterator]: () => {
          const { index, elements } = context;
          if (index >= elements.length) {
            return ({ value: undefined, done: true });
          } else {
            context.index = context.index + 1;
            return ({ value: elements[index], done: false });
          }
        }
      });
    }
  });

  global.LazyLoading = theLazyLoadingModule;
}(this, window, document));
