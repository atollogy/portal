'use strict';

(function (global, newSubjectStats) {
  const Atollogy = global.Atollogy || (global.Atollogy = {});

  Object.assign(Atollogy, newSubjectStats());
}(this, () => {
  const States = Object.freeze({
    stats: (theListOfStates, theReportConfig, theHighWatermark, theSubjectName, theMeasureName) => {
      const currentDatetime = moment.now();
      theHighWatermark = theHighWatermark || currentDatetime;
      var stateHash = theListOfStates.reduce(function (acc, val) {
        if (acc[val.state] === undefined) {
          acc[val.state] = 0;
        }

        var maxDate = theHighWatermark ? moment.min(moment(val.endTime), moment(theHighWatermark)) : moment(val.endTime);
        acc[val.state] += maxDate.diff(moment(val.startTime));
        return acc;
      }, {});

      let subjectConfigs = [];
      if (theSubjectName) {
        const subjCfg = theReportConfig[theSubjectName];
        const measureDef = subjCfg[theMeasureName];
        if (measureDef) subjectConfigs = Object.keys(measureDef['stateLabels']);
      }

      let stateKeys = Object.keys(stateHash);
      subjectConfigs.forEach(theSubjectConfig => {
        if (!stateKeys.includes(theSubjectConfig)) {
          stateHash[theSubjectConfig] = 0;
        }
      });

      return Object.keys(stateHash).map(key => {
        return {
          state: key,
          subject: theSubjectName,
          measure: theMeasureName,
          duration: stateHash[key],
          instances: theListOfStates.filter(d => d.state == key).length
        };
      });
    }
  });

  return Object.freeze({ States });
}));