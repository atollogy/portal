'use strict';

(function (global, newLabelModule) {
  const Atollogy = global.Atollogy || (global.Atollogy = {});

  Object.assign(Atollogy, newLabelModule(Atollogy));
}(this, ({ StateMappings }) => {
  const newLabelProvider = (reportConfig) => {
    return ({ subject, measure, state: theStateValue }) => {
      const theLabelStateObject = { /* */ };
      const theReportConfig = Object.assign({}, reportConfig);
      const theMappedState = Object.prototype.hasOwnProperty.call(StateMappings, theStateValue) ? StateMappings[theStateValue] : theStateValue;

      const { [subject]: theSubjectConfig } = Object.assign({}, { [subject]: {} }, theReportConfig);
      const { [measure]: theMeasureConfig } = Object.assign({}, { [measure]: {} }, theSubjectConfig);
      const { stateLabels: theStateLabel } = Object.assign({}, { stateLabels: {} }, theMeasureConfig);
      const { [theStateValue]: theCandidateLabel } = Object.assign({}, { [theMappedState]: theMappedState }, theStateLabel);

      Object.assign(theLabelStateObject, { [theMappedState]: theCandidateLabel });

      return theLabelStateObject[theMappedState];
    };
  };
  return Object.freeze({ newLabelProvider });
}));