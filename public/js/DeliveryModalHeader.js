'use strict';

(function makeModalHelper(global, registrar) {
  const { document } = global;
  const findElementBySelector = (cssSelector) => document.querySelector(cssSelector);

  registrar.call(global, `@atollogy/delivery-modal-header`, [], function () {
    const makeDeliveryModalHeaderMarkup = ({ uuid, deliveryHeader }) => (`
    <div id="${uuid}" class="modalRow">
      <span class="modal-subject">${deliveryHeader.subject}</span><br/>
      <span class="modal-dropOff">${deliveryHeader.dropOff}</span><br/>
      <span class="modal-deliveryTime">${deliveryHeader.deliveryTime}</span><br/>
    </div>
    `);

    const Model = Object.freeze({
      defaults: Object.freeze({ title: 'Atollogy' })
    });

    const DeliveryModalHeader = function ({ selector, model }) {
      const uuid = 'xxxxyxxxx-xxxx-xxxx-yxxx-xxxxxxxx'.replace(/[xy]/g, (character) => {
        const random = Math.random() * 16 | 0;
        const part = character === 'x' ? random : ((random & 0x03) | 0x08);
        return part.toString(16).toLowerCase();
      });

      Object.assign(this, {
        uuid, selector,
        element: findElementBySelector(selector),
        model: Object.assign({}, Model.defaults, model)
      });
    };

    DeliveryModalHeader.prototype.updateMedia = function (eventInfo) {
      const { model } = this;
      const deliveryHeader = eventInfo.deliveryHeader || model.deliveryHeader;

      DeliveryModalHeader.prototype.update.call(this, { deliveryHeader });
    };

    DeliveryModalHeader.prototype.update = function (model) {
    };

    DeliveryModalHeader.prototype.render = function (model) {
      model = model || this.model || Model.defaults;

      const { uuid, selector } = this;
      const element = findElementBySelector(selector);

      const deliveryHeader = model;
      element.innerHTML = makeDeliveryModalHeaderMarkup({ uuid, deliveryHeader });
    };

    return DeliveryModalHeader;
  });

  registrar.call(global, `DeliveryModalHeader`, ['@atollogy/delivery-modal-header'], (DeliveryModalHeader) => DeliveryModalHeader);

}(this, function makeRegistrarHelper(name, dependencies, factory) {
  const global = this;
  const { module } = this;

  if ('function' === typeof require && 'object' === typeof module && hasOwnProperty.call(module, 'exports')) {
    dependencies = dependencies.map((dependency) => require(dependency));
    module.exports = factory.apply(global, dependencies);
    return;
  }

  dependencies = Array.isArray(dependencies) ? dependencies : [dependencies];
  dependencies = dependencies.map((dependency) => global[dependency]);

  const previous = global[name] || undefined;
  const newModuleInstance = factory.apply(global, dependencies);

  newModuleInstance.noConflict = () => {
    global[name] = previous;
    return newModuleInstance;
  };

  global[name] = newModuleInstance;
}));