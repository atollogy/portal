(function (global, factory){
  const { ModalHeader, DeliveryModalHeader, TimelineComponent, MeasureComponent, CarouselComponent } = global;

  const { ObservationModal } = factory({ ModalHeader, DeliveryModalHeader, TimelineComponent, MeasureComponent, CarouselComponent });

  global.ObservationModal = ObservationModal;
}(this, function makeObservationModal({
  ModalHeader,
  DeliveryModalHeader,
  TimelineComponent,
  MeasureComponent,
  CarouselComponent
}) {
  const reSubSecondPrecision = /(\.\d{1,})/;
  const reDateStringFormat = /\d{2,4}-\d{1,2}-\d{1,2}T\d{1,2}:\d{1,2}:\d{1,2}(.\d{1,6})/;
  const ObservationModal = function (theCustomer, listOfImages, deliveryHeader=null) {
    const listOfMeasures = {};
    const timelineElements = [];

    listOfImages = listOfImages.map((element) => {
      const { imgKey: imageObject, linkName } = Object.assign({ imgKey: {}, linkName: '' }, element);
      const { gateway_id, camera_id, collection_time, step_name, name, measure = undefined } = imageObject;

      if (Object.prototype.hasOwnProperty.call(imageObject, 'measure') && 'undefined' !== imageObject.measure) {
        listOfMeasures[imageObject.measure] = 1;
      }
      let theImageTimeStamp = null;
      if (isNaN(collection_time)) {
        const theTimeUnix = moment(collection_time).unix();
        let theTimePrecision = '';
        if (reDateStringFormat.test(collection_time)) {
          theTimePrecision = reDateStringFormat.exec(collection_time).find((_, index) => index === 1);
        } else if (reSubSecondPrecision.test(collection_time)) {
          theTimePrecision = reSubSecondPrecision.exec(collection_time / 1000).find((_, index) => index === 1);
        }
        theImageTimeStamp = `${theTimeUnix}${Number(theTimePrecision) > 0 ? theTimePrecision : ''}`;
      } else {
        theImageTimeStamp = `${collection_time}`;
      }
      timelineElements.push({
        measure: measure,
        timestamp: theImageTimeStamp,
        caption: linkName
      });
      const theImageURL = `/portal/image/${theCustomer}/${gateway_id}/${camera_id}/${theImageTimeStamp}/${step_name}/${name}`;
      return Object.assign({}, { url: theImageURL }, element);
    });

    const infoImage = listOfImages.filter(({ info = '' }) => info && info !== 'Company: N/A');

    const headerModel = listOfImages.length ? {
      title: listOfImages[0].header,
      info: infoImage.length ? infoImage[0].info: listOfImages[0].info,
      icon: listOfImages[0].imgKey.name.endsWith('.mp4') ? 'videocam' : 'photo_camera'
    } : { title: 'No image available', icon: 'photo_camera' };

    const measureElements = Object.keys(listOfMeasures).filter(Boolean).map((measure) => ({ value: measure, caption: measure }));

    Object.defineProperty(this, 'Components', {
      value: Object.freeze({
        /** */
        Header: deliveryHeader ? new DeliveryModalHeader({
          selector: '#attributes',
          model: deliveryHeader
        }) : new ModalHeader({
          selector: '#attributes',
          model: headerModel
        }),
        /** */
        Measures: new MeasureComponent({
          selector: '#measure-container',
          model: {
            elements: measureElements.length > 1 ? measureElements : []
          }
        }),
        /** */
        Timeline: new TimelineComponent({
          selector: '#imgLinks',
          model: {
            elements: timelineElements
          }
        }),
        /** */
        Carousel: new CarouselComponent({
          selector: '#carousel-container',
          model: {
            elements: listOfImages
          }
        })
      }),
      enumerable: true,
      writable: false
    });

    bindComponentEvents(this);
    setInitialState(this);
  };

  const setInitialState = ({ Components }) => {
    const { Carousel } = Components;
    Carousel.markSelected({ to: 0 });
  };

  const bindComponentEvents = ({ Components }) => {
    /**
     * When a measure item is clicked, update the timeline
     * @param {*} eventInfo
     */
    const { Header, Timeline, Measures, Carousel } = Components;
    const onMeasureItemSelected = (eventInfo) => {
      Timeline.setMeasureSelected(eventInfo);
    };

    /**
     * When a time is clicked, update the carousel
     * @param {*} eventInfo
     */
    const onTimelineItemSelected = (eventInfo) => {
      Carousel.markSelected(eventInfo);
      Measures.markSelected(eventInfo);
    };

    /**
     * When a carousel item is displayed, update the measures and the timeline
     * @param {*} eventInfo
     */
    const onCarouselTransitionComplete = (eventInfo) => {
      Timeline.markSelected(eventInfo);
      Measures.markSelected(eventInfo);
      Header.updateMedia(eventInfo);
    };

    Measures.on('measure:element:activated', onMeasureItemSelected);
    Timeline.on('timeline:element:activated', onTimelineItemSelected);
    Carousel.on('carousel:transition:completed', onCarouselTransitionComplete);
  };

  ObservationModal.prototype.render = function () {
    const { Components } = this;

    Object.keys(Components).forEach((name) => {
      const component = Components[name];
      component.render();
    });
  };

  return Object.freeze({ ObservationModal });
}));
