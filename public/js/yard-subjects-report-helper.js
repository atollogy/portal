/* eslint-disable no-undef,no-unused-vars,no-redeclare, no-global-assign*/
var barWidth = 720;
var barMarginRight = 20;
var barMarginLeft = 20;
var barMarginTop = 3;
var barMarginBottom = 3;
var barHeight = 36;
var color;
var header = null;
var dataRefreshInterval;
var inProgress = false;
let test = false;
//config
var definitions;
var reportCfg = {};
var lastResult, previousShiftResult;
let configObj = null;

const { newColorProvider, newLabelProvider, newWidthCalculator, hasFlashingState, Legends, States } = Atollogy;

const INTERESTING_STATES = ['present', 'running'];

const makeZoomObject = () => {
  const state = {
    enabled: false,
    active: false
  };

  const zoomObject = Object.freeze({
    enter: (element)  => {
      if (state.enabled === false) {
        /** Zoom feature flag */
        return;
      }

      state.active = true;
      const siblings = [ ...element.parentElement.children ];
      siblings.sort((left, right) => {
        const leftX = parseFloat(left.getAttribute('x'));
        const rightX = parseFloat(right.getAttribute('x'));
        return Math.sign(leftX - rightX);
      });
      const elementIndex = siblings.indexOf(element);
      const lowerBoundary = Math.max(0, elementIndex - 3);
      const upperBoundary = Math.min(siblings.length - 1, elementIndex + 3);

      const zoomable  = siblings.slice(lowerBoundary, upperBoundary + 1);

      const totalWidth = zoomable.reduce((accumulated, item) => {
        const reference = d3.select(item);
        const elementWidth = parseFloat(reference.attr('width'));
        return accumulated + elementWidth;
      }, 0);

      const sanitizedSize = Math.max(8 * zoomable.length, totalWidth);

      zoomable.forEach((rectangle) => {
        const matrix  = [1, 0, 0, 1, 0, 0];
        const reference = d3.select(rectangle);

        const elementWidth = parseFloat(reference.attr('width'));
        const elementHeight = parseFloat(reference.attr('height'));
        const scaleX = Math.max(1, Math.round(8 / elementWidth));

        matrix[0] = scaleX; /** Resize the element to 16px wide if smaller than so. */
        matrix[3] = 1; /** Keep the element hight the same. */

        matrix[4] = 0; /* Panning in X */
        matrix[5] = 0; /* No panning needed on Y as we dont resize vertically */

        reference.attr('transform', `matrix(${matrix.join(' ')})`);
      });
    },
    exit: (element)  => {
      if (state.enabled && state.active) {
        const siblings = [ ...element.parentElement.children ];

        siblings.forEach((item) => {
          const reference = d3.select(item);
          reference.attr('transform', 'matrix(1, 0, 0, 1, 0, 0)');
        });
      }
    }
  });

  return zoomObject;
};

const zoom = makeZoomObject();

const getAdditionalInfo = (domainObject, imageObject) =>  {
  const customerName = imageObject.customer_name || domainObject.customer_name || 'N/A';
  const itemNumber = imageObject.unit_number || domainObject.unit_number || undefined;

  let info = '';
  if (imageObject.customer_name && imageObject.unit_number) {
    info = `Company: ${imageObject.customer_name}-${imageObject.unit_number}`;
  } else if (domainObject.customer_name && domainObject.unit_number) {
    info = `Company: ${domainObject.customer_name}-${domainObject.unit_number}`;
  }
  if (domainObject.violations && 'safe_cage' in domainObject.violations) {
    for (const [key, value] of Object.entries(domainObject.violations['safe_cage'])) {
      let violation = '';
      for (const [key, value] of Object.entries(domainObject.violations['safe_cage'])) {
        if(domainObject.violations['safe_cage'][key].length > 0) {
          domainObject.violations['safe_cage'][key].forEach(vi => {
            violation = violation + vi + ', ';
          });
        }
      }
      if (violation !== '') {
        violation = violation.slice(0, -2);
        if (info !== '') {
          return `${info};
          Violations: ${violation}`;
        } else {
          return ` Violations: ${violation}`;
        }
      }
    }
  }
};

function mergeUtilization(d, data) {
  if (d.duration !== 0 && d.instances !== 0) {
    let legend_annotation = ': ' + formatDurationAsTime(d.duration) + ' (' +
        formatDurationAsPercentage(d.duration, moment(data.highWaterMarkTime || data.shiftEndTime).diff(moment(data.shiftStartTime)))
        + ') ' + 'Avg: ' + formatDurationAsTime(d.duration / d.instances);
    return legend_annotation;
  } else {
    return ': ' + formatDurationAsTime(d.duration) + ' (' +
        formatDurationAsPercentage(d.duration, moment(data.highWaterMarkTime || data.shiftEndTime).diff(moment(data.shiftStartTime)))
        + ') ' + 'Avg: 0 sec';
  }
}

function insertOverallUtilization(d, data)  {
  if (d.aggregates.utilization == null){
    return null;
  }
  let utilization = d.aggregates.utilization || 0;
  if (utilization.percentage !== null) {
    return `${utilization.duration}, ${utilization.percentage}`;
  } else {
    return utilization.duration;
  }

}

function renderSummary(data, parameters) {
  //append timezone text to report title
  d3.select('#reportDateTitle div.tz').text(` ${timezone}`);
  //Missing Data-Some message if necessary
  if (!data.subjects.length) {
    renderNoShift(parameters.shift, parameters.date);
  } else {
    d3.select('#message').text('');
    d3.select('#station-count').text(function () {
      if (data.subjects.length > 1) {
        return 'Stations: ' + data.subjects.length;
      } else if (data.subjects.length == 1) {
        return 'Station: ' + data.subjects.length;
      }
    });
  }
  var hasImages = data.subjects.some(function (subject) {
    const measureStates = subject.measures[definitions.graphMeasure];
    const lastState = measureStates && measureStates.length ? lastAvailableState(measureStates) : undefined;
    return lastState && lastState.images && lastState.images.length;
  });
  d3.select('div#subjects').classed('inProgress', inProgress).classed('hasImages', hasImages);
  //create the scale
  var xAxis = d3.scaleTime().domain([
    moment(data.shiftStartTime).valueOf(),
    moment(data.shiftEndTime).valueOf()
  ]).range([barMarginLeft, barWidth + barMarginLeft]);

  var timeTicks = xAxis.ticks(12).filter((t) => t.getMinutes() == 0);
  let prevDt = null;
  var scaleSvg = d3.select('div#subjects div.scale svg')
    .attr('height', 35)
    .attr('width', barWidth + barMarginLeft + barMarginRight);
  var scaleTicks = scaleSvg.selectAll('line.tick').data(timeTicks);
  scaleTicks.enter().append('line').classed('tick', true)
    .attr('stroke', '#ccc')
    .attr('stroke-width', 1)
    .attr('y1', 30)
    .attr('y2', 35)
    .merge(scaleTicks)
    .attr('x1', xAxis)
    .attr('x2', xAxis);
  var scaleLabels = scaleSvg.selectAll('text.tickLabel').data(timeTicks);
  scaleLabels.enter().append('text').classed('tickLabel', true)
    .attr('y', 0)
    .attr('dy', 10)
    .attr('fill', '#bbb')
    .attr('text-anchor', 'middle')
    .merge(scaleLabels)
    .attr('x', xAxis)
    .text(function (d, i) {
      // return moment.tz(d, timezone).format('HH:mm');
      let dt = '';
      if (i > 0 && (moment.tz(d, timezone).format('MM/DD') !== moment.tz(prevDt, timezone).format('MM/DD'))) {
        dt = moment.tz(d, timezone).format('MM/DD');
      }
      prevDt = d;
      return dt + ' ' + moment.tz(d, timezone).format('HH:mm');
    });
  d3.selectAll('.tickLabel').call(wrap, 10);
  //create the subjects containers
  var subjectDivs = d3.select('div#subjects').selectAll('div.subject').data(data.subjects, ({ name }) => name).order();
  var newSubjectDivs = subjectDivs.enter().append('div').classed('subject', true);
  let subjectTitle = newSubjectDivs.append('div').classed('subjectHeader', true);
  subjectTitle.append('div').classed('title',true);
  // let compositeConfig = subjectTitle.append('div').classed('config',true);
  // compositeConfig.append('i').classed('material-icons', true).text('settings');
  var aggregateDiv = newSubjectDivs.append('div').classed('aggregate', true).classed(`${parameters.customer}`, true);
  aggregateDiv.append('span').classed('label', true);
  let metrics = aggregateDiv.append('div').classed('metrics', true);
  metrics.append('span').classed('count', true);
  metrics.append('span').classed('duration', true);
  metrics.append('span').classed('percentage', true);
  aggregateDiv.append('span').classed('change', true);
  var statusTimelineContainer = newSubjectDivs.append('div').classed('statusTimeline', true);
  newSubjectDivs.append('div').classed('legend', true);
  var timelineSvg = statusTimelineContainer.append('svg').classed('statusTimeline', true)
    .attr('height', barHeight + barMarginTop + barMarginBottom)
    .attr('width', barWidth + barMarginLeft + barMarginRight)
    .attr('viewBox', `0 0 ${barWidth + barMarginLeft + barMarginRight} ${barHeight + barMarginTop + barMarginBottom}`);
  timelineSvg.append('g').classed('ticks', true);
  const measureGroup = timelineSvg.append('g').classed('measures', true);
  measureGroup.append('rect')
    .attr('fill', '#eee')
    .attr('x', barMarginLeft)
    .attr('y', barMarginTop)
    .attr('width', barWidth)
    .attr('height', barHeight);

  if (inProgress && !isHistoricalDay()) {
    var currentStatus = newSubjectDivs.append('div').classed('currentStatus', true).classed('status-badge', true);
    currentStatus.append('div').classed('duration', true);
    currentStatus.append('div').classed('status', true);
    currentStatus.append('div').classed('updateTime', true);
    newSubjectDivs.append('div').classed('currentImage', true);
  }

  var mergedSubjectDivs = newSubjectDivs.merge(subjectDivs);
  mergedSubjectDivs.select('div.title')
    .on('click', function (d) {
      parameters.subject = d.name;
      let paramString = Object.keys(parameters).map(p => p + '=' + parameters[p]).join('&');
      window.location = '/portal/reports/YardSubjectDetail?' + paramString;
    })
    .text(function (d) {
      return d.name;
    });
  mergedSubjectDivs.select('div.currentStatus').on('click', function (d) {
    parameters.subject = d.name;
    let paramString = Object.keys(parameters).map(p => p + '=' + parameters[p]).join('&');
    window.location = '/portal/reports/YardSubjectDetail?' + paramString;
  });
  subjectDivs.exit().remove();

  //the timeline bar
  var ticks = mergedSubjectDivs.select('svg.statusTimeline g.ticks').selectAll('line.tick').data(timeTicks);
  ticks.enter().append('line')
    .classed('tick', true)
    .attr('stroke', '#ccc')
    .attr('stroke-width', 1)
    .attr('y1', 0)
    .attr('y2', barHeight + barMarginTop + barMarginBottom)
    .merge(ticks)
    .attr('x1', xAxis)
    .attr('x2', xAxis);

  var eventMarks = mergedSubjectDivs.select('svg.statusTimeline g.measures').selectAll('rect.mark').data(function (d) {
    return d.measures[definitions.graphMeasure] ? d.measures[definitions.graphMeasure].map(v => {
      return {
        subject: d.name,
        measure: definitions.graphMeasure,
        state: v.state,
        missingMeasures: v.missingMeasures,
        startTime: v.startTime,
        endTime: v.endTime,
        images: v.images,
        turn: v.turn || {},
        centroids: v.centroids || null,
        violations: v.violations || null,
        customer_name: v.customer_name,
        unit_number: v.unit_number
      };
    }) : [];
  });
  var newMarks = eventMarks.enter().append('rect')
    .classed('mark', true)
    .attr('y', barMarginTop)
    .attr('height', barHeight);
  newMarks.append('title');

  const theColorProvider = newColorProvider(reportCfg);
  const theLabelProvider = newLabelProvider(reportCfg);
  const theWidthCalculator = newWidthCalculator(xAxis, reportCfg);
  var mergedMarks = newMarks.merge(eventMarks)
    .attr('fill', theColorProvider)
    .attr('x', ({ startTime }) => xAxis(moment(startTime).valueOf()))
    .attr('width', theWidthCalculator)
    .classed('flashing', hasFlashingState)
    .classed('imageBacked', function (d) {
      return d.images && d.images.length;
    })
    .on('mouseenter', function (d) {
      d3.select(this).raise();
    })
    .on('mouseover', function (d) {
      tooltip.transition()
        .duration(200)
        .style('opacity', .9);
      let start = moment.tz(d.startTime, timezone).format('hh:mm a');
      let end = moment.tz(d.endTime, timezone).format('hh:mm a');
      let missingMeasures = '';
      if (d.missingMeasures.length > 0) {
        missingMeasures = ' => ' + d.missingMeasures.join(', ');
      }
      tooltip.html(reportCfg[d.subject][d.measure]['stateLabels'][d.state] + missingMeasures + ' : ' + start + ' - ' + end)
        .style('left', (d3.event.pageX - 50) + 'px')
        .style('top', (d3.event.pageY - 28) + 'px');
      zoom.enter(this);
    })
    .on('mouseout', function (d) {
      tooltip.transition()
        .duration(500)
        .style('opacity', 0);
      zoom.exit(this);
    })
    .on('click', function (d) {
      if (d.measure === definitions.graphMeasure && d.turn && Object.keys(d.turn).length) {
        d.turn.turnStartTime = moment.tz(d.turn.turnStartTime, timezone).format('YYYY-MM-DD HH:mm:ss');
        d.turn.turnEndTime = moment.tz(d.turn.turnEndTime, timezone).format('YYYY-MM-DD HH:mm:ss');
        window.location.href = `/portal/reports/gateTurn?customer=${parameters.customer}&facility=${parameters.facility}&subject=${d.subject}&start=${d.turn.turnStartTime}&end=${d.turn.turnEndTime}`;
        return;
      }
      if (!d.images) return;
      $('#imgModal').modal({backdrop: 'static'});
      let interval = moment(d.startTime.valueOf()).format('HH:mm') + ' - ' + moment(d.endTime.valueOf()).format('HH:mm');
      let measureState = d.measure + ' : ' + reportCfg[d.subject][d.measure]['stateLabels'][d.state];
      let intervalDiv = d3.select('div.interval');
      let subjectDiv = d3.select('div.subject-name');
      let stateDiv = d3.select('div.measure-state');
      intervalDiv.selectAll('span').remove();
      subjectDiv.selectAll('span').remove();
      stateDiv.selectAll('span').remove();
      let newIntervalDiv = intervalDiv.append('span').text(interval);
      let newSubjectDiv = subjectDiv.append('span').text(d.subject);
      let newStateDiv = stateDiv.append('span').text(measureState);
      newIntervalDiv.merge(intervalDiv);
      newSubjectDiv.merge(subjectDiv);
      newStateDiv.merge(stateDiv);
      let lastImg = d.images.length ? d.images[d.images.length - 1] : undefined;
      if (lastImg === 'undefined') {
        return;
      }
      //decimate images as per configured imgInterval
      let listOfImages = [];
      //decimate images as per configured imgInterval
      let measureImgMap = d.images.reduce((measureMap, img) => {
        if(!measureMap[img.measure]) {
          measureMap[img.measure] = [];
        }
        measureMap[img.measure].push(img);
        return measureMap;
      }, {});

      let images = [];
      Object.keys(measureImgMap).forEach(m => {
        let filteredImages = measureImgMap[m].filter(
          (imageRef, index, array) => {
            if (index == 0 || index == array.length - 1) {
              return true;
            }
            if (typeof reportCfg[d.subject][d.measure]['imgInterval'] === 'number') {
              return index % reportCfg[d.subject][d.measure]['imgInterval'] == 0;
            } else {
              return (index % (reportCfg[d.subject][d.measure]['imgInterval'][d.state] || 1 )) == 0;
            }
          });
        images = images.concat(filteredImages);
      });
      // Filter any null values (when the image is missing for a particular duration)
      images = images.filter(Boolean);
      images.forEach(function (imageObject) {
        listOfImages.push({
          linkName: moment.tz(imageObject.collection_time, timezone).format('HH:mm:ss'),
          imgKey: imageObject,
          url: newImageComponent(parameters.customer, imageObject).toImageURL(),
          header: `${upperFirst(d.measure)}: ${reportCfg[d.subject][d.measure]['stateLabels'][d.state] || upperFirst(d.state)}`,
          info: getAdditionalInfo(d, imageObject),
          customerTag: {
            tagArr: [],
            subject: d.state
          }
        });
      });
      renderImgModal(listOfImages, d);
    });
  mergedMarks.select('title').text(function (d) {
    return d.state + ': ' + moment.tz(d.startTime, timezone).format('hh:mm a') + ' - ' + moment.tz(d.endTime, timezone).format('hh:mm a');
  });
  eventMarks.exit().remove();

  //the legend
  var legendEntries = mergedSubjectDivs.select('div.legend').selectAll('div.legendEntry').data(function (d) {
    let stats = States.stats(
      d.measures[definitions.graphMeasure] ? d.measures[definitions.graphMeasure] : [],
      reportCfg,
      data.highWaterMarkTime,
      d.name,
      definitions.graphMeasure
    );
    return stats;
  });
  var newLegendEntries = legendEntries.enter()
    .append('div')
    .classed('legendEntry', true)
    .classed('hidden', (d) => d.duration === 0 || d.instances === 0);

  newLegendEntries.append('svg').classed('legendSwatch', true)
    .append('circle')
    .classed('flashing', hasFlashingState)
    .attr('stroke-width', '0.75')
    .attr('stroke', 'black')
    .attr('cx', 5)
    .attr('cy', 5)
    .attr('r', 5)
    .attr('fill', function (d) {
      return reportCfg[d.subject][definitions.graphMeasure]['stateColors'][d.state];
    });
  newLegendEntries.append('div').classed('legendLabel', true).text(theLabelProvider);
  newLegendEntries.append('div').classed('legendAnnotation', true);

  newLegendEntries.merge(legendEntries).select('div.legendAnnotation').text(function(d){ return mergeUtilization(d, data);});

  legendEntries.exit().remove();

  //overall utilization
  mergedSubjectDivs.select('div.metrics span.count').text(function (d) {
    if (d.aggregates.utilization == null) {
      return null;
    }
    let utilization = d.aggregates.utilization || 0;
    return 'Utilization: ' + `${utilization.activityCount},`;
  });
  mergedSubjectDivs.select('div.metrics span.duration').text(function (d) {
    if (d.aggregates.utilization == null) {
      return null;
    }
    let utilization = d.aggregates.utilization || 0;
    return ` ${ utilization.duration}`;
  });
  mergedSubjectDivs.select('div.metrics span.percentage').text(function (d) {
    if (d.aggregates.utilization == null) {
      return null;
    }
    let utilization = d.aggregates.utilization || 0;
    if (utilization.percentage !== null){
      return `, ${ utilization.percentage}`;
    } else {
      return null;
    }
  });

  if (inProgress) {
    mergedSubjectDivs.select('div.currentStatus').style('background-color', function (d) {
      let states = d.measures[definitions.graphMeasure] || [];
      let lastState = lastAvailableState(states);
      if (!lastState) return '#a6a6a6';
      let duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
      if (lastState.state === 'arrived' && duration >= 120) {
        return '#ffc823';
      } else {
        return reportCfg[d.name][definitions.graphMeasure]['stateColors'][lastState.state];
      }
    }).classed('status-badge', true).style('color', function (d) {
      let states = d.measures[definitions.graphMeasure] || [];
      let lastState = lastAvailableState(states);
      let stateColor = lastState ? reportCfg[d.name][definitions.graphMeasure]['stateColors'][lastState.state] : '#eee';
      return stateColor === '#eee' ? 'black' : 'white';
    }).classed('status-badge', true).classed('status-badge', true).classed('flash-low', d => {
      let states = d.measures[definitions.graphMeasure] || [];
      let lastState = lastAvailableState(states);
      if (!lastState) return false;
      let duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
      let cfgs = reportCfg[d.name][definitions.graphMeasure]['alerts'] ? reportCfg[d.name][definitions.graphMeasure]['alerts'][lastState.state] : null;
      return cfgs && cfgs['low'] && cfgs['medium'] ? duration >= cfgs['low']['threshold'] && duration < cfgs['medium']['threshold'] : false;
    }).classed('flash-medium', d => {
      if (test) {
        return true;
      }
      let states = d.measures[definitions.graphMeasure] || [];
      let lastState = lastAvailableState(states);
      if (!lastState) return false;
      let duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
      let cfgs = reportCfg[d.name][definitions.graphMeasure]['alerts'] ? reportCfg[d.name][definitions.graphMeasure]['alerts'][lastState.state] : null;
      return cfgs && cfgs['medium'] && cfgs['high'] ? duration >= cfgs['medium']['threshold'] && duration < cfgs['high']['threshold'] : false;
    }).classed('flash-high', d => {
      let states = d.measures[definitions.graphMeasure] || [];
      let lastState = lastAvailableState(states);
      if (!lastState) return false;
      let duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
      let cfgs = reportCfg[d.name][definitions.graphMeasure]['alerts'] ? reportCfg[d.name][definitions.graphMeasure]['alerts'][lastState.state] : null;
      return cfgs && cfgs['high'] ? duration >= cfgs['high']['threshold'] : false;
    }).style('--delay', d => {
      let states = d.measures[definitions.graphMeasure] || [];
      let lastState = lastAvailableState(states);
      if (!lastState) return false;
      let duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
      let cfgs = reportCfg[d.name][definitions.graphMeasure]['alerts'] ? reportCfg[d.name][definitions.graphMeasure]['alerts'][lastState.state] : null;
      if (cfgs) {
        if (duration >= cfgs['low']['threshold'] && duration < cfgs['medium']['threshold'] && cfgs['low']['delay']) {
          return cfgs['low']['delay'] + 's';
        } else if (duration >= cfgs['medium']['threshold'] && duration < cfgs['high']['threshold'] && cfgs['medium']['delay']) {
          return cfgs['medium']['delay'] + 's';
        } else if (duration >= cfgs['high']['threshold'] && cfgs['high']['delay']) {
          return cfgs['high']['delay'] + 's';
        }
      }
    }).style('--alert-color', d => {
      let states = d.measures[definitions.graphMeasure] || [];
      let lastState = lastAvailableState(states);
      if (!lastState) return false;
      let duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
      let cfgs = reportCfg[d.name][definitions.graphMeasure]['alerts'] ? reportCfg[d.name][definitions.graphMeasure]['alerts'][lastState.state] : null;
      if (cfgs) {
        if (duration >= cfgs['low']['threshold'] && duration < cfgs['medium']['threshold']) {
          return 'rgba(0, 51, 17, 0.96)';
        } else if (duration >= cfgs['medium']['threshold'] && duration < cfgs['high']['threshold']) {
          return 'rgb(230, 153, 0)';
        } else if (duration >= cfgs['high']['threshold']) {
          return 'rgb(230, 0, 0)';
        }
      } else {
        if (test) {
          let colorArr = ['rgba(0, 51, 17, 0.96)', 'rgb(230, 153, 0)', 'rgb(230, 0, 0)'];
          return colorArr[Math.floor(Math.random() * Math.floor(3))];
        }
      }
    }).style('--flashing-speed', d => {
      let states = d.measures[definitions.graphMeasure] || [];
      let lastState = lastAvailableState(states);
      if (!lastState) return false;
      let duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
      let cfgs = reportCfg[d.name][definitions.graphMeasure]['alerts'] ? reportCfg[d.name][definitions.graphMeasure]['alerts'][lastState.state] : null;
      if (cfgs) {
        if (duration >= cfgs['low']['threshold'] && duration < cfgs['medium']['threshold'] && cfgs['low']['flashing_speed']) {
          return cfgs['low']['flashing_speed'] + 's';
        } else if (duration >= cfgs['medium']['threshold'] && duration < cfgs['high']['threshold'] && cfgs['medium']['flashing_speed']) {
          return cfgs['medium']['flashing_speed'] + 's';
        } else if (duration >= cfgs['high']['threshold'] && cfgs['high']['flashing_speed']) {
          return cfgs['high']['flashing_speed'] + 's';
        }
      }
    });

    mergedSubjectDivs.select('div.currentStatus div.status').text(function (d) {
      let states = d.measures[definitions.graphMeasure] || [];
      let lastState = lastAvailableState(states) || undefined;
      if (!lastState) return 'No Data Available';
      return reportCfg[d.name][definitions.graphMeasure]['stateLabels'][lastState.state];
    });

    mergedSubjectDivs.select('div.currentStatus div.duration').text(function (d) {
      let states = d.measures[definitions.graphMeasure] || [];
      let lastState = lastAvailableState(states) || undefined;
      if (!lastState) return '';
      let duration = moment(lastState.endTime).diff(moment(lastState.startTime));
      return formatDuration(duration);
    });

    mergedSubjectDivs.select('div.currentStatus div.updateTime').text(function (d) {
      let states = d.measures[definitions.graphMeasure] || undefined;
      if (!states) return moment(data.highWaterMarkTime).format('h:mm a');
      let lastState = lastAvailableState(states);
      if (!lastState) return moment(data.highWaterMarkTime).format('h:mm a');
      let recordedTime = moment.tz(lastState.endTime, timezone).subtract(data.timeslotSizeMillis, 'milliseconds');
      return 'at ' + recordedTime.format('h:mm a');
    });

    //current image
    let currentImage = mergedSubjectDivs.select('div.currentImage').selectAll('img.currentImage').data(function (d) {
      let states = d.measures[definitions.graphMeasure] || [];
      if (!states.length) return [];
      let lastState = lastAvailableState(states);
      if (!lastState || !lastState.images || !lastState.images.length) return [];
      let lastImage = getLatestImage(lastState, reportCfg[d.name][definitions.graphMeasure].img_source_measure_name);
      if (!lastImage) return [];
      return lastImage.name.endsWith('.mp4') ? [] : [lastImage];
    });
    let newCurrentImage = currentImage.enter().append('img').classed('currentImage', true);
    currentImage.merge(newCurrentImage)
      .classed('lazy', true)
      .attr('data-src', (imageObject) => newImageComponent(parameters.customer, imageObject).toImageURL());
    currentImage.exit().remove();

    //current video
    let currentVideo = mergedSubjectDivs.select('div.currentImage').selectAll('video.currentVideo').data(function (d) {
      let states = d.measures[definitions.graphMeasure] || [];
      if (!states.length) return [];
      let lastState = lastAvailableState(states);
      if (!lastState || !lastState.images || !lastState.images.length) return [];
      let lastImage = getLatestImage(lastState, reportCfg[d.name][definitions.graphMeasure].img_source_measure_name);
      if (!lastImage) return [];
      return lastImage.name.endsWith('.mp4') ? [lastImage] : [];
    }, function (d) {
      return `${d.gateway_id}/${d.camera_id}/${moment(d.collection_time).unix()}/${d.step_name}`;
    });
    let newCurrentVideo = currentVideo.enter().append('video').classed('currentVideo', true)
      .attr('loop', 'loop')
      .attr('muted', 'muted')
      .attr('playsinline', 'playsinline');
    newCurrentVideo.append('source');
    currentVideo.merge(newCurrentVideo)
      .select('source')
      .classed('lazy', true)
      .attr('data-src', (imageObject) => newImageComponent(parameters.customer, imageObject).toImageURL());
    currentVideo.exit().remove();
    //Missing images
    var noImageAvailable = mergedSubjectDivs.select('div.currentImage').selectAll('div.noImg').data(function (d) {
      var states = d.measures[definitions.graphMeasure] || [];
      if (!states.length) return [{}];
      var lastState = lastAvailableState(states);
      if (!lastState || !lastState.images || !lastState.images.length && isConfiguredForImage(d)) {
        return [{}];
      }
      return [];
    });

    noImageAvailable.enter().append('div').classed('noImg', true).text('No Image Available');
    noImageAvailable.exit().remove();
  } else {
    mergedSubjectDivs.select('div.currentImage').html('');
    mergedSubjectDivs.select('div.currentStatus div.status').text('');
    mergedSubjectDivs.select('div.currentStatus div.duration').text('');
    mergedSubjectDivs.select('div.currentStatus div.updateTime').text('');
    mergedSubjectDivs.select('div.currentStatus').style('background-color', null);
    mergedSubjectDivs.select('div.currentStatus').classed('status-badge', false);
  }
}

//Rendering detail report for Kiosk
function renderDetailForKiosk(data, parameters) {
  Messages.config(parameters);
  d3.select('#reportDateTitle div.tz').text(` ${timezone}`);

  //Missing Data-Some message if necessary
  if (!data.subjects.length) {
    renderNoShift(parameters.shift, parameters.date);
  } else {
    d3.select('#message').text('');
  }
  //create the subjects containers
  var subjectName = parameters.subject || latestConfig.config.primarySubject;
  var subjectDivs = d3.select('div#subjects').selectAll('div.subject-container').data(data.subjects);
  var newSubjectDivs = subjectDivs.enter().append('div').classed('subject-container', true);
  let badge = newSubjectDivs.append('div').classed('header1',true);
  let headerElement = newSubjectDivs.append('div').classed('header2',true).attr('id','reportDateTitle');
  headerElement.append('div').classed('report-title', true);
  headerElement.append('div').classed('date', true);
  headerElement.append('div').classed('tz', true);
  headerElement.append('div').classed('liveModeIndicator', true);
  var aggregateDiv = headerElement.append('div').classed('aggregate', true);
  aggregateDiv.append('span').classed('label', true).text(parameters.customer == 'lumenetix' ? 'Total Red + Orange + Blue: ' : parameters.customer == 'gatwickairport' ? 'Average Cycle Time: ' : 'Utilization: ');
  aggregateDiv.append('span').classed('value', true);
  aggregateDiv.append('span').classed('change', true);
  let bars = newSubjectDivs.append('div').classed('report',true);
  bars.append('div').attr('id', 'message');
  if (liveMode) {
    d3.select('#reportDateTitle div.date').text(`Shift for ${moment.tz(new Date(), timezone).format('MMMM Do, YYYY hh:mm a z')}`);
  } else {
    d3.select('#reportDateTitle div.date').text(`Shift for ${moment(parameters.date).format('MMMM Do, YYYY')}`);
  }
  d3.select('#reportDateTitle .tz').text(timezone);
  d3.select('#reportDateTitle .report-title').text(`Station: ${parameters.subject}`);
  if (shiftIsActive() && !isHistoricalDay()) {
    let statusBadgeDiv = badge.append('div').classed('status-badge-div', true);
    let currentStatus = statusBadgeDiv.append('div').classed('currentStatus', true);
    currentStatus.append('div').classed('duration', true);
    currentStatus.append('div').classed('status', true);
    currentStatus.append('div').classed('updateTime', true);
  }


  let timeScaleDiv = bars.append('div').classed('scale', true).classed('container', true);
  let scaleRow = timeScaleDiv.append('div').classed('row', true);
  let svgCol = scaleRow.append('div').classed('col-9', true).classed('scaleSvg', true);
  svgCol.append('svg').classed('statusTimeline', true).classed('scaleTimeline', true);
  bars.append('div').classed('statusTimelines', true);

  var mergedSubjectDivs = newSubjectDivs.merge(subjectDivs);
  subjectDivs.exit().remove();

  //create the scale
  var xAxis = d3.scaleTime().domain([moment(data.shiftStartTime).valueOf(), moment(data.shiftEndTime).valueOf()]).range([barMarginLeft, barWidth + barMarginLeft]);
  var timeTicks = xAxis.ticks(12).filter(function (t) {
    return t.getMinutes() == 0;
  });
  // var scaleDiv = d3.select('svg.scaleTimeline');
  var scaleSvg = d3.select('svg.scaleTimeline')
    .attr('height', 35)
    .attr('width', barWidth + barMarginLeft + barMarginRight)
    .attr('viewBox', `0 0 ${barWidth + barMarginLeft + barMarginRight} ${35}`);
  var scaleTicks = scaleSvg.selectAll('line.tick').data(timeTicks);
  scaleTicks.enter().append('line').classed('tick', true)
    .attr('stroke', '#ccc')
    .attr('stroke-width', 1)
    .attr('y1', 25)
    .attr('y2', 32)
    .merge(scaleTicks)
    .attr('x1', xAxis)
    .attr('x2', xAxis);
  var scaleLabels = scaleSvg.selectAll('text.tickLabel').data(timeTicks);

  const newDateFormatting = function () {
    let previousDate = '';

    return function (currentDate, index) {
      let currentDateString = '';

      if (index > 0 && (moment.tz(currentDate, timezone).format('MM/DD') !== moment.tz(previousDate, timezone).format('MM/DD'))) {
        currentDateString = moment.tz(currentDate, timezone).format('MM/DD');
      }

      previousDate = currentDate;

      return moment.tz(currentDate, timezone).format('HH:mm') + ' ' + currentDateString;
    };
  };

  scaleLabels.enter().append('text').classed('tickLabel', true)
    .attr('y', 8)
    .attr('dy', 10)
    .attr('fill', '#bbb')
    .attr('text-anchor', 'middle')
    .merge(scaleLabels)
    .attr('x', xAxis)
    .text(newDateFormatting());

  d3.selectAll('.tickLabel').call(wrap, 10);
  scaleLabels.exit().remove();

  //the timeline bar
  var timelines = mergedSubjectDivs.select('div.statusTimelines').selectAll('div.statusTimeline-row').data(function (d) {
    return Object.keys(d.measures).map(key => {
      return {name: key, values: d.measures[key], subject: d.name};
    }).sort((a, b) => {
      if (a.name == definitions.graphMeasure && b.name != definitions.graphMeasure) return -1;
      if (b.name == definitions.graphMeasure && a.name != definitions.graphMeasure) return 1;
      if (a.name == b.name) return 0;
      if (reportCfg[a.subject][a.name].priority) {
        if (reportCfg[b.subject][b.name].priority) {
          if (reportCfg[a.subject][a.name].priority < reportCfg[b.subject][b.name].priority) {
            return -1;
          } else {
            return 1;
          }
        }
        return -1;
      } else if (reportCfg[b.subject][b.name].priority) {
        return 1;
      }
      return a.name > b.name ? 1 : -1;
    });
  });
  var timelinesEnter = timelines.enter().append('div').classed('statusTimeline-row', true).classed('container', true);
  let label = timelinesEnter.append('div').classed('row', true).classed('measureLabel', true);
  let svgBar = timelinesEnter.append('div').classed('row', true).classed('svgBar', true);
  let legendRow = timelinesEnter.append('div').classed('row', true).classed('legendRow', true);
  let labelCol = label.append('div').classed('col-11', true);

  labelCol.append('div').classed('timelineLabel', true).text(({ name }) => Messages.transpose({ key: name }));
  // let logoCol = svgBar.append('div').classed('col-1', true);
  // let imgLogo = logoCol.append('div').classed('logos', true);
  // imgLogo.append('img').classed('logo', true).attr('src', (d) => {
  //   return `/portal/images/measureIcons/${reportCfg[d.subject][d.name]['imgName']}`;
  // });
  let svgBarCol = svgBar.append('div').classed('col-11', true);
  var newTimelines = svgBarCol.append('svg').classed('statusTimeline', true)
    .attr('height', barHeight + barMarginTop + barMarginBottom)
    .attr('width', barWidth + barMarginLeft + barMarginRight)
    .attr('viewBox', `0 0 ${barWidth + barMarginLeft + barMarginRight} ${barHeight + barMarginTop + barMarginBottom}`);
  newTimelines.append('g').classed('ticks', true);
  const measureGroup = newTimelines.append('g').classed('measures', true);
  measureGroup.append('rect')
    .attr('fill', '#eee')
    .attr('x', barMarginLeft)
    .attr('y', barMarginTop)
    .attr('width', barWidth)
    .attr('height', barHeight)
    .on('mouseover', function (d) {
      if(!d.values.length){
        tooltip.transition()
          .duration(200)
          .style('opacity', .9);
        let downGw = reportCfg[subjectName][d.name]['gwName'];
        tooltip.html(`Missing gateway-subject name ${downGw}`)
          .style('left', (d3.event.pageX - 50) + 'px')
          .style('top', (d3.event.pageY - 28) + 'px');
      }
    })
    .on('mouseout', function (d) {
      if(!d.values.length){
        tooltip.transition()
          .duration(500)
          .style('opacity', 0);
      }
    });
  let legendCol = legendRow.append('div').classed('col-11', true);
  legendCol.append('div').classed('legend', true);
  var mergedTimelines = timelinesEnter.merge(timelines);
  timelines.exit().remove();

  var ticks = mergedTimelines.select('svg.statusTimeline g.ticks').selectAll('line.tick').data(timeTicks);
  ticks.enter().append('line')
    .classed('tick', true)
    .attr('stroke', '#ccc')
    .attr('stroke-width', 1)
    .attr('y1', 0)
    .attr('y2', barHeight + barMarginTop + barMarginBottom)
    .merge(ticks)
    .attr('x1', x)
    .attr('x2', x);

  var eventMarks = mergedTimelines.select('svg.statusTimeline g.measures').selectAll('rect.mark').data(function (d) {
    return d.values.map(v => {
      return {
        subject: d.subject,
        measure: d.name,
        state: v.state,
        missingMeasures: v.missingMeasures,
        startTime: v.startTime,
        endTime: v.endTime,
        images: v.images,
        turn: v.turn,
        centroids: v.centroids || null,
        violations: v.violations || null,
        customer_name: v.customer_name,
        unit_number: v.unit_number
      };
    });
  });
  var newMarks = eventMarks.enter().append('rect')
    .classed('mark', true)
    .attr('y', barMarginTop)
    .attr('height', barHeight);
  newMarks.append('title');

  const theColorProvider = newColorProvider(reportCfg);
  const theWidthCalculator = newWidthCalculator(xAxis, reportCfg);
  var mergedMarks = newMarks.merge(eventMarks)
    .attr('fill', theColorProvider)
    .attr('x', ({ startTime }) => xAxis(moment(startTime).valueOf()))
    .attr('width', theWidthCalculator)
    .classed('flashing', hasFlashingState)
    .classed('imageBacked', function (d) {
      return d.images && d.images.length;
    })
    .on('mouseenter', function (d) {
      d3.select(this).raise();
    })
    .on('mouseover', function (d) {
      tooltip.transition()
        .duration(200)
        .style('opacity', .9);
      let start = moment.tz(d.startTime, timezone).format('hh:mm a');
      let end = moment.tz(d.endTime, timezone).format('hh:mm a');
      let missingMeasures = '';
      if (d.missingMeasures.length > 0) {
        missingMeasures = '=> ' + d.missingMeasures.join(', ');
      }
      tooltip.html(reportCfg[d.subject][d.measure]['stateLabels'][d.state] + missingMeasures + ' : ' + start + ' - ' + end)
        .style('left', (d3.event.pageX - 50) + 'px')
        .style('top', (d3.event.pageY - 28) + 'px');
      zoom.enter(this);
    })
    .on('mouseout', function (d) {
      tooltip.transition()
        .duration(500)
        .style('opacity', 0);
      zoom.exit(this);
    })
    .on('click', function (d) {
      if(d.measure === definitions.graphMeasure && d.turn && Object.keys(d.turn).length) {
        d.turn.turnStartTime = moment.tz(d.turn.turnStartTime,timezone).format('YYYY-MM-DD HH:mm:ss');
        d.turn.turnEndTime = moment.tz(d.turn.turnEndTime,timezone).format('YYYY-MM-DD HH:mm:ss');
        window.location.href = `/portal/reports/gateTurn?customer=${parameters.customer}&facility=${parameters.facility}&subject=${d.subject}&start=${d.turn.turnStartTime}&end=${d.turn.turnEndTime}`;
        return;
      }
      if (!d.images.length) return;
      $('#imgModal').modal({backdrop: 'static'});
      let interval = moment(d.startTime.valueOf()).format('HH:mm') + ' - ' + moment(d.endTime.valueOf()).format('HH:mm');
      let measureState = d.measure + ' : ' + d.state;
      let intervalDiv = d3.select('div.interval');
      let subjectDiv = d3.select('div.subject-name');
      let stateDiv = d3.select('div.measure-state');
      intervalDiv.selectAll('span').remove();
      subjectDiv.selectAll('span').remove();
      stateDiv.selectAll('span').remove();
      let newIntervalDiv = intervalDiv.append('span').text(interval);
      let newSubjectDiv = subjectDiv.append('span').text(d.subject);
      let newStateDiv = stateDiv.append('span').text(measureState);
      newIntervalDiv.merge(intervalDiv);
      newSubjectDiv.merge(subjectDiv);
      newStateDiv.merge(stateDiv);
      let lastImg = d.images.length ? d.images[d.images.length - 1] : undefined;
      if (lastImg === 'undefined') {
        return;
      }

      let listOfImages = [];
      //decimate images as per configured imgInterval
      const { measure } = d;
      let images = d.images.filter(
        (imageRef, index, array) => {
          if (index == 0 || index == array.length - 1) {
            return true;
          }
          if (typeof reportCfg[d.subject][d.measure]['imgInterval'] === 'number') {
            return index % reportCfg[d.subject][d.measure]['imgInterval'] == 0;
          } else {
            return (index % (reportCfg[d.subject][d.measure]['imgInterval'][d.state] || 1 )) == 0;
          }
        });
      images.forEach(function (imageObject) {
        listOfImages.push({
          linkName: moment.tz(imageObject.collection_time, timezone).format('HH:mm:ss'),
          imgKey: imageObject,
          url: newImageComponent(parameters.customer, imageObject).toImageURL(),
          header: `${upperFirst(d.measure)}: ${reportCfg[d.subject][d.measure]['stateLabels'][d.state] || upperFirst(d.state)}`,
          info: getAdditionalInfo(d, imageObject),
          customerTag: {
            tagArr: [],
            subject: d.state
          }
        });
      });
      renderImgModal(listOfImages, d);
    });

  mergedMarks.select('title').text(function (d) {
    return d.state + ': ' + moment.tz(d.startTime, timezone).format('hh:mm a') + ' - ' + moment.tz(d.endTime, timezone).format('hh:mm a');
  });

  eventMarks.exit().remove();

  //the legend
  Legends.render(reportCfg, data, subjectName, mergedTimelines, { States, mergeUtilization });

  //overall utilization
  mergedSubjectDivs.select('div.aggregate span.label').text(d => {
    return d.aggregates.utilization ? (parameters.customer === 'lumenetix' ? 'Total Red + Orange + Blue : ' : 'Utilization: ') : '';
  });
  mergedSubjectDivs.select('div.aggregate span.value').text(function (d) {return insertOverallUtilization(d, data);});


  //current state
  if (shiftIsActive()) {
    mergedSubjectDivs.select('div.currentStatus')
      .style('background-color', function (d) {
        var states = d.measures[definitions.graphMeasure] || [];
        var lastState = lastAvailableState(states) || undefined;
        if (!lastState) return '#a6a6a6';
        if(lastState.state === 'arrived' && duration >= 120) {
          return '#ffc823';
        } else {
          return reportCfg[subjectName][definitions.graphMeasure]['stateColors'][lastState.state] || '#a6a6a6';
        }
      })
      .style('color', function (d) {
        let states = d.measures[definitions.graphMeasure] || [];
        var lastState = lastAvailableState(states);
        let stateColor = lastState ? reportCfg[d.name][definitions.graphMeasure]['stateColors'][lastState.state] : '#eee';
        return stateColor === '#eee' ? 'black' : 'white';
      })
      .classed('status-badge', true)
      .classed('flash-low', d => {
        var states = d.measures[definitions.graphMeasure] || [];
        let lastState = lastAvailableState(states) || undefined;
        if (!lastState) return false;
        var duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
        let cfgs = reportCfg[d.name][definitions.graphMeasure]['alerts'] ? reportCfg[d.name][definitions.graphMeasure]['alerts'][lastState.state] : null;
        return cfgs && cfgs['low'] && cfgs['medium'] ? duration >= cfgs['low']['threshold'] && duration < cfgs['medium']['threshold'] : false;
      })
      .classed('flash-medium', d => {
        if (test) {
          return true;
        }
        var states = d.measures[definitions.graphMeasure] || [];
        var lastState = lastAvailableState(states) || undefined;
        if (!lastState) return false;
        var duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
        const graphMeasure = reportCfg[d.name][definitions.graphMeasure];
        let cfgs = (graphMeasure && graphMeasure['alerts']) ? graphMeasure['alerts'][lastState.state] : null;
        return cfgs && cfgs['medium'] && cfgs['high'] ? duration >= cfgs['medium']['threshold'] && duration < cfgs['high']['threshold'] : false;
      })
      .classed('flash-high', d => {
        var states = d.measures[definitions.graphMeasure] || [];
        var lastState = lastAvailableState(states) || undefined;
        if (!lastState) return false;
        var duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
        const graphMeasure = reportCfg[d.name][definitions.graphMeasure];
        let cfgs = (graphMeasure && graphMeasure['alerts']) ? graphMeasure['alerts'][lastState.state] : null;
        return cfgs && cfgs['high'] ? duration >= cfgs['high']['threshold'] : false;
      })
      .style('--delay', d => {
        var states = d.measures[definitions.graphMeasure] || [];
        var lastState = lastAvailableState(states);
        if (!lastState) return false;
        var duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
        const graphMeasure = reportCfg[d.name][definitions.graphMeasure];
        let cfgs = (graphMeasure && graphMeasure['alerts']) ? graphMeasure['alerts'][lastState.state] : null;
        if (cfgs) {
          if (duration >= cfgs['low']['threshold'] && duration < cfgs['medium']['threshold'] && cfgs['low']['delay']) {
            return cfgs['low']['delay'] + 's';
          } else if (duration >= cfgs['medium']['threshold'] && duration < cfgs['high']['threshold'] && cfgs['medium']['delay']) {
            return cfgs['medium']['delay'] + 's';
          } else if (duration >= cfgs['high']['threshold'] && cfgs['high']['delay']) {
            return cfgs['high']['delay'] + 's';
          }
        }
      })
      .style('--alert-color', d => {
        var states = d.measures[definitions.graphMeasure] || [];
        var lastState = lastAvailableState(states);
        if (!lastState) return false;
        var duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
        const graphMeasure = reportCfg[d.name][definitions.graphMeasure];
        let cfgs = (graphMeasure && graphMeasure['alerts']) ? graphMeasure['alerts'][lastState.state] : null;
        if (cfgs) {
          if (duration >= cfgs['low']['threshold'] && duration < cfgs['medium']['threshold']) {
            return 'rgba(0, 51, 17, 0.96)';
          } else if (duration >= cfgs['medium']['threshold'] && duration < cfgs['high']['threshold']) {
            return 'rgb(230, 153, 0)';
          } else if (duration >= cfgs['high']['threshold']) {
            return 'rgb(230, 0, 0)';
          }
        } else {
          if (test) {
            let colorArr = ['rgba(0, 51, 17, 0.96)', 'rgb(230, 153, 0)', 'rgb(230, 0, 0)'];
            return colorArr[Math.floor(Math.random() * Math.floor(3))];
          }
        }
      })
      .style('--flashing-speed', d => {
        var states = d.measures[definitions.graphMeasure] || [];
        var lastState = lastAvailableState(states);
        if (!lastState) return false;
        var duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
        const graphMeasure = reportCfg[d.name][definitions.graphMeasure];
        let cfgs = (graphMeasure && graphMeasure['alerts']) ? graphMeasure['alerts'][lastState.state] : null;
        if (cfgs) {
          if (duration >= cfgs['low']['threshold'] && duration < cfgs['medium']['threshold'] && cfgs['low']['flashing_speed']) {
            return cfgs['low']['flashing_speed'] + 's';
          } else if (duration >= cfgs['medium']['threshold'] && duration < cfgs['high']['threshold'] && cfgs['medium']['flashing_speed']) {
            return cfgs['medium']['flashing_speed'] + 's';
          } else if (duration >= cfgs['high']['threshold'] && cfgs['high']['flashing_speed']) {
            return cfgs['high']['flashing_speed'] + 's';
          }
        }
      });

    mergedSubjectDivs.select('div.currentStatus div.status').text(function (d) {
      var states = d.measures[definitions.graphMeasure] || [];
      var lastState = lastAvailableState(states);
      if (!lastState) return 'No Data Available';
      return reportCfg[d.name][definitions.graphMeasure]['stateLabels'][lastState.state];
    });

    mergedSubjectDivs.select('div.currentStatus div.duration').text(function (d) {
      var states = d.measures[definitions.graphMeasure];
      if (!states) return '';
      var lastState = lastAvailableState(states);
      if (!lastState) return formatDuration(0);
      var duration = moment(lastState.endTime).diff(moment(lastState.startTime));
      return formatDuration(duration);
    });

    mergedSubjectDivs.select('div.currentStatus div.updateTime').text(function (d) {
      var states = d.measures[definitions.graphMeasure];
      if (!states) return moment(data.highWaterMarkTime).format('h:mm a');
      var lastState = lastAvailableState(states);
      if (!lastState) return moment(data.highWaterMarkTime).format('h:mm a');
      var recordedTime = moment.tz(lastState.endTime, timezone).subtract(data.timeslotSizeMillis, 'milliseconds');
      return 'at ' + recordedTime.format('h:mm a');
    });
  }
  if (!reportCfg[parameters.subject]['composite']) {
    d3.select('.status-badge').remove();
    return;
  }
  subjectDivs.exit().remove();
}

function renderDetail(data, parameters) {
  Messages.config(parameters);

  //append timezone text to report title
  d3.select('#reportDateTitle div.tz').text(` ${timezone}`);

  //Missing Data-Some message if necessary
  if (!data.subjects.length) {
    renderNoShift(parameters.shift, parameters.date);
  } else {
    d3.select('#message').text('');
  }
  //create the subjects containers
  var subjectName = parameters.subject || latestConfig.config.primarySubject;
  var subjectDivs = d3.select('div#subjects').selectAll('div.subject').data(data.subjects);
  var newSubjectDivs = subjectDivs.enter().append('div').classed('subject', true);
  newSubjectDivs.append('div').classed('title', true);

  var aggregateDiv = newSubjectDivs.append('div').classed('aggregate', true);
  aggregateDiv.append('span').classed('label', true).text(parameters.customer == 'lumenetix' ? 'Total Red + Orange + Blue: ' : parameters.customer == 'gatwickairport' ? 'Average Cycle Time: ' : 'Utilization: ');
  let metrics = aggregateDiv.append('div').classed('metrics', true);
  metrics.append('span').classed('count', true);
  metrics.append('span').classed('duration', true);
  metrics.append('span').classed('percentage', true);
  aggregateDiv.append('span').classed('change', true);


  let timeScaleDiv = newSubjectDivs.append('div').classed('scale', true).classed('container', true);
  let scaleRow = timeScaleDiv.append('div').classed('row', true);
  scaleRow.append('div').classed('col-1', true);
  let svgCol = scaleRow.append('div').classed('col-9', true).classed('scaleSvg', true);
  svgCol.append('svg').classed('statusTimeline', true).classed('scaleTimeline', true);
  newSubjectDivs.append('div').classed('statusTimelines', true);
  if (shiftIsActive()) {
    var currentStatus = newSubjectDivs.append('div').classed('currentStatus', true);
    currentStatus.append('div').classed('duration', true);
    currentStatus.append('div').classed('status', true);
    currentStatus.append('div').classed('updateTime', true);
  }
  var mergedSubjectDivs = newSubjectDivs.merge(subjectDivs);
  mergedSubjectDivs.select('div.title').text(({name}) => name);
  subjectDivs.exit().remove();

  //create the scale
  var xAxis = d3.scaleTime().domain([
    moment(data.shiftStartTime).valueOf(),
    moment(data.shiftEndTime).valueOf()
  ]).range([barMarginLeft, barWidth + barMarginLeft]);
  var timeTicks = xAxis.ticks(12).filter((t) => t.getMinutes() == 0);

  var scaleSvg = d3.select('svg.scaleTimeline')
    .attr('height', 35)
    .attr('width', barWidth + barMarginLeft + barMarginRight)
    .attr('viewBox', `0 0 ${barWidth + barMarginLeft + barMarginRight} ${35}`);

  var scaleTicks = scaleSvg.selectAll('line.tick').data(timeTicks);
  scaleTicks.enter().append('line').classed('tick', true)
    .attr('stroke', '#ccc')
    .attr('stroke-width', 1)
    .attr('y1', 25)
    .attr('y2', 32)
    .merge(scaleTicks)
    .attr('x1', xAxis)
    .attr('x2', xAxis);

  var scaleLabels = scaleSvg.selectAll('text.tickLabel').data(timeTicks);
  scaleLabels.enter().append('text').classed('tickLabel', true)
    .attr('y', 8)
    .attr('dy', 10)
    .attr('fill', '#bbb')
    .attr('text-anchor', 'middle')
    .merge(scaleLabels)
    .attr('x', xAxis)
    .text(function (d, i) {
      // return moment.tz(d, timezone).format('HH:mm');
      let dt = '';
      if (i > 0 && (moment.tz(d, timezone).format('MM/DD') !== moment.tz(prevDt, timezone).format('MM/DD'))) {
        dt = moment.tz(d, timezone).format('MM/DD');
      }
      prevDt = d;
      return moment.tz(d, timezone).format('HH:mm') + ' ' + dt;
    });

  d3.selectAll('.tickLabel').call(wrap, 10);
  scaleLabels.exit().remove();

  //the timeline bar
  var timelines = mergedSubjectDivs.select('div.statusTimelines').selectAll('div.statusTimeline-row').data(function (d) {
    return Object.keys(d.measures).map(key => {
      return {name: key, values: d.measures[key], subject: d.name};
    }).sort((a, b) => {
      if (a.name == definitions.graphMeasure && b.name != definitions.graphMeasure) return -1;
      if (b.name == definitions.graphMeasure && a.name != definitions.graphMeasure) return 1;
      if (a.name == b.name) return 0;
      if (reportCfg[a.subject][a.name].priority) {
        if (reportCfg[b.subject][b.name].priority) {
          if (reportCfg[a.subject][a.name].priority < reportCfg[b.subject][b.name].priority) {
            return -1;
          } else {
            return 1;
          }
        }
        return -1;
      } else if (reportCfg[b.subject][b.name].priority) {
        return 1;
      }
      return a.name > b.name ? 1 : -1;
    });
  });
  var timelinesEnter = timelines.enter().append('div').classed('statusTimeline-row', true).classed('container', true);
  let label = timelinesEnter.append('div').classed('row', true).classed('measureLabel', true);
  let svgBar = timelinesEnter.append('div').classed('row', true).classed('svgBar', true);
  let legendRow = timelinesEnter.append('div').classed('row', true).classed('legendRow', true);

  label.append('div').classed('col-1', true);
  let labelCol = label.append('div').classed('col-11', true);

  labelCol.append('div').classed('timelineLabel', true).text(({ name }) => Messages.transpose({ key: name }));

  let logoCol = svgBar.append('div').classed('col-1', true);
  let imgLogo = logoCol.append('div').classed('logos', true);
  imgLogo.append('img').classed('logo', true).attr('src', (d) => {
    return `/portal/images/measureIcons/${reportCfg[d.subject][d.name]['imgName']}`;
  });
  let svgBarCol = svgBar.append('div').classed('col-11', true);
  var newTimelines = svgBarCol.append('svg').classed('statusTimeline', true)
    .attr('height', barHeight + barMarginTop + barMarginBottom)
    .attr('width', barWidth + barMarginLeft + barMarginRight)
    .attr('viewBox', `0 0 ${barWidth + barMarginLeft + barMarginRight} ${barHeight + barMarginTop + barMarginBottom}`);
  newTimelines.append('g').classed('ticks', true);
  const measureGroup = newTimelines.append('g').classed('measures', true);
  measureGroup.append('rect')
    .attr('fill', '#eee')
    .attr('x', barMarginLeft)
    .attr('y', barMarginTop)
    .attr('width', barWidth)
    .attr('height', barHeight)
    .on('mouseover', function (d) {
      if (!d.values.length) {
        tooltip.transition()
          .duration(200)
          .style('opacity', .9);
        let downGw = reportCfg[subjectName][d.name]['gwName'];
        tooltip.html(`Missing gateway-subject name ${downGw}`)
          .style('left', (d3.event.pageX - 50) + 'px')
          .style('top', (d3.event.pageY - 28) + 'px');
      }
    })
    .on('mouseout', function (d) {
      if (!d.values.length) {
        tooltip.transition()
          .duration(500)
          .style('opacity', 0);
      }
    });
  legendRow.append('div').classed('col-1', true);
  let legendCol = legendRow.append('div').classed('col-11', true);
  legendCol.append('div').classed('legend', true);
  var mergedTimelines = timelinesEnter.merge(timelines);
  timelines.exit().remove();

  var ticks = mergedTimelines.select('svg.statusTimeline g.ticks').selectAll('line.tick').data(timeTicks);
  ticks.enter().append('line')
    .classed('tick', true)
    .attr('stroke', '#ccc')
    .attr('stroke-width', 1)
    .attr('y1', 0)
    .attr('y2', barHeight + barMarginTop + barMarginBottom)
    .merge(ticks)
    .attr('x1', xAxis)
    .attr('x2', xAxis);

  var eventMarks = mergedTimelines.select('svg.statusTimeline g.measures').selectAll('rect.mark').data(function (d) {
    return d.values.map(v => {
      return {
        subject: d.subject,
        measure: d.name,
        state: v.state,
        missingMeasures: v.missingMeasures,
        startTime: v.startTime,
        endTime: v.endTime,
        images: v.images,
        turn: v.turn,
        centroids: v.centroids || null,
        violations: v.violations || null,
        customer_name: v.customer_name,
        unit_number: v.unit_number
      };
    });
  });
  var newMarks = eventMarks.enter().append('rect')
    .classed('mark', true)
    .attr('y', barMarginTop)
    .attr('height', barHeight);
  newMarks.append('title');

  const theColorProvider = newColorProvider(reportCfg);
  const theWidthCalculator = newWidthCalculator(xAxis, reportCfg);
  newMarks.merge(eventMarks)
    .attr('fill', theColorProvider)
    .attr('x', function (d) {
      if (INTERESTING_STATES.includes(d.state)) {
        d3.select(this).raise();
      }
      return xAxis(moment(d.startTime).valueOf());
    })
    .attr('width', theWidthCalculator)
    .classed('flashing', hasFlashingState)
    .classed('imageBacked', function (d) {
      return d.images && d.images.length;
    })
    .on('mouseover', function (d) {
      tooltip.transition()
        .duration(200)
        .style('opacity', .9);
      let start = moment.tz(d.startTime, timezone).format('hh:mm a');
      let end = moment.tz(d.endTime, timezone).format('hh:mm a');
      let missingMeasures = '';
      if (d.missingMeasures.length > 0) {
        missingMeasures = '=> ' + d.missingMeasures.join(', ');
      }
      tooltip.html((reportCfg[d.subject][d.measure]['stateLabels'][d.state] || d.state) + missingMeasures + ' : ' + start + ' - ' + end)
        .style('left', (d3.event.pageX - 50) + 'px')
        .style('top', (d3.event.pageY - 28) + 'px');
      zoom.enter(this);
    })
    .on('mouseout', function (d) {
      tooltip.transition()
        .duration(500)
        .style('opacity', 0);
      zoom.exit(this);
    })
    .on('click', function (d) {

      if (d.measure === definitions.graphMeasure && d.turn && Object.keys(d.turn).length) {
        d.turn.turnStartTime = moment.tz(d.turn.turnStartTime, timezone).format('YYYY-MM-DD HH:mm:ss');
        d.turn.turnEndTime = moment.tz(d.turn.turnEndTime, timezone).format('YYYY-MM-DD HH:mm:ss');
        window.location.href = `/portal/reports/gateTurn?customer=${parameters.customer}&facility=${parameters.facility}&subject=${d.subject}&start=${d.turn.turnStartTime}&end=${d.turn.turnEndTime}`;
        return;
      }
      if (!d.images.length) return;
      $('#imgModal').modal({backdrop: 'static'});
      let interval = moment(d.startTime.valueOf()).format('HH:mm') + ' - ' + moment(d.endTime.valueOf()).format('HH:mm');
      let measureState = d.measure + ' : ' + d.state;
      let intervalDiv = d3.select('div.interval');
      let subjectDiv = d3.select('div.subject-name');
      let stateDiv = d3.select('div.measure-state');
      intervalDiv.selectAll('span').remove();
      subjectDiv.selectAll('span').remove();
      stateDiv.selectAll('span').remove();
      let newIntervalDiv = intervalDiv.append('span').text(interval);
      let newSubjectDiv = subjectDiv.append('span').text(d.subject);
      let newStateDiv = stateDiv.append('span').text(measureState);
      newIntervalDiv.merge(intervalDiv);
      newSubjectDiv.merge(subjectDiv);
      newStateDiv.merge(stateDiv);
      let lastImg = d.images.length ? d.images[d.images.length - 1] : undefined;
      if (lastImg === 'undefined') {
        return;
      }

      let listOfImages = [];
      //decimate images as per configured imgInterval
      let measureImgMap = d.images.reduce((measureMap, img) => {
        if(!measureMap[img.measure]) {
          measureMap[img.measure] = [];
        }
        measureMap[img.measure].push(img);
        return measureMap;
      }, {});

      let images = [];
      Object.keys(measureImgMap).forEach(m => {
        let filteredImages = measureImgMap[m].filter(
          (imageRef, index, array) => {
            if (index == 0 || index == array.length - 1) {
              return true;
            }
            if (typeof reportCfg[d.subject][d.measure]['imgInterval'] === 'number') {
              return index % reportCfg[d.subject][d.measure]['imgInterval'] == 0;
            } else {
              return (index % (reportCfg[d.subject][d.measure]['imgInterval'][d.state] || 1 )) == 0;
            }
          });
        images = images.concat(filteredImages);
      });

      images.forEach(function (imageObject) {
        listOfImages.push({
          linkName: moment.tz(imageObject.collection_time, timezone).format('HH:mm:ss'),
          imgKey: imageObject,
          url: newImageComponent(parameters.customer, imageObject).toImageURL(),
          header: `${upperFirst(d.measure)}: ${reportCfg[d.subject][d.measure]['stateLabels'][d.state] || upperFirst(d.state)}`,
          info: getAdditionalInfo(d, imageObject),
          customerTag: {
            tagArr: [],
            subject: d.state
          }
        });
      });
      renderImgModal(listOfImages, d);
    });

  eventMarks.exit().remove();

  //the legend
  Legends.render(reportCfg, data, subjectName, mergedTimelines, { States, mergeUtilization });

  //overall utilization
  mergedSubjectDivs.select('div.aggregate span.label').text(d => {
    return d.aggregates.utilization ? (parameters.customer === 'lumenetix' ? 'Total Red + Orange + Blue : ' : 'Utilization: ') : '';
  });

  //overall utilization
  mergedSubjectDivs.select('div.metrics span.count').text(function (d) {
    if (d.aggregates.utilization == null) {
      return null;
    }
    let utilization = d.aggregates.utilization || 0;
    return `${utilization.activityCount},`;
  });
  mergedSubjectDivs.select('div.metrics span.duration').text(function (d) {
    if (d.aggregates.utilization == null) {
      return null;
    }
    let utilization = d.aggregates.utilization || 0;
    return ` ${ utilization.duration}`;
  });
  mergedSubjectDivs.select('div.metrics span.percentage').text(function (d) {
    if (d.aggregates.utilization == null) {
      return null;
    }
    let utilization = d.aggregates.utilization || 0;
    if (utilization.percentage !== null){
      return `, ${ utilization.percentage}`;
    } else {
      return null;
    }
  });

  //current state
  if (shiftIsActive() && !isHistoricalDay()) {
    mergedSubjectDivs.select('div.currentStatus').style('background-color', function (d) {
      var states = d.measures[definitions.graphMeasure] || [];
      var lastState = lastAvailableState(states);
      if (!lastState) return '#a6a6a6';
      if (lastState.state === 'arrived' && duration >= 120) {
        return '#ffc823';
      } else {
        return reportCfg[subjectName][definitions.graphMeasure]['stateColors'][lastState.state];
      }
    }).style('color', function (d) {
      let states = d.measures[definitions.graphMeasure] || [];
      var lastState = lastAvailableState(states) || undefined;
      if (!lastState) return '#eee';
      let stateColor = reportCfg[d.name][definitions.graphMeasure]['stateColors'][lastState.state];
      return stateColor === '#eee' ? 'black' : 'white';
    }).classed('status-badge', true)
      .classed('flash-low', d => {
        var states = d.measures[definitions.graphMeasure] || [];
        let lastState = lastAvailableState(states) || undefined;
        if (!lastState) return false;
        var duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
        let cfgs = reportCfg[d.name][definitions.graphMeasure]['alerts'] ? reportCfg[d.name][definitions.graphMeasure]['alerts'][lastState.state] : null;
        return cfgs && cfgs['low'] && cfgs['medium'] ? duration >= cfgs['low']['threshold'] && duration < cfgs['medium']['threshold'] : false;
      })
      .classed('flash-medium', d => {
        if (test) {
          return true;
        }
        var states = d.measures[definitions.graphMeasure] || [];
        var lastState = lastAvailableState(states) || undefined;
        if (!lastState) return false;
        var duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
        const graphMeasure = reportCfg[d.name][definitions.graphMeasure];
        let cfgs = (graphMeasure && graphMeasure['alerts']) ? graphMeasure['alerts'][lastState.state] : null;
        return cfgs && cfgs['medium'] && cfgs['high'] ? duration >= cfgs['medium']['threshold'] && duration < cfgs['high']['threshold'] : false;
      }).classed('flash-high', d => {
        var states = d.measures[definitions.graphMeasure] || [];
        var lastState = lastAvailableState(states) || undefined;
        if (!lastState) return false;
        var duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
        const graphMeasure = reportCfg[d.name][definitions.graphMeasure];
        let cfgs = (graphMeasure && graphMeasure['alerts']) ? graphMeasure['alerts'][lastState.state] : null;
        return cfgs && cfgs['high'] ? duration >= cfgs['high']['threshold'] : false;
      }).style('--delay', d => {
        var states = d.measures[definitions.graphMeasure] || [];
        var lastState = lastAvailableState(states) || undefined;
        if (!lastState) return false;
        var duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
        const graphMeasure = reportCfg[d.name][definitions.graphMeasure];
        let cfgs = (graphMeasure && graphMeasure['alerts']) ? graphMeasure['alerts'][lastState.state] : null;
        if (cfgs) {
          if (duration >= cfgs['low']['threshold'] && duration < cfgs['medium']['threshold'] && cfgs['low']['delay']) {
            return cfgs['low']['delay'] + 's';
          } else if (duration >= cfgs['medium']['threshold'] && duration < cfgs['high']['threshold'] && cfgs['medium']['delay']) {
            return cfgs['medium']['delay'] + 's';
          } else if (duration >= cfgs['high']['threshold'] && cfgs['high']['delay']) {
            return cfgs['high']['delay'] + 's';
          }
        }
      }).style('--alert-color', d => {
        var states = d.measures[definitions.graphMeasure] || [];
        var lastState = lastAvailableState(states);
        if (!lastState) return false;
        var duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
        const graphMeasure = reportCfg[d.name][definitions.graphMeasure];
        let cfgs = (graphMeasure && graphMeasure['alerts']) ? graphMeasure['alerts'][lastState.state] : null;
        if (cfgs) {
          if (duration >= cfgs['low']['threshold'] && duration < cfgs['medium']['threshold']) {
            return 'rgba(0, 51, 17, 0.96)';
          } else if (duration >= cfgs['medium']['threshold'] && duration < cfgs['high']['threshold']) {
            return 'rgb(230, 153, 0)';
          } else if (duration >= cfgs['high']['threshold']) {
            return 'rgb(230, 0, 0)';
          }
        } else {
          if (test) {
            let colorArr = ['rgba(0, 51, 17, 0.96)', 'rgb(230, 153, 0)', 'rgb(230, 0, 0)'];
            return colorArr[Math.floor(Math.random() * Math.floor(3))];
          }
        }
      }).style('--flashing-speed', d => {
        var states = d.measures[definitions.graphMeasure] || [];
        var lastState = lastAvailableState(states);
        if (!lastState) return false;
        var duration = (moment(lastState.endTime).diff(moment(lastState.startTime))) / 1000;
        const graphMeasure = reportCfg[d.name][definitions.graphMeasure];
        let cfgs = (graphMeasure && graphMeasure['alerts']) ? graphMeasure['alerts'][lastState.state] : null;
        if (cfgs) {
          if (duration >= cfgs['low']['threshold'] && duration < cfgs['medium']['threshold'] && cfgs['low']['flashing_speed']) {
            return cfgs['low']['flashing_speed'] + 's';
          } else if (duration >= cfgs['medium']['threshold'] && duration < cfgs['high']['threshold'] && cfgs['medium']['flashing_speed']) {
            return cfgs['medium']['flashing_speed'] + 's';
          } else if (duration >= cfgs['high']['threshold'] && cfgs['high']['flashing_speed']) {
            return cfgs['high']['flashing_speed'] + 's';
          }
        }
      });

    mergedSubjectDivs.select('div.currentStatus div.status').text(function (d) {
      var states = d.measures[definitions.graphMeasure] || [];
      var lastState = lastAvailableState(states);
      if (!lastState) return 'No Data Available';
      return reportCfg[d.name][definitions.graphMeasure]['stateLabels'][lastState.state];
    });

    mergedSubjectDivs.select('div.currentStatus div.duration').text(function (d) {
      var states = d.measures[definitions.graphMeasure];
      if (!states) return '';
      var lastState = lastAvailableState(states);
      if (!lastState) return formatDuration(0);
      var duration = moment(lastState.endTime).diff(moment(lastState.startTime));
      return formatDuration(duration);
    });

    mergedSubjectDivs.select('div.currentStatus div.updateTime').text(function (d) {
      var states = d.measures[definitions.graphMeasure];
      if (!states) return moment(data.highWaterMarkTime).format('h:mm a');
      var lastState = lastAvailableState(states);
      if (!lastState) return moment(data.highWaterMarkTime).format('h:mm a');
      var recordedTime = moment.tz(lastState.endTime, timezone).subtract(data.timeslotSizeMillis, 'milliseconds');
      return 'at ' + recordedTime.format('h:mm a');
    });
  }
  subjectDivs.exit().remove();
  if (!reportCfg[parameters.subject]['composite']) {
    d3.select('.status-badge').remove();
    return;
  }
}

async function reload(parameters, reportType) {
  test = parameters.test;
  const paramsString = Object.keys(urlParams).map(function (key) {
    return encodeURIComponent(key) + '=' + encodeURIComponent(urlParams[key]);
  }).join('&');

  configObj = await fetchYardCfg(parameters);

  if (liveMode) {
    d3.select('#reportDateTitle div.date').text(`Shift for ${moment.tz(new Date(), timezone).format('MMMM Do, YYYY hh:mm a z')}`);
  } else {
    d3.select('#reportDateTitle div.date').text(`Shift for ${moment(parameters.date).format('MMMM Do, YYYY')}`);
  }

  d3.select('#reportDate').attr('value', parameters.date);
  let dataRoute = null;
  if (reportType === 'safetySubjectDetail') {
    dataRoute = '/portal/data/safetySubjectDetail/' + parameters.customer + '?' + paramsString;
  } else{
    dataRoute = '/portal/data/yardsubjects/' + parameters.customer + '?' + paramsString;
  }
  let data = await d3.json(dataRoute, {credentials: 'include'});
  if (!Object.keys(data).length) {
    showNoDataIndicator();
    return data;
  }

  fillMissingStateColors(reportCfg, data.subjects);

  //Only filter the subject selected in detail report
  data.subjects = data.subjects.filter(function (d) {
    if (reportType === 'detail' || reportType === 'safetySubjectDetail') {
      return (!parameters.subject || d.name === parameters.subject);
    } else {
      return d;
    }
  });

  for (let i = 0; i < data.subjects.length; i++) {
    for (let measure in data.subjects[i].measures) {
      if (data.subjects[i].measures[measure].length === 0 &&
        measure.includes('Worker Distancing')) {
        delete data.subjects[i].measures[measure];
      }
      if (!reportCfg[data.subjects[i].name][measure]) {
        delete data.subjects[i].measures[measure];
      }
    }
  }

  //Calculate Cycle time for Gatwick
  if (parameters.customer === 'gatwickairport' || parameters.customer === 'tia') {
    data.subjects.forEach(stand => {
      if (Object.keys(stand.measures).length) {
        const numerator = stand.measures.composite
          .filter(m => m.state !== 'idle').reduce((out, interval) => {
            return out + moment.duration(moment(interval.endTime).diff(moment(interval.startTime))).asMilliseconds();
          }, 0);
        if (('aircraft' in stand.measures) && stand.measures.aircraft.length) {
          const denom = stand.measures.aircraft.filter(m => m.state === 'present').length;
          stand.avgCycleTime = denom !== 0 ? numerator / denom : 0;
        }
      } else {
        stand.avgCycleTime = 0;
      }
    });
  }

  lastResult = data;
  if (reportType === 'detail' && (data.subjects[0] && Object.keys(data.subjects[0].measures).length === 0)) {
    return {};
  }
  if (latestConfig) {
    if (latestConfig.screen_type === 'tv') {
      data.subjects = data.subjects.filter(function (d) {
        if (latestConfig.config.subjects.includes(d.name)) {
          return d;
        }
      });
    } else {
      data.subjects = data.subjects.filter(function (d) {
        return (!latestConfig.config.primarySubject || d.name === latestConfig.config.primarySubject);
      });

      data.subjects.forEach(function (subject) {
        subject.aggregates = {};
        subject.aggregates.utilization = calculateUtilization(data, subject);
      });
    }
  }
  data.subjects.sort(function (a, b) {
    return naturalCompare(a.name, b.name);
  });
  data.subjects.forEach(function (subject) {
    subject.aggregates = {};
    subject.aggregates.utilization = calculateUtilization(data, subject);
  });

  inProgress = shiftIsActive(lastResult);

  return data;
}

async function handleIntervalFired(parameters, reportType) {
  latestConfig = await getLatestConfig();
  if (latestConfig) {
    if (latestConfig.config.landing_page !== currentLandingPage) {
      location.reload();
    }
    parameters.customer = latestConfig.cgr;
    parameters.facility = latestConfig.config.facility;
    parameters.subject = latestConfig.config.primarySubject;
    urlParams = Object.assign(urlParams, parameters);
  }

  if (lastResult && moment(lastResult.highWaterMarkTime).isBefore(moment(lastResult.shiftEndTime))) {
    //only reload if the data is still updating
    let data = await reload(parameters, reportType);
    reportType == 'summary' ? renderSummary(data, parameters) : renderDetail(data, parameters);
  }
}

async function handleIntervalFiredForKiosk(parameters, reportType) {
  latestConfig = await getLatestConfig();
  if (latestConfig) {
    if (latestConfig.config.landing_page !== currentLandingPage) {
      location.reload();
    }
    parameters.customer = latestConfig.cgr;
    parameters.facility = latestConfig.config.facility;
    parameters.subject = latestConfig.config.primarySubject;
    urlParams = Object.assign(urlParams, parameters);
  }

  if (lastResult && moment(lastResult.highWaterMarkTime).isBefore(moment(lastResult.shiftEndTime))) {
    //only reload if the data is still updating
    let data = await reload(parameters, reportType);
    renderDetailForKiosk(data, parameters);
  }
}

async function fetchCfg(parameters) {
  urlParams = Object.assign(urlParams, parameters);
  const paramsString = Object.keys(urlParams).map((key) => encodeURIComponent(key) + '=' + encodeURIComponent(urlParams[key])).join('&');
  let cfg = await d3.json('/portal/config/getConfig/' + parameters.customer + '?' + paramsString, {credentials: 'include'});
  // Only keeping subject-measures configs which are not hidden
  cfg.filter(({ hidden }) => hidden !== true).forEach(row => {
    try {
      let measureName = row.measure_name;
      let subjectName = row.subject_name;
      if (!Object.keys(reportCfg).some(s => s == subjectName)) {
        reportCfg[subjectName] = {};
      }
      if (reportCfg[subjectName]) {
        if (!Object.keys(reportCfg[subjectName]).some(m => m == measureName)) {
          reportCfg[subjectName][measureName] = {};
        }
      }

      //Put values
      reportCfg[subjectName][measureName]['stateColors'] = {};
      reportCfg[subjectName][measureName]['stateLabels'] = {};
      reportCfg[subjectName][measureName]['alerts'] = {};
      reportCfg[subjectName][measureName]['gwName'] = row.gateway_name;
      try {
        reportCfg[subjectName][measureName]['imgName'] = row.cs_config.image_name;
      } catch (e) {
        if ('Cannot read property \'image_name\' of null' === e.message) {
          throw new Error(`CRC row for ${subjectName}::${measureName} needs a cs_config.image_name.`);
        } else {
          throw e;
        }
      }
      reportCfg[subjectName][measureName]['priority'] = row.priority;
      reportCfg[subjectName][measureName]['img_source_measure_name'] = row.cs_config.img_source_measure_name;
      reportCfg[subjectName][measureName]['imgInterval'] = row.cs_config.img_interval;
      reportCfg[subjectName][measureName]['utilization_definition'] = row.cs_config.utilization_definition ? row.cs_config.utilization_definition : null;
      if (row.cs_config.states) {
        Object.keys(row.cs_config.states).forEach(stateName => {
          if (!Object.keys(reportCfg[subjectName][measureName]['stateColors']).some(name => name == stateName)) {
            reportCfg[subjectName][measureName]['stateColors'][stateName] = row.cs_config.states[stateName].color;
            reportCfg[subjectName][measureName]['stateLabels'][stateName] = row.cs_config.states[stateName].cs_label;
            if ('alerts_threshold' in row.cs_config.states[stateName]) {
              reportCfg[subjectName][measureName]['alerts'][stateName] = row.cs_config.states[stateName].alerts_threshold;
            }
          }
        });
      }
      if (row.cs_config.no_utilization_percentage) {
        reportCfg[subjectName][measureName]['noUtilizationPercentage'] = row.cs_config.no_utilization_percentage;
      }
      if (row.cs_config.uniform_state_legend_color) {
        reportCfg[subjectName][measureName].uniform_state_legend_color = row.cs_config.uniform_state_legend_color;
      }
      if (row.cs_config.uniform_state_legend_name) {
        reportCfg[subjectName][measureName].uniform_state_legend_name = row.cs_config.uniform_state_legend_name;
      }
      if (row.cs_config.hide_utilization_from_legends) {
        reportCfg[subjectName][measureName].hide_utilization_from_legends= row.cs_config.uniform_state_legend_name;
      }
    } catch (e) {
      console.error('Problems processing a cfg row:', row);
      console.error(e);
      console.error('Fetched config:', row);
      console.table(cfg);
    }
  });
}

function calculateUtilization(data, subject, maxTime) {
  let utilization=  {};
  let numeratorAgg = 0;
  let denominatorAgg = 0;
  let percentageUtilization = null;
  let activityCount = 0;
  if (!subject.measures[definitions.graphMeasure]) {
    utilization.error = 'No measure found to calculate the composite';
    utilization.numerator = numeratorAgg;
    utilization.denominator = denominatorAgg;
    utilization.duration = formatDurationAsTime(utilization.numerator);
    utilization.percentage = formatDurationAsPercentage(utilization.numerator, utilization.denominator);
    utilization.activityCount = activityCount;
    return utilization;
  }
  var stateList = subject.measures[definitions.graphMeasure].filter(function (s) {
    return !maxTime || moment(s.startTime).diff(maxTime) < 0;
  }).map(function (s) {
    if (!maxTime || moment(s.endTime).diff(maxTime) <= 0) {
      return s;
    }
    return {
      state: s.state,
      startTime: s.startTime,
      endTime: maxTime
    };
  });

  let states = States.stats(stateList, reportCfg);

  if (definitions.graphMeasure in reportCfg[subject.name] && 'utilization_definition' in reportCfg[subject.name][definitions.graphMeasure]){
    let utilization_definition = reportCfg[subject.name][definitions.graphMeasure]['utilization_definition'];
    if (utilization_definition === '_no_utilization'){
      return null;
    }
    if (utilization_definition && 'numerator' in utilization_definition && Array.isArray(utilization_definition['numerator'])){
      states.map(x => {
        if (utilization_definition['numerator'].includes(x.state)){
          numeratorAgg += x.duration;
        }
      });
    }
    if (utilization_definition && 'denominator' in utilization_definition && Array.isArray(utilization_definition['denominator'])){

      states.map(x => {
        if (utilization_definition['denominator'].includes(x.state)){
          denominatorAgg += x.duration;
        }
      });
    }

    if (numeratorAgg === 0) {
      if (reportCfg[subject.name][definitions.graphMeasure]['utilization_definition']) {
        states.map(x => {
          if (utilization_definition['numerator'].includes(x.state)){
            numeratorAgg += x.duration;
          }
        });
      } else{
        states.map(x => {
          if (definitions.aggregationStates.includes(x.state)){
            numeratorAgg += x.duration;
          }
        });
      }
    }

    if(activityCount === 0) {
      let runningStates = stateList.filter(({ state }) => definitions.aggregationStates.includes(state));
      activityCount = runningStates.length;
    }

    if (denominatorAgg === 0) {
      denominatorAgg = moment(data.highWaterMarkTime || data.shiftEndTime).diff(moment(data.shiftStartTime));
    }

    utilization.duration = formatDurationAsTime(numeratorAgg);

    // Calculating percentage Utilization
    if (!reportCfg[subject.name][definitions.graphMeasure].noUtilizationPercentage) {
      percentageUtilization = formatDurationAsPercentage(numeratorAgg, denominatorAgg);
    }
  } else {
    utilization.error = "No 'utilization_definition' found";
  }

  utilization.numerator = numeratorAgg;
  utilization.denominator = denominatorAgg;
  utilization.percentage = percentageUtilization; //str
  utilization.error = false;
  utilization.activityCount = `${activityCount} Activity Event(s)`;
  return utilization;
}


(function (global){
  async function YardSubjectsReport(parameters, reportType) {
    definitions = {
      graphMeasure: (parameters.customer === 'lumenetix') ? 'bulb' : 'composite',
      aggregationStates: parameters.customer === 'lumenetix' ? ['solid blue', 'solid red', 'solid amber'] : ['running', 'violation']
    };

    setTimeout(() => YardSubjectsReport(parameters, reportType), 60 * 1000);

    //Fetch Config
    await fetchCfg(parameters);

    //Fetch Data
    let data = await reload(parameters, reportType);
    await reloadCustomerTags(parameters);

    $('.loading').remove();
    if (Object.keys(data).length) {
      reportType === 'summary' ? renderSummary(data, parameters) : renderDetail(data, parameters);
    } else if (global && Object.prototype.hasOwnProperty.call(global, 'renderNoShift') && 'function' === typeof global['renderNoShift']) {
      const fCallbackFunction = global[`renderNoShift`];
      fCallbackFunction(parameters.shift, parameters.date);
    }
  }

  global.YardSubjectsReport = YardSubjectsReport;
})(this);


async function constructSubjectPicker(parameters) {
  const customer = parameters.customer;
  const facility = parameters.facility;
  let subjects = await d3.json('/portal/data/getSubjectList?facility=' + facility + '&customer=' + customer, {credentials: 'include'});
  subjects.sort(naturalCompare);
  if (!parameters.subject) {
    parameters.subject = subjects[0];
  }
  renderSubjectPicker(subjects, parameters.subject, parameters);
}

function lastAvailableState(states) {
  let lastIndex = 1;
  while ((lastIndex <= states.length - 1) && states[states.length - lastIndex].state === 'Missing Data-Some') {
    lastIndex++;
  }
  return states[states.length - lastIndex];
}
const getLatestImage = (lastState, img_source_measure_name) => {
  if (img_source_measure_name) {
    lastState.images = lastState.images.filter(s => s.measure === img_source_measure_name);
  }
  return lastState.images[lastState.images.length - 1];
};
function sanityTest() {
  return 'sane';
}

if (typeof window === 'undefined') {
  module.exports = {sanityTest, lastAvailableState};
}

const newRandomGenerator = (seed) => {
  if (!isFinite(seed)) {
    throw new Error('Seed not a finite number');
  }

  const context = [
    { multiplier: 171, modulus: 30269, value: 0 },
    { multiplier: 172, modulus: 30307, value: 0 },
    { multiplier: 170, modulus: 30323, value: 0 }
  ];

  context.forEach((element, index) => { element.value = seed + index; });

  return () => {
    const generated = context.reduce((value, element) => {
      element.value = (element.value * element.multiplier) % element.modulus;
      return value + element.value / element.modulus;
    }, 0);
    return generated % 1;
  };
};

const newMissingColorGenerator = () => {
  const BOUNDARY = 0xFFFFFF;
  const fRandomValue = newRandomGenerator(100);
  return () => {
    const value = Math.floor(fRandomValue() * BOUNDARY).toString(16);
    return `#${value}`;
  };
};

function fillMissingStateColors(reportConfig, data) {
  const fGenerateColor = newMissingColorGenerator();
  Object.keys(reportConfig).forEach(subject => {
    Object.keys(reportConfig[subject]).forEach(measure => {
      let measureRow = reportConfig[subject][measure];
      const subjRow = data.filter(d => d.name === subject);
      const measureRows = subjRow.length && subjRow[0].measures[measure];

      if (measureRows) {
        const states = [...new Set(measureRows.map(item => item.state))];
        if (!Object.keys(measureRow.stateColors).length) {
          measureRow.stateColors = states.reduce((mapOfStateToColor, state) => Object.assign(mapOfStateToColor, { [state]: fGenerateColor() }), {});
          measureRow.stateLabels = states.reduce((mapOfStateToLabel, state) => Object.assign(mapOfStateToLabel, { [state]: capitalize(state) }), {});
        }
      }
    });
  });
}
