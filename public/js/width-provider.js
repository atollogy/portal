'use strict';

(function (global, newWidthModule) {
  const Atollogy = global.Atollogy || (global.Atollogy = {});
  Object.assign(Atollogy, newWidthModule());
}(this, () => {
  // ToDo: Fix it properly with priority based layers of states
// Instead of using the hardcoded interesting states use configs with priority
  const MINIMUM_WIDTH = 1;
  const DEFAULT_IMAGE_INTERVAL = 30;

  const newWidthCalculator = (xAxis, theReportConfig) => {
    const localReportConfig = Object.assign({}, theReportConfig);

    const getMeasureUnit = (subject, measure, nDefaultValue) => {
      const theDefaultValue = nDefaultValue ? Number(nDefaultValue) : DEFAULT_IMAGE_INTERVAL;
      const theSubject = subject && 'string' === typeof subject ? `${subject }` : 'default-subject-config';
      const theMeasure = measure && 'string' === typeof measure ? `${measure }` : 'default-measure-config';
      const theTentativeValue = Number(localReportConfig[theSubject][theMeasure]['imgInterval']) || theDefaultValue;
      const theProposedValue = Number.isFinite(theTentativeValue) ? theTentativeValue : DEFAULT_IMAGE_INTERVAL;
      return 1000 * theProposedValue;
    };

    return ({ subject, measure, startTime, endTime }) => {
      const theStartTime = moment(startTime).valueOf();
      const theFinishingTime = moment(endTime).valueOf();

      const xUnit = getMeasureUnit(subject, measure);
      const theSuggestedEndTime = theFinishingTime - theStartTime < xUnit ? (theStartTime + xUnit) : theFinishingTime;

      const delta = xAxis(theSuggestedEndTime) - xAxis(theStartTime);
      return Math.max(MINIMUM_WIDTH, delta);
    };
  };

  return Object.freeze({ newWidthCalculator });
}));
