/* eslint-disable no-undef,no-unused-vars */

const actions = {
  date: () => { /** NoOp */ },
  search: () => { /** NoOp*/ },
  tag: () => { /** NoOp*/ }
};

const stateObject = Object.freeze({
  selectedDate: '',
  searchCriteria: '',
  customerTags: [],
  filterOnTags: []
});

const renderCardImages = (function (global) {
  const theExecutionContext = global || {};
  const theLazyLoadingModule = theExecutionContext.LazyLoading || Object.freeze({
    init:  () => {
      d3.selectAll('img.display-img.lazy').attr('src', function() {
        return this.attributes['data-src'].value || false;
      });
    }
  });

  const theRenderFunction = theLazyLoadingModule.init || undefined;

  return theRenderFunction;
}(this));

function onTagElementClicked(element) {
  const { iterations } = this;
  const { filterOnTags } = stateObject;
  const criteria = element.text();

  let setOfAppliedTags = new Set(...filterOnTags);
  const isRemovingFilter = !setOfAppliedTags.add(criteria);
  if (isRemovingFilter) {
    setOfAppliedTags.delete(criteria);
  }

  filterOnTags.splice(0);
  setOfAppliedTags.forEach((customerTag) => filterOnTags.add(customerTag));

  const filtered =  iterations.filter(
    (iteration) => Object.prototype.hasOwnProperty.call(iteration, 'customerTags')
  ).filter(
    ({ customerTags }) => customerTags.some((customerTag) => setOfAppliedTags.has(customerTag))
  );

  const newViewState = [].concat(Object.assign({}, this, { iterations: filtered }));
  renderDeliveryLog(newViewState, parameters, renderCardImages);
}

function onSearchDeliveryLog(text) {
  const { iterations } = this;
  const criteria = text.toLowerCase();

  const filtered =  iterations.filter(({ subject }) => subject.toLowerCase().includes(criteria));
  const newViewState = [].concat(Object.assign({}, this, { iterations: filtered }));

  stateObject.searchCriteria = criteria;

  renderDeliveryLog(newViewState, parameters, renderCardImages);
}

function keepEndOfCycleEvents(theResponseObject) {
  theResponseObject = Object.assign({ iterations: [] }, theResponseObject);

  let iterations = theResponseObject.iterations.filter(
    (iteration) => iteration.states.length
  ).filter(
    (iteration) => iteration.states.some(({ is_end }) => is_end)
  );

  return Object.assign({}, theResponseObject,  { iterations });
}

async function reloadDeliveryLog(parameters) {
  let theLatestConfig = await getLatestConfig();
  if (!theLatestConfig) {
    await reloadCustomerTags(parameters);
  } else {
    d3.select('div.cycleTitle').text('Facility: ' + parameters.facility);
  }

  d3.select('#reportDate').attr('value', parameters.date);
  let rawDataObj = await reload(parameters);
  $('.loading').remove();

  const customer = Object.assign({}, { customer: '' }, parameters);

  if (rawDataObj.length) {
    const site = parameters.facility.toLocaleUpperCase();
    rawDataObj = rawDataObj.filter((record) => getCustomerFilterForSite(customer, site, record));

    const iterations = rawDataObj[0].iterations;
    const states = iterations.length && iterations[0].states;
    if (states.length && states[0].is_start && states[0].is_end) {
      rawDataObj = convertSingleStateToCycle(rawDataObj);
    }

    rawDataObj[0] = keepEndOfCycleEvents(rawDataObj[0]); //FIXME

    //Calculate cycle stats for filtered data
    rawDataObj[0] = cycleStats(rawDataObj[0]);

    rawDataObj[0].iterations.forEach(x => {
      x.states[0].isReportStart = true;
      x.states[x.states.length - 1].isReportEnd = true;
    });

    if (reportView == 'new') {
      rawDataObj = mergeStatesOfTruck(rawDataObj);
    }
    let tempData = JSON.parse(JSON.stringify(rawDataObj));

    if (tempData[0].iterations.length) {
      return tempData;
    } else {
      showNoDataIndicator();
    }

  } else {
    showNoDataIndicator();
  }
}

const DELIVERY_ID = Symbol('id');
const cacheOfDeliveryRecords = {};
const COALESCING_WINDOW = 10 * 60 * 1000;

const clearDeliveryLogCache = () => Object.keys(cacheOfDeliveryRecords).forEach(key => delete cacheOfDeliveryRecords[key] );
const findOrCreateDelivery = (iteration) => {
  const { subject, states } = iteration;

  const theEndTime = moment(states[states.length - 1].endTime);
  const theFinishTime = COALESCING_WINDOW * Math.floor(theEndTime.valueOf() / COALESCING_WINDOW);
  const identity = `${subject}:${moment(theFinishTime).format('YYYY-MM-DD HH:mm z')}`;

  if (Object.prototype.hasOwnProperty.call(cacheOfDeliveryRecords, identity)) {
    return cacheOfDeliveryRecords[identity];
  }

  const theIterationObject = Object.assign({}, iteration, { [DELIVERY_ID]: identity });
  const theDeliveryRecord = newDeliveryRecord(theIterationObject);
  Object.assign(cacheOfDeliveryRecords, { [identity]: theDeliveryRecord });
  return theDeliveryRecord;
};

const renderTagComponent = ({ customerTags, filterOnTags }) => {
  const theTagSelectionPicker = $('.selectpicker').html('');
  const options = customerTags.map(
    (customerTag) => `<option>${customerTag}</option>`
  ).join('');

  theTagSelectionPicker.append(options);
  theTagSelectionPicker.val(filterOnTags);

  $('select').selectpicker('refresh');
  d3.selectAll('li a.dropdown-item').on('click', function () {
    actions.tag(d3.select(this));
  });
};

const newDeliveryRecord = (iteration) => {
  const { subject, state, [DELIVERY_ID]: id } = iteration;

  const duration = calculateVisitLogDuration(
    moment.tz(iteration.states[0].endTime, timezone),
    moment.tz(iteration.states[iteration.states.length - 1].endTime, timezone),
    displayTrail=false
  );

  const image = iteration.states[iteration.states.length - 1].img_keys['lpr'][0];

  return Object.freeze({ subject, image, duration, state, id, images: new Set() });
};

async function renderDeliveryLog(data, parameters, onRenderComplete) {
  const elements = data && data.length ? data[0] : { iterations: [] };

  let iterations = elements.iterations.filter(
    (iteration) => iteration.states.length
  ).filter(
    (iteration) => iteration.states.some(({ is_end }) => is_end)
  );

  iterations.map((iteration) => {
    const timeline = iteration.states.filter((state) => {
      return Object.prototype.hasOwnProperty.call(state, 'img_keys') &&
        Object.prototype.hasOwnProperty.call(state.img_keys, 'lpr');
    }).reduce((boundaries, state) => {
      const { start, finish } = boundaries;

      return {
        start: Math.min(start, Date.parse(state.startTime)),
        finish: Math.max(finish, Date.parse(state.endTime))
      };
    }, ({ start: Number.MAX_SAFE_INTEGER, finish: Number.MIN_SAFE_INTEGER }));

    return Object.assign(iteration, timeline);
  });

  //sort the array based on the last entered
  iterations.sort((left, right) => {
    const dateA = new Date(left.finish);
    const dateB = new Date(right.finish);
    return Math.sign(dateB - dateA); //sort by lastSeen descending
  });

  const mapOfIterations = iterations.reduce((mapOfIterations, theOriginalIteration) => {
    const theIterationObject = findOrCreateDelivery(theOriginalIteration);
    const states = theOriginalIteration.states;
    const imageList = states[states.length - 1].img_keys['lpr'];
    imageList[imageList.length - 1]['measure'] = states[states.length - 1].state;
    theIterationObject.images.add(imageList[imageList.length - 1]);
    mapOfIterations.set(theIterationObject, Object.assign({}, theIterationObject, theOriginalIteration));
    return mapOfIterations;
  }, new Map);

  iterations = Array.from(mapOfIterations.values());
  mapOfIterations.clear();

  if (iterations.length) {
    const filtered = [];
    const previous = { subject: '', states: [] };

    for (let index = 0; index < iterations.length; index += 1) {
      const current = iterations[index];

      if (current.subject === previous.subject && current.states.length === 1) {
        /** Do nothing this is an orphan entry */
      } else {
        if (current.subject === previous.subject && previous.states.length === 1) {
          filtered.pop();
          current.states.push(previous.states.find(Boolean));
        }
        filtered.push(current);
        previous.states = current.states;
        previous.subject = current.subject;
      }
    }

    iterations = filtered;
  }

  const visitCounters = {};
  iterations.forEach((iteration) => {
    const { subject } = iteration;
    const visitCounter = visitCounters[subject] || (visitCounters[subject] = { value : 0 });
    visitCounter.value += 1;
    iteration.visit = visitCounter.value;
  });
  let records = createGroupedArray(iterations, 4);

  if (records.length) {
    const setOfTags = new Set();
    const { customerTags } = stateObject;

    iterations.filter(
      (iteration) => Object.prototype.hasOwnProperty.call(iteration, 'customerTags')
    ).filter(
      ({ customerTags }) => Array.isArray(customerTags)
    ).forEach(
      ({ customerTags }) => customerTags.forEach((customerTag) => setOfTags.add(customerTag))
    );

    customerTags.splice(0);
    setOfTags.forEach((customerTag) => customerTags.push(customerTag));

    renderTagComponent(stateObject);
  }

  d3.select('#truckCount').html('');
  d3.select('div.facility').text(`Facility: ${parameters.facility}`);
  d3.select('#truckCount').text(`Truck Delivery Count: ${iterations.length}`);
  if (!records.length) {
    showNoDataIndicator();
  } else {
    hideNoDataIndicator();
  }

  const buildCardSetID = (records) => records.map((record) => record.id).join('/');
  var cycleDivsRow = d3.select('#container').selectAll('div.cycle-cards').data(records, buildCardSetID);
  let newGridRows = cycleDivsRow.enter().append('div').classed('cycle-cards', true).classed('mdl-grid', true);
  let mergedGridDivs = newGridRows.merge(cycleDivsRow);
  cycleDivsRow.exit().remove();
  let gridCol = mergedGridDivs.selectAll('div.card-col').data((record) => record, (record) => record.id);
  let newGridColDivs = gridCol.enter().append('div').classed('card-col', true).classed('mdl-cell', true).classed('mdl-cell--3-col', true);

  let cardBodyContent = newGridColDivs.append('div')
    .classed('demo-card-square', true)
    .classed('mdl-card', true)
    .classed('mdl-shadow--2dp', true)
    .classed('center-align', true)
    .classed('mdl-card__supporting-text', true);

  cardBodyContent.append('div').classed('truck', true);
  cardBodyContent.append('div').classed('start-time', true);
  cardBodyContent.append('div').classed('duration', true);
  cardBodyContent.append('div').classed('visit', true);

  cardBodyContent.append('img').classed('display-img', true)
    .on('mouseout', function() {
      d3.select(this).style('cursor', 'default');
    })
    .on('mouseover', function() {
      d3.select(this).style('cursor', 'pointer');
    })
    .on('click', (d) => {
      deliveryHeader = {};
      deliveryHeader.subject = `Truck: ${d.subject}`;
      deliveryHeader.dropOff = `Drop Off: ${moment.tz(d.states[d.states.length - 1].endTime, timezone).format('YYYY-MM-DD HH:mm z')}`;
      const duration = calculateVisitLogDuration(moment.tz(d.states[0].endTime, timezone), moment.tz(d.states[d.states.length - 1].endTime, timezone), displayTrail=false) || 'NA';
      if (reportCfg[d.states[0].state].inferredState.type === 'entry') {
        deliveryHeader.deliveryTime = `Load & Delivery Time: ${duration}`;
      } else {
        deliveryHeader.deliveryTime = `Delivery Time: ${duration}`;
      }
      constructAndRenderImgModal(d, true, deliveryHeader);
    });

  cardBodyContent.append('div').classed('tags', true);
  let mergedCols = newGridColDivs.merge(gridCol);
  mergedCols.select('div.truck').text(function (d) {
    return `Truck: ${d.subject}`;
  }).on('click', (d) => {
    deliveryHeader = {};
    deliveryHeader.subject = `Truck: ${d.subject}`;
    deliveryHeader.dropOff = `Drop Off: ${moment.tz(d.states[d.states.length - 1].endTime, timezone).format('YYYY-MM-DD HH:mm z')}`;
    const duration = calculateVisitLogDuration(moment.tz(d.states[0].endTime, timezone), moment.tz(d.states[d.states.length - 1].endTime, timezone), displayTrail=false) || 'NA';
    if (reportCfg[d.states[0].state].inferredState.type === 'entry') {
      deliveryHeader.deliveryTime = `Load & Delivery Time: ${duration}`;
    } else {
      deliveryHeader.deliveryTime = `Delivery Time: ${duration}`;
    }
    constructAndRenderImgModal(d, true, deliveryHeader);
  });
  mergedCols.select('button.tag-btn').attr('id', function (d, i) {
    return `demo-menu-lower-right- ${i}`;
  });
  mergedCols.select('div.start-time').text(function (d) {
    return `Drop Off: ${moment.tz(d.states[d.states.length - 1].endTime, timezone).format('YYYY-MM-DD HH:mm z')}`;
  });
  mergedCols.select('div.duration').text(function (d) {
    const duration = calculateVisitLogDuration(moment.tz(d.states[0].endTime, timezone), moment.tz(d.states[d.states.length - 1].endTime, timezone), displayTrail=false) || 'NA';
    if (reportCfg[d.states[0].state].inferredState.type === 'entry') {
      return `Load & Delivery Time: ${duration}`;
    } else {
      return `Delivery Time: ${duration}`;
    }
  });
  mergedCols.select('div.visit').text(({ subject, visit }) => {
    const { value: totalVisits } = visitCounters[subject];
    const theDisplayVisit = totalVisits - visit + 1;
    return `${theDisplayVisit}/${totalVisits}`;
  });

  mergedCols.select('img.display-img')
    .classed('lazy', true)
    .attr('data-src', function (d) {
      const imageObject = d.states[d.states.length - 1].img_keys['lpr'][0];
      return newImageComponent(parameters.customer, imageObject).toImageURL();
    });

  mergedCols.select('div.mdl-card').classed('custom-color', true).style('--customColor', (d) => {
    let diff = moment.tz(d.states[d.states.length - 1].endTime, timezone).diff(moment.tz(d.states[0].endTime, timezone), 'seconds');
    let temp = cardColoring(diff);
    return temp;
  });
  gridCol.exit().remove();
  mergedGridDivs.merge(mergedCols);

  onDeliveryRenderComplete(onRenderComplete);
}

function onDeliveryRenderComplete(onRenderComplete) {
  try {
    onRenderComplete();
  } catch(ignored) {
    /** ignored */
  }
}

async function deliveryLog(parameters) {
  setTimeout(() => deliveryLog(parameters), 60 * 1000);
  latestConfig = await getLatestConfig();

  let data = await reloadDeliveryLog(parameters);
  $('.loading').remove();
  if (data && data.length) {
    actions.search = onSearchDeliveryLog.bind(Object.assign({}, data[0]));
    actions.tag = onTagElementClicked.bind(Object.assign({}, data[0]));

    now = (parameters.time ? parameters.time : moment());
    d3.select('div.title').text(`Delivery Log for ${moment.tz(now, timezone).format('MMMM Do, YYYY HH:mm z')}`);
    renderDeliveryLog(data, parameters, renderCardImages);
    clearDeliveryLogCache();
  } else {
    showNoDataIndicator();
  }
}