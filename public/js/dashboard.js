/* eslint-disable no-unused-vars,no-undef */
let dataRefreshInterval;
let globalData, searchText = '';

async function handleInterval(parameters) {
  let data = await reloadStatus(parameters);
  if (data.length) {
    renderStatus(data, parameters);
    hideNoDataIndicator();
  } else {
    showNoDataIndicator();
  }
}

function hideNoDataIndicator() {
  d3.select('#noDataIndicator').style('display', 'none');
}

function showNoDataIndicator() {
  d3.select('#noDataIndicator').style('display', 'block');
}

async function getStatus(parameters) {

  setTimeout(() => getStatus(parameters), 60 * 1000);

  let data = await reloadStatus(parameters);
  if (data.length) {
    renderStatus(data, parameters);
    hideNoDataIndicator();
  } else {
    showNoDataIndicator();
  }
}

async function reloadStatus(parameters) {
  let data = await d3.json('/portal/data/gwImgs?customer=' + parameters.customer, {credentials: 'include'});
  globalData = data;
  return data;
}

function dateTimeWithTZ(dt) {
  let timezone = moment.tz(moment.tz.guess()).zoneAbbr();
  return dt.format('YYYY-MM-DD HH:mm') + ' ' + timezone;
}


function renderStatus(data, parameters) {
  let colWidthPercent = 100 / 5;

  if(searchText!=''){
    data = data.filter(x=>{
      return (x.facility && x.facility.includes(searchText)) || (x.name && x.name.includes(searchText)) || (x.gwid && x.gwid.includes(searchText))  || (x.nodeName && x.nodeName.includes(searchText));
    });
  }

  d3.select('.gwCount').text('Camera Count: ' + data.length + ' (last updated: ' + moment.tz(moment(),timezone).format('YYYY-MM-DD HH:mm') + ' '+timezone+')');
  let records = createGroupedArray(data, 5);
  var gwDivsRow = d3.select('#container').selectAll('div.gw-cards').data(records);
  let newGridRows = gwDivsRow.enter().append('div').classed('gw-cards', true).classed('mdl-grid', true);
  let mergedGridDivs = newGridRows.merge(gwDivsRow);
  gwDivsRow.exit().remove();
  let gridCol = mergedGridDivs.selectAll('div.card-col').data(function (d) {
    return d;
  });
  let newGridColDivs = gridCol.enter().append('div').classed('card-col', true).classed('mdl-cell', true).style('--percent-width', colWidthPercent + '%');

  let cardBodyContent = newGridColDivs.append('div').classed('demo-card-square', true).classed('mdl-card', true)
    .classed('mdl-shadow--2dp', true).classed('center-align', true).classed('mdl-card__supporting-text', true);
  cardBodyContent.append('div').classed('facility', true);
  cardBodyContent.append('div').classed('gwName', true);
  cardBodyContent.append('div').classed('gwId', true);
  cardBodyContent.append('div').classed('nodeName', true);
  cardBodyContent.append('div').classed('cameraId', true);
  cardBodyContent.append('div').classed('stepName', true);
  cardBodyContent.append('div').classed('csLabel', true);
  cardBodyContent.append('div').classed('collectionTime', true);
  cardBodyContent.append('div').classed('collectionInterval', true);
  let imgLink = cardBodyContent.append('a').classed('img-link', true);
  imgLink.append('img').classed('display-img', true);
  imgLink.append('video').classed('display-video', true);
  cardBodyContent.append('div').classed('no-img', true);
  cardBodyContent.append('div').classed('message', true);
  let mergedCols = newGridColDivs.merge(gridCol);
  mergedCols.select('div.facility').text(d => 'Facility: ' + d.facility);
  mergedCols.select('div.gwName').text(d => 'Camera Name: ' + d.name);
  mergedCols.select('div.gwId').text(d => 'Camera ID: ' + d.gatewayId);
  mergedCols.select('div.nodeName').text(d => 'Node Name: ' + d.nodeName);
  mergedCols.select('div.cameraId').text(d => d.cameraId!=null ? 'Camera Id: ' + d.cameraId : '');
  mergedCols.select('div.stepName').text(d => d.stepName != null ? 'Step Name: ' + d.stepName : '');
  mergedCols.select('div.collectionInterval').text(d => {
    return d.collectionInterval ? 'Interval: ' + d.collectionInterval + ' sec' : '';
  });
  mergedCols.select('div.collectionTime').text(d => {
    return d.collectionTime ? 'Time: ' + moment.tz(d.collectionTime, timezone).format('YYYY-MM-DD HH:mm z') : '';
  });
  mergedCols.select('a.img-link').attr('href', '#')
    .on('click', d => {
      let listOfImages = [];
      if (d.img_keys != null) {
        let obj = {};
        obj.imgKey = d.img_keys[0];
        if(d.collectiontime!= null){
          obj['header'] = {'Collection Time' : moment(d.collectionTime).format('MM-DD-YYYY hh:mm a')};
        }
        listOfImages.push(obj);
        renderImgModal(listOfImages);
      }
    });
  //display img
  mergedCols.select('a img.display-img')
    .classed('show-img-div', d => {
      return d.img_keys && d.img_keys[0].name.endsWith('.jpg');
    })
    .classed('lazy', true)
    .attr('data-src', function (d) {
      if (d.img_keys != null && !d.img_keys[0].name.endsWith('.mp4')) {
        let imageObject = d.img_keys[0];
        return newImageComponent(parameters.customer, imageObject).toImageURL();
      }
    });

  //display video
  mergedCols.select('video.display-video source').html('');
  mergedCols.select('a video.display-video').classed('show-img-div', d => {
    return !d.img_keys || !d.img_keys[0].name.endsWith('.mp4');
  }).attr('loop', 'loop')
    .attr('muted', 'muted')
    .attr('playsinline', 'playsinline')
    .append('source')
    .classed('lazy', true)
    .attr('data-src', function (d) {
      if (d.img_keys != null && d.img_keys[0].name.endsWith('.mp4')) {
        let imageObject = d.img_keys[0];
        return newImageComponent(parameters.customer, imageObject).toImageURL();
      }
    });

  mergedCols.select('div.no-img').classed('no-img-div', d => {
    return d.img_keys == null;
  }).text(d => {
    return d.img_keys == null ? 'No Image Available' : '';
  });
  mergedCols.select('div.message').classed('show-message',d=>{
    return !d.collectionTime || d.name == null;
  }).text(d=>{
    if(d.collectionTime == null){
      return 'No image was collected in the last 7 days';
    }
    if (d.name == null) {
      return 'Camera is deployed but not configured for reporting';
    }
    let diff = moment.tz(moment(),timezone).diff(d.collectionTime, 'seconds');
    return diff >= 120 ? 'Stale Image' : '';

  }).classed('stale-message', d=>{
    let diff = moment.tz(moment(),timezone).diff(d.collectionTime, 'seconds');
    return diff >= 120;
  });
  mergedCols.select('div.mdl-card').classed('longDuration', function (d) {
    let diff = moment.tz(moment(),timezone).diff(d.collectionTime, 'seconds');
    return (diff >= 120);
  });
  gwDivsRow.exit().remove();
}

function search(text) {
  searchText = text;
  renderStatus(globalData, parameters);
}
