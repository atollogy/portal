'use strict';

(function (global, registrar) {
  registrar.call(global, '@atollogy/cache', ['moize'], function (moize) {
    return Object.freeze({
      default: (supplier, options) => {
        return moize.default(supplier, options);
      }
    });
  });
}(this, function makeRegistrarHelper(moduleName, dependencies, factory) {
  const global = this;
  const hasOwnProperty = Object.prototype.hasOwnProperty;

  if ('function' === typeof require && 'undefined' !== typeof module && hasOwnProperty.call(module, 'exports')) {
    dependencies = dependencies.map((name) => require(name));
    module.exports = factory.apply(global, dependencies);
    return;
  }

  dependencies = Array.isArray(dependencies) ? dependencies : [ dependencies ];
  for (let index = 0; index < dependencies.length; index += 1) {
    const dependency = dependencies[index];
    dependencies[index] = global[dependency];
  }

  const previous = global[moduleName] || undefined;

  const theModule = factory.apply(global, dependencies);
  global[moduleName] = theModule;

  theModule.noConflict = () => {
    global[moduleName] = previous;
    return theModule;
  };
}));