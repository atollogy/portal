#!/usr/bin/env bash
OUT_DIR=/data/mov4joseph/files
DATE_PART="2019/05/28"

# not all cameras name their LPR steps the same way
declare -A PLAN
#PLAN[01]=main_gate_entry_trailer
#PLAN[02]=main_gate_entry_tractor
#PLAN[03]=main_gate_exit_tractor
#PLAN[04]=main_gate_exit_trailer
PLAN[05]=tractor_trailer
PLAN[06]=tractor_trailer
#PLAN[09]=lpr
#PLAN[10]=lpr
#PLAN[11]=lpr
#PLAN[12]=lpr
#PLAN[13]=lpr
#PLAN[14]=lpr

for NODE_NUM in "${!PLAN[@]}"
do
    S3_URL="s3://atl-prd-trimac-data/tractor_trailer/db8cam${NODE_NUM}_trimac_prd/video0/${PLAN[${NODE_NUM}]}/annotated/${DATE_PART}/"
    echo aws s3 ls ${S3_URL}
    aws s3 sync ${S3_URL} ${OUT_DIR}/db8cam${NODE_NUM}/
done
