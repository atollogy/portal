#!/usr/bin/env bash

OUT_DIR=/data/mov4joseph/files
for NODE_NUM in 01 02 03 04 05 06 09 10 11 12 13 14
do
    cd ${OUT_DIR}/db8cam${NODE_NUM}
    ffmpeg -framerate 12 -pattern_type glob \
           -y -i '**/*capture_annotated.jpg' \
           -c:v libx264 -pix_fmt yuv420p \
           "${OUT_DIR}/../movies/db8cam${NODE_NUM}_trimac_prd.mp4"
done
