#!/usr/bin/env bash

working_dir="."
host_name=""
port=5432

do_daily_cycles_extract() {

    # args
    user_name=$1
    db_name=$2
    v_date=$4

    # clean-up previous run output
    echo "removing previous output"
    rm -v ${working_dir}/datamart_extract_for_*.zip || true
    rm -v ${working_dir}/out_*.csv || true

    zip_file_name="datamart_extract_for_${v_date}.zip"

    # build the output
    PGPASSWORD="$3" \
    psql --host ${host_name} \
        --port ${port} \
        --user ${user_name} ${db_name} \
        -c "COPY ( SELECT *
            FROM f_cycles_detailed
            WHERE date_trunc('day', start_time) = '${v_date}'
            ) TO STDOUT WITH CSV HEADER" > ${working_dir}/out_detailed_${v_date}.csv

    PGPASSWORD="$3" \
    psql --host ${host_name} \
        --port ${port} \
        --user ${user_name} ${db_name} \
        -c "COPY ( SELECT *
            FROM f_cycles_summarized
            WHERE date_trunc('day', start_time) = '${v_date}'
            ) TO STDOUT WITH CSV HEADER" > ${working_dir}/out_summarized_${v_date}.csv

    #zip is sensitive to current working dir, so cd in a sub-shell
    (cd ${working_dir}
    zip ${zip_file_name} out_*${v_date}.csv)

    # describe output
    echo "output for this run"
    ls -al ${working_dir}/datamart_extract_for_*.zip ${working_dir}/out_*.csv

}

do_weekly_intervals_extract() {

    # args
    user_name=$1
    db_name=$2
    v_date=$4

    # clean-up previous run output
    echo "removing previous output"
    rm -v ${working_dir}/datamart_extract_for_*.zip || true
    rm -v ${working_dir}/out_*.csv || true

    zip_file_name="datamart_extract_for_${v_date}.zip"
    output_csv_file="out_${v_date}.csv"

    # build the output
    PGPASSWORD="$3" \
    psql --host ${host_name} \
        --port ${port} \
        --user ${user_name} ${db_name} \
        -c "COPY ( SELECT * from f_intervals WHERE (
                measure_start_time >=
                date_trunc('week', to_date('${v_date}', 'YYYY-MM-DD')) AND
                measure_start_time <=
                date_trunc('week', to_date('${v_date}', 'YYYY-MM-DD')) + ('7 days' :: INTERVAL - '1 second' :: INTERVAL)
            ) ) TO STDOUT WITH CSV HEADER" > ${working_dir}/${output_csv_file}

    #zip is sensitive to current working dir, so cd in a sub-shell
    (cd ${working_dir}
    zip ${zip_file_name} ${output_csv_file})

    # describe output
    echo "output for this run"
    ls -al ${working_dir}/datamart_extract_for_*.zip ${working_dir}/out_*.csv

}

#### Example usage / Typical usage
#
# start_date=$(date --date="last tue" +"%Y-%m-%d")
#
# # this is Schaeffler's dmart conn details
#
#
# # this is Borg Warner's dmart conn details
#
#
#### Use for YIO extracts:
#
# start_date=$(date +"%Y-%m-%d")
