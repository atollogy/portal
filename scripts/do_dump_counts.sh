#!/usr/bin/env bash

do_dump_counts () {
    local dbname=$1
    local username=$2
    local password=$3

    echo ${dbname}
    set -e

    PGPASSWORD="${password}" \
    psql --host= --port=5432 --username=${username} --dbname=${dbname} \
    -c "\COPY (SELECT 'f_cycles_detailed' tablename, '${dbname}' dbname, cgr, site, min(start_time), max(start_time), count(*) FROM f_cycles_detailed GROUP BY 1, 2, 3, 4 ORDER BY max(start_time)) TO '/tmp/output.csv' DELIMITER ',' CSV" || true
    cat /tmp/output.csv >> output_all.csv
    cat /tmp/output.csv

    PGPASSWORD="${password}" \
    psql --host= --port=5432 --username=${username} --dbname=${dbname} \
    -c "\COPY (SELECT 'f_cycles_summarized' tablename, '${dbname}' dbname, cgr, site, min(start_time), max(start_time), count(*) FROM f_cycles_summarized GROUP BY 1, 2, 3, 4 ORDER BY max(start_time)) TO '/tmp/output.csv' DELIMITER ',' CSV" || true
    cat /tmp/output.csv >> output_all.csv
    cat /tmp/output.csv

    PGPASSWORD="${password}" \
    psql --host= --port=5432 --username=${username} --dbname=${dbname} \
    -c "\COPY (SELECT 'wide_example' tablename, '${dbname}' dbname, d_cgr, 'no-site' site, min(f_collection_time), max(f_collection_time), count(*) FROM wide_example GROUP BY 1, 2, 3, 4 ORDER BY max(f_collection_time)) TO '/tmp/output.csv' DELIMITER ',' CSV" || true
    cat /tmp/output.csv >> output_all.csv
    cat /tmp/output.csv

    PGPASSWORD="${password}" \
    psql --host= --port=5432 --username=${username} --dbname=${dbname} \
    -c "\COPY (SELECT 'f_events' tablename, '${dbname}' dbname, d_cgr, 'no-site' site, min(f_collection_time), max(f_collection_time), count(*) FROM f_events GROUP BY 1, 2, 3, 4 ORDER BY max(f_collection_time)) TO '/tmp/output.csv' DELIMITER ',' CSV" || true
    cat /tmp/output.csv >> output_all.csv
    cat /tmp/output.csv

    PGPASSWORD="${password}" \
    psql --host= --port=5432 --username=${username} --dbname=${dbname} \
    -c "\COPY (SELECT 'f_intervals' tablename, '${dbname}' dbname, cgr, 'no-site' site, min(measure_start_time), max(measure_start_time), count(*) FROM f_intervals GROUP BY 1, 2, 3, 4 ORDER BY max(measure_start_time)) TO '/tmp/output.csv' DELIMITER ',' CSV" || true
    cat /tmp/output.csv >> output_all.csv
    cat /tmp/output.csv

    set +e
}

delete_datamart_nightly () {
    PGPASSWORD="" \
    psql --host= --port=5432 --username atollogy --dbname=datamart \
    -c "DELETE FROM datamart_sync WHERE EXTRACT(EPOCH FROM sync_event_time) >= EXTRACT (EPOCH FROM (now() - INTERVAL '1 day'));"

    set +e
}

delete_datamart_weekly () {
    PGPASSWORD="" \
    -c "DELETE FROM datamart_sync WHERE EXTRACT(EPOCH FROM sync_event_time) >= EXTRACT (EPOCH FROM (now() - INTERVAL '7 days'));"

    set +e
}

do_datamart_top_off_subjects () {
    local dmInstance=$1
    NODE_ENV="prd" \
    DEBUG="portal*,-portal:instru*,-portal:db-query" \
    node --max-old-space-size=4096 lib/datamart/incremental-dataloader/index.js --datamart-instance=${dmInstance}
}

do_datamart_top_off_cycles () {
    local dmInstance=$1
    NODE_ENV="prd" \
    DEBUG="portal*,-portal:instru*,-portal:db-query" \
    node --max-old-space-size=4096 lib/datamart/incremental-dataloader/etl-cycles.js --datamart-instance=${dmInstance}
}

do_datamart_top_off_beacons () {
    local dmInstance=$1
    NODE_ENV="prd" \
    DEBUG="portal*,-portal:instru*,-portal:db-query" \
    node --max-old-space-size=4096 lib/datamart/incremental-dataloader/etl-beacons.js --datamart-instance=${dmInstance}
}

safe_remove_output_csv() {
    if [ -f output_all.csv ]; then
        rm -v output_all.csv
    fi
}

