#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###########
# Setup
###########

# settings.py
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())

import requests
import pytz
import json
import time
import os
from pytz import timezone
from datetime import datetime

host = os.getenv("HOST")

cycleCfg = {
    'cemex': {
        'facilities': ['Eliot', 'Cache%20Creek'],
        'data_services': ['cycle', 'cycleLog', 'cycle_known_identifiers'],
        'tz': 'US/Pacific'
    },
    'cemexusa': {
        'facilities': ['Brooksville', 'Victorville'],
        'data_services': ['cycle', 'cycleLog', 'cycle_known_identifiers'],
        'tz': 'US/Pacific'
    },
    'titanamerica': {
        'facilities': ['Medley'],
        'data_services': ['cycle', 'cycleLog', 'cycle_known_identifiers'],
        'tz': 'US/Eastern'
    },
    'lehighhanson': {
        'facilities': ['Permanente'],
        'data_services': ['cycle', 'cycleLog', 'cycle_known_identifiers'],
        'tz': 'US/Pacific'
    },
    'trimac': {
        'facilities': ['Trimac%20Trucks', 'Trimac%20Trailers', 'National%20Tank%20Services', 'Washbay%203',
        'Washbay%204', 'Washbay%205'],
        'data_services': ['cycle', 'cycleLog', 'cycle_known_identifiers'],
        'tz': 'US/Eastern'
    }
}

subjectCfg = {
    'sbay': ['Lathes', 'Mill Group 1', 'Proto Cells'],
    'lumenetix': ['Lumenetix'],
    'tesla': ['Site1']
}


def getData(url):
    dataUrl = f"{url}&date={today}&shift=day"
    print(f"Data route {dataUrl}.")
    return session.get(dataUrl, cookies=session.cookies, timeout=None, headers={'Connection': 'keep-alive'})


def is_valid(res, name, duration):
    data = json.loads(res.text)
    if len(data) > 0:
        print(f"Successfully downloaded data for {cgr} - {facility} - {name}. Took {str(round(duration, 2))} seconds. Size is {len(res.text)/1024:,.2f} KiB.")
    else:
        print(f"No data downloaded for {cgr} - {facility} - {name}.")


######################
# Login to portal
######################
credentials = {
    "email": (os.getenv("USER_EMAIL")),
    "password": (os.getenv("USER_PASSWORD"))
}
session = requests.Session()
login = session.post(f"{host}/portal/login", data=credentials, headers={'Connection': 'keep-alive'})

# Make sure login successful
if login.url == f"{host}/portal/reports/homepage":
    print('\nLogin Successful!')
else:
    raise ValueError('Login Failed. Try Again')

######################
# Run Data Route
######################
today = datetime.now(tz=pytz.utc).astimezone(timezone('US/Pacific')).strftime('%Y-%m-%d')  # Today in cust tz

# Loop through all cycle reports => all cgr-facility combos
for cgr in cycleCfg.keys():
    for facility in cycleCfg[cgr]['facilities']:
        for service in cycleCfg[cgr]['data_services']:
            baseUrl = f"{host}/portal/data/{service}/{cgr}?customer={cgr}&facility={facility}"
            print(f"{cgr}:{facility}:{service}")
            start_time = time.time()
            resp = ""
            try:
                resp = getData(baseUrl)  # get response form data route
            except Exception as detail:
                print('Handling run-time error:', detail)
                pass
            stop_time = time.time()
            is_valid(resp, service, stop_time - start_time)  # verify response is valid

print('\n\n=========End of script=========')

# for cgr in subjectCfg.keys():
