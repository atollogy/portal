# Get started!
```
virtualenv -p python3.6 venv
source venv/bin/activate
pip install -r requirements.txt

export DEBUG=portal*,-portal:instr*
export NODE_ENV=prd
export NEW_RELIC_ENABKLED=false
export USER_EMAIL=shreyas@atollogy.com
export USER_PASSWORD=foo
export HOST=https://atollogy.com

time ./populateCache.py
```

# Loop it!
```
pip install --upgrade pip
virtualenv -p python3.6 venv && source venv/bin/activate && pip install -r requirements.txt

export DEBUG=portal*,-portal:instr*
export NODE_ENV=prd
export NEW_RELIC_ENABKLED=false
export USER_EMAIL=shreyas@atollogy.com
export USER_PASSWORD=foo
export HOST=https://atollogy.com

while true; do sleep 120; time ./populateCache.py; done
```

