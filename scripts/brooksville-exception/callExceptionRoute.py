#!/usr/bin/env python3
# -*- coding: utf-8 -*-

###########
# Setup
###########
from dotenv import load_dotenv, find_dotenv
import requests
import os
import sys
import pytz
from datetime import datetime
from datetime import timedelta

load_dotenv(find_dotenv())
host = os.getenv("HOST")
username = os.getenv("USER_EMAIL")
password = os.getenv("USER_PASSWORD")
customer = 'cemexusa'
facility = 'Brooksville'
email = sys.argv[1]

######################
# Login to portal
######################
credentials = {
    "email": username,
    "password": password
}
session = requests.Session()
login = session.post(f"{host}/portal/login", data=credentials, headers={'Connection': 'keep-alive'})

# Make sure login successful
if login.url == f"{host}/portal/reports/cycleList?customer=cemexusa":
    print('\nLogin Successful!')
else:
    raise ValueError('Login Failed. Try Again')

######################
# Hit Data Route
######################
to = datetime.now(pytz.timezone('US/Eastern'))  # Curent time in customer TZ
_from = to - timedelta(minutes=7)  # from 7 mins ago
_from = _from.strftime('%Y-%m-%dT%H:%M:%S')
to = to.strftime('%Y-%m-%dT%H:%M:%S')

dataUrl = f"{host}/portal/reportExceptions/brooksvilleViolation/{customer}?from={_from}&to={to}&facility={facility}&email={email}"
print(f"Data route {dataUrl}.")
try:
    resp = session.get(dataUrl, cookies=session.cookies, timeout=None,
                       headers={'Connection': 'keep-alive'})  # save response form data route
    print('server responded with data')
    print(resp.content)
    exit(0)
except Exception as detail:
    print('run-time error:', detail)
    exit(1)
