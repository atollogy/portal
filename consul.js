const fs = require('fs');
const debug = require('debug')('portal:consul');

const moize = require('./cache').default;
const config = require('./lib/dynamic-config');
const ca = fs.readFileSync('./lib/certs/digicert.pem', 'utf8');

const MAX_AGE = 10 * 60 * 1000;

let consulClient;

async function getConsul() {
  debug('providing a consul client');
  if (consulClient) {
    return consulClient;
  }

  consulClient = require('consul')({
    host: config.consulHost,
    port: 443,
    secure: true,
    ca: ca,
    promisify: true
  });
  debug(`created Consul connection to ${config.consulHost}:443`);

  return consulClient;
}

const waitFor = async (interval) => new Promise((resolve) => setTimeout(resolve.bind(null, interval), interval));

const DEFAULT_RETRIES = 3;
const DEFAULT_RETRY_INTERVAL = 100;
function newRetriablePromise(
  promiseObject,                  /** @param {Promise} */
  maxRetries = DEFAULT_RETRIES,   /** @param {Number} */
  delay = DEFAULT_RETRY_INTERVAL  /** @param {Number} */
) {
  return promiseObject().catch((cause) => {
    return maxRetries > 1
      ? waitFor(delay).then(() => newRetriablePromise(promiseObject, maxRetries - 1, delay * 2))
      : Promise.reject(cause);
  });
}

module.exports.getConsul = async () => await newRetriablePromise(getConsul.bind(this));

async function extractValue(key) {
  debug(`consul kv get: ${key}`);
  const consulObject = await module.exports.getConsul();
  const { kv: keyValueObject } = consulObject;
  try {
    const valueObject = await keyValueObject.get({key: key});
    if (valueObject && valueObject.Value) {
      try {
        return JSON.parse(valueObject.Value);
      } catch (cause) {
        debug('Unable to parse value', cause);
        //ignore illegal values for now
      } finally {
        debug(`consul kv get finished`);
      }
    }
  } catch(error) {
    throw 'Unable to perform kv.get!!';
  }
}

async function getNodeInfoAtKeyPath(key) {
  var gatewayId = await module.exports.extractValue(key + 'gateway_id');

  if (!gatewayId) return;

  var updateDateStr = await module.exports.extractValue(key + 'updated');
  var updateDate = new Date(updateDateStr);

  var shostPromise = module.exports.extractValue(key + 'shost');
  var hwPromise = module.exports.extractValue(key + 'hwprofile/hardware');
  var rvPromise = module.exports.extractValue(key + 'hwprofile/revision');
  var vidPromise = module.exports.extractValue(key + 'hwprofile/video');
  var statusPromise = module.exports.extractValue(key + 'chef_last_run_state');
  var networkPromise = module.exports.extractValue(key + 'primary_network');

  var shost = await shostPromise;
  var hardware = await hwPromise || '';
  var revision = await rvPromise;
  var mfgInfo = undefined;

  if (hardware.toLowerCase().indexOf('bcm') == 0) {
    switch (revision) {
      case 'a02082':
        mfgInfo = 'Pi 3 Model B (Sony, UK)';
        break;
      case 'a22082':
        mfgInfo = 'Pi 3 Model B (Embest, CN)';
        break;
      case '900092':
        mfgInfo = 'Pi 0 v1.2';
        break;
      case '900093':
        mfgInfo = 'Pi 0 v1.3';
        break;
      case '9000C1':
        mfgInfo = 'Pi 0 W';
        break;
    }
  } else if (hardware.toLowerCase().indexOf('rockchip') >= 0) {
    mfgInfo = 'Tinker Board';
  }

  var video = undefined;
  var videoRaw = await vidPromise;
  if (videoRaw) {
    video = videoRaw.length > 0;
  }

  var network = await networkPromise;

  var statusString = await statusPromise;
  var statusOK = statusString && !/fail/i.test(statusString);

  return {
    gatewayId: gatewayId,
    updateDate: updateDate,
    nodeName: shost,
    hardware: mfgInfo,
    video: video,
    chefStatus: statusOK ? 'ok' : 'error',
    wired: network === 'eth0'
  };

}

async function getSubEntriesAsHash(keyBase) {
  try {
    debug(`begin consul getSubEntriesAsHash ${keyBase}`);
    const consulObject = await module.exports.getConsul();
    const { kv: keyValueObject } = consulObject;

    var props = await keyValueObject.get({key: keyBase, recurse: true});
    var result = {};

    if (!props) {
      return result;
    }

    props.forEach(entry => {
      var subKey = entry.Key.substring(keyBase.length);
      if (subKey.length) {
        try {
          result[subKey] = JSON.parse(entry.Value);
        } catch (e) {
          //do nothing
        }
      }
    });

    return result;
  } finally {
    debug(`complete consul getSubEntriesAsHash ${keyBase}`);
  }
}
module.exports.getSubEntriesAsHash = moize(getSubEntriesAsHash, {maxAge: MAX_AGE, isPromise: true});

async function getNodeInfo(customer) {
  try {
    debug(`begin getNodeInfo for ${customer}`);
    const consulObject = await module.exports.getConsul();
    const { kv: keyValueObject } = consulObject;

    var nodeKeys = await keyValueObject.keys({
      key: '_nodeData/' + customer + '/',
      recurse: true,
      separator: '/'
    });

    var nodeInfoPromises = nodeKeys.map((key) => {
      return module.exports.getNodeInfoAtKeyPath(key);
    });

    return (await Promise.all(nodeInfoPromises)).filter(entry => entry !== undefined).reduce((result, entry) => {
      if (!result[entry.gatewayId] || entry.updateDate > result[entry.gatewayId].updateDate) {
        result[entry.gatewayId] = entry;
      }
      return result;
    }, {});
  } catch (e) {
    debug(e);
  } finally {
    debug(`end getNodeInfo for ${customer}`);
  }
  return {};
}

async function getDatabaseConnectionInfo(customer) {
  return await module.exports.getSubEntriesAsHash('envs/' + config.env + '/regions/' + customer + '/endpoints/db/');
}

async function getKnownCGRs() {
  debug(`consul getKnownCGRs begins`);
  const cgrs = await module.exports.extractValue('cgrsActive/' + config.env);
  const withoutApdx = cgrs.filter(cgr => cgr !== 'apdx');
  debug(`consul getKnownCGRs finished`);
  return withoutApdx;
}

function onCacheHit(cache, options) {
  debug(`${options.profileName.split(' ')[0]}:HIT`,cache.keys);
}

function onCacheAdd(cache, options) {
  debug(`${options.profileName.split(' ')[0]}:MISS`,cache.keys);
}

const CACHE_OPTS = { maxAge: MAX_AGE, isPromise: true, onCacheHit, onCacheAdd };
module.exports.extractValue = moize(extractValue, CACHE_OPTS);
module.exports.getNodeInfoAtKeyPath = moize(getNodeInfoAtKeyPath, CACHE_OPTS);
module.exports.getNodeInfo = moize(getNodeInfo, CACHE_OPTS);
module.exports.getKnownCGRs = moize(getKnownCGRs, CACHE_OPTS);
module.exports.getDatabaseConnectionInfo = moize(getDatabaseConnectionInfo, CACHE_OPTS);
