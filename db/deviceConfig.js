function fetchCustomerDeviceConfig(newDeviceConfig, connection, yesql) {
  return (
    async ({customer, facility, created}) => {
      if ('undefined' === typeof customer) {
        throw new Error('customer paramter is required');
      }

      const parameters = { customer };
      if ('string' === typeof facility && facility.length > 0) {
        parameters.facility = facility;
      }

      if ('undefined' !== typeof created) {
        /* */
        parameters.created = created;
      }

      const criteria = Object.keys(parameters).reduce((statement, property) => {
        if (statement.length) {
          statement = `${statement} AND `;
        }
        return `${statement} ${property} = :${property}`;
      }, '');
      const query = `SELECT * FROM customer_device_config WHERE ${criteria}`;
      const theQueryStatement = yesql(query)(parameters);

      const { rows: records } = await connection.query(theQueryStatement.sql || theQueryStatement.text, theQueryStatement.values);
      const makeCustomerDeviceConfig = newDeviceConfig();
      return records.map((record) => makeCustomerDeviceConfig(record));
    });
}

function newCustomerDeviceConfigMaker() {
  const cache = {};

  const schema = Object.freeze({
    uuid: { required: true, nullable: false },
    customer: { required: true, nullable: false },
    facility: { required: true, nullable: false },
    device: { required: true, nullable: false },
    enabled: { required: false, default: () => true },
    created: { required: false, default: () => Date.now() }
  });

  const getRecordIdentity = ({ customer, facility, device, created }) => {
    return `${customer}:${facility}:${device}:${created}`;
  };

  function makeCustomerDeviceConfig(element) {
    const record = Object.assign({}, element);

    Object.keys(schema).forEach((property) => {
      const settings = Object.assign({
        required: true,
        nullable: false,
        default: () => undefined
      }, schema[property]);

      const {
        required: isRequired,
        nullable: isNullable,
        default: getDefaultValue
      } = settings;

      if (isRequired && !Object.prototype.hasOwnProperty.call(record, property)) {
        throw new Error(`${property} is required`);
      }

      const value = record[property] || undefined;
      if (!isNullable && 'undefined' === typeof value) {
        throw new Error(`${property} cannot be null, current value: '${value}'`);
      }

      record[property] = record[property] || getDefaultValue();
    });

    const identity = getRecordIdentity(record);
    if (!Object.prototype.hasOwnProperty.call(cache, identity)) {
      const valueObject = Object.assign({}, record);
      cache[identity] = valueObject;
    }

    return cache[identity];
  }

  return makeCustomerDeviceConfig;
}

module.exports = {
  fetchCustomerDeviceConfig,
  newCustomerDeviceConfigMaker
};