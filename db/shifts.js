const fs = require('fs');
const path = require('path');
const db = require('../db/');
const sql = require('yesql').pg;
const debug = require('debug')('portal:db:shifts');
const moize = require('../cache').default;

const shiftsQuery = `WITH shift_date AS (
    SELECT
        d AS date,
        s.shift_name,
        d AT TIME ZONE s.shift_timezone_name + s.start_offset AS shift_start,
        d AT TIME ZONE s.shift_timezone_name + s.end_offset   AS shift_end,
        s.shift_timezone_name,
        s.facility
    FROM generate_series(date_trunc('day', to_timestamp(:time,'YYYY-MM-DD HH24: MI: SS')) :: TIMESTAMP WITHOUT TIME ZONE - INTERVAL '14 days',
                                             date_trunc('day', to_timestamp(:time,'YYYY-MM-DD HH24: MI: SS')) :: TIMESTAMP WITHOUT TIME ZONE + INTERVAL '14 days',
                                             INTERVAL '1 day'
             ) d
        JOIN customer_shift s ON s.day_of_week IS NULL OR s.day_of_week = date_part('dow', d)
    WHERE (CAST(:facility AS TEXT) IS NULL OR COALESCE(facility, :facility) = :facility)
)

SELECT *
FROM ((SELECT DISTINCT ON (shift_name, in_progress)
         date :: timestamp :: date :: text,
         shift_name,
         shift_start,
         shift_end,
         (:time ::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE shift_timezone_name) BETWEEN shift_start AND shift_end AS in_progress,
          FALSE AS completed,
          shift_timezone_name
       FROM shift_date
       WHERE shift_end > :time ::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE shift_timezone_name
       ORDER BY shift_name, in_progress, shift_start ASC)
      UNION
      (SELECT DISTINCT ON (shift_name)
          date::timestamp::date::text,
          shift_name,
          shift_start,
          shift_end,
          FALSE AS in_progress,
          TRUE  AS completed,
          shift_timezone_name
       FROM shift_date
       WHERE shift_end < :time ::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE shift_timezone_name
       ORDER BY shift_name, shift_end DESC)) x
       WHERE x.in_progress = TRUE
ORDER BY shift_start ASC;`;

const readCurrentShiftQuery = (function () {
  let _CachedQueryStatement;

  return () => {
    let queryStatement = _CachedQueryStatement;

    if (!queryStatement) {
      queryStatement = fs.readFileSync(path.join(__dirname, 'query', 'currentShiftInfo.sql'), 'utf8');
      _CachedQueryStatement = queryStatement;
    }
    return queryStatement;
  };
}());

module.exports.currentShiftsInfo = async function (customer, queryParams) {
  const query = readCurrentShiftQuery();

  const q = sql(query)({facility: queryParams.facility, date: queryParams.date});
  const {rows} = await db.query(customer, q.text, q.values);
  return rows;
};

const listFacilitiesR = async function (customer) {

  const query = `SELECT DISTINCT (facility)
                 FROM customer_binding
                 WHERE (end_time IS NULL OR end_time >= now())
                   AND (start_time IS NULL OR start_time <= now());`;
  const {rows} = await db.query(customer, query);
  return rows;
};

module.exports.listSubjects = async function (customer, facility) {
  if (!facility) throw new Error('facility is a required parameter');
  const query = `SELECT DISTINCT (subject_name)
                 FROM customer_report_config
                 WHERE (end_time IS NULL OR end_time >= now())
                   AND (start_time IS NULL OR start_time <= now())
                   AND (facility IS NULL OR facility = $1)
                   AND (hidden IS NOT TRUE)
                 ORDER BY subject_name;`;
  const {rows} = await db.query(customer, query, [facility]);
  return rows;
};

module.exports.findShiftsInfo = async function (customer, queryParams) {
  const q = sql(shiftsQuery)(queryParams);
  const {rows} = await db.query(customer, q.text, q.values);
  return rows.length > 0 ? rows[0].date || null : null;
};

//temporary DS to fetch shift info for a given time
module.exports.findShiftInfoForGroundOps = async function (customer, queryParams) {
  const q = sql(shiftsQuery)(queryParams);
  const {rows} = await db.query(customer, q.text, q.values);
  return rows.length > 0 ? rows[0] : null;
};

function onCacheHit(cache, options) {
  debug(`${options.profileName.split(' ')[0]}:HIT`,cache.keys);
}

function onCacheAdd(cache, options) {
  debug(`${options.profileName.split(' ')[0]}:MISS`,cache.keys);
}

const MAX_AGE = 2 * 60 * 1000;
const CACHE_OPTS = {maxAge: MAX_AGE, isPromise: true, onCacheHit, onCacheAdd};

module.exports.listFacilities = moize(listFacilitiesR, CACHE_OPTS);
