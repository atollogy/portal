const sql = require('yesql').pg;
const userdb = require('../lib/user-management');

const newQueryStatement = ({ customer, facility, gateway_id }) => {
  return `
    SELECT * FROM customer_filters WHERE 1 = 1
    ${ 'string' === typeof customer && customer.length ? ' AND customer = :customer': '' }
    ${ 'string' === typeof facility && facility.length ? ' AND facility = :facility': '' }
    ${ 'string' === typeof gateway_id && gateway_id.length ? ' AND gateway_id = :gateway_id': '' }
    ORDER BY customer, facility, gateway_id
  `;
};

const newInsertStatement = () => `INSERT INTO customer_filters (
  customer, facility, gateway_id, filter_type, measure_name, min_threshold, max_threshold, mode, is_enable, group_name
) VALUES (
  :customer, :facility, :gwId, :filterType, :measureName, :minThreshold, :maxThreshold, :filterMode, :isEnabled, :groupName
)`;

const newUpdateStatement = () => `UPDATE customer_filters SET
customer = :customer,
facility = :facility,
gateway_id = :gwId,
filter_type = :filterType,
measure_name = :measureName,
min_threshold = :minThreshold,
max_threshold = :maxThreshold,
mode = :filterMode,
is_enable = :isEnabled,
group_name = :groupName
WHERE id = :id
`;

module.exports.getFilterByCriteria = async function (parameters) {
  const parameterObject = Object.assign({ customer: '', facility:'', gateway_id: ''}, parameters);

  const statement = newQueryStatement(parameterObject);
  const query = sql(statement)(parameterObject);
  const { rows: results } = await userdb.query(query.text, query.values);

  return results.map(element => Object.assign({ _pristine: true }, element));
};

module.exports.fetchAllFilters = async function () {
  return await module.exports.getFilterByCriteria();
};

module.exports.updateFilters = async function (queryParams) {
  let sqlQ = queryParams.addNewFlag ? newInsertStatement() : newUpdateStatement();
  const statement = sql(sqlQ);
  const query = statement(queryParams);
  let {rows: results} = await userdb.query(query.text, query.values);
  return results;
};
