WITH qcfg AS (
    SELECT interval '10 minutes'                                        AS min_cycle_interval,
           interval '7 days'                                            AS max_cycle_interval,
           8                                                            AS subject_event_multiple,
           4                                                            AS place_event_multiple,
           60.0                                                         AS candidate_confidence_threshold,
           'Rozenburg'                                                  AS _facility,
           'day'                                                        AS _shift,
           '2021-11-26'                                                 AS _start_date,
           COALESCE(:date, now()::text)::timestamptz                    AS _any_datetime,       -- changed to match last version
           '2021-11-03'                                                 AS _from_date,          -- Nov 3rd was when we turned on Container OCR
           '2022-12-31'                                                 AS _to_date,
           COALESCE(:lookback_slots, '0')::int                          AS _lookback_slots,
           COALESCE(:tagNames, '{customer_tag, qc_tag}')::text[]        AS _tag_names,
           COALESCE(:filters, '{whitelist}')::text[]                    AS _tag_filters,
           COALESCE(:asset_look_back_duration_in_second,
                    '180 seconds')::INTERVAL                            AS _look_back,    -- was 60
           COALESCE(:asset_look_ahead_duration_in_second,
                    '180 seconds')::INTERVAL                            AS _look_ahead
),

shift_times_new AS (
    SELECT  DISTINCT ON (shift_date, shift_name) shift_name,
            shift_date                          date,
            shift_start                         start_time,
            shift_end                           end_time,
            shift_timezone_name
    FROM (
        SELECT  shift_date::date::text,
                shift_name,
                shift_timezone_name,
                shift_start,
                shift_end
        FROM (
          SELECT  d shift_date,
                  s.shift_name,
                  d AT TIME ZONE s.shift_timezone_name + s.start_offset shift_start,
                  d AT TIME ZONE s.shift_timezone_name + s.end_offset shift_end,
                  s.shift_timezone_name
          FROM generate_series ((SELECT _any_datetime FROM qcfg) - INTERVAL '7 days',
                                (SELECT _any_datetime FROM qcfg),
                                INTERVAL '1 day') d
          JOIN customer_shift s
            ON s.day_of_week IS NULL
              OR s.day_of_week = date_part('dow', d)
          WHERE shift_name = (SELECT _shift FROM qcfg)
            AND s.facility = (SELECT _facility FROM qcfg)
        )x
    )y
    WHERE shift_date = (SELECT _any_datetime FROM qcfg)::text
),

shift_times AS (
    SELECT  (SELECT _start_date FROM qcfg)::TIMESTAMP AT TIME ZONE s.shift_timezone_name + s.start_offset        start_time,
            (SELECT _start_date FROM qcfg)::TIMESTAMP AT TIME ZONE s.shift_timezone_name + s.end_offset          end_time
    FROM customer_shift s
    WHERE s.shift_name =  (SELECT _shift FROM qcfg)
      AND s.facility = (SELECT _facility FROM qcfg)
      AND (s.day_of_week IS NULL
            OR s.day_of_week = date_part('dow', (SELECT _start_date FROM qcfg)::TIMESTAMP AT TIME ZONE 'UTC'))
),

times AS (
    SELECT  COALESCE(
                (SELECT _from_date FROM qcfg)::TIMESTAMP AT TIME ZONE (
                        SELECT DISTINCT shift_timezone_name
                        FROM customer_shift
                        WHERE facility = (SELECT _facility FROM qcfg)),
                (SELECT start_time FROM shift_times)
            )                                                                                           start_time,
            COALESCE(
                (SELECT _to_date FROM qcfg)::TIMESTAMP AT TIME ZONE (
                        SELECT DISTINCT shift_timezone_name
                        FROM customer_shift
                        WHERE facility = (SELECT _facility FROM qcfg)),
                (SELECT end_time FROM shift_times)
            )                                                                                           end_time
),

camera_cfg as (
    SELECT distinct on (i.gateway_id, i.step_name, i.camera_id)
           i.gateway_id,
           fn_uid_name(i.gateway_id) nodename,
           b.subject_name            gw_name,
           i.step_name,
           i.step_function,
           i.camera_id,
           b.facility,
           b.zone_name,
           c.subject_name,
           c.measure_name           ms_name,    -- measure_name <-> state_name
           teb.cycle_name,
           c.extract_type,
           c.extract_key,
           c.extract_image_name,
           c.cs_config::jsonb,
           c.composite_definition::jsonb[],
           c.smoothing_look_ahead_slots look_ahead,
           c.smoothing_look_back_slots  look_back,
           c.start_time,
           c.end_time,
           c.hidden,
           c.violation,
           teb.start_cycle is_start,
           teb.end_cycle is_end,
           (not teb.start_cycle and not teb.end_cycle) is_intermediate,
           teb.is_whitelist,
           teb.priority
    FROM image_step i
    LEFT OUTER JOIN customer_binding b
        ON i.gateway_id = b.beacon_id
       AND (b.namespace = 'ad135bf7eecc08752f96')
       AND b.facility = (SELECT _facility FROM qcfg)
       AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
       AND (b.end_time IS NULL OR i.collection_time < b.end_time)
    LEFT JOIN customer_transition_event_binding teb
        ON i.gateway_id = teb.gateway_id
       AND i.camera_id = teb.camera_id
       AND i.step_name = teb.step_name
       AND i.collection_time >= teb.start_time
       AND (teb.end_time IS NULL OR i.collection_time < teb.end_time)
    LEFT JOIN customer_report_config c
        ON b.subject_name = c.gateway_name
       AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
       AND (c.end_time IS NULL OR i.collection_time < c.end_time)
       AND b.facility = c.facility
       AND i.camera_id = c.camera_id
       AND i.step_name = c.step_name
    WHERE i.collection_time BETWEEN (SELECT start_time FROM times) AND (SELECT end_time FROM times)
),

raw_cycle_data AS (
    SELECT _candidates.plate AS                                                                  alt_subject_id,
           _candidates.confidence                                                                alt_score,
           step_output ->> 'identifier'                                                          subject_id,
           (step_output ->> 'confidence')::float                                                 subject_score,
           collection_time,
           (gateway_id, camera_id, step_name, collection_time, 'annotated.jpg')::image_reference img_keys
    FROM image_step i,
         jsonb_to_recordset(
                 (i.step_output ::jsonb #> '{fullResponse,results,0}' ->> 'candidates')::jsonb) AS _candidates(plate text, confidence float, matches_template int)
    WHERE i.step_output ->> 'identifier' IS NOT NULL
      AND length(i.step_output ->> 'identifier') <= 8
      AND (step_output ->> 'confidence')::float >= (select candidate_confidence_threshold from qcfg)
      AND i.collection_time between (SELECT start_time FROM times) AND (SELECT end_time FROM times)
      AND _candidates.confidence >= (SELECT candidate_confidence_threshold FROM qcfg)
      AND json_typeof(i.step_output #> '{fullResponse,results,0,candidates}') = json_typeof('[]')
),

filtered_raw_cycle_data as(
     SELECT alt_subject_id,
            alt_score,
            subject_id,
            subject_score,
            img_keys,
            (CASE
                WHEN (levenshtein((LAG(subject_id, 1) over w), subject_id) < 2)
                        AND ((LAG(subject_score, 1) over w) > subject_score)
                    THEN TRUE
                ELSE FALSE
            END) block
     FROM raw_cycle_data
     WINDOW w AS (PARTITION BY (img_keys).gateway_id ORDER BY img_keys)
),

selections AS (
    SELECT  alt_subject_id,
            subject_id,
            (SUM(alt_score) * COUNT(DISTINCT (img_keys).gateway_id))                         score,
            RANK() OVER (PARTITION BY alt_subject_id
                ORDER BY SUM(alt_score) * (COUNT(DISTINCT (img_keys).gateway_id)) desc)      rank
    FROM filtered_raw_cycle_data
    WHERE NOT block
    GROUP BY alt_subject_id, subject_id
    ORDER BY alt_subject_id, score
),

correlations AS (
    SELECT  alt_subject_id,
            subject_id,
            reverse(subject_id)                 reversed_id,
            max(score)                          score
    FROM    selections s
    WHERE   rank = 1 and score > 100
    GROUP BY alt_subject_id, subject_id, score
    ORDER BY reversed_id, s.alt_subject_id
),

modified AS (
     SELECT (CASE
                WHEN (SELECT TRUE
                      FROM correlations c
                      WHERE rcd.alt_subject_id = c.alt_subject_id)
                    THEN (select c.subject_id
                          from correlations c
                          where rcd.alt_subject_id = c.alt_subject_id)
                WHEN (SELECT TRUE
                      FROM correlations c
                      WHERE rcd.subject_id = c.alt_subject_id)
                    THEN (select c.subject_id from correlations c where rcd.subject_id = c.alt_subject_id)
                ELSE rcd.subject_id
            END) subject_id,
            rcd.subject_score,
            floor(extract(epoch from (rcd.img_keys).collection_time)/60) slot,
            rcd.img_keys
     FROM raw_cycle_data rcd
     WHERE rcd.subject_id IS NOT NULL
     GROUP BY subject_id, subject_score, img_keys, alt_subject_id, slot
),

vehicle_annotations as (
    SELECT (CASE
                WHEN c.extract_type = 'ocr' THEN 'container_id'
                ELSE c.extract_type
           END)                                                                     attr_name,
           floor(extract(epoch from i.collection_time)/60)                          slot,
           i.collection_time                                                        ctime,
           array_agg(CASE
                WHEN c.extract_type = 'weight' THEN (i.step_output::jsonb ->> 'weight')::text
                ELSE (i.step_output::jsonb ->> 'text')
           END)                                                                     attr_value,
           (i.step_output::jsonb ->> 'warning')                                     attr_flag,
           array_agg((i.gateway_id,
            i.camera_id,
            i.step_name,
            i.collection_time,
            substring (i.images[1] FROM '_([^_]*)$'))::image_reference)             img_keys
        FROM image_step i
        LEFT OUTER JOIN customer_binding b
            ON i.gateway_id = b.beacon_id
           AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
           AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
           AND (b.end_time IS NULL OR i.collection_time < b.end_time)
        JOIN customer_report_config c
            ON b.subject_name = c.gateway_name
           AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
           AND (c.end_time IS NULL OR i.collection_time < c.end_time)
           AND b.facility = c.facility
           AND i.camera_id = c.camera_id
           AND i.step_name = c.step_name
           AND c.extract_type in ('weight', 'ocr')
        WHERE c.extract_type in ('weight', 'ocr')
          AND i.collection_time BETWEEN (SELECT start_time FROM times) AND (SELECT end_time FROM times)
          AND (
               ((i.step_output::jsonb ->> 'weight') is not null
                    AND
                (i.step_output::jsonb ->> 'weight')::int > 0)
               OR
               ((i.step_output::jsonb ->> 'text' is not null
                    AND
                i.step_output::jsonb ->> 'text' != '{}'))
              )
        GROUP BY i.collection_time, i.gateway_id, i.camera_id, i.step_name, i.images, i.step_output::jsonb, c.extract_type
),

vehicle_attributes as (
    SELECT distinct(slot) slot,
            (extract(epoch from ctime)) ctime,
           attr_name,
           (CASE
                WHEN attr_name = 'weight' THEN (select max(x::int) from unnest(attr_value) x)::text
                ELSE (fn_top_n(1, attr_value))[1]
           END) attr_value,
           attr_flag,
           (
               CASE
                    WHEN attr_name = 'weight'
                        THEN img_keys[array_position(attr_value, (select max(x::int) from unnest(attr_value) x)::text)]
                    ELSE img_keys[array_position(attr_value, (fn_top_n(1, attr_value))[1])]
               END
            ) img_keys
    FROM vehicle_annotations va
    GROUP BY ctime, slot, attr_name, attr_value, attr_flag, img_keys
),

raw_cycle_events AS (
     SELECT subject_id,
            subject_score                                           score,
            (m.img_keys).collection_time                            start_time,
            (
                subject_id,
                subject_score,
                (m.img_keys).gateway_id,
                (m.img_keys).camera_id,
                (m.img_keys).step_name,
                'lpr'::text,
                (m.img_keys).collection_time,
                cc.facility,
                cc.ms_name,
                cc.is_start,
                cc.is_end,
                cc.priority,
                cc.is_whitelist,
                m.img_keys
            )::raw_cycle_event                                      ce_record,
            v.attr                                                  vattr
     FROM modified m
     LEFT JOIN camera_cfg cc
            ON cc.gateway_id = (m.img_keys).gateway_id
           AND cc.step_name = (m.img_keys).step_name
     LEFT JOIN LATERAL (
        SELECT (CASE
                    WHEN cc.zone_name = 'scale'
                        THEN (select (attr_name, attr_value, attr_flag, img_keys)::vattr
                                from vehicle_attributes va
                               where va.attr_name = 'weight' and
                                     va.ctime between (extract(epoch from (m.img_keys).collection_time) - cc.look_back) and
                                                 (extract(epoch from (m.img_keys).collection_time) + cc.look_ahead)
                            group by ctime, slot, attr_name, attr_value, attr_flag, img_keys
                            order by va.ctime asc
                            limit 1)
                    WHEN cc.zone_name = 'container_id'
                        THEN (select (attr_name, attr_value, attr_flag, img_keys)::vattr
                                from vehicle_attributes va
                               where va.attr_name = 'container_id' and
                                     va.ctime between (extract(epoch from (m.img_keys).collection_time) - cc.look_back) and
                                                 (extract(epoch from (m.img_keys).collection_time) + cc.look_ahead)
                            group by ctime, slot, attr_name, attr_value, attr_flag, img_keys
                            order by va.ctime asc
                            limit 1)
            END) attr
     ) v on true
     GROUP BY m.subject_id, m.subject_score, m.img_keys, m.slot,
              cc.facility, cc.ms_name, cc.priority, cc.is_start, cc.is_end, cc.is_whitelist,
              cc.gateway_id, cc.subject_name, v.attr
     ORDER BY start_time DESC
),

filtered_cycle_events as(
     SELECT subject_id,
            score,
            start_time,
            vattr,
            ce_record,
            (CASE
                WHEN (levenshtein((LAG(subject_id, 1) over w), subject_id) < 2)
                        AND ((LAG(score, 1) over w) > score)
                    THEN TRUE
                ELSE FALSE
            END) block
     FROM raw_cycle_events rce WINDOW w AS (PARTITION BY (rce.ce_record).gateway_id ORDER BY start_time)
),

entry_events as (
    SELECT *
    FROM filtered_cycle_events fce
    WHERE (ce_record).start_cycle
      AND NOT block
),

exit_events as (
    SELECT *
    FROM filtered_cycle_events
    WHERE (ce_record).end_cycle
      AND NOT block
),

cycle_rank as (
    select array[ee.subject_id, xe.subject_id]                                          cycle_key,
           floor(extract(epoch from ee.start_time)/
                (select subject_event_multiple from qcfg))                              start_cycle_event,
           floor(extract(epoch from xe.start_time)/
                (select place_event_multiple from qcfg))                                end_cycle_event,
           (CASE
                WHEN ee.subject_id != xe.subject_id
                 AND ee.score  >= xe.score
                    THEN ee.subject_id
                ELSE xe.subject_id
           END)                                                                         cycle_plate,
           (CASE
                WHEN ee.subject_id != xe.subject_id
                 AND ee.score < xe.score
                    THEN xe.subject_id
                ELSE ee.subject_id
           END)                                                                         match_plate,
           ee.score                                                                     entry_score,
           xe.score                                                                     exit_score,
           word_similarity(ee.subject_id, xe.subject_id)                                plate_similarity,
           word_similarity(reverse(ee.subject_id), reverse(xe.subject_id))              rev_plate_similarity,
           ee.start_time                                                                cycle_start,
           xe.start_time                                                                cycle_end,
           min(xe.start_time - ee.start_time)                                           duration,
           (CASE
                WHEN ee.subject_id != xe.subject_id
                 AND ee.score  < xe.score
                    THEN (xe.subject_id, xe.score,
                         (ee.ce_record).gateway_id, (ee.ce_record).camera_id,
                         (ee.ce_record).step_name, (ee.ce_record).step_function,
                         (ee.ce_record).collection_time, (ee.ce_record).facility,
                         (ee.ce_record).state, (ee.ce_record).start_cycle,
                         (ee.ce_record).end_cycle, (ee.ce_record).priority,
                         (ee.ce_record).whitelist, (ee.ce_record).img_keys)::raw_cycle_event
                ELSE ee.ce_record
           END)                                                                         entry_record,
           ee.vattr                                                                    entry_vattr,
           (CASE
                WHEN ee.subject_id != xe.subject_id
                 AND ee.score >= xe.score
                    THEN (ee.subject_id, ee.score,
                         (xe.ce_record).gateway_id, (xe.ce_record).camera_id,
                         (xe.ce_record).step_name, (xe.ce_record).step_function,
                         (xe.ce_record).collection_time, (xe.ce_record).facility,
                         (xe.ce_record).state, (xe.ce_record).start_cycle,
                         (xe.ce_record).end_cycle, (xe.ce_record).priority,
                         (xe.ce_record).whitelist, (xe.ce_record).img_keys)::raw_cycle_event
                ELSE xe.ce_record
           END)                                                                         exit_record,
           xe.vattr                                                                    exit_vattr,
           rank() over (PARTITION BY floor(extract(epoch from ee.start_time)/(select place_event_multiple from qcfg))
                order by min(xe.start_time - ee.start_time),
                         max(word_similarity(ee.subject_id, xe.subject_id)))   rank
    from entry_events ee
    inner join exit_events xe on
        ((ee.subject_id = xe.subject_id) or
         ((word_similarity(reverse(ee.subject_id), reverse(xe.subject_id)) > 0.4) AND
          (word_similarity(ee.subject_id, xe.subject_id) +
          word_similarity(reverse(ee.subject_id), reverse(xe.subject_id)) > 0.85) AND
          (word_similarity(ee.subject_id, xe.subject_id) +
          word_similarity(reverse(ee.subject_id), reverse(xe.subject_id)) < 1.15))) AND
         xe.start_time between ee.start_time and ee.start_time + (select max_cycle_interval from qcfg)
    group by ee.subject_id, start_cycle_event, end_cycle_event, xe.subject_id,
             ee.start_time, xe.start_time, ee.score, xe.score, entry_record, entry_vattr, exit_record, exit_vattr
),

cycles as (
   select cycle_key,
          start_cycle_event,
          cycle_plate,
          match_plate,
          entry_score,
          exit_score,
          plate_similarity,
          rev_plate_similarity,
          rank,
          cycle_start,
          cycle_end,
          duration,
          entry_record,
          entry_vattr,
          exit_record,
          exit_vattr,
          (CASE
               WHEN (lag(cycle_key, 1) over w) && cycle_key
                    THEN TRUE
               ELSE FALSE
          END) block
    from cycle_rank
    where rank = 1
     and duration >= (select min_cycle_interval from qcfg)
    GROUP BY cycle_plate, start_cycle_event, plate_similarity, rev_plate_similarity, cycle_key,
             rank, cycle_start, cycle_end, duration, entry_record, entry_vattr, exit_record, exit_vattr,
             match_plate, entry_score, exit_score
    WINDOW w AS (ORDER BY start_cycle_event desc)
    order by  cycle_plate, start_cycle_event, entry_score
),

image_event_transition as (
    select (x.subject_id)                            subject_id,
           (x.score)                                 score,
           (x.gateway_id)                            gateway_id,
           (x.camera_id)                             camera_id,
           (x.step_name)                             step_name,
           (x.collection_time)                       start_time,
           (x.gateway_id)                            beacon_id,
           (x.subject_id)                            gw_name,
           (x.facility)                              facility,
           (x.state)                                 state,
           (x.start_cycle)                           start_cycle,
           (x.end_cycle)                             end_cycle,
           (x.priority)                              priority,
           (x.whitelist)                             is_whitelist,
           (x.vattr)                                 vattr,
           (x.img_keys)                              img_keys
    from (
        select (entry_record).*,
               entry_vattr vattr
            from cycles where not block
        union
        select (exit_record).*,
               exit_vattr vattr
            from cycles where not block
        union
        select (fce.ce_record).*,
               vattr
            from raw_cycle_events fce
            where not (fce.ce_record).start_cycle
              and not (fce.ce_record).end_cycle
              and fce.subject_id in (select cycle_plate from cycles
                                        union
                                     select match_plate from cycles)
    ) x
),

extended AS (
    SELECT  namespace,
            beacon_id,
            gateway_id,
            gateway_name,
            gateway_facility,
            gateway_radius,
            slot + (ofst * INTERVAL '1 minute') slot,
            (CASE
                -- no time gap
                WHEN ofst = 0 THEN rssi
                -- fill IN small gaps with the worse of the two values
                WHEN gap <= 5 AND ofst < gap THEN LEAST(future_rssi, rssi)
            END)  rssi,
            (CASE
                WHEN ofst = 0 THEN reading_count
                WHEN gap <= 5 AND ofst < gap THEN 0
            END) reading_count,
            tx_power
    FROM (
        SELECT  namespace,
                beacon_id,
                gateway_id,
                gateway_name,
                gateway_facility,
                gateway_radius,
                slot,
                rssi,
                tx_power,
                reading_count,
                CASE
                    WHEN gap > 1 THEN (SELECT * FROM generate_series(0, LEAST(gap-1, 4), 1))
                    ELSE 0
                END ofst,
                gap,
                future_rssi
        FROM (
            SELECT  r.namespace,
                    r.beacon_id,
                    r.gateway_id,
                    gateway_binding.facility gateway_facility,
                    COALESCE(gateway_binding.zone_name, gateway_binding.subject_name, r.node_name) gateway_name,
                    r.slot,
                    r.avg_rssi rssi,
                    r.reading_count,
                    r.tx_power_1m tx_power,
                    EXTRACT(EPOCH FROM LEAD(r.slot) OVER (PARTITION BY r.namespace, r.beacon_id, r.gateway_id ORDER BY r.slot) - r.slot)::NUMERIC / 60 gap,
                    LEAD(r.avg_rssi) OVER (PARTITION BY r.namespace, r.beacon_id, r.gateway_id ORDER BY r.slot) future_rssi,
                    gateway_binding.radius gateway_radius
            FROM beacon_reading r
            LEFT OUTER JOIN customer_binding gateway_binding
               ON 'ad135bf7eecc08752f96' = gateway_binding.namespace -- the Atollogy Edystone gateway namespace
              AND r.gateway_id = gateway_binding.beacon_id
              AND (gateway_binding.start_time IS NULL OR r.slot >= gateway_binding.start_time)
              AND (gateway_binding.end_time IS NULL OR r.slot < gateway_binding.end_time)
            JOIN customer_beacon_transition_event_binding b
               ON b.zone_name = COALESCE(gateway_binding.zone_name, gateway_binding.subject_name, r.node_name)
              AND b.facility = gateway_binding.facility
            JOIN times t
               ON r.slot >= t.start_time
              AND r.slot < t.end_time
            WHERE (gateway_binding.radius IS NULL OR gateway_binding.radius > 0)
              AND gateway_binding.subject = 'gateway'
        ) AS x
    ) AS y
),

moving_avg AS (
    SELECT  namespace,
            beacon_id,
            gateway_id,
            gateway_facility,
            gateway_name,
            gateway_radius,
            slot,
            (CASE
               WHEN reading_count IS NOT NULL THEN (SUM(reading_count::float) OVER w / COUNT(reading_count) OVER w)
            END) reading_count,
            (CASE
               WHEN rssi IS NOT NULL THEN (SUM(rssi) OVER w  / COUNT(rssi) OVER w)
            END) rssi,
            (COUNT(rssi) OVER w) tx_power
    FROM extended
         WINDOW w AS (PARTITION BY namespace, beacon_id, gateway_id ORDER BY slot ROWS BETWEEN (SELECT _lookback_slots FROM qcfg) PRECEDING AND CURRENT ROW)
),

-- This has the smoothed RSSI AND distance by beacon AND gateway
smoothed AS (
    SELECT namespace,
           beacon_id,
           gateway_id,
           gateway_facility,
           gateway_name,
           gateway_radius,
           slot,
           reading_count,
           rssi,
           tx_power,
           POW(10, (tx_power - rssi) / 20.0) distance
    FROM moving_avg
),

-- this has the basic winner election of the best gateway for each beacon
winner AS (
    SELECT  b.subject subject,
            winner.distance score,
            winner.namespace,
            winner.beacon_id,
            winner.slot,
            winner.gateway_id,
            winner.distance,
            COALESCE(b.subject_name, winner.beacon_id) beacon_name,
            winner.gateway_facility,
            winner.gateway_name,
            b.binding_id beacon_binding
    FROM (
        SELECT  DISTINCT ON (namespace, beacon_id, slot) namespace,
                beacon_id,
                slot,
                distance,
                gateway_id,
                gateway_facility,
                gateway_name
        FROM smoothed
        WHERE rssi IS NOT NULL
          AND distance <= COALESCE(gateway_radius, 10)
        ORDER BY 1, 2, 3, 4 ASC
    ) winner
    LEFT OUTER JOIN customer_binding b
        ON  b.subject <> 'gateway'
       AND winner.beacon_id = b.beacon_id
       AND (b.start_time IS NULL OR winner.slot >= b.start_time)
       AND (b.end_time IS NULL OR winner.slot < b.end_time)
),

-- requires customer_beacon_transition_event_binding: zone_name, start_cycle, end_cycle
beacon_event_transition AS (
    SELECT  w.beacon_id,
            w.distance score,
            w.slot start_time,
            w.beacon_name,
            b.facility,
            COALESCE(b.state_name, w.gateway_name) state,
            b.start_cycle,
            b.end_cycle,
            FALSE as is_whitelist,
            b.priority
    FROM winner w
    JOIN customer_beacon_transition_event_binding b
      ON b.zone_name = w.gateway_name
     AND b.facility = w.gateway_facility
),

summarized_asset_attribute AS (
    SELECT  asset_id,
            MIN(latest_seen_time)                                                                                               latest_seen_time,
            json_object_agg(qualifier, json_build_object('attr_value', attribute_value, 'last_seen', latest_seen_time))::jsonb  attributes,
            array_remove(ARRAY_AGG(DISTINCT(CASE WHEN a.qualifier in (select * from unnest((SELECT _tag_names FROM qcfg))x) THEN attribute_value ELSE NULL END)), NULL) tags,
            (array_to_json(array_remove(ARRAY_AGG(a.asset_info -> 'source_image_key'), NULL))::jsonb -> -1)                     asset_img_keys
    FROM (
        SELECT  DISTINCT ON (asset_id, qualifier) a.*
        FROM asset_attribute a
        JOIN times t
          ON (a.latest_seen_time IS NULL)
            OR (a.latest_seen_time BETWEEN t.start_time AND t.end_time)
        ORDER BY asset_id, qualifier, latest_seen_time DESC
    ) AS a
    GROUP BY asset_id
),

asset_img_transition AS (
    SELECT  iet.subject_id,
            iet.score,
            iet.start_time,
            iet.gw_name,
            iet.facility,
            iet.state,
            iet.start_cycle,
            iet.end_cycle,
            iet.is_whitelist,
            iet.priority,
            lia.attributes,
            lia.tags,
            iet.vattr,
            iet.img_keys,
            lia.asset_img_keys
    FROM image_event_transition iet
    LEFT OUTER JOIN summarized_asset_attribute lia
       ON iet.subject_id = lia.asset_id
    JOIN times t
       ON iet.start_time BETWEEN t.start_time AND t.end_time
),

asset_beacon_transition AS (
    SELECT  saa.asset_id        subject_id,
            bet.score,
            bet.start_time,
            'beacon'::text      gw_name,
            bet.facility,
            bet.state,
            bet.start_cycle,
            bet.end_cycle,
            bet.is_whitelist,
            bet.priority,
            saa.attributes,
            saa.tags,
            null::vattr,
            null::image_reference,
            saa.asset_img_keys
    FROM beacon_event_transition bet
    JOIN summarized_asset_attribute saa
      ON bet.beacon_name = (saa.attributes->'beacon_id'->>'attr_value')
),

asset_transition AS (
    SELECT * FROM asset_img_transition
    UNION
    SELECT * FROM asset_beacon_transition
    WHERE subject_id IS NOT NULL
),

filtered_items AS (
    SELECT asset_id subject_id
    FROM asset_attribute
    WHERE attribute_value in (select * from unnest((SELECT _tag_filters FROM qcfg))x)
      AND qualifier = 'qc_tag'
),

-- remove duplicate readings
coalesced_event_transition AS (
    SELECT  MIN(start_time) start_time,
            MAX(start_time) max_start_time,
            subject_id,
            score,
            tags,
            attributes,
            facility,
            state,
            bool_or(start_cycle) start_cycle,
            bool_or(end_cycle) end_cycle,
            bool_or(is_whitelist) is_whitelist,
            AVG(priority) cycle_priority,
            (CASE
                WHEN (vattr).attr_value = '{}' THEN null
                WHEN length((vattr).attr_value) > 8 THEN container_number((vattr).attr_value)
                ELSE (vattr).attr_value
            END) weight,
           json_build_object(
                'lpr', array_to_json(array_remove(ARRAY_AGG(DISTINCT (img_keys)), NULL)),
                'asset', array_to_json(array_remove(ARRAY_AGG(DISTINCT (asset_img_keys)), NULL)),
                'weight', array_to_json(array_remove(ARRAY_AGG(DISTINCT(
                            CASE
                               WHEN (vattr).attr_value is not NULL AND length((vattr).attr_value) > 3
                                   THEN (vattr).img_keys
                               ELSE null
                            END)), null))
               ) img_keys
    FROM (
        SELECT  *,
                SUM(transition_count) OVER (PARTITION BY facility, subject_id
                                                ORDER BY start_time ASC) grp
        FROM (
            SELECT  *,
                    (CASE
                        WHEN state = (LAG(state) OVER w) AND
                             start_time < ((LAG(start_time) OVER w) + interval '16 seconds')
                            THEN 0
                        ELSE 1
                    END) transition_count
            FROM asset_transition
                WINDOW w AS (PARTITION BY facility, subject_id ORDER BY start_time ASC)
        ) AS a
    ) AS b
    GROUP BY  subject_id, facility, state, grp, tags, attributes, score, weight
),

cycle_states AS (
    SELECT  start_time,
            end_time,
            subject_id,
            facility,
            state,
            weight,
            (SUM(cycle_cnt) OVER w) cycle_id,
            is_start,
            is_end,
            is_whitelist,
            priority,
            null as warning,
            (CASE
                WHEN x.is_end THEN '{whitelist}'::text[]
                ELSE x.tags
            END) tags,
                   (CASE
            WHEN x.attributes is null and x.is_whitelist
                THEN json_build_object(
                        'qc_tag', json_build_object('last_seen', x.start_time::text, 'attr_value', 'whitelist')::jsonb,
                        'license_plate', json_build_object('last_seen', x.start_time::text, 'attr_value', subject_id )::jsonb
                    )::jsonb
                ELSE x.attributes
            END) attributes,
            img_keys
    FROM (
        SELECT  *,
                max_start_time end_time,
                (CASE
                    WHEN start_cycle IS TRUE AND end_cycle IS FALSE AND (LAG(start_cycle) OVER w) IS FALSE THEN 1
                    ELSE 0
                END) cycle_cnt,
                start_cycle is_start,
                end_cycle is_end,
                cycle_priority priority
        FROM coalesced_event_transition
            WHERE ((SELECT COUNT(*) FROM filtered_items) = 0 OR(subject_id IN (SELECT subject_id FROM filtered_items))) --if whitelist filter supplied THEN apply it
              AND (subject_id NOT IN (SELECT asset_id FROM asset_attribute WHERE attribute_value = 'blacklist' AND qualifier = 'qc_tag')) --remove blacklisted LPs
        WINDOW w AS (PARTITION BY facility, subject_id ORDER BY start_time ASC)
    ) x
    WINDOW w AS (PARTITION BY facility, subject_id ORDER BY start_time ASC)
),

visit_log AS (
  SELECT *
  FROM(
      SELECT  DISTINCT ON (facility, subject_id, start_time)
              facility,
              subject_id,
              subject_id || COALESCE(' (' || (attributes -> 'Truck Type' ->> 'attr_value') || ':#' || (attributes -> 'Unit Number' ->> 'attr_value') || ')', '') extended_subject_id,
              state last_state,
              start_time last_state_start_time,
              COUNT(CASE WHEN is_start THEN 1 END) OVER w visit_count,
              img_keys,
              is_start,
              is_end,
              priority,
              tags,
              attributes
      FROM cycle_states
      WHERE (is_start = TRUE OR is_end = TRUE)
          WINDOW w AS (PARTITION BY subject_id, facility)
      ORDER BY facility, subject_id, start_time, COALESCE(end_time, start_time) DESC
  ) x
  ORDER BY x.subject_id, x.last_state_start_time DESC
),

cycle_log AS (
    SELECT  start_time,
            (CASE WHEN end_time IS NULL THEN now() ELSE end_time END) end_time,
            subject_id || COALESCE(' (' || (cwwoi.attributes -> 'Truck Type' ->> 'attr_value') || ':#' || (cwwoi.attributes -> 'Unit Number' ->> 'attr_value') || ')', '') subject_id,
            (CASE
                WHEN is_whitelist THEN array['whitelist']::text[]
                ELSE cwwoi.tags
            END) tags,
            (CASE
                WHEN is_whitelist
                    THEN json_build_object(
                            'qc_tag', json_build_object('last_seen', start_time::text,
                                                         'attr_value', 'whitelist')::jsonb,
                            'license_plate', json_build_object('last_seen', start_time::text,
                                                               'attr_value', subject_id )::jsonb
                    )::jsonb
                ELSE cwwoi.attributes
            END) attributes,
            facility,
            state,
            cycle_id,
            weight,
            img_keys,
            is_start,
            is_end,
            priority,
            warning,
            COUNT(CASE WHEN is_start THEN 1 END) OVER w visit_count
    FROM cycle_states cwwoi
         WINDOW w AS (PARTITION BY subject_id, facility)
    ORDER BY start_time DESC
)

--select *,
--       fn_permalink_from_jsonb_image_reference(img_keys::jsonb #> '{weight}' -> 0) image_link
--from cycle_states;

/**********************************************************/
/**********************************************************/
/***************** ROBS SQL STARTS HERE *******************/
/**********************************************************/
/**********************************************************/
--/**
SELECT
 cs.start_time
,cs.end_time
,cs.subject_id
,cs.tags
,cs.attributes
,cs.facility
,cs.state
,cs.cycle_id
,(CASE
   WHEN cs.weight IS NULL THEN 'LPN'
   WHEN cs.state IN('Scale Path 1','Scale Path 2') THEN 'WEIGHT'
   WHEN cs.state IN('Loading A1','Loading A2','Loading B1','Loading B2') THEN 'CONTAINER'
   ELSE 'OTHER'
END) as event_type
,cs.weight as event_value
,cs.img_keys
,fn_permalink_from_image_reference(jsonb_populate_record(null::image_reference, cs.img_keys::jsonb #>  '{weight,0}')) image_link
,fn_permalink_from_image_reference(jsonb_populate_record(null::image_reference, cs.img_keys::jsonb #>  '{lpr,0}')) image_link_lpn
--,fn_permalink_from_image_reference((cs.img_keys ->> '{weight.0}')::image_reference) image_link      -- constructed Line 757
--,fn_permalink_from_image_reference((cs.img_keys ->> '{lpr.0}')::image_reference) image_link_lpn
,cs.is_start
,cs.is_end
,cs.is_whitelist
,cs.priority
,cs.warning
FROM
cycle_states cs
WHERE cs.subject_id NOT LIKE '%118%'
ORDER BY 3,8,1  -- LPN, cycle id, start date
--**/

/**********************************************************/
/**  Grouped retrieval to look for complete cycle data   **/
/**********************************************************/
/**
SELECT
 cwo.subject_id	as trailer_lpn
,cwo.cycle_id
,cwo.facility
,cwo.state
,(CASE
   WHEN cwo.weight IS NULL THEN                 '1.LPN'
   WHEN length(cwo.weight) > 8 THEN             '3.CONTAINER'
   WHEN cwo.weight::INTEGER < 16000 THEN        '2.WEIGHT_TARE'        -- Use the weight to determine tare
   ELSE                                         '4.WEIGHT_FULL'
END) as event_type
--,cwo.weight as event_value
,(CASE
   WHEN cwo.weight IS NULL THEN cwo.subject_id
   ELSE cwo.weight
END) as event_value
,cwo.is_start
,cwo.is_end
,count(*)
FROM
cycle_states cwo
WHERE cwo.subject_id NOT LIKE '%118%'
GROUP BY 1,2,3,4,5,6,7,8
ORDER BY 1,2,5
;
**/
