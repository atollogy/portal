WITH
shift_times AS (
  SELECT shift_name, date::date::text, start_time shift_start, end_time shift_end, shift_timezone_name
  FROM (
    SELECT
      date,
      shift_name,
      shift_timezone_name,
      CASE WHEN :previous THEN (lag(shift_start) OVER (ORDER BY date)) ELSE shift_start END start_time,
      CASE WHEN :previous THEN (lag(shift_end) OVER (ORDER BY date)) ELSE shift_end END end_time
    FROM (
      SELECT
        d date,
        s.shift_name,
        d AT TIME ZONE s.shift_timezone_name + s.start_offset shift_start,
        d AT TIME ZONE s.shift_timezone_name + s.end_offset shift_end,
        s.shift_timezone_name
      FROM generate_series(
               COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE - INTERVAL '7 days',
               COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE + INTERVAL '7 days',
               INTERVAL '1 day'
           ) d
      JOIN customer_shift s ON s.day_of_week IS NULL OR s.day_of_week = date_part('dow', d)
      WHERE ((:shift::text IS NULL) OR shift_name = :shift) AND (facility IS NULL OR s.facility = :facility)
    )x
  )y
  WHERE date = COALESCE(:date, (now()::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE shift_timezone_name)::date)
),
times as (
    SELECT
      coalesce(:from::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM shift_times), (select min(shift_start) from shift_times)) start_time,
      coalesce(:to::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM shift_times), (select max(shift_end) from shift_times)) end_time
),
filtered_image_step AS (
  SELECT *
  FROM image_step
  WHERE calculate_slot(collection_time,collection_interval) BETWEEN (SELECT start_time FROM times) AND (SELECT end_time FROM times)
),
weight AS (
  SELECT

    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    CASE
      WHEN (i.step_output ->> 'brightness') :: float > 140 OR (i.step_output ->> 'brightness') :: float <= 45 THEN 'BRIGHTNESS EXCEPTION'
      ELSE COALESCE(i.step_output->>'reason', i.step_output->>'warning')
    END exception,
    CASE
      WHEN i.step_output ->> 'status' = 'unknown' OR i.step_output ->> 'status' = 'error' THEN NULL -- previously 'MISSING DATA'
      ELSE (i.step_output ->> c.extract_key)::integer
    END state,
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
      END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
     i.gateway_id = b.beacon_id
     AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
     AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
     AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
		AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'weight'
  LEFT JOIN unnest(i.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
)