SELECT r.namespace, r.beacon_id, b.subject, b.subject_name 
FROM (SELECT namespace, beacon_id FROM beacon_reading WHERE slot > now() - INTERVAL '1 day' GROUP BY namespace, beacon_id) r 
LEFT JOIN customer_binding b 
  ON b.beacon_id = r.beacon_id AND b.namespace = r.namespace AND (b.start_time IS NULL OR now() >= b.start_time) AND (b.end_time IS NULL OR now() < b.end_time)