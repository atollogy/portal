-- requires either from and to or shift and day to be set

WITH shift_times_new AS (
  SELECT DISTINCT ON (date, shift_name) shift_name, date::date::text, shift_start start_time, shift_end end_time, shift_timezone_name
  FROM (
    SELECT
      date,
      shift_name,
      shift_timezone_name,
      shift_start ,
      shift_end
    FROM (
      SELECT
        d date,
        s.shift_name,
        d AT TIME ZONE s.shift_timezone_name + s.start_offset shift_start,
        d AT TIME ZONE s.shift_timezone_name + s.end_offset shift_end,
        s.shift_timezone_name
      FROM generate_series(
               COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE - INTERVAL '7 days',
               COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE,
               INTERVAL '1 day'
           ) d
      JOIN customer_shift s ON s.day_of_week IS NULL OR s.day_of_week = date_part('dow', d)
      WHERE ((:shift::text IS NULL) OR shift_name = :shift) AND facility = :facility
    )x
  )y
  WHERE date = COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE
),
shift_times AS(
            SELECT
                    to_timestamp(:date,'YYYY-MM-DD')::timestamp without time zone AT TIME ZONE s.shift_timezone_name + s.start_offset start_time,
                    to_timestamp(:date,'YYYY-MM-DD')::timestamp without time zone AT TIME ZONE s.shift_timezone_name + s.end_offset end_time
            FROM customer_shift s
            WHERE
                    s.shift_name = :shift AND
                    (CAST(:facility AS TEXT) IS NULL OR COALESCE(s.facility, :facility) = :facility) AND
                    (s.day_of_week is null OR s.day_of_week = date_part('dow', to_timestamp(:date,'YYYY-MM-DD')::timestamp without time zone AT TIME ZONE 'UTC'))),
times as (
    select
        coalesce(:from::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM customer_shift WHERE (facility = :facility OR facility IS NULL)), (select start_time from shift_times)) start_time,
                coalesce(:to::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM customer_shift WHERE (facility = :facility OR facility IS NULL)), (select end_time from shift_times)) end_time
),
image_event_transition AS (
    SELECT
        s.collection_time start_time,
        (s.gateway_id,s.camera_id,s.step_name,s.collection_time,'annotated.jpg')::image_reference img_keys,
        s.step_output->>'identifier' subject_id,
        s.gateway_id,
        b.beacon_id,
        b.subject_name gw_name,
        teb.facility,
        teb.state_name state,
        teb.start_cycle,
        teb.end_cycle,
    teb.priority,
        teb.is_whitelist,
    (c.extract_threshold_definitions -> 'skew_calibration' ->> 'enabled')::bool enabled,
    (c.extract_threshold_definitions -> 'skew_calibration' ->> 'metric') metric,
    (c.extract_threshold_definitions -> 'skew_calibration' -> 'bounds' ->> '>')::float lower,
    (c.extract_threshold_definitions -> 'skew_calibration' -> 'bounds' ->> '<')::float upper,
    (s.step_output->'skew_metrics') skew_metrics
    FROM image_step s
    JOIN customer_binding b
            ON s.gateway_id = b.beacon_id
                 AND b.facility = :facility
    JOIN customer_transition_event_binding teb
      ON s.gateway_id = teb.gateway_id
      AND s.camera_id = teb.camera_id
      AND s.step_name = teb.step_name
      AND (teb.start_time IS NULL OR s.collection_time >= teb.start_time)
      AND (teb.end_time IS NULL OR s.collection_time < teb.end_time)
  LEFT JOIN customer_report_config c ON
      b.subject_name = c.gateway_name
      AND (c.start_time IS NULL OR s.collection_time >= c.start_time)
      AND (c.end_time IS NULL OR s.collection_time < c.end_time)
      AND b.facility = c.facility
      AND s.camera_id = c.camera_id
      AND s.step_name = c.step_name
    WHERE
        s.step_output->>'identifier' IS NOT NULL
    AND coalesce(:facility, teb.facility) = teb.facility
        AND s.collection_time BETWEEN (SELECT start_time FROM times) AND (SELECT end_time FROM times)
),

filter_on_skew AS (
  SELECT *
  FROM image_event_transition
  WHERE (skew_metrics->>metric)::float IS NULL
        OR enabled IS FALSE
        OR enabled IS NULL
        OR ((lower IS NULL OR (skew_metrics->>metric)::float > lower)
            AND (upper IS NULL OR (skew_metrics ->> metric) :: float < upper
        ))
),

extended AS (
    SELECT
        namespace,
        beacon_id,
        gateway_id,
        gateway_name,
        gateway_facility,
        gateway_radius,
        slot + (ofst * INTERVAL '1 minute') slot,
        CASE WHEN ofst = 0 THEN rssi
             WHEN gap <= 5 AND ofst < gap THEN LEAST(future_rssi, rssi) -- fill in small gaps with the worse of the two values
             END rssi,
        CASE WHEN ofst = 0 THEN reading_count WHEN gap <= 5 AND ofst < gap THEN 0 END reading_count,
        tx_power
    FROM (
        SELECT
      namespace,
      beacon_id,
      gateway_id,
      gateway_name,
      gateway_facility,
      gateway_radius,
      slot,
      rssi,
      tx_power,
      reading_count,
      CASE WHEN gap > 1 THEN (select * from generate_series(0, LEAST(gap-1, 4), 1)) ELSE 0 END ofst,
      gap,
      future_rssi
        FROM (
            SELECT r.namespace,
                r.beacon_id,
                r.gateway_id,
                gateway_binding.facility gateway_facility,
                coalesce(gateway_binding.zone_name, gateway_binding.subject_name, r.node_name) gateway_name,
                r.slot,
                r.avg_rssi rssi,
                r.reading_count,
                r.tx_power_1m tx_power,
                extract(epoch from lead(r.slot) OVER (PARTITION BY r.namespace, r.beacon_id, r.gateway_id ORDER BY r.slot) - r.slot)::NUMERIC / 60 gap,
                lead(r.avg_rssi) OVER (PARTITION BY r.namespace, r.beacon_id, r.gateway_id ORDER BY r.slot) future_rssi,
                gateway_binding.radius gateway_radius
            FROM beacon_reading r
            LEFT OUTER JOIN customer_binding gateway_binding ON
                'ad135bf7eecc08752f96' = gateway_binding.namespace AND -- the Atollogy Edystone gateway namespace
                r.gateway_id = gateway_binding.beacon_id AND
                (gateway_binding.start_time IS NULL OR r.slot >= gateway_binding.start_time) AND
                (gateway_binding.end_time IS NULL OR r.slot < gateway_binding.end_time)
            JOIN customer_beacon_transition_event_binding b
                ON b.zone_name = coalesce(gateway_binding.zone_name, gateway_binding.subject_name, r.node_name)
                AND b.facility = gateway_binding.facility
            JOIN times t
                ON r.slot >= t.start_time
                AND r.slot < t.end_time
            WHERE (gateway_binding.radius IS NULL OR gateway_binding.radius > 0)
                  AND gateway_binding.subject = 'gateway'
        ) x
    ) y
),

moving_avg AS (
  SELECT namespace, beacon_id, gateway_id, gateway_facility, gateway_name, gateway_radius, slot, CASE WHEN reading_count is not null THEN (sum(reading_count::float) OVER w  / count(reading_count) OVER w) END reading_count, CASE WHEN rssi is not null THEN (sum(rssi) OVER w  / count(rssi) OVER w) END rssi, count(rssi) over w, tx_power
    FROM extended
    WINDOW w AS (PARTITION BY namespace, beacon_id, gateway_id ORDER BY slot ROWS BETWEEN :lookback_slots PRECEDING AND CURRENT ROW)
),

-- This has the smoothed RSSI and distance by beacon and gateway
smoothed AS (
    select namespace, beacon_id, gateway_id, gateway_facility, gateway_name, gateway_radius, slot, rssi, tx_power, reading_count, POW(10, (tx_power - rssi) / 20.0) distance from moving_avg
),

-- this has the basic winner election of the best gateway for each beacon
winner AS (
    SELECT
    winner.namespace,
    winner.beacon_id,
    winner.slot,
    winner.gateway_id,
    winner.distance,
    b.subject subject,
    coalesce(b.subject_name, winner.beacon_id) beacon_name,
    winner.gateway_facility,
    winner.gateway_name,
    b.binding_id beacon_binding
    FROM (
        SELECT DISTINCT ON (namespace, beacon_id, slot) namespace,
            beacon_id,
            slot,
            distance,
            gateway_id,
            gateway_facility,
            gateway_name
        FROM smoothed
        WHERE rssi IS NOT NULL AND distance <= coalesce(gateway_radius, 10)
        ORDER BY 1, 2, 3, 4 ASC
    ) winner
    LEFT OUTER JOIN customer_binding b ON
        b.subject <> 'gateway' AND
        winner.beacon_id = b.beacon_id AND
        (b.start_time IS NULL OR winner.slot >= b.start_time) AND
        (b.end_time IS NULL OR winner.slot < b.end_time)
),

-- requires customer_beacon_transition_event_binding: zone_name, start_cycle, end_cycle
beacon_event_transition AS (
    SELECT
        w.slot start_time,
        w.beacon_id,
        w.beacon_name,
        b.facility,
        coalesce(b.state_name, w.gateway_name) state,
        b.start_cycle,
        b.end_cycle,
    b.priority
    FROM winner w
    JOIN customer_beacon_transition_event_binding b
        ON b.zone_name = w.gateway_name
        AND b.facility = w.gateway_facility
),

summarized_asset_attribute AS (
    SELECT
        asset_id,
        min(latest_seen_time) latest_seen_time,
        json_object_agg(qualifier,
                                        json_build_object('attr_value', attribute_value,
                                                                            'last_seen', latest_seen_time)
        )::jsonb attributes,
        array_remove(array_agg(distinct(CASE WHEN a.qualifier = ANY(:tagNames) THEN attribute_value ELSE NULL END)), NULL) tags,
    array_to_json(array_remove(array_agg(a.asset_info -> 'source_image_key'), NULL))::jsonb -> -1 asset_img_keys
    FROM (
    SELECT DISTINCT ON (asset_id, qualifier) a.*
    FROM asset_attribute a
    JOIN times t ON (a.latest_seen_time IS NULL) OR (a.latest_seen_time BETWEEN t.start_time AND t.end_time)
    ORDER BY asset_id, qualifier, latest_seen_time DESC
  )a
  GROUP BY asset_id
),

asset_img_transition AS (
    SELECT
        iet.start_time,
        iet.subject_id,
        iet.gw_name,
      iet.facility,
        iet.state,
        iet.start_cycle,
        iet.end_cycle,
    iet.priority,
        lia.attributes,
        lia.tags,
        iet.img_keys,
    lia.asset_img_keys
        FROM filter_on_skew iet
        LEFT OUTER JOIN summarized_asset_attribute lia ON iet.subject_id = lia.asset_id
        JOIN times t ON iet.start_time BETWEEN t.start_time AND t.end_time
),

asset_beacon_transition AS (
    SELECT
      bet.start_time,
      saa.asset_id subject_id,
      'beacon'::text gw_name,
      bet.facility,
      bet.state,
      bet.start_cycle,
      bet.end_cycle,
      bet.priority,
      saa.attributes,
      saa.tags,
      null::image_reference,
      saa.asset_img_keys
    FROM beacon_event_transition bet
    JOIN summarized_asset_attribute saa ON bet.beacon_name = (saa.attributes->'beacon_id'->>'attr_value')
),

asset_transition AS (
    SELECT * FROM asset_img_transition
    UNION
    SELECT * FROM asset_beacon_transition
    WHERE subject_id IS NOT NULL
),

filtered_items AS (
    SELECT asset_id subject_id
    FROM asset_attribute
    WHERE attribute_value = ANY(:filters) AND qualifier = 'qc_tag'
),

-- remove duplicate readings
coalesced_event_transition AS (
    SELECT
        min(start_time) start_time,
        max(start_time) max_start_time,
        subject_id,
        tags,
        attributes,
        facility,
        state,
        bool_or(start_cycle) start_cycle,
        bool_or(end_cycle) end_cycle,
    avg(priority) cycle_priority,
        json_build_object('lpr', array_to_json(array_remove(array_agg(distinct(img_keys)), NULL)),
                       'asset', array_to_json(array_remove(array_agg(distinct(asset_img_keys)), NULL))
                      )::jsonb img_keys
    FROM (
        SELECT *,
            sum(transition_count) OVER (PARTITION BY facility, subject_id ORDER BY start_time ASC) grp
        FROM (
            SELECT *,
                CASE WHEN state = lag(state) OVER w AND start_time < lag(start_time) OVER w + INTERVAL '2 minutes'
                     THEN 0
                ELSE 1 END transition_count
            FROM asset_transition
            WINDOW w AS (PARTITION BY facility, subject_id ORDER BY start_time ASC)
        ) a
    ) b
    GROUP BY subject_id, facility, state, grp, tags, attributes
),

cycle_states AS (
    SELECT *, SUM(cycle_cnt) OVER w cycle_id
    FROM (
        SELECT *,
            max_start_time end_time,
            (CASE WHEN start_cycle IS TRUE AND end_cycle IS FALSE AND (lag(start_cycle) OVER w) IS FALSE THEN 1 ELSE 0 END) cycle_cnt,
            start_cycle is_start,
            end_cycle is_end,
            cycle_priority priority
        FROM coalesced_event_transition
        WHERE ((SELECT COUNT(*) FROM filtered_items) = 0 OR subject_id IN (SELECT subject_id FROM filtered_items)) --if whitelist filter supplied then apply it
            AND subject_id NOT IN (SELECT asset_id FROM asset_attribute WHERE attribute_value = 'blacklist' AND qualifier = 'qc_tag') --remove blacklisted LPs
        WINDOW w AS (PARTITION BY facility, subject_id ORDER BY start_time ASC)
    )x
    WINDOW w AS (PARTITION BY facility, subject_id ORDER BY start_time ASC)
),

cycle_weights AS (
  SELECT
    DISTINCT ON (a.subject_id, a.facility, a.start_time, a.end_time, a.cycle_id)
    a.start_time,
    a.end_time,
    a.subject_id,
    a.tags,
    a.attributes,
    a.facility,
    a.state,
    b.img_keys::jsonb || json_build_object('weight', array_to_json(array_remove(array_agg(distinct(a.img_keys)), NULL)))::jsonb img_keys,
    a.cycle_id,
    a.weight,
    a.is_start,
    a.is_end,
    a.priority,
    a.warning
  FROM (
     SELECT
     DISTINCT ON (s.subject_id, s.facility, s.start_time, s.end_time, s.cycle_id)
         s.start_time,
         s.end_time,
         s.subject_id,
               x.gw_name,
         s.facility,
         s.cycle_id,
         s.state,
         x.val weight,
         x.img_keys,
         s.is_start,
         s.is_end,
         s.priority,
         x.warning,
         s.tags,
         s.attributes
     FROM cycle_states s
     LEFT JOIN (
       SELECT
         s.collection_time,
                 b.subject_name gw_name,
         REGEXP_REPLACE( COALESCE ( s.step_output->>'weight', '0'), '[^0-9]*', '0'):: integer val,
                     s.step_output->>'warning' AS warning,
         CASE WHEN l.img IS NOT NULL
           THEN ( s.gateway_id, s.camera_id, s.step_name, s.collection_time, substring (l.img FROM '_([^_]*)$'))::image_reference
         END img_keys,
         c.measure_name state,
         c.facility
       FROM image_step s
         LEFT OUTER JOIN customer_binding b
           ON s.gateway_id = b.beacon_id
              AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
              AND (b.start_time IS NULL OR s.collection_time >= b.start_time)
              AND (b.end_time IS NULL OR s.collection_time < b.end_time)
         JOIN customer_report_config c
           ON b.subject_name = c.gateway_name
              AND (c.start_time IS NULL OR s.collection_time >= c.start_time)
              AND (c.end_time IS NULL OR s.collection_time < c.end_time)
              AND b.facility = c.facility
              AND s.camera_id = c.camera_id
              AND s.step_name = c.step_name
         LEFT JOIN unnest(s.images) AS l(img)
           ON l.img LIKE '%'
       WHERE c.extract_type = 'weight'
             AND s.collection_time BETWEEN (SELECT start_time FROM times) AND (SELECT end_time FROM times)
      ) x
     ON x.state = s.state
        AND x.facility = s.facility
        AND x.collection_time BETWEEN s.start_time - :asset_look_back_duration_in_second::INTERVAL AND s.start_time + :asset_look_ahead_duration_in_second::INTERVAL
--     GROUP BY s.start_time, s.end_time, s.subject_id, s.facility, s.state, s.cycle_id
     ORDER BY s.subject_id, s.facility, s.start_time, s.end_time, s.cycle_id, s.state, x.val DESC, x.collection_time DESC
  ) a
  LEFT JOIN
  cycle_states b ON
     a.start_time = b.start_time AND
     a.subject_id = b.subject_id AND
     a.facility = b.facility AND
     a.state = b.state AND
     a.cycle_id = b.cycle_id
    GROUP BY
        a.start_time,
        a.end_time,
        a.subject_id,
        a.tags,
        a.attributes,
        a.facility,
        a.state,
        a.gw_name,
        a.cycle_id,
        b.img_keys,
        a.weight,
        a.is_start,
        a.is_end,
        a.priority,
        a.warning
),

cycle_weights_with_other_imgs AS (
    SELECT
        cw.start_time,
        cw.end_time,
        cw.subject_id subject_id,
        cw.tags,
        cw.attributes,
        cw.facility,
        cw.state,
        cw.cycle_id,
        cw.weight,
        CASE WHEN x.gw_name IS NOT NULL THEN cw.img_keys || json_build_object(x.gw_name, array_to_json(array_remove(array_agg(distinct(x.img_keys)), NULL)))::jsonb
                     ELSE cw.img_keys
                END img_keys,
            cw.is_start,
        cw.is_end,
        cw.priority,
        cw.warning
    FROM cycle_weights cw
    LEFT OUTER JOIN (
            SELECT
                CASE WHEN l.img IS NOT NULL
                         THEN (i.gateway_id, i.camera_id, i.step_name, i.collection_time, substring (l.img FROM '_([^_]*)$'))::image_reference
                END img_keys,
                b.subject_name gw_name,
                c.extract_type,
                i.collection_time
            FROM image_step i
            LEFT OUTER JOIN customer_binding b ON
                    i.gateway_id = b.beacon_id
                    AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
                    AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
                    AND (b.end_time IS NULL OR i.collection_time < b.end_time)
            JOIN customer_report_config c ON
                    b.facility = c.facility
                    AND b.subject_name = c.gateway_name
                    AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
                    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
                    AND i.camera_id = c.camera_id
                    AND i.step_name = c.step_name
            LEFT JOIN unnest(i.images) AS l(img)
                    ON l.img LIKE '%'
            WHERE i.collection_time BETWEEN (SELECT start_time FROM times) AND (SELECT end_time FROM times)
    )x ON cw.state = x.extract_type
          AND x.collection_time BETWEEN cw.start_time - :asset_look_back_duration_in_second::INTERVAL AND cw.start_time + :asset_look_ahead_duration_in_second::INTERVAL
   GROUP BY cw.start_time, cw.end_time, cw.subject_id, cw.tags, cw.attributes, cw.facility, cw.img_keys,
            cw.state, cw.cycle_id, cw.weight, cw.is_start, cw.is_end, cw.warning, cw.priority, x.gw_name
   ORDER BY subject_id, facility, start_time, end_time, cycle_id, state, weight DESC
),

visit_log AS (
  SELECT *
  FROM(
    SELECT DISTINCT ON (facility, subject_id, start_time)
        facility,
        subject_id || COALESCE(' (' || (attributes -> 'Truck Type' ->> 'attr_value') || ':#' || (attributes -> 'Unit Number' ->> 'attr_value') || ')', '') subject_id,
        state last_state,
        start_time last_state_start_time,
        count(CASE WHEN is_start THEN 1 END) OVER w visit_count,
        img_keys,
        is_start,
        is_end,
        priority,
        tags,
        attributes
    FROM cycle_states
    WHERE is_start = TRUE OR is_end = TRUE
        WINDOW w AS (PARTITION BY subject_id, facility)
        ORDER BY facility, subject_id, start_time, coalesce(end_time, start_time) DESC
  )x
--  WHERE x.is_start = TRUE
  ORDER BY x.subject_id, x.last_state_start_time DESC
),

cycle_log AS (
    SELECT
        start_time,
        CASE WHEN end_time IS NULL THEN now() ELSE end_time END end_time,
        subject_id || COALESCE(' (' || (cwwoi.attributes -> 'Truck Type' ->> 'attr_value') || ':#' || (cwwoi.attributes -> 'Unit Number' ->> 'attr_value') || ')', '') subject_id,
        cwwoi.tags,
        cwwoi.attributes,
        facility,
        state,
        cycle_id,
        weight,
        img_keys,
        is_start,
        is_end,
        priority,
        warning,
      count(CASE WHEN is_start THEN 1 END) OVER w visit_count
    FROM cycle_weights_with_other_imgs cwwoi
    WINDOW w AS (PARTITION BY subject_id, facility)
    ORDER BY start_time DESC
)