WITH customer_report_config_without_hidden AS(
SELECT * FROM customer_report_config
WHERE hidden IS NOT TRUE
),
shift_times AS (
  SELECT shift_name, date::date::text, start_time shift_start, end_time shift_end, shift_timezone_name
  FROM (
    SELECT
      date,
      shift_name,
      shift_timezone_name,
      CASE WHEN :previous THEN (lag(shift_start) OVER (ORDER BY date)) ELSE shift_start END start_time,
      CASE WHEN :previous THEN (lag(shift_end) OVER (ORDER BY date)) ELSE shift_end END end_time
    FROM (
      SELECT
        d date,
        s.shift_name,
        d AT TIME ZONE s.shift_timezone_name + s.start_offset shift_start,
        d AT TIME ZONE s.shift_timezone_name + s.end_offset shift_end,
        s.shift_timezone_name
      FROM generate_series(
               COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE - INTERVAL '7 days',
               COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE + INTERVAL '7 days',
               INTERVAL '1 day'
           ) d
      JOIN customer_shift s ON s.day_of_week IS NULL OR s.day_of_week = date_part('dow', d)
      WHERE ((:shift::text IS NULL) OR shift_name = :shift) AND (facility IS NULL OR s.facility = :facility)
    )x
  )y
  WHERE date = COALESCE(:date, (now()::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE shift_timezone_name)::date)
),
times as (
    SELECT
      coalesce(:from::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM shift_times), (select min(shift_start) from shift_times)) start_time,
      coalesce(:to::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM shift_times), (select max(shift_end) from shift_times)) end_time
),
filtered_image_step AS (
  SELECT *
  FROM image_step
  WHERE calculate_slot(collection_time,collection_interval) BETWEEN (SELECT start_time FROM times) AND (SELECT end_time FROM times)
),
only_image AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    null::text,
    'image'::text,
    null::text,
	null::json,
    null::text,
    null::bool,
    null::bool,
    null::json,
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
    END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
    i.gateway_id = b.beacon_id
    AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
    AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
    AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
    AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'only_image'
   JOIN unnest(i.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
),
bbox AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    COALESCE(i.step_output->>'reason', i.step_output->>'warning') as exception,
    CASE
      WHEN i.step_output ->> 'status' = 'unknown' OR i.step_output ->> 'status' = 'error' THEN NULL -- previously 'MISSING DATA'
      WHEN ((i.step_output -> 'subjects' ->> c.extract_key)::int > '0'::int) THEN 'present'
      WHEN ((i.step_output -> 'operator1' -> 'subjects' ->> c.extract_key)::int > '0'::int) THEN 'present'
      WHEN ((i.step_output -> 'operator2' -> 'subjects' ->> c.extract_key)::int > '0'::int) THEN 'present'
      WHEN ((i.step_output -> 'operator3' -> 'subjects' ->> c.extract_key)::int > '0'::int) THEN 'present'
      WHEN ((i.step_output -> 'operator4' -> 'subjects' ->> c.extract_key)::int > '0'::int) THEN 'present'
      WHEN ((i.step_output -> 'operator5' -> 'subjects' ->> c.extract_key)::int > '0'::int) THEN 'present'
      ELSE 'absent'
    END state,
    (i.step_output -> 'subjects' ->> c.extract_key) operator_count,
		i.step_output -> 'bboxes' bounding_box,
    null::text confidence,
    null::bool corelated_lp,
    null::bool license_plate_asset,
    null::json violations,
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
      END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
    i.gateway_id = b.beacon_id
    AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
    AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
    AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
    AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'bbox'
  LEFT JOIN unnest(i.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
),
bbox3 AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    COALESCE(i.step_output->>'reason', i.step_output->>'warning') as exception,
    CASE
      WHEN i.step_output ->> 'status' = 'unknown' OR i.step_output ->> 'status' = 'error' THEN NULL -- previously 'MISSING DATA'
      WHEN i.step_output -> 'subjects' ->> c.extract_key IS NULL THEN 'UNKNOWN'
      ELSE i.step_output -> 'subjects' ->> c.extract_key
    END state,
    (i.step_output -> 'subjects' ->> c.extract_key) subject_count,
		i.step_output -> 'bboxes' bounding_box,
    null::text,
    null::bool,
    null::bool,
    null::json,
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
      END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
    i.gateway_id = b.beacon_id
    AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
    AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
    AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
    AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'bbox3'
  LEFT JOIN unnest(i.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
),

sticker_count AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    COALESCE(i.step_output->>'reason', i.step_output->>'warning') as exception,
      i.step_output -> c.extract_key ->> 'seen' state,
    null::text full_response,
	  json_build_object('', ''),
    null::text,
    null::bool,
    null::bool,
    null::json,
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
      END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
    i.gateway_id = b.beacon_id
    AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
    AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
    AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
    AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'sticker_count'
  LEFT JOIN unnest(i.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
),

anchor_state AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    COALESCE(i.step_output->>'reason', i.step_output->>'warning') as exception,
    CASE
      WHEN JSON_ARRAY_LENGTH(i.step_output -> 'anchor_state_list') > 0 THEN
      i.step_output -> 'anchor_state_list' -> c.extract_key::INT ->>'anchor_state'
    END state,
    (i.step_output -> 'subjects' ->> c.extract_key) subject_count,
		i.step_output -> 'bboxes' bounding_box,
    null::text,
    null::bool,
    null::bool,
    json_build_object('', ''),
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
      END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
    i.gateway_id = b.beacon_id
    AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
    AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
    AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
    AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'anchor_state'
  LEFT JOIN unnest(i.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
),
image_similarity AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    i.step_output ->> 'warning' as exception,
    i.step_output -> c.extract_key ->> 'detection' state,
    null::text full_response,
	json_build_object('', ''),
    null::text,
    null::bool,
    null::bool,
    null::json,
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
      END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
    i.gateway_id = b.beacon_id
    AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
    AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
    AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
    AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'image_similarity'
  LEFT JOIN unnest(i.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
),
motion_direction AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    i.step_output ->> 'warning' as exception,
    i.step_output ->> c.extract_key state,
    null::text full_response,
	json_build_object('', ''),
    null::text,
    null::bool,
    null::bool,
    null::json,
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
      END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
    i.gateway_id = b.beacon_id
    AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
    AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
    AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
    AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'motion_direction'
  LEFT OUTER JOIN filtered_image_step j
    ON i.collection_time = j.collection_time
       AND i.gateway_id = j.gateway_id
       AND j.step_name = COALESCE(c.cs_config->>'image_source_step_name', 'src_video')
       AND i.camera_id = j.camera_id
  LEFT JOIN unnest(j.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
),
classifier AS (
  SELECT DISTINCT ON (facility, subject_name, measure_name, date_trunc('minute', collection_time)) *
  FROM(
    SELECT
      c.facility,
      c.subject_name,
      c.measure_name,
      calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
      i.collection_interval,
      COALESCE(i.step_output->>'reason', i.step_output->>'warning') as exception,
      CASE
        WHEN i.step_output ->> 'status' = 'unknown' OR i.step_output ->> 'status' = 'error' THEN NULL -- previously 'MISSING DATA'
        WHEN COALESCE(i.step_output ->> c.extract_key, i.step_output -> 'rank_result'->>c.extract_key)::float > (c.extract_threshold_definitions ->> c.extract_key)::float THEN 'present'
        ELSE 'absent'
      END state,
      null::text full_response,
      i.step_output -> 'rank_result' confidence_scores,
      null::text,
      null::bool,
      null::bool,
      null::json,
      CASE WHEN l.image IS NOT NULL
        THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
      END image_info
    FROM filtered_image_step i
    LEFT OUTER JOIN customer_binding b ON
      i.gateway_id = b.beacon_id
      AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
      AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
      AND (b.end_time IS NULL OR i.collection_time < b.end_time)
    JOIN customer_report_config_without_hidden c ON
      b.facility = c.facility
      AND (c.extract_type = 'airplane_classifier' OR b.subject_name = c.gateway_name)
      AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
      AND (c.end_time IS NULL OR i.collection_time < c.end_time)
      AND i.camera_id = c.camera_id
      AND i.step_name = c.step_name
      AND c.extract_type IN ('classifier', 'airplane_classifier')
    LEFT JOIN unnest(i.images) AS l(image)
      ON l.image LIKE '%' || c.extract_image_name
  )x
    WHERE x.facility = :facility
  ORDER BY facility, subject_name, measure_name, date_trunc('minute', collection_time), state DESC
),
bbox2 AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    COALESCE(i.step_output->>'reason', i.step_output->>'warning') as exception,
    CASE
      WHEN i.step_output ->> 'status' = 'unknown' OR i.step_output ->> 'status' = 'error' THEN NULL -- previously 'MISSING DATA'
      WHEN (
        COALESCE(
--        We will remove The hardcoded 'brake' key extract once
--        We modify 'report_config'->'extract_key' from 'activity' to 'brake' or 'dry'. (VDB, Schneider and Schaeffler)
            (i.step_output -> 'brake' ->> 'activity'),
            (i.step_output -> c.extract_key ->> 'violation'),
            (i.step_output -> c.extract_key ->> 'activity'))::int > '0'::int)
        THEN 'present'
      WHEN (
        COALESCE(
--        We will remove The hardcoded 'brake' key extract once
--        We modify 'report_config'->'extract_key' from 'activity' to 'brake' or 'dry'. (VDB, Schneider and Schaeffler)
            (i.step_output -> 'brake' ->> 'activity'),
            (i.step_output -> c.extract_key ->> 'violation'),
            (i.step_output -> c.extract_key ->> 'activity'))::int = '0')
         THEN 'absent'
    END state,
    null::text full_response,
    json_build_object('', ''),
    null::text,
    null::bool,
    null::bool,
    null::json,
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
    END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
    i.gateway_id = b.beacon_id
    AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
    AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
    AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
	AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'bbox2'
  LEFT JOIN unnest(i.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
),
state AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    CASE
      WHEN (i.step_output ->> 'brightness') :: float > 140 OR (i.step_output ->> 'brightness') :: float <= 45 THEN 'BRIGHTNESS EXCEPTION'
      ELSE COALESCE(i.step_output->>'reason', i.step_output->>'warning')
    END exception,
    CASE
      WHEN i.step_output ->> 'status' = 'unknown' OR i.step_output ->> 'status' = 'error' THEN NULL -- previously 'MISSING DATA'
      WHEN i.step_function = 'fast_sticker' THEN
        CASE
          WHEN true = (SELECT '"open"' = ANY(array_agg(x.step_output -> y)::text[])
                            FROM (SELECT step_function, step_output
                                FROM image_step
                                WHERE step_function = 'fast_sticker'
                                AND collection_time = i.collection_time
                                AND image_step_id = i.image_step_id
                                AND gateway_id = i.gateway_id
                                LIMIT 1) x
                            JOIN json_object_keys(x.step_output) y ON true
                            GROUP BY step_function)
          THEN 'open'
          ELSE 'closed'
        END
      WHEN i.step_function LIKE '%violation%' THEN
        CASE WHEN i.step_output ->> c.extract_key IS NOT NULL
        THEN
            CASE WHEN jsonb_array_length((i.step_output -> c.extract_key -> 'safe_cage' -> 'cage')::jsonb) > 0 AND jsonb_array_length((i.step_output -> c.extract_key -> 'safe_cage' -> 'ppe')::jsonb) > 0
            THEN 'multiple'
            WHEN jsonb_array_length((i.step_output -> c.extract_key -> 'safe_cage' -> 'cage')::jsonb) > 0
            THEN 'cage'
            ELSE
                'ppe'
            END
        END
      WHEN i.step_function = 'lpr' THEN
          CASE
              WHEN i.step_output ->> c.extract_key IS NULL THEN 'No LPN'
              WHEN NOT (SELECT EXISTS(SELECT
                                      FROM asset_attribute
                                      WHERE asset_id = i.step_output ->> c.extract_key
                                        AND attribute_value = 'blacklist'
                                        AND qualifier = 'qc_tag'))
                  THEN i.step_output ->> c.extract_key
              END
      WHEN i.step_name = 'work_order_ocr' AND i.step_function = 'ocr' THEN
          CASE
              WHEN i.step_output ->> c.extract_key IS NULL THEN 'no_work_order'
              ELSE i.step_output ->> c.extract_key
              END
      WHEN i.step_function = 'grid_activation' THEN
        i.step_output -> c.extract_key ->> 'active'
      WHEN i.step_output ->> c.extract_key IS NULL THEN 'UNKNOWN'
      ELSE i.step_output ->> c.extract_key
    END state,
    step_output ->> 'fullResponse' fullResponse,
    json_build_object('', ''),
    i.step_output ->> 'confidence' confidence,
    CASE WHEN i.step_output ->> 'modifier' = 'correlation'
    THEN TRUE
    ELSE FALSE
    END corelated_lp,
    CASE WHEN i.step_function = 'lpr'
    THEN TRUE
    ELSE FALSE
    END license_plate_asset,
    CASE WHEN i.step_output ->> 'violations' IS NOT NULL
    THEN i.step_output -> 'violations' ELSE json_build_object('', '') END violations,
    CASE WHEN l.image IS NOT NULL
      THEN CASE WHEN l.image LIKE '%-data%'
        THEN (i.gateway_id,i.camera_id,i.step_name,to_timestamp((split_part(split_part(l.image, '/', 13), '_', 3))::float8),substring(l.image FROM '_([^_]*)$'))::image_reference
        ELSE
            (i.gateway_id,i.camera_id,i.step_name,to_timestamp((split_part(split_part(l.image, '/', 12), '_', 3))::float8),substring(l.image FROM '_([^_]*)$'))::image_reference
        END
      END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
     i.gateway_id = b.beacon_id
     AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
     AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
     AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
		AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'state'
  LEFT JOIN unnest(i.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
),
pixel_avg AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    CASE
      WHEN (i.step_output ->> 'brightness') :: float > 140 OR (i.step_output ->> 'brightness') :: float <= 45 THEN 'BRIGHTNESS EXCEPTION'
      ELSE COALESCE(i.step_output->>'reason', i.step_output->>'warning')
    END exception,
    CASE
      WHEN i.step_output ->> 'status' = 'unknown' OR i.step_output ->> 'status' = 'error' THEN NULL -- previously 'MISSING DATA'
      WHEN  (i.step_output -> c.extract_key ->> 0)::float >= (c.extract_threshold_definitions->'r'->>'min')::float
          AND (i.step_output -> c.extract_key ->> 0)::float < (c.extract_threshold_definitions->'r'->>'max')::float
          AND (i.step_output -> c.extract_key ->> 1)::float >= (c.extract_threshold_definitions->'g'->>'min') ::float
          AND (i.step_output -> c.extract_key ->> 1)::float < (c.extract_threshold_definitions->'g'->>'max') ::float
          AND (i.step_output -> c.extract_key ->> 2)::float >= (c.extract_threshold_definitions->'b'->>'min') :: float
          AND (i.step_output -> c.extract_key ->> 2)::float < (c.extract_threshold_definitions->'b'->>'max') :: float THEN 'in_range'
      ELSE 'out_of_range'
    END state,
    i.step_output ->> 'fullResponse' fullResponse,
	json_build_object('', ''),
    null::text,
    null::bool,
    null::bool,
    null::json,
    CASE WHEN l.image IS NOT NULL
      THEN (j.gateway_id,j.camera_id,j.step_name,j.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
      END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
     i.gateway_id = b.beacon_id
     AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
     AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
     AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
		AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'pixel_avg'
  LEFT OUTER JOIN filtered_image_step j
      ON i.collection_time = j.collection_time
      AND i.gateway_id = j.gateway_id
      AND j.step_name in ('default', 'heartbeat')
      AND i.camera_id = j.camera_id
  LEFT JOIN unnest(j.images) AS l(image)
      ON l.image LIKE '%' || c.extract_image_name
      WHERE c.facility = :facility
),
color_state AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    i.step_output ->> 'reason' as exception,
    CASE WHEN (i.step_output ->> 'state') IS NULL THEN NULL
         WHEN (i.step_output ->> 'state') <> 'off' THEN (i.step_output ->> 'state') || ' ' || (i.step_output ->> 'color')
         WHEN (i.step_output ->> 'state') = 'off' THEN 'off'
    END state,
    null::text full_response,
    json_build_object('', ''),
    null::text,
    null::bool,
    null::bool,
    null::json,
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
      END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
     i.gateway_id = b.beacon_id
     AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
     AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
     AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
		AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'color_state'
  LEFT JOIN unnest(i.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
),
hue_label AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    i.step_output ->> 'warning' as exception,
    CASE
        WHEN (i.step_output->'hue_result'->'card_hue'->>'subject_hue_label') = '_nondeterministic' THEN
            CASE
                WHEN (i.step_output->>'warning_code') = 'img_too_dark' THEN 'image_too_dark'
                ELSE 'unknown'
            END
        ELSE (i.step_output->'hue_result'->'card_hue'->>'subject_hue_label')
    END state,
    null::text full_response,
	json_build_object('', ''),
    null::text,
    null::bool,
    null::bool,
    null::json,
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
      END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
     i.gateway_id = b.beacon_id
     AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
     AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
     AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
		AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'hue_label'
  LEFT JOIN unnest(i.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
),
subject_event AS (
  SELECT *,
    (SELECT DISTINCT shift_timezone_name FROM shift_times) tz,
    json_strip_nulls(row_to_json(x.image_info)) images
  FROM ((
    (SELECT * FROM bbox)
    UNION ALL (SELECT * FROM only_image)
    UNION ALL (SELECT * FROM classifier)
    UNION ALL (SELECT * FROM bbox2)
    UNION ALL (SELECT * FROM bbox3)
    UNION ALL (SELECT * FROM sticker_count)
    UNION ALL (SELECT * FROM state)
    UNION ALL (SELECT * FROM pixel_avg)
    UNION ALL (SELECT * FROM color_state)
    UNION ALL (SELECT * FROM hue_label)
    UNION ALL (SELECT * FROM anchor_state)
    UNION ALL (SELECT * FROM image_similarity)
    UNION ALL (SELECT * FROM motion_direction)
  )x JOIN times t
  ON x.collection_time BETWEEN t.start_time AND t.end_time)
    LEFT JOIN shift_times s
  ON x.collection_time BETWEEN s.shift_start AND s.shift_end
  WHERE state <> 'UNKNOWN'
),
subjects_cycle_log AS (
  SELECT
    s.facility,
    CASE WHEN :byClass THEN s.subject_name || '@' || s.measure_name
         ELSE s.measure_name || '@' || s.subject_name
    END subject_id,
    CASE WHEN :byClass THEN s.measure_name
         ELSE s.subject_name
    END state,
    json_agg(s.bounding_box)->0->>c.extract_key bbox,
    s.collection_time start_time,
    s.collection_time end_time,
    json_build_object('other', array_to_json(array_remove(array_agg(s.image_info order by (s.image_info).collection_time), NULL))) img_keys
  FROM subject_event s
  LEFT OUTER JOIN customer_report_config_without_hidden c
    ON c.subject_name = s.subject_name
			 AND c.measure_name = s.measure_name
			 AND (c.start_time IS NULL OR s.collection_time >= c.start_time)
    	 AND (c.end_time IS NULL OR s.collection_time < c.end_time)
  WHERE s.state = 'present'
  GROUP BY s.facility, s.subject_name, s.measure_name, s.collection_time, c.extract_key
  ORDER BY s.subject_name, s.measure_name, s.collection_time DESC
),
events AS (
  SELECT
    (SELECT start_time FROM times) shift_start,
    (SELECT end_time FROM times) shift_end,
    MIN(collection_interval) collection_interval,
    collection_time,
    facility,
    subject_name,
    measure_name,
    state,
   (array_agg(images))[1] images
  FROM subject_event
  WHERE state IS NOT NULL
  GROUP BY facility, subject_name, measure_name, state, collection_time
  ORDER BY facility, subject_name, measure_name, collection_time
),
aircraft AS (
  SELECT
    date_trunc('minute', calculate_slot(i.collection_time, i.collection_interval)) collection_time,
    i.step_name,
    b.facility,
    i.collection_interval,
    b.beacon_id gateway_id,
    COALESCE(i.step_output ->> 'reason', i.step_output ->> 'warning') as exception,
    CASE
    WHEN ((i.step_output ->> 'airplane') :: float > '0.2' :: float)
      THEN 'present'
    WHEN ((i.step_output ->> 'airplane') :: float < '0.2' :: float)
      THEN 'absent'
    WHEN (i.step_output ->> 'airplane' IS NULL)
      THEN 'absent'
    END state,
    cast(i.step_output ->> 'airplane' as float)  accuracy,
    null :: text full_response,
    null::bool,
    null::bool,
    -- 		i.step_output -> 'bboxes' bounding_box,
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id, i.camera_id, i.step_name, i.collection_time,
      substring(l.image FROM'_([^_]*)$')) :: image_reference
    END image_info
  FROM filtered_image_step i
    LEFT OUTER JOIN customer_binding b ON
       i.gateway_id = b.beacon_id
       AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
       AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
       AND (b.end_time IS NULL OR i.collection_time < b.end_time)
    LEFT JOIN unnest(i.images) AS l(image)
      ON l.image LIKE '%'
  WHERE i.step_name LIKE '%classifier%'
  ),
gate_status AS(
  SELECT DISTINCT ON (facility, step_name, date_trunc('minute', collection_time)) *,
    split_part( x.step_name, 'classifier' , 1 ) as subject_name,
      'aircraft' as measure_name,
       json_strip_nulls(row_to_json(x.image_info)) images,
      (SELECT start_time FROM times) shift_start,
     (SELECT end_time FROM times) shift_end
  FROM aircraft x
    JOIN shift_times s
    ON x.collection_time BETWEEN s.shift_start AND s.shift_end
  ORDER BY facility, step_name, date_trunc('minute', collection_time), accuracy DESC
),
thumbnails AS (
  SELECT max(collection_time) collection_time,
  gateway_id,
  max(images),
  step_name
  FROM image_step
  WHERE step_name = 'glamour_crop' AND collection_time
           BETWEEN (SELECT start_time FROM times) AND (SELECT end_time FROM times)
  GROUP BY gateway_id,step_name

)