WITH recent_steps AS (SELECT gateway_id,
                             step_name,
                             max(image_step_id) AS image_step_id
                      FROM image_step
                      WHERE insert_time > now() - INTERVAL '1 hour' --
                          AND
                            step_function NOT IN ('thumbnail', 'baseMetadata')
                            --                             step_function NOT IN ('thumbnail', 'baseMetadata', 'default', 'motion')
                      GROUP BY 1, 2),
     recent_image_steps AS (SELECT a.*
                            FROM image_step a
                                   JOIN recent_steps b ON a.image_step_id = b.image_step_id),
     intstep AS (
       SELECT gateway_id                                                  AS is_gateway_id,
              date_trunc('minute', insert_time)                           AS is_insert_time,
              isd.step_name                                               AS is_step_name,
              step_function                                               AS is_step_function,
              COALESCE(((step_output -> 'subjects') ->> 'person'),
                       (((step_output ->> 'state') || ' ') ||
                        (step_output ->> 'color' || '')),
                       (step_output ->> 'position'),
                       (step_output ->> 'average_color'),
                       (step_output ->> 'Work_order'),
                       (step_output ->> 'card_state'),
                       (step_output ->> 'weight'),
                       (step_output ->> 'license_plate'))                 AS is_simplified_output,
              step_output,
              --               images,
              split_part(images [1], '/', 5) AS is_node_name,
              --        COALESCE(processor_role, 'unknown')                         AS is_processor_role,
              cb.binding_id                                               AS cb_binding_id,
              cb.start_time                                               AS cb_start_time,
              cb.end_time                                                 AS cb_end_time,
              cb.subject                                                  AS cb_subject,
              cb.subject_name                                             AS cb_subject_name,
              cb.radius                                                   AS cb_radius,
              cb.zone_name                                                AS cb_zone_name,
              cb.facility                                                 AS cb_facility,
              crc.subject_name                                            AS crc_subject_name,
              crc.measure_name                                            AS crc_measure_name,
              crc.facility                                                AS crc_facility,
              crc.gateway_name                                            AS crc_gateway_name,
              crc.camera_id                                               AS crc_camera_id,
              crc.step_name                                               AS crc_step_name,
              crc.extract_type                                            AS crc_extract_type,
              crc.extract_key                                             AS crc_extract_key,
              crc.extract_image_name                                      AS crc_extract_image_name,
              crc.cs_config                                               AS crc_cs_config,
              crc.composite_definition                                    AS crc_composite_definition,
              crc.hidden                                                  AS crc_hidden,
              crc.start_time                                              AS crc_start_time,
              crc.end_time                                                AS crc_end_time

       FROM recent_image_steps AS isd
              FULL OUTER JOIN customer_binding cb ON isd.gateway_id = cb.beacon_id
              FULL OUTER JOIN customer_Report_config crc
                              ON (crc.facility = cb.facility AND crc.gateway_name = cb.subject_name AND
                                  isd.step_name = crc.step_name AND
                                  isd.camera_id = crc.camera_id)
       WHERE (cb.start_time IS NULL OR isd.collection_time >= cb.start_time) AND
             (cb.end_time IS NULL OR isd.collection_time < cb.end_time) AND
             (crc.start_time IS NULL OR isd.collection_time >= crc.start_time) AND
             (crc.end_time IS NULL OR isd.collection_time < crc.end_time)
     )
SELECT *
FROM intstep
ORDER BY 1;
