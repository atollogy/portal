WITH image_record AS (SELECT s.gateway_id,
                             s.camera_id,
                             s.step_name,
                             s.collection_time,
                             b.subject_name,
                             split_part(s.images [1], '/', 5) AS node_name,
                             i.image                                                          image_location,
                             i.n                                                              ord
                      FROM image_step s
                             LEFT JOIN customer_binding b ON s.gateway_id = b.beacon_id AND b.subject = 'gateway',
                           unnest(s.images) WITH ORDINALITY i (image, n)
                      WHERE array_length(s.images, 1) > 0
                        AND collection_time >= $1
                        AND collection_time < $2
                        AND gateway_id = $3
                        AND camera_id = $4),
     empty_images AS (SELECT i.step_output,
                             i.gateway_id,
                             i.camera_id,
                             i.step_name,
                             i.collection_time,
                             b.subject_name,
                             cast('' as text) node_name,
                             i.images
                      FROM image_step i
                             LEFT JOIN customer_binding b ON i.gateway_id = b.beacon_id AND b.subject = 'gateway'
                      WHERE array_length(i.images, 1) IS NULL
                        AND collection_time >= $1
                        AND collection_time < $2
                        AND gateway_id = $3
                        AND camera_id = $4),
     selected_images AS (SELECT gateway_id,
                                camera_id,
                                step_name,
                                collection_time,
                                subject_name,
                                node_name,
                                array_agg(substring(image_location FROM '_([^_]*)$')
                                    ORDER BY ord) images
                         FROM image_record
                         GROUP BY 1, 2, 3, 4, 5, 6)

SELECT ss.step_output, si.*
FROM selected_images si
       LEFT JOIN image_step ss ON (
        si.gateway_id = ss.gateway_id AND
        si.camera_id = ss.camera_id AND
        si.step_name = ss.step_name AND
        si.collection_time = ss.collection_time
        )
UNION ALL (SELECT *
           FROM empty_images)
ORDER BY collection_time DESC, gateway_id, camera_id, step_name
