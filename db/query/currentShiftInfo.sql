WITH shift_date AS (
    SELECT
        d AS date,
        s.shift_name,
        d AT TIME ZONE s.shift_timezone_name + s.start_offset AS shift_start,
        d AT TIME ZONE s.shift_timezone_name + s.end_offset   AS shift_end,
        s.shift_timezone_name,
        s.facility
    FROM generate_series(date_trunc('day', COALESCE(:date, now())) :: TIMESTAMP WITHOUT TIME ZONE - INTERVAL '14 days',
                                             date_trunc('day', COALESCE(:date, now())) :: TIMESTAMP WITHOUT TIME ZONE + INTERVAL '14 days',
                                             INTERVAL '1 day'
             ) d
        JOIN customer_shift s ON s.day_of_week IS NULL OR s.day_of_week = date_part('dow', d)
    WHERE (CAST(:facility AS TEXT) IS NULL OR COALESCE(facility, :facility) = :facility)
)
SELECT *
FROM ((SELECT
          date,
          shift_name,
          shift_start,
          shift_end,
          now() BETWEEN shift_start AND shift_end AS in_progress,
          FALSE AS completed,
          shift_timezone_name
       FROM shift_date
       WHERE shift_end >= COALESCE(:date::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE shift_timezone_name, now())
       ORDER BY shift_name, in_progress, shift_start ASC)
      UNION
      (SELECT DISTINCT ON (shift_name)
          date,
          shift_name,
          shift_start,
          shift_end,
          FALSE AS in_progress,
          TRUE  AS completed,
          shift_timezone_name
       FROM shift_date
       WHERE shift_end < COALESCE(:date::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE shift_timezone_name, now())
       ORDER BY shift_name, shift_end DESC)) x
ORDER BY shift_start ASC