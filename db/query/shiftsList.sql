SELECT shift_name,
  (to_timestamp(:date, 'YYYY-MM-DD')::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE shift_timezone_name) + start_offset::INTERVAL as shift_start,
  (to_timestamp(:date, 'YYYY-MM-DD')::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE shift_timezone_name) + end_offset::INTERVAL as shift_end,
  EXTRACT('epoch' FROM end_offset::INTERVAL) - EXTRACT('epoch' FROM start_offset::INTERVAL) as shift_duration,
  shift_timezone_name as shift_timezone
FROM customer_shift
WHERE (facility = :facility OR facility IS NULL OR :facility::text is NULL)
  AND (day_of_week IS NULL OR day_of_week = date_part('dow', to_timestamp(:date, 'YYYY-MM-DD')::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE shift_timezone_name))
ORDER BY shift_start, shift_end;