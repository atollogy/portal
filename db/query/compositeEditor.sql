WITH state_slots AS (
  SELECT
    d_subject_name_major subject,
    d_subject_name_minor measure,
    d_state state,
    d_time_slot_time slot
  FROM f_events
  WHERE d_time_slot_time BETWEEN now() - INTERVAL '3 months' AND now()
        AND d_subject_name_major = :subject
        AND d_subject_name_minor = ANY(:measures)
),

group_slots AS (
  SELECT slot, ('{' || string_agg('"' || measure || '": "' || state || '"', ',') || '}')states
  FROM (SELECT * FROM state_slots ORDER BY measure, state)x
  GROUP BY slot
),

combinations AS (
  SELECT DISTINCT ON (states)
    states,
    (count(slot) OVER w) count,
    (count(slot) OVER w) * INTERVAL '1 minute' duration
  FROM group_slots
  WINDOW w AS (PARTITION BY states)
  ORDER BY states, slot DESC
)

SELECT states::json, count, duration
FROM combinations
ORDER BY duration DESC, count DESC
