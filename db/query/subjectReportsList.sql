WITH dates AS (
    SELECT generate_series(
        date_trunc('day',
            to_timestamp(
                GREATEST(
                    EXTRACT (EPOCH FROM now() - INTERVAL '1 year'),
                    EXTRACT (EPOCH FROM (SELECT min(collection_time) FROM image_step))
                )
            )
        ),
        (SELECT date_trunc('day', max(collection_time)) FROM image_step),
        INTERVAL '1 day'
    ) date
),
shift_date AS (
  SELECT
    to_char(d.date::TIMESTAMP WITHOUT TIME ZONE, 'YYYY-MM-DD') date,
    s.shift_name,
    d.date::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE s.shift_timezone_name + s.start_offset shift_start,
    d.date::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE s.shift_timezone_name + s.end_offset   shift_end,
    s.shift_timezone_name,
    s.facility as facility
  FROM
    dates d
    JOIN customer_shift s
      ON s.day_of_week IS NULL OR s.day_of_week = date_part('dow', d.date)
),
subject_reports_list AS (
    SELECT DISTINCT sd.date AS shift_date, sd.facility as facility
      FROM shift_date sd
)
SELECT shift_date
  FROM subject_reports_list
 WHERE $1::text IS NULL
    OR ($1::text IS NOT NULL AND facility = $1)
 ORDER BY shift_date DESC