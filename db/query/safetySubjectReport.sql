WITH customer_report_config_without_hidden AS(
SELECT * FROM customer_report_config
WHERE hidden IS NOT TRUE
AND (:violation::text is NULL OR violation = :violation)
),
shift_times AS (
  SELECT shift_name, date::date::text, start_time shift_start, end_time shift_end, shift_timezone_name
  FROM (
    SELECT
      date,
      shift_name,
      shift_timezone_name,
      CASE WHEN :previous THEN (lag(shift_start) OVER (ORDER BY date)) ELSE shift_start END start_time,
      CASE WHEN :previous THEN (lag(shift_end) OVER (ORDER BY date)) ELSE shift_end END end_time
    FROM (
      SELECT
        d date,
        s.shift_name,
        d AT TIME ZONE s.shift_timezone_name + s.start_offset shift_start,
        d AT TIME ZONE s.shift_timezone_name + s.end_offset shift_end,
        s.shift_timezone_name
      FROM generate_series(
               COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE - INTERVAL '7 days',
               COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE + INTERVAL '7 days',
               INTERVAL '1 day'
           ) d
      JOIN customer_shift s ON s.day_of_week IS NULL OR s.day_of_week = date_part('dow', d)
      WHERE ((:shift::text IS NULL) OR shift_name = :shift) AND (facility IS NULL OR s.facility = :facility)
    )x
  )y
  WHERE date = COALESCE(:date, (now()::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE shift_timezone_name)::date)
),
times as (
    SELECT
      coalesce(:from::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM shift_times), (select min(shift_start) from shift_times)) start_time,
      coalesce(:to::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM shift_times), (select max(shift_end) from shift_times)) end_time
),
filtered_image_step AS (
  SELECT *
  FROM image_step
  WHERE calculate_slot(collection_time,collection_interval) BETWEEN (SELECT start_time FROM times) AND (SELECT end_time FROM times)
),
bbox AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    COALESCE(i.step_output->>'reason', i.step_output->>'warning') as exception,
    CASE
      WHEN i.step_output ->> 'status' = 'unknown' OR i.step_output ->> 'status' = 'error' THEN NULL -- previously 'MISSING DATA'
      WHEN ((i.step_output -> 'subjects' ->> c.extract_key)::int > '0'::int) THEN 'present'
      WHEN ((i.step_output -> 'operator1' -> 'subjects' ->> c.extract_key)::int > '0'::int) THEN 'present'
      WHEN ((i.step_output -> 'operator2' -> 'subjects' ->> c.extract_key)::int > '0'::int) THEN 'present'
      WHEN ((i.step_output -> 'operator3' -> 'subjects' ->> c.extract_key)::int > '0'::int) THEN 'present'
      WHEN ((i.step_output -> 'operator4' -> 'subjects' ->> c.extract_key)::int > '0'::int) THEN 'present'
      WHEN ((i.step_output -> 'operator5' -> 'subjects' ->> c.extract_key)::int > '0'::int) THEN 'present'
      ELSE 'absent'
    END state,
    (i.step_output -> 'subjects' ->> c.extract_key) operator_count,
		i.step_output -> 'bboxes' bounding_box,
    i.step_output -> 'object_centers' object_centers,
    i.step_output -> 'object_bottom_midpoints' object_bottom_midpoints,
    i.step_output -> 'distances' distances,
    i.step_output -> 'sd_params' -> 'display_trapezoid' display_trapezoid,
    i.step_output -> 'sd_params' -> 'pixel_distance' pixel_distance,
    i.step_output -> 'sd_params' -> 'version' sd_version,
    i.step_output -> 'sd_params' -> 'use_in_detection' use_in_detection,
    i.step_output -> 'sd_params' -> 'ppf_x' ppf_x,
    i.step_output -> 'sd_params' -> 'ppf_y' ppf_y,
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
      END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
    i.gateway_id = b.beacon_id
    AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
    AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
    AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
    AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'bbox'
  LEFT JOIN unnest(i.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
),
bbox3 AS (
  SELECT
    c.facility,
    c.subject_name,
    c.measure_name,
    calculate_slot(i.collection_time, COALESCE(i.collection_interval, INTERVAL '60 seconds')) collection_time,
    i.collection_interval,
    COALESCE(i.step_output->>'reason', i.step_output->>'warning') as exception,
    CASE
      WHEN i.step_output ->> 'status' = 'unknown' OR i.step_output ->> 'status' = 'error' THEN NULL -- previously 'MISSING DATA'
      WHEN i.step_output -> 'subjects' ->> c.extract_key IS NULL THEN 'UNKNOWN'
      ELSE i.step_output -> 'subjects' ->> c.extract_key
    END state,
    (i.step_output -> 'subjects' ->> c.extract_key) subject_count,
		i.step_output -> 'bboxes' bounding_box,
    i.step_output -> 'object_centers' object_centers,
    i.step_output -> 'object_bottom_midpoints' object_bottom_midpoints,
    i.step_output -> 'distances' distances,
    i.step_output -> 'sd_params' -> 'display_trapezoid' display_trapezoid,
    i.step_output -> 'sd_params' -> 'pixel_distance' pixel_distance,
    i.step_output -> 'sd_params' -> 'version' sd_version,
    i.step_output -> 'sd_params' -> 'use_in_detection' use_in_detection,
    i.step_output -> 'sd_params' -> 'ppf_x' ppf_x,
    i.step_output -> 'sd_params' -> 'ppf_y' ppf_y,
    CASE WHEN l.image IS NOT NULL
      THEN (i.gateway_id,i.camera_id,i.step_name,i.collection_time,substring(l.image FROM '_([^_]*)$'))::image_reference
      END image_info
  FROM filtered_image_step i
  LEFT OUTER JOIN customer_binding b ON
    i.gateway_id = b.beacon_id
    AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
    AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
    AND (b.end_time IS NULL OR i.collection_time < b.end_time)
  JOIN customer_report_config_without_hidden c ON
    b.facility = c.facility
    AND b.subject_name = c.gateway_name
    AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
    AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    AND i.camera_id = c.camera_id
    AND i.step_name = c.step_name
    AND c.extract_type = 'bbox3'
  LEFT JOIN unnest(i.images) AS l(image)
    ON l.image LIKE '%' || c.extract_image_name
    WHERE c.facility = :facility
),
subject_event AS (
  SELECT *,
    (SELECT DISTINCT shift_timezone_name FROM shift_times) tz,
    json_strip_nulls(row_to_json(x.image_info)) images
  FROM ((
    (SELECT * FROM bbox)
    UNION ALL (SELECT * FROM bbox3)
  )x JOIN times t
  ON x.collection_time BETWEEN t.start_time AND t.end_time)
    LEFT JOIN shift_times s
  ON x.collection_time BETWEEN s.shift_start AND s.shift_end
  WHERE state <> 'UNKNOWN'
),
events AS (
  SELECT
    (SELECT start_time FROM times) shift_start,
    (SELECT end_time FROM times) shift_end,
    MIN(collection_interval) collection_interval,
    collection_time,
    facility,
    subject_name,
    measure_name,
    state,
   (array_agg(images))[1] images
  FROM subject_event
  WHERE state IS NOT NULL
  GROUP BY facility, subject_name, measure_name, state, collection_time
  ORDER BY facility, subject_name, measure_name, collection_time
),
thumbnails AS (
  SELECT max(collection_time) collection_time,
  gateway_id,
  max(images),
  step_name
  FROM image_step
  WHERE step_name = 'glamour_crop' AND collection_time
           BETWEEN (SELECT start_time FROM times) AND (SELECT end_time FROM times)
  GROUP BY gateway_id,step_name
)