WITH shift_times AS(
			SELECT
					to_timestamp(:date,'YYYY-MM-DD')::timestamp without time zone AT TIME ZONE s.shift_timezone_name + s.start_offset start_time,
					to_timestamp(:date,'YYYY-MM-DD')::timestamp without time zone AT TIME ZONE s.shift_timezone_name + s.end_offset end_time
			FROM customer_shift s
			WHERE
					s.shift_name = :shift AND
					(CAST(:facility AS TEXT) IS NULL OR COALESCE(s.facility, :facility) = :facility) AND
					(s.day_of_week is null OR s.day_of_week = date_part('dow', to_timestamp(:date,'YYYY-MM-DD')::timestamp without time zone AT TIME ZONE 'UTC'))
),

times as (
    select
        coalesce( (select start_time from shift_times), :from::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM customer_shift)) start_time,
        coalesce( (select end_time from shift_times), :to::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM customer_shift)) end_time
),

extended AS (
	SELECT
		namespace,
		beacon_id,
		gateway_id,
		gateway_name,
		gateway_facility,
		gateway_radius,
		slot_size,
		slot + (ofst * INTERVAL '1 second' * slot_size) slot,
		CASE WHEN ofst = 0 THEN rssi
			 WHEN gap <= 5 AND ofst < gap THEN LEAST(future_rssi, rssi) -- fill in small gaps with the worse of the two values
			 END rssi,
		CASE WHEN ofst = 0 THEN reading_count WHEN gap <= 5 AND ofst < gap THEN 0 END reading_count,
		tx_power
	FROM (
		SELECT
		  namespace,
		  beacon_id,
		  gateway_id,
		  gateway_name,
		  gateway_facility,
		  gateway_radius,
		  slot_size,
		  slot,
		  rssi,
		  tx_power,
		  reading_count,
		  CASE WHEN gap > 1 THEN generate_series(0, LEAST(gap-1, 4), 1) ELSE 0 END ofst,
		  gap,
		  future_rssi
		FROM (
			SELECT r.namespace,
				r.beacon_id,
				r.gateway_id,
				coalesce(gateway_binding.zone_name, gateway_binding.subject_name, r.node_name) gateway_name,
				gateway_binding.facility gateway_facility,
				gateway_binding.beacon_slot_size_seconds slot_size,
				r.slot,
				r.avg_rssi rssi,
				r.reading_count,
				r.tx_power_1m tx_power,
				extract(epoch from lead(r.slot) OVER (PARTITION BY r.namespace, r.beacon_id, r.gateway_id ORDER BY r.slot) - r.slot)::NUMERIC / gateway_binding.beacon_slot_size_seconds gap,
				lead(r.avg_rssi) OVER (PARTITION BY r.namespace, r.beacon_id, r.gateway_id ORDER BY r.slot) future_rssi,
				gateway_binding.radius gateway_radius
			FROM beacon_reading r
			LEFT OUTER JOIN customer_binding gateway_binding ON
				'ad135bf7eecc08752f96' = gateway_binding.namespace AND -- the Atollogy Edystone gateway namespace
				r.gateway_id = gateway_binding.beacon_id AND
				(gateway_binding.start_time IS NULL OR r.slot >= gateway_binding.start_time) AND
				(gateway_binding.end_time IS NULL OR r.slot < gateway_binding.end_time)
				JOIN times t
          ON r.slot >= t.start_time
          AND r.slot < t.end_time
			WHERE (gateway_binding.radius IS NULL OR gateway_binding.radius > 0)
		) x
	) y
),

moving_avg AS (
  SELECT namespace, beacon_id, gateway_id, gateway_name, gateway_facility, gateway_radius, slot_size, slot, CASE WHEN reading_count is not null THEN (sum(reading_count::float) OVER w  / count(reading_count) OVER w) END reading_count, CASE WHEN rssi is not null THEN (sum(rssi) OVER w  / count(rssi) OVER w) END rssi, count(rssi) over w, tx_power
	FROM extended
	WINDOW w AS (PARTITION BY namespace, beacon_id, gateway_id ORDER BY slot ROWS BETWEEN :lookback_slots PRECEDING AND CURRENT ROW)
),

-- This has the smoothed RSSI and distance by beacon and gateway
smoothed AS (
	select namespace, beacon_id, gateway_id, gateway_name, gateway_facility, gateway_radius, slot_size, slot, rssi, tx_power, reading_count, POW(10, (tx_power - rssi) / 20.0) distance from moving_avg
),

-- this has the basic winner election of the best gateway for each beacon
winner AS (
	SELECT
	    winner.namespace,
	    winner.beacon_id,
	    winner.slot,
	    winner.slot_size,
			winner.gateway_id,
			winner.gateway_facility,
			winner.distance,
			b.subject subject,
			coalesce(b.subject_name, winner.beacon_id) beacon_name,
			winner.gateway_name,
			b.binding_id beacon_binding
	FROM (
		SELECT DISTINCT ON (namespace, beacon_id, slot) namespace,
			beacon_id,
		    slot,
		    distance,
		    gateway_id,
		    gateway_name,
		    gateway_facility,
		    slot_size
		FROM smoothed
		WHERE rssi IS NOT NULL AND distance <= coalesce(gateway_radius, 10)
		ORDER BY 1, 2, 3, 4 ASC
	) winner
	LEFT OUTER JOIN customer_binding b ON
		b.subject <> 'gateway' AND
		winner.namespace = b.namespace AND
		winner.beacon_id = b.beacon_id AND
		(b.start_time IS NULL OR winner.slot >= b.start_time) AND
		(b.end_time IS NULL OR winner.slot < b.end_time)
),

-- now we can do event coalescing:
gaps AS (
 	SELECT namespace, beacon_id,
 		slot,
		slot_size,
 		gateway_name,
 		gateway_facility,
		beacon_binding,
		subject,
		beacon_name,
		distance,
			CASE
					WHEN (lag(gateway_name) OVER (PARTITION BY namespace, beacon_id, beacon_binding ORDER BY slot) <> gateway_name) THEN 1
					ELSE 0
			END AS new_gate,
			CASE
					WHEN extract (epoch from slot - lag(slot) OVER (PARTITION BY namespace, beacon_id ORDER BY slot)) > slot_size THEN 1
					ELSE 0
			END AS gap
	FROM winner
),

groups AS (
 	SELECT namespace, beacon_id,
 		gateway_name,
 		gateway_facility,
		beacon_binding,
		subject,
		beacon_name,
		slot,
		slot_size,
		sum(new_gate + gap) OVER (PARTITION BY namespace, beacon_id, beacon_binding ORDER BY slot) AS grp
  FROM gaps
),

beacon_event AS (
	SELECT
		namespace,
		beacon_id,
		beacon_binding,
		subject,
		beacon_name,
		gateway_name,
		gateway_facility,
		grp,
		min(slot) AS start_time,
	    max(slot) + (INTERVAL '1 second' * slot_size) AS end_time
    FROM groups
    GROUP BY 1,2,3,4,5,6,7,8, slot_size
)