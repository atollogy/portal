WITH last_annotation AS(
  SELECT DISTINCT ON ( 1,2,3,4,5,6 )
  gateway_id, camera_id, step_name, step_function_version, step_config_version, collection_time,
  is_accurate
  FROM image_step_annotation
  WHERE qc_result IS NOT NULL
  ORDER BY 1,2,3,4,5,6,created_at_date desc
),
stats AS (
SELECT gateway_id, camera_id, step_name,
    coalesce(b.subject_name, i.gateway_id) as gateway_name,
    step_function_version,
    step_config_version,
    max(collection_time) last_report,
    count(case is_accurate when TRUE then 1 else null end) as accurate,
    count(case is_accurate when FALSE then 1 else null end) as inacurrate,
    count(*) as total,
    (count(case is_accurate when TRUE then 1 else null end)/ count(*) ::numeric)*100 as accurate_percentage
FROM last_annotation i
LEFT OUTER JOIN customer_binding b ON
      i.gateway_id = b.beacon_id
      AND (b.namespace = 'ad135bf7eecc08752f96') -- Atollogy gw namespace
      AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
      AND (b.end_time IS NULL OR i.collection_time < b.end_time)
GROUP BY gateway_name, gateway_id, camera_id, step_name, step_function_version, step_config_version
ORDER BY 1,2,3,4,5,6)