SELECT
    shifts.shift_name,
    to_timestamp(:date,'YYYY-MM-DD')::timestamp without time zone AT TIME ZONE shifts.shift_timezone_name + shifts.start_offset start_time,
    to_timestamp(:date,'YYYY-MM-DD')::timestamp without time zone AT TIME ZONE shifts.shift_timezone_name + shifts.end_offset end_time,
    shifts.shift_timezone_name
FROM customer_shift shifts
WHERE
    shifts.shift_name = :shift AND
		(CAST(:facility AS TEXT) IS NULL OR COALESCE(shifts.facility, :facility) = :facility) AND
    (shifts.day_of_week is null OR shifts.day_of_week = date_part('dow', to_timestamp(:date,'YYYY-MM-DD')::timestamp without time zone AT TIME ZONE 'UTC'))