WITH qcfg AS (
    SELECT interval '30 minutes'                                        AS min_cycle_interval,
           2                                                            AS subject_event_multiple,
           1                                                            AS place_event_multiple,
           40.0                                                         AS candidate_confidence_threshold,
           :facility::text                                              AS _facility,
           COALESCE(:shift, 'day')::text                                AS _shift,
           :date::timestamptz::date                                     AS _start_date,
           COALESCE(:date, now())::timestamptz                          AS _any_datetime,
           :from::timestamptz::date                                     AS _from_date,
           :to::timestamptz::date                                       AS _to_date,
           COALESCE(:lookback_slots, '0')::int                          AS _lookback_slots,
           COALESCE(:tagNames, '{customer_tag, qc_tag}')::text[]        AS _tag_names,
           COALESCE(:filters, '{whitelist}')::text[]                    AS _tag_filters,
           COALESCE(:asset_look_back_duration_in_second,
                    '0 seconds')::INTERVAL                              AS _look_back,
           COALESCE(:asset_look_ahead_duration_in_second,
                    '0 seconds')::INTERVAL                              AS _look_ahead
),

shift_times AS (
    SELECT  (SELECT _start_date FROM qcfg)::TIMESTAMP AT TIME ZONE s.shift_timezone_name + s.start_offset        start_time,
            (SELECT _start_date FROM qcfg)::TIMESTAMP AT TIME ZONE s.shift_timezone_name + s.end_offset          end_time
    FROM customer_shift s
    WHERE s.shift_name =  (SELECT _shift FROM qcfg)
      AND s.facility = (SELECT _facility FROM qcfg)
      AND (s.day_of_week IS NULL
            OR s.day_of_week = date_part('dow', (SELECT _start_date FROM qcfg)::TIMESTAMP AT TIME ZONE 'UTC'))
),

times AS (
    SELECT  COALESCE(
                (SELECT _from_date FROM qcfg)::TIMESTAMP AT TIME ZONE (
                        SELECT DISTINCT shift_timezone_name
                        FROM customer_shift
                        WHERE facility = (SELECT _facility FROM qcfg)),
                (SELECT start_time FROM shift_times)
            )                                                                                           start_time,
            COALESCE(
                (SELECT _to_date FROM qcfg)::TIMESTAMP AT TIME ZONE (
                        SELECT DISTINCT shift_timezone_name
                        FROM customer_shift
                        WHERE facility = (SELECT _facility FROM qcfg)),
                (SELECT end_time FROM shift_times)
            )                                                                                           end_time
),

camera_cfg as (
    SELECT distinct on (b.beacon_id, c.step_name, c.camera_id)
           b.beacon_id gateway_id,
           fn_uid_name(b.beacon_id) nodename,
           b.subject_name            gw_name,
           c.step_name,
           (select etype from resolver r where b.beacon_id = r.uid and c.step_name = r.sname limit 1) step_function,
           c.camera_id,
           b.facility,
           b.zone_name,
           teb.state_name subject_name,
           c.measure_name,
           teb.cycle_name,
           c.extract_type,
           c.extract_key,
           c.extract_image_name,
           c.cs_config::jsonb,
           c.composite_definition::jsonb[],
           c.smoothing_look_ahead_slots look_ahead,
           c.smoothing_look_back_slots  look_back,
           c.start_time,
           c.end_time,
           c.hidden,
           c.violation,
           teb.start_cycle is_start,
           teb.end_cycle is_end,
           (not teb.start_cycle and not teb.end_cycle) is_intermediate,
           teb.is_whitelist,
           teb.priority
    FROM customer_binding b
    LEFT JOIN customer_report_config c
        ON b.subject_name = c.gateway_name
       AND (c.start_time IS NULL OR (SELECT start_time FROM times) >= c.start_time)
       AND (c.end_time IS NULL OR (SELECT end_time FROM times) < c.end_time)
       AND b.facility = c.facility
       AND c.extract_type in ('ocr', 'lpr', 'weight')
    LEFT JOIN customer_transition_event_binding teb
        ON b.beacon_id = teb.gateway_id
    WHERE b.facility = (SELECT _facility FROM qcfg)
       AND c.step_name is not null
       AND c.measure_name is not null
       AND c.step_name = teb.state_name
       AND b.start_time IS NULL OR (SELECT start_time FROM times) >= b.start_time
       AND b.end_time IS NULL OR (SELECT end_time FROM times) < b.end_time
),

raw_cycle_data AS (
    SELECT  _candidates.plate AS                                                                        alt_subject_id,
            _candidates.confidence                                                                      alt_score,
            step_output ->> 'identifier'                                                                subject_id,
            (step_output ->> 'confidence')::float                                                       subject_score,
            collection_time,
            (gateway_id, camera_id, step_name, collection_time, 'annotated.jpg')::image_reference       img_keys,
            ocid
    FROM image_step i,
         jsonb_to_recordset(
                 (i.step_output ::jsonb #> '{fullResponse,results,0}' ->> 'candidates')::jsonb) AS _candidates(plate text, confidence float, matches_template int)
    WHERE i.collection_time between (SELECT start_time FROM times) AND (SELECT end_time FROM times)
      AND i.step_output ->> 'identifier' IS NOT NULL
      AND (step_output ->> 'confidence')::float >= (select candidate_confidence_threshold from qcfg)
      AND _candidates.confidence >= (SELECT candidate_confidence_threshold FROM qcfg)
      AND json_typeof(i.step_output #> '{fullResponse,results,0,candidates}') = json_typeof('[]')
),

filtered_raw_cycle_data as(
     SELECT alt_subject_id,
            alt_score,
            subject_id,
            subject_score,
            img_keys,
            ocid,
            (CASE
                WHEN (levenshtein((LAG(subject_id, 1) over w), subject_id) < 2)
                        AND ((LAG(subject_score, 1) over w) > subject_score)
                    THEN TRUE
                ELSE FALSE
            END) block
     FROM raw_cycle_data
     WINDOW w AS (PARTITION BY (img_keys).gateway_id ORDER BY ocid)
),

selections AS (
    SELECT  alt_subject_id,
            subject_id,
            (SUM(alt_score) * COUNT(DISTINCT (img_keys).gateway_id))                        score,
            array_agg(ocid)                                                                 ocid,
            RANK() OVER (PARTITION BY alt_subject_id
                ORDER BY SUM(alt_score) * (COUNT(DISTINCT (img_keys).gateway_id)) desc)     rank
    FROM filtered_raw_cycle_data
    WHERE NOT block
    GROUP BY alt_subject_id, subject_id
    ORDER BY alt_subject_id, score
),

correlations AS (
    SELECT  alt_subject_id,
            subject_id,
            reverse(subject_id)                 reversed_id,
            max(score)                          score,
            ocid
    FROM    selections s
    WHERE   rank = 1 and score > 100
    GROUP BY alt_subject_id, subject_id, score, ocid
    ORDER BY s.alt_subject_id
),

modified AS (
     select *
     from (
         SELECT alt_subject_id,
                (CASE
                     WHEN (SELECT TRUE
                           FROM correlations c
                           WHERE array[rcd.ocid]::uuid[] && c.ocid
                           LIMIT 1)
                         THEN (select c.subject_id
                                 from correlations c
                                WHERE array[rcd.ocid]::uuid[] && c.ocid
                                LIMIT 1)
                     WHEN (SELECT TRUE
                          FROM correlations c
                          WHERE rcd.alt_subject_id = c.alt_subject_id
                          limit 1)
                        THEN (select c.subject_id
                              from correlations c
                              where rcd.alt_subject_id = c.alt_subject_id
                              order by score
                              limit 1)
                     WHEN (SELECT TRUE
                           FROM correlations c
                           WHERE rcd.subject_id = c.alt_subject_id
                           limit 1)
                         THEN (select c.subject_id
                                 from correlations c
                                where rcd.subject_id = c.alt_subject_id
                             order by c.score
                                limit 1)
                    ELSE rcd.subject_id
                END) subject_id,
                rcd.subject_score,
                rcd.img_keys
         FROM raw_cycle_data rcd
         WHERE rcd.subject_id IS NOT NULL
    ) m
),

raw_cycle_events AS (
     SELECT subject_id,
            max(subject_score)                                      score,
            (m.img_keys).collection_time                            start_time,
            (
                subject_id,
                subject_score,
                (m.img_keys).gateway_id,
                (m.img_keys).camera_id,
                (m.img_keys).step_name,
                'lpr'::text,
                (m.img_keys).collection_time,
                cc.facility,
                cc.subject_name,
                cc.is_start,
                cc.is_end,
                cc.priority,
                cc.is_whitelist,
                m.img_keys
            )::raw_cycle_event                                      ce_record
     FROM modified m
     LEFT JOIN camera_cfg cc
            ON cc.gateway_id = (m.img_keys).gateway_id
           AND cc.step_name = (m.img_keys).step_name
           AND cc.camera_id = (m.img_keys).camera_id
           AND cc.facility = (select _facility from qcfg)
     GROUP BY m.subject_id, m.subject_score, m.img_keys, cc.facility, cc.subject_name, cc.priority,
              cc.is_start, cc.is_end, cc.is_whitelist, cc.gateway_id
     ORDER BY start_time DESC
),

filtered_cycle_events as(
     SELECT subject_id,
            score,
            start_time,
            ce_record,
            (CASE
                WHEN (levenshtein((LAG(subject_id, 1) over w), subject_id) < 2)
                        AND ((LAG(score, 1) over w) > score)
                    THEN TRUE
                ELSE FALSE
            END) block
     FROM raw_cycle_events rce WINDOW w AS (PARTITION BY (rce.ce_record).gateway_id ORDER BY start_time)
),

image_event_transition as (
    select (x.subject_id)                            subject_id,
           (x.score)                                 score,
           (x.gateway_id)                            gateway_id,
           (x.camera_id)                             camera_id,
           (x.step_name)                             step_name,
           (x.collection_time)                       start_time,
           (x.gateway_id)                            beacon_id,
           (x.subject_id)                            gw_name,
           (x.facility)                              facility,
           (x.state)                                 state,
           (x.start_cycle)                           start_cycle,
           (x.end_cycle)                             end_cycle,
           (x.priority)                              priority,
           (x.whitelist)                             is_whitelist,
           (v.attr)                                  vattr,
           (x.img_keys)                              img_keys
    from (select (ce_record).*
          from filtered_cycle_events
         ) x
    LEFT JOIN camera_cfg cc
            ON cc.gateway_id = (x.img_keys).gateway_id
           AND cc.step_name = (x.img_keys).step_name
    LEFT JOIN LATERAL (
        SELECT (CASE
                    WHEN cc.zone_name = 'scale'
                        THEN (select (attr_name, attr_value, attr_flag, img_keys)::vattr
                               from (SELECT 'weight'::text                                                      attr_name,
                                            (i.step_output::jsonb ->> 'weight')::int                            attr_value,
                                            (i.step_output::jsonb ->> 'warning')                                attr_flag,
                                            (i.gateway_id,
                                                i.camera_id,
                                                i.step_name,
                                                i.collection_time,
                                                substring (i.images[1] FROM '_([^_]*)$'))::image_reference     img_keys
                                     from image_step i
                                     where i.collection_time between ((x.img_keys).collection_time - interval '1 second' * cc.look_back) and
                                                                     ((x.img_keys).collection_time + interval '1 second' * cc.look_ahead)
                                       AND ((i.step_output::jsonb ->> 'weight') is not null AND
                                            (i.step_output::jsonb ->> 'weight')::int > 0)
                                     group by i.gateway_id, i.camera_id, i.step_name, i.collection_time, i.step_output::jsonb
                                     order by attr_value desc
                                     limit 1
                                   ) w
                                )
                    WHEN cc.zone_name = 'container_id'
                        THEN  (select (attr_name, attr_value, attr_flag, img_keys)::vattr
                               from (SELECT 'container_id'::text                                                attr_name,
                                            COALESCE((i.step_output::jsonb ->> 'text'),
                                                     (i.step_output::jsonb ->> 'container_id'))                 attr_value,
                                            (i.step_output::jsonb ->> 'warning')                                attr_flag,
                                            (i.gateway_id,
                                                i.camera_id,
                                                i.step_name,
                                                i.collection_time,
                                                substring (i.images[1] FROM '_([^_]*)$'))::image_reference     img_keys
                                     from image_step i
                                     where i.collection_time between ((x.img_keys).collection_time - interval '1 second' * cc.look_back) and
                                                                     ((x.img_keys).collection_time + interval '1 second' * cc.look_ahead)
                                       AND (
                                             (i.step_output::jsonb ->> 'text' is not null AND
                                              i.step_output::jsonb ->> 'text' != '{}')
                                             OR
                                             (i.step_output::jsonb ->> 'container_id' is not null AND
                                              i.step_output::jsonb ->> 'container_id' != '{}')
                                         )
                                     group by i.gateway_id, i.camera_id, i.step_name, i.collection_time, i.step_output::jsonb
                                     order by i.collection_time desc
                                     limit 1
                                   ) cid
                                )
                    END) attr
     ) v on true
),

extended AS (
    SELECT  namespace,
            beacon_id,
            gateway_id,
            gateway_name,
            gateway_facility,
            gateway_radius,
            slot + (ofst * INTERVAL '1 minute') slot,
            (CASE
                -- no time gap
                WHEN ofst = 0 THEN rssi
                -- fill IN small gaps with the worse of the two values
                WHEN gap <= 5 AND ofst < gap THEN LEAST(future_rssi, rssi)
            END)  rssi,
            (CASE
                WHEN ofst = 0 THEN reading_count
                WHEN gap <= 5 AND ofst < gap THEN 0
            END) reading_count,
            tx_power
    FROM (
        SELECT  namespace,
                beacon_id,
                gateway_id,
                gateway_name,
                gateway_facility,
                gateway_radius,
                slot,
                rssi,
                tx_power,
                reading_count,
                CASE
                    WHEN gap > 1 THEN (SELECT * FROM generate_series(0, LEAST(gap-1, 4), 1))
                    ELSE 0
                END ofst,
                gap,
                future_rssi
        FROM (
            SELECT  r.namespace,
                    r.beacon_id,
                    r.gateway_id,
                    gateway_binding.facility gateway_facility,
                    COALESCE(gateway_binding.zone_name, gateway_binding.subject_name, r.node_name) gateway_name,
                    r.slot,
                    r.avg_rssi rssi,
                    r.reading_count,
                    r.tx_power_1m tx_power,
                    EXTRACT(EPOCH FROM LEAD(r.slot) OVER (PARTITION BY r.namespace, r.beacon_id, r.gateway_id ORDER BY r.slot) - r.slot)::NUMERIC / 60 gap,
                    LEAD(r.avg_rssi) OVER (PARTITION BY r.namespace, r.beacon_id, r.gateway_id ORDER BY r.slot) future_rssi,
                    gateway_binding.radius gateway_radius
            FROM beacon_reading r
            LEFT OUTER JOIN customer_binding gateway_binding
               ON 'ad135bf7eecc08752f96' = gateway_binding.namespace -- the Atollogy Edystone gateway namespace
              AND r.gateway_id = gateway_binding.beacon_id
              AND (gateway_binding.start_time IS NULL OR r.slot >= gateway_binding.start_time)
              AND (gateway_binding.end_time IS NULL OR r.slot < gateway_binding.end_time)
            JOIN customer_beacon_transition_event_binding b
               ON b.zone_name = COALESCE(gateway_binding.zone_name, gateway_binding.subject_name, r.node_name)
              AND b.facility = gateway_binding.facility
            JOIN times t
               ON r.slot >= t.start_time
              AND r.slot < t.end_time
            WHERE (gateway_binding.radius IS NULL OR gateway_binding.radius > 0)
              AND gateway_binding.subject = 'gateway'
        ) AS x
    ) AS y
),

moving_avg AS (
    SELECT  namespace,
            beacon_id,
            gateway_id,
            gateway_facility,
            gateway_name,
            gateway_radius,
            slot,
            (CASE
               WHEN reading_count IS NOT NULL THEN (SUM(reading_count::float) OVER w / COUNT(reading_count) OVER w)
            END) reading_count,
            (CASE
               WHEN rssi IS NOT NULL THEN (SUM(rssi) OVER w  / COUNT(rssi) OVER w)
            END) rssi,
            (COUNT(rssi) OVER w) tx_power
    FROM extended
         WINDOW w AS (PARTITION BY namespace, beacon_id, gateway_id ORDER BY slot ROWS BETWEEN (SELECT _lookback_slots FROM qcfg) PRECEDING AND CURRENT ROW)
),

-- This has the smoothed RSSI AND distance by beacon AND gateway
smoothed AS (
    SELECT namespace,
           beacon_id,
           gateway_id,
           gateway_facility,
           gateway_name,
           gateway_radius,
           slot,
           reading_count,
           rssi,
           tx_power,
           POW(10, (tx_power - rssi) / 20.0) distance
    FROM moving_avg
),

-- this has the basic winner election of the best gateway for each beacon
winner AS (
    SELECT  b.subject subject,
            winner.distance score,
            winner.namespace,
            winner.beacon_id,
            winner.slot,
            winner.gateway_id,
            winner.distance,
            COALESCE(b.subject_name, winner.beacon_id) beacon_name,
            winner.gateway_facility,
            winner.gateway_name,
            b.binding_id beacon_binding
    FROM (
        SELECT  DISTINCT ON (namespace, beacon_id, slot) namespace,
                beacon_id,
                slot,
                distance,
                gateway_id,
                gateway_facility,
                gateway_name
        FROM smoothed
        WHERE rssi IS NOT NULL
          AND distance <= COALESCE(gateway_radius, 10)
        ORDER BY 1, 2, 3, 4 ASC
    ) winner
    LEFT OUTER JOIN customer_binding b
        ON  b.subject <> 'gateway'
       AND winner.beacon_id = b.beacon_id
       AND (b.start_time IS NULL OR winner.slot >= b.start_time)
       AND (b.end_time IS NULL OR winner.slot < b.end_time)
),

-- requires customer_beacon_transition_event_binding: zone_name, start_cycle, end_cycle
beacon_event_transition AS (
    SELECT  w.beacon_id,
            w.distance score,
            w.slot start_time,
            w.beacon_name,
            b.facility,
            COALESCE(b.state_name, w.gateway_name) state,
            b.start_cycle,
            b.end_cycle,
            FALSE as is_whitelist,
            b.priority
    FROM winner w
    JOIN customer_beacon_transition_event_binding b
      ON b.zone_name = w.gateway_name
     AND b.facility = w.gateway_facility
),

summarized_asset_attribute AS (
    SELECT  asset_id,
            MIN(latest_seen_time)                                                                                               latest_seen_time,
            json_object_agg(qualifier, json_build_object('attr_value', attribute_value, 'last_seen', latest_seen_time))::jsonb  attributes,
            array_remove(ARRAY_AGG(DISTINCT(CASE WHEN a.qualifier in (select * from unnest((SELECT _tag_names FROM qcfg))x) THEN attribute_value ELSE NULL END)), NULL) tags,
            (array_to_json(array_remove(ARRAY_AGG(a.asset_info -> 'source_image_key'), NULL))::jsonb -> -1)                     asset_img_keys
    FROM (
        SELECT  DISTINCT ON (asset_id, qualifier) a.*
        FROM asset_attribute a
        JOIN times t
          ON (a.latest_seen_time IS NULL)
            OR (a.latest_seen_time BETWEEN t.start_time AND t.end_time)
        ORDER BY asset_id, qualifier, latest_seen_time DESC
    ) AS a
    GROUP BY asset_id
),

asset_img_transition AS (
    SELECT  iet.subject_id,
            iet.score,
            iet.start_time,
            iet.gw_name,
            iet.facility,
            iet.state,
            iet.start_cycle,
            iet.end_cycle,
            iet.is_whitelist,
            iet.priority,
            lia.attributes,
            lia.tags,
            iet.vattr,
            iet.img_keys,
            lia.asset_img_keys
    FROM image_event_transition iet
    JOIN times t
       ON iet.start_time BETWEEN t.start_time AND t.end_time
    LEFT JOIN LATERAL (
        select attributes,
               tags,
               asset_img_keys
        from summarized_asset_attribute saa
        where iet.subject_id = saa.asset_id
    ) lia on true
),

asset_beacon_transition AS (
    SELECT  saa.asset_id        subject_id,
            bet.score,
            bet.start_time,
            'beacon'::text      gw_name,
            bet.facility,
            bet.state,
            bet.start_cycle,
            bet.end_cycle,
            bet.is_whitelist,
            bet.priority,
            saa.attributes,
            saa.tags,
            null::vattr,
            null::image_reference,
            saa.asset_img_keys
    FROM beacon_event_transition bet
    JOIN summarized_asset_attribute saa
      ON bet.beacon_name = (saa.attributes->'beacon_id'->>'attr_value')
),

asset_transition AS (
    SELECT * FROM asset_img_transition
    UNION
    SELECT * FROM asset_beacon_transition
    WHERE subject_id IS NOT NULL
),

filtered_items AS (
    SELECT asset_id subject_id
    FROM asset_attribute
    WHERE attribute_value in (select * from unnest((SELECT _tag_filters FROM qcfg))x)
      AND qualifier = 'qc_tag'
),

-- remove duplicate readings
coalesced_event_transition AS (
    SELECT  MIN(start_time) start_time,
            MAX(start_time) max_start_time,
            subject_id,
            score,
            tags,
            attributes,
            facility,
            state,
            bool_or(start_cycle) start_cycle,
            bool_or(end_cycle) end_cycle,
            bool_or(is_whitelist) is_whitelist,
            AVG(priority) cycle_priority,
            (CASE
                WHEN (vattr).attr_name = 'weight' THEN (vattr).attr_value
                ELSE NULL
            END) weight,
            (CASE
                WHEN (vattr).attr_name = 'container_id' THEN container_number((vattr).attr_value)
                ELSE NULL
            END) container_id,
            (vattr).attr_flag AS warning,
            (CASE
               WHEN (vattr).attr_value IS NOT NULL AND (vattr).attr_value != '{}'
                   THEN json_build_object(
                       'lpr', array_to_json(array_remove(ARRAY_AGG(DISTINCT (img_keys)), NULL)),
                       'asset', array_to_json(array_remove(ARRAY_AGG(DISTINCT (asset_img_keys)), NULL)),
                       (vattr).attr_name, array_to_json(array_remove(ARRAY_AGG(DISTINCT((vattr).img_keys)), NULL))
                   )
               ELSE json_build_object(
                    'lpr', array_to_json(array_remove(ARRAY_AGG(DISTINCT (img_keys)), NULL)),
                    'asset', array_to_json(array_remove(ARRAY_AGG(DISTINCT (asset_img_keys)), NULL))
                   )
             END) img_keys
    FROM (
        SELECT  *,
                SUM(transition_count) OVER (PARTITION BY facility, subject_id
                                                ORDER BY start_time ASC) grp
        FROM (
            SELECT  *,
                    (CASE
                        WHEN state = (LAG(state) OVER w) AND
                             start_time < ((LAG(start_time) OVER w) + interval '30 seconds')
                            THEN 0
                        ELSE 1
                    END) transition_count
            FROM asset_transition
                WINDOW w AS (PARTITION BY facility, subject_id ORDER BY start_time ASC)
        ) AS a
    ) AS b
    WHERE facility is not null
      AND state is not null
    GROUP BY  subject_id, facility, state, grp, tags, attributes, score, weight, container_id, warning, b.vattr
),

cycle_states AS (
    SELECT  start_time,
            end_time,
            subject_id,
            facility,
            state,
            weight,
            container_id,
            (SUM(cycle_cnt) OVER w) cycle_id,
            is_start,
            is_end,
            is_whitelist,
            priority,
            warning,
            (CASE
                WHEN x.is_end THEN '{whitelist}'::text[]
                ELSE x.tags
            END) tags,
                   (CASE
            WHEN x.attributes is null and x.is_whitelist
                THEN json_build_object(
                        'qc_tag', json_build_object('last_seen', x.start_time::text, 'attr_value', 'whitelist')::jsonb,
                        'license_plate', json_build_object('last_seen', x.start_time::text, 'attr_value', subject_id )::jsonb
                    )::jsonb
                ELSE x.attributes
            END) attributes,
            img_keys
    FROM (
        SELECT  *,
                max_start_time end_time,
                (CASE
                    WHEN start_cycle IS TRUE AND end_cycle IS FALSE AND (LAG(start_cycle) OVER w) IS FALSE THEN 1
                    ELSE 0
                END) cycle_cnt,
                start_cycle is_start,
                end_cycle is_end,
                cycle_priority priority
        FROM coalesced_event_transition
        WHERE ((SELECT COUNT(*) FROM filtered_items) = 0 OR(subject_id IN (SELECT subject_id FROM filtered_items))) --if whitelist filter supplied THEN apply it
          AND NOT (SELECT BOOL_OR(subject_id like asset_id) FROM asset_attribute WHERE attribute_value = 'blacklist' AND qualifier = 'qc_tag')
        WINDOW w AS (PARTITION BY facility, subject_id ORDER BY start_time ASC)
    ) x
    WINDOW w AS (PARTITION BY facility, subject_id ORDER BY start_time ASC)
),

cycle_weights as (
    select *
    from cycle_states
),

cycle_weights_with_other_imgs as (
    select *
    from cycle_states
),

visit_log AS (
  SELECT *
  FROM(
      SELECT  DISTINCT ON (facility, subject_id, start_time)
              facility,
              subject_id,
              subject_id || COALESCE(' (' || (attributes -> 'Truck Type' ->> 'attr_value') || ':#' || (attributes -> 'Unit Number' ->> 'attr_value') || ')', '') extended_subject_id,
              state last_state,
              start_time last_state_start_time,
              COUNT(CASE WHEN is_start THEN 1 END) OVER w visit_count,
              img_keys,
              is_start,
              is_end,
              priority,
              tags,
              attributes
      FROM cycle_states
      WHERE (is_start = TRUE OR is_end = TRUE)
          WINDOW w AS (PARTITION BY subject_id, facility)
      ORDER BY facility, subject_id, start_time, COALESCE(end_time, start_time) DESC
  ) x
  ORDER BY x.subject_id, x.last_state_start_time DESC
),

cycle_log AS (
    SELECT  start_time,
            (CASE WHEN end_time IS NULL THEN now() ELSE end_time END) end_time,
            subject_id || COALESCE(' (' || (cwwoi.attributes -> 'Truck Type' ->> 'attr_value') || ':#' || (cwwoi.attributes -> 'Unit Number' ->> 'attr_value') || ')', '') subject_id,
            (CASE
                WHEN is_whitelist THEN array['whitelist']::text[]
                ELSE cwwoi.tags
            END) tags,
            (CASE
                WHEN is_whitelist
                    THEN json_build_object(
                            'qc_tag', json_build_object('last_seen', start_time::text,
                                                         'attr_value', 'whitelist')::jsonb,
                            'license_plate', json_build_object('last_seen', start_time::text,
                                                               'attr_value', subject_id )::jsonb
                    )::jsonb
                ELSE cwwoi.attributes
            END) attributes,
            facility,
            state,
            cycle_id,
            weight,
            container_id,
            img_keys,
            is_start,
            is_end,
            priority,
            warning,
            COUNT(CASE WHEN is_start THEN 1 END) OVER w visit_count
    FROM cycle_states cwwoi
         WINDOW w AS (PARTITION BY subject_id, facility)
    ORDER BY start_time DESC
)

select * from cycle_states;