WITH shift_times AS (
    SELECT shift_name, date::date::text, start_time shift_start, end_time shift_end, shift_timezone_name
    FROM (
      SELECT
        date,
        shift_name,
        shift_timezone_name,
        CASE WHEN :previous THEN (lag(shift_start) OVER (ORDER BY date)) ELSE shift_start END start_time,
        CASE WHEN :previous THEN (lag(shift_end) OVER (ORDER BY date)) ELSE shift_end END end_time
      FROM (
        SELECT
          d date,
          s.shift_name,
          d AT TIME ZONE s.shift_timezone_name + s.start_offset shift_start,
          d AT TIME ZONE s.shift_timezone_name + s.end_offset shift_end,
          s.shift_timezone_name
        FROM generate_series(
                 COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE - INTERVAL '7 days',
                 COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE + INTERVAL '7 days',
                 INTERVAL '1 day'
             ) d
        JOIN customer_shift s ON s.day_of_week IS NULL OR s.day_of_week = date_part('dow', d)
        WHERE ((:shift::text IS NULL) OR shift_name = :shift) AND (facility IS NULL OR s.facility = :facility)
      )x
    )y
    WHERE date = COALESCE(:date, (now()::TIMESTAMP AT TIME ZONE 'UTC' AT TIME ZONE shift_timezone_name)::date)
),
times as (
    SELECT
      coalesce((select min(shift_start) from shift_times), :from::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM customer_shift)) start_time,
      coalesce((select max(shift_end) from shift_times), :to::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM customer_shift)) end_time
)
SELECT *
FROM customer_report_config c
JOIN times t ON
    (c.start_time IS NULL OR t.start_time >= c.start_time)
    AND (c.end_time IS NULL OR t.end_time < c.end_time)
    WHERE facility = :facility
