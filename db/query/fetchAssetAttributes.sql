SELECT asset_id, attribute_value, qualifier
FROM asset_attribute
WHERE asset_id = ANY(:license_plates)
AND qualifier = ANY(:qualifiers)