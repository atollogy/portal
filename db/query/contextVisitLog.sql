WITH shift_times_new AS (
    SELECT DISTINCT ON (date, shift_name) shift_name,
                                          date::date::text,
                                          shift_start start_time,
                                          shift_end   end_time,
                                          shift_timezone_name
    FROM (
             SELECT date,
                    shift_name,
                    shift_timezone_name,
                    shift_start,
                    shift_end
             FROM (
                      SELECT d                                                     date,
                             s.shift_name,
                             d AT TIME ZONE s.shift_timezone_name + s.start_offset shift_start,
                             d AT TIME ZONE s.shift_timezone_name + s.end_offset   shift_end,
                             s.shift_timezone_name
                      FROM generate_series(
                                       COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE - INTERVAL '7 days',
                                       COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE,
                                       INTERVAL '1 day'
                               ) d
                               JOIN customer_shift s ON s.day_of_week IS NULL OR s.day_of_week = date_part('dow', d)
                      WHERE (:shift::text is null or shift_name = :shift)
                        AND facility = :facility
                  ) x
         ) y
    WHERE date = COALESCE(:date, now()::date)::TIMESTAMP WITHOUT TIME ZONE
),
     shift_times AS (
         SELECT to_timestamp(:date, 'YYYY-MM-DD')::timestamp without time zone AT TIME ZONE
                s.shift_timezone_name + s.start_offset start_time,
                to_timestamp(:date, 'YYYY-MM-DD')::timestamp without time zone AT TIME ZONE
                s.shift_timezone_name + s.end_offset   end_time
         FROM customer_shift s
         WHERE s.shift_name = :shift
           AND (CAST(:facility AS TEXT) IS NULL OR COALESCE(s.facility, :facility) = :facility)
           AND (s.day_of_week is null OR s.day_of_week = date_part('dow',
                                                                   to_timestamp(:date, 'YYYY-MM-DD')::timestamp without time zone AT TIME ZONE
                                                                   'UTC'))
     ),
     times as (
         select coalesce(:from::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name
                                                                          FROM customer_shift
                                                                          WHERE (facility = :facility OR facility IS NULL)),
                         (select start_time from shift_times)) start_time,
                coalesce(:to::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name
                                                                        FROM customer_shift
                                                                        WHERE (facility = :facility OR facility IS NULL)),
                         (select end_time from shift_times))   end_time
     ),
     lpr_data as (
         select (CASE
                     WHEN split_part(i.images[1], '/', 4) = 'image_readings'
                         THEN
                         jsonb_build_object('gateway_id', i.gateway_id, 'camera_id', 'video0', 'name',
                                            reverse(split_part(split_part(reverse(i.images[1]), '/', 1), '_', 1)),
                                            'step_name',
                                            split_part(i.images[1], '/', 7), 'collection_time',
                                            to_timestamp(split_part(split_part(i.images[1], '/', 12), '_', 3)::float8))
                     ELSE
                         jsonb_build_object('gateway_id', i.gateway_id, 'camera_id', 'video0', 'name',
                                            reverse(split_part(split_part(reverse(i.images[1]), '/', 1), '_', 1)),
                                            'step_name',
                                            split_part(i.images[1], '/', 7)) END)                           img_keys,
                (CASE
                     WHEN split_part(i.images[1], '/', 4) = 'image_readings'
                         THEN
                         concat('/portal/image/',
                                split_part(i.images[1], '-', 3),
                                '/',
                                i.gateway_id,
                                '/video0/',
                                split_part(split_part(i.images[1], '/', 12), '_', 3),
                                '/',
                                split_part(i.images[1], '/', 7),
                                '/',
                                reverse(
                                        split_part(split_part(reverse(i.images[1]), '/', 1), '_', 1)))
                     ELSE
                         concat('/portal/image/',
                                split_part(i.images[1], '-', 3),
                                '/',
                                i.gateway_id,
                                '/video0/',
                                split_part(split_part(i.images[1], '/', 13), '_', 3),
                                '/',
                                split_part(i.images[1], '/', 7),
                                '/',
                                reverse(
                                        split_part(split_part(reverse(i.images[1]), '/', 1), '_', 1))) END) media_link,
                i.collection_time,
                (EXTRACT(EPOCH FROM i.collection_time))::float                                              ctime,
                i.collection_interval,
                i.gateway_id,
                i.camera_id,
                i.step_name,
                text2ltree(concat(i.gateway_id, '.', i.camera_id, '.', i.step_name))                        epath,
                text2ltree(step_function)                                                                   etype,
                i.step_function,
                array_agg((i.step_output ->> 'identifier'))                                                 plate,
                array_agg((i.step_output ->> 'confidence')::float)                                          confidence,
                (select fn_get_event_number(concat(i.gateway_id, '.', i.camera_id, '.', i.step_name),
                                            i.step_function,
                                            i.collection_time,
                                            case
                                                when i.step_function = 'lpr' then interval '4 seconds'
                                                when i.step_function = 'videoRecording' then interval '30 seconds'
                                                else i.collection_interval
                                                end))                                                       tnum,
                floor(EXTRACT(EPOCH FROM i.collection_time) / 30)                                           wid,
                (case
                     when i.gateway_id in ('14a78b1d2cdd', '38af29016a4a') then 'Entry'
                     when i.gateway_id in ('38af29016917', '38af294f1452') then 'uncertain'
                     else 'Exit' end)                                                                       direction,
                (case
                     when i.gateway_id in ('38af29016a4a', '38af294f12c4') then 'Exit'
                     else 'Entry' end)                                                                      perspective,
                array_agg(case
                              when i.step_function = 'lpr' then interval '4 seconds'
                              when i.step_function = 'videoRecording'
                                  then ((i.step_output -> 'media_info' -> 'activation' -> 'mp4' ->> 'duration')::float *
                                        '1 second'::interval)
                              else i.collection_interval end)                                               duration

         from image_step i
         where i.gateway_id in ('14a78b1d2cdd', '38af294f12c4', '38af29016917', '38af294f1452', '38af29016a4a')
           AND i.step_function in ('lpr', 'videoRecording')
           AND (i.step_output ->> 'blacklistedContext' IS NULL OR
                (i.step_output ->> 'blacklistedContext')::bool != TRUE)
           AND i.images[1] is not null
           AND i.collection_time BETWEEN (SELECT start_time FROM times limit 1) AND (SELECT end_time FROM times limit 1)
         group by gateway_id, camera_id, step_name, collection_time, tnum, direction, perspective,
                  collection_interval, step_function, media_link, img_keys
         order by ctime, direction, perspective
     ),
     lpr_reads as (
         select gateway_id,
                camera_id,
                step_name,
                epath,
                etype,
                collection_time,
                ctime,
                tnum,
                wid,
                direction,
                perspective,
                collection_interval,
                step_function,
                plate[1]                                                           plate,
                confidence[1]                                                      confidence,
                media_link,
                img_keys,
                rank() over (partition by gateway_id, tnum order by confidence[1]) rank
         from lpr_data ld
         where step_function = 'lpr'
           and plate[1] is not null
           and confidence[1] > 60.0
           and plate[1] not in (select asset_id from asset_attribute where attribute_value = 'blacklist')
         group by gateway_id, tnum, confidence, camera_id, step_name, collection_time, ctime, direction, perspective,
                  wid,
                  collection_interval, step_function, plate, media_link, img_keys, epath, etype
     ),
     selected as (
         select wid,
                perspective,
                min(collection_time)                       start_time,
                max(collection_time + collection_interval) end_time
         from lpr_reads lr
         where rank = 1
         group by wid, perspective
     ),
     video_context as (
         select wid,
                array_agg(collection_time) ctimes,
                array_agg(perspective)     perspectives,
                array_agg(media_link)      media_links,
                array_agg(img_keys)        image_keys,
                array_agg(duration[1])     durations
         from lpr_data ld
         where step_function = 'videoRecording'
           and media_link is not null
           and img_keys ->> 'name' != ''
           and wid not in (select wid from selected where perspective = ld.perspective)
         group by wid
         order by 1
     ),
     records as (
         SELECT :facility                                          facility,
                null                                               subject_id,
                null                                               last_state,
                ctimes[cardinality(ctimes)]                        last_state_start_time,
                '0'                                                visit_count,
                false                                              is_start,
                false                                              is_end,
                0                                                  priority,
                array_agg(perspectives[cardinality(perspectives)]) tags,
                null                                               attributes,
                image_keys[cardinality(image_keys)]                img_keys,
                (select media_link
                 from lpr_data ld
                 where ld.wid = wid
                   and ld.step_function = 'lpr'
                   and ld.collection_time between (ctimes[cardinality(ctimes)] - interval '15 seconds') and (ctimes[cardinality(ctimes)] + interval '30 seconds')
                   and ld.perspective = perspectives[cardinality(perspectives)]
                 limit 1)                                          lpr_link,
                media_links[cardinality(media_links)]              video_link
         from video_context
         where cardinality(perspectives) > 1
         group by ctimes,
                  perspectives,
                  media_links,
                  image_keys,
                  durations
         order by last_state_start_time
     )
select *
from records
where lpr_link is not null
order by last_state_start_time;