-- For the last nn hours, list the distinct beacons seen and the zone it was last seen in.

WITH --
shift_times AS (
 SELECT to_timestamp(:date, 'YYYY-MM-DD')::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE
        s.shift_timezone_name + s.start_offset AS start_time,
        to_timestamp(:date, 'YYYY-MM-DD')::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE
        s.shift_timezone_name + s.end_offset   AS end_time
 FROM customer_shift s
 WHERE s.shift_name = :shift AND
       (CAST(:facility AS TEXT) IS NULL OR COALESCE(s.facility, :facility) = :facility) AND
       (s.day_of_week IS NULL
         OR
        s.day_of_week = date_part('dow',
                                  to_timestamp(:date, 'YYYY-MM-DD')::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE
                                  'UTC'))
),
times AS (
 SELECT coalesce(:from::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM customer_shift), (SELECT start_time FROM shift_times)) AS start_time,
        coalesce(:to::TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE (SELECT DISTINCT shift_timezone_name FROM customer_shift), (SELECT end_time FROM shift_times)) AS end_time
),
extended AS (
 SELECT namespace,
        beacon_id,
        gateway_id,
        gateway_name,
        gateway_facility,
        gateway_radius,
        slot + (ofst * INTERVAL '1 second' * slot_size) slot,
        CASE WHEN ofst = 0
               THEN rssi
             WHEN gap <= 5 AND ofst < gap
               -- fill in small gaps with the worse of the two values
               THEN LEAST(future_rssi, rssi)
          END                             AS rssi,
        CASE WHEN ofst = 0
               THEN reading_count
             WHEN gap <= 5 AND ofst < gap
               THEN 0 END                 AS reading_count,
        tx_power
 FROM (
        SELECT namespace,
               beacon_id,
               gateway_id,
               gateway_name,
               gateway_facility,
               gateway_radius,
               slot,
               slot_size,
               rssi,
               tx_power,
               reading_count,
               CASE WHEN gap > 1
                      THEN generate_series(0, LEAST(gap - 1, 4), 1)
                    ELSE 0 END AS ofst,
               gap,
               future_rssi
        FROM (
               SELECT r.namespace,
                      r.beacon_id,
                      r.gateway_id,
                      gateway_binding.facility                   AS gateway_facility,
                      coalesce(gateway_binding.zone_name, gateway_binding.subject_name,
                               r.node_name)                      AS gateway_name,
                      r.slot,
                      r.avg_rssi                                 AS rssi,
                      r.reading_count,
                      r.tx_power_1m                              AS tx_power,
                      extract(
                          EPOCH FROM lead(r.slot) OVER
                        (PARTITION BY r.namespace, r.beacon_id, r.gateway_id
                        ORDER BY r.slot) - r.slot)::NUMERIC / 60 AS gap,
                      lead(r.avg_rssi)
                           OVER
                             (PARTITION BY r.namespace, r.beacon_id, r.gateway_id
                             ORDER BY r.slot)                    AS future_rssi,
                      gateway_binding.radius                     AS gateway_radius,
							        gateway_binding.beacon_slot_size_seconds slot_size
               FROM beacon_reading r
                      LEFT OUTER JOIN customer_binding gateway_binding ON
                   r.gateway_id = gateway_binding.beacon_id
                   AND
                   (gateway_binding.start_time IS NULL
                     OR r.slot >= gateway_binding.start_time)
                   AND
                   (gateway_binding.end_time IS NULL
                     OR r.slot < gateway_binding.end_time)
                      JOIN customer_beacon_transition_event_binding b
                           ON b.zone_name =
                              coalesce(gateway_binding.zone_name, gateway_binding.subject_name, r.node_name)
                             AND b.facility = gateway_binding.facility
                      JOIN times t
                           ON r.slot >= t.start_time
                             AND r.slot < t.end_time
               WHERE (gateway_binding.radius IS NULL OR gateway_binding.radius > 0)
             ) x
      ) y
),
moving_avg AS (
 SELECT namespace,
        beacon_id,
        gateway_id,
        gateway_facility,
        gateway_name,
        gateway_radius,
        slot,
        CASE WHEN reading_count IS NOT NULL
               THEN (sum(reading_count::FLOAT) OVER w / count(reading_count) OVER w)
          END AS reading_count,
        CASE WHEN rssi IS NOT NULL
               THEN (sum(rssi) OVER w / count(rssi) OVER w)
          END AS rssi,
        count(rssi) OVER w,
        tx_power
 FROM extended WINDOW w AS (PARTITION BY namespace, beacon_id, gateway_id
   ORDER BY slot ROWS BETWEEN :lookback_slots PRECEDING AND CURRENT ROW)
),

-- This has the smoothed RSSI and distance by beacon and gateway
smoothed AS (
 SELECT namespace,
        beacon_id,
        gateway_id,
        gateway_facility,
        gateway_name,
        gateway_radius,
        slot,
        rssi,
        tx_power,
        reading_count,
        POW(10, (tx_power - rssi) / 20.0) AS distance
 FROM moving_avg
),

-- this has the basic winner election of the best gateway for each beacon
winner AS (
 SELECT winner.namespace,
        winner.beacon_id,
        winner.slot,
	    b.beacon_slot_size_seconds slot_size,
        winner.gateway_id,
        winner.distance,
        b.subject                  AS subject,
        coalesce(b.subject_name,
                 winner.beacon_id) AS beacon_name,
        winner.gateway_facility,
        winner.gateway_name,
        b.binding_id               AS beacon_binding
 FROM (
        SELECT DISTINCT ON (namespace, beacon_id, slot)
               namespace,
               beacon_id,
               slot,
               distance,
               gateway_id,
               gateway_facility,
               gateway_name
        FROM smoothed
        WHERE rssi IS NOT NULL AND
              distance <= coalesce(gateway_radius, 10)
        ORDER BY 1, 2, 3, 4 ASC
      ) winner
        JOIN customer_binding b ON
     --b.subject <> 'gateway' AND
     winner.beacon_id = b.beacon_id AND
     (b.start_time IS NULL OR winner.slot >= b.start_time) AND
     (b.end_time IS NULL OR winner.slot < b.end_time)
),
last_sighting AS (
 SELECT beacon_binding,
        max(slot) AS slot
 FROM winner
 GROUP BY beacon_binding
)