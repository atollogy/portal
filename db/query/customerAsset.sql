WITH tags AS (
  SELECT
    asset_id,
    array_agg(attribute_value) tags
  FROM asset_attribute
  WHERE qualifier = ANY(:tagNames)
  GROUP BY asset_id
)

SELECT
  a.asset_id,
  t.tags,
  a.asset_info->> 'source_image_key' latest_images,
  a.latest_seen_time last_seen_time,
  split_part((a.asset_info ->> 'source_image_link'), '/', 5) nodename
FROM tags t
JOIN asset_attribute a USING (asset_id)
WHERE a.qualifier = 'license_plate'
ORDER BY a.latest_seen_time DESC
