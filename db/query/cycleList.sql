WITH dates AS (
    SELECT generate_series(
                LEAST((select date_trunc('day',min(collection_time)) from image_step) , (select date_trunc('day',min(slot)) from beacon_reading)),
                GREATEST((select date_trunc('day',max(collection_time)) from image_step), (select date_trunc('day',max(slot)) from beacon_reading)) + INTERVAL '1 day',
                INTERVAL '1 day'
           ) date
),
shift_date AS (
  SELECT
    d.date :: TIMESTAMP WITHOUT TIME ZONE,
    s.shift_name,
    d.date :: TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE s.shift_timezone_name + s.start_offset shift_start,
    d.date :: TIMESTAMP WITHOUT TIME ZONE AT TIME ZONE s.shift_timezone_name + s.end_offset   shift_end,
    s.shift_timezone_name
  FROM
    dates d
    JOIN customer_shift s
      ON s.day_of_week IS NULL OR s.day_of_week = date_part('dow', d.date)
),

cycle_list AS
((
    --image based cycles
    SELECT DISTINCT sd.date, sd.shift_name, b.facility
    FROM shift_date sd
    CROSS JOIN customer_transition_event_binding b
    WHERE (
        SELECT COUNT(*)
        FROM image_step i
        WHERE i.gateway_id = b.gateway_id
            AND i.step_name = b.step_name
            AND i.camera_id = b.camera_id
            AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
            AND (b.end_time IS NULL OR i.collection_time < b.end_time)
            AND i.collection_time >= sd.shift_start
            AND i.collection_time <= sd.shift_end
            AND i.collection_time > '2018-05-07'
        ) > 0

) UNION (
    --beacon based cycles
    SELECT distinct sd.date, sd.shift_name, bb.facility
    FROM shift_date sd
    CROSS JOIN customer_beacon_transition_event_binding bb
    WHERE (
        SELECT COUNT(*)
        FROM beacon_reading br
        JOIN customer_binding gateway_binding
            ON 'ad135bf7eecc08752f96' = gateway_binding.namespace -- the Atollogy Edystone gateway namespace
            AND br.gateway_id = gateway_binding.beacon_id
            AND (gateway_binding.start_time IS NULL OR br.slot >= gateway_binding.start_time)
            AND (gateway_binding.end_time IS NULL OR br.slot < gateway_binding.end_time)
        WHERE br.slot >= sd.shift_start
        AND br.slot < sd.shift_end
        AND coalesce(gateway_binding.zone_name, gateway_binding.subject_name, br.node_name) = bb.zone_name
        AND gateway_binding.facility = bb.facility
        AND br.slot > '2018-05-07'
    ) > 0
))