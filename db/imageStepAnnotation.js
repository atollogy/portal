const fs = require('fs');
const path = require('path');
const db = require('../db/');
const sql = require('yesql').pg;
const user = require('../lib/user-management/');
const _ = require('lodash');
const annotationStats = fs.readFileSync(path.join(__dirname, 'query', 'annotationStats.sql'), 'utf8');


module.exports.setAnnotation = async function (customer, parameters) {
  parameters.step_function_version = Object.is(parameters.step_function_version, undefined) ? ' ' : parameters.step_function_version;
  parameters.step_config_version = Object.is(parameters.step_config_version, undefined) ? ' ' : parameters.step_config_version;
  parameters.session_id = Object.is(parameters.step_function_version, undefined) ? ' ' : parameters.step_function_version;
  parameters.qc_result = Object.is(parameters.qc_result, null) ? ' ' : parameters.qc_result;

  const q = sql(`
      INSERT INTO image_step_annotation
      (collection_time, gateway_id, camera_id, step_name, step_function_version, step_config_version, is_accurate,
       comment, session_id, user_id, qc_result)
      VALUES (:collection_time, :gateway_id, :camera_id, :step_name, :step_function_version, :step_config_version,
              :is_accurate, :comment, :session_id, :user_id, :qc_result);
  `)(parameters);
  try {
    const {rows} = await db.query(customer, q.text, q.values, true);
    return rows;
  } catch (error) {
    throw new Error(`Problems setting annotations. Error is: ${error}.`);
  }
};

module.exports.getAnnotation = async function (customer, parameters) {
  parameters.step_function_version = Object.is(parameters.step_function_version, undefined) ? ' ' : parameters.step_function_version;
  parameters.step_config_version = Object.is(parameters.step_config_version, undefined) ? ' ' : parameters.step_config_version;
  const q = sql(`
      SELECT is_accurate,
             comment,
             created_at_date,
             user_id
      FROM image_step_annotation
      WHERE collection_time = :collection_time
        AND gateway_id = :gateway_id
        AND camera_id = :camera_id
        AND step_name = :step_name
        AND (:step_function_version = ' ' OR step_function_version = :step_function_version)
        AND (:step_config_version = ' ' OR step_config_version = :step_config_version)
      ORDER BY created_at_date DESC
  `)(parameters);

  const {rows: annotations} = await db.query(customer, q.text, q.values);
  const {rows: users} = await user.fetchById(annotations.map(x => x.user_id).reduce((agg, val) => {
    if (!agg.includes(val)) {
      agg.push(val);
    }
    return agg;
  }, []));

  const userLookup = users.reduce((agg, val) => {
    if (!agg[val.user_id]) {
      agg[val.user_id] = val;
    }
    return agg;
  }, {});

  var result = annotations.map(annotation => _.merge(annotation, userLookup[annotation.user_id]));
  return result;
};

module.exports.getAnnotationStats = async function (customer) {
  const statement = annotationStats + 'SELECT * FROM stats';
  try {
    const {rows} = await db.query(customer, statement, []);
    return rows;
  } catch (error) {
    throw new Error(`Problems fetching annotation stats. Error is: ${error}.`);
  }
};

const updateImageStepWeightStatement = sql(`
  UPDATE image_step
  SET step_output = replace(step_output::TEXT, '"weight"', '"original_weight"')::jsonb
  WHERE gateway_id = :gateway_id
    and camera_id = :camera_id
    and step_name = :step_name
    and collection_time = :collection_time
`);

const updateImageStepOutputsStatement = sql(`
  UPDATE image_step
  SET step_output = jsonb_set(step_output::jsonb, '{weight}', CAST(:value AS TEXT), true)
  WHERE gateway_id = :gateway_id
    and camera_id = :camera_id
    and step_name = :step_name
    and collection_time = :collection_time
`);

module.exports.restateScaleWeight = async function (customer, parameters) {
  const value = parameters.weight;

  delete parameters.weight;
  const updateImageStepWeight = updateImageStepWeightStatement(parameters);
  const updateImageStepOutput = updateImageStepOutputsStatement(Object.assign({}, parameters, { value }));

  await db.query(customer, updateImageStepWeight.text, updateImageStepWeight.values, true);
  await db.query(customer, updateImageStepOutput.text, updateImageStepOutput.values, true);

  return 0;
};
