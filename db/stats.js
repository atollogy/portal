const moment = require('moment');
const db = require('../db');
const moize = require('../cache').default;
const debug = require('debug')('portal:db:stats');
const fs = require('fs');
const path = require('path');

const lastImageQuery = fs.readFileSync(path.join(__dirname, 'query', 'lastImage.sql'), 'utf8');

function filteredHash(hash, includedProperties){
  if (!hash) return;
  var result = {};
  includedProperties.forEach((prop) =>{
    result[prop] = hash[prop];
  });
  return result;
}

async function statsForCustomerNoConsul(customer, cutoff) {
  const services = db.query(customer, `
    SELECT DISTINCT gateway_id, application, date_trunc('minute', insert_time) slot, process_state 
      FROM gateway_service_status 
     WHERE insert_time > $1 ORDER BY 3`, [cutoff]);

  const stats = db.query(customer, `SELECT g.gateway_id,
                                           date_trunc('minute', insert_time) AS slot,
                                           SUM(reading_count)                AS reading_count,
                                           SUM(beacon_slot_count)            AS beacon_slot_count,
                                           SUM(backlog_reading_count)        AS backlog_reading_count,
                                           SUM(backlog_beacon_slot_count)    AS backlog_beacon_slot_count
                                      FROM gateway_beacon_stats g
                                     WHERE insert_time > $1
                                     GROUP BY 1, 2
                                     ORDER BY 2`, [cutoff]);

  const statsWithBeacons = db.query(customer, `
    SELECT g.gateway_id,
           node_name,
           date_trunc('minute', insert_time) AS slot,
           SUM(reading_count)                AS reading_count,
           SUM(beacon_slot_count)            AS beacon_slot_count,
           SUM(backlog_reading_count)        AS backlog_reading_count,
           SUM(backlog_beacon_slot_count)    AS backlog_beacon_slot_count
      FROM gateway_beacon_stats g LEFT JOIN (
        SELECT DISTINCT gateway_id, node_name
          FROM beacon_reading
         WHERE insert_time > $1) b
    ON (g.gateway_id = b.gateway_id)
  WHERE insert_time > $2
  GROUP BY 1, 2, 3
  ORDER BY 3`, [cutoff, cutoff]);

  const lastImageSteps = db.query(customer, lastImageQuery);

  const sensors = db.query(customer, `SELECT gateway_id,
                                             split_part(images [1], '/', 5)       AS node_name,
                                             coalesce(processor_role, 'unknown')  AS processor_role,
                                             date_trunc('minute', insert_time)    AS slot,
                                             SUM(array_length(images, 1))         AS image_count
                                        FROM image_step
                                       WHERE insert_time > $1
                                       GROUP BY 1, 2, 3, 4
                                       ORDER BY 3`, [cutoff]);
  const gwNames = db.query(customer, `
    SELECT subject_name, beacon_id gateway_id
      FROM customer_binding 
     WHERE subject = 'gateway' 
       AND COALESCE(start_time, now()) <= now() 
       AND COALESCE(end_time, now()) >= now()
  `);

  const responseObject = {};
  const fetchCustomerStats = customer === 'lumenetix' ? stats : statsWithBeacons;
  await Promise.allSettled([
    services.then(({ rows }) => responseObject.serviceRows = rows || []),
    fetchCustomerStats.then(({ rows }) => responseObject.statsRows = rows || []),
    sensors.then(({ rows }) => responseObject.sensorRows = rows || []),
    gwNames.then(({ rows }) => responseObject.nameRows = rows || []),
    lastImageSteps.then(({ rows }) => responseObject.imageStepRows = rows || [])
  ]);

  const {serviceRows, statsRows, sensorRows, nameRows, imageStepRows} = responseObject;

  var serviceHash = {};
  var sensorHash = {};
  var statsHash = {};
  var nameHash = {};

  var gatewayHash = {};

  const deviceProperties = imageStepRows.map(({gateway_id, node_name}) => {
    const element = {};
    element.customer = customer;
    element.gateway = gateway_id;
    element.nodename = node_name;
    return element;
  });

  serviceRows.forEach(record => {
    if (!serviceHash[record.gateway_id]) {
      serviceHash[record.gateway_id] = {};
      gatewayHash[record.gateway_id] = true;
    }
    if (!serviceHash[record.gateway_id][record.application]) {
      serviceHash[record.gateway_id][record.application] = [];
    }
    serviceHash[record.gateway_id][record.application].push({
      slot: moment(record.slot).startOf('minute'),
      state: record.process_state
    });
  });

  statsRows.forEach(record => {
    if (!statsHash[record.gateway_id]) {
      statsHash[record.gateway_id] = [];
      gatewayHash[record.gateway_id] = true;
    }
    statsHash[record.gateway_id].node_name = record.node_name;
    statsHash[record.gateway_id].push({
      slot: moment(record.slot).startOf('minute'),
      reading_count: Number(record.reading_count),
      beacon_slot_count: Number(record.beacon_slot_count),
      backlog_reading_count: Number(record.backlog_reading_count),
      backlog_beacon_slot_count: Number(record.backlog_beacon_slot_count)
    });
  });

  sensorRows.forEach(record => {
    if (!sensorHash[record.gateway_id]) {
      sensorHash[record.gateway_id] = [{
        slot: moment(record.slot).startOf('minute')
      }];
      gatewayHash[record.gateway_id] = true;
    }
    var list = sensorHash[record.gateway_id];
    sensorHash[record.gateway_id].node_name = record.node_name;
    var last = list[list.length - 1];
    var slot = moment(record.slot).startOf('minute');
    if (slot.diff(last.slot) !== 0) {
      list.push({
        slot: moment(record.slot).startOf('minute')
      });
    }
    if (!list[list.length - 1]['_' + record.processor_role]) {
      list[list.length - 1]['_' + record.processor_role] = 0;
    }
    list[list.length - 1]['_' + record.processor_role] += Number(record.image_count);
  });

  var missing = [];
  nameRows.forEach(record => {
    nameHash[record.gateway_id] = record.subject_name;

    if (!serviceHash[record.gateway_id] && !sensorHash[record.gateway_id] && !statsHash[record.gateway_id]) {
      missing.push({
        gateway_id: record.gateway_id,
        name: record.subject_name,
        nodeName: deviceProperties[record.gateway_id] && deviceProperties[record.gateway_id].nodeName,
        properties: filteredHash(deviceProperties[record.gateway_id], ['hardware', 'video', 'wired']),
        chefStatus: deviceProperties[record.gateway_id] && deviceProperties[record.gateway_id].chefStatus
      });
    }
  });

  return {
    system: customer,
    missing: missing,
    gateways: Object.keys(gatewayHash).map(function (gw) {
      const nodeNames = deviceProperties.filter((theDeviceObject) => gw === theDeviceObject.gateway);

      return {
        gateway_id: gw,
        name: nameHash[gw],
        nodeName: nodeNames.length ? nodeNames[0].nodename : '<unknown>',
        properties: filteredHash(deviceProperties[gw], ['hardware', 'video', 'wired']),
        chefStatus: deviceProperties[gw] && deviceProperties[gw].chefStatus,
        applications: (Object.keys(serviceHash[gw] || {})).filter(function (application) {
          return application !== 'gwstats' && application === 'motion';
        }).map(function (application) {
          return {
            application: application,
            records: serviceHash[gw][application]
          };
        }),
        beacons: statsHash[gw],
        images: sensorHash[gw]
      };
    }).concat(missing)
  };
}

async function deviceStatsForCustomer(customer, cutoff) {
  var services = db.query(customer, 'SELECT DISTINCT gateway_id, application, date_trunc(\'minute\', insert_time) slot, process_state FROM gateway_service_status WHERE insert_time > $1 ORDER BY 3', [cutoff]);
  var stats = db.query(customer, 'SELECT gateway_id, date_trunc(\'minute\', insert_time) slot, SUM(reading_count) reading_count, SUM(beacon_slot_count) beacon_slot_count, SUM(backlog_reading_count) backlog_reading_count,  SUM(backlog_beacon_slot_count) backlog_beacon_slot_count FROM gateway_beacon_stats WHERE insert_time > $1 GROUP BY 1,2 ORDER BY 2', [cutoff]);
  var sensors = db.query(customer, 'SELECT gateway_id, coalesce(processor_role, \'unknown\') processor_role, date_trunc(\'minute\', insert_time) slot, SUM(array_length(images,1)) image_count FROM image_step WHERE insert_time > $1 GROUP BY 1,2,3 ORDER BY 3', [cutoff]);
  var gwNames = db.query(customer, 'SELECT subject_name, beacon_id gateway_id, zone_name from customer_binding where subject = \'gateway\' AND COALESCE(start_time, now()) <= now() AND COALESCE(end_time, now()) >= now()');

  const responseObject = {};
  await Promise.allSettled([
    services.then(({ rows }) => responseObject.serviceRows = rows),
    stats.then(({ rows }) => responseObject.statsRows = rows),
    sensors.then(({ rows }) => responseObject.sensorRows = rows),
    gwNames.then(({ rows }) => responseObject.nameRows = rows)
  ]);
  const { serviceRows, statsRows, sensorRows, nameRows } = responseObject;

  var serviceHash = {};
  var sensorHash = {};
  var statsHash = {};
  var nameHash = {};

  var gatewayHash = {};

  serviceRows.forEach(record => {
    if (!serviceHash[record.gateway_id]) {
      serviceHash[record.gateway_id] = {};
      gatewayHash[record.gateway_id] = true;
    }
    if (!serviceHash[record.gateway_id][record.application]) {
      serviceHash[record.gateway_id][record.application] = [];
    }
    serviceHash[record.gateway_id][record.application].push({
      slot: moment(record.slot).startOf('minute'),
      state: record.process_state
    });
  });

  statsRows.forEach(record => {
    if (!statsHash[record.gateway_id]) {
      statsHash[record.gateway_id] = [];
      gatewayHash[record.gateway_id] = true;
    }
    statsHash[record.gateway_id].push({
      slot: moment(record.slot).startOf('minute'),
      reading_count: Number(record.reading_count),
      beacon_slot_count: Number(record.beacon_slot_count),
      backlog_reading_count: Number(record.backlog_reading_count),
      backlog_beacon_slot_count: Number(record.backlog_beacon_slot_count)
    });
  });

  sensorRows.forEach(record => {
    if (!sensorHash[record.gateway_id]) {
      sensorHash[record.gateway_id] = [{
        slot: moment(record.slot).startOf('minute')
      }];
      gatewayHash[record.gateway_id] = true;
    }
    var list = sensorHash[record.gateway_id];
    var last = list[list.length - 1];
    var slot = moment(record.slot).startOf('minute');
    if (slot.diff(last.slot) != 0) {
      list.push({
        slot: moment(record.slot).startOf('minute')
      });
    }
    if (!list[list.length - 1]['_' + record.processor_role]) {
      list[list.length - 1]['_' + record.processor_role] = 0;
    }
    list[list.length - 1]['_' + record.processor_role] += Number(record.image_count);
  });

  var missing = [];
  nameRows.forEach(record => {
    nameHash[record.gateway_id] = record.subject_name;

    if (!serviceHash[record.gateway_id] && !sensorHash[record.gateway_id] && !statsHash[record.gateway_id]) {
      missing.push({
        gateway_id: record.gateway_id,
        name: record.subject_name,
        zone_name: record.zone_name
      });
    }
  });

  return {
    system: customer, missing: missing, gateways: Object.keys(gatewayHash).map(function (gw) {
      return {
        gateway_id: gw,
        name: nameHash[gw],
        beacons: statsHash[gw],
        images: sensorHash[gw]
      };
    }).concat(missing)
  };
}

function onCacheHit(cache, options) {
  debug(`${options.profileName.split(' ')[0]}:HIT`,cache.keys);
}

function onCacheAdd(cache, options) {
  debug(`${options.profileName.split(' ')[0]}:MISS`,cache.keys);
}

const MAX_AGE = 59 * 1000;
const CACHE_OPTS = {maxAge: MAX_AGE, isPromise: true, maxArgs: 1, onCacheHit, onCacheAdd};

module.exports = Object.freeze({
  deviceStatsForCustomer: moize(deviceStatsForCustomer, CACHE_OPTS),
  statsForCustomerNoConsul: moize(statsForCustomerNoConsul, CACHE_OPTS)
});
