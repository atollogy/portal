'use strict';

const databse = require('../db');
const { pg: sql} = require('yesql');
const DAY_IN_SECONDS = 24 * 60 * 60;
const WEEK_IN_SECONDS = 7 * DAY_IN_SECONDS;

const FETCH_GATEWAYS = () => sql(`
SELECT DISTINCT gateway_id
  FROM image_step
 WHERE collection_time >= :start_time
   AND collection_time <= :end_time
`);

const FETCH_STEP_NAMES = () => sql(`
SELECT DISTINCT step_name
  FROM image_step
 WHERE collection_time >= :start_time
   AND collection_time <= :end_time
`);

const hasOwnProperty = Object.prototype.hasOwnProperty;

const hasParameterWithType = (theSourceObject, theProrpertyName, theExpectedType) => {
  return theSourceObject
    && theProrpertyName && 'string' === typeof theProrpertyName
    && hasOwnProperty.call(theSourceObject, theProrpertyName)
    && theExpectedType && 'string' === typeof theExpectedType
    && theExpectedType === typeof theSourceObject[theProrpertyName];
};

const checkCustomerParameter = (theParameterObject) => {
  if (!hasOwnProperty.call(theParameterObject, 'customer')) {
    throw new Error('customer');
  }

  const { customer } = theParameterObject;
  const oQueryParameters = Object.assign({}, theParameterObject);
  Object.defineProperty(oQueryParameters, 'customer', { value: customer, enumerable: false, writable: false });

  return oQueryParameters;
};

const FETCH_METRICS_INTERVALS = () => sql(`
WITH _records_ AS (
  SELECT
          MIN(collection_time) AS start_time,
          MAX(collection_time) AS end_time
    FROM image_step
    ORDER BY start_time, end_time
) SELECT
        start_time,
        end_time,
        ROUND((end_time - start_time) / 60) AS interval
  FROM _records_
`);

const FETCH_CUSTOMER_METRICS = ({ overview }) => sql(`
WITH _records_ AS (
  SELECT 
          gateway_id,
          step_name,
          date_trunc('${ overview ? 'hour' : 'minute' }', collection_time) collection_time, 
          COUNT(1) AS total_count
    FROM image_step
   WHERE 1 = 1
     AND collection_time >= :start_time
     AND collection_time <= :end_time
     AND step_name = :step
     ${ overview ? '' : 'AND gateway_id = :gateway' }
   GROUP BY gateway_id, step_name, 3
) SELECT
          gateway_id,
          step_name,
          collection_time,
          total_count
FROM _records_
LIMIT 100000
`);

module.exports = Object.freeze({
  /**
   * Fetch all gateways give the provided parameters object.
   *
   * !!IMPORTANT: The parameter object must specify the customer
   */
  fetchGateways: async (theParameterObject) => {
    const oQueryParameters = checkCustomerParameter(theParameterObject);

    try {
      const { customer } = oQueryParameters;

      const current = Math.floor(Date.now() / 1000);
      if (!hasParameterWithType(oQueryParameters, 'start_time', 'number')) {
        oQueryParameters['start_time'] = current - WEEK_IN_SECONDS;
      }
      if (!hasParameterWithType(oQueryParameters, 'end_time', 'number')) {
        oQueryParameters['end_time'] = current;
      }

      const statement = FETCH_GATEWAYS(/* */)(oQueryParameters);
      const { rows: records } = await databse.query(customer, statement.text, statement.values);

      return records.map((element) => element);
    } catch (exception) {
      throw new Error('Unable to fetch gateways', JSON.stringify(oQueryParameters));
    }
  },
  /**
   * Fetch all image step names give the provided parameters object.
   *
   * !!IMPORTANT: The parameter object must specify the customer
   */
  fetchStepNames: async(theParameterObject) => {
    const oQueryParameters = checkCustomerParameter(theParameterObject);

    try {
      const { customer } = oQueryParameters;

      const current = Math.floor(Date.now() / 1000);
      if (!hasParameterWithType(oQueryParameters, 'start_time', 'number')) {
        oQueryParameters['start_time'] = current - WEEK_IN_SECONDS;
      }
      if (!hasParameterWithType(oQueryParameters, 'end_time', 'number')) {
        oQueryParameters['end_time'] = current;
      }

      const statement = FETCH_STEP_NAMES(/* */)(oQueryParameters);
      const { rows: records } = await databse.query(customer, statement.text, statement.values);

      return records.map((element) => element);
    } catch (exception) {
      throw new Error('Unable to fetch step names', JSON.stringify(oQueryParameters));
    }
  },
  /**
   * Fetch all minimum and maximum collection times available for filtering.
   *
   * !!IMPORTANT: The parameter object must specify the customer
   */
  fetchMetricsInterval: async (theParameterObject) => {
    const oQueryParameters = checkCustomerParameter(theParameterObject);

    try {
      const { customer } = oQueryParameters;

      const current = Math.floor(Date.now() / 1000);
      if (!hasParameterWithType(oQueryParameters, 'start_time', 'number')) {
        oQueryParameters['start_time'] = current - WEEK_IN_SECONDS;
      }
      if (!hasParameterWithType(oQueryParameters, 'end_time', 'number')) {
        oQueryParameters['end_time'] = current;
      }

      const statement = FETCH_METRICS_INTERVALS(/* */)(oQueryParameters);
      const { rows: records } = await databse.query(customer, statement.text, statement.values);

      return records.map((element) => element);
    } catch (exception) {
      throw new Error('Unable to fetch metrics intervals for customer', JSON.stringify(oQueryParameters));
    }
  },
  /**
   * Fetch the data acquisition metrics base on the given parameters
   *
   * !!IMPORTANT: The parameter object must specify the customer
   */
  fetchMetrics: async (theParameterObject) => {
    const oQueryParameters = checkCustomerParameter(theParameterObject);

    try {
      const { customer } = oQueryParameters;

      const current = Math.floor(Date.now() / 1000);
      if (!hasParameterWithType(oQueryParameters, 'start_time', 'number')) {
        oQueryParameters['start_time'] = (new Date(1000 * (current - WEEK_IN_SECONDS))).toISOString();
      }
      if (!hasParameterWithType(oQueryParameters, 'end_time', 'number')) {
        oQueryParameters['end_time'] = (new Date(1000 * current)).toISOString();
      }
      if (!hasParameterWithType(oQueryParameters, 'step', 'string')) {
        oQueryParameters['step'] = 'default';
      }

      const overview = !hasParameterWithType(oQueryParameters, 'gateway', 'string');

      const statement = FETCH_CUSTOMER_METRICS({ overview })(oQueryParameters);
      const { rows: records } = await databse.query(customer, statement.text, statement.values);

      return records.map((element) => element);
    } catch (exception) {
      throw new Error('Unable to fetch metrics for customer', JSON.stringify(oQueryParameters));
    }
  }
});