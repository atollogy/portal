const fs = require('fs');
const path = require('path');
const db = require('../db');
const sql = require('yesql').pg;
const moment = require('moment');
const cycleQueryBase = fs.readFileSync(path.join(__dirname, 'query', 'cycleBase.sql'), 'utf8');
const cycleQueryBaseV2 = fs.readFileSync(path.join(__dirname, 'query', 'cycleBase2.sql'), 'utf8');
const contextVisitLog = fs.readFileSync(path.join(__dirname, 'query', 'contextVisitLog.sql'), 'utf8');
const subjectReportQuery = fs.readFileSync(path.join(__dirname, 'query', 'subjectReport.sql'), 'utf8');
const cycleListQuery = fs.readFileSync(path.join(__dirname, 'query', 'cycleList.sql'), 'utf8');
const scaleValuesQuery = fs.readFileSync(path.join(__dirname, 'query', 'scaleValues.sql'), 'utf8');
const OCRValuesQuery = fs.readFileSync(path.join(__dirname, 'query', 'OCRValues.sql'), 'utf8');
const scaleValuesQuerySql = sql(scaleValuesQuery + ' SELECT * FROM weight ORDER BY collection_time');
const OCRValuesQuerySql = sql(OCRValuesQuery);
const shiftDetailsQuery = fs.readFileSync(path.join(__dirname, 'query', 'shiftDetails.sql'), 'utf8');
const shiftDetailsQuerySql = sql(shiftDetailsQuery);
const { Cache } = require('../lib/cache');
const config = require('../db/reportConfig.js');
const cache30secs = new Cache(30);
const cache5mins = new Cache(5 * 60);
const { filterByBlacklistRules } = require('../lib/utils/filter');

const newCacheConfig = function(config) {

  const theCustomerSettingsCache = { };
  const CACHE_EXPIRATION = 5 * 60;

  const getCustomerConfigs = async ({ customer, facility }) => {
    const theCacheConfigKey = `${customer}:${facility}`.toLowerCase();
    const theCacheConfigEntry = theCustomerSettingsCache[theCacheConfigKey] || { expires: 0 };

    const theCurrentTimestamp = (Date.now() / 1000) >>> 0;
    const { expires: theCacheConfigExpiration } = theCacheConfigEntry;
    if ((theCurrentTimestamp - theCacheConfigExpiration) > CACHE_EXPIRATION) {
      const { settings } = await config.getCustomerConfigs({ customer, facility });
      const expires = theCurrentTimestamp + CACHE_EXPIRATION;
      Object.assign(theCacheConfigEntry, { settings, expires, customer, facility });
      Object.assign(theCustomerSettingsCache, { [theCacheConfigKey]: theCacheConfigEntry });
    }

    return theCacheConfigEntry;
  };

  return Object.freeze({ getCustomerConfigs });
};

const theCacheConfig = newCacheConfig(config);

const DefaultStartEvent = Object.freeze({ is_start: true, is_end: false, priority: 0 });
const DefaultFinishEvent = Object.freeze({ is_start: false, is_end: true, priority: 9000 });

const MAX_ENTRY_INCLUSION_INTERVAL = 20; /* Defautl period between entries for them to be consider the same iteration */
const MAX_EXIT_INCLUSION_INTERVAL = 20; /* Defautl period between extis for them to be consider the same iteration */
const MAX_CYCLE_DURACTION = 96 * 60; /* Default max cycle duration in minutes */
const hasOwnProperty = (target, property) => Object.prototype.hasOwnProperty.call(target, property);

const EmptyIteration = Object.freeze({ subject: undefined, cycle: Number.MAX_VALUE });

const makeSequence = () => {
  const state = { value: 1, step: 1 };

  return Object.freeze({
    next: () => {
      state.value += state.step;
      return state.value;
    }
  });
};

const newCycleObject = (event, { shiftStart, shiftEnd, legendArr }) => {
  let iterations = [{
    subject: event.subject_id,
    cycle: event.cycle_id,
    customerTags: event.tags || [],
    states: [],
    attributes: event.attributes,
    lastImgKeyForAsset: []
  }];

  return {
    facility: event.facility,
    iterations: iterations,
    shiftStart: shiftStart,
    shiftEnd: shiftEnd,
    legends: legendArr
  };
};

const { getCustomerFilterForSite } = require('./customerLPFilters');

const cycleStatesQueryWithWeight = `SELECT * FROM cycle_weights_with_other_imgs `;

const legendQuery = `
  SELECT state_name, start_cycle, end_cycle, priority, 'event' as source
    FROM customer_transition_event_binding
   WHERE facility = :facility
    AND (end_time IS NULL OR :date <= end_time)
    AND (start_time IS NULL OR :date >= start_time)
  UNION
  SELECT state_name, start_cycle, end_cycle, priority, 'beacon' as source
    FROM customer_beacon_transition_event_binding
   WHERE facility = :facility
   ORDER BY state_name;
`;
const legendQuerySql = sql(legendQuery);

const cycleStatesQueryWithWeightSql = sql(cycleQueryBase + cycleStatesQueryWithWeight);
const cycleStatesQueryWithWeightSqlV2 = sql(cycleQueryBaseV2 + cycleStatesQueryWithWeight);

const getClosestTimeIndex = (events, lpCollectionTime) => {
  let left = 0;
  let right = events.length - 1;
  const threshold = 1000;
  while (left < right) {
    let midpoint = Math.floor((left + right) / 2);

    const assetCollectionTime = moment(events[midpoint].collection_time);

    const delta = assetCollectionTime.diff(lpCollectionTime);

    if (threshold >= Math.abs(delta)) {
      return midpoint;
    } else if (assetCollectionTime > lpCollectionTime) {
      right = midpoint - 1;
    } else {
      left = midpoint + 1;
    }
  }
  return left == -1 ? 0 : left;
};

const getMaxWeightIndex = (events, startIndex, endIndex) => {
  let maxWeight = events[startIndex].state;
  let maxWeightIndex = startIndex;
  for (let i=startIndex+1; i <= endIndex; ++i) {
    if (events[i].state > maxWeight) {
      maxWeightIndex = i;
      maxWeight = events[i].state;
    }
  }
  return maxWeightIndex;
};

async function fetchData(customer, query, parameters, name, cache = cache5mins) {
  let data = await cache.get(customer, {...parameters, ...name});
  let skipFilter = parameters.skipFilter ? parameters.skipFilter : false;
  //First collect lookback_slots param
  const bindings = (await db.query(customer, `SELECT DISTINCT rssi_smoothing_window_slots lookback_slots FROM customer_binding`, [])).rows;
  try {
    const facility = parameters.facility;
    const { settings } = await theCacheConfig.getCustomerConfigs({ customer, facility });
    parameters.asset_look_back_duration_in_second = settings.length && settings[0].settings &&
    Object.prototype.hasOwnProperty.call(settings[0].settings, 'asset_look_back_duration_in_second') ?
      settings[0].settings.asset_look_back_duration_in_second + ' seconds' : '0 seconds';
    parameters.asset_look_ahead_duration_in_second = settings.length && settings[0].settings &&
    Object.prototype.hasOwnProperty.call(settings[0].settings,'asset_look_ahead_duration_in_second') ?
      settings[0].settings.asset_look_ahead_duration_in_second + ' seconds': '60 seconds';
  } catch (ignored) {
    /* exception ignored on purpose, we proceed even if we fail to obtain the configuration */
  }
  //Supply this param to query to apply lookback_slots for beacon moving avg. window
  parameters.lookback_slots = bindings && bindings[0] && bindings[0].lookback_slots;
  if (!data || !data.length) {
    let statement = query(parameters);
    const { rows } = await db.query(customer, statement.text, statement.values, true);
    data = rows;
  }

  const site = parameters.facility.toLocaleUpperCase();
  if(!skipFilter) {
    data = data.filter(row => {
      return getCustomerFilterForSite(customer, site, row);
    });
  }
  await cache.put(customer, {...parameters, ...name}, data);

  //Convert time strings to Datetime objects in-case reading from cache messed em up
  data = data.map(d => ({
    ...d,
    start_time: new Date(d.start_time),
    end_time: new Date(d.end_time)
  }));

  //Re-sort by facility + subject + start_time in case reading from messed up the order of rows
  data.sort((a, b) => {
    return a.facility.localeCompare(b.facility)
      || (b.subject_id && a.subject_id.localeCompare(b.subject_id))
      || (moment((b.last_state_start_time || a.start_time)).valueOf() - (moment((a.last_state_start_time || b.start_time)).valueOf()));
  });

  return data;
}

module.exports.isEntryExitInclusionEnabled = async ({ customer, facility }) => {
  let isEntryExitInclusionEnabled = false;

  try {
    const { settings } = await theCacheConfig.getCustomerConfigs({ customer, facility });
    isEntryExitInclusionEnabled = settings.length && settings[0].settings ? settings[0].settings.isEntryExitInclusionEnabled : false;
  } catch (ignored) {
    /* exception ignored on purpose, we proceed even if we fail to obtain the configuration */
  }

  return isEntryExitInclusionEnabled;
};

module.exports.isExitRequired = async ({ customer, facility }) => {
  let isExitRequired = false;

  try {
    const { settings } = await theCacheConfig.getCustomerConfigs({ customer, facility });
    isExitRequired = settings.length && settings[0].settings ? settings[0].settings.isExitRequired : false;
  } catch (ignored) {
    /* exception ignored on purpose, we proceed even if we fail to obtain the configuration */
  }

  return isExitRequired;
};

module.exports.isEntryRequired = async ({ customer, facility }) => {
  let isEntryRequired = false;

  try {
    const { settings } = await theCacheConfig.getCustomerConfigs({ customer, facility });
    isEntryRequired = settings.length && settings[0].settings ? settings[0].settings.isEntryRequired : false;
  } catch (ignored) {
    /* exception ignored on purpose, we proceed even if we fail to obtain the configuration */
  }

  return isEntryRequired;
};

module.exports.maxInterExitInterval = async ({ customer, facility }) => {
  let theExitInclusionThreshold = undefined;

  try {
    const { settings } = await theCacheConfig.getCustomerConfigs({ customer, facility });
    theExitInclusionThreshold = settings.length && settings[0].settings ? settings[0].settings.exitInclusionInterval  : undefined;
  } catch (ignored) {
    /* exception ignored on purpose, we proceed even if we fail to obtain the configuration */
  }
  return theExitInclusionThreshold || MAX_EXIT_INCLUSION_INTERVAL;
};

module.exports.maxInterEntryInterval = async ({ customer, facility }) => {
  let theEntryInclusionThreshold = undefined;

  try {
    const { settings } = await theCacheConfig.getCustomerConfigs({ customer, facility });
    theEntryInclusionThreshold = settings.length && settings[0].settings ? settings[0].settings.entryInclusionInterval : undefined;
  } catch (ignored) {
    /* exception ignored on purpose, we proceed even if we fail to obtain the configuration */
  }

  return theEntryInclusionThreshold || MAX_ENTRY_INCLUSION_INTERVAL;
};

module.exports.maxCycleDuration = async ({ customer, facility }) => {
  let maxCycleDuration = undefined;

  try {
    const { settings } = await theCacheConfig.getCustomerConfigs({ customer, facility });
    maxCycleDuration = settings.length && settings[0].settings ? settings[0].settings.maxCycleDuration : undefined;
  } catch (ignored) {
    /* exception ignored on purpose, we proceed even if we fail to obtain the configuration */
  }

  return maxCycleDuration || MAX_CYCLE_DURACTION;
};

module.exports.getOCRRegEx = async ({ customer, facility }) => {
  let OCRRegex = undefined;
  try {
    const { settings } = await theCacheConfig.getCustomerConfigs({ customer, facility });
    OCRRegex = settings.length && settings[0].settings && settings[0].settings.OCRRegex ? settings[0].settings.OCRRegex : '.*';
  } catch (ignored) {
    /* exception ignored on purpose, we proceed even if we fail to obtain the configuration */
  }
  return OCRRegex;
};

module.exports.findWeightImagesForState = async function findWeightImagesForState({
  state: stateOfInterest,
  events: weightEvents,
  customer,
  parameters
}, reportCfg) {
  let weightCfg = {};
  reportCfg.forEach (cfg => {
    if (cfg.cs_config.asset_look_ahead_duration_in_second || cfg.cs_config.asset_look_back_duration_in_second) {
      weightCfg[cfg.subject_name] = {};
      weightCfg[cfg.subject_name]['subject_name'] = cfg.subject_name;
      weightCfg[cfg.subject_name]['asset_look_ahead_duration_in_second'] = cfg.cs_config.asset_look_ahead_duration_in_second;
      weightCfg[cfg.subject_name]['asset_look_back_duration_in_second'] = cfg.cs_config.asset_look_back_duration_in_second;
    }
  });

  const listOfImages = [];
  let subject_name = stateOfInterest.state;
  let stateStartTime = moment(stateOfInterest.startTime);
  if (weightCfg[subject_name]) {

    let lookBackTime = weightCfg[subject_name].asset_look_back_duration_in_second ?
      stateStartTime.subtract(weightCfg[subject_name].asset_look_back_duration_in_second, 'seconds')
      : stateStartTime;

    stateStartTime = moment(stateOfInterest.startTime);
    let lookAheadTime = weightCfg[subject_name].asset_look_ahead_duration_in_second ?
      stateStartTime.add(weightCfg[subject_name].asset_look_ahead_duration_in_second, 'seconds')
      : stateStartTime;

    let startIndex = getClosestTimeIndex(weightEvents, lookBackTime);
    let endIndex = getClosestTimeIndex(weightEvents, lookAheadTime);

    let index = getMaxWeightIndex(weightEvents, startIndex, endIndex);
    stateOfInterest.weight = weightEvents[index].state;

    if (weightEvents[index].image_info) {
      let arrayOfImageKeys = weightEvents[index].image_info.slice(1,-1).split(',');
      if (arrayOfImageKeys.length == 5) {
        const [gateway_id, camera_id, step_name, collection_time, name] = arrayOfImageKeys;
        let theImageObject = {
          collection_time: moment(collection_time.slice(1,-1)),
          gateway_id,
          camera_id,
          step_name,
          name
        };
        listOfImages.push(theImageObject);
      }
    }
  }

  return listOfImages;
};

module.exports.findOCRImagesForState = function findOCRImagesForState({
  state: stateOfInterest,
  events: OCREvents,
  customer,
  parameters
}, OCRCfg) {

  let filteredEvents = OCREvents.filter(o => OCRCfg[stateOfInterest.state]['ocr_gateways'].includes(o.gateway_id));

  const listOfImages = [];
  let subject_name = stateOfInterest.state;
  let stateStartTime = moment(stateOfInterest.startTime);

  if (OCRCfg[subject_name]) {

    let lookBackTime = OCRCfg[subject_name].ocr_look_back_duration_in_second ?
      stateStartTime.subtract(OCRCfg[subject_name].ocr_look_back_duration_in_second, 'seconds')
      : stateStartTime;

    stateStartTime = moment(stateOfInterest.startTime);
    let lookAheadTime = OCRCfg[subject_name].ocr_look_ahead_duration_in_second ?
      stateStartTime.add(OCRCfg[subject_name].ocr_look_ahead_duration_in_second, 'seconds')
      : stateStartTime;

    let startIndex = getClosestTimeIndex(filteredEvents, lookBackTime);
    let endIndex = getClosestTimeIndex(filteredEvents, lookAheadTime);

    for (let index= startIndex; index <= endIndex; ++index) {
      if (filteredEvents[index].image_info) {
        let arrayOfImageKeys = filteredEvents[index].image_info.slice(1,-1).split(',');
        if (arrayOfImageKeys.length === 5) {
          const [gateway_id, camera_id, step_name, collection_time, name] = arrayOfImageKeys;
          let timeString = collection_time.slice(1,-1);
          timeString = timeString.split(' ')[0] + 'T' +  timeString.split(' ')[1];
          let theImageObject = {
            collection_time: timeString,
            gateway_id,
            camera_id,
            step_name,
            name,
            value: filteredEvents[index].state
          };
          listOfImages.push(theImageObject);
        }
      }
    }
  }
  return listOfImages;
};



module.exports.cycleEventsWithAttributes = async function (customer, cycleParams) {
  let cycleSql = null;
  const threshold = await module.exports.maxCycleDuration({ customer, facility: cycleParams.facility });

  if (cycleParams.version && cycleParams.version === '2') {
    cycleParams.max_cycle_duration = threshold;
    cycleSql = cycleStatesQueryWithWeightSqlV2;
  } else {
    cycleSql = cycleStatesQueryWithWeightSql;
  }
  const rows = await fetchData(customer, cycleSql, cycleParams, {serviceName: 'cycleEventsWithAttributes'});

  const mapOfCycles = rows.filter(
    /** Blacklist LPN - Disabled by config */
    row => filterByBlacklistRules(row.subject_id)
  ).reduce((mapOfSubjects, record) => {
    const {facility, subject_id} = record;

    const key = `${facility}:${subject_id}`;
    const elements = mapOfSubjects[key] || [];

    const priority = Number(record['priority'] || 0);
    Object.assign(record, { priority });

    elements.push(record);
    mapOfSubjects[key] = elements;
    return mapOfSubjects;
  }, {});

  const theEntryInclusionThreshold = await module.exports.maxInterEntryInterval({ customer, facility: cycleParams.facility });
  const theExitInclusionThreshold = await module.exports.maxInterExitInterval({ customer, facility: cycleParams.facility });
  const isEntryExitInclusionEnabled = await module.exports.isEntryExitInclusionEnabled({ customer, facility: cycleParams.facility });
  const theCycleBoundaries = { lower: 0xFFFF, upper: 0x0000 };

  const listOfCycles = Object.keys(mapOfCycles).reduce((elements, identity) => {
    const startOfSubject = 1 + identity.indexOf(':');
    const subject = identity.substring(startOfSubject);
    const listOfEvents = mapOfCycles[identity];

    if (listOfEvents && listOfEvents.length) {
      const context = { size: 0, subject };
      const countOfEvents = listOfEvents.length;

      let lastStartTime = moment(listOfEvents[0].start_time);

      for (let index = 0; index < countOfEvents; index += 1) {
        const theOriginalEvent = listOfEvents[index];
        const { start_time, priority } = theOriginalEvent;
        Object.assign(theCycleBoundaries, {
          lower: Math.min(theCycleBoundaries.lower, priority),
          upper: Math.max(theCycleBoundaries.upper, priority)
        });

        const currentTime = moment(start_time);
        const duration = currentTime.diff(lastStartTime, 'minutes');

        const theUpdatedEvent = Object.assign({}, theOriginalEvent, { duration, priority });

        const thePreviousEvent = index > 0 ? listOfEvents[index - 1] : Object.assign({}, DefaultStartEvent);
        const { is_end: isEndOfCycle, is_start: isStartOfCycle, priority: previous } = thePreviousEvent;
        const markEndOfCycle = isEndOfCycle || (!isStartOfCycle && index > 0 && previous > priority);
        Object.assign(thePreviousEvent, { is_end: markEndOfCycle });

        listOfEvents[index] = theUpdatedEvent;
        lastStartTime = currentTime;
      }

      for (let index = 0; index < countOfEvents; index += 1) {
        const element = listOfEvents[index];

        let { size: countOfElements } = context;
        const startTime = moment(element.start_time);
        const { is_end: isEndingPoint, is_start: isStartingPoint, priority: theCurrentPriority } = element;
        const isTrueStartOfCycle = isEntryExitInclusionEnabled && theCurrentPriority === theCycleBoundaries.lower;
        const isTrueEndOfCycle = isEntryExitInclusionEnabled && theCurrentPriority === theCycleBoundaries.upper;

        let theSelectedEvents = context[countOfElements] || (context[countOfElements] = []);
        const hasPrecedingEvents = theSelectedEvents.length >= 1;
        const hasFollowingEvents = index + 1 < countOfEvents;

        if (isStartingPoint) {
          const {
            is_start: isPrecededByEntry,
            is_end: isPrecededByExit,
            priority: thePrecedingPriority,
            start_time: thePrecedingStartTime
          } = hasPrecedingEvents ? listOfEvents[index - 1] : DefaultFinishEvent;

          const isSameEntryPriority = theCurrentPriority === thePrecedingPriority;
          let theProposedPriority = theCurrentPriority;
          if (isTrueStartOfCycle && isPrecededByEntry && isSameEntryPriority) {
            const startOfCycle = theSelectedEvents.length ? moment(thePrecedingStartTime) : startTime;
            const isOverThreshold = startTime.diff(startOfCycle, 'minutes') > theEntryInclusionThreshold;
            const isOutsideBounds = 85 >= Math.round(100 * (Math.min(startTime.diff(startOfCycle, 'minutes'), theEntryInclusionThreshold) / Math.max(startTime.diff(startOfCycle, 'minutes'), theEntryInclusionThreshold)));
            theProposedPriority += isOverThreshold && isOutsideBounds ? (thePrecedingPriority - 1) : theCurrentPriority;
          }

          countOfElements += isPrecededByEntry ? Math.max(0, Math.sign(thePrecedingPriority - theProposedPriority)) : (isPrecededByExit ? 1 : 0);
        }

        const startOfCycle = theSelectedEvents.length ? moment(theSelectedEvents[0].start_time) : startTime;
        const isOverThreshold = startTime.diff(startOfCycle, 'minutes') > threshold;
        // const isOutsideBounds = 85 >= Math.round(100 * (Math.min(startTime.diff(startOfCycle, 'minutes'), threshold) / Math.max(startTime.diff(startOfCycle, 'minutes'), threshold)));
        countOfElements += isOverThreshold ? 1 : 0;

        theSelectedEvents = context[countOfElements] || (context[countOfElements] = []);
        theSelectedEvents.push(element);

        if (isEndingPoint) {
          const {
            is_start: isFollowedByEntry,
            is_end: isFollowedByExit,
            priority: theFollowingPriority,
            startTime: theFollowingStartTime
          } = hasFollowingEvents ? listOfEvents[index + 1] : DefaultStartEvent;

          const isSameExitPriority = theCurrentPriority === theFollowingPriority;
          let theProposedPriority = theCurrentPriority;
          if (isTrueEndOfCycle && isFollowedByExit && isSameExitPriority) {
            const startOfCycle = theSelectedEvents.length ? moment(theFollowingStartTime) : startTime;
            const isOverThreshold = startTime.diff(startOfCycle, 'minutes') > theExitInclusionThreshold;
            const isOutsideBounds = 85 >= Math.round(100 * (Math.min(startTime.diff(startOfCycle, 'minutes'), theExitInclusionThreshold) / Math.max(startTime.diff(startOfCycle, 'minutes'), theExitInclusionThreshold)));
            theProposedPriority += isOverThreshold && isOutsideBounds ? (theFollowingPriority + 1) : theCurrentPriority;
          }

          const isStopCycleEvent = Math.sign(theProposedPriority - theFollowingPriority) >= 0;
          countOfElements += isFollowedByExit ? (isStopCycleEvent ? 1 : 0) : (isFollowedByEntry ? 1 : 0) ;
        }

        Object.assign(context, { size: countOfElements });
      }

      const partials = [];
      const { size } = context;
      for (let index = 0; index <= size; index += 1) {
        if (hasOwnProperty(context, index) && context[index] && context[index].length) {
          partials.push(context[index]);
        }
      }

      const cycleSummary = partials.reduce((contextObject, setOfElements, index) => {
        setOfElements.forEach((element) => element.cycle_id = 1 + index);
        Object.assign(contextObject, { [index]: setOfElements });
        return contextObject;
      }, { size: partials.length });

      elements.push( cycleSummary );
    }

    return elements;
  }, []);

  const groupOfEvents = [];
  const IdentitySequence = makeSequence();
  listOfCycles.forEach((cycleSummary) => {
    const { size: length } = cycleSummary;
    for (let index = 0; index <= length; index += 1) {
      if (hasOwnProperty(cycleSummary, index) && cycleSummary[index] && cycleSummary[index].length) {
        const identity = IdentitySequence.next();
        cycleSummary[index].forEach((element) => {
          element.identity = identity;
          groupOfEvents.push(element);
        });
      }
    }
  });

  const shiftQuery = shiftDetailsQuerySql(cycleParams);
  let {rows: shiftRows} = await db.query(customer, shiftQuery.text, shiftQuery.values);

  let legends = legendQuerySql(cycleParams);
  let {rows: tempLegends} = await db.query(customer, legends.text, legends.values);
  let legendArr = tempLegends.map(x => x.state_name);
  let shiftStart = null;
  let shiftEnd = null;

  if (shiftRows.length) {
    shiftStart = shiftRows[0].start_time;
    shiftEnd = shiftRows[0].end_time;
  }

  const cycles = {};

  groupOfEvents.forEach(row => {
    const currentCycle = cycles[row.facility] || ( cycles[row.facility] = newCycleObject(row, { shiftStart, shiftEnd, legendArr }) );

    let lastIndex = currentCycle.iterations.length - 1;
    let lastIteration = currentCycle.iterations[lastIndex] || EmptyIteration;
    if (lastIteration.subject != row.subject_id || lastIteration.cycle != row.cycle_id) {
      lastIteration = {
        cycle: row.cycle_id,
        subject: row.subject_id,
        attributes: row.attributes,
        customerTags: row.tags || [],
        states: [],
        lastImgKeyForAsset: []
      };
      currentCycle.iterations.push(lastIteration);
    }

    var stateInfo = {
      subject: row.subject_id,
      state: row.state,
      attributes: row.attributes,
      startTime: row.start_time,
      endTime: row.end_time,
      img_keys: row.img_keys,
      warning: row.warning,
      customerTags: row.tags || [],
      is_start: row.is_start,
      is_end: row.is_end,
      weight: 'weight' in row ? row.weight : undefined
    };

    lastIteration.lastImgKeyForAsset = lastIteration.lastImgKeyForAsset.concat(row.img_keys.lpr);
    lastIteration.states.push(stateInfo);
  });

  Object.values(cycles).forEach(cycle => {
    cycle.iterations.sort((a, b) => a.states[0].startTime - b.states[0].startTime);
  });

  const params = {
    previous: false,
    date: cycleParams.date,
    facility: cycleParams.facility,
    shift: cycleParams.shift,
    from: null,
    to: null
  };

  const reportCfg = await config.getConfig(customer, params);

  const weightsQuery = scaleValuesQuerySql(params);
  let {rows: weightEvents} = await db.query(customer, weightsQuery.text, weightsQuery.values);

  if (weightEvents.length &&
    Object.prototype.hasOwnProperty.call(cycles, cycleParams.facility) &&
    Object.prototype.hasOwnProperty.call(cycles[cycleParams.facility], 'iterations')
  ) {
    for (let index = 0; index < cycles[cycleParams.facility].iterations.length; ++index) {
      const iterationOfInterest = cycles[cycleParams.facility].iterations[index];
      for (let stateIndex = 0; stateIndex < iterationOfInterest.states.length; ++stateIndex) {
        const stateOfInterest =  iterationOfInterest.states[stateIndex];
        const scaleImages = module.exports.findWeightImagesForState({
          customer,
          parameters : params,
          events: weightEvents,
          state: stateOfInterest
        }, reportCfg);
        if (scaleImages.length > 0) {
          cycles[cycleParams.facility].iterations[index].states[stateIndex].img_keys['weight'].push(scaleImages);
        }
      }
    }
  }

  const OCRRegex = await module.exports.getOCRRegEx({ customer, facility: cycleParams.facility });

  params.reg_ex = OCRRegex;
  const OCRQuery = OCRValuesQuerySql(params);
  let {rows: OCREvents} = await db.query(customer, OCRQuery.text, OCRQuery.values);

  let OCRCfg = {};
  reportCfg.forEach (cfg => {
    if (cfg.cs_config.ocr) {
      OCRCfg[cfg.subject_name] = {};
      OCRCfg[cfg.subject_name]['ocr_gateways'] = cfg.cs_config.ocr.ocr_gateways;
      OCRCfg[cfg.subject_name]['ocr_look_ahead_duration_in_second'] =
      cfg.cs_config.ocr.ocr_look_ahead_duration_in_second || 60;
      OCRCfg[cfg.subject_name]['ocr_look_back_duration_in_second'] =
      cfg.cs_config.ocr.ocr_look_back_duration_in_second || 60;
    }
  });

  if (OCREvents.length &&
    Object.prototype.hasOwnProperty.call(cycles, cycleParams.facility) &&
    Object.prototype.hasOwnProperty.call(cycles[cycleParams.facility], 'iterations')
  ) {
    for (let index = 0; index < cycles[cycleParams.facility].iterations.length; ++index) {
      const iterationOfInterest = cycles[cycleParams.facility].iterations[index];
      for (let stateIndex = 0; stateIndex < iterationOfInterest.states.length; ++stateIndex) {
        const stateOfInterest = iterationOfInterest.states[stateIndex];
        if (OCRCfg[stateOfInterest.state] && 'ocr_gateways' in OCRCfg[stateOfInterest.state]) {
          const OCRImages = module.exports.findOCRImagesForState({
            customer,
            parameters : params,
            events: OCREvents,
            state: stateOfInterest
          }, OCRCfg);
          if (OCRImages.length > 0) {
            cycles[cycleParams.facility].iterations[index].states[stateIndex].img_keys['ocr'] = OCRImages;
            cycles[cycleParams.facility].iterations[index].states[stateIndex].ocr_value = OCRImages[0].value;

          }
        }
      }
    }
  }
  let facility = cycleParams.facility;
  const { settings } = await theCacheConfig.getCustomerConfigs({ customer, facility });
  if(settings[0].settings &&
    Object.prototype.hasOwnProperty.call(settings[0].settings, 'mergeIntermediateSingleStates')) {
    for (let index = 0; index < cycles[cycleParams.facility].iterations.length; ++index) {
      const iterations = cycles[cycleParams.facility].iterations[index];
      let prevState = iterations.states[0];
      let currentState = null;
      let nextState = null;
      if (cycles[cycleParams.facility].iterations.length > 2) {
        for (let stateIndex = 1; stateIndex < iterations.states.length - 1;) {
          currentState = iterations.states[stateIndex];
          if (stateIndex+1 > iterations.states.length){
            break;
          }
          nextState = iterations.states[stateIndex + 1];
          if (prevState.state === currentState.state
              && currentState.state === nextState.state
              && prevState.subject === currentState.subject
              && currentState.subject === nextState.subject) {
            iterations.states.splice(stateIndex, 1);
          } else {
            prevState = currentState;
            ++stateIndex;
          }
        }
      }
    }
  }

  return Object.values(cycles);
};

let cycleZonesQuery = `
WITH beacon_gateway_info AS
(
  SELECT t.state_name, t.start_cycle, t.end_cycle, t.priority, b.subject_name, b.beacon_id gateway_id, 'beacon'::text as source
    FROM customer_beacon_transition_event_binding t
  JOIN customer_binding b ON t.zone_name = b.zone_name AND t.facility = b.facility AND (end_time IS NULL OR :to <= end_time)
)
SELECT state_name, start_cycle, end_cycle, priority, null subject_name, gateway_id, 'event'::text as source
  FROM customer_transition_event_binding
 WHERE facility = :facility AND (end_time IS NULL OR :to <= end_time)
  UNION
SELECT * FROM beacon_gateway_info
  ORDER BY state_name;
`;

let subjectZonesQuery = `
  SELECT
    DISTINCT CASE WHEN :byClass THEN measure_name
                  ELSE subject_name
             END state_name,
    FALSE start_cycle,
    FALSE end_cycle,
    0 priority
  FROM customer_report_config
  WHERE facility = :facility AND (end_time IS NULL OR :to <= end_time)
  ORDER BY state_name;
`;

let queries = {
  cycleLogQuery: cycleQueryBase + `SELECT * FROM cycle_log`,
  subjectCycleLog: subjectReportQuery + `SELECT * FROM subjects_cycle_log`,
  cycleZonesQuery: cycleZonesQuery,
  subjectZonesQuery: subjectZonesQuery
};

module.exports.cycleLog = async function (customer, cycleLogParams) {
  let rows = await fetchData(customer, sql(queries[cycleLogParams.dataQueryName]), cycleLogParams, {serviceName: 'cycleLog'});

  const site = cycleLogParams.facility.toLocaleUpperCase();
  rows = rows.filter(row => {
    return getCustomerFilterForSite(customer, site, row);
  });

  //Get All possible Zones in an array and sort them by order in the cycle (start -> intermediate -> end)
  let zoneQ = sql(queries[cycleLogParams.zonesQueryName])(cycleLogParams);
  let {rows: zoneRows} = await db.query(customer, zoneQ.text, zoneQ.values);
  let endState = {}, startState = {}, zoneObj = {};
  zoneRows = zoneRows.filter(x => x.state_name !== 'composite');
  zoneRows.forEach((zoneRecord) => {
    if (zoneRecord.start_cycle) {
      if (!startState[zoneRecord.state_name]) {
        startState[zoneRecord.state_name] = [];
      }
      startState[zoneRecord.state_name].push(zoneRecord.gateway_id);
    } else if (zoneRecord.end_cycle) {
      if (!endState[zoneRecord.state_name]) {
        endState[zoneRecord.state_name] = [];
      }
      endState[zoneRecord.state_name].push(zoneRecord.gateway_id);
    } else {
      if (!zoneObj[zoneRecord.state_name]) {
        zoneObj[zoneRecord.state_name] = [];
      }
      zoneObj[zoneRecord.state_name].push(zoneRecord.gateway_id);
    }
  });
  let listOfZones = Object.keys(startState).concat(Object.keys(zoneObj)).concat(Object.keys(endState).reverse());
  zoneObj = Object.assign({}, zoneObj, startState, endState);
  return {
    data: rows,
    allZones: listOfZones,
    gatewayZoneMap: zoneObj
  };
};

module.exports.activeCycleList = async function (customer) {
  const cycleList = cycleListQuery + ' SELECT * FROM cycle_list ORDER BY date desc';
  var {rows} = await db.query(customer, cycleList, []);
  var cycles = {};

  rows.forEach(row => {
    var cycleKey = `${row.facility}}`;
    if (!cycles[cycleKey]) {
      cycles[cycleKey] = {facility: row.facility, shifts: []};
    }
    if (cycles[cycleKey].shifts.length == 0 ||
      cycles[cycleKey].shifts[cycles[cycleKey].shifts.length - 1].date != row.date ||
      cycles[cycleKey].shifts[cycles[cycleKey].shifts.length - 1].shift != row.shift_name) {
      cycles[cycleKey].shifts.push({
        date: moment(row.date).format('YYYY-MM-DD'),
        shift: row.shift_name
      });
    }

  });

  Object.keys(cycles).forEach(cycle => {
    const d = cycles[cycle];
    let out;

    const site = d.facility.toLocaleUpperCase();
    out = Object.assign(d, {
      subjects: d.subjects.filter(row => getCustomerFilterForSite(customer, site, row))
    });

    cycles[cycle] = out;
  });

  return Object.values(cycles);
};

module.exports.knownIdentifiers = async function (customer, visitLogParams) {
  const cycleList = contextVisitLog;
  const rows = await fetchData(customer, sql(cycleList), visitLogParams, {serviceName: 'knownIdentifiers'}, cache30secs);

  var cycles = {};
  rows.forEach((row, idx) => {
    var cycleKey = row.facility;
    if (!cycles[cycleKey]) {
      cycles[cycleKey] = {
        facility: row.facility,
        subjects: []
      };
    }

    if (cycles[cycleKey].subjects.length == 0 ||
      cycles[cycleKey].subjects[cycles[cycleKey].subjects.length - 1].subjectId != row.subject_id
    // || cycles[cycleKey].subjects[cycles[cycleKey].subjects.length - 1].cycle != row.cycle_id
    ) {
      cycles[cycleKey].subjects.push({
        subjectId: row.subject_id || `UnknownLP:${idx}`,
        customerTags: row.tags || [],
        states: [],
        visited: 0,
        lprLink: row.lpr_link || null,
        attributes: row.attributes
      });
    }

    var stateInfo = {
      subjectId: row.subject_id || `UnknownLP:${idx}`,
      lastSeen: row.last_state_start_time,
      lastState: row.last_state,
      isEnd: row.is_end,
      visited: row.visit_count,
      attributes: row.attributes,
      customerTags: row.tags || []
    };
    stateInfo.img_keys = {
      'lpr': [row.img_keys]
    };
    cycles[cycleKey].subjects[cycles[cycleKey].subjects.length - 1].states.push(stateInfo);
  });

  Object.keys(cycles).forEach(cycle => {
    const d = cycles[cycle];
    let out;

    const site = d.facility.toLocaleUpperCase();
    if (!visitLogParams.skipFilter) {
      out = Object.assign(d, {
        subjects: d.subjects.filter(row => getCustomerFilterForSite(customer, site, row))
      });
    } else {
      out = Object.assign(d, {
        subjects: d.subjects
      });
    }
    cycles[cycle] = out;
  });

  return Object.values(cycles);
};

module.exports.shiftDetails = async function (customer, parameters) {
  const shiftQuery = shiftDetailsQuerySql(parameters);
  const {rows} = await db.query(customer, shiftQuery.text, shiftQuery.values);
  return rows;
};

module.exports.shiftNames = async (customer, facility) => {
  const query = sql(`SELECT DISTINCT shift_name FROM customer_shift WHERE facility = :facility ORDER BY 1`);
  const statement = query({ facility });
  const { rows: records } = await db.query(customer, statement.text, statement.values);
  return records.map(({shift_name}) => shift_name);
};

module.exports.latestBindingStatus = async function (customer) {
  const q = `
  WITH summarized_asset_info AS (
    SELECT
      asset_id,
      min(latest_seen_time) latest_seen_time,
      json_object_agg(qualifier,
                      json_build_object('attr_value', attribute_value,
                                        'last_seen', latest_seen_time)
      )::jsonb attributes
    FROM asset_attribute
    GROUP BY asset_id
  )

  SELECT
    asset_id wo_id,
    latest_seen_time,
    CASE WHEN b.subject_name IS NOT NULL THEN 'SUCCESS' ELSE 'FAIL' END status
  FROM summarized_asset_info s
  LEFT OUTER JOIN customer_binding b ON s.attributes->'beacon_id'->>'attr_value' = b.subject_name AND b.subject = 'beacon'
  ORDER BY latest_seen_time DESC
  LIMIT 3;
`;
  let {rows} = await db.query(customer, q, []);
  return rows;
};
