const debug = require('debug')('portal:db:imageStep');
const moize = require('../cache').default;
const fs = require('fs');
const path = require('path');
const db = require('../db/');
const moment = require('moment');
// const consul = require ('../consul.js');
const sql = require('yesql').pg;
const lastImageQuery = fs.readFileSync(path.join(__dirname, 'query', 'lastImage.sql'), 'utf8');
const timeImageQuery = fs.readFileSync(path.join(__dirname, 'query', 'imageStep.sql'), 'utf8');
const imageStepHistoryQuery = fs.readFileSync(path.join(__dirname, 'query', 'imageStepHistory.sql'), 'utf8');

const mapRecordToImageObject = (record) => Object.freeze({
  name: record.subject_name,
  gatewayId: record.gateway_id,
  nodeName: record.node_name || '<unknown>',
  cameraId: record.camera_id,
  stepName: record.step_name,
  collection_time: record.collection_time,
  images: record.images,
  stepOutput: record.step_output,
  _created: Date.now()
});

const lastImageStepsNoConsul = async function(customer){
  var {rows} = await db.query(customer, lastImageQuery);

  var result = rows.map(row => mapRecordToImageObject(row));

  return result;
};

module.exports.imageStepsAtTimeNoConsul = async function(customer, date){
  var {rows} = await db.query(customer, timeImageQuery, [date]);

  var result = rows.map(row => mapRecordToImageObject(row));

  return result;
};

module.exports.imageStepHistoryNoConsul = async function(customer, gatewayId, cameraId, fromDate, toDate){
  var {rows} = await db.query(customer, imageStepHistoryQuery, [fromDate, toDate, gatewayId, cameraId]);

  var result = rows.map(row => mapRecordToImageObject(row));

  return result;
};

const IMAGE_INDEX_QUERY = `
SELECT images[:index] image
  FROM image_step
 WHERE gateway_id = :gw
   AND camera_id = :cam
   AND collection_time = :time
   AND step_name = :step
`;

const IMAGE_NAME_QUERY = `
SELECT image FROM (
  SELECT gateway_id, camera_id, step_name, collection_time, UNNEST(images) image
    FROM image_step
   WHERE gateway_id = :gw
     AND camera_id = :cam
     AND collection_time = :time
     AND step_name = :step
  ) i WHERE substring(image from '_([^_]*)$') = :index
`;

const IMAGE_WINDOW_QUERY = `
WITH img_links AS (SELECT UNNEST(images) image from image_step WHERE
step_name = :step
AND gateway_id = :gw
AND camera_id = :cam
AND collection_time > :startTime AND collection_time <= :time)

SELECT image FROM img_links  WHERE split_part(split_part(image, '/', 13), '_', 3) = :timeStr
`;

const STEP_OUTPUT_QUERY = `
SELECT step_output
FROM image_step
WHERE gateway_id = :gateway_id
AND camera_id = :camera_id
AND collection_time = to_timestamp(:collection_time)
AND step_name = :step_name LIMIT 1`;

const BLACKLIST_CONTEXT_QUERY = `
UPDATE image_step
SET step_output = :step_output
WHERE gateway_id = :gateway_id
AND camera_id = :camera_id
AND collection_time = to_timestamp(:collection_time)
AND step_name = :step_name
`;

const imageLocation = async function(customer, parameters) {
  var query = (isNaN(Number(parameters.index))) ? IMAGE_NAME_QUERY : IMAGE_INDEX_QUERY;
  parameters.index =  (isNaN(Number(parameters.index))) ? parameters.index : Number(parameters.index);
  const statement  = sql(query)(parameters);
  var { rows: imageLocationRows } = await db.query(customer, statement.text, statement.values);

  if (!imageLocationRows || imageLocationRows.length === 0) {
    query = IMAGE_WINDOW_QUERY;
    let timeStr = moment(parameters.time).unix();
    if (`${parameters.time}`.split('.').length > 1) {
      timeStr += '.' + `${parameters.time}`.split('.')[1].split('+')[0].trim();
    }
    parameters.timeStr = timeStr;
    parameters.startTime = moment(parameters.time).subtract(3, 'minutes').format();
    delete parameters.index;
    const newStatement = sql(query)(parameters);
    var { rows: imageRows } = await db.query(customer, newStatement.text, newStatement.values);
    imageLocationRows = imageRows;
  }
  const oImageParts = imageLocationRows.map(
    ({image}) => 'string' === typeof image ? image.split('/') : []
  ).filter(
    (components) => components && components.length
  );

  oImageParts.forEach((oImageKeyObject) => {
    if (oImageKeyObject && oImageKeyObject.length) {
      const oImageFileName = oImageKeyObject[oImageKeyObject.length - 1];
      const oFileComponents = oImageFileName.split('_');
      oImageKeyObject[oImageKeyObject.length - 1] = oFileComponents.join('_');
    }
  });
  oImageParts.forEach((element, index) => oImageParts[index] = element.join('/'));

  const oFinalFileName = oImageParts.find((element) => element.length);

  return oFinalFileName;
};

module.exports.getStepOutput = async function(customer, parameters) {
  const query = STEP_OUTPUT_QUERY;
  const statement  = sql(query)(parameters);
  let { rows: result } = await db.query(customer, statement.text, statement.values);
  return result;
};


module.exports.blacklistContext = async function(customer, parameters) {
  const query = BLACKLIST_CONTEXT_QUERY;
  const statement  = sql(query)(parameters);
  let { rowCount: result } = await db.query(customer, statement.text, statement.values, true);
  return result;
};

const MAX_AGE = 2 * 60 * 1000;
function onCacheHit(cache, options) {
  debug(`${options.profileName.split(' ')[0]}:HIT`,cache.keys);
}
function onCacheAdd(cache, options) {
  debug(`${options.profileName.split(' ')[0]}:MISS`,cache.keys);
}

const CACHE_OPTS = { maxAge: MAX_AGE, isPromise: true, onCacheHit, onCacheAdd };

module.exports.imageLocation = moize(imageLocation, CACHE_OPTS);
module.exports.lastImageStepsNoConsul = moize(lastImageStepsNoConsul, CACHE_OPTS);
