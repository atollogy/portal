function getCenterPoint(bboxList) {
// get center point from bbox Object
  const listOfOriginalCenters = [];

  for (let index = 0; index < bboxList.length; index++) {
    const centerX = Math.round(bboxList[index][0] + (bboxList[index][2] / 2));
    const centerY = Math.round(bboxList[index][1] + (bboxList[index][3] / 2));
    listOfOriginalCenters.push([centerX, centerY]);
  }
  return listOfOriginalCenters;
}

function getDistance(listOfTransformedCenters, listOfOriginalCenters, xScalingFactor = 1, yScalingFactor = 1){
// compate two center points
  const listOfDistances = [];

  for (let first = 0; first < listOfTransformedCenters.length - 1; ++first){
    for (let second = first + 1; second < listOfTransformedCenters.length; ++second) {
      const xDelta = (listOfTransformedCenters[first][0] - listOfTransformedCenters[second][0]) / xScalingFactor;
      const yDelta = (listOfTransformedCenters[first][1] - listOfTransformedCenters[second][1]) / yScalingFactor;
      const distance = Math.hypot(xDelta, yDelta);
      listOfDistances.push([distance, [listOfOriginalCenters[first], listOfOriginalCenters[second]]]);
    }
  }
  return listOfDistances;
}

function fetchSeverityCount(detectedObjectList, listOfTransformedCenters, xScalingFactor, yScalingFactor, violationCalculationMultiplyer) {
  let peopleBbox = [];
  const safeDistance = 8 * violationCalculationMultiplyer;
  const minorViolation = 6 * violationCalculationMultiplyer;
  const siginificantViolation = 4 * violationCalculationMultiplyer;
  let socialDistanceResult = {centroids: {}, severityCount: 0};

  const objectsOfInterest = ['person', 'operator'];

  for (const key in detectedObjectList) {
    if (objectsOfInterest.includes(key)) {
      if (detectedObjectList[key].length > 0) {
        peopleBbox = [...peopleBbox,...detectedObjectList[key]];
      }
    }
  }

  if (peopleBbox.length === 1) {
    return socialDistanceResult;
  }

  let listOfOriginalCenters = null;
  if (detectedObjectList) {
    listOfOriginalCenters = getCenterPoint(peopleBbox);
  } else {
    listOfOriginalCenters = listOfTransformedCenters;
  }
  let listOfDistances = null;
  if (violationCalculationMultiplyer !== 1) {
    listOfDistances = getDistance(listOfTransformedCenters, listOfOriginalCenters);
  } else {
    listOfDistances = getDistance(listOfTransformedCenters, listOfOriginalCenters, xScalingFactor, yScalingFactor);
  }

  for (let index = 0 ; index < listOfDistances.length; index++){
    let severityCount = 0;
    if (listOfDistances[index][0] < siginificantViolation) {
      severityCount = Math.max(severityCount, 4);
    } else if (listOfDistances[index][0] < minorViolation) {
      severityCount = Math.max(severityCount, 3);
    } else if (listOfDistances[index][0] < safeDistance) {
      severityCount = Math.max(severityCount, 2);
    } else {
      severityCount = Math.max(severityCount, 1);
    }
    if (!socialDistanceResult.centroids[severityCount]) {
      socialDistanceResult.centroids[severityCount] = [];
    }
    socialDistanceResult.centroids[severityCount] =
    [...socialDistanceResult.centroids[severityCount],[...listOfDistances[index][1]]];
    socialDistanceResult.severityCount = Math.max(socialDistanceResult.severityCount, severityCount);
  }
  return socialDistanceResult;
}

module.exports = {
  getDistance,
  getCenterPoint,
  fetchSeverityCount
};