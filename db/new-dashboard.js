const db = require('../db');

const gwImgs = `
SELECT DISTINCT ON (b.facility, b.beacon_id, i.gateway_id)
  COALESCE(b.beacon_id, i.gateway_id)       gateway_id,
  i.camera_id                               camera_id,
  i.step_name                               step_name,
  b.subject_name    as                      name,
  i.collection_time as                      collection_time,
  EXTRACT(epoch FROM i.collection_interval) collection_interval,
  b.facility                                facility,
  CASE WHEN l.img IS NOT NULL THEN split_part(l.img, '/', 5) 
       ELSE null
  END nodename,
  CASE WHEN l.img IS NOT NULL
    THEN array_to_json( array_agg(
            (i.gateway_id,
             i.camera_id,
             i.step_name,
             i.collection_time,
             substring(l.img FROM '_([^_]*)$')) :: image_reference))
  END                                       img_keys
FROM (SELECT *
      FROM image_step
      WHERE collection_time > now() - INTERVAL '2 days') i
  FULL OUTER JOIN customer_binding b ON
                                       i.gateway_id = b.beacon_id
                                       AND (b.namespace = 'ad135bf7eecc08752f96') --atollogy gw namespace
                                       AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
                                       AND (b.end_time IS NULL OR i.collection_time <= b.end_time)
  LEFT JOIN unnest(i.images) AS l(img) ON l.img LIKE '%'
GROUP BY b.facility, b.beacon_id, i.gateway_id, i.camera_id, i.step_name, b.subject_name, i.collection_time,
  i.collection_interval, l.img
ORDER BY b.facility, b.beacon_id, i.gateway_id, i.collection_time DESC
`;

module.exports.gwImgs = async function (customer) {
  var {rows} = await db.query(customer, gwImgs, []);
  return rows;
};


