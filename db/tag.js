const db = require('../db/');
const sql = require('yesql').pg;
const fs = require('fs');
const path = require('path');
const { Cache } = require('../lib/cache');
const cache = new Cache(5 * 60);
const assetQuery = fs.readFileSync(path.join(__dirname, 'query', 'customerAsset.sql'), 'utf8');

module.exports.insertAsset = async function (customer, parameters) {
  let q_insert_new_asset = `
    INSERT INTO asset_attribute(asset_id, qualifier, attribute_value, latest_seen_time, first_seen_time, asset_info)
    VALUES (:licensePlate, 'license_plate', :licensePlate, now(), now(), '{}')
    ON CONFLICT(asset_id, qualifier, attribute_value) DO NOTHING
  `;

  let q_trailer_info = `
  INSERT INTO asset_attribute(asset_id, qualifier, attribute_value, latest_seen_time, first_seen_time, asset_info)
  VALUES (:licensePlate, 'Unit Number', :trailerId, NULL, now(), '{}')
  ON CONFLICT(asset_id, qualifier, attribute_value) DO NOTHING
`;

  let q_carrier_info = `
  INSERT INTO asset_attribute(asset_id, qualifier, attribute_value, latest_seen_time, first_seen_time, asset_info)
  VALUES (:licensePlate, 'Truck Type', :carrierName, NULL, now(), '{}')
  ON CONFLICT(asset_id, qualifier, attribute_value) DO NOTHING
`;
  //First attempt to insert the new asset. If asset already exists then do nothing
  q_insert_new_asset = sql(q_insert_new_asset)(parameters);
  await db.query(customer, q_insert_new_asset.text, q_insert_new_asset.values, true);

  q_trailer_info = sql(q_trailer_info)(parameters);
  await db.query(customer, q_trailer_info.text, q_trailer_info.values, true);

  q_carrier_info = sql(q_carrier_info)(parameters);
  await db.query(customer, q_carrier_info.text, q_carrier_info.values, true);

};


module.exports.insertTag = async function (customer, parameters) {
  let q_insert_new_asset = `
      INSERT INTO asset_attribute(asset_id, qualifier, attribute_value, latest_seen_time, first_seen_time, asset_info)
      VALUES (:lp, 'license_plate', :lp, now(), now(), '{}')
      ON CONFLICT(asset_id, qualifier, attribute_value) DO NOTHING
  `;

  let q_new_tags = `
WITH insert_new_tags AS (
	INSERT INTO asset_attribute (asset_id, attribute_value, qualifier, latest_seen_time, first_seen_time, asset_info)
	SELECT :lp, attr, :tagName, NULL, now(), '{}'
	FROM unnest(:tagsAdded :: TEXT[]) attr ON CONFLICT DO NOTHING
)

DELETE FROM asset_attribute
WHERE asset_id = :lp AND attribute_value = ANY(:tagsRemoved::TEXT[]);
`;
  //First attempt to insert the new asset. If asset already exists then do nothing
  q_insert_new_asset = sql(q_insert_new_asset)(parameters);
  await db.query(customer, q_insert_new_asset.text, q_insert_new_asset.values, true);

  //Next insert new tags (attributes) for the asset in question
  q_new_tags = sql(q_new_tags)(parameters);
  await db.query(customer, q_new_tags.text, q_new_tags.values, true);
};

module.exports.getAssetId = async function (customer, parameters = {}) {

  let q = sql(`
  SELECT a1.asset_id FROM asset_attribute a1, asset_attribute a2
  WHERE a1.qualifier = 'Truck Type' and upper(a1.attribute_value) = :carrierName
  and a2.qualifier = 'Unit Number' and upper(a2.attribute_value) = :trailerId
  and upper(a1.asset_id) = upper(a2.asset_id)`)(parameters);
  try {
    const { rows } = await db.query(customer, q.text, q.values, true);
    return rows;
  } catch (error) {
    throw `Could not fetch list of customer tags. Error is: ${error}.`;
  }
};

module.exports.getAllLPNAssets = async function (customer, parameters = {}) {

  let data = [];
  //Fetch data => try from cache first; if not then get it from db
  if (Object.prototype.hasOwnProperty.call(parameters, 'disable-cache')) {
    delete parameters['disable-cache'];
  } else {
    const cachedData = await cache.get(customer, {...parameters});
    data = data.concat(cachedData).filter(Boolean);
  }
  if (data.length > 0) {
    return data;
  } else {
    let q = sql(`
    SELECT a1.asset_id license_plate, a1.attribute_value carrier_name, a2.attribute_value unit_number FROM asset_attribute a1, asset_attribute a2
    where a1.qualifier = 'Truck Type' and a2.qualifier = 'Unit Number'
    and a1.asset_id = a2.asset_id`)(parameters);
    try {
      const { rows } = await db.query(customer, q.text, q.values, true);
      data = rows;
      cache.put(customer, {...parameters}, data);
      return data;
    } catch (error) {
      throw `Could not fetch list of customer tags. Error is: ${error}.`;
    }
  }
};

module.exports.listAllTags = async function (customer, parameters = {}) {
  const tagNames = Object.assign({}, { tagNames: [] }, parameters);
  const listOfTags = (Array.isArray(tagNames) ? tagNames : [tagNames]).filter(Boolean);
  Object.assign(parameters, { tagNames: listOfTags });

  let q = sql(`SELECT DISTINCT attribute_value FROM asset_attribute WHERE qualifier = ANY(:tagNames)`)(parameters);
  try {
    const { rows } = await db.query(customer, q.text, q.values, true);
    return rows;
  } catch (error) {
    throw `Could not fetch list of customer tags. Error is: ${error}.`;
  }
};

module.exports.listAllTagAssets = async function (customer, parameters) {
  let q = sql(assetQuery)(parameters);
  try {
    const {rows} = await db.query(customer, q.text, q.values, true);
    return rows;
  } catch (error) {
    throw `Could not fetch list of customer tags. Error is: ${error}.`;
  }
};

module.exports.filterTagAssets = async function (customer, parameters) {
  let q = sql(`
    with tags as (
      select
        asset_id,
        array_agg(attribute_value) tags
      from asset_attribute
      where qualifier = any(:tagNames)
      group by asset_id
    ), notags as (
      select 
      asset_id,
      '{}'::_text as tags
      from asset_attribute
      where qualifier = 'license_plate' and asset_id not in (
        select asset_id from tags
      )
      group by asset_id
    ), filter_tags as (
      select t.*
      from tags t
      where (
        case when :tags::text[] is null then true
          when array_length(:tags::text[], 1) = 1 and :includeNotag::boolean = true then t.tags = :tags::text[]
          when array_length(:tags::text[], 1) = 1 and :includeNotag::boolean = false then t.tags @> :tags::text[]
          when array_length(:tags::text[], 1) > 1 then (:tags::text[] @> t.tags)
          else t.asset_id = all(t.tags)
        end
      )
    ), result_tags as (
      select asset_id from filter_tags union select asset_id from notags where (case when :includeNotag::boolean = true then true else false end)
    ), assets as (
      select t.asset_id, (case when a.tags is null then '{}'::_text else a.tags end) as tags
      from result_tags t
      left join filter_tags a on a.asset_id = t.asset_id
    )
    select
      a.asset_id,
      t.tags,
      a.asset_info->> 'source_image_key' latest_images,
      a.latest_seen_time last_seen_time,
      split_part((a.asset_info ->> 'source_image_link'), '/', 5) nodename,
      a.asset_info ->> 'source_image_link' image,
      count(*) OVER() AS total
    from assets t
    join asset_attribute a using (asset_id)
    where a.qualifier = 'license_plate' and (case when :assetId::text is not null then lower(a.asset_id) like lower(concat('%', :assetId::text, '%')) else true end)
      and (case when :camIds::text is not null then split_part((a.asset_info ->> 'source_image_link'), '/', 5) = ANY(:camIds) else true end)
      and (:from::timestamp IS NULL or a.latest_seen_time >= :from)
      and (:to::timestamp IS NULL or a.latest_seen_time < :to)
      and a.asset_info->> 'source_image_key' is not null
    order by a.latest_seen_time ${parameters.sortDate}
    limit :pageSize offset :offset;
  `)(parameters);
  try {
    const {rows} = await db.query(customer, q.text, q.values, true);
    return rows;
  } catch (error) {
    throw `Could not fetch list of customer tags. Error is: ${error}.`;
  }
};

module.exports.filterTagAssetCount = async function (customer, parameters) {
  let q = sql(`
    with tags as (
      select
        asset_id,
        array_agg(attribute_value) tags
      from asset_attribute
      where qualifier = any(:tagNames)
      group by asset_id
    ), filter_tags as (
      select t.*
      from tags t
      where (case when :tags::text[] is not null then t.tags @> :tags::text[] else true end)
    )
    select
      count(*) AS total
    from filter_tags t
    join asset_attribute a using (asset_id)
    where a.qualifier = 'license_plate' and (case when :assetId::text is not null then lower(a.asset_id) like lower(concat('%', :assetId::text, '%')) else true end)
      and (case when :camIds::text is not null then split_part((a.asset_info ->> 'source_image_link'), '/', 5) = ANY(:camIds) else true end);
  `)(parameters);
  try {
    const {rows} = await db.query(customer, q.text, q.values, true);
    return rows;
  } catch (error) {
    throw `Could not fetch list of customer tags. Error is: ${error}.`;
  }
};

module.exports.updateTags = async function (customer, parameters) {
  try {
    let q = sql(`
      with insert_new_asset as (
        INSERT INTO asset_attribute(asset_id, qualifier, attribute_value, latest_seen_time, first_seen_time, asset_info)
        select asset_id_in, 'license_plate', asset_id_in, now(), now(), '{}'
        from unnest(:assetIds::TEXT[]) asset_id_in
        ON CONFLICT(asset_id, qualifier, attribute_value) DO NOTHING
      ), insert_new_tags as (
        insert into asset_attribute (asset_id, attribute_value, qualifier, latest_seen_time, first_seen_time, asset_info)
        select asset_id_in, tag_in, :tagName, NULL, now(), '{}'
        from unnest(:assetIds::TEXT[]) asset_id_in, 
          unnest(:tags::TEXT[]) tag_in
          on conflict(asset_id, qualifier, attribute_value) do nothing	
      )
      delete from asset_attribute
      where asset_id = any(:assetIds::TEXT[]) 
        and attribute_value not in (select unnest(:tags::TEXT[]))
        and qualifier <> 'license_plate'
    `)(parameters);
    const {rows} = await db.query(customer, q.text, q.values, true);
    return rows;
  } catch (error) {
    throw `Could not fetch list of customer tags. Error is: ${error}.`;
  }
};
