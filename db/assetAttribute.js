const fs = require('fs');
const path = require('path');
const db = require('.');
const sql = require('yesql').pg;
const assetAttributesQuery = fs.readFileSync(path.join(__dirname, 'query', 'fetchAssetAttributes.sql'), 'utf8');


module.exports.fetchAssetAttributes = async (customer, assetParams) => {
  const assetQ = sql(assetAttributesQuery)(assetParams);
  const {rows} = await db.query(customer, assetQ.text, assetQ.values);
  const license_plates_map = rows.reduce((lp_map, row) => {
    if (row.qualifier) {
      if (!lp_map[row.asset_id]) {
        lp_map[row.asset_id] = {};
      }
      lp_map[row.asset_id][row.qualifier] = row.attribute_value;
    }
    return lp_map;
  }, {});
  return license_plates_map;
};