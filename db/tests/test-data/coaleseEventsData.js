const moment = require('moment');

const testData = [
  [ // ----- inputs for test #1
    "Coalesce 4 consecutive events",
    [
      {
        "collection_time": moment("2018-12-10T10:00:00"),
        "collection_interval": 60,
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "state": "present",
        "images": [
          "test1.jpg"
        ]
      },
      {
        "collection_time": moment("2018-12-10T10:01:00"),
        "collection_interval": 60,
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "state": "present",
        "images": [
          "test2.jpg"
        ]
      },
      {
        "collection_time": moment("2018-12-10T10:02:00"),
        "collection_interval": 60,
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "state": "present",
        "images": [
          "test3.jpg"
        ]
      },
      {
        "collection_time": moment("2018-12-10T10:03:00"),
        "collection_interval": 60,
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "state": "present",
        "images": [
          "test4.jpg"
        ]
      }
    ],
    // ---- expected output for test #1
    [
      {
        "start_time": "2018-12-10T10:00:00",
        "end_time": "2018-12-10T10:04:00",
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "state": "present",
        "images": [
          "test1.jpg",
          "test2.jpg",
          "test3.jpg",
          "test4.jpg"
        ]
      }
    ]
  ],
  [ // ----- inputs for test #2
    "Coalesce 4 subsequent events with a gap",
    [
      {
        "collection_time": moment("2018-12-10T10:00:00"),
        "collection_interval": 60,
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "state": "present",
        "images": [
          "test1.jpg"
        ]
      },
      {
        "collection_time": moment("2018-12-10T10:02:00"),
        "collection_interval": 60,
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "state": "present",
        "images": [
          "test2.jpg"
        ]
      },
      {
        "collection_time": moment("2018-12-10T10:03:00"),
        "collection_interval": 60,
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "state": "present",
        "images": [
          "test3.jpg"
        ]
      },
      {
        "collection_time": moment("2018-12-10T10:04:00"),
        "collection_interval": 60,
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "state": "present",
        "images": [
          "test4.jpg"
        ]
      }
    ],
    [ // ---- expected output for test #2
      {
        "start_time": "2018-12-10T10:00:00",
        "end_time": "2018-12-10T10:01:00",
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "state": "present",
        "images": [
          "test1.jpg"
        ]
      },
      {
        "start_time": "2018-12-10T10:02:00",
        "end_time": "2018-12-10T10:05:00",
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "state": "present",
        "images": [
          "test2.jpg",
          "test3.jpg",
          "test4.jpg"
        ]
      }
    ]
  ]
];

module.exports = testData;