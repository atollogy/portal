const moment = require('moment');

const testData = [
  [ // ----- inputs for test 0
    "Smooth out an absent slot between 2 present slots",
    [
      {
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "collection_time": moment("2018-12-10T10:00:00"),
        "collection_interval": 60,
        "state": "present"
      },
      {
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "collection_time": moment("2018-12-10T10:01:00"),
        "collection_interval": 60,
        "state": "absent"
      },
      {
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "collection_time": moment("2018-12-10T10:02:00"),
        "collection_interval": 60,
        "state": "present"
      }
    ],
    {
      "current_index": 1
    },
    {
      "back_slots": 1,
      "forward_slots": 1
    },
    // ---- expected output for test 0
    {
      "subject_name": "Machine 1",
      "measure_name": "activity",
      "collection_time": "2018-12-10T10:01:00",
      "collection_interval": 60,
      "state": "present"
    }
  ],
  [ // ----- inputs for test 1
    "Negative test - Try to smooth out a present slot in between 2 absent slots and verify that no smoothing happens",
    [
      {
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "collection_time": moment("2018-12-10T10:00:00"),
        "collection_interval": 60,
        "state": "absent"
      },
      {
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "collection_time": moment("2018-12-10T10:01:00"),
        "collection_interval": 60,
        "state": "present"
      },
      {
        "subject_name": "Machine 1",
        "measure_name": "activity",
        "collection_time": moment("2018-12-10T10:02:00"),
        "collection_interval": 60,
        "state": "absent"
      }
    ],
    {
      "current_index": 1
    },
    {
      "back_slots": 1,
      "forward_slots": 1
    },
    // ---- expected output for test 1
    {
      "subject_name": "Machine 1",
      "measure_name": "activity",
      "collection_time": "2018-12-10T10:01:00",
      "collection_interval": 60,
      "state": "present"
    }
  ],
  [ // ----- inputs for test 2
    "Smooth out 2 absent slots between 2 present slots",
    [
      {
        "subject_name": "Machine 2",
        "measure_name": "operator",
        "collection_time": moment("2018-12-10T10:00:00"),
        "collection_interval": 60,
        "state": "present"
      },
      {
        "subject_name": "Machine 2",
        "measure_name": "operator",
        "collection_time": moment("2018-12-10T10:01:00"),
        "collection_interval": 60,
        "state": "absent"
      },
      {
        "subject_name": "Machine 2",
        "measure_name": "operator",
        "collection_time": moment("2018-12-10T10:02:00"),
        "collection_interval": 60,
        "state": "present"
      },
      {
        "subject_name": "Machine 2",
        "measure_name": "operator",
        "collection_time": moment("2018-12-10T10:03:00"),
        "collection_interval": 60,
        "state": "present"
      }
    ],
    {
      "current_index": 1
    },
    {
      "back_slots": 1,
      "forward_slots": 2
    },
    // ---- expected output for test 2
    {
      "subject_name": "Machine 2",
      "measure_name": "operator",
      "collection_time": "2018-12-10T10:01:00",
      "collection_interval": 60,
      "state": "present"
    }
  ],
  [ // ----- inputs for test 3
    "Negative Test - Try to smooth out 2 present slots between 2 absent slots",
    [
      {
        "subject_name": "Machine 2",
        "measure_name": "operator",
        "collection_time": moment("2018-12-10T10:00:00"),
        "collection_interval": 60,
        "state": "absent"
      },
      {
        "subject_name": "Machine 2",
        "measure_name": "operator",
        "collection_time": moment("2018-12-10T10:01:00"),
        "collection_interval": 60,
        "state": "present"
      },
      {
        "subject_name": "Machine 2",
        "measure_name": "operator",
        "collection_time": moment("2018-12-10T10:02:00"),
        "collection_interval": 60,
        "state": "absent"
      },
      {
        "subject_name": "Machine 2",
        "measure_name": "operator",
        "collection_time": moment("2018-12-10T10:03:00"),
        "collection_interval": 60,
        "state": "absent"
      }
    ],
    {
      "current_index": 1
    },
    {
      "back_slots": 1,
      "forward_slots": 2
    },
    // ---- expected output for test 3
    {
      "subject_name": "Machine 2",
      "measure_name": "operator",
      "collection_time": "2018-12-10T10:01:00",
      "collection_interval": 60,
      "state": "present"
    }
  ],
  [ // ----- inputs for test 4
    "Dont smooth anything",
    [
      {
        "subject_name": "Machine 3",
        "measure_name": "material",
        "collection_time": moment("2018-12-10T10:00:00"),
        "collection_interval": 60,
        "state": "absent"
      },
      {
        "subject_name": "Machine 3",
        "measure_name": "material",
        "collection_time": moment("2018-12-10T10:01:00"),
        "collection_interval": 60,
        "state": "absent"
      },
      {
        "subject_name": "Machine 3",
        "measure_name": "material",
        "collection_time": moment("2018-12-10T10:02:00"),
        "collection_interval": 60,
        "state": "absent"
      }
    ],
    {
      "current_index": 1
    },
    {
      "back_slots": 1,
      "forward_slots": 1
    },
    // ---- expected output for test 4
    {
      "subject_name": "Machine 3",
      "measure_name": "material",
      "collection_time": "2018-12-10T10:01:00",
      "collection_interval": 60,
      "state": "absent"
    }
  ]
];

module.exports = testData;