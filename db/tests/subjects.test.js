const moment = require('moment');
const subjects = require('../subjects');
const smoothTestData = require('./test-data/smoothedState');
const coaleseEventsData = require('./test-data/coaleseEventsData');

//Test for smoothTestData function
describe('Provide mock data with gaps in it and slot sizes to interpolate the gaps, then verify each gap was filled as expected', () => {
  test.each(smoothTestData)(
    'Test Case: %s',
    (name, a, b, c, expected) => {
      let result = subjects.smoothedState(b.current_index, a, {
        smoothing_look_back_slots: c.back_slots,
        smoothing_look_ahead_slots: c.forward_slots
      });
      result.collection_time = moment(result.collection_time).format('YYYY-MM-DD[T]HH:mm:ss');
      expect(result).toEqual(expected);
    },
  );
});


//Test for coalesceData function
describe('Provide consecutive (and not consecutive) time-series events then see if they were coalesced together correctly', () => {
  test.each(coaleseEventsData)(
    'Test Case: %s',
    (name, input, expected) => {
      let result = subjects.coaleseEvents(input);
      result = result.map(r => ({
        ...r,
        start_time: r.start_time.format('YYYY-MM-DD[T]HH:mm:ss'),
        end_time: moment(r.end_time).format('YYYY-MM-DD[T]HH:mm:ss')
      }));
      expect(result).toMatchObject(expected);
    },
  );
});
