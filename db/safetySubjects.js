const fs = require('fs');
const path = require('path');
const db = require('../db');
const _ = require('lodash');
const moment = require('moment');
const sql = require('yesql').pg;
const subjectReportQuery = fs.readFileSync(path.join(__dirname, 'query', 'safetySubjectReport.sql'), 'utf8');
const reportConfigQuery = fs.readFileSync(path.join(__dirname, 'query', 'reportConfig.sql'), 'utf8');
const { Cache } = require('../lib/cache');
const cache = new Cache(5 * 60);
const debug = require('debug')('portal:subjects');
const { newMissingDataHandler } = require('../lib/missing-data');
const portalHelper = require('../lib/portalHelper');

const reportCfgAPIs = require('../db/reportConfig.js');

const FILTER_MODE_TYPES = ['exclusion', 'marker'];

const userDB = require('../lib/user-management');
const { Observation, Filters }  = require('../lib/customer-filter');
const { withMeasureName, withSubjectName, withGatewayId } = Filters;
const { filterByBlacklistRules } = require('../lib/utils/filter');

const { hasOwnProperty } = {};
const hasPropertyValue = (sourceObject, propertyName) => {
  return hasOwnProperty.call(sourceObject, propertyName) && null != sourceObject[propertyName];
};

const socialDistance = require('./socialDistance.js');
const {  newCustomerDeviceConfigMaker, fetchCustomerDeviceConfig } = require('./deviceConfig.js');
const fetchDeviceConfig = fetchCustomerDeviceConfig(newCustomerDeviceConfigMaker, userDB, sql);

const MINIMUM_CONFIDENCE_SCORE = 0.30;
const objectsOfInterest = ['person', 'operator'];
const DefaultDistanceObject = Object.freeze({ xScalingFactor: 8192, yScalingFactor: 8192 });


const newDeviceConfigQueryBuilder = () => {
  let deviceConfigQuery = undefined;
  return (queryParams) => {
    return 'undefined' !== typeof deviceConfigQuery ? deviceConfigQuery : (deviceConfigQuery = fetchDeviceConfig(queryParams));
  };
};

function searchConfig(event, config) {
  return config.filter(cfg => {
    return cfg.facility === event.facility && cfg.subject_name === event.subject_name && cfg.measure_name === event.measure_name;
  });
}

function smoothedState(currIndex, eventRows, cfg) {
  let lookBack = cfg && (cfg.smoothing_look_back_slots ? Number(cfg.smoothing_look_back_slots) : cfg.smoothing_look_back_slots);
  let lookAhead = cfg && (cfg.smoothing_look_ahead_slots ? Number(cfg.smoothing_look_ahead_slots) : cfg.smoothing_look_ahead_slots);

  //If lookAhead number of events don't exist in data or the previous event does not exist then return existing state
  if ((lookBack === null && lookAhead === null) || !eventRows[currIndex + lookAhead] || !eventRows[currIndex - lookBack] || currIndex == 0 || !['present', 'absent', 'moved', 'stationary'].includes(eventRows[currIndex].state) || eventRows[currIndex].measure_name === 'composite') {
    return eventRows[currIndex];
  }

  let nextStates = [];
  let nextSubjects = [];
  let nextMeasures = [];
  let prevStates = [];
  let prevSubjects = [];
  let prevMeasures = [];

  //Push lookAhead number of future states into nextVals
  for (let index = 1; index <= lookAhead; index++) {
    if (eventRows[currIndex + index].collection_time.diff(eventRows[currIndex + index - 1].collection_time, 'seconds') <= eventRows[currIndex].collection_interval) {
      nextStates.push(eventRows[currIndex + index].state);
      nextSubjects.push(eventRows[currIndex + index].subject_name);
      nextMeasures.push(eventRows[currIndex + index].measure_name);
    } else {
      nextSubjects.push(null);
      nextMeasures.push(null);
      nextStates.push(null);
    }
  }

  //Push lookBack number of past states into prevVals
  for (let i = 1; i <= lookBack; i++) {
    if (eventRows[currIndex - i + 1].collection_time.diff(eventRows[currIndex - i].collection_time, 'seconds') <= eventRows[currIndex].collection_interval) {
      prevStates.push(eventRows[currIndex - i].state);
      prevSubjects.push(eventRows[currIndex - i].subject_name);
      prevMeasures.push(eventRows[currIndex - i].measure_name);
    } else {
      prevStates.push(null);
      prevSubjects.push(null);
      prevMeasures.push(null);
    }
  }

  //If the given lookAhead number of future states do not belong to the same Subject/Measure as eventRows[currIndex].state
  //then return its original value without changing as we dont know enough to smooth it
  if (!nextSubjects.every(s => s === eventRows[currIndex].subject_name)
    || !nextMeasures.every(m => m === eventRows[currIndex].measure_name)) {
    return eventRows[currIndex];
  }
  if (!prevSubjects.every(s => s === eventRows[currIndex].subject_name)
    || !prevMeasures.every(m => m === eventRows[currIndex].measure_name)) {
    return eventRows[currIndex];
  }

  //Check if lookBack number of events exist and state was present
  const newEvent = eventRows[currIndex];

  if (prevStates.includes('present') && nextStates.includes('present')) {
    newEvent.state = 'present';
  }
  return newEvent;
}

function coaleseEvents(events) {
  let intervals = [];
  events.forEach((theEventObject, index) => { // coalesce events into intervals

    //Push very first event into intervals as it is with its own start/end time and state
    if (index == 0) {
      intervals.push(Object.assign(theEventObject, {
        start_time: theEventObject.collection_time,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate()
      }));

      //If state of current interval is the same as that of previous one AND then current event is not too far from previous event (within 1 collection interval) then group them together
    } else if (theEventObject.subject_name == events[index - 1].subject_name && theEventObject.measure_name == events[index - 1].measure_name && theEventObject.state == events[index - 1].state && moment(theEventObject.collection_time).diff(events[index - 1].collection_time) <= theEventObject.collection_interval * 1000) {
      let temp = Object.assign(theEventObject, {
        start_time: events[index - 1].start_time,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate(),
        images: events[index - 1].images ? [].concat(events[index - 1].images).concat(theEventObject.images) : [].concat(theEventObject.images)
      });
      //Remove previous event and update it with current events end_time and previous events start_time
      intervals.pop();
      if (!temp.centroids || temp.centroids.length === 0) {
        delete temp.centroids;
      }
      if (!temp.distances || temp.distances.length === 0) {
        delete temp.distances;
      }

      if (!temp.display_trapezoid) {
        delete temp.display_trapezoid;
      }

      intervals.push(temp);

      //else dont group them together
    } else {
      let temp = Object.assign(theEventObject, {
        start_time: theEventObject.collection_time,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate()
      });
      intervals.push(temp);
    }
  });

  return intervals;
}

const EventObject = function() {};

const NullControl = Object.freeze({
  withState: () => false,
  withLastState: () => false
});

const EventControl = Object.freeze({
  helper: (states) => {
    const listOfStates = Array.isArray(states) ? [].concat(states) : [ states ];
    return Object.freeze({
      withState: (state) => listOfStates.some((element) => element.state === state),
      withLastState: (state) => listOfStates.length && listOfStates[listOfStates.length - 1] && state === listOfStates[listOfStates.length - 1].state
    });
  },
  none: () => NullControl
});

EventObject.prototype.getMeasures = function () {
  const mapOfMeasures = this.states.reduce((measuresAggregate, { measure }) => {
    measuresAggregate[measure] = 1;
    return measuresAggregate;
  }, {});
  return Object.keys(mapOfMeasures);
};

EventObject.prototype.getStates = function () {
  const mapOfStates = this.states.reduce((stateAggregate, { state }) => {
    stateAggregate[state] = 1;
    return stateAggregate;
  }, {});
  return Object.keys(mapOfStates);
};

EventObject.prototype.hasMeasure = function (measure) {
  const statesOfInterest = this.states.filter((element) => element.measure === measure);
  return statesOfInterest && statesOfInterest.length ? EventControl.helper(statesOfInterest) : EventControl.none();
};

EventObject.prototype.getImagesFor = function (measure, state) {
  const images = [];
  this.states.filter(
    (element) => element.state === state && element.measure === measure
  ).reduce((listOfImages, stateObject) => {
    stateObject.images.forEach((element) => listOfImages.push(element));
    return listOfImages;
  }, images);
  return images;
};

EventObject.fromObject = (theSourceObject, theInstanceObject) => {
  if (theSourceObject) {
    theInstanceObject = theInstanceObject || new EventObject();
    Object.keys(theSourceObject).forEach((property) => {
      const value = theSourceObject[property];
      theInstanceObject[property] = value;
    });
  }
  return theInstanceObject;
};
function fillGapsofNMinutes(smoothenedIntervals, missingGapsFillDurationInMinutes) {
  for (let i = 0; i< smoothenedIntervals.length - 1; i++) {
    if (smoothenedIntervals[i].subject_name === smoothenedIntervals[i+1].subject_name &&
      smoothenedIntervals[i].measure_name === smoothenedIntervals[i+1].measure_name &&
      smoothenedIntervals[i+1].start_time.toDate() > smoothenedIntervals[i].end_time &&
      smoothenedIntervals[i+1].start_time.toDate() <=
      moment(smoothenedIntervals[i].end_time).add(missingGapsFillDurationInMinutes,'minutes').toDate()
    ) {
      if (smoothenedIntervals[i].state !== smoothenedIntervals[i+1].state) {
        smoothenedIntervals[i].end_time = smoothenedIntervals[i+1].start_time;
      } else {
        smoothenedIntervals[i+1].start_time = smoothenedIntervals[i].start_time;
        smoothenedIntervals[i+1].images =
        smoothenedIntervals[i].images.concat(smoothenedIntervals[i+1].images);
        smoothenedIntervals.splice(i, 1);
        --i;
      }
    }
  }
  return smoothenedIntervals;
}
function grpConcurrentEvents(listOfEvents) {
  const listOfConcurrentEvents = listOfEvents.reduce((mapOfEvents, theEventObject) => {
    const key = theEventObject.facility + '-' + theEventObject.subject_name + '-' + theEventObject.collection_time;
    const theConcurrentEventObject = mapOfEvents.get(key) || Object.assign({}, theEventObject, {
      measure_name: 'composite',
      state: '',
      states: [],
      concurrentStates: {},
      concurrentImages: {},
      concurrentBbox: {},
      _created: Date.now()
    });

    const theMeasureName = theEventObject.measure_name;
    theConcurrentEventObject.states.push(Object.freeze({
      measure: theEventObject.measure_name,
      state: theEventObject.state,
      images: theEventObject.images
    }));

    theConcurrentEventObject.concurrentStates[theMeasureName] = theEventObject.state;
    theConcurrentEventObject.concurrentImages[theMeasureName] = theEventObject.images;
    theConcurrentEventObject.concurrentBbox[theMeasureName] = theEventObject.bounding_box;
    theEventObject._parent = theConcurrentEventObject;

    theConcurrentEventObject._updated = Date.now();

    return mapOfEvents.set(key, theConcurrentEventObject);
  }, new Map());

  const theConcurrentEvents = [...listOfConcurrentEvents.values()].map(element => EventObject.fromObject(element));

  return theConcurrentEvents;
}

function calculateComposite(groups, config) {
  groups.forEach((configGroup) => {
    let matchEach = [];
    let missingMeasures = [];
    let compositeConfig = searchConfig(configGroup, config)[0];

    const compositeImages = {};
    if (compositeConfig && compositeConfig.composite_definition && compositeConfig.composite_definition.length) {
      compositeConfig.composite_definition.forEach((compositeElementConfig) => {

        Object.keys(compositeElementConfig).forEach((stateName) => {
          const compositeStateConfig = compositeElementConfig[stateName];
          if(stateName !== 'image_source') {
            let match = true;
            let altImgSource = null;
            Object.keys(compositeStateConfig).forEach((measureName) => {
              const expectedMeasureState = compositeStateConfig[measureName];

              const measureControlObject = configGroup.hasMeasure(measureName);
              match = match && (
                compositeStateConfig[measureName] === ':optional' ||
                measureControlObject.withState(expectedMeasureState));
              if (measureControlObject.withState(expectedMeasureState)) {
                const listOfImages = compositeImages[measureName] || [];
                const imagesOfInterest = configGroup.getImagesFor(measureName, expectedMeasureState);
                imagesOfInterest.map(
                  (imageObject) => Object.assign({}, imageObject, { measure: measureName })
                ).forEach(
                  (imageObject) => listOfImages.push(imageObject)
                );
                compositeImages[measureName] = listOfImages;
              }

              if (compositeStateConfig[measureName] !== ':optional' && (
                !(measureName in configGroup.concurrentStates) && !missingMeasures.includes(measureName)
              )) {
                missingMeasures.push(measureName);
              } else {
                altImgSource = compositeElementConfig['image_source'] || null;
              }
            });
            if (match) {
              matchEach.push({ state: stateName, imgSource: altImgSource || null });
            }
          }
        });
      });
      configGroup.state = matchEach.length ? matchEach[0].state : missingMeasures.length ? 'Missing Data-Some' : 'NA';
      configGroup.missingMeasures = missingMeasures;
      if (Object.keys(compositeImages).length) {
        configGroup.concurrentImages = compositeImages;
      }
      configGroup.images = Object.keys(configGroup.concurrentImages).map(
        (measure) => configGroup.concurrentImages[measure].map((imageObj) => Object.assign({}, imageObj, { measure }))
      ).reduce((accumulator, images) => accumulator.concat(images), []);
    } else {
      configGroup.state = 'NA';
      configGroup.missingCompositeDef = true;
    }
  });

  //Temporary way of removing rows where composite definition did not exist in config
  return groups.filter(g => !g.missingCompositeDef);
}

function applyFacilityFilter(facility, data) {
  const filtered = data.filter(record => record.facility === facility);
  return filtered;
}

function filterStatesBasedOnTheConfigs(events, config, phase) {
  return events.filter((e) => {
    const cfg = searchConfig(e, config);

    let filterStates = Array.isArray(cfg) && cfg[0].cs_config && cfg[0].cs_config.filter_states ? cfg[0].cs_config.filter_states : [];

    // Backward Compatibility - Convert to new format
    // Example: From: filter_states : ['present']
    // To: filter_states : [
    //       {  'state': 'present',
    //          'phases': ['fetching', 'dispatch']
    //       }
    //     ]
    filterStates = filterStates.map(element => 'string' === typeof element ? ({ state: element, phases: ['fetching','dispatch'] }) : element);

    return filterStates && filterStates.length ?
      !filterStates.some(element => element.state === e.state && element.phases.includes(phase)) : true;
  });
}

async function fetchOptionalComponentsParams(customer, facility ) {
  const OptionalComponentsParams = {};

  try {
    const { settings } = await reportCfgAPIs.getCustomerConfigs({ customer, facility });
    OptionalComponentsParams.optionalLookAheadDurationinSeconds = settings.length && settings[0].settings ? settings[0].settings.optionalLookAheadDurationinSeconds * 1000 : 240 * 1000;
    OptionalComponentsParams.optionalLookBackDurationinSeconds = settings.length && settings[0].settings ? settings[0].settings.optionalLookBackDurationinSeconds * 1000: 240 * 1000;
  } catch (ignored) {
    /* exception ignored on purpose, we proceed even if we fail to obtain the configuration */
    OptionalComponentsParams.optionalLookAheadDurationinSeconds = 240 * 1000;
    OptionalComponentsParams.optionalLookBackDurationinSeconds = 0;
  }

  return OptionalComponentsParams;
}

async function fetchMissingGapsDuration(customer, facility ) {
  let missingGapsFillDurationInMinutes = undefined;
  try {
    const { settings } = await reportCfgAPIs.getCustomerConfigs({ customer, facility });
    missingGapsFillDurationInMinutes = settings.length && settings[0].settings ? settings[0].settings.missingGapsFillDurationInMinutes : undefined;
  } catch (ignored) {
    /* exception ignored on purpose, we proceed even if we fail to obtain the configuration */
  }
  return missingGapsFillDurationInMinutes;
}

async function fetchSafetySubjects(customer, queryParams) {
  if (!Object.prototype.hasOwnProperty.call(queryParams, 'violation')) {
    queryParams = Object.assign({ violation: false }, queryParams);
  }

  debug('[SAFETY]  Obtaining configuration and data for %s', JSON.stringify(queryParams, null, 2));
  let {data, config, filters} = await fetchDataAndConfig(customer, queryParams);

  // Only keeping subject-measures configs which are not hidden
  config = config.filter((configEntry) => configEntry.hidden !== true);

  // Filter out states listed it in the 'customer_report_config->>cs_config' row has 'filter_states'
  // (only keep the states not in filter_states)
  // Filtering before calculating the composite
  // Example:
  // "filter_states": [
  //   {  state : 'present', phases: ['dispatch'],
  //      state: 'absent', phases: ['fetching']
  //   }
  // ]
  data = filterStatesBasedOnTheConfigs(data, config, 'fetching');

  //list of configured subjects
  let configuredSubjects = [...new Set(config.map(x => x.subject_name))];
  if (configuredSubjects.find(s => (s.indexOf('&') !== -1 || s.indexOf('#') !== -1)) !== undefined) throw new Error('Subject names and measure names in CRCs must not contain ampersands (&) nor pound signs (#).');
  if (queryParams.facility) data = applyFacilityFilter(queryParams.facility, data);

  //Sort and smooth data
  let events = _.sortBy(data, ['facility', 'subject_name', 'measure_name', 'collection_time']);

  /**
   * TODO: (eprha.carvajal create a feature flag to enable/disable LPN filtering) [Relates to AT-4445]
   * (!event.images || !event.state || event.images.step_name != 'lpr'
   */

  events = events.filter(
    (event) => filterByBlacklistRules(event.state)
  ).map(x => {
    return {
      ...x,
      shift_start: moment(x.start_time),
      shift_end: moment(x.end_time),
      images: x.images ? [{...x.images, measure: x.measure_name}] : [],
      collection_time: moment(x.collection_time),
      collection_interval: x.collection_interval ? moment.duration(x.collection_interval).asSeconds() : 60
    };
  });
  events = events.map((e, i, ary) => smoothedState(i, ary, searchConfig(ary[i], config)[0])); // smooth the detail measures
  if (filters.length) {
    events = applyCustomerFilters(filters, events, config.filter(x=>x.facility === queryParams.facility));
  }

  //Coalesce into intervals
  events = _.sortBy(events, ['facility', 'subject_name', 'measure_name', 'collection_time']);

  const deviceConfigQuery = newDeviceConfigQueryBuilder();
  const deviceConfigOptions = Object.assign({}, { customer }, { ...queryParams});
  const deviceConfiguration = await deviceConfigQuery(deviceConfigOptions);
  const mapOfGateways = deviceConfiguration.reduce((mapOfDevices, deviceConfigObject) => {
    const { device, configuration } = deviceConfigObject;
    const { social_feet_pixel } = configuration;
    Object.assign(mapOfDevices, {
      [device]: {
        xScalingFactor: social_feet_pixel,
        yScalingFactor: social_feet_pixel
      }
    });
    return mapOfDevices;
  }, {});

  const filteredEvents = applyCustomerFilters(
    filters.filter(({ mode: theFilterMode }) => 'marker' === theFilterMode),
    events,
    config.filter((theConfigObject) => theConfigObject.facility === queryParams.facility)
  );

  const eventsOfInterest = filteredEvents.filter(
    /** Does not include composite */
    (eventObject) => eventObject && eventObject.measure_name !== 'composite'
  ).filter(
    /** Must have at least one image */
    (eventObject) => eventObject.images && eventObject.images.length
  ).filter(
    /** Must have object_centers (new deployment to use transformed coordinates*/
    (eventObject) => eventObject.object_centers || eventObject.object_bottom_midpoints
  ).filter(
    /** Image should have the gateway_id */
    (eventObject) => eventObject.images.some((imageObject) => imageObject && imageObject.gateway_id)
  );

  let safetyEvents = eventsOfInterest.map((eventObject) => {
    const safetyViolationEventObject = Object.assign({}, eventObject);
    safetyViolationEventObject.subject_name = eventObject.subject_name;
    safetyViolationEventObject.measure_name = `${eventObject.measure_name} Worker Distancing`;
    safetyViolationEventObject.images = eventObject.images.slice();
    const arrayOfApplicableImages = eventObject.images.filter((imageObject) => imageObject && imageObject.gateway_id);
    const { xScalingFactor, yScalingFactor } = arrayOfApplicableImages.map(
      ({ gateway_id }) => mapOfGateways[gateway_id] || DefaultDistanceObject
    )[0];
    let socialDistanceResult = {};
    /*
     * In SD v1 and v2.1, all the distances are in Pixels
     * so violationCalculationMultiplyer is same as xScalingFactor
     * while v2.2 does independent X and Y scaling, and all the distances are in feets
     * so violationCalculationMultiplyer is 1
     */
    let violationCalculationMultiplyer = 1;
    if (eventObject.use_in_detection === 'undistorted_with_trapezoid' || eventObject.sd_version === '3') {
      socialDistanceResult.severityCount = '0';
      let keys = Object.keys(eventObject.distances).sort().reverse();
      for (let i in keys) {
        if (eventObject.distances[keys[i]].length) {
          socialDistanceResult.severityCount = keys[i];
          break;
        }
      }
    } else if (eventObject.object_centers) {
      violationCalculationMultiplyer = xScalingFactor;
      socialDistanceResult = socialDistance.fetchSeverityCount(eventObject.bounding_box,
        eventObject.object_centers /* listOfTransformedCenters */, xScalingFactor, yScalingFactor, violationCalculationMultiplyer);
    } else {
      let theXScalingFactor = null;
      let theYScalingFactor = null;
      if (eventObject.ppf_x && eventObject.ppf_y) {
        theXScalingFactor = eventObject.ppf_x;
        theYScalingFactor = eventObject.ppf_y;
        violationCalculationMultiplyer = 1;
      } else {
        const pixel_distance = Math.max(eventObject.pixel_distance, 1e-9);

        // Multiply by 0.0833333 to convert from inches to feet as the pixel_distance factor is in inches
        // 12 / eventObject.pixel_distance would result in the same thing.
        theXScalingFactor = 1 / (pixel_distance * 0.0833333);
        theYScalingFactor = 1 / (pixel_distance * 0.0833333);
        violationCalculationMultiplyer = theXScalingFactor;
      }

      const object_bottom_midpoints = Object.keys(eventObject.bounding_box).filter(
        (theObjectType) => objectsOfInterest.includes(theObjectType)
      ).reduce((elements, element) => {
        const candidates = eventObject.bounding_box[element] || [];
        return elements.concat(candidates);
      }, []).filter(
        (elements) => elements && elements.length && elements[elements.length - 1] >= MINIMUM_CONFIDENCE_SCORE
      ).map(
        ([ x, y, width, height ]) => [ Math.floor(x + 0.5 * width ), height > width ? Math.floor(y + (height - width) + 0.5 * width) : Math.floor(y + 0.5 * height) ]
      );

      Object.assign(safetyViolationEventObject, { object_bottom_midpoints });

      socialDistanceResult = socialDistance.fetchSeverityCount(null,
        object_bottom_midpoints /* listOfTransformedCenters */, theXScalingFactor, theYScalingFactor, violationCalculationMultiplyer);
    }
    safetyViolationEventObject.state = socialDistanceResult.severityCount;
    // #Todo: remove the for loop, as the images should always have only 1 image
    // (though put index_out_of_bounds check)
    for (let i = 0; i < safetyViolationEventObject.images.length; ++i) {
      const imageObject = Object.assign({}, safetyViolationEventObject.images[i]);
      if (eventObject.use_in_detection === 'undistorted_with_trapezoid' || eventObject.sd_version === '3') {
        imageObject.distances = eventObject.distances;
      } else {
        imageObject.centroids = socialDistanceResult.centroids;
      }
      imageObject.trapezoid = eventObject.display_trapezoid;
      safetyViolationEventObject.images[i] = imageObject;
    }
    return safetyViolationEventObject;
  });

  safetyEvents = eventsOfInterest.concat(safetyEvents);
  let intervals = coaleseEvents(safetyEvents);

  intervals = portalHelper.generatePortalImageLinks(intervals, customer);

  //Convert to subjects
  let theSubjects = curateSubjects(intervals) || { subjects: [] };
  theSubjects.timeLineStart = data.length ? data[0].start_time : null;
  theSubjects.timeLineEnd = data.length ? data[0].end_time : null;
  theSubjects.shiftName = queryParams.shift || undefined;

  const theMissingDataHandler = newMissingDataHandler(customer, config);
  return await theMissingDataHandler(theSubjects);
}

const FilterKey = Object.freeze({
  create: ({ customer, facility, gateway_id, measure_name, group_name, mode }) => {
    return `${customer}#${facility}#${gateway_id}#${measure_name}#${group_name}:${mode}`;
  }
});

function applyCustomerFilters(filters, events, config) {
  //creating config object
  let configObj = config.reduce((theConfigObject, theFilterObject) => {
    const { subject_name, measure_name, extract_key } = theFilterObject;
    if (!theConfigObject[subject_name]) {
      theConfigObject[subject_name] = {};
    }
    if (!theConfigObject[subject_name][measure_name]) {
      theConfigObject[subject_name][measure_name] = {};
    }
    theConfigObject[subject_name][measure_name] = { extractKey: extract_key };
    return theConfigObject;
  }, {});

  // creating filter object
  const mapOfFilterObjects = FILTER_MODE_TYPES.reduce((mapOfFilters, theFilterMode) => {
    const mapOfFilterObjectsWithMode = filters.filter(
      (filterConfig) => theFilterMode === filterConfig.mode
    ).reduce((mapOfFilterWithMode, filterConfig) => {
      const filterKey = FilterKey.create(filterConfig);
      let filterChain = mapOfFilterWithMode[filterKey] || Filters.identity();

      if (hasPropertyValue(filterConfig, 'gateway_id')) {
        const { gateway_id } = filterConfig;
        filterChain = filterChain.and( withGatewayId(gateway_id) );
      }

      if (hasPropertyValue(filterConfig, 'subject_name')) {
        const { subject_name } = filterConfig;
        filterChain = filterChain.and( withSubjectName(subject_name) );
      }

      if (hasPropertyValue(filterConfig, 'measure_name')) {
        const { measure_name } = filterConfig;
        filterChain = filterChain.and( withMeasureName(measure_name) );
      }

      if (hasPropertyValue(filterConfig, 'min_threshold') || hasPropertyValue(filterConfig, 'max_threshold')) {
        const { filter_type, measure_name, min_threshold, max_threshold } = filterConfig;
        const theThresholdConfig = Object.freeze({ lower: min_threshold, upper: max_threshold, measure: measure_name});
        filterChain = filterChain.and( Filters.ofType(filter_type).withThresholds(theThresholdConfig) );
      }

      if (filterChain !== Filters.identity()) {
        mapOfFilterWithMode[filterKey] = filterChain;
      }

      return mapOfFilterWithMode;
    }, {});

    return Object.assign(mapOfFilters, { [theFilterMode]: mapOfFilterObjectsWithMode });
  }, {});

  const newEventsProcessor = (listOfFilters) => ({
    matcher: (eventObject) => {
      const listOfObservations = [];

      if (hasPropertyValue(eventObject, 'bounding_box')) {
        const mapOfObjects = eventObject['bounding_box'];

        if (hasPropertyValue(eventObject, 'concurrentBbox')) {
          const mapOfCompositeObjects = eventObject['concurrentBbox'];

          Object.keys(mapOfCompositeObjects).forEach((type) => {
            const listOfObjectAttributes = mapOfCompositeObjects[type];

            if (listOfObjectAttributes && Array.isArray(listOfObjectAttributes)) {
              listOfObjectAttributes.filter(element => !!element).forEach((measure) => {
                mapOfObjects[measure] = mapOfObjects[measure] || [];
                mapOfObjects[measure].concat(listOfObjectAttributes[measure]);
              });
            }
          });
        }

        Object.keys(mapOfObjects).forEach((type) => {
          const listOfObjectAttributes = mapOfObjects[type];

          if (listOfObjectAttributes && Array.isArray(listOfObjectAttributes)) {
            const CONFIDENCE_SCORE_INDEX = 4;
            const newObjectSource = (pObjectType, index) => {
              return () => Object.freeze({
                mark: () => {
                  const listOfBoundingBoxes = Object.assign({ [pObjectType]: [] }, eventObject['bounding_box'])[pObjectType];
                  if (listOfBoundingBoxes && listOfBoundingBoxes.length && index < listOfBoundingBoxes.length) {
                    eventObject['bounding_box'][pObjectType][index][CONFIDENCE_SCORE_INDEX] = 0.20;
                  } else if (listOfBoundingBoxes && listOfBoundingBoxes.length) {
                    const listOfConcurrentBoundingBoxes = Object.assign({ [pObjectType]: [] }, eventObject['concurrentBbox'])[pObjectType];
                    if (listOfConcurrentBoundingBoxes && listOfConcurrentBoundingBoxes.length) {
                      const target = index - listOfBoundingBoxes.length;
                      eventObject['concurrentBbox'][pObjectType][target][CONFIDENCE_SCORE_INDEX] = 0.20;
                    }
                  }
                }
              });
            };

            listOfObjectAttributes.map(
              /* map the bounding box array to an object */
              ([ x, y, width, height, confidence ]) => ({ type, x, y, width, height, confidence })
            ).map(
              /* map the bounding box object to an Observation */
              (objectAttributes, index) => {
                const theObservationObject = Observation.fromBoundingBox(objectAttributes);
                return Object.assign({}, theObservationObject, { getSourceObject: newObjectSource(type, index) });
              }
            ).forEach(
              /* append every observation to the list */
              (observation) => listOfObservations.push(observation)
            );
          }
        });
      }

      const { images } = Object.assign({ images: [] }, eventObject);
      const gateways = images.filter(
        (theImageObject) => hasOwnProperty.call(theImageObject, 'gateway_id')
      ).map(
        ( { gateway_id } ) => gateway_id.toLowerCase()
      );

      const { bounding_box, measure_name, subject_name, shift_name, state, facility } = eventObject;
      const theEventConfigObject = Object.freeze(configObj[subject_name]);

      const theEventObject = Object.assign({}, {
        state, shift_name, facility, measure_name, subject_name, bounding_box, gateways,
        _Observations: listOfObservations,
        _ConfigObject: theEventConfigObject
      });

      const theComplementObjects = {};
      const theEventVariations = listOfObservations.map((theObservationObject) => {
        const { type: theObservationType } = theObservationObject;

        if (!Object.prototype.hasOwnProperty.call(theComplementObjects, theObservationObject)) {
          const listOfComplementaryObservations = listOfObservations.filter(
            ({ type: mObservationType }) => mObservationType !== theObservationObject.type
          );
          Object.assign(theComplementObjects, { [theObservationType]: listOfComplementaryObservations });
        }

        const theLocalObservations = [].concat(theObservationObject).concat(theComplementObjects[theObservationType]);

        return Object.assign({}, {
          _ParentEventObject: theEventObject,
          state, shift_name, facility, measure_name, subject_name, bounding_box, gateways,
          _Observations: theLocalObservations,
          _ConfigObject: theEventConfigObject
        });
      });

      /* only evaluate the filter if we haven't found a match (OR behavior) */
      const listOfMatchingVariations = theEventVariations.filter(
        (theEventVariation) => listOfFilters.some((theApplicableFilterObject) => theApplicableFilterObject.match(theEventVariation))
      );

      listOfMatchingVariations.filter(({ _Observations }) => _Observations.length).forEach(({ _Observations }) => {
        const theEventObservation = _Observations.shift();
        const theSourceBoundingBox = theEventObservation.getSourceObject();
        theSourceBoundingBox.mark();
      });

      return listOfMatchingVariations.length > 0;
    }
  });

  const EventsProcessor = Object.freeze({
    NoOp: (listOfEvents) => listOfEvents,
    exclusion: (listOfEvents, listOfFilters) => {
      if (listOfFilters.length) {
        const theEventProcessor = newEventsProcessor(listOfFilters);
        return listOfEvents.filter((theEventObject) => theEventProcessor.matcher(theEventObject));
      }
      return listOfEvents;
    },
    marker: (listOfEvents, listOfFilters) => {
      if (listOfFilters.length) {
        const theEventProcessor = newEventsProcessor(listOfFilters);
        return listOfEvents.map(theEventObject => Object.assign(theEventObject, {
          _Hidden: theEventProcessor.matcher(theEventObject)
        }));
      }
      return listOfEvents;
    }
  });

  const listOfMatchingEvents = FILTER_MODE_TYPES.reduce((listOfEvents, theFilterType) => {
    const listOfExcludeFilters = Object.values(mapOfFilterObjects[theFilterType]);
    const theEventProcessor = EventsProcessor[theFilterType] || EventsProcessor.NoOp;
    return theEventProcessor(listOfEvents, listOfExcludeFilters);
  }, events);

  return listOfMatchingEvents;
}

async function fetchDataAndConfig(customer, queryParams) {
  queryParams.from = queryParams.from || null;
  queryParams.to = queryParams.to || null;
  queryParams.byClass = queryParams.byClass || false;
  queryParams.violation = queryParams.violation || null;

  if (!Object.prototype.hasOwnProperty.call(queryParams, 'violation')) {
    queryParams = Object.assign({ violation: false }, queryParams);
  }

  let queries = [
    /* Query to fetch the filters from userdb */
    `SELECT * FROM customer_filters
      WHERE customer = :customer
        AND facility = :facility
        AND is_enable = TRUE
      ORDER BY measure_name, group_name`,
    reportConfigQuery
  ];

  let data = [];
  let cachedRecords = undefined;
  //Fetch data => try from cache first; if not then get it from db
  if (Object.prototype.hasOwnProperty.call(queryParams, 'disable-cache')) {
    delete queryParams['disable-cache'];
  } else {
    const cachedData = await cache.get(customer, {...queryParams});
    data = data.concat(cachedData).filter((element) => element);
    cachedRecords = { rows: data };
  }

  /**
   * If we have no data cache, lets queue the subject query
   */
  if (data.length === 0) {
    queries.push(`${subjectReportQuery} SELECT * FROM subject_event`);
  }

  const dataSections = [undefined, undefined, cachedRecords];
  await Promise.allSettled(
    queries.map( /* prepare the query context to the config */
      (sentence, index) => {
        const statement = (index === 0
          ? sql(sentence)({ customer, facility: queryParams.facility })
          : sql(sentence)(queryParams));

        const connection = (index === 0
          ? Object.freeze({ query: (text, values) => userDB.query(text, values) })
          : Object.freeze({ query: (text, values) => db.query(customer, text, values) }));

        return connection.query(statement.text, statement.values).then((result) => {
          dataSections[index] = result;
        }).catch((error) => {
          debug('Unable to process query for index', index);
        });
      })
  );

  const [ filters, config, records ] = dataSections.map(({ rows }) => rows);
  if (records && records.length && data.length === 0) {
    data = data.concat(records);
    cache.put(customer, {...queryParams}, records);
  }

  //Remove data that has no associated config
  data = data.filter((element) => searchConfig(element, config).length);

  return { data, config, filters };
}

function curateSubjects(intervals) {
  let highWaterMark = intervals.length ? intervals[0].shift_start : null;
  let timeSlotSize = null;
  const tempSubjects = {};
  _.each(intervals, (row) => {
    const subject = _.get(tempSubjects, row.subject_name, {});
    _.set(subject, 'name', row.subject_name);
    _.set(subject, 'facility', row.facility);

    const observation = {};
    _.set(observation, 'startTime', row.start_time);
    _.set(observation, 'endTime', row.end_time);
    highWaterMark = Math.max(highWaterMark, row.collection_interval ? (row.end_time - row.collection_interval * 1000) : row.start_time);

    _.set(observation, 'images', row.images);
    _.set(observation, 'd_images', row.d_images);
    _.set(observation, 'state', row.state);
    if (row.turn) {
      _.set(observation, 'turn', row.turn);
    }
    _.set(observation, 'missingMeasures', 'missingMeasures' in row ? row.missingMeasures : []);
    if (row.centroids) {
      _.set(observation, 'centroids', row.centroids);
    }

    if (row.distances) {
      _.set(observation, 'distances', row.distances);
    }

    if (row.display_trapezoid) {
      _.set(observation, 'trapezoid', row.display_trapezoid);
    }
    const obsAry = _.get(subject, `measures.${row.measure_name}`, []);
    obsAry.push(observation);
    _.set(subject, `measures.${row.measure_name}`, obsAry);
    _.set(tempSubjects, row.subject_name, subject);

    if (row.collection_interval) {
      timeSlotSize = Math.min(row.collection_interval * 1000, (timeSlotSize !== null) ? timeSlotSize : Infinity);
    }
  });

  if (!intervals.length) {
    return null;
  } else {
    return {
      timeSlotSizeMillis: timeSlotSize || 60000,
      shiftStartTime: intervals[0].shift_start,
      shiftEndTime: intervals[0].shift_end,
      timezone: intervals[0].tz,
      highWaterMarkTime: moment(highWaterMark).toDate(),
      subjects: _.values(tempSubjects)
    };
  }
}

function addMissingSubjectsAndMeasures(subjects, config) {
  return config.reduce((acc, curr) => {
    const subjectName = curr.subject_name;
    const facilityName = curr.facility;
    const measureName = curr.measure_name;
    const currSubject = subjects.subjects.find(s => s.name === subjectName) || {
      name: subjectName,
      facility: facilityName,
      measures: {}
    };
    const subject = acc[subjectName] || currSubject;
    if (!Object.keys(subject.measures).length || !(measureName in subject.measures)) {
      subject.measures[measureName] = [];
    }
    acc[subjectName] = subject;
    return acc;
  }, {});
}

module.exports = {
  applyCustomerFilters,
  addMissingSubjectsAndMeasures,
  fetchSafetySubjects,
  smoothedState,
  coaleseEvents,
  fillGapsofNMinutes,
  grpConcurrentEvents,
  calculateComposite,
  fetchOptionalComponentsParams,
  fetchMissingGapsDuration,
  curateSubjects
};
