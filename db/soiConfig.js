const fs = require('fs');
const path = require('path');
const db = require('../db/');
const sql = require('yesql').pg;
const soi = fs.readFileSync(path.join(__dirname, 'query', 'soiConfig.sql'), 'utf8');

module.exports.listSOIConfigs = async function (customer, facility) {
  const query = `${soi}`;
  const q = sql(query)();
  const {rows} = await db.query(customer, q.text, q.values, 1);
  return rows;
};
