const fs = require('fs');
const path = require('path');
const db = require('../db');
const _ = require('lodash');
const moment = require('moment');
const sql = require('yesql').pg;
const subjectReportQuery = fs.readFileSync(path.join(__dirname, 'query', 'subjectReport.sql'), 'utf8');
const reportConfigQuery = fs.readFileSync(path.join(__dirname, 'query', 'reportConfig.sql'), 'utf8');
const { Cache } = require('../lib/cache');
const cache = new Cache(5 * 60);
const debug = require('debug')('portal:subjects');
const { newMissingDataHandler } = require('../lib/missing-data');
const portalHelper = require('../lib/portalHelper');

const reportCfgAPIs = require('../db/reportConfig.js');
const assetAttributeAPIs = require('../db/assetAttribute.js');

const userDB = require('../lib/user-management');
const { Observation, Filters }  = require('../lib/customer-filter');
const { withMeasureName, withSubjectName, withGatewayId } = Filters;
const { filterByBlacklistRules } = require('../lib/utils/filter');

const { hasOwnProperty } = {};
const hasPropertyValue = (sourceObject, propertyName) => {
  return hasOwnProperty.call(sourceObject, propertyName) && null != sourceObject[propertyName];
};

function searchConfig(event, config) {
  return config.filter(cfg => {
    return cfg.facility === event.facility && cfg.subject_name === event.subject_name && cfg.measure_name === event.measure_name;
  });
}

function smoothedState(currIndex, eventRows, cfg) {
  let lookBack = cfg && (cfg.smoothing_look_back_slots ? Number(cfg.smoothing_look_back_slots) : cfg.smoothing_look_back_slots);
  let lookAhead = cfg && (cfg.smoothing_look_ahead_slots ? Number(cfg.smoothing_look_ahead_slots) : cfg.smoothing_look_ahead_slots);

  //If lookAhead number of events don't exist in data or the previous event does not exist then return existing state
  if ((lookBack === null && lookAhead === null) || !eventRows[currIndex + lookAhead] || !eventRows[currIndex - lookBack] || currIndex == 0 || !['present', 'absent', 'moved', 'stationary'].includes(eventRows[currIndex].state) || eventRows[currIndex].measure_name === 'composite') {
    return eventRows[currIndex];
  }

  let nextStates = [];
  let nextSubjects = [];
  let nextMeasures = [];
  let prevStates = [];
  let prevSubjects = [];
  let prevMeasures = [];

  //Push lookAhead number of future states into nextVals
  for (let index = 1; index <= lookAhead; index++) {
    if (eventRows[currIndex + index].collection_time.diff(eventRows[currIndex + index - 1].collection_time, 'seconds') <= eventRows[currIndex].collection_interval) {
      nextStates.push(eventRows[currIndex + index].state);
      nextSubjects.push(eventRows[currIndex + index].subject_name);
      nextMeasures.push(eventRows[currIndex + index].measure_name);
    } else {
      nextSubjects.push(null);
      nextMeasures.push(null);
      nextStates.push(null);
    }
  }

  //Push lookBack number of past states into prevVals
  for (let i = 1; i <= lookBack; i++) {
    if (eventRows[currIndex - i + 1].collection_time.diff(eventRows[currIndex - i].collection_time, 'seconds') <= eventRows[currIndex].collection_interval) {
      prevStates.push(eventRows[currIndex - i].state);
      prevSubjects.push(eventRows[currIndex - i].subject_name);
      prevMeasures.push(eventRows[currIndex - i].measure_name);
    } else {
      prevStates.push(null);
      prevSubjects.push(null);
      prevMeasures.push(null);
    }
  }

  //If the given lookAhead number of future states do not belong to the same Subject/Measure as eventRows[currIndex].state
  //then return its original value without changing as we dont know enough to smooth it
  if (!nextSubjects.every(s => s === eventRows[currIndex].subject_name)
    || !nextMeasures.every(m => m === eventRows[currIndex].measure_name)) {
    return eventRows[currIndex];
  }
  if (!prevSubjects.every(s => s === eventRows[currIndex].subject_name)
    || !prevMeasures.every(m => m === eventRows[currIndex].measure_name)) {
    return eventRows[currIndex];
  }

  //Check if lookBack number of events exist and state was present
  const newEvent = eventRows[currIndex];

  if (prevStates.includes('present') && nextStates.includes('present')) {
    newEvent.state = 'present';
  } else if (prevStates.includes('stationary') && nextStates.includes('stationary')) {
    newEvent.state = 'stationary';
  } else if (prevStates.includes('moved') && nextStates.includes('moved')) {
    newEvent.state = 'moved';
  }
  return newEvent;
}

function coaleseEvents(events) {
  let intervals = [];
  events.forEach((x, i) => { // coalesce events into intervals

    //Push very first event into intervals as it is with its own start/end time and state
    if (i == 0) {
      intervals.push(Object.assign(x, {
        start_time: x.collection_time,
        end_time: moment(x.collection_time).add(x.collection_interval, 's').toDate()
      }));

      //If state of current interval is the same as that of previous one AND then current event is not too far from previous event (within 1 collection interval) then group them together
    } else if (x.subject_name == events[i - 1].subject_name && x.measure_name == events[i - 1].measure_name && x.state == events[i - 1].state && moment(x.collection_time).diff(events[i - 1].collection_time) <= x.collection_interval * 1000) {
      let temp = Object.assign(x, {
        start_time: events[i - 1].start_time,
        end_time: moment(x.collection_time).add(x.collection_interval, 's').toDate(),
        images: events[i - 1].images ? [].concat(events[i - 1].images).concat(x.images) : [].concat(x.images)
      });
      //Remove previous event and update it with current events end_time and previous events start_time
      intervals.pop();
      if (!temp.centroids) {
        delete temp.centroids;
      }

      intervals.push(temp);

      //else dont group them together
    } else {
      let temp = Object.assign(x, {
        start_time: x.collection_time,
        end_time: moment(x.collection_time).add(x.collection_interval, 's').toDate()
      });
      intervals.push(temp);
    }
  });

  return intervals;
}

const EventObject = function() {};

const NullControl = Object.freeze({
  withState: () => false,
  withLastState: () => false
});

const EventControl = Object.freeze({
  helper: (states) => {
    const listOfStates = Array.isArray(states) ? [].concat(states) : [ states ];
    return Object.freeze({
      withState: (state) => listOfStates.some((element) => element.state === state),
      withLastState: (state) => listOfStates.length && listOfStates[listOfStates.length - 1] && state === listOfStates[listOfStates.length - 1].state
    });
  },
  none: () => NullControl
});

EventObject.prototype.getMeasures = function () {
  const mapOfMeasures = this.states.reduce((measuresAggregate, { measure }) => {
    measuresAggregate[measure] = 1;
    return measuresAggregate;
  }, {});
  return Object.keys(mapOfMeasures);
};

EventObject.prototype.getStates = function () {
  const mapOfStates = this.states.reduce((stateAggregate, { state }) => {
    stateAggregate[state] = 1;
    return stateAggregate;
  }, {});
  return Object.keys(mapOfStates);
};

EventObject.prototype.hasMeasure = function (measure) {
  const statesOfInterest = this.states.filter((element) => element.measure === measure);
  return statesOfInterest && statesOfInterest.length ? EventControl.helper(statesOfInterest) : EventControl.none();
};

EventObject.prototype.getImagesFor = function (measure, state) {
  const images = [];
  this.states.filter(
    (element) => element.state === state && element.measure === measure
  ).reduce((listOfImages, stateObject) => {
    stateObject.images.forEach((element) => listOfImages.push(element));
    return listOfImages;
  }, images);
  return images;
};

EventObject.fromObject = (theSourceObject, theInstanceObject) => {
  if (theSourceObject) {
    theInstanceObject = theInstanceObject || new EventObject();
    Object.keys(theSourceObject).forEach((property) => {
      const value = theSourceObject[property];
      theInstanceObject[property] = value;
    });
  }
  return theInstanceObject;
};
function fillGapsofNMinutes(smoothenedIntervals, missingGapsFillDurationInMinutes) {
  for (let i = 0; i< smoothenedIntervals.length - 1; i++) {
    if (smoothenedIntervals[i].subject_name === smoothenedIntervals[i+1].subject_name &&
      smoothenedIntervals[i].measure_name === smoothenedIntervals[i+1].measure_name &&
      smoothenedIntervals[i+1].start_time.toDate() > smoothenedIntervals[i].end_time &&
      smoothenedIntervals[i+1].start_time.toDate() <=
      moment(smoothenedIntervals[i].end_time).add(missingGapsFillDurationInMinutes,'minutes').toDate()
    ) {
      if (smoothenedIntervals[i].state !== smoothenedIntervals[i+1].state) {
        smoothenedIntervals[i].end_time = smoothenedIntervals[i+1].start_time;
      } else {
        smoothenedIntervals[i+1].start_time = smoothenedIntervals[i].start_time;
        smoothenedIntervals[i+1].images =
        smoothenedIntervals[i].images.concat(smoothenedIntervals[i+1].images);
        smoothenedIntervals.splice(i, 1);
        --i;
      }
    }
  }
  return smoothenedIntervals;
}
function grpConcurrentEvents(listOfEvents) {
  const listOfConcurrentEvents = listOfEvents.reduce((mapOfEvents, theEventObject) => {
    const key = theEventObject.facility + '-' + theEventObject.subject_name + '-' + theEventObject.collection_time;
    const theConcurrentEventObject = mapOfEvents.get(key) || Object.assign({}, theEventObject, {
      measure_name: 'composite',
      state: '',
      states: [],
      concurrentStates: {},
      concurrentImages: {},
      concurrentBbox: {},
      _created: Date.now()
    });

    if (theEventObject.violations) {
      theConcurrentEventObject.violations = theEventObject.violations;
    }

    const theMeasureName = theEventObject.measure_name;
    theConcurrentEventObject.states.push(Object.freeze({
      measure: theEventObject.measure_name,
      state: theEventObject.state,
      images: theEventObject.images
    }));

    Object.assign(
      theConcurrentEventObject,
      { concurrentStates: { [theMeasureName]: theEventObject.state } },
      { concurrentImages: { [theMeasureName]: theEventObject.images } },
      { concurrentBbox: { [theMeasureName]: theEventObject.bounding_box } }
    );

    theEventObject._parent = theConcurrentEventObject;

    theConcurrentEventObject._updated = Date.now();

    return mapOfEvents.set(key, theConcurrentEventObject);
  }, new Map());

  const theConcurrentEvents = [...listOfConcurrentEvents.values()].map(element => EventObject.fromObject(element));

  return theConcurrentEvents;
}

// This function relies on only current (active) composite definitions being sent in config
// This function enumerates the composite components across all subject-measures and does not do this per subject/measure:
// so, if we have certain measures that part of the composite definition for some subjects but not for others then this function
// will NOT remove them for the subjects that need them to be removed
function keepOnlyCompositeComponents(events, config) {
  const servicing_measures = new Set();
  config.forEach((configEntry) => {
    if (configEntry.measure_name === 'composite' && configEntry.composite_definition) {
      if (configEntry.cs_config && configEntry.cs_config.servicing_measures) {
        configEntry.cs_config.servicing_measures.forEach(item => servicing_measures.add(item));
      }
      configEntry.composite_definition.forEach((compositeDefinition) => {
        Object.values(compositeDefinition).forEach(measure => {
          const keys = Object.keys(measure);
          keys.filter(
            (measureName) => measure[measureName] !== ':optional'
          ).forEach(
            (measureName) => servicing_measures.add(measureName)
          );
        });
      });
    }
  });

  return events.filter((element) => servicing_measures.has(element.measure_name));
}

function calculateComposite(groups, config) {
  groups.forEach((configGroup, index) => {
    let matchEach = [];
    let missingMeasures = [];
    let compositeConfig = searchConfig(configGroup, config)[0];

    const compositeImages = {};
    if (compositeConfig && compositeConfig.composite_definition && compositeConfig.composite_definition.length) {
      compositeConfig.composite_definition.forEach((compositeElementConfig) => {

        Object.keys(compositeElementConfig).forEach((stateName) => {
          const compositeStateConfig = compositeElementConfig[stateName];
          if(stateName !== 'image_source') {
            let match = true;
            let altImgSource = null;
            let compositeMeasureStateMap = {};
            Object.keys(compositeStateConfig).forEach((measureName) => {
              const expectedMeasureState = compositeStateConfig[measureName];

              const measureControlObject = configGroup.hasMeasure(measureName);
              match = match && (
                compositeStateConfig[measureName] === ':optional' ||
                measureControlObject.withState(expectedMeasureState));
              if (match) {
                compositeMeasureStateMap[measureName] = expectedMeasureState;
              }
              if (measureControlObject.withState(expectedMeasureState)) {
                const listOfImages = compositeImages[measureName] || [];
                const imagesOfInterest = configGroup.getImagesFor(measureName, expectedMeasureState);
                imagesOfInterest.map(
                  (imageObject) => Object.assign({}, imageObject, { measure: measureName })
                ).forEach(
                  (imageObject) => listOfImages.push(imageObject)
                );
                compositeImages[measureName] = listOfImages;
              }

              if (compositeStateConfig[measureName] !== ':optional' && (
                !(measureName in configGroup.concurrentStates) && !missingMeasures.includes(measureName)
              )) {
                missingMeasures.push(measureName);
              } else {
                altImgSource = compositeElementConfig['image_source'] || null;
              }
            });
            if (match && (stateName === 'running' || stateName === 'violation') && groups[index].images.length <= 1) {
              groups[index].images = [];
              Object.keys(compositeMeasureStateMap).forEach(compositeMeasure => {
                let state = compositeMeasureStateMap[compositeMeasure];
                let compositeStates = configGroup.states.filter(s => s.measure === compositeMeasure
                  && (s.state === state || s.state === 'image'));
                if (compositeStates.length) {
                  compositeStates.forEach(st => {
                    groups[index].images.push(...st.images);
                  });
                }
              });
            }
            if (match){
              matchEach.push({ state: stateName, imgSource: altImgSource || null });
            }
          }
        });
      });
      configGroup.state = matchEach.length ? matchEach[0].state : missingMeasures.length ? 'Missing Data-Some' : 'NA';
      configGroup.missingMeasures = missingMeasures;
      // configGroup.images = Object.keys(configGroup.concurrentImages).map(
      //   (measure) => configGroup.concurrentImages[measure].map((imageObj) => Object.assign({}, imageObj, { measure }))
      // ).reduce((accumulator, images) => accumulator.concat(images), []);
    } else {
      configGroup.missingCompositeDef = true;
    }
  });

  //Temporary way of removing rows where composite definition did not exist in config
  return groups.filter(g => !g.missingCompositeDef);
}

function applyFacilityFilter(facility, data) {
  const out = data.filter(f => f.facility === facility);
  return out;
}

function filterStatesBasedOnTheConfigs(events, config, phase) {
  return events.filter((e) => {
    const cfg = searchConfig(e, config);

    let filterStates = Array.isArray(cfg) && cfg[0].cs_config && cfg[0].cs_config.filter_states ? cfg[0].cs_config.filter_states : [];

    // Backward Compatibility - Convert to new format
    // Example: From: filter_states : ['present']
    // To: filter_states : [
    //       {  'state': 'present',
    //          'phases': ['fetching', 'dispatch']
    //       }
    //     ]
    filterStates = filterStates.map(element => 'string' === typeof element ? ({ state: element, phases: ['fetching','dispatch'] }) : element);

    return filterStates && filterStates.length ?
      !filterStates.some(element => element.state === e.state && element.phases.includes(phase)) : true;
  });
}

async function fetchOptionalComponentsParams(customer, facility ) {
  const OptionalComponentsParams = {};

  try {
    const { settings } = await reportCfgAPIs.getCustomerConfigs({ customer, facility });
    OptionalComponentsParams.optionalLookAheadDurationinSeconds = settings.length && settings[0].settings ? settings[0].settings.optionalLookAheadDurationinSeconds * 1000 : 240 * 1000;
    OptionalComponentsParams.optionalLookBackDurationinSeconds = settings.length && settings[0].settings ? settings[0].settings.optionalLookBackDurationinSeconds * 1000: 240 * 1000;
  } catch (ignored) {
    /* exception ignored on purpose, we proceed even if we fail to obtain the configuration */
    OptionalComponentsParams.optionalLookAheadDurationinSeconds = 240 * 1000;
    OptionalComponentsParams.optionalLookBackDurationinSeconds = 0;
  }

  return OptionalComponentsParams;
}

async function fetchMissingGapsDuration(customer, facility ) {
  const configObject = {
    atomic: false,
    composite: false,
    duration: 1
  };

  try {
    const { settings } = await reportCfgAPIs.getCustomerConfigs({ customer, facility });
    const missingGapsFillDurationInMinutes = settings.length && settings[0].settings ? settings[0].settings.missingGapsFillDurationInMinutes : undefined;
    Object.assign(configObject, {
      atomic: false,
      composite: 'number' === typeof missingGapsFillDurationInMinutes,
      duration: missingGapsFillDurationInMinutes
    });
  } catch (ignored) {
    /**
     * exception ignored on purpose, we proceed even
     * if we fail to obtain the configuration
     **/
  }

  return configObject;
}

const EMPTY_INTERVAL = {};
const findClosestOptional = (targetTime, threshold, intervals) => {
  let left = 0;
  let right = intervals.length - 1;
  while (left < right) {
    let midpoint = Math.floor((left + right) / 2);

    const { collection_time: currentCollectionTime } = intervals[midpoint];

    const delta = currentCollectionTime.diff(targetTime);

    if (threshold >= Math.abs(delta)) {
      return {
        index: midpoint,
        interval: intervals[midpoint],
        interval_order: delta < 0 ? -1 : 1
      };
    } else if (delta > threshold) {
      right = midpoint - 1;
    } else {
      left = midpoint + 1;
    }
  }

  return {
    index: left,
    interval: EMPTY_INTERVAL
  };
};

/**
 * Always augment the composite with optional states info if available
 * @param events
 * @param config
 * @param composite
 */
function augmentCompositeWithOptionals({intervals, compositeIntervals, config, threshold}) {
  /**
   * get optional measures from config
   * filter intervals by the optional measures
   * filter the compositeIntervals with _state other than `ready-augmented`
   * for every compositeIntervals left, find the closes optional event available
   */
  compositeIntervals = _.sortBy(compositeIntervals, ['facility', 'subject_name', 'start_time']);
  const servicing_measures = new Set();
  let interesting_states_for_optionals = new Set();
  config.forEach((configEntry) => {
    if (configEntry.measure_name === 'composite' && configEntry.composite_definition) {
      configEntry.composite_definition.forEach(cd => {
        Object.values(cd).forEach(measure => {
          const keys = Object.keys(measure);
          keys.filter(
            (measureName) => measure[measureName] === ':optional'
          ).forEach(
            (measureName) => servicing_measures.add(measureName)
          );
        });
        Object.keys(cd).forEach(function(key) {
          Object.keys(cd[key]).forEach(function(measure){
            if (cd[key][measure] === ':optional') {
              interesting_states_for_optionals.add(key);
            }
          });
        });
      });
    }
  });

  intervals = intervals.filter((interval) => servicing_measures.has(interval.measure_name));
  intervals = _.sortBy(intervals, ['facility', 'subject_name', 'measure_name', 'start_time']);

  const subjectGroups = intervals.reduce((subjectName, interval) => {
    if (!subjectName[interval.subject_name]) {
      subjectName[interval.subject_name] = [];
    }
    subjectName[interval.subject_name].push(interval);
    return subjectName;
  }, {});
  let lpCompositeEvents = [];
  compositeIntervals = compositeIntervals.map((target, index, array) => {
    let match = null;
    // Optional association criteria
    // 1. Needs be specified in composite definition for a particular state
    // 2. Only associate with Transition interval OR start and end of the interval
    const allIntervals = subjectGroups[target.subject_name] || [];
    const filteredIntervalswithLP = allIntervals.filter((e) => {
      return e.state !== 'No LPN';
    }) || [];
    if (interesting_states_for_optionals.has(target.state)) {
      match = findClosestOptional(target.start_time, threshold, filteredIntervalswithLP);

      let index = match.index || 0;
      let intervalStartTime = target.start_time;
      let intervalEndTime = moment(target.end_time);

      while (index < filteredIntervalswithLP.length &&
          moment(filteredIntervalswithLP[index].collection_time) - intervalEndTime <= threshold
      ) {
        if (intervalStartTime - moment(filteredIntervalswithLP[index].collection_time) >= threshold
        ) {
          index++;
          continue;
        }
        if (!match.interval.confidence ||
          (match.interval.corelated_lp === false &&
            filteredIntervalswithLP[index].corelated_lp === true) ||
          (match.interval.corelated_lp === false &&
          filteredIntervalswithLP[index].confidence > match.interval.confidence)
        ) {
          match.interval = filteredIntervalswithLP[index];
          match.index = index;
        }
        index++;
      }
      if (match.interval === EMPTY_INTERVAL) {
        match = findClosestOptional(target.start_time, threshold, allIntervals);
      } else {
        match.isLPEvent = true;
      }
    } else {
      match = Object.freeze({
        index: -1,
        interval: EMPTY_INTERVAL
      });
    }

    let lpEvent = null;
    if (match.interval === EMPTY_INTERVAL) {
      /** nothing todo, no interval close enough */
    } else {
      /** merge match into interval */
      const { interval: element } = match;
      element.images.forEach((image) => {
        image.measure = element.state;
        if (element.customer_name) {
          image.customer_name = element.customer_name;
        }
        if (element.unit_number) {
          image.unit_number = element.unit_number;
        }
      });
      if (element.state === 'No LPN' && element.images.length > 4) {
        element.images = element.images.filter((e, idx, arr) =>
          idx % 4 === 0);
      }
      if (match.interval_order === -1 ) {
        target.images = [...element.images, ...target.images];
      } else {
        target.images = [...target.images, ...element.images];
      }
      if (match.isLPEvent) {
        lpEvent = Object.assign({}, target);
        lpEvent.measure_name = 'composite:LP';
        lpEvent.state = element.state;
      }
    }
    if (lpEvent) {
      lpCompositeEvents.push(lpEvent);
    }

    return target;
  });
  compositeIntervals.push(...lpCompositeEvents);
  return compositeIntervals;
}


async function fetchYardSubjects(customer, queryParams) {
  debug('[YARD] Obtaining configuration and data for %s', JSON.stringify(queryParams, null, 2));
  let {data, config, filters} = await fetchDataAndConfig(customer, queryParams);

  // Only keeping subject-measures configs which are not hidden
  config = config.filter((configEntry) => configEntry.hidden !== true);

  // Filter out states listed it in the 'customer_report_config->>cs_config' row has 'filter_states'
  // (only keep the states not in filter_states)
  // Filtering before calculating the composite
  // Example:
  // "filter_states": [
  //   {  state : 'present', phases: ['dispatch'],
  //      state: 'absent', phases: ['fetching']
  //   }
  // ]
  data = filterStatesBasedOnTheConfigs(data, config, 'fetching');

  //list of configured subjects
  let configuredSubjects = [...new Set(config.map(x => x.subject_name))];
  if (configuredSubjects.find(s => (s.indexOf('&') !== -1 || s.indexOf('#') !== -1)) !== undefined) throw new Error('Subject names and measure names in CRCs must not contain ampersands (&) nor pound signs (#).');
  if (queryParams.facility) data = applyFacilityFilter(queryParams.facility, data);

  //Sort and smooth data
  let events = _.sortBy(data, ['facility', 'subject_name', 'measure_name', 'collection_time']);

  /**
   * TODO: (eprha.carvajal create a feature flag to enable/disable LPN filtering) [Relates to AT-4445]
   * (!event.images || !event.state || event.images.step_name != 'lpr'
   */

  events = events.filter(
    (event) => event.state === 'No LPN' || !event.confidence || filterByBlacklistRules(event.state, true)
  ).map(x => {
    return {
      ...x,
      shift_start: moment(x.start_time),
      shift_end: moment(x.end_time),
      images: x.images ? [{...x.images, measure: x.measure_name}] : [],
      collection_time: moment(x.collection_time),
      collection_interval: x.collection_interval ? moment.duration(x.collection_interval).asSeconds() : 60
    };
  });

  // Populate LPs with asset_attirubtes

  const lps = events.reduce((lps,event) => {
    if (event.license_plate_asset && event.confidence) {
      lps.add(event.state);
    }
    return lps;
  }, new Set());

  const license_plates = Array.from(lps);

  const qualifiers = ['Truck Type', 'Unit Number'];

  const assetParams = {
    customer: customer,
    license_plates: license_plates,
    qualifiers: qualifiers
  };

  const license_plate_map = await assetAttributeAPIs.fetchAssetAttributes(customer, assetParams);

  for (let i = 0; i < events.length; ++i) {
    if (events[i].license_plate_asset && events[i].confidence) {
      const lp = events[i].state;
      if (license_plate_map[lp]) {
        events[i].customer_name = license_plate_map[lp]['Truck Type'];
        events[i].unit_number = license_plate_map[lp]['Unit Number'];
      }
    }
  }
  events = events.map((e, i, ary) => smoothedState(i, ary, searchConfig(ary[i], config)[0])); // smooth the detail measures

  if(filters.length) {
    events = applyCustomerFilters(filters, events, config.filter(x=>x.facility === queryParams.facility));
  }

  // eslint-disable-next-line no-unused-vars,no-undef
  if (config.map(a => a.measure_name).includes('composite')) {

    const onlyCompositeComponents = keepOnlyCompositeComponents(events, config);

    let groups = grpConcurrentEvents(onlyCompositeComponents);

    if(filters.length) {
      groups = applyCustomerFilters(filters, groups, config.filter(x=>x.facility === queryParams.facility));
    }
    let composite = calculateComposite(groups, config);

    // filter States from composite
    composite = filterStatesBasedOnTheConfigs(composite, config, 'dispatch');

    events = events.concat(composite);
  }

  //Coalesce into intervals
  events = _.sortBy(events, ['facility', 'subject_name', 'measure_name', 'collection_time']);
  const rawEvents =  _.cloneDeep(events);
  let intervals = await coaleseEvents(events);

  const OptionalComponentsParams = await fetchOptionalComponentsParams(customer, queryParams.facility);
  const threshold = Math.min(
    OptionalComponentsParams.optionalLookBackDurationinSeconds,
    OptionalComponentsParams.optionalLookAheadDurationinSeconds
  );

  let compositeIntervals = intervals.filter(i => i.measure_name === 'composite');
  let rawIntervals = intervals.filter(i => i.measure_name !== 'composite');

  // Fill Missing-data gaps of upto N (based on config) minutes
  const missingGapsFillDurationInMinutes = await fetchMissingGapsDuration(customer, queryParams.facility);

  if (missingGapsFillDurationInMinutes.atomic) {
    rawIntervals = fillGapsofNMinutes(rawIntervals, missingGapsFillDurationInMinutes.duration);
  }

  if (missingGapsFillDurationInMinutes.composite) {
    compositeIntervals = fillGapsofNMinutes(compositeIntervals, missingGapsFillDurationInMinutes.duration);
  }

  let augmentedCompositeIntervals = augmentCompositeWithOptionals({ intervals, compositeIntervals, config, threshold });
  let compositeLPIntervals = augmentedCompositeIntervals.filter(a => a.measure_name === 'composite:LP');
  augmentedCompositeIntervals = augmentedCompositeIntervals.filter(a => a.measure_name !== 'composite:LP');
  let finalIntervals = rawIntervals.concat(augmentedCompositeIntervals);
  finalIntervals = _.sortBy(finalIntervals, ['facility', 'subject_name', 'measure_name', 'start_time']);

  // filter States from composite and atomic
  finalIntervals = filterStatesBasedOnTheConfigs(finalIntervals, config, 'dispatch');

  //Convert to subjects
  finalIntervals.push(...compositeLPIntervals);
  finalIntervals = portalHelper.generatePortalImageLinks(finalIntervals, customer);

  let theSubjects = curateSubjects(finalIntervals) || { subjects: [] };

  //Adding missing subjects and measures
  let tempSubjects = addMissingSubjectsAndMeasures(theSubjects, config);

  //Convert to subjects
  theSubjects.subjects = Object.values(tempSubjects);
  theSubjects.shiftName = queryParams.shift || undefined;
  theSubjects.rawEvents = rawEvents;
  theSubjects.config = config;

  const theMissingDataHandler = newMissingDataHandler(customer, config);
  return await theMissingDataHandler(theSubjects);
}

function applyCustomerFilters(filters, events, config) {
  //creating config object
  let configObj = config.reduce((acc, val) => {
    if (!acc[val.subject_name]) {
      acc[val.subject_name] = {};
    }
    if (!acc[val.subject_name][val.measure_name]) {
      acc[val.subject_name][val.measure_name] = {};
    }
    acc[val.subject_name][val.measure_name] = {
      extractKey: val.extract_key
    };
    return acc;
  }, {});

  const FilterKey = Object.freeze({
    create: ({ customer, facility, gateway_id, measure_name, group_name }) => {
      return `${customer}#${facility}#${gateway_id}#${measure_name}#${group_name}`;
    }
  });

  // creating filter object
  const mapOfFilterObjects = filters.reduce((mapOfFilters, filterConfig) => {

    const filterKey = FilterKey.create(filterConfig);
    let filterChain = mapOfFilters[filterKey] || Filters.identity();

    if (hasPropertyValue(filterConfig, 'gateway_id')) {
      const { gateway_id } = filterConfig;
      filterChain = filterChain.and( withGatewayId(gateway_id) );
    }

    if (hasPropertyValue(filterConfig, 'subject_name')) {
      const { subject_name } = filterConfig;
      filterChain = filterChain.and( withSubjectName(subject_name) );
    }

    if (hasPropertyValue(filterConfig, 'measure_name')) {
      const { measure_name } = filterConfig;
      filterChain = filterChain.and( withMeasureName(measure_name) );
    }

    if (hasPropertyValue(filterConfig, 'min_threshold') || hasPropertyValue(filterConfig, 'max_threshold')) {
      const { filter_type, measure_name, min_threshold, max_threshold } = filterConfig;
      const theThresholdConfig = Object.freeze({ lower: min_threshold, upper: max_threshold, measure: measure_name});
      filterChain = filterChain.and( Filters.ofType(filter_type).withThresholds(theThresholdConfig) );
    }

    if (filterChain !== Filters.identity()) {
      mapOfFilters[filterKey] = filterChain;
    }

    return mapOfFilters;
  }, {});

  /**
   * negate the filter since we're excluding the matches
   */
  const listOfExcludeFilters = Object.values(mapOfFilterObjects).map(
    filterObject => filterObject
  );

  const EventsProcessor = {
    matcher: (eventObject) => {
      const listOfObservations = [];

      if (hasPropertyValue(eventObject, 'bounding_box')) {
        const mapOfObjects = eventObject['bounding_box'];

        if (hasPropertyValue(eventObject, 'concurrentBbox')) {
          const mapOfCompositeObjects = eventObject['concurrentBbox'];

          Object.keys(mapOfCompositeObjects).forEach((type) => {
            const listOfObjectAttributes = mapOfCompositeObjects[type];

            if (listOfObjectAttributes && Array.isArray(listOfObjectAttributes)) {
              listOfObjectAttributes.filter(element => !!element).forEach((measure) => {
                mapOfObjects[measure] = mapOfObjects[measure] || [];
                mapOfObjects[measure].concat(listOfObjectAttributes[measure]);
              });
            }
          });
        }

        Object.keys(mapOfObjects).forEach((type) => {
          const listOfObjectAttributes = mapOfObjects[type];

          if (listOfObjectAttributes && Array.isArray(listOfObjectAttributes)) {
            listOfObjectAttributes.map(
              /* map the bounding box array to an object */
              ([ x, y, width, height, confidence ]) => ({ type, x, y, width, height, confidence })
            ).map(
              /* map the bounding box object to an Observation */
              (objectAttributes) => Observation.fromBoundingBox(objectAttributes)
            ).forEach(
              /* append every observation to the list */
              (observation) => listOfObservations.push(observation)
            );
          }
        });
      }

      const { images } = Object.assign({ images: [] }, eventObject);
      const gateways = images.filter(
        (item) => hasOwnProperty.call(item, 'gateway_id')
      ).map(
        ( { gateway_id } ) => gateway_id.toLowerCase()
      );

      const { bounding_box, measure_name, subject_name, shift_name, state, facility } = eventObject;
      const theEventObject = Object.freeze({
        state, shift_name, facility, measure_name, subject_name, bounding_box, gateways,
        _Observations: listOfObservations,
        _ConfigObject: Object.freeze(configObj[eventObject.subject_name])
      });

      const matchesAtLeastOnce = listOfExcludeFilters.reduce(
        /* only evaluate the filter if we haven't found a match (OR behavior) */
        (isMatch, theExcludeFilterObject) => isMatch || theExcludeFilterObject.match(theEventObject), false);
      return !matchesAtLeastOnce;
    }
  };

  Object.defineProperties(EventsProcessor, {
    'exclusion': {
      value: (arrayOfEvents) => arrayOfEvents.filter(EventsProcessor.matcher)
    },
    'marker': {
      value: (arrayOfEvents) => arrayOfEvents.map(event => Object.assign(event, {_Hidden: EventsProcessor.matcher(event)}))
    }
  });

  let exclusionEvents = EventsProcessor.exclusion(events);
  return EventsProcessor.marker(exclusionEvents);
}

async function fetchMetrics(customer, queryParams, facility) {
  let {data, config} = await fetchDataAndConfig(customer, queryParams);
  //Sort and smooth data
  let events = _.sortBy(data, ['facility', 'subject_name', 'measure_name', 'collection_time']);
  events = events.map(x => {
    return {
      ...x,
      images: x.images ? [{...x.images, measure: x.measure_name}] : [],
      collection_time: moment(x.collection_time),
      collection_interval: x.collection_interval ? moment.duration(x.collection_interval).asSeconds() : 60
    };
  });
  events = events.map((e, i, ary) => smoothedState(i, ary, searchConfig(ary[i], config)[0])); // smooth the detail measures

  // eslint-disable-next-line no-unused-vars,no-undef
  if (config.map(a => a.measure_name).includes('composite')) {
    let groups = grpConcurrentEvents(events);
    let composite = calculateComposite(groups, config);
    events = events.concat(composite);
  }

  let metrics = calculateMetrics(events, queryParams.measures, queryParams.states);
  return metrics;
}

async function fetchDataAndConfig(customer, queryParams) {
  queryParams.from = queryParams.from || null;
  queryParams.to = queryParams.to || null;
  queryParams.byClass = queryParams.byClass || false;

  if (!Object.prototype.hasOwnProperty.call(queryParams, 'l_image_step_id')) {
    queryParams = Object.assign({ l_image_step_id: 0 }, queryParams);
  }
  if (!Object.prototype.hasOwnProperty.call(queryParams, 'r_image_step_id')) {
    queryParams = Object.assign({ r_image_step_id: 0x7FFFFFFFFFFF }, queryParams);
  }

  let queries = [
    /* Query to fetch the filters from userdb */
    `SELECT * FROM customer_filters
      WHERE customer = :customer
        AND facility = :facility
        AND is_enable = TRUE
      ORDER BY measure_name, group_name`,
    reportConfigQuery
  ];

  let data = [];
  let cachedRecords = undefined;
  //Fetch data => try from cache first; if not then get it from db
  if (Object.prototype.hasOwnProperty.call(queryParams, 'disable-cache')) {
    delete queryParams['disable-cache'];
  } else {
    const cachedData = await cache.get(customer, {...queryParams});
    data = data.concat(cachedData).filter((element) => element);
    cachedRecords = { rows: data };
  }

  /**
   * If we have no data cache, lets queue the subject query
   */
  if (data.length === 0) {
    queries.push(`${subjectReportQuery} SELECT * FROM subject_event`);
  }

  const dataSections = [undefined, undefined, cachedRecords];
  await Promise.allSettled(
    queries.map( /* prepare the query context to the config */
      (sentence, index) => {
        const statement = (index === 0
          ? sql(sentence)({ customer, facility: queryParams.facility })
          : sql(sentence)(queryParams));

        const connection = (index === 0
          ? Object.freeze({ query: (text, values) => userDB.query(text, values) })
          : Object.freeze({ query: (text, values) => db.query(customer, text, values) }));

        return connection.query(statement.text, statement.values).then((result) => {
          dataSections[index] = result;
        }).catch((error) => {
          debug('Unable to process query for index', index);
        });
      })
  );

  const [ filters, config, records ] = dataSections.map(({ rows }) => rows);
  if (records && records.length && data.length === 0) {
    data = data.concat(records);
    cache.put(customer, {...queryParams}, records);
  }

  //Remove data that has no associated config
  data = data.filter((element) => searchConfig(element, config).length);

  return { data, config, filters };
}

function curateSubjects(intervals) {
  let highWaterMark = intervals.length ? intervals[0].shift_start : null;
  let timeSlotSize = null;
  const tempSubjects = {};
  _.each(intervals, (row) => {
    const subject = _.get(tempSubjects, row.subject_name, {});
    _.set(subject, 'name', row.subject_name);
    _.set(subject, 'facility', row.facility);

    const observation = {};
    _.set(observation, 'startTime', row.start_time);
    _.set(observation, 'endTime', row.end_time);
    highWaterMark = Math.max(highWaterMark, row.collection_interval ? (row.end_time - row.collection_interval * 1000) : row.start_time);

    _.set(observation, 'images', row.images);
    _.set(observation, 'd_images', row.d_images);
    _.set(observation, 'state', row.state);
    if (row.turn) {
      _.set(observation, 'turn', row.turn);
    }
    if (row.customer_name) {
      _.set(observation, 'customer_name', row.customer_name);
    }
    if (row.unit_number) {
      _.set(observation, 'unit_number', row.unit_number);
    }
    _.set(observation, 'missingMeasures', 'missingMeasures' in row ? row.missingMeasures : []);
    if (row.centroids) {
      _.set(observation, 'centroids', row.centroids);
    }
    if (row.violations) {
      _.set(observation, 'violations', row.violations);
    }
    const obsAry = _.get(subject, `measures.${row.measure_name}`, []);
    obsAry.push(observation);
    _.set(subject, `measures.${row.measure_name}`, obsAry);
    _.set(tempSubjects, row.subject_name, subject);

    if (row.collection_interval) {
      timeSlotSize = Math.min(row.collection_interval * 1000, (timeSlotSize !== null) ? timeSlotSize : Infinity);
    }
  });

  if (!intervals.length) {
    return null;
  } else {
    return {
      timeSlotSizeMillis: timeSlotSize || 60000,
      shiftStartTime: intervals[0].shift_start,
      shiftEndTime: intervals[0].shift_end,
      timezone: intervals[0].tz,
      highWaterMarkTime: moment(highWaterMark).toDate(),
      subjects: _.values(tempSubjects)
    };
  }
}

function addMissingSubjectsAndMeasures(subjects, config) {
  return config.reduce((acc, curr) => {
    const subjectName = curr.subject_name;
    const facilityName = curr.facility;
    const measureName = curr.measure_name;
    const currSubject = subjects.subjects.find(s => s.name === subjectName) || {
      name: subjectName,
      facility: facilityName,
      measures: {}
    };
    const subject = acc[subjectName] || currSubject;
    if (!Object.keys(subject.measures).length || !(measureName in subject.measures)) {
      subject.measures[measureName] = [];
    }
    acc[subjectName] = subject;
    return acc;
  }, {});
}

function calculateMetrics(data, measures, states) {
  let metrics = data.reduce((r, o) => {
    //Initialize intervals and times to seconds for easier calculation
    let collection_time = moment(o.collection_time).valueOf() / 1000;
    let shiftEnd = moment(o.shift_end).valueOf() / 1000;
    let shiftStart = moment(o.shift_start).valueOf() / 1000;

    //Groupby facility + subject_name + date + shift_name and to calculate util across that group
    let key = o.facility + '-' + o.subject_name + '-' + o.date + '-' + o.shift_name;

    //Initialize what output should look like
    let newItem = r.get(key) || Object.assign({}, o, {
      highWaterMark: collection_time + o.collection_interval,
      numerator: moment.duration(0, 'minutes').asSeconds(),
      denominator: moment.duration(0, 'minutes').asSeconds(),
      utilization: 0.0,
      total_duration: 0
    });

    // end time of latest data slot we got for the group
    newItem.highWaterMark = Math.max(newItem.highWaterMark, collection_time + o.collection_interval);

    if (measures.includes(o.measure_name)) {
      // total time elapsed into a shift or the entire duration of shift whichever is smaller
      newItem.denominator = Math.min(shiftEnd, newItem.highWaterMark) - shiftStart;
      // duration of time in requested list of measures-states
      newItem.numerator = states.includes(o.state) ? newItem.numerator + o.collection_interval : newItem.numerator;
      // utilization
      newItem.utilization = (newItem.numerator / newItem.denominator) * 100;
      newItem.total_duration = newItem.numerator;
    }
    return r.set(key, newItem);
  }, new Map);
  metrics = [...metrics.values()];
  return metrics;
}

module.exports = {
  addMissingSubjectsAndMeasures,
  fetchYardSubjects,
  fetchMetrics,
  smoothedState,
  coaleseEvents,
  fillGapsofNMinutes,
  grpConcurrentEvents,
  calculateComposite,
  fetchOptionalComponentsParams,
  fetchMissingGapsDuration,
  curateSubjects,
  calculateMetrics
};
