const hash = require('string-hash');
const _ = require('lodash');

const consul = require('../consul.js');
const {Pool} = require('../lib/database');
const debug = require('debug')('portal:db-query');
const sanitizeParameters = require('../lib/sanitize');
const { hashCode } = require('../lib/utils/hash');

const readOnlyPools = {};
const writablePools = {};
const metrics = {
  connected: 0,
  acquired: 0,
  removed: 0,
  errors: 0
};

const fBindPoolEvents = (theConnectionPool, theCustomerName) => {
  const logger = require('debug')(`portal:metrics:${theCustomerName}`);

  theConnectionPool.on('remove', (/* client */) => {
    metrics.removed += 1;
    logger(`Connection removed (${theCustomerName}: ${JSON.stringify(metrics)})`);
  });
  theConnectionPool.on('acquire', (/* client */) => {
    metrics.acquired += 1;
    logger(`Connection acquired (${theCustomerName}: ${JSON.stringify(metrics)})`);
  });
  theConnectionPool.on('error', (error /* client */) => {
    metrics.errors += 1;
    logger(`Error in connection ${Object.prototype.hasOwnProperty.call(error, 'message') ? error.message : error}`);
  });
  theConnectionPool.on('connect', (client) => {
    metrics.connected += 1;
    client.query(`SET work_mem = '16MB'`);
    client.query(`SET temp_buffers = '16MB'`);
    logger(`Opening new connection (metrics:${theCustomerName}: ${JSON.stringify(metrics)})`);
  });
  return theConnectionPool;
};


async function databasePoolFor(customer, writable) {
  var pools = (writable) ? writablePools : readOnlyPools;

  if (pools[customer]) {
    return pools[customer];
  }

  const dbConfig = await consul.getDatabaseConnectionInfo(customer);
  const host = (!writable && dbConfig.ro_host) || dbConfig.host;
  debug((writable ? 'write ' : 'read ') + 'database for ' + customer + ' is ' + host);

  if (host === undefined) {
    return undefined;
  }

  pools[customer] = new Pool({
    user: dbConfig.user,
    host: host,
    database: dbConfig.database,
    password: dbConfig.password,
    port: dbConfig.port,
    idleTimeoutMillis: 60 * 1000,
    connectionTimeoutMillis: 60 * 1000
  });

  const currentConnectionPool = fBindPoolEvents(pools[customer], customer);

  return currentConnectionPool;
}

function writeDebugLine(text) {
  const qryHash = hash(text);
  const from = _.get(text.match(/from\W+(.*)/i), '[0]', text.substr(0, 20));
  debug('begin qry', qryHash, 'is', from);
}

const SimpleQueryStrategy = async (pool, text, parameters) => {
  const hashcode = hashCode(text);

  try {
    const theQueryPromise = new Promise((onQueryResolve, onQueryReject) => {
      pool.query(text, parameters, (cause, result) => {
        if (cause) {
          console.log(cause.stack);
          onQueryReject({ error: cause, source: 'client' });
        } else {
          onQueryResolve(result);
        }
      });
    });

    return await theQueryPromise;
  } catch (cause) {
    const summary = sanitizeParameters(parameters);
    const message = `${hashcode} DB threw an error while running a statement. Both follow:\nError:\n${cause}\n\nSQL:\n${text}\n\nParameters:\n${JSON.stringify(summary, null, 2)}`;
    cause.message = message;
    throw cause;
  }
};

const ManagedQueryStrategy = async (pool, text, parameters) => {
  const control = {};
  const hashcode = hashCode(text);

  try {
    const theQueryPromise = new Promise((resolve, reject) => {
      pool.connect((error, client, release) => {
        Object.assign(control, { release });

        if (error) {
          console.log(error.stack);
          reject({ error, source: 'pool' });
        } else {

          client.query(text, parameters, (cause, result) => {
            release();

            Object.assign(control, { release: false });
            if (cause) {
              console.log(cause.stack);
              reject({ error: cause, source: 'client' });
            } else {
              resolve(result);
            }
          });
        }
      });
    }).catch((cause) => {
      const summary = sanitizeParameters(parameters);
      throw new Error(`${hashcode} DB threw an error while running a statement. Both follow:\nError:\n${cause}\n\nSQL:\n${text}\n\nParameters:\n${JSON.stringify(summary, null, 2)}`);
    });

    return await theQueryPromise;
  } finally {
    try {
      if (control && control.release && 'function' === typeof control) {
        control.release(true);
      }
    } catch(cause) {
      debug('[WARNING] error while trying to release client', cause.message ? cause.message : cause);
    }
  }
};

const DatabaseManager = function (pQueryStrategy) {
  const theQueryStrategy = pQueryStrategy || ManagedQueryStrategy;
  Object.assign(this, { QueryStrategy: theQueryStrategy });
};

DatabaseManager.prototype.query = async function query(customer, text, parameters, writable, dbQueryTitle = 'query') {
  const { QueryStrategy: _query } = this;
  const pool = await databasePoolFor(customer, writable);

  if (!pool) {
    throw new Error(`skipping query: no database configured for ${customer}`);
  }

  writeDebugLine(text);

  return await _query(pool, text, parameters);
};

DatabaseManager.ManagedQueryStrategy = ManagedQueryStrategy;
DatabaseManager.SimpleQueryStrategy = SimpleQueryStrategy;
DatabaseManager.Singleton = new DatabaseManager(ManagedQueryStrategy);

module.exports = Object.freeze(DatabaseManager.Singleton);
