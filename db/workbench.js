const fs = require('fs');
const path = require('path');
const db = require('../db/');
const user = require('../lib/user-management/');
const _ = require('lodash');
const sql = require('yesql').pg;
const annotationStats = fs.readFileSync(path.join(__dirname, 'query', 'annotationStats.sql'), 'utf8');


module.exports.stepInfo = async function (customer) {
  const statement = `
        SELECT i.gateway_id,
               coalesce(b.subject_name, i.gateway_id) as gateway_name,
               i.camera_id,
               i.step_name,
               i.step_function_version,
               i.step_config_version,
               max(i.collection_time) last_seen_at_date,
               (case
                   when i.images [1] like '%image_readings%'
                        then split_part(split_part(i.images [1], '/', 12), '_', 3)
                   else split_part(split_part(i.images [1], '/', 13), '_', 3)
               end) collection_time_str
        FROM image_step i
                 LEFT OUTER JOIN unnest(i.images) j ON j LIKE '%_annotated.jpg'
                 LEFT OUTER JOIN customer_binding b ON
                i.gateway_id = b.beacon_id
                AND (b.namespace = 'ad135bf7eecc08752f96') -- Atollogy gw namespace
                AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
                AND (b.end_time IS NULL OR i.collection_time < b.end_time)
        WHERE insert_time > NOW() - INTERVAL '3 weeks'
        GROUP BY 1, 2, 3, 4, 5, 6, collection_time_str
        ORDER BY gateway_name, max(i.collection_time) DESC
  `;
  try {
    const {rows} = await db.query(customer, statement);
    return rows;
  } catch (error) {
    throw new Error(`Problems fetching step_name from image_step for db => ${[customer]}. Error is: ${error}.`);
  }
};


module.exports.stepKeys = async function (customer, parameters) {
  const statement = `
  SELECT r.*, count(*) OVER() AS total
  FROM (
    SELECT DISTINCT ON (i.collection_time, i.step_name, i.gateway_id, i.step_function_version, i.step_config_version)
      i.gateway_id,
      coalesce(b.subject_name, i.gateway_id) as gateway_name,
      i.camera_id,
      i.collection_time,
      i.step_name,
      i.step_function,
      i.step_function_version,
      i.step_config_version,
      i.images,
      i.image_step_id,
      i.step_output ->> 'identifier' as lpn,
      i.step_output->'qc_img_props'->'fuzzy_matched_lpn'->>'fuzzy_result' fuzzy_result,
      i.step_output->'qc_img_props'->'fuzzy_matched_lpn'->>'original_result' original_result,
      i.step_output ->> 'fullResponse' as ocr_full_response,
      CASE WHEN (i.step_output->'skew_metrics'->>(c.extract_threshold_definitions->'skew_calibration_result'->>'metric'))::int
                BETWEEN (c.extract_threshold_definitions->'skew_calibration_result'->>'>')::int
                AND (c.extract_threshold_definitions->'skew_calibration_result'->>'<')::int
          THEN 'whitelist'
          WHEN c.cs_config->'skew_calibration_result' IS NULL
                OR i.step_output->'skew_metrics' IS NULL
          THEN NULL
          ELSE 'blacklist'
      END skew_result,
      (case
          when i.images [1] like '%image_readings%'
               then split_part(split_part(i.images [1], '/', 12), '_', 3)
          else split_part(split_part(i.images [1], '/', 13), '_', 3)
       end) collection_time_str
    FROM image_step i
    JOIN unnest(i.images) j
      /*
          "%_sourceimg.jpg" image file name pattern is a workaround
          to allow OCR image showing up in QC Workbench per AT-5731
      */
      ON j LIKE '%_annotated.jpg' OR j LIKE '%_anonymous.jpg' OR j LIKE '%_sourceimg.jpg'
    LEFT OUTER JOIN customer_binding b ON
                        i.gateway_id = b.beacon_id
                        AND (b.namespace = 'ad135bf7eecc08752f96') -- Atollogy gw namespace
                        AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
                        AND (b.end_time IS NULL OR i.collection_time < b.end_time)
    LEFT OUTER JOIN customer_report_config c ON
                        b.facility = c.facility
                        AND i.step_name = c.step_name
                        AND b.subject_name = c.gateway_name
                        AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
                        AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    WHERE (:stepFunction::text IS NULL or i.step_function = ANY(:stepFunction))
      AND (:step::text IS NULL or i.step_name = ANY(:step))
      AND (:gw::text IS NULL or i.gateway_id = ANY(:gw))
      AND (:camId::text IS NULL or i.camera_id = :camId)
      AND (:imageStepId::text IS NULL or i.image_step_id = :imageStepId)
      AND (:from::timestamp IS NULL or i.collection_time >= :from)
      AND (:to::timestamp IS NULL or i.collection_time < :to)
      AND (:fnVersion::text IS NULL or i.step_function_version = :fnVersion)
      AND (:cfgVersion::text IS NULL or i.step_config_version = :cfgVersion)
      AND ((i.step_function <> 'lpr' AND i.step_function <> 'ocr')
          OR (i.step_function = 'ocr' AND (
              (:resultType = 'anchorMoved' AND i.step_output ->> 'fullResponse' IS NOT NULL AND i.step_output -> 'anchor_state_list' -> 0 ->> 'anchor_state' = 'moved')
              OR (:resultType = 'anchorStationary' AND i.step_output ->> 'fullResponse' IS NULL AND i.step_output -> 'anchor_state_list' -> 0 ->> 'anchor_state' = 'stationary')
              OR (:resultType = 'withText' AND i.step_output ->> 'fullResponse' IS NOT NULL) OR (:resultType = 'withoutText' AND i.step_output ->> 'fullResponse' is NULL)
            ))
          OR :resultType::text IS NULL
          OR (:resultType = 'lpr' AND i.step_output ->> 'identifier' IS NOT NULL)
          OR (:resultType = 'not-lpr' AND i.step_output ->> 'identifier' IS NULL)
          OR (:resultType = 'fuzzy' AND i.step_output ->> 'identifier' IS NOT NULL
              AND i.step_output->'qc_img_props'->'fuzzy_matched_lpn'->>'fuzzy_result' IS NOT NULL)
          OR (:resultType = 'correlation'
              AND i.step_output->'qc_img_props'->'correlation' IS NOT NULL)
          OR (:resultType = 'side-read'
            AND i.step_output ->> 'identifier' IS NOT NULL
            AND  (
                  c.extract_threshold_definitions->'skew_calibration' IS NOT NULL
                  AND i.step_output->'skew_metrics' IS NOT NULL
                  AND (((c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '<') IS NULL
                          OR (i.step_output->'skew_metrics' ->> (c.extract_threshold_definitions->'skew_calibration'->>'metric')::text)::float
                            > (c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '<')::float)
                        OR ((c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '>') IS NULL
                          OR (i.step_output->'skew_metrics' ->> (c.extract_threshold_definitions->'skew_calibration'->>'metric')::text)::float
                            < (c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '>')::float)
                      )
              )
          )
          OR (:resultType = 'lpr-skew'
            AND i.step_output ->> 'identifier' IS NOT NULL
            AND  (
                  c.extract_threshold_definitions->'skew_calibration' IS NOT NULL
                  AND i.step_output->'skew_metrics' IS NOT NULL
                  AND (((c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '<') IS NULL
                          OR (i.step_output->'skew_metrics' ->> (c.extract_threshold_definitions->'skew_calibration'->>'metric')::text)::float
                            < (c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '<')::float)
                        AND ((c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '>') IS NULL
                          OR (i.step_output->'skew_metrics' ->> (c.extract_threshold_definitions->'skew_calibration'->>'metric')::text)::float
                            > (c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '>')::float)
                      )
              )
          )
      )
    ORDER BY i.collection_time
  ) r
  LIMIT :pageSize OFFSET :offset;
  `;
  try {
    let q = sql(statement)(parameters);
    const {rows} = await db.query(customer, q.text, q.values);
    return {stepKeys: rows};
  } catch (error) {
    throw new Error(`Problems fetching step_name from image_step for db => ${[customer]}. Error is: ${error}.`);
  }
};

module.exports.getImageAnnonationsCount = async function (customer, parameters) {
  const statement = `
    select count(i.image_step_id)
    from (
      SELECT DISTINCT ON (i.collection_time, i.step_name, i.gateway_id, i.step_function_version, i.step_config_version)
          i.* , latest_qc_ann.image_step_annotation_id, latest_qc_ann.created_at_date, latest_qc_ann.qc_result, latest_qc_ann.comment
        FROM image_step i
        left join (
          select latest_qc_ann.*
          from (
              select
                max(image_step_annotation_id) as image_step_annotation_id
              from image_step_annotation
              group by
                collection_time, gateway_id, camera_id, step_name,
                step_config_version, step_function_version
          ) as latest_ann_id
          inner JOIN image_step_annotation as latest_qc_ann on latest_ann_id.image_step_annotation_id = latest_qc_ann.image_step_annotation_id
        ) as latest_qc_ann on latest_qc_ann.collection_time = i.collection_time and
          i.gateway_id = latest_qc_ann.gateway_id and
          i.camera_id = latest_qc_ann.camera_id and
          i.step_name = latest_qc_ann.step_name and
          i.step_config_version = latest_qc_ann.step_config_version and
          i.step_function_version = latest_qc_ann.step_function_version
        JOIN unnest(i.images) j
          /*
              "%_sourceimg.jpg" image file name pattern is a workaround
              to allow OCR image showing up in QC Workbench per AT-5731
          */
          ON j LIKE '%_annotated.jpg' OR j LIKE '%_anonymous.jpg' OR j LIKE '%_sourceimg.jpg'
        LEFT OUTER JOIN customer_binding b ON
          i.gateway_id = b.beacon_id
          AND (b.namespace = 'ad135bf7eecc08752f96') -- Atollogy gw namespace
          AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
          AND (b.end_time IS NULL OR i.collection_time < b.end_time)
        LEFT OUTER JOIN customer_report_config c ON
          b.facility = c.facility
          AND i.step_name = c.step_name
          AND b.subject_name = c.gateway_name
          AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
          AND (c.end_time IS NULL OR i.collection_time < c.end_time)
          WHERE (:stepFunction::text IS NULL or i.step_function = ANY(:stepFunction))
          AND (:step::text IS NULL or i.step_name = ANY(:step))
          AND (:gw::text IS NULL or i.gateway_id = ANY(:gw))
          AND (:camId::text IS NULL or i.camera_id = :camId)
          AND (:imageStepId::text IS NULL or i.image_step_id = :imageStepId)
          AND (:from::timestamp IS NULL or i.collection_time >= :from)
          AND (:to::timestamp IS NULL or i.collection_time < :to)
          AND (:fnVersion::text IS NULL or i.step_function_version = :fnVersion)
          AND (:cfgVersion::text IS NULL or i.step_config_version = :cfgVersion)
          AND ((i.step_function <> 'lpr' AND i.step_function <> 'ocr')
          OR (i.step_function = 'ocr' AND (
              (:resultType = 'anchorMoved' AND i.step_output ->> 'fullResponse' IS NOT NULL AND i.step_output -> 'anchor_state_list' -> 0 ->> 'anchor_state' = 'moved')
              OR (:resultType = 'anchorStationary' AND i.step_output ->> 'fullResponse' IS NULL AND i.step_output -> 'anchor_state_list' -> 0 ->> 'anchor_state' = 'stationary')
              OR (:resultType = 'withText' AND i.step_output ->> 'fullResponse' IS NOT NULL) OR (:resultType = 'withoutText' AND i.step_output ->> 'fullResponse' is NULL)
            ))
          OR :resultType::text IS NULL
              OR (:resultType = 'lpr' AND i.step_output ->> 'identifier' IS NOT NULL)
              OR (:resultType = 'not-lpr' AND i.step_output ->> 'identifier' IS NULL)
              OR (:resultType = 'fuzzy' AND i.step_output ->> 'identifier' IS NOT NULL
                  AND i.step_output->'qc_img_props'->'fuzzy_matched_lpn'->>'fuzzy_result' IS NOT NULL)
              OR (:resultType = 'side-read'
                AND i.step_output ->> 'identifier' IS NOT NULL
                AND  (
                      c.extract_threshold_definitions->'skew_calibration' IS NOT NULL
                      AND i.step_output->'skew_metrics' IS NOT NULL
                      AND (((c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '<') IS NULL
                              OR (i.step_output->'skew_metrics' ->> (c.extract_threshold_definitions->'skew_calibration'->>'metric')::text)::float
                                > (c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '<')::float)
                            OR ((c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '>') IS NULL
                              OR (i.step_output->'skew_metrics' ->> (c.extract_threshold_definitions->'skew_calibration'->>'metric')::text)::float
                                < (c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '>')::float)
                          )
                  )
              )
              OR (:resultType = 'lpr-skew'
                AND i.step_output ->> 'identifier' IS NOT NULL
                AND  (
                      c.extract_threshold_definitions->'skew_calibration' IS NOT NULL
                      AND i.step_output->'skew_metrics' IS NOT NULL
                      AND (((c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '<') IS NULL
                              OR (i.step_output->'skew_metrics' ->> (c.extract_threshold_definitions->'skew_calibration'->>'metric')::text)::float
                                < (c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '<')::float)
                            AND ((c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '>') IS NULL
                              OR (i.step_output->'skew_metrics' ->> (c.extract_threshold_definitions->'skew_calibration'->>'metric')::text)::float
                                > (c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '>')::float)
                          )
                  )
              )
          )
    ) i
  `;
  try {
    let q = sql(statement)(parameters);
    const {rows} = await db.query(customer, q.text, q.values);
    return rows[0].count;
  } catch (error) {
    throw new Error(`Problems fetching step_name from image_step for db => ${[customer]}. Error is: ${error}.`);
  }
};

module.exports.getImageAnnonations = async function (customer, parameters) {
  const statement = `
    SELECT DISTINCT ON (i.collection_time, i.step_name, i.gateway_id, i.step_function_version, i.step_config_version)
      i.* , latest_qc_ann.image_step_annotation_id, latest_qc_ann.created_at_date, latest_qc_ann.qc_result, latest_qc_ann.comment, i.step_output ->> 'fullResponse' as ocr_full_response
    FROM image_step i
    left join (
      select latest_qc_ann.*
      from (
          select
            max(image_step_annotation_id) as image_step_annotation_id
          from image_step_annotation
          group by
            collection_time, gateway_id, camera_id, step_name,
            step_config_version, step_function_version
      ) as latest_ann_id
      inner JOIN image_step_annotation as latest_qc_ann on latest_ann_id.image_step_annotation_id = latest_qc_ann.image_step_annotation_id
    ) as latest_qc_ann on latest_qc_ann.collection_time = i.collection_time and
      i.gateway_id = latest_qc_ann.gateway_id and
      i.camera_id = latest_qc_ann.camera_id and
      i.step_name = latest_qc_ann.step_name and
      i.step_config_version = latest_qc_ann.step_config_version and
      i.step_function_version = latest_qc_ann.step_function_version
    JOIN unnest(i.images) j
      /*
          "%_sourceimg.jpg" image file name pattern is a workaround
          to allow OCR image showing up in QC Workbench per AT-5731
      */
      ON j LIKE '%_annotated.jpg' OR j LIKE '%_anonymous.jpg' OR j LIKE '%_sourceimg.jpg'
    LEFT OUTER JOIN customer_binding b ON
      i.gateway_id = b.beacon_id
      AND (b.namespace = 'ad135bf7eecc08752f96') -- Atollogy gw namespace
      AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
      AND (b.end_time IS NULL OR i.collection_time < b.end_time)
    LEFT OUTER JOIN customer_report_config c ON
      b.facility = c.facility
      AND i.step_name = c.step_name
      AND b.subject_name = c.gateway_name
      AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
      AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    WHERE (:stepFunction::text IS NULL or i.step_function = ANY(:stepFunction))
      AND (:step::text IS NULL or i.step_name = ANY(:step))
      AND (:gw::text IS NULL or i.gateway_id = ANY(:gw))
      AND (:camId::text IS NULL or i.camera_id = :camId)
      AND (:imageStepId::text IS NULL or i.image_step_id = :imageStepId)
      AND (:from::timestamp IS NULL or i.collection_time >= :from)
      AND (:to::timestamp IS NULL or i.collection_time < :to)
      AND (:fnVersion::text IS NULL or i.step_function_version = :fnVersion)
      AND (:cfgVersion::text IS NULL or i.step_config_version = :cfgVersion)
      AND ((i.step_function <> 'lpr' AND i.step_function <> 'ocr')
          OR (i.step_function = 'ocr' AND (
              (:resultType = 'anchorMoved' AND i.step_output ->> 'fullResponse' IS NOT NULL AND i.step_output -> 'anchor_state_list' -> 0 ->> 'anchor_state' = 'moved')
              OR (:resultType = 'anchorStationary' AND i.step_output ->> 'fullResponse' IS NULL AND i.step_output -> 'anchor_state_list' -> 0 ->> 'anchor_state' = 'stationary')
              OR (:resultType = 'withText' AND i.step_output ->> 'fullResponse' IS NOT NULL) OR (:resultType = 'withoutText' AND i.step_output ->> 'fullResponse' is NULL)
            ))
          OR :resultType::text IS NULL
          OR (:resultType = 'lpr' AND i.step_output ->> 'identifier' IS NOT NULL)
          OR (:resultType = 'not-lpr' AND i.step_output ->> 'identifier' IS NULL)
          OR (:resultType = 'fuzzy' AND i.step_output ->> 'identifier' IS NOT NULL
              AND i.step_output->'qc_img_props'->'fuzzy_matched_lpn'->>'fuzzy_result' IS NOT NULL)
          OR (:resultType = 'side-read'
            AND i.step_output ->> 'identifier' IS NOT NULL
            AND  (
                  c.extract_threshold_definitions->'skew_calibration' IS NOT NULL
                  AND i.step_output->'skew_metrics' IS NOT NULL
                  AND (((c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '<') IS NULL
                          OR (i.step_output->'skew_metrics' ->> (c.extract_threshold_definitions->'skew_calibration'->>'metric')::text)::float
                            > (c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '<')::float)
                        OR ((c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '>') IS NULL
                          OR (i.step_output->'skew_metrics' ->> (c.extract_threshold_definitions->'skew_calibration'->>'metric')::text)::float
                            < (c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '>')::float)
                      )
              )
          )
          OR (:resultType = 'lpr-skew'
            AND i.step_output ->> 'identifier' IS NOT NULL
            AND  (
                  c.extract_threshold_definitions->'skew_calibration' IS NOT NULL
                  AND i.step_output->'skew_metrics' IS NOT NULL
                  AND (((c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '<') IS NULL
                          OR (i.step_output->'skew_metrics' ->> (c.extract_threshold_definitions->'skew_calibration'->>'metric')::text)::float
                            < (c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '<')::float)
                        AND ((c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '>') IS NULL
                          OR (i.step_output->'skew_metrics' ->> (c.extract_threshold_definitions->'skew_calibration'->>'metric')::text)::float
                            > (c.extract_threshold_definitions->'skew_calibration'-> 'bounds' ->> '>')::float)
                      )
              )
          )
      )
  `;
  try {
    let q = sql(statement)(parameters);
    const {rows} = await db.query(customer, q.text, q.values);
    return rows;
  } catch (error) {
    throw new Error(`Problems fetching step_name from image_step for db => ${[customer]}. Error is: ${error}.`);
  }
};

module.exports.setAnnotation = async function (customer, parameters) {
  const statement = `INSERT INTO image_step_annotation(
        collection_time, gateway_id, camera_id, step_name, step_function_version, step_config_version, qc_result, comment, session_id, user_id)
      VALUES (:collection_time, :gateway_id, :camera_id, :step_name, :function_version, :config_version, :qc_result, :comment, :sessionId, :userId);
  `;
  try {
    let q = sql(statement)(parameters);
    const {rows} = await db.query(customer, q.text, q.values, true);
    return rows;
  } catch (error) {
    throw new Error(`Problems storing annotation from image_step_annotation for db => ${[customer]} with paramters ${JSON.stringify(parameters)}. Error is: ${error}.`);
  }
};

module.exports.getAnnotations = async function (customer, parameters) {
  const statement = `
      SELECT qc_result,
             comment,
             created_at_date,
             user_id
      FROM image_step_annotation
      WHERE collection_time = :collectionTime
        AND gateway_id = :gwId
        AND (:camId = '' OR camera_id = :camId)
        AND (:stepName = '' OR step_name = :stepName)
      ORDER BY created_at_date DESC;
  `;
  try {
    let q = sql(statement)(parameters);
    const {rows: annotations} = await db.query(customer, q.text, q.values);
    const {rows: users} = await user.fetchById(annotations.map(x => x.user_id).reduce((agg, val) => {
      if (!agg.includes(val)) {
        agg.push(val);
      }
      return agg;
    }, []));

    const userLookup = users.reduce((agg, val) => {
      if (!agg[val.user_id]) {
        agg[val.user_id] = val;
      }
      return agg;
    }, {});

    var result = annotations.map(annotation => _.merge(annotation, userLookup[annotation.user_id]));
    return result;
  } catch (error) {
    throw new Error(`Problems fetching versions from image_step for db => ${[customer]}. Error is: ${error}.`);
  }
};

module.exports.geStepOutPut = async function (customer, parameters) {
  const statement = `
    select image_step_id, step_output from image_step where image_step_id = any(:imageStepIdList)
  `;
  try {
    let q = sql(statement)(parameters);
    const {rows} = await db.query(customer, q.text, q.values);
    return rows;
  } catch (error) {
    throw new Error(`Problems fetching step_name from image_step for db => ${[customer]}. Error is: ${error}.`);
  }
};

module.exports.getGetewayId = async function (customer) {
  const statement = `
    SELECT distinct(i.gateway_id), max(i.images[1]) as s3_url
    FROM image_step i
    WHERE i.insert_time > NOW() - INTERVAL '3 weeks' and i.images <> '{}'
    group by i.gateway_id
  `;
  try {
    let q = sql(statement)();
    const {rows} = await db.query(customer, q.text, q.values);
    return rows;
  } catch (error) {
    console.log();
    throw new Error(`Problems fetching step_name from image_step for db => ${[customer]}. Error is: ${error}.`);
  }
};

module.exports.getAnnotationStats = async function (customer) {
  const statement = annotationStats + 'SELECT * FROM stats';
  const {rows} = await db.query(customer, statement, []);
  return rows;
};


module.exports.stepKeysObjectDetection = async function (customer, parameters) {
  const statement = `
  SELECT r.*, count(*) OVER() AS total
  FROM (
    SELECT DISTINCT ON (i.collection_time, i.step_name, i.gateway_id, i.step_function_version, i.step_config_version)
      i.gateway_id,
      coalesce(b.subject_name, i.gateway_id) as gateway_name,
      i.camera_id,
      i.collection_time,
      i.step_name,
      i.step_function,
      i.step_function_version,
      i.step_config_version,
      i.images,
      i.image_step_id,
      i.step_output,
      (case
          when i.images [1] like '%image_readings%'
               then split_part(split_part(i.images [1], '/', 12), '_', 3)
          else split_part(split_part(i.images [1], '/', 13), '_', 3)
      end) collection_time_str
    FROM image_step i
    JOIN unnest(i.images) j
      ON j LIKE '%_annotated.jpg' OR j LIKE '%_anonymous.jpg' OR j LIKE '%_sourceimg.jpg'
    LEFT OUTER JOIN customer_binding b ON
      i.gateway_id = b.beacon_id
      AND (b.namespace = 'ad135bf7eecc08752f96') -- Atollogy gw namespace
      AND (b.start_time IS NULL OR i.collection_time >= b.start_time)
      AND (b.end_time IS NULL OR i.collection_time < b.end_time)
    LEFT OUTER JOIN customer_report_config c ON
      b.facility = c.facility
      AND i.step_name = c.step_name
      AND b.subject_name = c.gateway_name
      AND (c.start_time IS NULL OR i.collection_time >= c.start_time)
      AND (c.end_time IS NULL OR i.collection_time < c.end_time)
    WHERE (:stepFunction::text IS NULL or i.step_function = ANY(:stepFunction))
      AND (:from::timestamp IS NULL or i.collection_time >= :from)
      AND (:to::timestamp IS NULL or i.collection_time < :to)
      AND (:step::text IS NULL or i.step_name = ANY(:step))
      AND (:gw::text IS NULL or i.gateway_id = ANY(:gw))
      AND (:camId::text IS NULL or i.camera_id = :camId)
      AND (:imageStepId::text IS NULL or i.image_step_id = :imageStepId)
      AND EXISTS (
        select * from (
          select array_agg(r.detected_object_class ->> 'text') as arrays
          from (
            select * from json_to_recordset(step_output -> 'object_group_list')
            as obj_rec(detected_object_class JSON, detected_object_list JSON)
              where json_array_length(detected_object_list) > 0
              and detected_object_class ->> 'text' is not null
          ) r
        ) re
        where (:resultType::text is null  or re.arrays @> :resultType)
      )
    ORDER BY i.collection_time
  ) r
  LIMIT :pageSize OFFSET :offset;
  `;
  try {
    let q = sql(statement)(parameters);
    const {rows} = await db.query(customer, q.text, q.values);
    return {stepKeys: rows};
  } catch (error) {
    throw new Error(`Problems fetching step_name from image_step for db => ${[customer]}. Error is: ${error}.`);
  }
};

module.exports.getDataRawImage = async function (customer, parameters) {
  const statement = `
    select i.*,
           (case
              when i.images [1] like '%image_readings%'
                   then split_part(split_part(i.images [1], '/', 12), '_', 3)
              else split_part(split_part(i.images [1], '/', 13), '_', 3)
          end) collection_time_str
    from image_step i
    where i.gateway_id = :gwId and
          i.collection_time = :collectionTime and
          i.images[1] like '%image_readings%.jpg'
  `;
  try {
    let q = sql(statement)(parameters);
    const {rows} = await db.query(customer, q.text, q.values);
    return rows && rows.length > 0 ? rows[0] : {};
  } catch (error) {
    throw new Error(`Problems fetching step_name from image_step for db => ${[customer]}. Error is: ${error}.`);
  }
};

module.exports.getCameraStatus = async function (customer, parameters) {
  const statement = `
    select * from image_step
    where  gateway_id = :gw
      and step_output ->> 'bd_factor' is not null and step_output ->> 'is_blurry_or_dirty' is not null
    order by collection_time desc
    limit 1
  `;
  try {
    let q = sql(statement)(parameters);
    const {rows} = await db.query(customer, q.text, q.values);
    return rows && rows.length > 0 ? rows[0] : {};
  } catch (error) {
    throw new Error(`Problems fetching step_name from image_step for db => ${[customer]}. Error is: ${error}.`);
  }
};

module.exports.getCameraImages = async function (customer, parameters) {
  const statement = `
    select i.*,
           (case
               when i.images [1] like '%image_readings%'
                   then split_part(split_part(i.images [1], '/', 12), '_', 3)
               else split_part(split_part(i.images [1], '/', 13), '_', 3)
            end) collection_time_str
    from image_step i
    where (:gw::text IS NULL or i.gateway_id = :gw)
      AND i.images[1] like '%_annotated.jpg'
      AND (:from::timestamp IS NULL or i.collection_time >= :from)
      AND (:to::timestamp IS NULL or i.collection_time < :to)
    order by i.collection_time desc
    limit :pageSize offset :offset;
  `;
  try {
    let q = sql(statement)(parameters);
    const { rows } = await db.query(customer, q.text, q.values);
    return rows && rows.length > 0 ? rows : [];
  } catch (error) {
    throw new Error(`Problems fetching step_name from image_step for db => ${[customer]}. Error is: ${error}.`);
  }
};

module.exports.getDateList = async function (customer, parameters) {
  const statement = `
    with dates as (
      select distinct i.collection_time::date as date
      from image_step i
      where (:gw::text IS NULL or i.gateway_id = :gw) and i.images[1] like '%_annotated.jpg'
    )
    select *
    from dates
    order by date desc
  `;
  try {
    let q = sql(statement)(parameters);
    const { rows } = await db.query(customer, q.text, q.values);
    return rows && rows.length > 0 ? rows.map(i => i.date) : [];
  } catch (error) {
    throw new Error(`Problems fetching step_name from image_step for db => ${[customer]}. Error is: ${error}.`);
  }
};