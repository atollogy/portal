const db = require('../db');
const fs = require('fs');
const path = require('path');
const debug = require('debug')('portal:shifts');
const { pg: PostgreSQLBuilder } = require('yesql');
const customerReportList = fs.readFileSync(path.join(__dirname, 'query', 'subjectReportsList.sql'), 'utf8');
const shiftsList = fs.readFileSync(path.join(__dirname, 'query', 'shiftsList.sql'), 'utf8');

module.exports.reportList = async function(customer, facility) {
  const reportList = customerReportList;
  var {rows} = await db.query(customer, reportList, [facility]);
  return rows;
};


const IMAGE_INTERVAL = `
SELECT min(start_offset) AS start_offset,
       max(ending_offset) AS ending_offset,
       sum(total_records) AS total_records
FROM (
  SELECT min(image_step_id) AS start_offset,
         max(image_step_id) AS ending_offset,
         count(*) AS total_records
    FROM image_step
   WHERE collection_time >= $1::timestamp
     AND collection_time <= $2::timestamp
     AND image_step_id > $3::bigint
   GROUP BY collection_time::date
) as bounds;`;

module.exports.fetchImageInterval = async function fetchImageInterval(customer, theStartDate, theEndDate, theImageStepOffset = 0) {
  const records = [];
  try {
    const { rows } = await db.query(customer, IMAGE_INTERVAL, [theStartDate, theEndDate, theImageStepOffset]);


    if (rows && rows.length) {
      rows.forEach(record => records.push(record));
    } else {
      throw new Error('No records found!!!');
    }
  } catch(cause) {
    console.error('Unable to get image step interval', cause);
    records.push({ start_offset: 0, ending_offset: Number.MAX_SAFE_INTEGER });
  }

  return records.map(({ start_offset, ending_offset }) => Object.assign({
    startOffset: start_offset,
    endingOffset: ending_offset
  })).shift();
};

module.exports.getShifts = async function getShifts(customer, date, facility) {
  const theQueryStatement = PostgreSQLBuilder(shiftsList, { useNullForMissing: true });
  const theQueryContext = theQueryStatement({ facility, date });
  const { rows } = await db.query(customer, (theQueryContext.sql || theQueryContext.text), theQueryContext.values);

  const shifts = rows.map((record) => Object.freeze({
    'name': record.shift_name,
    'start': record.shift_start,
    'end': record.shift_end,
    'duration': record.shift_duration,
    'timezone': record.shift_timezone
  }));
  debug(`Adding ${JSON.stringify(shifts, null, 2)} to ${customer}:${facility}:${date}`);
  return shifts;
};
