const fs = require('fs');
var path = require('path');
const sql = require('yesql').pg;
const db = require('../db');
const moment = require('moment');
const beaconTrackingQuery = fs.readFileSync(path.join(__dirname, 'query', 'beaconTracking.sql'), 'utf8');
const debug = require('debug')('portal:beacons');

const queryBase = fs.readFileSync(path.join(__dirname, 'query', 'base.sql'), 'utf8');
const beaconListQuery = fs.readFileSync(path.join(__dirname, 'query', 'beaconList.sql'), 'utf8');

async function beaconObservations(customer, queryParams) {
  var infoQuery = 'SELECT subject, subject_name, rssi_smoothing_window_slots lookback_slots FROM customer_binding WHERE namespace = :namespace AND beacon_id = :beaconId AND (start_time IS NULL OR now() >= start_time) AND (end_time IS NULL OR now() < end_time)';
  var dataQuery = queryBase + " SELECT s.gateway_id, b.facility || ': ' || b.subject_name gateway_name, s.distance, s.slot, s.reading_count from smoothed s LEFT JOIN customer_binding b ON b.namespace = 'ad135bf7eecc08752f96' AND b.beacon_id = s.gateway_id AND (b.start_time IS NULL OR s.slot >= b.start_time) AND (b.end_time IS NULL OR s.slot < b.end_time) WHERE s.rssi IS NOT NULL AND s.namespace = :namespace AND s.beacon_id = :beaconId AND slot >= :from AND slot < :to ORDER BY slot ASC";

  const infoQ = sql(infoQuery)(queryParams);
  const {rows: infoRows} = await db.query(customer, infoQ.text, infoQ.values);
  queryParams.lookback_slots = infoRows[0] && infoRows[0].lookback_slots; //Supply lookback_slots param to query for beacon moving avg. window

  const dataQ = sql(dataQuery)(queryParams);
  const {rows: dataRows} = await db.query(customer, dataQ.text, dataQ.values);

  var gatewayData = {};
  dataRows.forEach((element) => {
    if (!gatewayData[element.gateway_id]) {
      gatewayData[element.gateway_id] = {
        gatewayId: element.gateway_id,
        name: element.gateway_name || element.gateway_id,
        observations: []
      };
    }
    if (element.distance) {
      gatewayData[element.gateway_id].observations.push({
        t: element.slot.getTime() / 1000,
        d: Math.round(element.distance * 1000) / 1000,
        c: element.reading_count
      });
    }
  });

  return {
    namespace: queryParams.namespace,
    beaconId: queryParams.beaconId,
    subject: infoRows[0] && infoRows[0].subject,
    name: infoRows[0] && infoRows[0].subject_name,
    gateways: Object.values(gatewayData)
  };
}

async function beaconObservationsForGateway(customer, gatewayId) {
  var infoQuery = "SELECT subject, subject_name, rssi_smoothing_window_slots lookback_slots FROM customer_binding WHERE namespace = 'ad135bf7eecc08752f96' AND beacon_id = :gatewayId AND (start_time IS NULL OR now() >= start_time) AND (end_time IS NULL OR now() < end_time)";
  var dataQuery = queryBase + " SELECT s.namespace, s.beacon_id, b.subject, b.subject_name beacon_name, s.distance, s.slot FROM smoothed s LEFT JOIN customer_binding b ON b.namespace = s.namespace AND b.beacon_id = s.beacon_id AND (b.start_time IS NULL OR s.slot >= b.start_time) AND (b.end_time IS NULL OR s.slot < b.end_time) WHERE s.rssi IS NOT NULL AND gateway_id = :gatewayId AND slot >= now() - INTERVAL '1 day' ORDER BY slot ASC";

  let queryParams = {gatewayId: gatewayId};
  const infoQ = sql(infoQuery)(queryParams);
  const {rows: infoRows} = await db.query(customer, infoQ.text, infoQ.values);
  queryParams.lookback_slots = infoRows[0] && infoRows[0].lookback_slots; //Supply lookback_slots param to query for beacon moving avg. window

  const dataQ = sql(dataQuery)(queryParams);
  const {rows: dataRows} = await db.query(customer, dataQ.text, dataQ.values);

  var beaconData = {};
  dataRows.forEach((element) => {
    var key = element.namespace + '|' + element.beacon_id + '|' + element.subject + '|' + element.subject_name;
    if (!beaconData[key]) {
      beaconData[key] = {
        namespace: element.namespace,
        id: element.beacon_id,
        subject: element.subject || undefined,
        name: element.beacon_name || undefined,
        observations: []
      };
    }
    if (element.distance) {
      beaconData[key].observations.push({
        t: element.slot.getTime() / 1000,
        d: Math.round(element.distance * 1000) / 1000
      });
    }
  });

  return {
    gatewayId: gatewayId,
    name: infoRows[0] && infoRows[0].subject_name,
    beacons: Object.values(beaconData)
  };
}

async function winners(customer, queryParams) {
  var conditions = [];
  if (queryParams.namespace) {
    conditions.push('namespace = :namespace');
  }
  if (queryParams.beaconId) {
    conditions.push('beacon_id = :beaconId');
  }

  var dataQuery = queryBase + ' SELECT * FROM beacon_event' + (conditions.length ? (' WHERE ' + conditions.join(' AND ')) : '');

  //First collect lookback_slots param
  const bindings = (await db.query(customer, `SELECT DISTINCT rssi_smoothing_window_slots lookback_slots FROM customer_binding`, [])).rows;
  queryParams.lookback_slots = bindings && bindings[0] && bindings[0].lookback_slots;

  //Supply this param to query to apply lookback_slots for beacon moving avg. window
  const q = sql(dataQuery)(queryParams);
  const {rows: dataRows} = await db.query(customer, q.text, q.values);

  var result = {};

  dataRows.forEach(row => {
    var key = row.namespace + '|' + row.beacon_id + '|' + row.subject + '|' + row.beacon_name;
    if (!result[key]) {
      result[key] = {
        namespace: row.namespace,
        beaconId: row.beacon_id,
        subject: row.subject || undefined,
        name: row.beacon_name == row.beacon_id ? undefined : row.beacon_name,
        events: []
      };
    }
    result[key].events.push({
      startTime: row.start_time.getTime()/1000,
      endTime: row.end_time.getTime()/1000,
      gatewayId: row.gateway_id,
      gatewayName: row.gateway_name == row.gateway_id ? undefined : row.gateway_name,
      gatewayFacility: row.gateway_facility
    });
  });

  return Object.values(result);
}

async function activeBeaconList(customer) {
  return (await db.query(customer, beaconListQuery)).rows.map(row => {
    //get rid of nulls in output
    for (var key in row) {
      if (row[key] == null) {
        delete row[key];
      }
    }
    return row;
  });
}

async function beaconTracking(customer, beaconParams) {
  let qry = `
    ${beaconTrackingQuery}
    SELECT a.*
     FROM winner a
    INNER JOIN last_sighting b ON a.slot = b.slot AND a.beacon_binding = b.beacon_binding
    ORDER BY a.slot DESC;
  `;

  let zoneQry = `
    WITH beacon_gateway_info AS (
      SELECT t.state_name, t.start_cycle, t.end_cycle, t.priority, b.subject_name, b.beacon_id gateway_id, 'beacon'::text as source
        FROM customer_beacon_transition_event_binding t
        JOIN customer_binding b ON t.zone_name = b.zone_name AND t.facility = b.facility
    )
    SELECT state_name, start_cycle, end_cycle, null subject_name, gateway_id, priority, 'event'::text as source
      FROM customer_transition_event_binding
    WHERE facility = :facility AND (end_time IS NULL OR :to <= end_time)
      UNION
    SELECT *
      FROM beacon_gateway_info
      ORDER BY state_name;
  `;

  //First collect lookback_slots param
  const bindings = (await db.query(customer, `SELECT DISTINCT rssi_smoothing_window_slots lookback_slots FROM customer_binding`, [])).rows;
  beaconParams.lookback_slots = bindings && bindings[0] && bindings[0].lookback_slots;

  //Supply this param to query to apply lookback_slots for beacon moving avg. window
  const q = sql(qry)(beaconParams);
  let {rows} = await db.query(customer, q.text, q.values);
  let zoneQ = sql(zoneQry)(beaconParams);
  let {rows: zoneRows} = await db.query(customer, zoneQ.text, zoneQ.values);
  let endState = {}, startState = {}, zoneObj = {};
  zoneRows = zoneRows.filter(x => x.state_name !== 'composite');
  zoneRows.forEach(z => {
    if (z.start_cycle) {
      if (!startState[z.state_name]) {
        startState[z.state_name] = [];
      }
      startState[z.state_name].push(z.gateway_id);
    } else if (z.end_cycle) {
      if (!endState[z.state_name]) {
        endState[z.state_name] = [];
      }
      endState[z.state_name].push(z.gateway_id);
    } else {
      if (!zoneObj[z.state_name]) {
        zoneObj[z.state_name] = [];
      }
      zoneObj[z.state_name].push(z.gateway_id);
    }
  });
  let zoneArr = Object.keys(startState).concat(Object.keys(zoneObj)).concat(Object.keys(endState).reverse());
  zoneObj = Object.assign({}, zoneObj, startState, endState);
  return {
    data: rows,
    allZones: zoneArr,
    gatewayZoneMap: zoneObj
  };
}

async function beaconHistoryService(customer, beaconHistory) {
  let qry = `${beaconTrackingQuery} 
  SELECT  beacon_name, gateway_name, slot FROM winner
  WHERE beacon_name = :beaconName
  ORDER BY slot DESC;`;

  //First collect lookback_slots param
  const bindings = (await db.query(customer, `SELECT DISTINCT rssi_smoothing_window_slots lookback_slots FROM customer_binding`, [])).rows;

  //Supply this param to query to apply lookback_slots for beacon moving avg. window
  beaconHistory.lookback_slots = bindings && bindings[0] && bindings[0].lookback_slots;

  const q = sql(qry)(beaconHistory);
  let {rows: history} = await db.query(customer, q.text, q.values);
  // let events = _.orderBy(history, [ 'beacon_name', 'gateway_name', 'slot'], ['desc']);
  let intervals = await coaleseEvents(history);
  return intervals;
}

function coaleseEvents(events) {
  let intervals = [];
  events.forEach((x, i) => { // coalesce events into intervals
    if (x.gateway_name === 'Line 23') {
      debug('stop');
    }
    //Push very first event into intervals as it is with its own start/end time and state
    if (i == 0) {
      intervals.push(Object.assign(x, {
        start_time: x.slot,
        end_time: moment(x.slot).add(60, 's').toDate()
      }));

      //If state of current interval is the same as that of previous one AND then current event is not too far from previous event (within 1 collection interval) then group them together
    } else if (x.beacon_name == events[i - 1].beacon_name && x.gateway_name == events[i - 1].gateway_name && moment(events[i - 1].slot).diff(x.slot) <= 60 * 1000) {
      let temp = Object.assign(x, {
        start_time: events[i - 1].start_time,
        end_time: moment(x.slot).add(60, 's').toDate()
        // images: events[i - 1].images ? events[i - 1].images.concat(x.images) : x.images
      });
      //Remove previous event and update it with current events end_time and previous events start_time
      intervals.pop();
      intervals.push(temp);

      //else dont group them together
    } else {
      let temp = Object.assign(x, {
        start_time: x.slot,
        end_time: moment(x.slot).add(60, 's').toDate()
      });
      intervals.push(temp);
    }
  });

  return intervals;
}

module.exports = {
  activeBeaconList: activeBeaconList,
  beaconObservations: beaconObservations,
  beaconObservationsForGateway: beaconObservationsForGateway,
  winners: winners,
  beaconTracking: beaconTracking,
  beaconHistoryService: beaconHistoryService
};
