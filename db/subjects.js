const fs = require('fs');
const path = require('path');
const db = require('../db');
const _ = require('lodash');
const moment = require('moment');
const sql = require('yesql').pg;
const subjectReportQuery = fs.readFileSync(path.join(__dirname, 'query', 'subjectReport.sql'), 'utf8');
const reportConfigQuery = fs.readFileSync(path.join(__dirname, 'query', 'reportConfig.sql'), 'utf8');
const { Cache } = require('../lib/cache');
const cache = new Cache(5 * 60);
const debug = require('debug')('portal:subjects');
const { newMissingDataHandler } = require('../lib/missing-data');
const shifts = require('../db/shifts');

const portalHelper = require('../lib/portalHelper');

const reportCfgAPIs = require('../db/reportConfig.js');
const FSM = require('../lib/state-machine/ground-ops').fsm;
const sortAlpha = (elements) => elements.sort((left, right) => left.localeCompare(right));

const FILTER_MODE_TYPES = ['exclusion', 'marker'];

const mode = (elements) => elements.reduce((current, proposed, index, values) => {
  const countOfCurrent = values.filter(item => item === current).length;
  const countOfProposed = values.filter(item => item === proposed).length;
  return countOfCurrent >= countOfProposed ? current : proposed;
}, /* initial value */ null);

const userDB = require('../lib/user-management');
const { Observation, Filters }  = require('../lib/customer-filter');
const { withMeasureName, withSubjectName, withGatewayId } = Filters;
const { filterByBlacklistRules } = require('../lib/utils/filter');

const { hasOwnProperty } = {};
const hasPropertyValue = (sourceObject, propertyName) => {
  return hasOwnProperty.call(sourceObject, propertyName) && null != sourceObject[propertyName];
};

function searchConfig(event, config) {
  return config.filter(cfg => {
    return cfg.facility === event.facility && cfg.subject_name === event.subject_name && cfg.measure_name === event.measure_name;
  });
}

function smoothedState(currIndex, eventRows, cfg) {
  let lookBack = cfg && (cfg.smoothing_look_back_slots ? Number(cfg.smoothing_look_back_slots) : cfg.smoothing_look_back_slots);
  let lookAhead = cfg && (cfg.smoothing_look_ahead_slots ? Number(cfg.smoothing_look_ahead_slots) : cfg.smoothing_look_ahead_slots);

  //If lookAhead number of events don't exist in data or the previous event does not exist then return existing state
  if ((lookBack === null && lookAhead === null) || !eventRows[currIndex + lookAhead] || !eventRows[currIndex - lookBack] || currIndex == 0 || !['present', 'absent', 'moved', 'stationary'].includes(eventRows[currIndex].state) || eventRows[currIndex].measure_name === 'composite') {
    return eventRows[currIndex];
  }

  let nextStates = [];
  let nextSubjects = [];
  let nextMeasures = [];
  let prevStates = [];
  let prevSubjects = [];
  let prevMeasures = [];

  //Push lookAhead number of future states into nextVals
  for (let index = 1; index <= lookAhead; index++) {
    if (eventRows[currIndex + index].collection_time.diff(eventRows[currIndex + index - 1].collection_time, 'seconds') <= eventRows[currIndex].collection_interval) {
      nextStates.push(eventRows[currIndex + index].state);
      nextSubjects.push(eventRows[currIndex + index].subject_name);
      nextMeasures.push(eventRows[currIndex + index].measure_name);
    } else {
      nextSubjects.push(null);
      nextMeasures.push(null);
      nextStates.push(null);
    }
  }

  //Push lookBack number of past states into prevVals
  for (let i = 1; i <= lookBack; i++) {
    if (eventRows[currIndex - i + 1].collection_time.diff(eventRows[currIndex - i].collection_time, 'seconds') <= eventRows[currIndex].collection_interval) {
      prevStates.push(eventRows[currIndex - i].state);
      prevSubjects.push(eventRows[currIndex - i].subject_name);
      prevMeasures.push(eventRows[currIndex - i].measure_name);
    } else {
      prevStates.push(null);
      prevSubjects.push(null);
      prevMeasures.push(null);
    }
  }

  //If the given lookAhead number of future states do not belong to the same Subject/Measure as eventRows[currIndex].state
  //then return its original value without changing as we dont know enough to smooth it
  if (!nextSubjects.every(s => s === eventRows[currIndex].subject_name)
    || !nextMeasures.every(m => m === eventRows[currIndex].measure_name)) {
    return eventRows[currIndex];
  }
  if (!prevSubjects.every(s => s === eventRows[currIndex].subject_name)
    || !prevMeasures.every(m => m === eventRows[currIndex].measure_name)) {
    return eventRows[currIndex];
  }

  //Check if lookBack number of events exist and state was present
  const newEvent = eventRows[currIndex];

  if (prevStates.includes('present') && nextStates.includes('present')) {
    newEvent.state = 'present';
  } else if (prevStates.includes('stationary') && nextStates.includes('stationary')) {
    newEvent.state = 'stationary';
  } else if (prevStates.includes('moved') && nextStates.includes('moved')) {
    newEvent.state = 'moved';
  }
  return newEvent;
}

function eventIdentitityCheck(events, current, backOffst, forwardOffst) {
  const currEvent = events[current];
  const extract = events.slice(current - backOffst, current).concat(events.slice(current, current + forwardOffst));
  return extract.length && extract.every(s => s.subject_name === currEvent.subject_name && s.measure_name === currEvent.measure_name);
}

function smoothedStateForGroundOps(currIndex, eventRows, cfg) {
  const currEvent = eventRows[currIndex];
  const lookBack = cfg.smoothing_look_back_slots ? Number(cfg.smoothing_look_back_slots) : cfg.smoothing_look_back_slots;
  const lookAhead = cfg.smoothing_look_ahead_slots ? Number(cfg.smoothing_look_ahead_slots) : cfg.smoothing_look_ahead_slots;

  //Check if previous lookBack number of events and the next lookAhead number of events are "identical" (same subject-measure) to current event
  const identityBool = lookBack && lookAhead && eventIdentitityCheck(eventRows, currIndex, lookBack, lookAhead);

  //If we fail identity check or if the event belongs to the composite measure then return it intact
  if (!identityBool || eventRows[currIndex].measure_name === 'composite') return eventRows[currIndex];

  //Collect next and previous events
  const nextEvents = eventRows.slice(currIndex, currIndex + lookAhead);
  const prevEvents = eventRows.slice(currIndex, lookBack + currIndex - 1);

  //Collect states values form only consecutive events
  const nextStates = nextEvents.reduce((acc, e, i, arr) => (i === arr.length - 1) || arr[i + 1].collection_time.diff(e.collection_time, 'seconds') <= e.collection_interval ? acc.concat(e.state) : acc, []);
  const prevStates = prevEvents.reduce((acc, e, i, arr) => (i === 0) || e.collection_time.diff(arr[i - 1].collection_time, 'seconds') <= e.collection_interval ? acc.concat(e.state) : acc, []);

  //If mode of prevStates and mode of nextStates match then set the current state equal to the mode else keep it intact
  const modePrev = mode(sortAlpha(prevStates));
  const modeNext = mode(sortAlpha(nextStates));
  return {...currEvent, state: modePrev === modeNext ? modePrev : currEvent.state};
}

function smoothOCR(currIndex, eventRows, cfg) {
  let lookBack = cfg.smoothing_look_back_slots ? Number(cfg.smoothing_look_back_slots) : cfg.smoothing_look_back_slots;
  let lookAhead = cfg.smoothing_look_ahead_slots ? Number(cfg.smoothing_look_ahead_slots) : cfg.smoothing_look_ahead_slots;

  //If lookAhead number of events don't exist in data or the previous event does not exist then return existing state
  if (lookBack === null || lookAhead === null || !eventRows[currIndex + lookAhead] || !eventRows[currIndex - lookBack] || currIndex == 0 || eventRows[currIndex].measure_name === 'composite') {
    return eventRows[currIndex];
  }

  let nextStates = [];
  let nextSubjects = [];
  let nextMeasures = [];
  let prevStates = [];
  let prevSubjects = [];
  let prevMeasures = [];

  //Push lookAhead number of future states into nextVals
  for (let i = 1; i <= lookAhead; i++) {
    nextStates.push(eventRows[currIndex + i].state);
    nextSubjects.push(eventRows[currIndex + i].subject_name);
    nextMeasures.push(eventRows[currIndex + i].measure_name);
  }

  //Push lookBack number of past states into prevVals
  for (let i = 1; i <= lookBack; i++) {
    prevStates.push(eventRows[currIndex - i].state);
    prevSubjects.push(eventRows[currIndex - i].subject_name);
    prevMeasures.push(eventRows[currIndex - i].measure_name);
  }

  //If the given lookAhead number of future states do not belong to the same Subject/Measure as eventRows[currIndex].state
  //then return its original value without changing as we dont know enough to smooth it
  if (!nextSubjects.every(s => s === eventRows[currIndex].subject_name)
    || !nextMeasures.every(m => m === eventRows[currIndex].measure_name)) {
    return eventRows[currIndex];
  }
  if (!prevSubjects.every(s => s === eventRows[currIndex].subject_name)
    || !prevMeasures.every(m => m === eventRows[currIndex].measure_name)) {
    return eventRows[currIndex];
  }

  const newEvent = eventRows[currIndex];
  //Check if current state has atleast 3 characters in Job#. If not then use the next/prev state names
  const foundPrev = prevStates.filter(p => p && p.length === 3)[0];
  const foundNext = nextStates.filter(n => n && n.length === 3)[0];
  if (newEvent.state.length < 3) {
    newEvent.state = foundNext || foundPrev;
  }
  return newEvent;
}

function smoothedPresentStateForTia(currIndex, eventRows, cfg) {
  let lookBack = cfg.smoothing_look_back_slots_for_present ? Number(cfg.smoothing_look_back_slots_for_present) : cfg.smoothing_look_back_slots_for_present;
  let lookAhead = cfg.smoothing_look_ahead_slots_for_present ? Number(cfg.smoothing_look_ahead_slots_for_present) : cfg.smoothing_look_ahead_slots_for_present;

  //If lookAhead number of events don't exist in data or the previous event does not exist then return existing state
  if (lookBack === null || lookAhead === null || !['present', 'absent'].includes(eventRows[currIndex].state) || eventRows[currIndex].measure_name === 'composite') {
    return eventRows[currIndex];
  }
  let nextStates = [];
  let nextSubjects = [];
  let nextMeasures = [];
  let prevStates = [];
  let prevSubjects = [];
  let prevMeasures = [];

  if (eventRows[currIndex + lookAhead] && eventRows[currIndex + lookAhead].subject_name === eventRows[currIndex].subject_name) {
    for (let i = 1; i <= lookAhead; i++) {
      if (eventRows[currIndex + i].collection_time.diff(eventRows[currIndex + i - 1].collection_time, 'seconds') <= eventRows[currIndex].collection_interval) {
        nextStates.push(eventRows[currIndex + i].state);
        nextSubjects.push(eventRows[currIndex + i].subject_name);
        nextMeasures.push(eventRows[currIndex + i].measure_name);
      } else {
        nextSubjects.push(null);
        nextMeasures.push(null);
        nextStates.push(null);
      }
    }
    //If the given lookAhead number of future states do not belong to the same Subject/Measure as eventRows[currIndex].state
    //then return its original value without changing as we dont know enough to smooth it
    if (!nextSubjects.every(s => s === eventRows[currIndex].subject_name)
      || !nextMeasures.every(m => m === eventRows[currIndex].measure_name)) {
      return eventRows[currIndex];
    }
  }

  //Push lookBack number of past states into prevVals
  if (eventRows[currIndex - lookBack] && eventRows[currIndex - lookBack].subject_name === eventRows[currIndex].subject_name) {
    for (let i = 1; i <= lookBack; i++) {
      if (eventRows[currIndex - i + 1].collection_time.diff(eventRows[currIndex - i].collection_time, 'seconds') <= eventRows[currIndex].collection_interval) {
        prevStates.push(eventRows[currIndex - i].state);
        prevSubjects.push(eventRows[currIndex - i].subject_name);
        prevMeasures.push(eventRows[currIndex - i].measure_name);
      } else {
        prevStates.push(null);
        prevSubjects.push(null);
        prevMeasures.push(null);
      }
    }
    if (!prevSubjects.every(s => s === eventRows[currIndex].subject_name)
      || !prevMeasures.every(m => m === eventRows[currIndex].measure_name)) {
      return eventRows[currIndex];
    }
  }

  //Check if lookBack number of events exist and state was present
  const newEvent = eventRows[currIndex];
  if (prevStates.includes('present') && nextStates.includes('present')
    || prevStates.length === 0 && nextStates.includes('present')
    || nextStates.length === 0 && prevStates.includes('present')) {
    newEvent.state = 'present';
  }
  return newEvent;
}

function smoothedAbsentStateForTia(currIndex, eventRows, cfg) {
  let lookBack = cfg.smoothing_look_back_slots_for_absent ? Number(cfg.smoothing_look_back_slots_for_absent) : cfg.smoothing_look_back_slots_for_absent;
  let lookAhead = cfg.smoothing_look_ahead_slots_for_absent ? Number(cfg.smoothing_look_ahead_slots_for_absent) : cfg.smoothing_look_ahead_slots_for_absent;

  //If lookAhead number of events don't exist in data or the previous event does not exist then return existing state
  if (lookBack === null || lookAhead === null || !['present', 'absent'].includes(eventRows[currIndex].state) || eventRows[currIndex].measure_name === 'composite') {
    return eventRows[currIndex];
  }

  let nextStates = [];
  let nextSubjects = [];
  let nextMeasures = [];
  let prevStates = [];
  let prevSubjects = [];
  let prevMeasures = [];

  //Push lookAhead number of future states into nextVals
  if (eventRows[currIndex + lookAhead] && eventRows[currIndex + lookAhead].subject_name === eventRows[currIndex].subject_name) {
    for (let i = 1; i <= lookAhead; i++) {
      if (eventRows[currIndex + i].collection_time.diff(eventRows[currIndex + i - 1].collection_time, 'seconds') <= eventRows[currIndex].collection_interval) {
        nextStates.push(eventRows[currIndex + i].state);
        nextSubjects.push(eventRows[currIndex + i].subject_name);
        nextMeasures.push(eventRows[currIndex + i].measure_name);
      } else {
        nextSubjects.push(null);
        nextMeasures.push(null);
        nextStates.push(null);
      }
    }
    //If the given lookAhead number of future states do not belong to the same Subject/Measure as eventRows[currIndex].state
    //then return its original value without changing as we dont know enough to smooth it
    if (!nextSubjects.every(s => s === eventRows[currIndex].subject_name)
      || !nextMeasures.every(m => m === eventRows[currIndex].measure_name)) {
      return eventRows[currIndex];
    }
  }

  //Push lookBack number of past states into prevVals
  if (eventRows[currIndex - lookBack] && eventRows[currIndex - lookBack].subject_name === eventRows[currIndex].subject_name) {
    for (let index = 1; index <= lookBack; index++) {
      if (eventRows[currIndex - index + 1].collection_time.diff(eventRows[currIndex - index].collection_time, 'seconds') <= eventRows[currIndex].collection_interval) {
        prevStates.push(eventRows[currIndex - index].state);
        prevSubjects.push(eventRows[currIndex - index].subject_name);
        prevMeasures.push(eventRows[currIndex - index].measure_name);
      } else {
        prevStates.push(null);
        prevSubjects.push(null);
        prevMeasures.push(null);
      }
    }
    if (!prevSubjects.every(s => s === eventRows[currIndex].subject_name)
      || !prevMeasures.every(m => m === eventRows[currIndex].measure_name)) {
      return eventRows[currIndex];
    }
  }

  //Check if lookBack number of events exist and state was present
  const newEvent = eventRows[currIndex];
  if (prevStates.length === 0 && nextStates.includes('absent')
    || nextStates.length === 0 && prevStates.includes('absent')
    || prevStates.includes('absent') && nextStates.includes('absent')) {
    newEvent.state = 'absent';
  }
  return newEvent;
}

function setStateBasedOnThreshold(event, cfg) {
  try {
    if (event.accuracy >= cfg[event.subject_name].accuracy_threshold) {
      event.state = 'present';
    } else {
      event.state = 'absent';
    }
    return event;
  } catch (e) {
    debug(`setStateBasedOnThreshold swallowed an error on arg: ${event.subject_name}... error is: ${e}.`);
  }
  return;
}

function coaleseEvents(events) {
  let intervals = [];
  events.forEach((theEventObject, index) => { // coalesce events into intervals

    //Push very first event into intervals as it is with its own start/end time and state
    if (index == 0) {
      intervals.push(Object.assign(theEventObject, {
        start_time: theEventObject.collection_time,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate()
      }));

      //If state of current interval is the same as that of previous one AND then current event is not too far from previous event (within 1 collection interval) then group them together
    } else if (theEventObject.subject_name == events[index - 1].subject_name && theEventObject.measure_name == events[index - 1].measure_name && theEventObject.state == events[index - 1].state && moment(theEventObject.collection_time).diff(events[index - 1].collection_time) <= theEventObject.collection_interval * 1000) {
      let temp = Object.assign(theEventObject, {
        start_time: events[index - 1].start_time,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate(),
        images: events[index - 1].images ? [].concat(events[index - 1].images).concat(theEventObject.images) : [].concat(theEventObject.images)
      });
      //Remove previous event and update it with current events end_time and previous events start_time
      intervals.pop();
      if (!temp.centroids || temp.centroids.length === 0) {
        delete temp.centroids;
      }

      if (!temp.display_trapezoid) {
        delete temp.display_trapezoid;
      }

      intervals.push(temp);

      //else dont group them together
    } else {
      let temp = Object.assign(theEventObject, {
        start_time: theEventObject.collection_time,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate()
      });
      intervals.push(temp);
    }
  });

  return intervals;
}

function coaleseEventsWithNoInterval(events) {
  let intervals = [];
  events.forEach((theEventObject, index) => { // coalesce events into intervals

    //Push very first event into intervals as it is with its own start/end time and state
    if (index == 0) {
      intervals.push(Object.assign(theEventObject, {
        start_time: theEventObject.collection_time,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate()
      }));

      //If state of current interval is the same as that of previous one AND then current event is not too far from previous event (within 1 collection interval) then group them together
    } else if (theEventObject.subject_name == events[index - 1].subject_name && theEventObject.measure_name == events[index - 1].measure_name && theEventObject.state == events[index - 1].state) {
      let temp = Object.assign(theEventObject, {
        start_time: events[index - 1].start_time,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate(),
        images: events[index - 1].images ? events[index - 1].images.concat(theEventObject.images) : theEventObject.images
      });
      //Remove previous event and update it with current events end_time and previous events start_time
      intervals.pop();
      intervals.push(temp);

      //else dont group them together
    } else {
      let temp = Object.assign(theEventObject, {
        start_time: theEventObject.collection_time,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate()
      });
      intervals.push(temp);
    }
  });

  return intervals;
}

function coaleseEventsWithDefInterval(events) {
  let intervals = [];
  events.forEach((theEventObject, index) => { // coalesce events into intervals

    //Push very first event into intervals as it is with its own start/end time and state
    if (index == 0) {
      intervals.push(Object.assign(theEventObject, {
        start_time: moment(theEventObject.collection_time).diff(theEventObject.shift_start) === 0 ? theEventObject.collection_time : theEventObject.shift_start,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate()
      }));

      //If state of current interval is the same as that of previous one AND then current event is not too far from previous event (within 1 collection interval) then group them together
    } else if (theEventObject.subject_name == events[index - 1].subject_name && theEventObject.measure_name == events[index - 1].measure_name && theEventObject.state == events[index - 1].state && moment(theEventObject.collection_time).diff(events[index - 1].collection_time) <= 120 * 1000) {
      let temp = Object.assign(theEventObject, {
        start_time: events[index - 1].start_time,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate(),
        images: events[index - 1].images ? events[index - 1].images.concat(theEventObject.images) : theEventObject.images
      });
      //Remove previous event and update it with current events end_time and previous events start_time
      intervals.pop();
      intervals.push(temp);
    } else if (theEventObject.subject_name == events[index - 1].subject_name && theEventObject.measure_name == events[index - 1].measure_name) {
      // If its the same subject and measure, start time of the current event = end time of the previous event in order to remove gaps
      let temp = Object.assign(theEventObject, {
        start_time: events[index - 1].end_time,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate()
      });
      intervals.push(temp);
    } else {
      //else dont group them together
      let temp = Object.assign(theEventObject, {
        start_time: moment(theEventObject.collection_time).diff(theEventObject.shift_start) === 0 ? theEventObject.collection_time : theEventObject.shift_start,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate()
      });

      intervals.push(temp);
    }
  });

  let smootherIntervals = [];
  intervals.forEach((theEventObject, index) => {
    if (index == 0 || index === intervals.length - 1) {
      smootherIntervals.push(theEventObject);
    } else if (intervals[index - 1].step_name === theEventObject.step_name && (moment(theEventObject.end_time).diff(theEventObject.start_time) <= 600000 || intervals[index - 1].state === theEventObject.state)) {
      let temp = Object.assign(theEventObject, {
        start_time: intervals[index - 1].start_time,
        end_time: moment(theEventObject.collection_time).add(theEventObject.collection_interval, 's').toDate(),
        images: intervals[index - 1].images ? intervals[index - 1].images.concat(theEventObject.images) : theEventObject.images,
        state: intervals[index - 1].state
      });
      //Remove previous event and update it with current events end_time and previous events start_time
      smootherIntervals.pop();
      smootherIntervals.push(temp);
    } else {
      smootherIntervals.push(theEventObject);
    }
  });

  return smootherIntervals;
}

const EventObject = function() {};

const NullControl = Object.freeze({
  withState: () => false,
  withLastState: () => false
});

const EventControl = Object.freeze({
  helper: (states) => {
    const listOfStates = Array.isArray(states) ? [].concat(states) : [ states ];
    return Object.freeze({
      withState: (state) => listOfStates.some((element) => element.state === state),
      withLastState: (state) => listOfStates.length && listOfStates[listOfStates.length - 1] && state === listOfStates[listOfStates.length - 1].state
    });
  },
  none: () => NullControl
});

EventObject.prototype.getMeasures = function () {
  const mapOfMeasures = this.states.reduce((measuresAggregate, { measure }) => {
    measuresAggregate[measure] = 1;
    return measuresAggregate;
  }, {});
  return Object.keys(mapOfMeasures);
};

EventObject.prototype.getStates = function () {
  const mapOfStates = this.states.reduce((stateAggregate, { state }) => {
    stateAggregate[state] = 1;
    return stateAggregate;
  }, {});
  return Object.keys(mapOfStates);
};

EventObject.prototype.hasMeasure = function (measure) {
  const statesOfInterest = this.states.filter((element) => element.measure === measure);
  return statesOfInterest && statesOfInterest.length ? EventControl.helper(statesOfInterest) : EventControl.none();
};

EventObject.prototype.getImagesFor = function (measure, state) {
  const images = [];
  this.states.filter(
    (element) => element.state === state && element.measure === measure
  ).reduce((listOfImages, stateObject) => {
    stateObject.images.forEach((element) => listOfImages.push(element));
    return listOfImages;
  }, images);
  return images;
};

EventObject.fromObject = (theSourceObject, theInstanceObject) => {
  if (theSourceObject) {
    theInstanceObject = theInstanceObject || new EventObject();
    Object.keys(theSourceObject).forEach((property) => {
      const value = theSourceObject[property];
      theInstanceObject[property] = value;
    });
  }
  return theInstanceObject;
};

/**
 * @param duration {Number} Missing gaps fill duration in minutes
 */
function fillGapsofNMinutes(smoothenedIntervals, duration) {
  for (let index = 0; index < smoothenedIntervals.length - 1; index++) {
    const nextIndex = index + 1;
    if (smoothenedIntervals[index].subject_name === smoothenedIntervals[nextIndex].subject_name &&
      smoothenedIntervals[index].measure_name === smoothenedIntervals[nextIndex].measure_name &&
      smoothenedIntervals[nextIndex].start_time.toDate() > smoothenedIntervals[index].end_time &&
      smoothenedIntervals[nextIndex].start_time.toDate() <= moment(smoothenedIntervals[index].end_time).add(duration, 'minutes').toDate()
    ) {
      if (smoothenedIntervals[index].state !== smoothenedIntervals[nextIndex].state) {
        smoothenedIntervals[index].end_time = smoothenedIntervals[nextIndex].start_time;
      } else {
        smoothenedIntervals[nextIndex].start_time = smoothenedIntervals[index].start_time;
        smoothenedIntervals[nextIndex].images =
        smoothenedIntervals[index].images.concat(smoothenedIntervals[nextIndex].images);
        smoothenedIntervals.splice(index, 1);
        --index;
      }
    }
  }
  return smoothenedIntervals;
}
function grpConcurrentEvents(listOfEvents, compositeCollectionInterval=null) {
  const listOfConcurrentEvents = listOfEvents.reduce((mapOfEvents, theEventObject) => {
    const key = theEventObject.facility + '-' + theEventObject.subject_name + '-' + theEventObject.collection_time;
    const theConcurrentEventObject = mapOfEvents.get(key) || Object.assign({}, theEventObject, {
      measure_name: 'composite',
      state: '',
      states: [],
      concurrentStates: {},
      concurrentImages: {},
      concurrentBbox: {},
      _created: Date.now()
    });

    if (compositeCollectionInterval && theEventObject.subject_name in compositeCollectionInterval) {
      theConcurrentEventObject.collection_interval = compositeCollectionInterval[theEventObject.subject_name];
    }
    const theMeasureName = theEventObject.measure_name;
    if (Object.prototype.hasOwnProperty.call(theEventObject, 'collection_interval') &&
      !Object.prototype.hasOwnProperty.call(theConcurrentEventObject, 'collection_interval')) {
      theConcurrentEventObject.collection_interval = theEventObject.collection_interval;
    }

    theConcurrentEventObject.states.push(Object.freeze({
      measure: theEventObject.measure_name,
      state: theEventObject.state,
      images: theEventObject.images
    }));

    theConcurrentEventObject.concurrentStates[theMeasureName] = theEventObject.state;
    theConcurrentEventObject.concurrentImages[theMeasureName] = theEventObject.images;
    theConcurrentEventObject.concurrentBbox[theMeasureName] = theEventObject.bounding_box;
    theEventObject._parent = theConcurrentEventObject;

    theConcurrentEventObject._updated = Date.now();

    return mapOfEvents.set(key, theConcurrentEventObject);
  }, new Map());

  const theConcurrentEvents = [...listOfConcurrentEvents.values()].map(element => EventObject.fromObject(element));

  return theConcurrentEvents;
}

// This function relies on only current (active) composite definitions being sent in config
// This function enumerates the composite components across all subject-measures and does not do this per subject/measure:
// so, if we have certain measures that part of the composite definition for some subjects but not for others then this function
// will NOT remove them for the subjects that need them to be removed
function keepOnlyCompositeComponents(events, config) {
  const servicing_measures = new Set();
  config.forEach((configEntry) => {
    if (configEntry.measure_name === 'composite' && configEntry.composite_definition) {
      if (configEntry.cs_config && configEntry.cs_config.servicing_measures) {
        configEntry.cs_config.servicing_measures.forEach(item => servicing_measures.add(item));
      }
      configEntry.composite_definition.forEach((compositeDefinition) => {
        Object.values(compositeDefinition).forEach(measure => {
          const keys = Object.keys(measure);
          keys.filter(
            (measureName) => measure[measureName] !== ':optional'
          ).forEach(
            (measureName) => servicing_measures.add(measureName)
          );
        });
      });
    }
  });

  return events.filter((element) => servicing_measures.has(element.measure_name));
}

function calculateComposite(groups, config) {
  groups.forEach((configGroup) => {
    let matchEach = [];
    let missingMeasures = [];
    let compositeConfig = searchConfig(configGroup, config)[0];

    const compositeImages = {};
    if (compositeConfig && compositeConfig.composite_definition && compositeConfig.composite_definition.length) {
      compositeConfig.composite_definition.forEach((compositeElementConfig) => {

        Object.keys(compositeElementConfig).forEach((stateName) => {
          const compositeStateConfig = compositeElementConfig[stateName];
          if(stateName !== 'image_source') {
            let match = true;
            let altImgSource = null;
            Object.keys(compositeStateConfig).forEach((measureName) => {
              const expectedMeasureState = compositeStateConfig[measureName];

              const measureControlObject = configGroup.hasMeasure(measureName);
              match = match && (
                compositeStateConfig[measureName] === ':optional' ||
                measureControlObject.withState(expectedMeasureState));
              if (measureControlObject.withState(expectedMeasureState)) {
                const listOfImages = compositeImages[measureName] ? [...compositeImages[measureName]] : [];
                const imagesOfInterest = configGroup.getImagesFor(measureName, expectedMeasureState);
                imagesOfInterest.map(
                  (imageObject) => Object.assign({}, imageObject, { measure: measureName })
                ).forEach(
                  (imageObject) => listOfImages.push(imageObject)
                );
                if(!Object.keys(compositeImages).includes(measureName)) {
                  compositeImages[measureName] = listOfImages;
                }
              }

              if (compositeStateConfig[measureName] !== ':optional' && (
                !(measureName in configGroup.concurrentStates) && !missingMeasures.includes(measureName)
              )) {
                missingMeasures.push(measureName);
              } else {
                altImgSource = compositeElementConfig['image_source'] || null;
              }
            });
            if (match) {
              matchEach.push({ state: stateName, imgSource: altImgSource || null });
            }
          }
        });
      });
      configGroup.state = matchEach.length ? matchEach[0].state : missingMeasures.length ? 'Missing Data-Some' : 'NA';
      configGroup.missingMeasures = missingMeasures;
      if (Object.keys(compositeImages).length) {
        configGroup.concurrentImages = compositeImages;
      }
      configGroup.images = Object.keys(configGroup.concurrentImages).map(
        (measure) => configGroup.concurrentImages[measure].map((imageObj) => Object.assign({}, imageObj, { measure }))
      ).reduce((accumulator, images) => accumulator.concat(images), []);
    } else {
      configGroup.state = 'NA';
      configGroup.missingCompositeDef = true;
    }
  });

  //Temporary way of removing rows where composite definition did not exist in config
  return groups.filter(g => !g.missingCompositeDef);
}

function applyFacilityFilter(facility, data) {
  const filtered = data.filter(record => record.facility === facility);
  return filtered;
}

function isCgrGroundOps(customer) {
  return ['tia', 'cvgairport', 'gatwickairport'].some((x) => x === customer);
}

//Servicing condition : if any of the servicing class defined in servicingDef is present
function ifStartServicing(e, servicingDef) {
  let servicingFlag = false;
  servicingDef.forEach(x => {
    servicingFlag = servicingFlag || e.hasMeasure(x).withState('present');
  });
  return servicingFlag;
}

//Serviced condition : if all the servicing class defined in servicingDef is absent
function ifDoneServicing(e, servicingDef) {
  let servicingFlag = true;
  servicingDef.forEach(x => {
    servicingFlag = servicingFlag && !e.hasMeasure(x).withLastState('absent');
  });
  return servicingFlag;
}

function calculateCompositeForGroundOps(intervals, config) {
  let startIndex = -1, endIndex = -1;
  let turnStartTime = '', turnEndTime = '';
  let gsm = new FSM();
  intervals.forEach((x, i) => {
    let cfg = searchConfig(x, config)[0];
    let servicingDef = cfg.cs_config.servicing_measures || [];
    x.turn = {};
    if (i > 0 && intervals[i - 1].subject_name !== x.subject_name) {
      gsm = new FSM();
    }
    if (moment(x.collection_time).diff(x.shift_start, 'minutes') === 0 && ifStartServicing(x, servicingDef)) {
      startIndex = i;
      turnStartTime = x.collection_time;
      gsm.goto('running');
    } else if (moment(x.collection_time).diff(x.shift_start, 'minutes') === 0
        && x.hasMeasure('aircraft').withState('present')
        && x.hasMeasure('jetbridge').withState('present')) {
      startIndex = i;
      turnStartTime = x.collection_time;
      gsm.goto('serviced');
    } else if (gsm.is('idle') && x.hasMeasure('aircraft').withState('present')) {
      gsm.planeArrived();
      startIndex = i;
      turnStartTime = x.collection_time;
    } else if (gsm.is('arrived') && x.hasMeasure('jetbridge').withState('present')) {
      gsm.startServicing();
    } else if (gsm.is('arrived') && !x.hasMeasure('aircraft').withLastState('absent') || gsm.is('leaving') && !x.hasMeasure('aircraft').withLastState('absent')) {
      gsm.planeLeft();
      endIndex = i;
      turnEndTime = x.collection_time;
    } else if (gsm.is('running') && ifStartServicing(x, servicingDef)) {
      gsm.startServicing();
    } else if ((gsm.is('running') && ifDoneServicing(x, servicingDef)) || (gsm.is('leaving') && x.hasMeasure('jetbridge').withState('present'))) {
      gsm.doneServicing();
    } else if (gsm.is('serviced') && ifStartServicing(x, servicingDef)) {
      gsm.startServicing();
    } else if ((gsm.is('serviced') || gsm.is('running')) && x.hasMeasure('jetbridge').withLastState('absent')) {
      gsm.planeLeaving();
    } else if (x.collection_time === x.shift_start) {
      gsm.goto('servicing');
      startIndex = i;
      turnStartTime = x.collection_time;
    }
    if ((moment(x.collection_time).diff(x.shift_end) === 0 || i === intervals.length - 1) && startIndex != -1) {
      endIndex = i;
      turnEndTime = x.collection_time;
    }
    if (startIndex != -1 && endIndex != -1) {
      while (startIndex <= endIndex) {
        intervals[startIndex].turn.turnStartTime = turnStartTime;
        intervals[startIndex].turn.turnEndTime = turnEndTime;
        startIndex++;
      }
      startIndex = -1;
      endIndex = -1;
      turnStartTime = '';
      turnEndTime = '';
    }
    x.state = gsm.state;
    x.images = x.concurrentImages[cfg.cs_config['img_source_measure_name']] || null;
  });
  return intervals;
}

async function fetchTurn(customer, queryParams) {
  let {data, config} = await fetchDataAndConfig(customer, queryParams);
  let shiftInfo = await shifts.findShiftInfoForGroundOps(customer, {
    time: queryParams.from,
    facility: queryParams.facility
  });
  //list of configured subjects
  let configuredSubjects = [...new Set(config.map(x => x.subject_name))];
  if (configuredSubjects.find(s => (s.indexOf('&') !== -1 || s.indexOf('#') !== -1)) !== undefined) throw new Error('Subject names and measure names in CRCs must not contain ampersands (&) nor pound signs (#).');
  if (queryParams.facility) data = applyFacilityFilter(queryParams.facility, data);

  //Sort and smooth data
  let events = _.sortBy(data, ['facility', 'subject_name', 'measure_name', 'collection_time']);
  events = events.map(x => {
    return {
      ...x,
      shift_start: moment(shiftInfo.shift_start || x.start_time),
      shift_end: moment(shiftInfo.shift_end || x.end_time),
      images: x.images ? [x.images] : [],
      collection_time: moment(x.collection_time),
      collection_interval: customer === 'tia' ? 75 : x.collection_interval ? moment.duration(x.collection_interval).asSeconds() : 60 //for tia set collection interval to 75s due to gateway drifts observed
    };
  });

  if (customer === 'tia') {

    events = events.map((e, i, ary) => smoothedStateForGroundOps(i, ary, searchConfig(ary[i], config)[0])); // smooth the detail measures
    events = events.map((e, i, ary) => smoothedStateForGroundOps(i, ary, searchConfig(ary[i], config)[0])); // smooth again
  } else if (customer === 'gatwickairport') {
    events = events.map((e, i, ary) => smoothedAbsentStateForTia(i, ary, searchConfig(ary[i], config)[0]));
    events = events.map((e, i, ary) => smoothedPresentStateForTia(i, ary, searchConfig(ary[i], config)[0]));
  } else {
    events = events.map((e, i, ary) => smoothedState(i, ary, searchConfig(ary[i], config)[0])); // smooth the detail measures
  }

  // eslint-disable-next-line no-unused-vars,no-undef
  if (config.map(a => a.measure_name).includes('composite')) {
    let groups = grpConcurrentEvents(events);
    // let composite = calculateComposite(groups, config);
    let composite = calculateCompositeForGroundOps(groups, config);
    events = events.concat(composite);
  }
  //Coalesce into intervals
  events = _.sortBy(events, ['facility', 'subject_name', 'measure_name', 'collection_time']);

  // do we really need 'await' here?

  let intervals = await coaleseEvents(events);
  let smoothenedIntervals = isCgrGroundOps(customer) ? await smoothenServicingState(intervals) : intervals;

  //Convert to subjects
  let theSubjects = curateSubjects(smoothenedIntervals) || { subjects: [] };
  theSubjects.timeLineStart = data.length ? data[0].start_time : null;
  theSubjects.timeLineEnd = data.length ? data[0].end_time : null;
  theSubjects.shiftName = queryParams.shift || undefined;

  const theMissingDataHandler = newMissingDataHandler(customer, config);
  return await theMissingDataHandler(theSubjects);
}

function filterStatesBasedOnTheConfigs(events, config, phase) {
  return events.filter((e) => {
    const cfg = searchConfig(e, config);

    let filterStates = Array.isArray(cfg) && cfg[0].cs_config && cfg[0].cs_config.filter_states ? cfg[0].cs_config.filter_states : [];

    // Backward Compatibility - Convert to new format
    // Example: From: filter_states : ['present']
    // To: filter_states : [
    //       {  'state': 'present',
    //          'phases': ['fetching', 'dispatch']
    //       }
    //     ]
    filterStates = filterStates.map(element => 'string' === typeof element ? ({ state: element, phases: ['fetching','dispatch'] }) : element);

    return filterStates && filterStates.length ?
      !filterStates.some(element => element.state === e.state && element.phases.includes(phase)) : true;
  });
}

async function fetchOptionalComponentsParams(settings) {
  const OptionalComponentsParams = {};

  try {
    OptionalComponentsParams.optionalLookAheadDurationinSeconds = settings.length && settings[0].settings ? settings[0].settings.optionalLookAheadDurationinSeconds * 1000 : 240 * 1000;
    OptionalComponentsParams.optionalLookBackDurationinSeconds = settings.length && settings[0].settings ? settings[0].settings.optionalLookBackDurationinSeconds * 1000: 240 * 1000;
  } catch (ignored) {
    /* exception ignored on purpose, we proceed even if we fail to obtain the configuration */
    OptionalComponentsParams.optionalLookAheadDurationinSeconds = 240 * 1000;
    OptionalComponentsParams.optionalLookBackDurationinSeconds = 0;
  }

  return OptionalComponentsParams;
}

async function fetchMissingGapsDuration(customer, facility) {
  let missingGapsFillDurationInMinutes = undefined;
  try {
    const { settings } = await reportCfgAPIs.getCustomerConfigs({ customer, facility });
    missingGapsFillDurationInMinutes = settings.length && settings[0].settings ? settings[0].settings.missingGapsFillDurationInMinutes : undefined;
  } catch (ignored) {
    /* exception ignored on purpose, we proceed even if we fail to obtain the configuration */
  }
  return missingGapsFillDurationInMinutes;
}

const EMPTY_EVENT = Object.freeze({});
const findClosestOptional = ({ collection_time: targetCollectionTime}, threshold, events) => {
  let left = 0;
  let right = events.length - 1;
  while (left < right) {
    let midpoint = Math.floor((left + right) / 2);

    const { collection_time: currentCollectionTime } = events[midpoint];

    const delta = currentCollectionTime.diff(targetCollectionTime);

    if (threshold >= Math.abs(delta)) {
      return Object.freeze({
        index: midpoint,
        event: events[midpoint],
        event_order: delta < 0 ? -1 : 1
      });
    } else if (delta > threshold) {
      right = midpoint - 1;
    } else {
      left = midpoint + 1;
    }
  }

  return Object.freeze({
    index: -(left + 1),
    event: EMPTY_EVENT
  });
};

/**
 * Always augment the composite with optional states info if available
 * @param events
 * @param config
 * @param composite
 */
function augmentCompositeWithOptionals({events, composite, config, threshold}) {
  /**
   * get optional measures from config
   * filter events by the optional measures
   * filter the composites with _state other than `ready-augmented`
   * for every composite left, find the closes optional event available
   */
  composite = _.sortBy(composite, ['facility', 'subject_name', 'collection_time']);
  const servicing_measures = new Set();
  let interesting_states_for_optionals = new Set();
  config.forEach((configEntry) => {
    if (configEntry.measure_name === 'composite' && configEntry.composite_definition) {
      configEntry.composite_definition.forEach(cd => {
        Object.values(cd).forEach(measure => {
          const keys = Object.keys(measure);
          keys.filter(
            (measureName) => measure[measureName] === ':optional'
          ).forEach(
            (measureName) => servicing_measures.add(measureName)
          );
        });
        Object.keys(cd).forEach(function(key) {
          Object.keys(cd[key]).forEach(function(measure){
            if (cd[key][measure] === ':optional') {
              interesting_states_for_optionals.add(key);
            }
          });
        });
      });
    }
  });

  events = events.filter((event) => servicing_measures.has(event.measure_name));
  events = _.sortBy(events, ['facility', 'subject_name', 'collection_time']);

  const subjectGroups = events.reduce((subjectName, event) => {
    if (!subjectName[event.subject_name]) {
      subjectName[event.subject_name] = [];
    }
    subjectName[event.subject_name].push(event);
    return subjectName;
  }, {});
  return composite.map((target, index, array) => {

    let match = null;
    // Optional association criteria
    // 1. Needs be specified in composite definition for a particular state
    // 2. Only associate with Transition event OR start and end of the event
    const allEvents = subjectGroups[target.subject_name] || [];
    const filteredEventswithLP = allEvents.filter((e) => {
      return e.state !== 'No LPN';
    }) || [];
    if (interesting_states_for_optionals.has(target.state)) {
      if ( (index === 0 || index === array.length - 1)
        || (index + 1 < array.length && array[index].state !== array[index+1].state)
        || (index - 1 >= 0 && array[index - 1].state !== array[index].state)
        || ((index + 1 < array.length && index - 1 > 0)
        && ((array[index].collection_time - array[index - 1].collection_time > 240000) //4 minutes
        || (array[index + 1].collection_time - array[index].collection_time > 240000)))) { //4 minutes
        // In case of License plate 'optional', first look for image with LP(filteredEventswithLP),
        // And then as a fall back look for any activity event (allEvents)
        match = findClosestOptional(target, threshold, filteredEventswithLP);
        if (match.event === EMPTY_EVENT) {
          match = findClosestOptional(target, threshold, allEvents);
        }
      } else {
        match = Object.freeze({
          index: -1,
          event: EMPTY_EVENT
        });
      }
    } else {
      match = Object.freeze({
        index: -1,
        event: EMPTY_EVENT
      });
    }

    if (match.event === EMPTY_EVENT) {
      /** nothing todo, no event close enough */
    } else {
      /** merge match into event */
      const { event: element } = match;
      element.images.forEach((image) => {
        image.measure = element.state;
      });
      if (match.event_order === -1 ) {
        target.images = [...element.images, ...target.images];
      } else {
        target.images = [...target.images, ...element.images];
      }
    }

    return target;
  });
}


async function fetchSubjects(customer, queryParams) {
  debug('[STANDARD]  Obtaining configuration and data for %s', JSON.stringify(queryParams, null, 2));
  let {data, config, filters} = await fetchDataAndConfig(customer, queryParams);
  //Apply temporary patch for OCR for schneider
  if (customer === 'schneiderelectric') {
    data = patchOCRState(data);
  }

  // Only keeping subject-measures configs which are not hidden
  config = config.filter((configEntry) => configEntry.hidden !== true);

  // Filter out states listed it in the 'customer_report_config->>cs_config' row has 'filter_states'
  // (only keep the states not in filter_states)
  // Filtering before calculating the composite
  // Example:
  // "filter_states": [
  //   {  state : 'present', phases: ['dispatch'],
  //      state: 'absent', phases: ['fetching']
  //   }
  // ]
  data = filterStatesBasedOnTheConfigs(data, config, 'fetching');

  //list of configured subjects
  //let configuredSubjects = [...new Set(config.map(x => x.subject_name))];
  //if (configuredSubjects.find(s => (s.indexOf('&') !== -1 || s.indexOf('#') !== -1)) !== undefined) throw new Error('Subject names and measure names in CRCs must not contain ampersands (&) nor pound signs (#).');
  if (queryParams.facility) data = applyFacilityFilter(queryParams.facility, data);

  //Sort and smooth data
  let events = _.sortBy(data, ['facility', 'subject_name', 'measure_name', 'collection_time']);

  /**
   * TODO: (eprha.carvajal create a feature flag to enable/disable LPN filtering) [Relates to AT-4445]
   * (!event.images || !event.state || event.images.step_name != 'lpr'
   */

  events = events.filter(
    (event) => filterByBlacklistRules(event.state)
  ).map(x => {
    return {
      ...x,
      shift_start: moment(x.start_time),
      shift_end: moment(x.end_time),
      images: x.images ? [{...x.images, measure: x.measure_name}] : [],
      collection_time: moment(x.collection_time),
      collection_interval: customer === 'tia' ? 75 : x.collection_interval ? moment.duration(x.collection_interval).asSeconds() : 60 //for tia set collection interval to 75s due to gateway drifts observed
    };
  });

  //Enumerate number of operators for SchaefflerFM1
  if (customer === 'schaefflerfm1') events = enumOperatorCount(events);

  events = filterJobNumberFromOCR(customer, events); //Only keep the first number string of the OCR for tesla
  if (customer === 'tia') {
    events = events.map((e, i, ary) => smoothedStateForGroundOps(i, ary, searchConfig(ary[i], config)[0])); // smooth the detail measures
    events = events.map((e, i, ary) => smoothedStateForGroundOps(i, ary, searchConfig(ary[i], config)[0])); // smooth again
  } else if (customer === 'gatwickairport') {
    events = events.map((e, i, ary) => smoothedAbsentStateForTia(i, ary, searchConfig(ary[i], config)[0]));
    events = events.map((e, i, ary) => smoothedPresentStateForTia(i, ary, searchConfig(ary[i], config)[0]));
  } else {
    events = events.map((e, i, ary) => smoothedState(i, ary, searchConfig(ary[i], config)[0])); // smooth the detail measures
  }
  if (customer === 'tesla') {
    events = events.map((e, i, ary) => smoothOCR(i, ary, searchConfig(ary[i], config)[0]));
  }
  if (filters.length) {
    events = applyCustomerFilters(filters, events, config.filter(x=>x.facility === queryParams.facility));
  }

  // eslint-disable-next-line no-unused-vars,no-undef
  if (config.map(a => a.measure_name).includes('composite')) {

    const onlyCompositeComponents = keepOnlyCompositeComponents(events, config);

    const facility = queryParams.facility;
    const { settings } = await reportCfgAPIs.getCustomerConfigs({ customer, facility });

    const compositeCollectionInterval = settings.length && settings[0].settings ? settings[0].settings.compositeCollectionInterval : null;

    let groups = grpConcurrentEvents(onlyCompositeComponents, compositeCollectionInterval);

    if(filters.length) {
      groups = applyCustomerFilters(filters, groups, config.filter(x=>x.facility === queryParams.facility));
    }
    let composite = isCgrGroundOps(customer) ? calculateCompositeForGroundOps(groups, config) : calculateComposite(groups, config);


    const OptionalComponentsParams = await fetchOptionalComponentsParams(settings);
    const threshold = Math.min(
      OptionalComponentsParams.optionalLookBackDurationinSeconds,
      OptionalComponentsParams.optionalLookAheadDurationinSeconds
    );
    composite = augmentCompositeWithOptionals({ events, composite, config, threshold });

    events = events.concat(composite);
    // filter States from composite and the other Atomic States with 'dispatch' filter config
    events = filterStatesBasedOnTheConfigs(events, config, 'dispatch');
  }

  //Coalesce into intervals
  events = _.sortBy(events, ['facility', 'subject_name', 'measure_name', 'collection_time']);
  let intervals = customer === 'tesla' ? await coaleseEventsWithNoInterval(events) : await coaleseEvents(events);
  let smoothenedIntervals = isCgrGroundOps(customer) ? await smoothenServicingState(intervals) : intervals;
  //Convert to subjects

  const missingGapsFillDurationInMinutes = await fetchMissingGapsDuration(customer, queryParams.facility);

  if (missingGapsFillDurationInMinutes) {
    smoothenedIntervals = fillGapsofNMinutes(smoothenedIntervals, missingGapsFillDurationInMinutes);
  }

  smoothenedIntervals = portalHelper.generatePortalImageLinks(smoothenedIntervals, customer);
  let theSubjects = curateSubjects(smoothenedIntervals) || {subjects: []};

  //Adding missing subjects and measures
  let tempSubjects = addMissingSubjectsAndMeasures(theSubjects, config);

  theSubjects.subjects = Object.values(tempSubjects);
  theSubjects.timeLineStart = data.length ? data[0].start_time : null;
  theSubjects.timeLineEnd = data.length ? data[0].end_time : null;
  theSubjects.shiftName = queryParams.shift || undefined;
  theSubjects.filters = filters;

  const theMissingDataHandler = newMissingDataHandler(customer, config);
  return await theMissingDataHandler(theSubjects);
}


async function gateStatus(customer, queryParams) {
  let {data, heroImgs} = await fetchDataAndConfigForGates(customer, queryParams);
  if (queryParams.facility) data = applyFacilityFilter(queryParams.facility, data);
  if (queryParams.smoothening) {
    let filteredGates = ['gate_83_classifier', 'gate_87_classifier', 'gate_88_classifier'];
    data = data.filter(y => filteredGates.some(z => z === y.step_name));
  }

  //Sort and smooth data
  let events = _.sortBy(data, ['facility', 'subject_name', 'measure_name', 'collection_time']);
  events = events.map(x => {
    let splitSubjectName = x.subject_name.split('_');
    let subjectName = `${_.upperFirst(splitSubjectName[0])} ${splitSubjectName[1]}`;
    let temp = {
      ...x,
      images: x.images ? [x.images] : [],
      collection_time: moment(x.collection_time),
      collection_interval: customer === 'tia' ? 75 : x.collection_interval ? moment.duration(x.collection_interval).asSeconds() : 60 //for tia set collection interval to 75s due to gateway drifts observed
    };
    temp.subject_name = subjectName;
    return temp;
  });
  let cfg = {
    'Gate 82': {
      accuracy_threshold: 0.3
    },
    'Gate 83': {
      accuracy_threshold: 0.3
    },
    'Gate 84': {
      accuracy_threshold: 0.3
    },
    'Gate 85': {
      accuracy_threshold: 0.3
    },
    'Gate 86': {
      accuracy_threshold: 0.3
    },
    'Gate 88': {
      accuracy_threshold: 0.3
    },
    'Gate 87': {
      accuracy_threshold: 0.3
    },
    'Gate 90': {
      accuracy_threshold: 0.3
    },
    'Gate 89': {
      accuracy_threshold: 0.3
    },
    smoothing_look_back_slots: 2,
    smoothing_look_ahead_slots: 2,
    smoothing_look_back_slots_for_absent: 3,
    smoothing_look_ahead_slots_for_absent: 3,
    smoothing_look_back_slots_for_present: 3,
    smoothing_look_ahead_slots_for_present: 3
  };

  events = events.map((e) => setStateBasedOnThreshold(e, cfg));
  if (queryParams.smoothening) {
    events = events.map((e, i, ary) => smoothedAbsentStateForTia(i, ary, cfg));
    events = events.map((e, i, ary) => smoothedPresentStateForTia(i, ary, cfg));
  }
  //Coalesce into intervals
  events = _.sortBy(events, ['facility', 'subject_name', 'measure_name', 'collection_time']);
  let intervals = queryParams.smoothening ? await coaleseEventsWithDefInterval(events) : await coaleseEvents(events);

  //creating a mapping of the Gate and gwID for the glamour_crop thumbnails
  let gwMappingForHeroImgs = {
    'Gate 83': '38af294f151d',
    'Gate 85': '38af2901699e',
    'Gate 87': '38af29016951',
    'Gate 88': '38af29016a26'
  };

  //Convert to subjects
  let subjects = curateSubjects(intervals);
  if (subjects) {

    //temp logic for thumbnails
    subjects.subjects = subjects.subjects.map(x => {
      let mapping = heroImgs.filter(y => y.gateway_id === gwMappingForHeroImgs[x.name]);
      if (mapping.length) {
        return {
          ...x,
          thumbnail: {
            collection_time: mapping[0].collection_time,
            gateway_id: mapping[0].gateway_id
          }
        };
      } else {
        return x;
      }
    });
    subjects.shiftName = queryParams.shift;
  }
  return subjects;
}

const FilterKey = Object.freeze({
  create: ({ customer, facility, gateway_id, measure_name, group_name, mode }) => {
    return `${customer}#${facility}#${gateway_id}#${measure_name}#${group_name}:${mode}`;
  }
});

function applyCustomerFilters(filters, events, config) {
  //creating config object
  let configObj = config.reduce((theConfigObject, theFilterObject) => {
    const { subject_name, measure_name, extract_key } = theFilterObject;
    if (!theConfigObject[subject_name]) {
      theConfigObject[subject_name] = {};
    }
    if (!theConfigObject[subject_name][measure_name]) {
      theConfigObject[subject_name][measure_name] = {};
    }
    theConfigObject[subject_name][measure_name] = { extractKey: extract_key };
    return theConfigObject;
  }, {});

  // creating filter object
  const mapOfFilterObjects = FILTER_MODE_TYPES.reduce((mapOfFilters, theFilterMode) => {
    const mapOfFilterObjectsWithMode = filters.filter(
      (filterConfig) => theFilterMode === filterConfig.mode
    ).reduce((mapOfFilterWithMode, filterConfig) => {
      const filterKey = FilterKey.create(filterConfig);
      let filterChain = mapOfFilterWithMode[filterKey] || Filters.identity();

      if (hasPropertyValue(filterConfig, 'gateway_id')) {
        const { gateway_id } = filterConfig;
        filterChain = filterChain.and( withGatewayId(gateway_id) );
      }

      if (hasPropertyValue(filterConfig, 'subject_name')) {
        const { subject_name } = filterConfig;
        filterChain = filterChain.and( withSubjectName(subject_name) );
      }

      if (hasPropertyValue(filterConfig, 'measure_name')) {
        const { measure_name } = filterConfig;
        filterChain = filterChain.and( withMeasureName(measure_name) );
      }

      if (hasPropertyValue(filterConfig, 'min_threshold') || hasPropertyValue(filterConfig, 'max_threshold')) {
        const { filter_type, measure_name, min_threshold, max_threshold } = filterConfig;
        const theThresholdConfig = Object.freeze({ lower: min_threshold, upper: max_threshold, measure: measure_name});
        filterChain = filterChain.and( Filters.ofType(filter_type).withThresholds(theThresholdConfig) );
      }

      if (filterChain !== Filters.identity()) {
        mapOfFilterWithMode[filterKey] = filterChain;
      }

      return mapOfFilterWithMode;
    }, {});

    return Object.assign(mapOfFilters, { [theFilterMode]: mapOfFilterObjectsWithMode });
  }, {});

  const newEventsProcessor = (listOfFilters) => ({
    matcher: (eventObject) => {
      const listOfObservations = [];

      if (hasPropertyValue(eventObject, 'bounding_box')) {
        const mapOfObjects = eventObject['bounding_box'];

        if (hasPropertyValue(eventObject, 'concurrentBbox')) {
          const mapOfCompositeObjects = eventObject['concurrentBbox'];

          Object.keys(mapOfCompositeObjects).forEach((type) => {
            const listOfObjectAttributes = mapOfCompositeObjects[type];

            if (listOfObjectAttributes && Array.isArray(listOfObjectAttributes)) {
              listOfObjectAttributes.filter(element => !!element).forEach((measure) => {
                mapOfObjects[measure] = mapOfObjects[measure] || [];
                mapOfObjects[measure].concat(listOfObjectAttributes[measure]);
              });
            }
          });
        }

        Object.keys(mapOfObjects).forEach((type) => {
          const listOfObjectAttributes = mapOfObjects[type];

          if (listOfObjectAttributes && Array.isArray(listOfObjectAttributes)) {
            const CONFIDENCE_SCORE_INDEX = 4;
            const newObjectSource = (type, index) => {
              return () => Object.freeze({
                mark: () => {
                  const listOfBoundingBoxes = Object.assign({ [type]: [] }, eventObject['bounding_box'])[type];
                  if (listOfBoundingBoxes && listOfBoundingBoxes.length && index < listOfBoundingBoxes.length) {
                    eventObject['bounding_box'][type][index][CONFIDENCE_SCORE_INDEX] = 0.20;
                  } else if (listOfBoundingBoxes && listOfBoundingBoxes.length) {
                    const listOfConcurrentBoundingBoxes = Object.assign({ [type]: [] }, eventObject['concurrentBbox'])[type];
                    if (listOfConcurrentBoundingBoxes && listOfConcurrentBoundingBoxes.length) {
                      const target = index - listOfBoundingBoxes.length;
                      eventObject['concurrentBbox'][type][target][CONFIDENCE_SCORE_INDEX] = 0.20;
                    }
                  }
                }
              });
            };

            listOfObjectAttributes.map(
              /* map the bounding box array to an object */
              ([ x, y, width, height, confidence ]) => ({ type, x, y, width, height, confidence })
            ).map(
              /* map the bounding box object to an Observation */
              (objectAttributes, index) => {
                const theObservationObject = Observation.fromBoundingBox(objectAttributes);
                return Object.assign({}, theObservationObject, { getSourceObject: newObjectSource(type, index) });
              }
            ).forEach(
              /* append every observation to the list */
              (observation) => listOfObservations.push(observation)
            );
          }
        });
      }

      const { images } = Object.assign({ images: [] }, eventObject);
      const gateways = images.filter(
        (theImageObject) => hasOwnProperty.call(theImageObject, 'gateway_id')
      ).map(
        ( { gateway_id } ) => gateway_id.toLowerCase()
      );

      const { bounding_box, measure_name, subject_name, shift_name, state, facility } = eventObject;
      const theEventConfigObject = Object.freeze(configObj[subject_name]);

      const theEventObject = Object.assign({}, {
        state, shift_name, facility, measure_name, subject_name, bounding_box, gateways,
        _Observations: listOfObservations,
        _ConfigObject: theEventConfigObject
      });

      const theComplementObjects = {};
      const theEventVariations = listOfObservations.map((theObservationObject) => {
        const { type: theObservationType } = theObservationObject;

        if (!Object.prototype.hasOwnProperty.call(theComplementObjects, theObservationObject)) {
          const listOfComplementaryObservations = listOfObservations.filter(
            ({ type: theObservationType }) => theObservationType !== theObservationObject.type
          );
          Object.assign(theComplementObjects, { [theObservationType]: listOfComplementaryObservations });
        }

        const theLocalObservations = [].concat(theObservationObject).concat(theComplementObjects[theObservationType]);

        return Object.assign({}, {
          _ParentEventObject: theEventObject,
          state, shift_name, facility, measure_name, subject_name, bounding_box, gateways,
          _Observations: theLocalObservations,
          _ConfigObject: theEventConfigObject
        });
      });

      /* only evaluate the filter if we haven't found a match (OR behavior) */
      const listOfMatchingVariations = theEventVariations.filter(
        (theEventVariation) => listOfFilters.some((theApplicableFilterObject) => theApplicableFilterObject.match(theEventVariation))
      );

      listOfMatchingVariations.filter(({ _Observations }) => _Observations.length).forEach(({ _Observations }) => {
        const theEventObservation = _Observations.shift();
        const theSourceBoundingBox = theEventObservation.getSourceObject();
        theSourceBoundingBox.mark();
      });

      return listOfMatchingVariations.length > 0;
    }
  });

  const EventsProcessor = Object.freeze({
    NoOp: (listOfEvents) => listOfEvents,
    exclusion: (listOfEvents, listOfFilters) => {
      if (listOfFilters.length) {
        const theEventProcessor = newEventsProcessor(listOfFilters);
        return listOfEvents.filter((theEventObject) => theEventProcessor.matcher(theEventObject));
      }
      return listOfEvents;
    },
    marker: (listOfEvents, listOfFilters, theEventProcessor) => {
      if (listOfFilters.length) {
        const theEventProcessor = newEventsProcessor(listOfFilters);
        return listOfEvents.map(theEventObject => Object.assign(theEventObject, {
          _Hidden: theEventProcessor.matcher(theEventObject)
        }));
      }
      return listOfEvents;
    }
  });

  const listOfMatchingEvents = FILTER_MODE_TYPES.reduce((listOfEvents, theFilterType) => {
    const listOfExcludeFilters = Object.values(mapOfFilterObjects[theFilterType]);
    const theEventProcessor = EventsProcessor[theFilterType] || EventsProcessor.NoOp;
    return theEventProcessor(listOfEvents, listOfExcludeFilters);
  }, events);

  return listOfMatchingEvents;
}

async function fetchMetrics(customer, queryParams, facility) {
  let {data, config} = await fetchDataAndConfig(customer, queryParams);
  //Sort and smooth data
  let events = _.sortBy(data, ['facility', 'subject_name', 'measure_name', 'collection_time']);
  events = events.map(x => {
    return {
      ...x,
      images: x.images ? [{...x.images, measure: x.measure_name}] : [],
      collection_time: moment(x.collection_time),
      collection_interval: x.collection_interval ? moment.duration(x.collection_interval).asSeconds() : 60
    };
  });
  events = events.map((e, i, ary) => smoothedState(i, ary, searchConfig(ary[i], config)[0])); // smooth the detail measures

  if (config.some(({ measure_name }) => 'composite' === measure_name)) {
    let groups = grpConcurrentEvents(events);
    let composite = calculateComposite(groups, config);
    events = events.concat(composite);
  }

  return calculateMetrics(events, queryParams.measures, queryParams.states);
}

async function fetchDataAndConfig(customer, queryParams) {
  queryParams.from = queryParams.from || null;
  queryParams.to = queryParams.to || null;
  queryParams.byClass = queryParams.byClass || false;

  let queries = [
    /* Query to fetch the filters from userdb */
    `SELECT * FROM customer_filters
      WHERE customer = :customer
        AND facility = :facility
        AND is_enable = TRUE
      ORDER BY measure_name, group_name`,
    reportConfigQuery
  ];

  let data = [];
  let cachedRecords = undefined;
  //Fetch data => try from cache first; if not then get it from db
  if (Object.prototype.hasOwnProperty.call(queryParams, 'disable-cache')) {
    delete queryParams['disable-cache'];
  } else {
    const cachedData = await cache.get(customer, {...queryParams});
    data = data.concat(cachedData).filter(Boolean);
    cachedRecords = { rows: data };
  }

  /**
   * If we have no data cache, lets queue the subject query
   */
  if (data.length === 0) {
    queries.push(`${subjectReportQuery} SELECT * FROM subject_event`);
  }

  const dataSections = [undefined, undefined, cachedRecords];
  await Promise.allSettled(
    /* prepare the query context to the config */
    queries.map((sentence, index) => {
      const statement = (index === 0
        ? sql(sentence)({ customer, facility: queryParams.facility })
        : sql(sentence)(queryParams));

      const connection = (index === 0
        ? Object.freeze({ query: (text, values) => userDB.query(text, values) })
        : Object.freeze({ query: (text, values) => db.query(customer, text, values) }));

      return connection.query(statement.text, statement.values).then((result) => {
        dataSections[index] = Object.assign({ rows: [] }, result);
      }).catch((error) => {
        debug('Unable to process query for index', index);
      });
    })
  );

  const [ filters, config, records ] = dataSections.map(({ rows }) => rows);
  if (records && records.length && data.length === 0) {
    data = data.concat(records);
    cache.put(customer, {...queryParams}, records);
  }

  //Remove data that has no associated config
  data = data.filter((element) => searchConfig(element, config).length);

  return { data, config, filters };
}

async function fetchDataAndConfigForGates(customer, queryParams) {
  queryParams.from = queryParams.from || null;
  queryParams.to = queryParams.to || null;
  queryParams.byClass = queryParams.byClass || false;

  let status = sql(`${subjectReportQuery} SELECT * FROM gate_status`)(queryParams);
  let thumbnails = sql(`${subjectReportQuery} SELECT * FROM thumbnails`)(queryParams);
  // eslint-disable-next-line no-unused-vars
  let { rows: heroImgs } = await db.query(customer, thumbnails.text, thumbnails.values);
  let { rows: data } = await db.query(customer, status.text, status.values);

  return Object.freeze({ data, heroImgs });
}

function curateSubjects(intervals) {
  let highWaterMark = intervals.length ? intervals[0].shift_start : null;
  let timeSlotSize = null;
  const tempSubjects = {};
  _.each(intervals, (row) => {
    const subject = _.get(tempSubjects, row.subject_name, {});
    _.set(subject, 'name', row.subject_name);
    _.set(subject, 'facility', row.facility);

    const observation = {};
    _.set(observation, 'startTime', row.start_time);
    _.set(observation, 'endTime', row.end_time);
    highWaterMark = Math.max(highWaterMark, row.collection_interval ? (row.end_time - row.collection_interval * 1000) : row.start_time);

    _.set(observation, 'images', row.images);
    _.set(observation, 'd_images', row.d_images);
    _.set(observation, 'state', row.state);
    if (row.turn) {
      _.set(observation, 'turn', row.turn);
    }
    _.set(observation, 'missingMeasures', 'missingMeasures' in row ? row.missingMeasures : []);
    if (row.centroids) {
      _.set(observation, 'centroids', row.centroids);
    }

    if (row.display_trapezoid) {
      _.set(observation, 'trapezoid', row.display_trapezoid);
    }
    const obsAry = _.get(subject, `measures.${row.measure_name}`, []);
    obsAry.push(observation);
    _.set(subject, `measures.${row.measure_name}`, obsAry);
    _.set(tempSubjects, row.subject_name, subject);

    if (row.collection_interval) {
      timeSlotSize = Math.min(row.collection_interval * 1000, (timeSlotSize !== null) ? timeSlotSize : Infinity);
    }
  });

  if (!intervals.length) {
    return null;
  } else {
    return {
      timeSlotSizeMillis: timeSlotSize || 60000,
      shiftStartTime: intervals[0].shift_start,
      shiftEndTime: intervals[0].shift_end,
      timezone: intervals[0].tz,
      highWaterMarkTime: moment(highWaterMark).toDate(),
      subjects: _.values(tempSubjects)
    };
  }
}

function addMissingSubjectsAndMeasures(groupOfSubjects, config) {
  return config.reduce((mapOfSubjects, currentConfig) => {
    const subjectName = currentConfig.subject_name;
    const facilityName = currentConfig.facility;
    const measureName = currentConfig.measure_name;
    const theStepName = currentConfig.step_name;

    const { cs_config: customerConfig } = Object.assign({ cs_config: { image_source_step_name: theStepName } }, currentConfig);
    const stepName = Object.prototype.hasOwnProperty.call(customerConfig, 'image_source_step_name') ? customerConfig.image_source_step_name : theStepName;

    const theCandidateSubjects = groupOfSubjects.subjects.find(s => s.name === subjectName);
    const listOfSubjects = theCandidateSubjects || {
      name: subjectName,
      facility: facilityName,
      measures: {}
    };
    const subject = mapOfSubjects[subjectName] || listOfSubjects;
    if (!Object.keys(subject.measures).length || !(measureName in subject.measures)) {
      subject.measures[measureName] = [];
    }

    if (stepName !== theStepName) {
      const measures = subject.measures[measureName];
      measures.forEach((measureObject) => {
        const images = measureObject.images;
        images.forEach((imageObject) => imageObject['step_name'] = stepName);
      });
    }

    mapOfSubjects[subjectName] = subject;
    return mapOfSubjects;
  }, {});
}

function calculateMetrics(data, measures, states) {
  let metrics = data.reduce((r, o) => {
    //Initialize intervals and times to seconds for easier calculation
    let collection_time = moment(o.collection_time).valueOf() / 1000;
    let shiftEnd = moment(o.shift_end).valueOf() / 1000;
    let shiftStart = moment(o.shift_start).valueOf() / 1000;

    //Groupby facility + subject_name + date + shift_name and to calculate util across that group
    let key = o.facility + '-' + o.subject_name + '-' + o.date + '-' + o.shift_name;

    //Initialize what output should look like
    let newItem = r.get(key) || Object.assign({}, o, {
      highWaterMark: collection_time + o.collection_interval,
      numerator: moment.duration(0, 'minutes').asSeconds(),
      denominator: moment.duration(0, 'minutes').asSeconds(),
      utilization: 0.0,
      total_duration: 0
    });

    // end time of latest data slot we got for the group
    newItem.highWaterMark = Math.max(newItem.highWaterMark, collection_time + o.collection_interval);

    if (measures.includes(o.measure_name)) {
      // total time elapsed into a shift or the entire duration of shift whichever is smaller
      newItem.denominator = Math.min(shiftEnd, newItem.highWaterMark) - shiftStart;
      // duration of time in requested list of measures-states
      newItem.numerator = states.includes(o.state) ? newItem.numerator + o.collection_interval : newItem.numerator;
      // utilization
      newItem.utilization = (newItem.numerator / newItem.denominator) * 100;
      newItem.total_duration = newItem.numerator;
    }
    return r.set(key, newItem);
  }, new Map);
  metrics = [...metrics.values()];
  return metrics;
}

function patchOCRState(data) {
  let digitPattern = ['3701', '3101', '3707', '3107', '2701', '2101', '2707', '2107'];
  data.forEach(d => {
    let foundStates = d.full_response ? d.full_response.split('\n') : null;
    if (foundStates) {
      if (d.state === 'IDLE' && foundStates[0] !== 'IDLE') {
        d.state = 'OK';
      } else if (digitPattern.some(p => d.full_response.includes(p))) {
        d.state = 'IDLE';
      } else if (d.state === 'OK' && foundStates[0] !== 'OK') {
        d.state = 'IDLE';
      }
    }
  });
  return data;
}

function filterJobNumberFromOCR(customer, events) {
  return events.map(e => ({
    ...e,
    state: customer === 'tesla' ? e.state.replace(/(^\d+)(\D*.*)/i, '$1') : e.state
  }));
}

function enumOperatorCount(events) {
  return events.map(e => {
    if (e.operator_count && !isNaN(e.operator_count)) {
      const operatorCnt = Number(e.operator_count);
      e.state = operatorCnt === 0 ? 'absent' : operatorCnt === 1 ? '1 operator present' : 'Multiple operators present';
      return e;
    } else {
      return e;
    }
  });
}

//smooth the toggling between servicing and serviced
async function smoothenServicingState(intervals) {
  let flag = false;
  // let composite = intervals.filter(x=> x.measure_name === 'composite');
  let smoothedIntervals = [];
  intervals.forEach((x, i) => {
    if (i !== 0 && intervals[i - 1].subject_name == x.subject_name && x.measure_name === 'composite') {
      if (i != intervals.length - 1) {
        if (x.state == 'running') {
          flag = true;
          if (smoothedIntervals[smoothedIntervals.length - 1].measure_name === 'composite' && smoothedIntervals[smoothedIntervals.length - 1].state === 'running') {
            smoothedIntervals[smoothedIntervals.length - 1].end_time = x.end_time;
            smoothedIntervals[smoothedIntervals.length - 1].images.concat(x.images);
          } else {
            smoothedIntervals.push(x);
          }
        } else if (flag && (intervals[i + 1].state == 'running')) {
          smoothedIntervals[smoothedIntervals.length - 1].end_time = x.end_time;
          smoothedIntervals[smoothedIntervals.length - 1].images.concat(x.images);
          flag = false;
        } else {
          flag = false;
          smoothedIntervals.push(x);
        }
      } else {
        if (i != intervals.length - 1) {
          if (x.state == 'running') {
            flag = true;
            return x;
          } else if (flag == true) {
            x.state = 'running'; //injecting new state "not running" mapped in the report config table
            smoothedIntervals[smoothedIntervals.length - 1].end_time = x.start_time;
            smoothedIntervals[smoothedIntervals.length - 1].images.concat(x.images);
            flag = false;
          } else {
            // return x;
            smoothedIntervals.push(x);
          }
        }
      }
    } else {
      flag = false;
      // return x;
      smoothedIntervals.push(x);
    }
  });

  return smoothedIntervals;
}

module.exports = {
  applyCustomerFilters,
  addMissingSubjectsAndMeasures,
  fetchSubjects,
  fetchMetrics,
  smoothedState,
  coaleseEvents,
  fillGapsofNMinutes,
  gateStatus,
  grpConcurrentEvents,
  calculateComposite,
  fetchOptionalComponentsParams,
  fetchMissingGapsDuration,
  curateSubjects,
  calculateMetrics,
  fetchTurn
};
