'use strict';

(function (register){
  const ExtensionID = '78a82824-f29c-4871-afc6-f192008cf4cb';

  const onlyKeep = function (record, qualifier, values) {
    return record.attributes
      && record.attributes[qualifier]
      && values.includes(record.attributes[qualifier]['attr_value']);
  };

  const keepExcept = function (record, qualifier, values) {
    return !record.attributes
      || !record.attributes[qualifier]
      || !values.includes(record.attributes[qualifier]['attr_value']);
  };

  const newFilterPredicate = (attribute, values, exclusion) => {
    if (!exclusion){
      return (recordObject) => onlyKeep(recordObject, attribute, [...values]);
    } else {
      return (recordObject) => keepExcept(recordObject, attribute, [...values]);
    }
  };

  const records = [
    { uuid: 'b72d9d77-38a9-49f8-b194-29add67fa401', customer: 'trimac', site: 'TRIMAC TRAILERS', attribute: 'Type', value: 'TRAILER', enabled: true },
    { uuid: 'b72d9d77-38a9-49f8-b194-29add67fa401', customer: 'trimac', site: 'TRIMAC TRAILERS', attribute: 'customer_vehicle_type', value: 'trailer', enabled: true },

    { uuid: '95e5d5c7-2a43-4a04-8b6e-158c58a0f309', customer: 'trimac', site: 'TRIMAC TRUCKS', attribute: 'Type', value: 'TRACTOR', enabled: true },

    { uuid: 'd22c7ec9-1964-4f54-a8ec-b7697484a3d5', customer: 'trimac', site: 'NTS TRUCK', attribute: 'vehicle_class', value: 'front', enabled: true },
    { uuid: 'd22c7ec9-1964-4f54-a8ec-b7697484a3d5', customer: 'trimac', site: 'NTS TRUCK', attribute: 'vehicle_class', value: 'detached', enabled: false },

    { uuid: 'b89a31ec-b67c-4572-9e44-e7eba72210ee', customer: 'trimac', site: 'NTS TRAILER', attribute: 'customer_vehicle_type', value: 'trailer', enabled: true },
    { uuid: 'b89a31ec-b67c-4572-9e44-e7eba72210ee', customer: 'trimac', site: 'NTS TRAILER', attribute: 'vehicle_class', value: 'back', enabled: false },

    { uuid: 'd2eb8e5f-fb84-4099-83b1-8820dc50e4df', customer: 'trimac', site: 'NTS TRAILERS', attribute: 'customer_vehicle_type', value: 'trailer', enabled: true },
    { uuid: 'd2eb8e5f-fb84-4099-83b1-8820dc50e4df', customer: 'trimac', site: 'NTS TRAILERS', attribute: 'vehicle_class', value: 'back', enabled: false },

    { uuid: 'd2eb8e5f-fb84-4099-83b1-8820dc50e4fg', customer: 'lccaustinp2p9', site: 'P2P9-READY MIX', attribute: 'customer_vehicle_type', value: 'Ready Mix', enabled: true },
    { uuid: 'd2eb8e5f-fb84-4099-83b1-8820dc50e4gh', customer: 'lccaustinp3p4', site: 'P3P4-READY MIX', attribute: 'customer_vehicle_type', value: 'Ready Mix', enabled: true },

    { uuid: 'd2eb8e5f-fb84-4099-83b1-8820dc50e4hd', customer: 'lccaustinp2p9', site: 'P2P9-HAULERS', attribute: 'customer_vehicle_type', value: 'Ready Mix', enabled: true, exclusion: true },
    { uuid: 'd2eb8e5f-fb84-4099-83b1-8820dc50e4s2', customer: 'lccaustinp3p4', site: 'P3P4-HAULERS', attribute: 'customer_vehicle_type', value: 'Ready Mix', enabled: true, exclusion: true },

    { uuid: 'd2eb8e5f-fb84-4099-83b1-8820dc50e4hd', customer: 'invistartm', site: 'Rozenburg', attribute: 'customer_vehicle_type', value: 'Yard Tug', enabled: true, exclusion: true }
  ];

  register('@atollogy/trimac-hack', [], function () {
    const ALL_SITES = '*';
    const ALL_CUSTOMERS = '*';

    const getCustomerFilterForSite = (customer, site, record) => {
      const elements = records.filter(
        filter => filter.customer.toLocaleLowerCase() === customer.toLocaleLowerCase() || filter.customer.toLocaleLowerCase() === ALL_CUSTOMERS
      ).filter(
        filter => filter.site.toLocaleLowerCase() === site.toLocaleLowerCase() || filter.customer.toLocaleLowerCase() === ALL_SITES
      ).filter(
        filter => filter.enabled
      );

      const filters = elements.reduce((mapOfFilters, { uuid, attribute, value, exclusion }) => {
        let setOfValues = new Set();
        if (mapOfFilters.has(attribute)) {
          setOfValues = mapOfFilters.get(attribute);
        } else {
          mapOfFilters.set(attribute, setOfValues);
        }
        setOfValues.add(value);
        if (exclusion) {
          setOfValues.add(exclusion);
        }
        return mapOfFilters;
      }, new Map());

      const applicable = [];
      for (const [attribute, values] of filters) {
        let exclusion = false;
        if (values.size === 2) {
          exclusion = true;
          values.delete(exclusion);
        }
        applicable.push(newFilterPredicate(attribute, values, exclusion));
      }

      return applicable.length ? applicable.some(filter => filter(record)) : true;
    };

    return Object.freeze({
      getCustomerFilterForSite,
      ExtensionID
    });
  });
}(
  function (name, dependencies, factory) {
    const global = this;

    const objects = [];
    let newModuleInstance;

    if ('function' === typeof require && 'undefined' !== typeof module && hasOwnProperty.call(module, 'exports')) {
      for (let index = 0; index < dependencies.length; index += 1) {
        const requiredModule = dependencies[index];
        const moduleInstance = require( requiredModule );
        objects.push(moduleInstance);
      }

      newModuleInstance = factory.apply({}, objects);
      module.exports = newModuleInstance;
    } else {
      for (let index = 0; index < dependencies.length; index += 1) {
        const requiredModule = dependencies[index];
        const moduleInstance = global[requiredModule];
        objects.push(moduleInstance);
      }

      newModuleInstance = factory.apply({}, objects);
      global[name] = newModuleInstance;
    }

  }.bind(this)
));