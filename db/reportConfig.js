const fs = require('fs');
const path = require('path');
const db = require('../db/');
const sql = require('yesql').pg;
const reportConfigQuery = fs.readFileSync(path.join(__dirname, 'query', 'reportConfig.sql'), 'utf8');
const debug = require('debug')('portal:reportConfig');
const userManagement = require('../lib/user-management/');

module.exports.setConfig = async function (customer, insertParams) {
  const updateSql = `INSERT INTO customer_report_config (
    subject_name, measure_name, facility, gateway_name, camera_id,
    step_name, extract_type, extract_key, extract_image_name, cs_config, start_time
  ) VALUES (
    :subjectName, :measureName, :facilityName, :gwName, :camId,
    :stepName, :extractType, :extractKey, :extractImgName, :csConfig, now()
  ) ON CONFLICT (
    subject_name, measure_name, facility
  ) DO UPDATE SET
    gateway_name = :gwName,
    camera_id = :camId,
    step_name = :stepName,
    extract_type = :extractType,
    extract_key = :extractKey,
    extract_image_name = :extractImgName,
    cs_config = :csConfig`;
  let q = sql(updateSql)(insertParams);
  try{
    let {rows} = await db.query(customer, q.text, q.values, true);
    debug(rows);
  }catch(e){
    debug(e);
  }
};

module.exports.getConfig = async function(customer, parameters){
  const query = sql(reportConfigQuery)(parameters);
  let { rows } = await db.query(customer, query.text, query.values);
  return rows;
};

module.exports.deleteConfig = async function(customer, parameters){
  const query =  `UPDATE customer_report_config SET end_time = now() WHERE facility = :facility AND subject_name = :subject AND measure_name = :measureName`;
  const q = sql(query)(parameters);
  await db.query(customer, q.text, q.values, true);
};

module.exports.getCustomerConfigs = async function ({ customer, facility }) {
  const parameters = { customer, facility };
  const queries = [
    `SELECT * FROM customer_asset_config a WHERE a.customer = :customer AND a.facility = :facility;`,
    `SELECT * FROM customer_profile p WHERE p.customer = :customer AND p.facility = :facility;`
  ];
  const configSections = [undefined, undefined];
  await Promise.allSettled(
    queries.map( /* prepare the query context to the config */
      (sentence) => sql(sentence)(parameters)
    ).map( /* use the context from the previous map operation and execute the query */
      (queryContext, index) => userManagement.query(queryContext.text, queryContext.values).then((result) => configSections[index] = result)
    )
  );

  const [assetConfigs, settings] = configSections.filter(Boolean).map((recordset) => recordset.rows);

  return { assetConfigs, settings };
};
